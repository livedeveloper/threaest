//
//  Global.swift
//  Threadest
//
//  Created by Jaydeep on 22/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation
import UIKit

//Postion of scrollview of all viewcontroller
enum Position {
    case Center
    case Top
    case Bottom
    case Left
    case Right
}

enum postNotificationType: String {
    case notificationFollow = "notificationFollow"
    case notificationPost = "notificationPost"
    case notificationComment = "notificationComment"
    case notificationOrderSucessfully = "notificationOrderSucessfully"
    case notificationSearchDismiss = "notificationSearchDismiss"
    case notificationInfluenceScore = "notificationInfluenceScore"
    case notificationNearBy = "notificationNearBy"
    case notificationProduct = "notificationProduct"
}

enum userDefaultKey: String {
    case influencerScoreDefault = "influencerScoreDefault"
    case currentInfluencerScoreDefault = "currentInfluencerScoreDefault"
    case toolTipCameraLibraryDefault = "toolTipCameraLibraryDefault"
    case toolTipCameraFilterDefault = "toolTipCameraFilterDefault"
    case toolTipCameraTagDefault = "toolTipCameraTagDefault"
    
    case toolTipSocialTDefault = "toolTipSocialTDefault"
    case toolTipSocialRethreadDefault = "toolTipSocialRethreadDefault"
    case toolTipSocialScoreDefault = "toolTipSocialScoreDefault"
    
    case toolTipClosetTabIconDefault = "toolTipClosetTabIconDefault"
    case toolTipClosetExpandArrowDefault = "toolTipClosetExpandArrowDefault"
    case toolTipClosetYourClosetDefault = "toolTipClosetYourClosetDefault"
    
    case lastSharePostDateDefault = "lastSharePostDateDefault"
    case newExperiencePopup = "newExperiencePopup"
    
    case acceptPushNotification = "acceptPushNotification"
    case ARONOFF = "ARONOFF"
    
    case coachMarkBrandDefault = "coachMarkBrandDefault"
    case coachMarkARClosetDefault = "coachMarkARClosetDefault"
    case coachMarkNONARClosetDefault = "coachMarkNONARClosetDefault"
    
    case tapMeShowFirstDefault = "tapMeShowFirstDefault"
    case bulletinVideoTutorial = "bulletinVideoTutorial"
    case bulletinPopupShopNearYou = "bulletinPopupShopNearYou"
    case bulletinWelcomePopupCloset = "bulletinWelcomePopupCloset"
}

enum FONTS : String {
    case  SFUITextSemibold = "SFUIText-Semibold"
    case  SFUITextRegular = "SFUIText-Regular"
}

let PRIVACY_POLICY = "http://www.threadest.com/users/privacy_policy"
let deviceType = UIDevice.current.userInterfaceIdiom
let screenscale = UIScreen.main.bounds.width/320.0
let screenwidth = UIScreen.main.bounds.width
let screenheight = UIScreen.main.bounds.height

let userdefault = UserDefaults.standard
let appdelegate = UIApplication.shared.delegate as! AppDelegate

//Grid width for collectionview
let gridWidth : CGFloat = (UIScreen.main.bounds.width/3)-5.0
//User default key
let threadestFirstRunDefaultsKey = "threadestFirstRun"



extension CGFloat {
    var degreesToRadians: CGFloat { return CGFloat(self) * CGFloat(Double.pi / 180) }
    var radiansToDegrees: CGFloat { return CGFloat(self) * CGFloat(180 / Double.pi) }
}
