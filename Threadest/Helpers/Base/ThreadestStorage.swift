//
//  ThreadestStorage.swift
//  Threadest
//
//  Created by mac on 3/8/18.
//  Copyright © 2018 JC. All rights reserved.
//

import Foundation
import KeychainSwift

class ThreadestStorage: NSObject {
    
    
    /*
     * Verify whether the app just installs or not. If it's first installation, it will be clear data storrage in keychain.
     *
     */
    static func clearDataInKeychainIfNeeded() {
        
        let userDefaults = UserDefaults.standard
        if userDefaults.object(forKey: threadestFirstRunDefaultsKey) == nil {
            userDefaults.set(true, forKey: threadestFirstRunDefaultsKey)
            userDefaults.synchronize()
            let keychainSwift = KeychainSwift()
            keychainSwift.clear()
        }
    }
    
    static func isCoachMarkBrandDefault () -> Bool {
        let userDefaults = UserDefaults.standard
        if userDefaults.object(forKey: userDefaultKey.coachMarkBrandDefault.rawValue) == nil {
            userDefaults.set(true, forKey: userDefaultKey.coachMarkBrandDefault.rawValue)
            userDefaults.synchronize()
            return true
        }
        return false
    }
    
    static func clearCoachMarkIfNeeded() {
        
        if userdefault.object(forKey: userDefaultKey.coachMarkBrandDefault.rawValue) != nil {
            userdefault.removeObject(forKey: userDefaultKey.coachMarkBrandDefault.rawValue)
            userdefault.synchronize()
        }
        if userdefault.object(forKey: userDefaultKey.coachMarkARClosetDefault.rawValue) != nil {
            userdefault.removeObject(forKey: userDefaultKey.coachMarkARClosetDefault.rawValue)
            userdefault.synchronize()
        }
        if userdefault.object(forKey: userDefaultKey.coachMarkNONARClosetDefault.rawValue) != nil {
            userdefault.removeObject(forKey: userDefaultKey.coachMarkNONARClosetDefault.rawValue)
            userdefault.synchronize()
        }
    }
}
