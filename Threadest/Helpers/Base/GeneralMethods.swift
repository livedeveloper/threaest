//
//  GeneralMethods.swift
//  Threadest
//
//  Created by Jaydeep on 02/04/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import ISO8601
import FBSDKLoginKit
import FBSDKCoreKit
import Social
import TwitterKit
import SDWebImage
import Crashlytics
import UserNotifications
import Lottie
import ARKit
import Alamofire


class GeneralMethods: NSObject {
    static let sharedInstance = GeneralMethods()

    let modelsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    let animationView = LOTAnimationView(name: "loader_ring")

    //ARView
    func loadNodeWithID(_ id: String, modelUrl: String, completion: @escaping (SCNNode?) -> Void) {
        // Check that assets for that model are not already downloaded
        let fileManager = FileManager.default
        let dirForModel = modelsDirectory.appendingPathComponent(id)
        let dirExists = fileManager.fileExists(atPath: dirForModel.path)
        if dirExists {
            completion(loadNodeWithIdFromDisk(id))
        } else {
            let dumbURL = modelUrl
            downloadZip(from: dumbURL, at: id) {
                if let url = $0 {
                    print("Downloaded and unzipped at: \(url.absoluteString)")
                    completion(self.loadNodeWithIdFromDisk(id))
                } else {
                    print("Something went wrong!")
                    completion(nil)
                }
            }
        }
    }


    func loadNodeWithIdFromDisk(_ id: String) -> SCNNode? {
        let fileManager = FileManager.default
        let dirForModel = modelsDirectory.appendingPathComponent(id).appendingPathComponent(id)
        do {
            let files = try fileManager.contentsOfDirectory(atPath: dirForModel.path)
            if let objFile = files.first(where: {
                $0.hasSuffix(".scn")
                }) {
                let objScene = try? SCNScene(url: dirForModel.appendingPathComponent(objFile), options: nil)
                objScene?.rootNode.position = SCNVector3Zero
                objScene?.rootNode.eulerAngles = SCNVector3Zero
                let objNode = objScene?.rootNode.childNode(withName: "Cube", recursively: true)
                objNode?.position = SCNVector3Zero
                objNode?.eulerAngles = SCNVector3Zero

                debugPrint(objNode?.name)
                if let tgaFiles = files.first(where: { $0.hasSuffix(".tga") }) {
                    let img = load(fileURL: dirForModel.appendingPathComponent(tgaFiles))
                    let imgMaterial = SCNMaterial()
                    imgMaterial.isDoubleSided = false
                    imgMaterial.diffuse.contents = img
                    objNode?.geometry?.materials = [imgMaterial]
                    //objNode?.eulerAngles.x = (.pi/180)
                    objNode?.eulerAngles.y = -.pi/2
                    objNode?.eulerAngles.z = -.pi/2
                    debugPrint(objNode?.geometry)
                    return objNode
                }
                return objNode
            } else {
                print("No obj file in directory: \(dirForModel.path)")
                return SCNNode()
            }
        } catch {
            print("Could not enumarate files or load scene: \(error)")
            return SCNNode()
        }
    }

    func downloadZip(from urlString: String, at destFileName: String, completion: ((URL?) -> Void)?) {
        print("Downloading \(urlString)")
        let fullDestName = destFileName + ".zip"

        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let fileURL = self.modelsDirectory.appendingPathComponent(fullDestName)
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }

        Alamofire.download(urlString, to: destination).response { response in
            let error = response.error
            if error == nil {
                if let filePath = response.destinationURL?.path {
                    let nStr = NSString(string: filePath)
                    let id = NSString(string: nStr.lastPathComponent).deletingPathExtension
                    print(response)
                    print("file downloaded at: \(filePath)")
                    let fileManager = FileManager()
                    let sourceURL = URL(fileURLWithPath: filePath)
                    var destinationURL = self.modelsDirectory
                    destinationURL.appendPathComponent(id)
                    do {
                        try fileManager.createDirectory(at: destinationURL, withIntermediateDirectories: true, attributes: nil)
                        try fileManager.unzipItem(at: sourceURL, to: destinationURL)
                        completion?(destinationURL)
                    } catch {
                        completion?(nil)
                        print("Extraction of ZIP archive failed with error: \(error)")
                    }
                } else {
                    completion?(nil)
                    print("File path not found")
                }
            } else {
                // Handle error
                completion?(nil)
            }
        }
    }

    func publishMessage(message: String, link: String, completion:@escaping () -> Void) {
        FBSDKGraphRequest.init(graphPath: "me/feed", parameters: ["message" : message, "link": link], httpMethod: "POST").start(completionHandler: { (connection, result, error) -> Void in
            if let error = error {
                print("Error: \(error)")
            } else {
                print("Success")
                completion()
            }
        })
    }

    func loadLoading(view: UIView) {
        DispatchQueue.main.async {
            appdelegate.window?.isUserInteractionEnabled = false
            self.animationView.animationProgress = 100
            self.animationView.frame = CGRect(x: 0, y: 0, width: 160, height: 160)
            self.animationView.center = (appdelegate.window?.rootViewController?.view.center)!
            self.animationView.contentMode = .scaleAspectFill
            self.animationView.animationSpeed = 1
            appdelegate.window?.addSubview(self.animationView)
            self.animationView.loopAnimation = true
            view.isUserInteractionEnabled = false
            self.animationView.play { (sucess) in
                self.animationView.loopAnimation = true
                self.animationView.removeFromSuperview()
            }
        }
    }

    //to load image from url of local directory
    func load(fileURL: URL) -> UIImage? {
        //  let fileURL = documentsUrl.appendingPathComponent(fileName)
        do {
            let imageData = try Data(contentsOf: fileURL)
            return UIImage(data: imageData)
        } catch {
            print("Error loading image : \(error)")
        }
        return nil
    }

    func dismissLoading(view: UIView) {
        appdelegate.window?.isUserInteractionEnabled = true
        view.isUserInteractionEnabled = true
        self.animationView.pause()
    }

    func dispatchlocalNotification(with title: String, body: String, userInfo: [AnyHashable: Any]? = nil, at date:Date) {
        if #available(iOS 11.0, *) {
            let highFiveAction = UNNotificationAction(identifier: "product", title: title, options: [])
            let category = UNNotificationCategory(identifier: "product", actions: [highFiveAction], intentIdentifiers: [], hiddenPreviewsBodyPlaceholder: "", options: [.customDismissAction])
            UNUserNotificationCenter.current().setNotificationCategories([category])

            // 2
            let highFiveContent = UNMutableNotificationContent()
            highFiveContent.title = title
            highFiveContent.body = body

            // 3
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.1, repeats: false)

            // 4
            let highFiveRequestIdentifier = "sampleRequest"
            let highFiveRequest = UNNotificationRequest(identifier: highFiveRequestIdentifier, content: highFiveContent, trigger: trigger)
            UNUserNotificationCenter.current().add(highFiveRequest) { (error) in
                // handle the error if needed
            }
        } else {
            let notification = UILocalNotification()
            notification.fireDate = date
            notification.alertTitle = title
            notification.alertBody = body
            if let info = userInfo {
                notification.userInfo = info
            }
            notification.soundName = UILocalNotificationDefaultSoundName
            UIApplication.shared.scheduleLocalNotification(notification)
        }
    }

    func RethreadPostSucessPopup(message: String, viewController: UIViewController) {
        let facebookPostiongpopup = THFacebookPostingPopUp(nibName: THFacebookPostingPopUp.className, bundle: nil)
        facebookPostiongpopup.message = message
        viewController.addChildViewController(facebookPostiongpopup)
        facebookPostiongpopup.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
        viewController.view.addSubview(facebookPostiongpopup.view)
        facebookPostiongpopup.didMove(toParentViewController: viewController)
    }

    func uploadTwitterMedia(viewController: UIViewController, imageURL: String, status: String, token: String, userId: String, id: Int, type: String) {
        let _ = SDWebImageManager.shared().imageDownloader?.downloadImage(with: URL(string: imageURL), options: .allowInvalidSSLCertificates, progress: { (block1, block2, url) in
        }, completed: { (image, data, error, bool) in
            let client = TWTRAPIClient(userID: userId)
            client.uploadMedia(UIImageJPEGRepresentation(image!, 1)!, contentType: "media", completion: { (result, error) in
                if result != nil {
                    let requestURL = "https://api.twitter.com/1.1/statuses/update.json"
                    let parameters = ["status" : status,"media_ids":result!]
                    var request = client.urlRequest(withMethod: "POST", urlString: requestURL, parameters: parameters, error: nil)
                    request.addValue(token, forHTTPHeaderField: "Authorization")
                    client.sendTwitterRequest(request) { (response, responseData, error) -> Void in
                        if let err = error {
                            print("Error: \(err.localizedDescription)")
                        } else {
                            //Add funnel for Rethread Threadest Post
                            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                                Answers.logCustomEvent(withName: "Rethread post", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "post_id":"\(id)", "rethread_type":"Twitter"])
                            }
                            DispatchQueue.main.async {
                                GeneralMethods.sharedInstance.RethreadPostSucessPopup(message: "Post to Twitter - Successful", viewController: viewController)
                            }
                        }
                    }
                }
            })
        })
    }

    func AlertviewGeneral(title: String, body: String, cancelbutton: String, okbutton: String, completion:@escaping () -> Void) -> UIAlertController
    {
        let alertController = UIAlertController(title: title, message: body, preferredStyle: .alert)

        // Create the actions
        let cancelAction = UIAlertAction(title: cancelbutton, style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        let okAction = UIAlertAction(title: okbutton, style: UIAlertActionStyle.default) {
            UIAlertAction in
            completion()
        }
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        return alertController
    }

    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }



    func SimpleAlertview(title: String, body: String, completion:@escaping () -> Void) -> UIAlertController
    {
        let alertController = UIAlertController(title: title, message: body, preferredStyle: .alert)

        // Create the actions
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) {
            UIAlertAction in
            completion()
        }
        // Add the actions
        alertController.addAction(okAction)
        return alertController
    }

    //MARK: bulleting stuff
    func bullitenItemOfCameraDeniedPermission(openSettingCompletion: @escaping () -> Void, NotNowCompletion: @escaping () -> Void) -> PageBulletinItem {
        let page = PageBulletinItem(title: "Oops!")
        page.image = UIImage(named: "Camera")
        page.descriptionText = "Threadest is an augmented reailty camera app. 😎 To continue, you'll need to allow Camera access in Settings."
        page.actionButtonTitle = "Open Settings"
        page.alternativeButtonTitle = "Not now"
        page.actionHandler = { (item: PageBulletinItem) in
            openSettingCompletion()
        }
        page.alternativeHandler = { (item: PageBulletinItem) in
            NotNowCompletion()
        }
        return page
    }

    func bullitenItemOfCameraAllowPermission(yesCompletion: @escaping () -> Void, noCompletion: @escaping () -> Void) -> PageBulletinItem {
        let page = PageBulletinItem(title: "Enable Camera?")
        page.image = UIImage(named: "Camera")
        page.descriptionText = "Threadest is an augmented reailty camera app. 😎 To continue, you'll need to allow Camera access in Settings."
        page.actionButtonTitle = "Yes"
        page.alternativeButtonTitle = "No"
        page.actionHandler = { (item: PageBulletinItem) in
            yesCompletion()
        }
        page.alternativeHandler = {
            (item: PageBulletinItem) in
            noCompletion()
        }
        return page
    }

    func bullitenItemOfLocationAllowPermission(yesCompletion: @escaping () -> Void, noCompletion: @escaping () -> Void) -> PageBulletinItem {
        let page = PageBulletinItem(title: "Enable Location?")
        page.image = UIImage(named: "Maps")
        page.descriptionText = "😎 To see brands & exclusives nearby, you'll need to allow Location access."
        page.actionButtonTitle = "Yes"
        page.alternativeButtonTitle = "No"
        page.actionHandler = { (item: PageBulletinItem) in
            yesCompletion()
        }
        page.alternativeHandler = {
            (item: PageBulletinItem) in
            noCompletion()
        }
        return page
    }

    func bullitenItemOfWelcomeRetailPopupShop(continueCompletion: @escaping () -> Void) -> PageBulletinItem {
        let page = PageBulletinItem(title: "Popup Shops & Events Near You")
        page.actionButtonTitle = "Continue"
        page.actionHandler = { (item: PageBulletinItem) in
            continueCompletion()
        }
        return page
    }

    func bullitenItemOfLocationDeniedPermission(openSettingsCompletion: @escaping () -> Void, notNowCompletion: @escaping () -> Void) -> PageBulletinItem {
        let page = PageBulletinItem(title: "Oops!")
        page.image = UIImage(named: "Maps")
        page.descriptionText = "😎 To see brands & exclusives nearby, you'll need to allow Location access in settings"
        page.actionButtonTitle = "Open Settings"
        page.alternativeButtonTitle = "Not now"
        page.actionHandler = { (item: PageBulletinItem) in
            openSettingsCompletion()
        }
        page.alternativeHandler = {
            (item: PageBulletinItem) in
            notNowCompletion()
        }
        return page
    }

    func bulletinAlradyAccount(yesCompletion: @escaping () -> Void, noCompletion: @escaping () -> Void) -> PageBulletinItem {
        let page = PageBulletinItem(title: "Threadest")
        page.image = UIImage(named: "ThRequestEarlyLogo")
        page.descriptionText = "Do you already have an account?"
        page.actionButtonTitle = "Yes"
        page.alternativeButtonTitle = "No"
        page.actionHandler = { (item: PageBulletinItem) in
            yesCompletion()
        }
        page.alternativeHandler = {
            (item: PageBulletinItem) in
            noCompletion()
        }
        return page
    }

    func bulletinWelcometoThreadest(maleCompletion: @escaping () -> Void, FemaleCompletion: @escaping () -> Void) -> PageBulletinItem {
        let page = PageBulletinItem(title: "Welcome to Threadest")
        page.image = UIImage(named: "ThRequestEarlyLogo")
        page.descriptionText = "Choose a gender"
        page.actionButtonTitle = "Male"
        page.alternativeButtonTitle = "Female"
        page.actionHandler = { (item: PageBulletinItem) in
            maleCompletion()
        }
        page.alternativeHandler = {
            (item: PageBulletinItem) in
            FemaleCompletion()
        }
        return page
    }

    func bulletinWelcomeSocialToAccessContact(AllowCompletion: @escaping () -> Void, dontAllowCompletion: @escaping () -> Void) -> PageBulletinItem {
        let page = PageBulletinItem(title: "Find Friends")
        page.image = UIImage(named: "Contact")
        page.descriptionText = "See who you already know on Threadest"
        page.actionButtonTitle = "Allow"
        page.alternativeButtonTitle = "Not now"
        page.actionHandler = { (item: PageBulletinItem) in
            AllowCompletion()
        }
        page.alternativeHandler = {
            (item: PageBulletinItem) in
            dontAllowCompletion()
        }
        return page
    }

    func bulletinNotificationThreadest(AllowCompletion: @escaping () -> Void, dontAllowCompletion: @escaping () -> Void) -> PageBulletinItem {
        let page = PageBulletinItem(title: "Get Notified")
        page.image = UIImage(named: "Notification")
        page.descriptionText = "Push Notifications are currently disabled. Would you like to be notified when others engage with you on Threadest?"
        page.actionButtonTitle = "Notify me"
        page.alternativeButtonTitle = "Not now"
        page.actionHandler = { (item: PageBulletinItem) in
            AllowCompletion()
        }
        page.alternativeHandler = {
            (item: PageBulletinItem) in
            dontAllowCompletion()
        }
        return page
    }

    func bulletinNotificationSettingThreadest(AllowCompletion: @escaping () -> Void, dontAllowCompletion: @escaping () -> Void) -> PageBulletinItem {
        let page = PageBulletinItem(title: "Get Notified")
        page.image = UIImage(named: "Notification")
        page.descriptionText = "Push Notifications are currently disabled. Would you like to be notified when others engage with you on Threadest?"
        page.actionButtonTitle = "Open Settings"
        page.alternativeButtonTitle = "Not now"
        page.actionHandler = { (item: PageBulletinItem) in
            AllowCompletion()
        }
        page.alternativeHandler = {
            (item: PageBulletinItem) in
            dontAllowCompletion()
        }
        return page
    }

    func bulletinNotificationVisionBrandSelection(YesCompletion: @escaping () -> Void, NoCompletion: @escaping () -> Void) -> PageBulletinItem {
        let page = PageBulletinItem(title: "Do you know the brand?")
        page.image = nil
        page.descriptionText = ""
        page.actionButtonTitle = "Yes"
        page.alternativeButtonTitle = "No"
        page.actionHandler = { (item: PageBulletinItem) in
            YesCompletion()
        }
        page.alternativeHandler = {
            (item: PageBulletinItem) in
            NoCompletion()
        }
        return page
    }

    func bulletinPlayVideo(WatchCompletion: @escaping () -> Void, SkipCompletion: @escaping () -> Void) -> PageBulletinItem {
        let page = PageBulletinItem(title: "You're now on the Social Shop Feed")
        //page.image = UIImage(named: "play")
        page.descriptionText = "Tap the circular logos on photos to learn more about what's being worn and to even purchase instantly on Threadest."
        page.actionButtonTitle = "Ok"
       // page.alternativeButtonTitle = "Skip"
        page.actionHandler = { (item: PageBulletinItem) in
            WatchCompletion()
        }
        page.alternativeHandler = {
            (item: PageBulletinItem) in
            SkipCompletion()
        }
        return page
    }

    func bulletinWelcomePopup(FirstCompletion: @escaping () -> Void, SecondCompletion: @escaping () -> Void,ThirdCompletion: @escaping () -> Void) -> PageBulletinItem {
        let page = PageBulletinItem(title: "Welcome to Threadest")
        page.descriptionText = "What would you like to do on Threadest?"
        page.actionButtonTitle = "Find pieces I love and can buy"
        page.secondButtonTitle = "Earn $ as a brand influencer"
        page.thirdButtonTitle = "Promote my brand"

        page.actionHandler = { (item: PageBulletinItem) in
            FirstCompletion()
        }
        page.secondHandler = {
            (item: PageBulletinItem) in
            SecondCompletion()
        }

        page.thirdHandler = {
            (item: PageBulletinItem) in
            ThirdCompletion()
        }
        return page
    }

    func bulletinFirstPopup(ContinueCompletion: @escaping () -> Void) -> PageBulletinItem {
        let page = PageBulletinItem(title: "Welcome to Threadest")
        page.descriptionText = "Discover & shop 3000+ emerging and established brands. Buy what others are wearing in this feed."
        page.actionButtonTitle = "Continue"

        page.actionHandler = { (item: PageBulletinItem) in
            ContinueCompletion()
        }
        return page
    }

    func bulletinSecondPopup(ContinueCompletion: @escaping () -> Void) -> PageBulletinItem {
        let page = PageBulletinItem(title: "Earn Money on Threadest")
        page.descriptionText = "Interested in becoming an influencer for brands?! You can on Threadest! Email us now at influencers@threadest.com"
        page.actionButtonTitle = "Continue"

        page.actionHandler = { (item: PageBulletinItem) in
            ContinueCompletion()
        }
        return page
    }

    func bulletinThirdPopup(ContinueCompletion: @escaping () -> Void) -> PageBulletinItem {
        let page = PageBulletinItem(title: "Welcome to Threadest")
        page.descriptionText = "Use Threadest to sell your products, build customer loyalty, and meet influencers. Email us at hello@threadest.com"
        page.actionButtonTitle = "Continue"

        page.actionHandler = { (item: PageBulletinItem) in
            ContinueCompletion()
        }
        return page
    }



    func bulletinWelcomeClosetPopup(OkCompletion: @escaping () -> Void) -> PageBulletinItem {
        let page = PageBulletinItem(title: "Your Closet")
        //page.image = UIImage(named: "play")
        page.descriptionText = "Shop brands and add to your closet."
        page.actionButtonTitle = "Ok"
        // page.alternativeButtonTitle = "Skip"
        page.actionHandler = { (item: PageBulletinItem) in
            OkCompletion()
        }
        return page
    }


    func updateMaleFemaleValue(gender: String) {
        guard let usercurrent = THUserManager.sharedInstance.currentUserdata() else {
            return
        }
        let user:User = User(userId: 0, about: "", username: usercurrent.username, email: "", phoneNumber: "", imageURL: "",thumbnailImageURL:"" ,password: "", height: usercurrent.height, age: usercurrent.age, gender: gender, shirtSize: usercurrent.shirtSize, waistSize: usercurrent.waistSize, shoe_size: usercurrent.shoe_size, neckSize: usercurrent.neckSize, braSize: usercurrent.braSize, hipsSize: usercurrent.hipsSize, cartCount: 0, closetSetupOne: false, closetSetupTwo: false, closetSetupThree: false, closetSetupComplete: false, needsToFollowBrands: false, needsToFollowUsers: false, isFollowing:false, selectedImage:nil, influencerScore: 0, sharePostCode:0, tempAccount: false, invitePromoCode: "", website: "", verifiedAccount: false)
        THUserManager.sharedInstance.performUpdateUser(with: (THUserManager.sharedInstance.currentUserdata()?.userId)!, user: user) { (error) in
        }
    }

    func setAttributedStringWithColor_Font(text strFullText: String, partOfString: String, color: UIColor, font: UIFont) ->  NSMutableAttributedString {

        let range = (strFullText as NSString).range(of: partOfString)

        let attributedString = NSMutableAttributedString(string:strFullText)
        attributedString.addAttribute(NSFontAttributeName, value: font, range: range)
        attributedString.addAttribute(NSForegroundColorAttributeName, value: color , range: range)
        return attributedString
    }

    func timeAgoSinceDate(_ date:Date, numericDates:Bool = false) -> String {
        let calendar = NSCalendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = Date()
        let earliest = now < date ? now : date
        let latest = (earliest == now) ? date : now
        let components = calendar.dateComponents(unitFlags, from: earliest,  to: latest)

        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
    }

    func getTimeAgoStringFrom(dateString:String) -> String
    {
        let timeOfNotificationDate = Formatter.shared.date(string: dateString)

        let timeAgoStr = timeAgoSinceDate(timeOfNotificationDate!, numericDates: true)
        return timeAgoStr
    }

    func getPural(strCategory: String) -> String {
        switch strCategory {
        case "Shirt":
            return "Shirts"
        case "Shoe":
            return "Shoes"
        case "Watch":
            return "Watches"
        case "Bracelet":
            return "Bracelets"
        case "Sweater":
            return "Sweaters"
        case "Coat":
            return "Coats"
        case "Jacket":
            return "Jackets"
        case "Blazer":
            return "Blazers"
        case "Suit":
            return "Suits"
        case "Dress":
            return "Dresses"
        case "Hat":
            return "Hats"
        case "Jewelry":
            return "Jewelries"
        case "Accessory":
            return "Accessories"
        case "Blouse":
            return "Blouses"
        case "Purse":
            return "Purses"
        case "Handbag":
            return "Handbags"
        case "Heel":
            return "Heels"
        case "Skirt":
            return "Skirts"
        case "Bag":
            return "Bags"
        default:
            return strCategory
        }
    }



    func timeCountForCommentSinceDate(_ date:Date, numericDates:Bool = false) -> String {
        let calendar = NSCalendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = Date()
        let earliest = now < date ? now : date
        let latest = (earliest == now) ? date : now
        let components = calendar.dateComponents(unitFlags, from: earliest,  to: latest)

        if (components.year! >= 2) {
            return "\(components.year!) years"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour"
            } else {
                return "An hour"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute"
            } else {
                return "A minute"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds"
        } else {
            return "Just now"
        }
    }

    func getCommentTimeAgoStringFrom(dateString:String) -> String
    {
        let timeOfNotificationDate = Formatter.shared.date(string: dateString)

        let timeAgoStr = timeCountForCommentSinceDate(timeOfNotificationDate!, numericDates: true)
        return timeAgoStr
    }

}
