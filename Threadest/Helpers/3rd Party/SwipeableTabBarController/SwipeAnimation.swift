//
//  MGSwipeAnimation.swift
//  MGSwipeableTabBarController
//
//  Created by Marcos Griselli on 1/31/17.
//  Copyright © 2017 Marcos Griselli. All rights reserved.
//

import UIKit

/// Swipe animation conforming to `UIViewControllerAnimatedTransitioning`
/// Can be replaced by any other class confirming to `UIViewControllerTransitioning`
/// on your `SwipeableTabBarController` subclass.
class SwipeAnimation: NSObject, SwipeTransitioningProtocol {
    var animationType: SwipeAnimationTypeProtocol =  SwipeAnimationType.sideBySide
    
    
    fileprivate var animationDuration: TimeInterval!
    fileprivate var animationStarted = false
    
    // TODO (marcosgriselli): add support for snapshot views.
    var fromLeft = false
    var fromBottom = false
    //var animationType: SwipeAnimationType = SwipeAnimationType.sideBySide
    
    init(animationDuration: TimeInterval = 0.33, animationType: SwipeAnimationTypeProtocol = SwipeAnimationType.sideBySide) {
        super.init()
        self.animationDuration = animationDuration
        self.animationType = animationType
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return (transitionContext?.isAnimated == true) ? animationDuration : 0
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        guard let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from),
            var toViewController = transitionContext.viewController(forKey:
                UITransitionContextViewControllerKey.to)
            else {
                return transitionContext.completeTransition(false)
        }
        
        animationStarted = true
        
        let duration = transitionDuration(using: transitionContext)
        var toView = toViewController.view
        var fromView = fromViewController.view
        fromView?.endEditing(true)

    if (self.animationType.getType() == "push")
        {
           fromLeft = false
            
        }
        
        animationType.addTo(containerView: containerView, fromView: fromView!, toView: toView!)
        animationType.prepare(fromView: fromView, toView: toView, direction: fromLeft)
        
        UIView.animate(withDuration: duration,
                       delay: 0.0,
                       options: [.curveEaseOut],
                       animations: {
                        self.animationType.animation(fromView: fromView, toView: toView, direction: self.fromLeft)
        },
                       completion: {[unowned self] completed in
                        self.animationStarted = false
                        if transitionContext.transitionWasCancelled {
                            toView?.removeFromSuperview()
                        } else {
                            fromView?.removeFromSuperview()
                        }
                        transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
}
