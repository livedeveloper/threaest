//
//  Error.swift
//  Pods-SCNVideoWriter_Example
//
//  Created by Tomoya Hirano on 2017/08/20.
//

import UIKit
@available(iOS 11.0, *)
extension SCNVideoWriter {
  public struct VideoSizeError: Error {}
}

