//
//  SceneLocationView.swift
//  ARKit+CoreLocation
//
//  Created by Andrew Hart on 02/07/2017.
//  Copyright © 2017 Project Dent. All rights reserved.
//

import Foundation
import ARKit
import CoreLocation
import MapKit
import MetalKit

@available(iOS 11.0, *)
public protocol SceneLocationViewDelegate: class {
    func sceneLocationViewDidAddSceneLocationEstimate(sceneLocationView: SceneLocationView, position: SCNVector3, location: CLLocation)
    func sceneLocationViewDidRemoveSceneLocationEstimate(sceneLocationView: SceneLocationView, position: SCNVector3, location: CLLocation)
    
    ///After a node's location is initially set based on current location,
    ///it is later confirmed once the user moves far enough away from it.
    ///This update uses location data collected since the node was placed to give a more accurate location.
    func sceneLocationViewDidConfirmLocationOfNode(sceneLocationView: SceneLocationView, node: LocationNode)
    
    func sceneLocationViewDidSetupSceneNode(sceneLocationView: SceneLocationView, sceneNode: SCNNode)
    
    func sceneLocationViewDidUpdateLocationAndScaleOfLocationNode(sceneLocationView: SceneLocationView, locationNode: LocationNode)
    
    func sessionInterruptionEnded(_ session: ARSession)
    
    func sessionWasInterrupted(_ session: ARSession)
    
    func session(_ session: ARSession, didFailWithError error: Error)
    
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera)
    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval)
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor)
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor)
    
    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor)
    
    func renderer(_ renderer: SCNSceneRenderer, willUpdate node: SCNNode, for anchor: ARAnchor)
}

///Different methods which can be used when determining locations (such as the user's location).
public enum LocationEstimateMethod {
    ///Only uses core location data.
    ///Not suitable for adding nodes using current position, which requires more precision.
    case coreLocationDataOnly
    
    ///Combines knowledge about movement through the AR world with
    ///the most relevant Core Location estimate (based on accuracy and time).
    case mostRelevantEstimate
}

//Should conform to delegate here, add in future commit
@available(iOS 11.0, *)
public class SceneLocationView: ARSCNView, ARSCNViewDelegate {
    ///The limit to the scene, in terms of what data is considered reasonably accurate.
    ///Measured in meters.
    private static let sceneLimit = 10.0
    
    public weak var locationDelegate: SceneLocationViewDelegate?
    
    public var configuration: ARWorldTrackingConfiguration?
    
    ///The method to use for determining locations.
    ///Not advisable to change this as the scene is ongoing.
    public var locationEstimateMethod: LocationEstimateMethod = .mostRelevantEstimate
    
    let locationManager = LocationManager()
    
    private(set) var locationNodes = [LocationNode]()
    
    private(set) var locationBrandNodes = [LocationNode]()
    
    private var sceneLocationEstimates = [SceneLocationEstimate]()
    
    public private(set) var sceneNode: SCNNode? {
        didSet {
            if sceneNode != nil {
               /* for locationNode in locationNodes {
                    sceneNode!.addChildNode(locationNode)
                }*/
                locationDelegate?.sceneLocationViewDidSetupSceneNode(sceneLocationView: self, sceneNode: sceneNode!)
            }
        }
    }
    
    public private(set) var sceneBrandNode: SCNNode? {
        didSet {
            if sceneBrandNode != nil {
                for locationNode in locationBrandNodes {
                    sceneBrandNode!.addChildNode(locationNode)
                }
                locationDelegate?.sceneLocationViewDidSetupSceneNode(sceneLocationView: self, sceneNode: sceneBrandNode!)
            }
        }
    }
    
    public private(set) var focusSquare: SCNNode? {
        didSet {
            if focusSquare != nil {
                
            }
        }
    }
    
    private var updateEstimatesTimer: Timer?
    
    private var didFetchInitialLocation = true
    
    ///Whether debugging feature points should be displayed.
    ///Defaults to false
    var showFeaturePoints = true
    
    ///Only to be overrided if you plan on manually setting True North.
    ///When true, sets up the scene to face what the device considers to be True North.
    ///This can be inaccurate, hence the option to override it.
    ///The functions for altering True North can be used irrespective of this value,
    ///but if the scene is oriented to true north, it will update without warning,
    ///thus affecting your alterations.
    ///The initial value of this property is respected.
    public var orientToTrueNorth = false
    
    //MARK: Setup
    public convenience init() {
        self.init(frame: CGRect.zero, options: nil)
    }
    
    public override init(frame: CGRect, options: [String : Any]? = nil) {
        super.init(frame: frame, options: options)
        
        locationManager.delegate = self
        
        delegate = self
        
        // Show statistics such as fps and timing information
        showsStatistics = false
        
        if showFeaturePoints {
            debugOptions = [ARSCNDebugOptions.showFeaturePoints]
        }
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
    }
    
    public func run() {
        // Create a session configuration
        configuration = ARWorldTrackingConfiguration()
        configuration?.planeDetection = .horizontal
        if orientToTrueNorth {
            configuration?.worldAlignment = .gravityAndHeading
        } else {
            configuration?.worldAlignment = .gravity
        }
        
        // Run the view's session
        session = ARSession()
        session.run(configuration!)
        
        updateEstimatesTimer?.invalidate()
        updateEstimatesTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(SceneLocationView.updateLocationData), userInfo: nil, repeats: true)
    }
    
    public func reset() {
        // Create a session configuration
        session = ARSession()
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        configuration.worldAlignment = .gravity
        session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
        updateEstimatesTimer?.invalidate()
        updateEstimatesTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(SceneLocationView.updateLocationData), userInfo: nil, repeats: true)
    }
    
    public func pause() {
        session.pause()
        updateEstimatesTimer?.invalidate()
        updateEstimatesTimer = nil
    }
    
    @objc private func updateLocationData() {
        removeOldLocationEstimates()
        confirmLocationOfDistantLocationNodes()
        updatePositionAndScaleOfLocationNodes()
    }
    
    //MARK: True North
    ///iOS can be inaccurate when setting true north
    ///The scene is oriented to true north, and will update its heading when it gets a more accurate reading
    ///You can disable this through setting the
    ///These functions provide manual overriding of the scene heading,
    /// if you have a more precise idea of where True North is
    ///The goal is for the True North orientation problems to be resolved
    ///At which point these functions would no longer be useful
    
    ///Moves the scene heading clockwise by 1 degree
    ///Intended for correctional purposes
    public func moveSceneHeadingClockwise() {
        //sceneNode?.eulerAngles.y -= Float(1).degreesToRadians
        sceneBrandNode?.eulerAngles.y -= Float(1).degreesToRadians
    }
    
    ///Moves the scene heading anti-clockwise by 1 degree
    ///Intended for correctional purposes
    public func moveSceneHeadingAntiClockwise() {
        //sceneNode?.eulerAngles.y += Float(1).degreesToRadians
        sceneBrandNode?.eulerAngles.y += Float(1).degreesToRadians
    }
    
    ///Resets the scene heading to 0
    func resetSceneHeading() {
        sceneNode?.eulerAngles.y = 0
        sceneBrandNode?.eulerAngles.y = 0
    }
    
    //MARK: Scene location estimates
    
    public func currentScenePosition() -> SCNVector3? {
        guard let pointOfView = pointOfView else {
            return nil
        }
        
        return scene.rootNode.convertPosition(pointOfView.position, to: sceneNode)
    }
    
    public func currentEulerAngles() -> SCNVector3? {
        return pointOfView?.eulerAngles
    }
    
    ///Adds a scene location estimate based on current time, camera position and location from location manager
    fileprivate func addSceneLocationEstimate(location: CLLocation) {
        if let position = currentScenePosition() {
            let sceneLocationEstimate = SceneLocationEstimate(location: location, position: position)
            self.sceneLocationEstimates.append(sceneLocationEstimate)
            
            locationDelegate?.sceneLocationViewDidAddSceneLocationEstimate(sceneLocationView: self, position: position, location: location)
        }
    }
    
    
    
    private func removeOldLocationEstimates() {
        if let currentScenePosition = currentScenePosition() {
            self.removeOldLocationEstimates(currentScenePosition: currentScenePosition)
        }
    }
    
    private func removeOldLocationEstimates(currentScenePosition: SCNVector3) {
        let currentPoint = CGPoint.pointWithVector(vector: currentScenePosition)
        sceneLocationEstimates = sceneLocationEstimates.filter({
            let point = CGPoint.pointWithVector(vector: $0.position)
            let radiusContainsPoint = currentPoint.radiusContainsPoint(radius: CGFloat(SceneLocationView.sceneLimit), point: point)
            if !radiusContainsPoint {
                locationDelegate?.sceneLocationViewDidRemoveSceneLocationEstimate(sceneLocationView: self, position: $0.position, location: $0.location)
            }
            return radiusContainsPoint
        })
    }
    
    ///The best estimation of location that has been taken
    ///This takes into account horizontal accuracy, and the time at which the estimation was taken
    ///favouring the most accurate, and then the most recent result.
    ///This doesn't indicate where the user currently is.
    func bestLocationEstimate() -> SceneLocationEstimate? {
        let sortedLocationEstimates = sceneLocationEstimates.sorted(by: {
            if $0.location.horizontalAccuracy == $1.location.horizontalAccuracy {
                return $0.location.timestamp > $1.location.timestamp
            }
            
            return $0.location.horizontalAccuracy < $1.location.horizontalAccuracy
        })
        
        return sortedLocationEstimates.first
    }
    
    public func currentLocation() -> CLLocation? {
        if locationEstimateMethod ==   .coreLocationDataOnly {
            return locationManager.currentLocation
        }
        
        guard let bestEstimate = self.bestLocationEstimate(),
            let position = currentScenePosition() else {
                return nil
        }
        return bestEstimate.translatedLocation(to: position)
    }
    
    
    public func addNode(locationNode:LocationNode) {
        if self.locationEstimateMethod == .coreLocationDataOnly {
            locationNode.locationConfirmed = true
        } else {
            locationNode.locationConfirmed = false
        }
        
        locationNode.position = SCNVector3Make(locationNode.x, locationNode.y,locationNode.z)
        
        if self.sceneNode?.childNodes.count == 0 {
            if locationNode.isFirstLoad {
                sceneNode?.eulerAngles.x = 0//(self.pointOfView?.eulerAngles.x)!
                sceneNode?.eulerAngles.y = (self.pointOfView?.eulerAngles.y)!
                sceneNode?.eulerAngles.z = 0
                sceneNode?.position.x = locationNode.isSingle ?(self.pointOfView?.position.x)!+0.8 : (self.pointOfView?.position.x)!
                sceneNode?.position.y = 0
                sceneNode?.position.z = (self.pointOfView?.position.z)!
            } else {
                sceneNode?.position.x = locationNode.isSingle ?(self.pointOfView?.position.x)!+0.8 : (self.pointOfView?.position.x)!
            }
        }
        if locationNode.isShowTapText {
            if userdefault.object(forKey: userDefaultKey.tapMeShowFirstDefault.rawValue) == nil {
                let txtgeomentry = SCNText(string: "TAP ME", extrusionDepth: 0)
                txtgeomentry.font = UIFont.boldSystemFont(ofSize: 10)
                txtgeomentry.alignmentMode = kCAAlignmentCenter
                txtgeomentry.firstMaterial?.diffuse.contents = UIColor.hex("178dcd", alpha: 1)
                txtgeomentry.firstMaterial?.isDoubleSided = true
                let textNode = SCNNode(geometry: txtgeomentry)
                textNode.scale = SCNVector3Make(0.015, 0.015, 0.015)
                textNode.position = SCNVector3Make(locationNode.x, locationNode.y,locationNode.z)
                center(node: textNode)
                textNode.position.y = locationNode.y + 0.73
                textNode.name = "TAP ME"
                self.sceneNode?.addChildNode(textNode)
            }
            
            if locationNode.isSingle {
                let arrowNode = self.arrow()
                arrowNode.name = "arrowNode"
                arrowNode.position = SCNVector3Make(locationNode.x, locationNode.y,locationNode.z)
                arrowNode.position.y = locationNode.y - 1.3
                arrowNode.eulerAngles.x = .pi/2
                arrowNode.eulerAngles.y = 0
                arrowNode.eulerAngles.z = -.pi/2
                self.sceneNode?.addChildNode(arrowNode)
            }
        }
        DispatchQueue.global(qos: .background).async {
            self.prepare([locationNode]) { (invite) in
                self.sceneNode?.addChildNode(locationNode)
            }
        }
        
        
    }
    
  /*  public func addMultipleNode(locationNodes:[LocationNode]) {
        self.prepare(locationNodes) { (com) in
            for locationNode in locationNodes {
                if self.locationEstimateMethod == .coreLocationDataOnly {
                    locationNode.locationConfirmed = true
                } else {
                    locationNode.locationConfirmed = false
                }
                
                locationNode.position = SCNVector3Make(locationNode.x, locationNode.y,locationNode.z)
                if self.sceneNode?.childNodes.count == 0 {
                    if locationNode.isFirstLoad {
                        sceneNode?.eulerAngles.x = 0//(self.pointOfView?.eulerAngles.x)!
                        sceneNode?.eulerAngles.y = (self.pointOfView?.eulerAngles.y)!
                        sceneNode?.eulerAngles.z = 0
                        sceneNode?.position.x = (self.pointOfView?.position.x)!
                        sceneNode?.position.y = 0
                        sceneNode?.position.z = (self.pointOfView?.position.z)!
                    }
                }
                if locationNode.isShowTapText {
                    if userdefault.object(forKey: userDefaultKey.tapMeShowFirstDefault.rawValue) == nil {
                        let txtgeomentry = SCNText(string: "TAP ME", extrusionDepth: 0)
                        txtgeomentry.font = UIFont.boldSystemFont(ofSize: 10)
                        txtgeomentry.alignmentMode = kCAAlignmentCenter
                        txtgeomentry.firstMaterial?.diffuse.contents = UIColor.hex("178dcd", alpha: 1)
                        txtgeomentry.firstMaterial?.isDoubleSided = true
                        let textNode = SCNNode(geometry: txtgeomentry)
                        textNode.scale = SCNVector3Make(0.015, 0.015, 0.015)
                        textNode.position = SCNVector3Make(locationNode.x, locationNode.y,locationNode.z)
                        center(node: textNode)
                        textNode.position.y = locationNode.y + 0.73
                        textNode.name = "TAP ME"
                        self.sceneNode?.addChildNode(textNode)
                    }
                    
                    if locationNode.isSingle {
                        let arrowNode = self.arrow()
                        arrowNode.name = "arrowNode"
                        arrowNode.position = SCNVector3Make(locationNode.x, locationNode.y,locationNode.z)
                        arrowNode.position.y = locationNode.y - 1.3
                        arrowNode.eulerAngles.x = .pi/2
                        arrowNode.eulerAngles.y = 0
                        arrowNode.eulerAngles.z = -.pi/2
                        self.sceneNode?.addChildNode(arrowNode)
                    }
                }
                self.prepare([locationNode]) { (completion) in
                    self.sceneNode?.addChildNode(locationNode)
                }
            }
        }
    }*/
    
    func arrow() -> SCNNode {
        let vertcount = 48;
        let verts: [Float] = [ -1.4923, 1.1824, 2.5000, -6.4923, 0.000, 0.000, -1.4923, -1.1824, 2.5000, 4.6077, -0.5812, 1.6800, 4.6077, -0.5812, -1.6800, 4.6077, 0.5812, -1.6800, 4.6077, 0.5812, 1.6800, -1.4923, -1.1824, -2.5000, -1.4923, 1.1824, -2.5000, -1.4923, 0.4974, -0.9969, -1.4923, 0.4974, 0.9969, -1.4923, -0.4974, 0.9969, -1.4923, -0.4974, -0.9969 ];
        
        let facecount = 13;
        let faces: [CInt] = [  3, 4, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 0, 1, 2, 3, 4, 5, 6, 7, 1, 8, 8, 1, 0, 2, 1, 7, 9, 8, 0, 10, 10, 0, 2, 11, 11, 2, 7, 12, 12, 7, 8, 9, 9, 5, 4, 12, 10, 6, 5, 9, 11, 3, 6, 10, 12, 4, 3, 11 ];
        
        let vertsData  = NSData(
            bytes: verts,
            length: MemoryLayout<Float>.size * vertcount
        )
        
        let vertexSource = SCNGeometrySource(data: vertsData as Data,
                                             semantic: .vertex,
                                             vectorCount: vertcount,
                                             usesFloatComponents: true,
                                             componentsPerVector: 3,
                                             bytesPerComponent: MemoryLayout<Float>.size,
                                             dataOffset: 0,
                                             dataStride: MemoryLayout<Float>.size * 3)
        
        let polyIndexCount = 61;
        let indexPolyData  = NSData( bytes: faces, length: MemoryLayout<CInt>.size * polyIndexCount )
        
        let element1 = SCNGeometryElement(data: indexPolyData as Data,
                                          primitiveType: .polygon,
                                          primitiveCount: facecount,
                                          bytesPerIndex: MemoryLayout<CInt>.size)
        
        let geometry1 = SCNGeometry(sources: [vertexSource], elements: [element1])
        
        let material1 = geometry1.firstMaterial!
        
        material1.diffuse.contents = UIColor.hex("178dcd", alpha: 1)
        material1.lightingModel = .lambert
        material1.transparency = 1.00
        material1.transparencyMode = .dualLayer
        material1.fresnelExponent = 1.00
        material1.reflective.contents = UIColor(white:0.00, alpha:1.0)
        material1.specular.contents = UIColor(white:0.00, alpha:1.0)
        material1.shininess = 1.00
        let aNode = SCNNode()
        aNode.geometry = geometry1
        aNode.scale = SCNVector3(0.1, 0.1, 0.1)
        return aNode
    }
 
    
    func center(node: SCNNode) {
        let (min, max) = node.boundingBox
        
        let dx = min.x + 0.5 * (max.x - min.x)
        let dy = min.y + 0.5 * (max.y - min.y)
        let dz = min.z + 0.5 * (max.z - min.z)
        node.pivot = SCNMatrix4MakeTranslation(dx, dy, dz)
    }
    
    
    
    ///location not being nil, and locationConfirmed being true are required
    ///Upon being added, a node's position will be modified and should not be changed externally.
    ///location will not be modified, but taken as accurate.
    public func addLocationNodeWithConfirmedLocation(locationNode: LocationNode) {
        if locationNode.location == nil || locationNode.locationConfirmed == false {
            return
        }

        updatePositionAndScaleOfLocationNode(locationNode: locationNode, initialSetup: true, animated: false)
        locationBrandNodes.append(locationNode)
        sceneBrandNode?.addChildNode(locationNode)
    }
    
    public func removeTapMeNode() {
        if let node = self.sceneNode?.childNode(withName: "TAP ME", recursively: true) {
            if userdefault.object(forKey: userDefaultKey.tapMeShowFirstDefault.rawValue) == nil {
                userdefault.set(true, forKey: userDefaultKey.tapMeShowFirstDefault.rawValue)
                userdefault.synchronize()
            }
            node.geometry?.firstMaterial?.diffuse.contents = nil
            for child in node.childNodes {
                child.geometry?.firstMaterial?.diffuse.contents = nil
                child.removeFromParentNode()
            }
            node.removeFromParentNode()
        }
    }
    
    public func removeLocationNode(locationNode: LocationNode) {
         DispatchQueue.main.async {
            locationNode.geometry?.firstMaterial?.diffuse.contents = nil
            locationNode.geometry = nil
            for child in locationNode.childNodes {
                child.geometry?.firstMaterial?.diffuse.contents = nil
                child.removeFromParentNode()
            }
            locationNode.removeFromParentNode()
            if let node = self.sceneNode?.childNode(withName: "TAP ME", recursively: true) {
                node.geometry?.firstMaterial?.diffuse.contents = nil
                for child in node.childNodes {
                    child.geometry?.firstMaterial?.diffuse.contents = nil
                    child.removeFromParentNode()
                }
                node.removeFromParentNode()
            }
            
            if let node = self.sceneNode?.childNode(withName: "arrowNode", recursively: true) {
                node.geometry?.firstMaterial?.diffuse.contents = nil
                for child in node.childNodes {
                    child.geometry?.firstMaterial?.diffuse.contents = nil
                    child.removeFromParentNode()
                }
                node.removeFromParentNode()
            }
          /*  if let node = self.sceneNode?.childNode(withName: locationNode.name!, recursively: true) {
                 node.geometry?.firstMaterial?.diffuse.contents = nil
                for child in node.childNodes {
                    child.geometry?.firstMaterial?.diffuse.contents = nil
                    child.removeFromParentNode()
                }
                node.removeFromParentNode()
            }
            
            for nd in (self.sceneNode?.childNodes)! {
                nd.geometry?.firstMaterial?.diffuse.contents = nil
                for child in nd.childNodes {
                    child.geometry?.firstMaterial?.diffuse.contents = nil
                    child.removeFromParentNode()
                }
                nd.removeFromParentNode()
            }
            

            if let index = self.locationNodes.index(of: locationNode) {
                self.locationNodes[index].geometry = nil
                self.locationNodes.remove(at: index)
            }*/
            
           /* for node in (self.sceneNode?.childNodes)! {
                node.geometry = nil
                for child1 in node.childNodes {
                    child1.geometry = nil
                    child1.removeFromParentNode()
                }
                node.removeFromParentNode()
            }*/
            
           /* if let index = self.locationNodes.index(of: locationNode) {
                self.locationNodes[index].geometry = nil
                self.locationNodes.remove(at: index)
            }
            locationNode.removeFromParentNode()*/
        }
    }
    
    public func removeLocationBrandNode(locationNode: LocationNode) {
        DispatchQueue.main.async {
            locationNode.geometry = nil
            if let index = self.locationBrandNodes.index(of: locationNode) {
                if let i = self.locationNodes.index(of: locationNode) {
                    self.locationNodes[i].geometry = nil
                }
                self.locationBrandNodes.remove(at: index)
            }
            locationNode.removeFromParentNode()
        }
    }
    
    private func confirmLocationOfDistantLocationNodes() {
        guard let currentPosition = currentScenePosition() else {
            return
        }
        
        for locationNode in locationNodes {
            if !locationNode.locationConfirmed {
                let currentPoint = CGPoint.pointWithVector(vector: currentPosition)
                let locationNodePoint = CGPoint.pointWithVector(vector: locationNode.position)
                
                if !currentPoint.radiusContainsPoint(radius: CGFloat(SceneLocationView.sceneLimit), point: locationNodePoint) {
                    confirmLocationOfLocationNode(locationNode)
                }
            }
        }
        
        for locationBrandNode in locationBrandNodes {
            if !locationBrandNode.locationConfirmed {
                let currentPoint = CGPoint.pointWithVector(vector: currentPosition)
                let locationNodePoint = CGPoint.pointWithVector(vector: locationBrandNode.position)
                
                if !currentPoint.radiusContainsPoint(radius: CGFloat(SceneLocationView.sceneLimit), point: locationNodePoint) {
                    confirmLocationOfLocationNode(locationBrandNode)
                }
            }
        }
    }
    
    ///Gives the best estimate of the location of a node
    func locationOfLocationNode(_ locationNode: LocationNode) -> CLLocation {
        if locationNode.locationConfirmed || locationEstimateMethod == .coreLocationDataOnly {
            return locationNode.location!
        }
        
        if let bestLocationEstimate = bestLocationEstimate(),
            locationNode.location == nil ||
                bestLocationEstimate.location.horizontalAccuracy < locationNode.location!.horizontalAccuracy {
            let translatedLocation = bestLocationEstimate.translatedLocation(to: locationNode.position)
            
            return translatedLocation
        } else {
            return locationNode.location!
        }
    }
    
    private func confirmLocationOfLocationNode(_ locationNode: LocationNode) {
        locationNode.location = locationOfLocationNode(locationNode)
        
        locationNode.locationConfirmed = true
                locationDelegate?.sceneLocationViewDidConfirmLocationOfNode(sceneLocationView: self, node: locationNode)
    }
    
    func updatePositionAndScaleOfLocationNodes() {
        for locationNode in locationNodes {
            if locationNode.continuallyUpdatePositionAndScale {
                updatePositionAndScaleOfLocationNode(locationNode: locationNode, animated: true)
            }
        }
        
        for locationBrandNode in locationBrandNodes {
            if locationBrandNode.continuallyUpdatePositionAndScale {
                updatePositionAndScaleOfLocationNode(locationNode: locationBrandNode, animated: true)
            }
        }
    }
    
    public func updatePositionAndScaleOfLocationNode(locationNode: LocationNode, initialSetup: Bool = false, animated: Bool = false, duration: TimeInterval = 0.1) {
        guard let currentPosition = currentScenePosition(),
            let currentLocation = currentLocation() else {
            return
        }
        
        SCNTransaction.begin()
        
        if animated {
            SCNTransaction.animationDuration = duration
        } else {
            SCNTransaction.animationDuration = 0
        }
        
        let locationNodeLocation = locationOfLocationNode(locationNode)
        
        //Position is set to a position coordinated via the current position
        let locationTranslation = currentLocation.translation(toLocation: locationNodeLocation)
        
        
        let adjustedDistance: CLLocationDistance
        
        let distance = locationNodeLocation.distance(from: currentLocation)
        
        if locationNode.locationConfirmed &&
            (distance > 100 || locationNode.continuallyAdjustNodePositionWhenWithinRange || initialSetup) {
            if distance > 100 {
                //If the item is too far away, bring it closer and scale it down
                let scale = 100 / Float(distance)
                
                adjustedDistance = distance * Double(scale)
                
                let adjustedTranslation = SCNVector3(
                    x: Float(locationTranslation.longitudeTranslation) * scale,
                    y: Float(locationTranslation.altitudeTranslation) * scale,
                    z: Float(locationTranslation.latitudeTranslation) * scale)
                
                let position = SCNVector3(
                    x: currentPosition.x + adjustedTranslation.x,
                    y: currentPosition.y + adjustedTranslation.y,
                    z: currentPosition.z - adjustedTranslation.z)
                
                locationNode.position = position
                
                locationNode.scale = SCNVector3(x: scale, y: scale, z: scale)
            } else {
                adjustedDistance = distance
                let position = SCNVector3(
                    x: currentPosition.x + Float(locationTranslation.longitudeTranslation),
                    y: currentPosition.y + Float(locationTranslation.altitudeTranslation),
                    z: currentPosition.z - Float(locationTranslation.latitudeTranslation))
                
                locationNode.position = position
                locationNode.scale = SCNVector3(x: 1, y: 1, z: 1)
            }
        } else {
            //Calculates distance based on the distance within the scene, as the location isn't yet confirmed
            adjustedDistance = Double(currentPosition.distance(to: locationNode.position))

            locationNode.scale = SCNVector3(x: 1, y: 1, z: 1)
        }
        
        if let annotationNode = locationNode as? LocationAnnotationNode {
            //The scale of a node with a billboard constraint applied is ignored
            //The annotation subnode itself, as a subnode, has the scale applied to it
            let appliedScale = locationNode.scale
            locationNode.scale = SCNVector3(x: 1, y: 1, z: 1)
            
            var scale: Float
            
            if annotationNode.scaleRelativeToDistance {
                scale = appliedScale.y
                annotationNode.annotationNode.scale = appliedScale
            } else {
                //Scale it to be an appropriate size so that it can be seen
                scale = Float(adjustedDistance) * 0.181
                
                if distance > 3000 {
                    scale = scale * 0.75
                }
                
                annotationNode.annotationNode.scale = SCNVector3(x: scale, y: scale, z: scale)
            }
            
            annotationNode.pivot = SCNMatrix4MakeTranslation(0, -1.1 * scale, 0)
        }
        
        SCNTransaction.commit()
        locationDelegate?.sceneLocationViewDidUpdateLocationAndScaleOfLocationNode(sceneLocationView: self, locationNode: locationNode)
    }
    
    //MARK: ARSCNViewDelegate
    
    public func renderer(_ renderer: SCNSceneRenderer, didRenderScene scene: SCNScene, atTime time: TimeInterval) {
        if focusSquare == nil {
            focusSquare = SCNNode()
            scene.rootNode.addChildNode(focusSquare!)
        }
        if sceneNode == nil {
            sceneNode = SCNNode()
            scene.rootNode.addChildNode(sceneNode!)
        }
        
        if sceneBrandNode == nil {
            sceneBrandNode = SCNNode()
            scene.rootNode.addChildNode(sceneBrandNode!)
        }
        
        if !didFetchInitialLocation {
            //Current frame and current location are required for this to be successful
            if session.currentFrame != nil,
                let currentLocation = self.locationManager.currentLocation {
                didFetchInitialLocation = true
                
                self.addSceneLocationEstimate(location: currentLocation)
            }
        }
    }
    
    public func renderer(_ renderer: SCNSceneRenderer, willUpdate node: SCNNode, for anchor: ARAnchor) {
        locationDelegate?.renderer(renderer, willUpdate: node, for: anchor)
    }
    
    public func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        locationDelegate?.renderer(renderer, updateAtTime: time)
    }
    
    public func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        locationDelegate?.renderer(renderer, didAdd: node, for: anchor)
    }
    
    public func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        locationDelegate?.renderer(renderer, didUpdate: node, for: anchor)
    }
    
    public func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        locationDelegate?.renderer(renderer, didRemove: node, for: anchor)
    }
    
    public func sessionWasInterrupted(_ session: ARSession) {
        print("session was interrupted")
        locationDelegate?.sessionWasInterrupted(session)
    }
    
    public func sessionInterruptionEnded(_ session: ARSession) {
        print("session interruption ended")
        locationDelegate?.sessionInterruptionEnded(session)
    }
    
    public func session(_ session: ARSession, didFailWithError error: Error) {
        print("session did fail with error: \(error)")
        locationDelegate?.session(session, didFailWithError: error)
    }
    
    public func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        locationDelegate?.session(session, cameraDidChangeTrackingState: camera)
    }
}

//MARK: LocationManager
@available(iOS 11.0, *)
extension SceneLocationView: LocationManagerDelegate {
    func locationManagerUpdate(coordinate: CLLocationCoordinate2D) {
        
    }
    
    func locationManagerDidUpdateLocation(_ locationManager: LocationManager, location: CLLocation) {
        addSceneLocationEstimate(location: location)
    }
    
    func locationManagerDidUpdateHeading(_ locationManager: LocationManager, heading: CLLocationDirection, accuracy: CLLocationAccuracy) {
    }
}

extension UIBezierPath {
    static func arrow(from start: CGPoint, to end: CGPoint, tailWidth: CGFloat, headWidth: CGFloat, headLength: CGFloat) -> UIBezierPath {
        let length = hypot(end.x - start.x, end.y - start.y)
        let tailLength = length - headLength
        
        func p(_ x: CGFloat, _ y: CGFloat) -> CGPoint { return CGPoint(x: x, y: y) }
        let points: [CGPoint] = [
            p(0, tailWidth / 2),
            p(tailLength, tailWidth / 2),
            p(tailLength, headWidth / 2),
            p(length, 0),
            p(tailLength, -headWidth / 2),
            p(tailLength, -tailWidth / 2),
            p(0, -tailWidth / 2)
        ]
        
        let cosine = (end.x - start.x) / length
        let sine = (end.y - start.y) / length
        let transform = CGAffineTransform(a: cosine, b: sine, c: -sine, d: cosine, tx: start.x, ty: start.y)
        
        let path = CGMutablePath()
        path.addLines(between: points, transform: transform)
        path.closeSubpath()
        
        return self.init(cgPath: path)
    }
}
