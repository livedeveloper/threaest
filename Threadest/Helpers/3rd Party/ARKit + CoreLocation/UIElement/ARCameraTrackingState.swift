import Foundation
import ARKit

@available(iOS 11.0, *)
extension ARCamera.TrackingState {
	var presentationString: String {
		switch self {
        case .notAvailable:
            return "FIND A SURFACE WITH YOUR CAMERA. 📲"
        case .normal:
            return "GREAT! LOOK AROUND THROUGH YOUR CAMERA. 📲"
        case .limited(let reason):
            switch reason {
            case .excessiveMotion:
                return "SLOW DOWN YOUR MOVEMENTS."
            case .insufficientFeatures:
                return "MOVE TO AN AREA WITH MORE LIGHT. 🌞"
            case .initializing:
                return "FIND A SURFACE WITH YOUR CAMERA. 📲"
//            case .relocalizing:
//                return "STABILIZING - PAN YOUR PHONE AROUND."
            }
        }
	}
}
