//
//  SceneAnnotation.swift
//  ARKit+CoreLocation
//
//  Created by Andrew Hart on 02/07/2017.
//  Copyright © 2017 Project Dent. All rights reserved.
//

import Foundation
import SceneKit
import CoreLocation
import SDWebImage
///A location node can be added to a scene using a coordinate.
///Its scale and position should not be adjusted, as these are used for scene layout purposes
///To adjust the scale and position of items within a node, you can add them to a child node and adjust them there
@available(iOS 11.0, *)
open class LocationNode: SCNNode {
    static let ROOT_NAME = "Virtual object root node"
    ///Location can be changed and confirmed later by SceneLocationView.
    public var location: CLLocation!
    

    var viewController: THARClosetViewController?
    
    ///Whether the location of the node has been confirmed.
    ///This is automatically set to true when you create a node using a location.
    ///Otherwise, this is false, and becomes true once the user moves 100m away from the node,
    ///except when the locationEstimateMethod is set to use Core Location data only,
    ///as then it becomes true immediately.
    public var locationConfirmed = false
    
    ///Whether a node's position should be adjusted on an ongoing basis
    ///based on its' given location.
    ///This only occurs when a node's location is within 100m of the user.
    ///Adjustment doesn't apply to nodes without a confirmed location.
    ///When this is set to false, the result is a smoother appearance.
    ///When this is set to true, this means a node may appear to jump around
    ///as the user's location estimates update,
    ///but the position is generally more accurate.
    ///Defaults to true.
    public var continuallyAdjustNodePositionWhenWithinRange = false
    
    ///Whether a node's position and scale should be updated automatically on a continual basis.
    ///This should only be set to false if you plan to manually update position and scale
    ///at regular intervals. You can do this with `SceneLocationView`'s `updatePositionOfLocationNode`.
    public var continuallyUpdatePositionAndScale = true
    
    public var x = Float(0)
    
    public var y = Float(0)
    public var z = Float(0)
    
    public var isShowTapText = false
    public var isSingle = false
    
    public var isFirstLoad = false
    
    public init(location: CLLocation?) {
        self.location = location
        self.locationConfirmed = location != nil
        super.init()
    }
    
    
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func translateBasedOnScreenPos(_ pos: CGPoint, instantly: Bool, infinitePlane: Bool) {
        guard let controller = viewController else {
            return
        }
        let result = controller.worldPositionFromScreenPosition(pos, objectPos: self.position)
        controller.moveVirtualObjectToPosition(result.position, instantly, result.hitAPlane)
    }
    
    static func isNodePartOfVirtualObject(_ node: SCNNode) -> Bool {
        /*if node.name == actualNodeName {
            return true
        }
        
        if node.parent != nil {
            return isNodePartOfVirtualObject(node.parent!, actualNodeName: actualNodeName)
        }*/
        
        return true
    }
}

@available(iOS 11.0, *)
open class LocationAnnotationNode: LocationNode {
    ///An image to use for the annotation
    ///When viewed from a distance, the annotation will be seen at the size provided
    ///e.g. if the size is 100x100px, the annotation will take up approx 100x100 points on screen.
    public let image: UIImage
    
    ///Subnodes and adjustments should be applied to this subnode
    ///Required to allow scaling at the same time as having a 2D 'billboard' appearance
    public let annotationNode: SCNNode
    
    ///Whether the node should be scaled relative to its distance from the camera
    ///Default value (false) scales it to visually appear at the same size no matter the distance
    ///Setting to true causes annotation nodes to scale like a regular node
    ///Scaling relative to distance may be useful with local navigation-based uses
    ///For landmarks in the distance, the default is correct
    public var scaleRelativeToDistance = false
    
    private func load(fileURL: URL) -> UIImage? {
      //  let fileURL = documentsUrl.appendingPathComponent(fileName)
        do {
            let imageData = try Data(contentsOf: fileURL)
            return UIImage(data: imageData)
        } catch {
            print("Error loading image : \(error)")
        }
        return nil
    }
    
    public init(location: CLLocation?, node: SCNNode) {
        self.image = UIImage()
        annotationNode = SCNNode()
        super.init(location: location)
        addChildNode(node)
    }
    
    public init(location: CLLocation?, image: UIImage) {
        self.image = image
        
        let plane = SCNPlane(width: image.size.width / 100, height: image.size.height / 100)
        plane.firstMaterial!.diffuse.contents = image
        plane.firstMaterial!.lightingModel = .constant
        
        annotationNode = SCNNode()

        annotationNode.geometry = plane
        
        super.init(location: location)
        
        let billboardConstraint = SCNBillboardConstraint()
        billboardConstraint.freeAxes = SCNBillboardAxis.Y
        constraints = [billboardConstraint]
        
        addChildNode(annotationNode)
    }
    
    public init(location: CLLocation?, nameURL: String, distance: String) {
        self.image = UIImage()
        let planeGeoMetryP1:SCNPlane = SCNPlane(width: (300/100)*0.2, height: (400/100)*0.2)
        planeGeoMetryP1.firstMaterial?.fillMode = .fill
        planeGeoMetryP1.firstMaterial!.lightingModel = .constant
        
        annotationNode = SCNNode()
        annotationNode.name = "brandLocation"
        let imgView = UIImageView(frame: CGRect(x: 0, y: 100, width: 300, height: 300))
        imgView.backgroundColor = UIColor.white
        imgView.layer.borderColor = UIColor.hex("fcdf4e", alpha: 1).cgColor
        imgView.layer.borderWidth = 10
        imgView.layer.cornerRadius = 150
        imgView.layer.masksToBounds = true
        imgView.contentMode = .scaleAspectFit
        imgView.sd_setImage(with: URL(string: nameURL), placeholderImage: nil, options: .retryFailed, progress: { (block1, block2, imageurl) in
            
        }) { (image, error, imageCacheType, imageUrl) in
            if image != nil {
                let view = UIView(frame: CGRect(x: 0, y: 0, width: 300, height: 400))
                view.backgroundColor = UIColor.clear
                let distanceLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 300, height: 90))
                distanceLabel.backgroundColor = UIColor.blueTabbar
                distanceLabel.text = distance
                distanceLabel.textColor = UIColor.white
                distanceLabel.font = UIFont.AppFontBold(50)
                distanceLabel.textAlignment = .center
                distanceLabel.layer.cornerRadius = 45
                distanceLabel.layer.masksToBounds = true
                view.addSubview(distanceLabel)
                view.addSubview(imgView)
                
                 planeGeoMetryP1.firstMaterial?.diffuse.contents = UIImage(view: view)
            } else {
                print("image not found")
                //self.annotationNode.name = "name"
            }
        }
       self.annotationNode.geometry = planeGeoMetryP1
        super.init(location: location)
        
        let billboardConstraint = SCNBillboardConstraint()
        billboardConstraint.freeAxes = SCNBillboardAxis.Y
        constraints = [billboardConstraint]
        
        addChildNode(annotationNode)
    }
    
    public init(location: CLLocation?, viewCategories: UIView, name: String, imageurl: String, childPostion: Int = -1, isSingleView: Bool = true) {
        self.image = UIImage()
        annotationNode = SCNNode()
         var planeGeoMetry:SCNPlane!
        if isSingleView {
            planeGeoMetry = SCNPlane(width: (320/100)*0.3, height: (425/100)*0.3)
        } else {
            planeGeoMetry = SCNPlane()
        }
        planeGeoMetry.firstMaterial?.fillMode = .fill
        super.init(location: location)
        let categoryView = viewCategories as! THARCategoriesView
        categoryView.categoriesImageView.layer.cornerRadius = 10
        categoryView.categoriesImageView.layer.masksToBounds = true
        if childPostion == 0 {
            categoryView.categoriesImageView.layer.borderColor = UIColor.hex("fcdf4e", alpha: 1).cgColor
            categoryView.categoriesImageView.layer.borderWidth = 5
        }
        planeGeoMetry.firstMaterial?.diffuse.contents = UIImage(view: categoryView)
        self.name = name
        self.geometry = planeGeoMetry
        let billboardConstraint = SCNBillboardConstraint()
        billboardConstraint.freeAxes = SCNBillboardAxis.Y
        self.constraints = [billboardConstraint]
        
        categoryView.categoriesImageView.sd_setImage(with: URL(string: imageurl), placeholderImage: nil, options: .retryFailed, progress: { (block1, block2, imageurl) in
            
        }) { (image, error, imageCacheType, imageUrl) in
            if image != nil {
                if self.geometry?.firstMaterial?.diffuse.contents != nil {
                    categoryView.circularView.isHidden = true
                    planeGeoMetry.firstMaterial?.diffuse.contents = UIImage(view: categoryView)
                    self.name = name
                    self.geometry = planeGeoMetry
                }
            }else {
                print("image not found")
            }
        }
    }
    
    public init(location: CLLocation?, viewRecomodation: UIView, name: String, imageurl: String, childPostion: Int = -1) {
        self.image = UIImage()
        annotationNode = SCNNode()
        let planeGeoMetryP1:SCNPlane = SCNPlane(width: (320/100)*0.3, height: (425/100)*0.3)
        planeGeoMetryP1.firstMaterial?.fillMode = .fill
        super.init(location: location)
        let recomondationView = viewRecomodation as! THARRecomondationsView
        recomondationView.layer.cornerRadius = 10
        recomondationView.layer.masksToBounds = true
        if childPostion == 0 {
            recomondationView.layer.borderColor = UIColor.hex("fcdf4e", alpha: 1).cgColor
            recomondationView.layer.borderWidth = 5
        }
        planeGeoMetryP1.firstMaterial?.diffuse.contents = UIImage(view: recomondationView)
        self.name = name
        self.geometry = planeGeoMetryP1
        let billboardConstraint = SCNBillboardConstraint()
        billboardConstraint.freeAxes = SCNBillboardAxis.Y
        self.constraints = [billboardConstraint]
    
        recomondationView.shopImage.sd_setImage(with: URL(string: imageurl), placeholderImage: nil, options: .retryFailed, progress: { (block1, block2, imageurl) in
            
        }) { (image, error, imageCacheType, imageUrl) in
            if image != nil {
                if self.geometry?.firstMaterial?.diffuse.contents != nil {
                    recomondationView.circularView.isHidden = true
                    planeGeoMetryP1.firstMaterial?.diffuse.contents = UIImage(view: recomondationView)
                    self.name = name
                    self.geometry = planeGeoMetryP1
                }
            }else {
                print("image not found")
            }
        }
    }
    
    public init(location: CLLocation?, viewRecomodationGrid: UIView, name: String, imageurl: String) {
        self.image = UIImage()
        annotationNode = SCNNode()
        let planeGeoMetryP1:SCNPlane = SCNPlane()
        planeGeoMetryP1.firstMaterial?.fillMode = .fill
        super.init(location: location)
        let recomondationView = viewRecomodationGrid as! THARRecomondationsView
        recomondationView.layer.cornerRadius = 10
        recomondationView.layer.masksToBounds = true
        recomondationView.layer.borderColor = UIColor.hex("fcdf4e", alpha: 1).cgColor
        recomondationView.layer.borderWidth = 3
        planeGeoMetryP1.firstMaterial?.diffuse.contents = UIImage(view: recomondationView)
        self.name = name
        self.geometry = planeGeoMetryP1
        
        let billboardConstraint = SCNBillboardConstraint()
        billboardConstraint.freeAxes = SCNBillboardAxis.Y
        self.constraints = [billboardConstraint]
        
        recomondationView.shopImage.sd_setImage(with: URL(string: imageurl), placeholderImage: nil, options: .retryFailed, progress: { (block1, block2, imageurl) in
            
        }) { (image, error, imageCacheType, imageUrl) in
            if image != nil {
                if self.geometry?.firstMaterial?.diffuse.contents != nil {
                    recomondationView.circularView.isHidden = true
                    planeGeoMetryP1.firstMaterial?.diffuse.contents = UIImage(view: recomondationView)
                    self.name = name
                    self.geometry = planeGeoMetryP1
                }
            }else {
                print("image not found")
            }
        }
    }
    
    public init(location: CLLocation?, viewPost: UIView, name: String, imageurl: String, childPostion: Int = -1) {
        self.image = UIImage()
        annotationNode = SCNNode()
        let planeGeoMetryP1:SCNPlane = SCNPlane(width: (320/100)*0.3, height: (380/100)*0.3)
        planeGeoMetryP1.firstMaterial?.fillMode = .fill
        super.init(location: location)
        let postView = viewPost as! THARPostView
        postView.layer.cornerRadius = 10
        postView.layer.masksToBounds = true
        if childPostion == 0 {
            postView.layer.borderColor = UIColor.hex("fcdf4e", alpha: 1).cgColor
            postView.layer.borderWidth = 5
        }
            planeGeoMetryP1.firstMaterial?.diffuse.contents = UIImage(view: postView)//UIImage.imageWithView(view: postView)
            self.name = name
            self.geometry = planeGeoMetryP1
            
            let billboardConstraint = SCNBillboardConstraint()
            billboardConstraint.freeAxes = SCNBillboardAxis.Y
            self.constraints = [billboardConstraint]
            
            postView.socialPostImage.sd_setImage(with: URL(string: imageurl), placeholderImage: nil, options: .retryFailed, progress: { (block1, block2, imageurl) in
                
            }) { (image, error, imageCacheType, imageUrl) in
                if image != nil {
                    if self.geometry?.firstMaterial?.diffuse.contents != nil {
                        postView.circleView.isHidden = true
                        planeGeoMetryP1.firstMaterial?.diffuse.contents = UIImage(view: postView)//UIImage.imageWithView(view: postView)
                        self.name = name
                        self.geometry = planeGeoMetryP1
                    }
                } else {
                    print("image not found")
                }
            }
    }
    
    public init(location: CLLocation?, viewPostGrid: UIView, name: String, imageurl: String) {
        self.image = UIImage()
        annotationNode = SCNNode()
        let planeGeoMetryP1:SCNPlane = SCNPlane()
        planeGeoMetryP1.firstMaterial?.fillMode = .fill
        super.init(location: location)
        let postView = viewPostGrid as! THARPostView
        postView.layer.cornerRadius = 10
        postView.layer.masksToBounds = true
        postView.layer.borderColor = UIColor.hex("fcdf4e", alpha: 1).cgColor
        postView.layer.borderWidth = 3
        planeGeoMetryP1.firstMaterial?.diffuse.contents = UIImage(view: postView)
        self.name = name
        self.geometry = planeGeoMetryP1
        
        let billboardConstraint = SCNBillboardConstraint()
        billboardConstraint.freeAxes = SCNBillboardAxis.Y
        self.constraints = [billboardConstraint]
      
        postView.socialPostImage.sd_setImage(with: URL(string: imageurl), placeholderImage: nil, options: .retryFailed, progress: { (block1, block2, imageurl) in
            
        }) { (image, error, imageCacheType, imageUrl) in
            if image != nil {
                if self.geometry?.firstMaterial?.diffuse.contents != nil {
                    postView.circleView.isHidden = true
                    planeGeoMetryP1.firstMaterial?.diffuse.contents = UIImage(view: postView)
                    self.name = name
                    self.geometry = planeGeoMetryP1
                }
            }else {
                print("image not found")
            }
        }
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

@available(iOS 11.0, *)
extension LocationNode {
    
    
}

// MARK: - Protocols for Virtual Objects

protocol ReactsToScale {
    func reactToScale()
}

extension SCNNode {
    
    func reactsToScale() -> ReactsToScale? {
        if let canReact = self as? ReactsToScale {
            return canReact
        }
        
        if parent != nil {
            return parent!.reactsToScale()
        }

        return nil
    }
}

