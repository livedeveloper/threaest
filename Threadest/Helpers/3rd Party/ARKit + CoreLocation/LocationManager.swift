//
//  LocationManager.swift
//  ARKit+CoreLocation
//
//  Created by Andrew Hart on 02/07/2017.
//  Copyright © 2017 Project Dent. All rights reserved.
//

import Foundation
import CoreLocation

protocol LocationManagerDelegate: class {
    func locationManagerDidUpdateLocation(_ locationManager: LocationManager, location: CLLocation)
    func locationManagerDidUpdateHeading(_ locationManager: LocationManager, heading: CLLocationDirection, accuracy: CLLocationDirection)
}

protocol LocationMangerRegionDelegate: class {
    //delegates for region monitoring
    func locationManagerUpdate(coordinate: CLLocationCoordinate2D)
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion)
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion)
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion)
    func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion)
    func locationManager(_ manager: CLLocationManager, didVisit visit: CLVisit)
    func retailEndpoitCalled()
}

///Handles retrieving the location and heading from CoreLocation
///Does not contain anything related to ARKit or advanced location
class LocationManager: NSObject, CLLocationManagerDelegate {
    weak var delegate: LocationManagerDelegate?
    
    weak var regionDelegate: LocationMangerRegionDelegate?
    
    open var completion : (()->())?
    
    private var locationManager: CLLocationManager?
    var currentLocation: CLLocation?
    var heading: CLLocationDirection?
    var headingAccuracy: CLLocationDegrees?
    
    override init() {
        super.init()
        
        self.locationManager = CLLocationManager()
        //self.locationManager!.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        self.locationManager!.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager!.distanceFilter = 10
        //self.locationManager!.headingFilter = kCLDistanceFilterNone
        self.locationManager!.pausesLocationUpdatesAutomatically = false
        self.locationManager!.delegate = self
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways {
            locationManager!.allowsBackgroundLocationUpdates = true
        }
      //  self.locationManager!.startUpdatingHeading()
      //  self.locationManager!.startUpdatingLocation()
        self.locationManager?.startMonitoringSignificantLocationChanges()
        self.currentLocation = self.locationManager!.location
    }
    
    func forgroundLocation() {
        self.locationManager!.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager?.stopMonitoringSignificantLocationChanges()
        self.locationManager?.startUpdatingLocation()
    }
    
    func BackGroundLocation() {
        self.locationManager?.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        self.locationManager?.stopUpdatingLocation()
        self.locationManager?.startMonitoringSignificantLocationChanges()
    }
    
    func requestAuthorization(com: @escaping () -> Void) {
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse {
            return
        }
        
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.denied ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.restricted {
            return
        }
        completion = com
        self.locationManager?.requestAlwaysAuthorization()
    }
    
    func retailStoreLocation(view: UIView) {
        if let lat = LocationManager().currentLocation?.coordinate.latitude, let long = LocationManager().currentLocation?.coordinate.longitude {
            GeneralMethods.sharedInstance.loadLoading(view: view)
            THUserManager.sharedInstance.performUpdateUserLocaltion(with: lat, logitude: long, completion: { (error) in
                //GeneralMethods.sharedInstance.dismissLoading(view: view)
                for retailLocation in THUserManager.sharedInstance.retailLocation {
                    let currRegion = CLCircularRegion(center: CLLocationCoordinate2D(latitude: CLLocationDegrees(retailLocation.latitude), longitude: CLLocationDegrees(retailLocation.longitude)), radius: 50, identifier: "\(retailLocation.id)")
                    if (self.locationManager?.monitoredRegions.contains(currRegion))! {
                        self.locationManager?.requestState(for: currRegion)
                    } else {
                        self.locationManager?.startMonitoring(for: currRegion)
                        let dispatchTime = DispatchTime.now() + .seconds(2)
                        DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
                            self.locationManager?.requestState(for: currRegion)
                        }
                    }
                    self.regionDelegate?.locationManagerUpdate(coordinate: CLLocationCoordinate2D(latitude: CLLocationDegrees(retailLocation.latitude), longitude: CLLocationDegrees(retailLocation.latitude)))
                }
                self.regionDelegate?.retailEndpoitCalled()
            })
        }
    }
    
    //MARK: - CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status != .notDetermined {
            if completion != nil {
                completion!()
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        for location in locations {
            self.delegate?.locationManagerDidUpdateLocation(self, location: location)
        }
        debugPrint(self.currentLocation)
        self.currentLocation = manager.location
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        if newHeading.headingAccuracy >= 0 {
            self.heading = newHeading.trueHeading
        } else {
            self.heading = newHeading.magneticHeading
        }
        
        self.headingAccuracy = newHeading.headingAccuracy
        
        self.delegate?.locationManagerDidUpdateHeading(self, heading: self.heading!, accuracy: newHeading.headingAccuracy)
    }
    
    func locationManagerShouldDisplayHeadingCalibration(_ manager: CLLocationManager) -> Bool {
        return true
    }
    
    func locationManager(_ manager: CLLocationManager, didVisit visit: CLVisit) {
        self.regionDelegate?.locationManager(manager, didVisit: visit)
    }
    
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        self.regionDelegate?.locationManager(manager, didStartMonitoringFor: region)
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        self.regionDelegate?.locationManager(manager, didEnterRegion: region)
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        self.regionDelegate?.locationManager(manager, didExitRegion: region)
    }
    
    func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
        self.regionDelegate?.locationManager(manager, didDetermineState: state, for: region)
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        debugPrint(error.localizedDescription)
    }
}

