//
//  TTSliderDataSource.swift
//
//  Created by Sagar R on 26/03/17.
//

import UIKit

public protocol TTSliderDataSource : class {
    
    func numberOfSlides(_ slider: TTSlider) -> Int
    
    func slider(_ slider: TTSlider, slideAtIndex index: Int) -> TTFilter
    
    func startAtIndex(_ slider: TTSlider) -> Int
}

// MARK : - Extension Datasource

extension TTSliderDataSource {
    
    func numberOfSlides(_ slider: TTSlider) -> Int {
        
        return 3
    }
    
    func slider(_ slider: TTSlider, slideAtIndex index: Int) -> TTFilter {
        
        let filter = TTFilter(frame: slider.frame)
        switch index {
        case 0:
            filter.backgroundColor = UIColor.black
            return filter
        case 1:
            filter.backgroundColor = UIColor.green
            return filter
        case 2:
            filter.backgroundColor = UIColor.yellow
            return filter
        default:
            filter.backgroundColor = UIColor.yellow
            return filter
        }
    }
    
    func startAtIndex(_ slider: TTSlider) -> Int {
        return 0
    }
}
