//
//  TTSticker.swift
//
//  Created by Sagar R on 26/03/17.
//

import UIKit

open class TTSticker: UIImageView {
    
    public init(frame: CGRect, image:UIImage, withContentMode mode: UIViewContentMode = .scaleAspectFit, atZPosition zIndex:CGFloat? = nil) {
        super.init(frame: frame)
        
        self.contentMode = mode
        self.clipsToBounds = true
        self.image = image
        if let index = zIndex {
            self.layer.zPosition = index
        }
        else {
            self.layer.zPosition = 0
        }
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK : - NSCopying protocol 

extension TTSticker: NSCopying {
    
    public func copy(with zone: NSZone?) -> Any {
        let copy = TTSticker(frame: self.frame, image: (self.image)!, withContentMode: self.contentMode)
        return copy
    }
}
