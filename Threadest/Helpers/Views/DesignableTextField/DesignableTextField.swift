//
//  DesignableTextField.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 09/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableTextField: UITextField {
    @IBInspectable var insetX: CGFloat = 0.0
    @IBInspectable var insetY: CGFloat = 0.0
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor = .clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var placeholderColor: UIColor = .gray {
        didSet {
            let currentFont = UIFont(name: self.font!.fontName, size: self.font!.pointSize)!
            
            self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "", attributes:[NSForegroundColorAttributeName: placeholderColor, NSFontAttributeName: currentFont])
        }
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX, dy: insetY)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX, dy: insetY)
    }
}
