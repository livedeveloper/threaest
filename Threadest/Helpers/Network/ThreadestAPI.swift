    //
//  ThreadestAPI.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 09/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation
import Moya
import KeychainSwift

let endpointClosure = { (target: Threadest) -> Endpoint<Threadest> in
    let defaultEndpoint = MoyaProvider.defaultEndpointMapping(for: target)
    let headerFields = ["Authorization" : THUserManager.sharedInstance.authToken()]
    return defaultEndpoint.adding(newHTTPHeaderFields: headerFields)
}

let ThreadestProvider = MoyaProvider<Threadest>(endpointClosure: endpointClosure)

public enum Threadest {
    // Authentication
    case createUser(Registration, Bool)
    case resetPasswordEmail(String)
    case resetPasswordSMS(String)
    case checkSMSCode(String)
    case login(Login)
    case findUser(Int)
    case updateUser(Int, User)
    case updateUserAccount(Int, User)
    case updateUserTagList(Int, String)
    case checkIfAuthUserExists(String)
    case resetUserPassword(String,String)
    case retailLocation(Double,Double)
    // all users to follow
    case userToFollow(Int)
    case followAllUser
    case inviteFriend(Int)
    case checkLoyaltyStatusBrand(Int)
    
    //Reedem from promocode
    case redeemFromPromoCode(Int, String)
    
    //Add product
    case addProduct(AddProduct)

    // all brand list
    case brandsToFollow
    // follow All Brand
    case followAllBrands
    case isFollwingBrand(Int)

    case saveNewAuthToken(String, String, String)
    case createFacebookUser(String, String, String, String, Registration)
    case sendPNToken(String)
    case deletePNToken(String)
    case updatePNToken(String, String)
    
    //retrieve HashTag
    case retrieveHashTagProduct(String, Int)

    // Brands
    case retrieveBrands
    case retrieveBrand(Int, Int, Bool)
    
    //current Influencer Score
    case retrieveInfluencerScore(Int)

    // Social interactions
    case searchDiscoverPosts(Int)

    // Profie closet
    case userCloset(Int, Int)

    // Profile Posts
    case userProfilePosts(Int, Int)

    // Closet Category
    case closetCategory(Int, String)
    
    //Closet Favorite
    case closetFavorite(Int, Int)

    // Add to cart
    case addToCart(Int, Int, Int, String)

    // retrieve to cart
    case retrieveCartProducts(Double, Double)

    // Delete product from cart
    case deleteCartProduct(Int)

    // save user payment method
    case saveUserPaymentMethod(String, String, String, Int, String)

    // save user shipping address
    case saveUserShippingAddress(String, String, String, String, String,String)

    // retrieve user payment methods
    case retrieveUserPaymentMethods

    // retrieve user Shipping Address
    case retrieveUserShippingAddress

    // apple pay instant buy native ios
    case instantBuyNativeiOS(Int, Int, String, String, String, String, String, String, String, String, String, String, String, Int, String)

    // apple pay instant buy from cart native ios
    case instantBuyFromCartNativeiOS(String, String, String, String, String, String, String, String, String, String, String)

    // apple pay instant buy taxCalculation
    case instantBuyTaxCalculations(Int,String, String, String)

    // apple pay instant buy from cart taxCalculation
    case instantBuyFromCartTaxCalculations(String, String, String)

    // Featured Feed By Category
    case featuredFeedByCategory(Int, String, Int)

    // Social Interaction posts
    case socialInteractionPosts(Int)

    // Follow User
    case followUser(Int)

    // for Upvote
    case likeUpVotes(Int, Int, String)

    // for rethread post
    case rethreadPost(Int)

    // retrieve Comment
    case retrieveComment(Int, Int)

    // retrieve Product Comment
    case retrieveProductComment(Int, Int)

    // retrieve Product Comment
    case retrieveProduct(Int, Int)

    // post Comment
    case postComment(postComment)
    
    // delete Post Comment
    case deletePostComment(Int, Int)

    // post Product Comment
    case postProductComment(PostProductComment)
    
    // delete Product Comment
    case deleteProductComment(Int, Int, Int)

    // retrieve Brand Product
    case retrieveBrandProduct(Int, Int, Bool, String)
    
    // retrieve Bradn Product From Dots
    case retrieveBrandProductFromBlueDots(Int, Int, String, Int)

    // Notifications
    case notifications(Int)

    case earlyAccess(String)

    //Unfollow user
    case unFollowUser(Int)

    //Notification post
    case notificationPost(Int)

    //Create Product
    case createProduct(CreateProduct)

    //Update sizes
    case updateClosetSize(String,String,Int)

    //Add location
    case addUserLocation(Double,Double,Int)
    //Follow brand
    case followBrand(Int)

    //Unfollow brand
    case unFollowBrand(Int)

    //Delete post
    case deletePost(Int)
    
    //Current user purchases and orders
    case userPurchaseOrders(Int, Int)
    
    //Pay Now order
    case createOrderNativeiOS(PayNowOrder)
    
    //Current user followers
    case userFollowers(Int, Int)

    //Current user following
    case userFollowing(Int, Int)
    
    //Current user following
    case userBrandFollowing(Int, Int)
    
    //Current user update profileimage
    case userUpdateImage(Int,UIImage)
    
    //Follow users that are in your phone book
    case followUsersFromPhone(String)
    
    //Follow users that are in your facebook
    case followUsersFromFB(String)
    
    // Suggestions
    case getSuggestions(phoneNumbers: [String]?, facebookToken: String?)
    
    //retrieve post liked users
    case postVotesCount(postId: Int)
    
    //retail locations near by products
    case retailLocationNearProduct(Int,Int)
    
    // Notify me when available
    case notifyMeWhenInStock(Int, Int)
    
    //suggestion product
    case suggestionProduct(Int, Int, Int)
    
    case googleCloudVisionMatch(UIImage)
    
    case googleCloudVisionMatchByBrandId(UIImage, Int)
    
    case createBrand(String, UIImage)
    
    case brandFollower(Int, Int)
    
    case giveAwayContestant(Int, Int)
}

extension Threadest: TargetType {
    public var baseURL: URL { return URL(string: "https://threadest.herokuapp.com/api/v1/")! }
    public var path: String {
        switch self {
        // Authentication
        case .createUser:
            return "authentication/registrations"
        case .resetPasswordEmail:
            return "authentication/passwords/reset_via_email"
        case .resetPasswordSMS:
            return "authentication/passwords/reset_via_sms"
        case .checkSMSCode:
            return "authentication/passwords/check_sms_code"
        case .login:
            return "authentication/sessions"
        case .checkIfAuthUserExists:
            return "authentication/omniauth/check_if_auth_user_exists"
            
        //retrieve hashtag
        case .retrieveHashTagProduct:
            return "social_interactions/posts/hash_tag_page"
            
        //update coordinate of userlocation
        case .retailLocation:
            return "retail_locations/retail_locations/nearby"
            
        //Invite friend to get number of invite to get price
        case .inviteFriend(let userId):
            return "users/users/\(userId)/invite_friends"
            
        //Reedem From promocode
        case .redeemFromPromoCode(let userId, _):
            return "users/users/\(userId)/redeem_from_promo_code"
            
        //
        case .checkLoyaltyStatusBrand:
            return "social_interactions/follows/loyalty_status_with_brand"
            
        //closet favorite
        case .closetFavorite(let userid, _):
            return "users/users/\(userid)/favorites"
            
        //Add Product
        case .addProduct(let addProduct):
            return "brands/brands/\(addProduct.productBrandId)/products"
            
        //Current Influencer Score
        case .retrieveInfluencerScore(let userid):
            return "users/users/\(userid)/current_user_score"
        //all users to follow
        case .userToFollow:
            return "social_interactions/follows/users_to_follow"
        //follow all userToFollow
        case .followAllUser:
            return "social_interactions/follows/follow_all_users"
        //brands To Follow
        case .brandsToFollow:
            return "social_interactions/follows/brands_to_follow"
        //follow brand
        case .followBrand(let id):
            return "social_interactions/follows/\(id)/follow_brand"
        //unFollow Brand
        case .unFollowBrand(let id):
            return "social_interactions/follows/\(id)/unfollow_brand"
            
        case .isFollwingBrand:
            return "social_interactions/follows/is_following_brand"
        //follow all brands
        case .followAllBrands:
            return "social_interactions/follows/follow_all_brands"
        case .saveNewAuthToken:
            return "authentication/omniauth/save_new_auth_token"
        case .createFacebookUser:
            return "authentication/omniauth/facebook"
        case .sendPNToken:
            return "users/users/add_new_push_notification_token"
        case .deletePNToken:
            return "users/users/remove_push_notification_device"
        case .updatePNToken:
            return "users/users/update_push_notification_token"

        case .findUser(let id):
            return "users/users/\(id)"

        case .updateUser(let id, _):
            return "users/users/\(id)"
            
        case .updateUserTagList(let id, _):
            return "users/users/\(id)"
            
        case .resetUserPassword:
            return "authentication/passwords/sms_code_choose_new_password"
        // Brands
        case .retrieveBrands:
            return "brands/brands"
        case .retrieveBrand(let id, _ ,let isFromCloset):
            if isFromCloset {
                return "brands/brands/\(id)/closet_show"
            } else {
                return "brands/brands/\(id)"
            }
        // Social interactions
        case .searchDiscoverPosts:
            return "social_interactions/posts/search_and_discover"

        //Profile closet
        case .userCloset(let id, _):
            return "users/users/\(id)/closet"

        // Add To Cart
        case .addToCart:
            return "carts/cart_items"

        // retrieve products of cart
        case .retrieveCartProducts:
            return "carts/cart_items"
        // Delete product from cart
        case .deleteCartProduct(let cartProductID):
            return "carts/cart_items/\(cartProductID)"

        // save user payment method
        case .saveUserPaymentMethod:
            return "/transactions/user_payment_methods"

        // retrieve user payment method
        case .retrieveUserPaymentMethods:
            return "/transactions/user_payment_methods"

        // save user shipping address
        case .saveUserShippingAddress:
            if let currentUser = THUserManager.sharedInstance.currentUserdata() {
                return "users/users/\(currentUser.userId)"
            }
            return "users/users/"

        case .retrieveUserShippingAddress:
            if let currentUser = THUserManager.sharedInstance.currentUserdata() {
                return "users/users/\(currentUser.userId)"
            }
            return "users/users/"
            

        case .instantBuyNativeiOS:
            return "transactions/orders/instant_buy_native_ios"

        case .instantBuyFromCartNativeiOS:
            return "transactions/orders/instant_buy_from_cart_native_ios"

        case .instantBuyTaxCalculations:
            return "transactions/orders/instant_buy_tax_calculations"

        case .instantBuyFromCartTaxCalculations:
            return "transactions/orders/instant_buy_from_cart_tax_calculations"

        //Profile Posts
        case .userProfilePosts(let id, _):
            return "users/users/\(id)"
            
        case .updateUserAccount(let id, _):
            return "users/users/\(id)"

        //Closet Category
        case .closetCategory(let id, _):
            return "users/users/\(id)/closet_category"

        //Featured Feed By Category
        case .featuredFeedByCategory:
            return "social_interactions/posts/featured_feed_by_category"

        // Social Interaction posts
        case .socialInteractionPosts:
            return  "social_interactions/posts/your_feed"


        // Follow User
        case .followUser(let id):
            return  "social_interactions/follows/\(id)/follow_user"

        // for Upvote
        case .likeUpVotes(let id):
            return "social_interactions/posts/\(id)/votes"

        // for rethread Post
        case .rethreadPost(let postId):
            return "social_interactions/posts/\(postId)/rethread"

        // retrieve Post Comment
        case .retrieveComment(let postId, _):
            return "social_interactions/posts/\(postId)/comments"

        // retrieve Product
        case .retrieveProduct(let brandId, let productId):
            return "brands/brands/\(brandId)/products/\(productId)"

        // retrieve Product Comment
        case .retrieveProductComment(let brandId, let productId):
            return "brands/brands/\(brandId)/products/\(productId)/comments"


        // post Comment
        case .postComment(let postcomment):
            return "social_interactions/posts/\(postcomment.postId)/comments"
            
        // delete post Comment
        case .deletePostComment(let postId, let commentId):
            return "social_interactions/posts/\(postId)/comments/\(commentId)"

        // post Product Comment
        case .postProductComment(let postProductComment):
            return "brands/brands/\(postProductComment.brandId)/products/\(postProductComment.productId)/comments"
            
        // delete Product Comment
        case .deleteProductComment(let brandId, let productId, let commentId):
            return "brands/brands/\(brandId)/products/\(productId)/comments/\(commentId)"

        // retrieve Brand Product
        case .retrieveBrandProduct(let brandId, let productId, _, _):
            return "brands/brands/\(brandId)/products/\(productId)"
            
        // retrieve Brand Product From Blue dots
        case .retrieveBrandProductFromBlueDots(let brandId, let productId, _, _ ):
            return "brands/brands/\(brandId)/products/\(productId)"
            
        // Notifications
        case .notifications:
            return "notifications/notifications"

        case .earlyAccess:
            return "users/subscribers"

            // UnFollow user

        case .unFollowUser(let id):
            return  "social_interactions/follows/\(id)/unfollow_user"

        case .notificationPost(let id):
            return  "social_interactions/posts/\(id)"


        //Create Product
        case .createProduct:
            return "social_interactions/posts"

        //Delete Post
        case .deletePost(let id):
            return  "social_interactions/posts/\(id)"

            // updateCloset
        case .updateClosetSize(let id):
            return "users/users/\(id)"

        case .addUserLocation( _):
            return "users/users/\(String(describing: THUserManager.sharedInstance.currentUserdata()?.userId))"
        
        //Current user purchases and orders
        case .userPurchaseOrders(let id, _):
            return  "users/users/\(id)/purchases_and_orders"
            
        //Pay and Now Create Native iOS
        case .createOrderNativeiOS:
            return "transactions/orders/create_native_ios"
            
        //Current user followers
        case .userFollowers(let id, _):
            return "users/users/\(id)/followers"
        
        //Current user following
        case .userFollowing(let id, _):
            return "users/users/\(id)/followings"
            
        //Current user brand following
        case .userBrandFollowing(let id, _ ):
            return "users/users/\(id)/brand_followings"
        
        case .userUpdateImage(let id, _):
            return "users/users/\(id)"
            
        case .followUsersFromPhone(_):
            return "social_interactions/follows/by_phone_numbers"
        
        case .followUsersFromFB(_):
            return "social_interactions/follows/by_facebook"
            
        // Suggestions
        case .getSuggestions(_, _):
            return "suggestions_list"
            
        //count posts votes
        case .postVotesCount(let postId):
            return "social_interactions/posts/\(postId)/votes"
            
        //retail locations near by products
        case .retailLocationNearProduct:
            return "retail_locations/retail_locations/nearby_products"
            
        case .notifyMeWhenInStock(let brandId, let productId):
            return "brands/brands/\(brandId)/products/\(productId)/notify_me_when_in_stock"
            
        case .suggestionProduct(let brandId, let productId, _):
            return "brands/brands/\(brandId)/products/\(productId)/suggestions"
            
        case .googleCloudVisionMatch:
            return "social_interactions/posts/google_cloud_vision_matcher"
            
        case .googleCloudVisionMatchByBrandId(_, _):
            return "social_interactions/posts/google_cloud_vision_matcher_by_brand"
            
        case .createBrand:
            return "brands/brands"
            
        case .brandFollower(let brandId, _):
            return "brands/brands/\(brandId)/brand_followers"
            
        //GiveAway
        case .giveAwayContestant(let brandId, let productId):
            return "brands/brands/\(brandId)/products/\(productId)/giveaway_contestants"
            
        }
        
        
    }
    public var method: Moya.Method {
        switch self {
        case .createUser, .resetPasswordEmail, .saveNewAuthToken, .createFacebookUser, .sendPNToken, .resetPasswordSMS, .checkSMSCode, .login, .checkIfAuthUserExists, .followUser, .likeUpVotes, .rethreadPost, .earlyAccess, .addToCart, .likeUpVotes, .saveUserPaymentMethod, .postComment, .postProductComment, .createProduct, .followAllUser, .followBrand, .followAllBrands, .instantBuyNativeiOS, .instantBuyFromCartNativeiOS, .instantBuyTaxCalculations, .instantBuyFromCartTaxCalculations, .createOrderNativeiOS , .followUsersFromPhone, .followUsersFromFB, .getSuggestions, .resetUserPassword, .addProduct, .redeemFromPromoCode, .googleCloudVisionMatch, .googleCloudVisionMatchByBrandId, .createBrand, .giveAwayContestant:
                return .post

        case .findUser, .retrieveInfluencerScore, .closetFavorite,.userToFollow, .brandsToFollow, .retrieveHashTagProduct, .retrieveBrands, .searchDiscoverPosts, .userCloset, .userProfilePosts, .closetCategory, .featuredFeedByCategory, .socialInteractionPosts, .notifications, .notificationPost, .retrieveCartProducts, .retrieveComment, .retrieveProductComment, .retrieveBrand, .retrieveProduct, .retrieveBrandProduct, .retrieveBrandProductFromBlueDots, .retrieveUserPaymentMethods, .retrieveUserShippingAddress,.userPurchaseOrders,.userFollowers,.userFollowing,.userBrandFollowing, .postVotesCount, .isFollwingBrand, .retailLocation, .inviteFriend, .checkLoyaltyStatusBrand, .retailLocationNearProduct, .notifyMeWhenInStock, .suggestionProduct, .brandFollower:

            return .get

        case .updateUser, .updateClosetSize, .updateUserAccount, .saveUserShippingAddress,.addUserLocation, .updatePNToken, .updateUserTagList , .userUpdateImage:
            return .put

        case .unFollowUser, .deleteCartProduct, .unFollowBrand, .deletePNToken, .deletePost, .deletePostComment, .deleteProductComment:
            return .delete

        default:
            return .get
        }
    }


    public var parameters: [String: Any]? {
        switch self {
        case .createUser(let registration, let tempAccount):
            if tempAccount {
                return ["user[temp_account]": "true"]
            } else {
                return ["user[email]" : registration.email, "user[username]" : registration.username, "user[password]" : registration.password, "user[password_confirmation]" : registration.passwordConfirmation, "user[mobile_phone_number]": registration.mobilePhoneNumber]
            }
        case .updateUser(_, let user):
            return ["user[height]" : user.height, "user[age]": user.age, "user[gender]": user.gender, "user[waist_size]": user.waistSize, "user[shirt_size]": user.shirtSize, "user[shoe_size]": user.shoe_size, "user[neck_size]": user.neckSize, "user[bra_size]": user.braSize, "user[hips_size]": user.hipsSize, "user[about]": user.about, "user[website]": user.website]
        case .updateUserAccount(_, let user):
            return ["user[email]": user.email, "user[password]":user.password, "user[mobile_phone_number]": user.phoneNumber, "user[username]": user.username, "user[temp_account]": "\(user.tempAccount)"]
            
        case .resetUserPassword(let passwordToken , let password):
            return ["user[reset_password_token]":passwordToken , "user[password]":password]
            
        //Add Product
        case .addProduct(let addProduct):
            return ["product[title]": addProduct.productTitle, "product[url]": addProduct.productURL, "product[brand_id]": addProduct.productBrandId, "product[default_price]": addProduct.productDefaultPrice, "product[description]": addProduct.productDescription];
            
        //Reedem from promocode
        case .redeemFromPromoCode(_ , let promoCode):
            return ["promo_code": promoCode]
        
        case .retrieveBrandProduct(_, _, let geofence, let shareCode):
            if shareCode != "" {
                return ["geolocated": "\(geofence)", "share_code":shareCode]
            } else {
                return ["geolocated": "\(geofence)"]
            }
        case .checkLoyaltyStatusBrand(let brandId):
            return ["id": brandId]
            
        case .retailLocation(let latitude, let longitude):
            return ["latitude": latitude, "longitude":longitude]
        case .userToFollow(let page):
            return ["page":page]
        case .userFollowers(_, let page):
            return ["page":page]
        case .userFollowing(_, let page):
            return ["page":page]
        case .userProfilePosts(_ , let page):
            return ["page":page]
        case .userPurchaseOrders(_ , let page):
            return ["page": page]
        case .updateUserTagList(_, let taglist):
            return ["user[tag_list]" : taglist]
        case .checkIfAuthUserExists(let emailAddress):
            return ["email":emailAddress]
        case .resetPasswordEmail(let email):
            return ["user[email]" : email]
        case .resetPasswordSMS(let email):
            return ["user[email]" : email]
        case .checkSMSCode(let smsCode):
            return ["user[reset_password_token]" : smsCode]
        case .saveNewAuthToken(let email, let token, let uid):
            return ["email": email, "auth_token": token, "uid": uid]
        case .retrieveHashTagProduct(let hashTag, let page):
            return ["hash_tag":hashTag, "page":page]
        case .closetFavorite(_ , let page):
            return ["page": page]
        case .createFacebookUser(let email, let token, let uid, let gender, let registration):
            return ["email": registration.email, "username": registration.username, "mobile_phone_number": registration.mobilePhoneNumber, "password": registration.password, "facebookRegistrationData" : ["fbEmail" : email, "fbToken" : token, "fbUID" : uid, "fbGender" : gender]]
        case .sendPNToken(let token):
            return ["token": token, "platform" : "iOS"]
        case .deletePNToken(let token):
            return ["token" : token, "platform" : "iOS"]
        case .updatePNToken(let oldToken, let newToken):
            return ["token" : oldToken, "platform" : "iOS", "new_token" : newToken]

        case .login(let user):
            return ["user[email]" : user.email, "user[password]" : user.password]
            
        case .isFollwingBrand(let brandId):
            return ["id": brandId]
        case .searchDiscoverPosts(let page):
            return ["page" : page]
        case .closetCategory(_, let category):
            return ["category":category]
        case .retrieveBrand(_, let page, _):
            return ["page" : page]
            
        case .userCloset(_, let page):
            if page == 1 {
                return ["page": page]
            }
            return ["page" : page, "seed":"4.6050742925521799889115093702241588002e+37"]

        case .addToCart(let productID, let productVarID, let postId, let shareCode):
            if shareCode == "" {
                if postId == -1 {
                    return ["cart_item[product_id]" : productID, "cart_item[product_variation_id]" : productVarID]
                }
                return ["cart_item[product_id]" : productID, "cart_item[product_variation_id]" : productVarID, "cart_item[post_id]": postId]
            } else {
                if postId == -1 {
                    return ["cart_item[product_id]" : productID, "cart_item[product_variation_id]" : productVarID, "share_code": shareCode]
                }
                return ["cart_item[product_id]" : productID, "cart_item[product_variation_id]" : productVarID, "cart_item[post_id]": postId, "share_code": shareCode]
            }
            
            
        case .retrieveCartProducts(let lat, let long):
            return ["lat" : lat, "long" : long]

        // save user payment method
        case .saveUserPaymentMethod(let token, let paymentType, let brand, let last4, let paymentObjectId):
            return ["payment_method[token]" : token, "payment_method[payment_type]" : paymentType, "payment_method[brand]" : brand, "payment_method[last4]" : last4,"payment_method[payment_object_id]" : paymentObjectId]

        // save user payment method
        case .saveUserShippingAddress(let name, let address1, let address2, let city, let state, let zipCode):
            return ["user[name]" : name, "user[street_address_1]" : address1, "user[street_address_2]" : address2, "user[city]" : city, "user[state]" : state, "user[zip_code]" : zipCode]

        case .instantBuyNativeiOS(let productId,let productVarId,let tokenId, let cardToken,let firstName,let lastName,let streetAddress1,let streetAddress2,let state,let city,let zipCode, let phone, let email, let postId, let shareCode):
            if shareCode == "" {
                if postId == -1 {
                    return ["order[product_id]" : productId, "order[product_variation_id]" : productVarId, "order[token]" : tokenId, "order[card_token]" : cardToken, "order[first_name]" : firstName, "order[last_name]" : lastName, "order[street_address_1]" : streetAddress1, "order[street_address_2]" : streetAddress2, "order[state]" : state, "order[city]" : city, "order[zip_code]" : zipCode, "order[phone_number]" : phone, "order[email]" : email]
                } else {
                    return ["order[product_id]" : productId, "order[product_variation_id]" : productVarId, "order[token]" : tokenId, "order[card_token]" : cardToken, "order[first_name]" : firstName, "order[last_name]" : lastName, "order[street_address_1]" : streetAddress1, "order[street_address_2]" : streetAddress2, "order[state]" : state, "order[city]" : city, "order[zip_code]" : zipCode, "order[phone_number]" : phone, "order[email]" : email, "post_id": postId]
                }
            } else {
                if postId == -1 {
                    return ["order[product_id]" : productId, "order[product_variation_id]" : productVarId, "order[token]" : tokenId, "order[card_token]" : cardToken, "order[first_name]" : firstName, "order[last_name]" : lastName, "order[street_address_1]" : streetAddress1, "order[street_address_2]" : streetAddress2, "order[state]" : state, "order[city]" : city, "order[zip_code]" : zipCode, "order[phone_number]" : phone, "order[email]" : email, "share_code": shareCode]
                } else {
                    return ["order[product_id]" : productId, "order[product_variation_id]" : productVarId, "order[token]" : tokenId, "order[card_token]" : cardToken, "order[first_name]" : firstName, "order[last_name]" : lastName, "order[street_address_1]" : streetAddress1, "order[street_address_2]" : streetAddress2, "order[state]" : state, "order[city]" : city, "order[zip_code]" : zipCode, "order[phone_number]" : phone, "order[email]" : email, "post_id": postId, "share_code": shareCode]
                }
            }

        case .instantBuyFromCartNativeiOS(let tokenId, let cardToken,let firstName,let lastName,let streetAddress1,let streetAddress2,let state,let city,let zipCode, let phone, let email):
            return ["order[token]" : tokenId, "order[card_token]" : cardToken, "order[first_name]" : firstName, "order[last_name]" : lastName, "order[street_address_1]" : streetAddress1, "order[street_address_2]" : streetAddress2, "order[state]" : state, "order[city]" : city, "order[zip_code]" : zipCode, "order[phone_number]" : phone, "order[email]" : email]

        case .instantBuyTaxCalculations(let productId, let zipCode, let city, let state):
            return ["order[product_id]" : productId, "order[zip_code]" : zipCode, "order[city]":city, "order[state]":state]

        case .instantBuyFromCartTaxCalculations(let zipCode, let city, let state):
            return ["order[zip_code]" : zipCode, "order[city]":city, "order[state]":state]
            
        case .createOrderNativeiOS(let payNowOrder):
            return ["order[payment_method_id]": payNowOrder.orderToken, "order[street_address_1]": payNowOrder.orderStreetAddress1, "order[street_address_2]": payNowOrder.orderStreetAddress2, "order[zip_code]": payNowOrder.orderZipCode, "order[city]": payNowOrder.orderCity, "order[state]": payNowOrder.orderState, "order[first_name]": payNowOrder.orderFirstName, "order[last_name]": payNowOrder.orderLastName, "order[phone_number]": payNowOrder.orderPhoneNumber]

        case .featuredFeedByCategory(let page , let category, let id):
            return ["tag_category": category , "page": page, "id":id]
            
        case .userBrandFollowing(_, let page):
            return ["page":page]

        case .socialInteractionPosts(let page):
            return ["page": page]
        case .earlyAccess(let email):
            return ["subscriber[email]":email]
        case .likeUpVotes(_, let votableId, let votableType):
            return ["vote[votable_id]": votableId, "vote[votable_type]": votableType]
        case .createProduct(let product):
            if product.comment == "" {
                return ["photo_tags[]":product.photo_tags , "post[posted]":"true"]
            }
            return ["photo_tags[]":product.photo_tags , "post[posted]":"true","comment[comment]": product.comment,"comment[user_id]": product.usreid]
        case .notifications(let id):
            return ["page":id]

        case .retrieveComment(_, let page):
            return ["page": page]

        case .postComment(let postcomment):
            return ["comment[comment]":postcomment.comment,"post_id": postcomment.postId, "user_id": postcomment.userId, "comment[title]": postcomment.title]

        // for post product comment
        case .postProductComment(let postProductComment):
            return ["comment[comment]": postProductComment.comment, "product_id": postProductComment.productId, "user_id": postProductComment.userId, "comment[title]": postProductComment.commentTitle]

        case .updateClosetSize(let param,let value, _):

            return [param: value]

        case .addUserLocation(let latitude,let longitude, _):

            return ["user[estimated_latitude]":latitude ,"user[estimated_longitude]":longitude]

        case .userUpdateImage(_ , _):
            return ["":""]
            
        case .retrieveBrandProductFromBlueDots(_, _, let sharePostcode, let postId):
            return ["sc": sharePostcode, "p": postId]
        case .followUsersFromPhone(let arrPhone):
            return ["_json[]":arrPhone]
        case .followUsersFromFB(let fbAuthToken):
            return ["auth_token":fbAuthToken]
        // Suggestions
        case .getSuggestions(let phoneNumbers, let facebookToken):
            var suggestionsList: Dictionary<String, Any> = [:]
            if let phoneNumbers = phoneNumbers {
                suggestionsList["phone_numbers"] = phoneNumbers
            }
            if let facebookToken = facebookToken {
                suggestionsList["facebookToken"] = facebookToken
            }
            return [ "suggestions_list" : suggestionsList ]
            
        case .retailLocationNearProduct(let page, let locationId):
            return ["page":page, "retail_location_id": locationId]
            
        case .suggestionProduct(_ ,_ , let page):
            return ["page": page]
            
        case .googleCloudVisionMatch:
            return ["":""]
            
        case .googleCloudVisionMatchByBrandId(_, let brandId):
            return ["brand_id":brandId]
            
        case .createBrand(let brandName, _):
            return ["brand[name]":brandName,  "brand[provider]":"threadest"]
        case .brandFollower(_, let page):
            return ["page": page]
        case .giveAwayContestant(_, let productId):
            guard let currentUser = THUserManager.sharedInstance.currentUserdata() else {
                return [:]
            }
            return ["giveaway_contestant[user_id]":currentUser.userId,
                    "giveaway_contestant[product_id]": productId]
        default:
            return nil
        }
    }
    public var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }
    public var task: Task {
        switch self {
        case .createProduct:
            return .upload(.multipart([MultipartFormData(provider: .data(self.sampleData), name: "post[content]", fileName: "photo.png", mimeType: "image/png")]))
        case .userUpdateImage:
            return .upload(.multipart([MultipartFormData(provider: .data(self.sampleData), name: "user[profile_image]", fileName: "photo.png", mimeType: "image/png")]))
        case .googleCloudVisionMatch(let image):
            let imageData = UIImageJPEGRepresentation(image, 1)!
            let base64Encoding = imageData.base64EncodedString()
            //return .upload(.multipart([MultipartFormData(provider: .data(imageData.base64EncodedData()), name: "post[content]", fileName: "photo.png", mimeType: "image/png")]))
            
            return .upload(.multipart([MultipartFormData(provider: .data(self.sampleData), name: "post[content]", fileName: "photo.png", mimeType: "image/png")]))
            
        case .googleCloudVisionMatchByBrandId:
            return .upload(.multipart([MultipartFormData(provider: .data(self.sampleData), name: "post[content]", fileName: "photo.png", mimeType: "image/png")]))
            
        case .createBrand:
            return .upload(.multipart([MultipartFormData(provider: .data(self.sampleData), name: "brand[brand_logo]", fileName: "photo.png", mimeType: "image/png")]))
        default:
            return .request
        }
    }
    public var sampleData: Data {

        switch self {
        case .createProduct(let product):
            if let image_data = UIImagePNGRepresentation(product.product_image){
                return image_data
            }
            return Data()
        case  .userUpdateImage(_ , let image):
            if let image_data = UIImagePNGRepresentation(image){
                return image_data
            }
            return Data()
        case .googleCloudVisionMatch(let image):
            if let image_data = UIImagePNGRepresentation(image) {
                return image_data
            }
            return Data()
        case .googleCloudVisionMatchByBrandId(let image, _):
            if let image_data = UIImagePNGRepresentation(image) {
                return image_data
            }
            return Data()
        case .createBrand(_, let image):
            if let image_data = UIImagePNGRepresentation(image) {
                return image_data
            }
            return Data()
        default:
            return Data()
        }

    }
}
