//
//  AlgoliaAPI.swift
//  Threadest
//
//  Created by Jaydeep on 21/11/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation
import Moya
import KeychainSwift

let endpointAlgoliaClosure = { (target: Algolia) -> Endpoint<Algolia> in
    let defaultEndpoint = MoyaProvider.defaultEndpointMapping(for: target)
    let headerFields = ["X-Algolia-API-Key" : "e6f2342e6ec558541f9a97fa63a1f5a5", "X-Algolia-Application-Id" : "ESC3OCSKMP"]
    return defaultEndpoint.adding(newHTTPHeaderFields: headerFields)
}

let ThreadestAlgoliaProvider = MoyaProvider<Algolia>(endpointClosure: endpointAlgoliaClosure)

public enum Algolia {
    // Authentication
    case popularSearch(String)
   
}

extension Algolia: TargetType {
    public var baseURL: URL { return URL(string: "https://analytics.algolia.com/1/")! }
    public var path: String {
        switch self {
        // Authentication
        case .popularSearch(let indices):
            debugPrint("searches/\(indices)/popular")
            return "searches/\(indices)/popular"
        }
    }
        
    public var method: Moya.Method {
        switch self {
        case .popularSearch:
             return .get
            
        default:
            return .get
        }
    }
    
    public var parameters: [String : Any]? {
        switch self {
        
        default:
            return nil
        }
    }
    
    public var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        return .request
    }
}
