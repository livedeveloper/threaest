//
//  UILabel.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 10/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    func addCharactersSpacing(spacing: CGFloat) {
        if let currentAttributedText = self.attributedText {
            let attributedString = NSMutableAttributedString(attributedString: currentAttributedText)
            attributedString.addCharacterSpacing(characterSpacing: spacing)
            self.attributedText = attributedString
        }
    }
    
    func addLineSpacing(spacing: CGFloat, textAlignment: NSTextAlignment = .natural) {
        if let currentAttributedText = self.attributedText {
            let attributedString = NSMutableAttributedString(attributedString: currentAttributedText)
            attributedString.addLineSpacing(lineSpacing: spacing, textAlignment: textAlignment)
            self.attributedText = attributedString
        }
    }
}
