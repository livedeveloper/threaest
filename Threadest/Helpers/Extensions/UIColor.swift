//
//  UIColor.swift
//  Threadest
//
//  Created by Jaydeep on 22/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation
import UIKit

extension UIColor{
    class func hexa (_ hexStr : NSString, alpha : CGFloat) -> UIColor {

        let realHexStr = hexStr.replacingOccurrences(of: "#", with: "")
        let scanner = Scanner(string: realHexStr as String)
        var color: UInt32 = 0
        if scanner.scanHexInt32(&color) {
            let r = CGFloat((color & 0xFF0000) >> 16) / 255.0
            let g = CGFloat((color & 0x00FF00) >> 8) / 255.0
            let b = CGFloat(color & 0x0000FF) / 255.0
            return UIColor(red:r,green:g,blue:b,alpha:alpha)
        } else {
            print("invalid hex string", terminator: "")
            return UIColor.white
        }
    }

    class func custColor(r:CGFloat,g:CGFloat,b:CGFloat,a:CGFloat) -> UIColor{
        return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
    }

    class func AppFontBlack() -> UIColor {
        return UIColor(red: 74/255.0, green: 74/255.0, blue: 74/255.0, alpha: 1)
    }
    

    class var richElectricBlue: UIColor {
        return UIColor(red:0.09, green:0.55, blue:0.80, alpha:1.00)
    }

    class var darkGrayFollowButton: UIColor {
        return UIColor(red: 62/255.0, green: 62/255.0, blue: 62/255.0, alpha: 1)
    }

    class var richBlueButton: UIColor {
        return UIColor(red: 0, green: 138/255.0, blue: 208/255.0, alpha: 1)
    }
    
    class var blueTabbar: UIColor {
        return UIColor(red: 23/255.0, green: 141/255.0, blue: 205/255.0, alpha: 1)
    }

    class var lightGrayButton: UIColor {
        return UIColor(red: 119/255.0, green: 119/255.0, blue: 119/255.0, alpha: 1)
    }

    class var trasperantBlack: UIColor {
        return UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)
    }
    
    class var spiroDiscoBall: UIColor {
        return UIColor(red:0.11, green:0.68, blue:0.97, alpha:1.00)
    }

    class var trasperantRed: UIColor {
        return UIColor.red.withAlphaComponent(0.6)
    }
    
    class var trasperantblue: UIColor {
        return UIColor(red: 23/255.0, green: 141/255.0, blue: 205/255.0, alpha: 0.6)
    }
    
    class var closetBlack: UIColor {
        return UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
    }
    
    
}
