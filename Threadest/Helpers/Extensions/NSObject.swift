//
//  NSObject.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 05/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation
import UIKit

extension NSObject {
    class var className: String {
        if let className = String(describing: self).components(separatedBy: ".").last {
            return className
        } else {
            fatalError("There's something wrong with your code, please check it before trying again!")
        }
    }
}

extension NSObject {
    static func nib() -> UINib {
        let nib = UINib(nibName: self.nibName(), bundle: nil)
        return nib
    }
    
    static func nibName() -> String {
        return String.init(describing: self.self)
    }
    
    static func cellIdentifier() -> String {
        return String.init(describing: self.self)
    }
}

public extension RawRepresentable where Self.RawValue == Int {
    public static var allCases: [Self] {
        var i = -1
        return Array( AnyIterator{i += 1;return self.init(rawValue: i)})
    }
}

extension NSObject {
    var toJSON: String? {
        do {
            let data = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
            return String(data: data, encoding: String.Encoding.utf8)
        } catch {
            return nil
        }
    }
}
