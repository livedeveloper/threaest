//
//  NSMutableAttributedString.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 10/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation
import UIKit

extension NSMutableAttributedString {
    func addLineSpacing(lineSpacing: CGFloat, textAlignment: NSTextAlignment = .natural) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = textAlignment
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.lineBreakMode = .byWordWrapping
        
        addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, string.characters.count))
    }
    
    func addCharacterSpacing(characterSpacing: CGFloat) {
        addAttribute(NSKernAttributeName, value: characterSpacing, range: NSMakeRange(0, string.characters.count))
    }
}
