//
//  UIFont.swift
//  Threadest
//
//  Created by Jaydeep on 02/04/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    
    public class func AppFontRegular(_ fontSize: CGFloat) -> UIFont {
        return UIFont(name: "SFUIText-Regular", size: fontSize)!
    }
    
    public class func AppFontSemiBold(_ fontSize: CGFloat) -> UIFont {
        return UIFont(name: "SFUIText-Semibold", size: fontSize)!
    }
    
    public class func AppFontBold(_ fontSize: CGFloat) -> UIFont {
        return UIFont(name: "SFUIText-Bold", size: fontSize)!
    }
}
