//
//  UIViewController.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 15/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import Result

extension UIViewController {
    func showAlert(withMessage message: String) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        present(alertController, animated: true, completion: nil)
    }
    
    func showAlert(title:String, message: String, okClick: @escaping ()->Void ){
        
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertVC.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) -> Void in
            okClick()
        }))
        
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func showAlert(title:String, message: String, buttonText: String, okClick: @escaping ()->Void) {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertVC.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: nil))
        alertVC.addAction(UIAlertAction(title: buttonText, style: UIAlertActionStyle.default, handler: { (action) -> Void in
            okClick()
        }))
        
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = true
        self.view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}

@available(iOS 10.0, *)
extension UIImage {
    convenience init(view: UIView) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, false, 0.0)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: (image?.cgImage!)!)
       // self.init(CGImage: image?.CGImage)
    }
    
    class func imageWithView(view: UIView) -> UIImage {
        //var image = UIImage()
        //autoreleasepool(invoking: { () -> () in
           // UIGraphicsBeginImageContext(view.bounds.size)
        UIGraphicsBeginImageContextWithOptions(view.frame.size, false, 0.0)
            let renderer = UIGraphicsImageRenderer(size: view.bounds.size)
            let image = renderer.image { ctx in
                view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
            }
      //  })
        UIGraphicsEndImageContext()
        return image
    }
}
