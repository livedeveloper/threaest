//
//  JSONConvertible.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 13/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol JSONConvertible {
    associatedtype JsonConvertible
    
    static func fromJSON(json: JSON) -> JsonConvertible
    func toJSON() -> JSON
}

extension JSONConvertible {
    func toJSON() -> JSON {
        let jsonDict = [String : Any]()
        
        return JSON(jsonDict)
    }
}
