//
//  THProfilePostsManager.swift
//  Threadest
//
//  Created by Jaydeep on 01/04/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

let IS_TESTS_retrieveSuggestions = true

typealias RetrieveProfilePostsCompletion = (_ error: Error?) -> Void
typealias RetrieveSuggestionsCompletion = (_ response: SuggestionsListResponse?, _ error: Error?) -> Void

class THProfilePostsManager: NSObject {
    static let sharedInstance = THProfilePostsManager()
    
    var profile:Profile?
    var currentUserProfile:Profile?
    
    var profileFollowers = [User]()
    var profileFollowing = [User]()
    var profileBrandFollowing = [Brand]()
    var profilePosts = [SocialInteraction]()
    var currentUserProfilePosts = [SocialInteraction]()
    var notificationPost : SocialInteraction?
    var suggestions = [Suggestion]()
    
    var didLoadData = false
    var pullToRefresh = false
    var page = 1
    
    var currentUserdidLoadData = false
    var currentUserPullToRefresh = false
    var currentUserPage = 1
    
    var didLoadDataBrand = false
    var pageBrand = 1
    var pageFollowing = 1
    var pageFollowers = 1
    
    func retrieveProfilePosts(with id: Int, completion: @escaping RetrieveProfilePostsCompletion) {
        THNetworkManager.sharedInstance.retrieveProfilePosts(for: id, page: page) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            var resultSocialInteractions = [SocialInteraction]()
            
            let profileClosetdata = json["data"]
            let profilePostsArray = json["posts"]
            
            if profileClosetdata != JSON.null {
                //Profile
                self.profile = Profile.fromJSON(json: profileClosetdata)
                self.profile?.currentUserFollowing = json["current_user_following"].boolValue
                
                
                if profilePostsArray != JSON.null {
                    if let profilePostsArray = profilePostsArray.array {
                        resultSocialInteractions = profilePostsArray.map {return SocialInteraction.fromJSON(json: $0)}
                    }
                }
                
                if self.pullToRefresh {
                    self.pullToRefresh = false
                    self.profilePosts = [SocialInteraction]()
                }
                
                if resultSocialInteractions.count > 0 {
                    // At least one of the results was correctly parsed, hence we received a result
                    self.page += 1
                    self.profilePosts.append(contentsOf: resultSocialInteractions)
                    self.didLoadData = true
                    completion(nil)
                } else {
                    self.didLoadData = false
                    // TODO: Add corresponding error
                    completion(nil)
                }
                
            } else {
                // TODO: Add corresponding error
                completion(nil)
            }
        }
    }
    
    func retrieveCurrentProfilePosts(with id: Int, completion: @escaping RetrieveProfilePostsCompletion) {
        THNetworkManager.sharedInstance.retrieveProfilePosts(for: id, page: currentUserPage) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            var resultSocialInteractions = [SocialInteraction]()
            
            let profileClosetdata = json["data"]
            let profilePostsArray = json["posts"]
            
            if profileClosetdata != JSON.null {
                //Profile
                self.currentUserProfile = Profile.fromJSON(json: profileClosetdata)
                self.currentUserProfile?.currentUserFollowing = json["current_user_following"].boolValue
                
                
                if profilePostsArray != JSON.null {
                    if let profilePostsArray = profilePostsArray.array {
                        resultSocialInteractions = profilePostsArray.map {return SocialInteraction.fromJSON(json: $0)}
                    }
                }
                
                if self.currentUserPullToRefresh {
                    self.currentUserPullToRefresh = false
                    self.currentUserProfilePosts = [SocialInteraction]()
                }
                
                if resultSocialInteractions.count > 0 {
                    // At least one of the results was correctly parsed, hence we received a result
                    self.currentUserPage += 1
                    self.currentUserProfilePosts.append(contentsOf: resultSocialInteractions)
                    self.currentUserdidLoadData = true
                    completion(nil)
                } else {
                    self.currentUserdidLoadData = false
                    // TODO: Add corresponding error
                    completion(nil)
                }
                
            } else {
                // TODO: Add corresponding error
                completion(nil)
            }
        }
    }
    
    func retrieveNotificationPost(with id: Int, completion: @escaping RetrieveProfilePostsCompletion) {
        THNetworkManager.sharedInstance.retrieveCurrentPost(for: id) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            if json["status"].boolValue == false {
                self.notificationPost = nil
                completion(nil)
                return
            }
            
            let profileNotificationdata = json["data"]
            
            
            if profileNotificationdata != JSON.null
                
            {
                self.notificationPost = SocialInteraction.fromJSON(json: profileNotificationdata)
                
                completion(error)
            } else {
                // TODO: Add corresponding error
                completion(nil)
            }
        }
    }
    
    func deletePost(with id: Int, completion: @escaping RetrieveProfilePostsCompletion) {
        THNetworkManager.sharedInstance.deleteCurrentPost(for: id) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            let deletePostdata = json["data"]
            
            
            if deletePostdata != JSON.null
                
            {
                completion(error)
            } else {
                // TODO: Add corresponding error
                completion(nil)
            }
        }
    }
    
    func retrieveProfileFollowers(with userId: Int,completion:@escaping RetrieveShippedOrderCompletion) {
        THNetworkManager.sharedInstance.retrieveUserFollowers(for: userId, page: pageFollowers) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            var resultProfileFollowers = [User]()
            
            let ordersData = json
            if json["status"].boolValue {
                if ordersData != JSON.null {
                    //Closet Category
                    if let orderArray = ordersData["data"].array {
                        resultProfileFollowers = orderArray.map { return User.fromJSON(json: $0) }
                    }
                    
                    if resultProfileFollowers.count > 0 {
                        // At least one of the results was correctly parsed, hence we received a result
                        self.pageFollowers += 1
                        self.profileFollowers.append(contentsOf: resultProfileFollowers)
                        //self.didLoadDataBrand = true
                        completion(nil)
                    } else {
                        //self.didLoadDataBrand = false
                        // TODO: Add corresponding error
                        completion(nil)
                    }
                }
            }
            completion(error)
        }
        
    }
    
    func retrieveProfileFollowing(with userId: Int,completion:@escaping RetrieveShippedOrderCompletion) {
        THNetworkManager.sharedInstance.retrieveUserFollowing(for: userId, page: pageFollowing) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            var resultProfileFollowing = [User]()
            
            let ordersData = json
            if json["status"].boolValue {
                if ordersData != JSON.null {
                    //Closet Category
                    if let orderArray = ordersData["data"].array {
                        resultProfileFollowing = orderArray.map { return User.fromJSON(json: $0) }
                    }
                }
                
                if resultProfileFollowing.count > 0 {
                    // At least one of the results was correctly parsed, hence we received a result
                    self.pageFollowing += 1
                    self.profileFollowing.append(contentsOf: resultProfileFollowing)
                    //self.didLoadDataBrand = true
                    completion(nil)
                } else {
                    //self.didLoadDataBrand = false
                    // TODO: Add corresponding error
                    completion(nil)
                }
            }
            completion(error)
        }
        
    }
    
    func retrieveProfileBrandFollowing(with userId: Int,completion:@escaping RetrieveShippedOrderCompletion) {
        THNetworkManager.sharedInstance.retrieveUserBrandFollowing(for: userId, page: pageBrand) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            var resultBrandFollowing = [Brand]()
            
            let ordersData = json
            
            if json["status"].boolValue {
                
                if ordersData != JSON.null {
                    //Closet Category
                    if let orderArray = ordersData["data"].array {
                        resultBrandFollowing = orderArray.map { return Brand.fromJSON(json: $0) }
                    }
                }
                
                if resultBrandFollowing.count > 0 {
                    // At least one of the results was correctly parsed, hence we received a result
                    self.pageBrand += 1
                    self.profileBrandFollowing.append(contentsOf: resultBrandFollowing)
                    self.didLoadDataBrand = true
                    completion(nil)
                } else {
                    self.didLoadDataBrand = false
                    // TODO: Add corresponding error
                    completion(nil)
                }
            } else {
                completion(error)
            }
        }
        
    }
    
    func retrieveSuggestions(phoneNumbers: [String]?, facebookToken: String?, completion:@escaping RetrieveSuggestionsCompletion) {
        if IS_TESTS_retrieveSuggestions {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(10), execute: {
                let url = Bundle.main.url(forResource: "Suggestions", withExtension: "json")!
                let data = try! Data(contentsOf: url)
                let json = JSON(data: data)
                let response = SuggestionsListResponse.fromJSON(json: json)
                self.suggestions = response.suggestions
                completion(response, nil)
            })
        } else {
            THNetworkManager.sharedInstance.retriveSuggestions(phoneNumbers: phoneNumbers, facebookToken: facebookToken) { (json, error) in
                guard let json = json else {
                    completion(nil, error)
                    return
                }
                
                let response = SuggestionsListResponse.fromJSON(json: json)
                self.suggestions = response.suggestions
                completion(response, error)
            }
        }
    }
}
