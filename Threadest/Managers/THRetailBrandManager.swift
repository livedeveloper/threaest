//
//  THRetailBrandManager.swift
//  Threadest
//
//  Created by Jaydeep on 25/09/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

typealias RetrieveLocationBrandsCompletion = (_ error: Error?) -> Void

class THRetailBrandManager: NSObject {
    static let sharedInstance = THRetailBrandManager()
    
    var RetailProducts = [ClosetCategory]()
    var didLoadData = false
    var page = 1
    
    func retrievingRetailNearByProduct(for Locationid: Int,completion: @escaping RetrieveLocationBrandsCompletion) {
        THNetworkManager.sharedInstance.retrieveRetailLocationNearByProduct(for: Locationid, page: page) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            var resultNearbyProducts = [ClosetCategory]()

            if json["status"].boolValue {
                let retailNearByProductData = json["data2"]
                if retailNearByProductData != JSON.null {
                    if let productArray = retailNearByProductData.array {
                        resultNearbyProducts = productArray.map { return ClosetCategory.fromJSON(json: $0) }
                    }
                    
                    if resultNearbyProducts.count > 0 {
                        // At least one of the results was correctly parsed, hence we received a result
                        self.page += 1
                        self.RetailProducts.append(contentsOf: resultNearbyProducts)
                        
                        self.didLoadData = true
                        completion(nil)
                    } else {
                        self.didLoadData = false
                        // TODO: Add corresponding error
                        completion(nil)
                    }
                } else {
                    let error = NSError(domain: "Error", code: 0, userInfo: nil)
                    completion(error)
                }
            } else {
                let error = NSError(domain: "Error", code: 0, userInfo: nil)
                completion(error)
            }
        }
    }
    

}
