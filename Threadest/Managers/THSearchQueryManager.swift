//
//  THSearchQueryManager.swift
//  Threadest
//
//  Created by Synnapps on 07/04/2017.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import AlgoliaSearch
import ObjectMapper

class THSearchQueryManager: NSObject {
    
    var index:Index?
    var query:Query?
    var client:Client?
    
    var indexQueryUser:IndexQuery?
    var indexQueryProduct:IndexQuery?
    var indexQueryBrand:IndexQuery?
    var indexQuerySong: IndexQuery?
    
    static let kStringConstantAll:String = "All"
    static let kStringConstantProduct:String = "Product"
    static let kStringConstantUser:String = "User"
    static let kStringConstantBrand:String = "Brand"
    static let kStringConstantSong:String = "Song"
    
    override init() {
        super.init()
        self.initializeAlgoliaSearchKey()
        self.query = Query()
        self.indexQueryUser = IndexQuery(indexName: "User", query: Query(query: ""))
        self.indexQueryProduct = IndexQuery(indexName: "Product", query: Query(query: ""))
        self.indexQueryBrand = IndexQuery(indexName: "Brand", query: Query(query: ""))
        self.indexQuerySong = IndexQuery(indexName: "Song", query: Query(query: ""))
    }
    
    private func initializeAlgoliaSearchKey() {
        let apiID = Bundle.main.object(forInfoDictionaryKey: "AlgoliaAppID")
        let apiKey = Bundle.main.object(forInfoDictionaryKey: "AlgoliaAppKey")
        self.client = Client(appID: apiID as! String, apiKey: apiKey as! String)
    }
    
    public func queryResultsAll(search:String, userPage:Bool, productPage:Bool, brandPage:Bool, completionBlock: @escaping (Any,UInt,UInt,UInt) -> () = { _ in }) {
        
        self.indexQueryUser?.query.query = search;
        self.indexQueryProduct?.query.query = search;
        self.indexQueryBrand?.query.query = search;
        self.query?.filters = ""
        var indexQueries:[IndexQuery]?
        
        if(userPage==true&&productPage==true&&brandPage==true) {
            let queries = [
                self.indexQueryUser!,
                self.indexQueryProduct!,
                self.indexQueryBrand!
            ]
            indexQueries = queries
        }
        
        self.client!.multipleQueries(indexQueries!, completionHandler: { (content, error) -> Void in
            if error == nil {
                do {
                    print("Result: \(content!)")
                    guard let hits = content!["results"] as? [[String: AnyObject]] else { return }
                    
                    let user = hits[0]
                    let product = hits[1]
                    let brand = hits[2]
                    
                    guard let userHits = user["hits"] as? [[String: AnyObject]] else { return }
                    guard let nbPagesUser = user["nbPages"] as? UInt else { return }
                    guard let productHits = product["hits"] as? [[String: AnyObject]] else { return }
                    guard let nbPagesUProduct = product["nbPages"] as? UInt else { return }
                    guard let brandHits = brand["hits"] as? [[String: AnyObject]] else { return }
                    guard let nbPagesBrand = brand["nbPages"] as? UInt else { return }
                    
                    print("User: \(userHits)")
                    print("Product: \(productHits)")
                    print("Brand: \(brandHits)")
                    
                    let userHitsData =  try JSONSerialization.data(withJSONObject: userHits, options:JSONSerialization.WritingOptions.prettyPrinted)
                    let userConvertedString = String(data: userHitsData, encoding: String.Encoding.utf8)
                    let productHitsData =  try JSONSerialization.data(withJSONObject: productHits, options:JSONSerialization.WritingOptions.prettyPrinted)
                    let productConvertedString = String(data: productHitsData, encoding: String.Encoding.utf8)
                    let brandHitsData =  try JSONSerialization.data(withJSONObject: brandHits, options:JSONSerialization.WritingOptions.prettyPrinted)
                    let brandConvertedString = String(data: brandHitsData, encoding: String.Encoding.utf8)
                    
                    let userList: Array<SearchUser> = Mapper<SearchUser>().mapArray(JSONString:userConvertedString!)!;
                    let productList: Array<SearchProduct> = Mapper<SearchProduct>().mapArray(JSONString:productConvertedString!)!;
                    let brandList: Array<SearchBrand> = Mapper<SearchBrand>().mapArray(JSONString:brandConvertedString!)!;
                    
                    var genralListArray:[Any] = []
                    
                    for (index, element) in userList.enumerated() {
                        print("Item \(index): \(element)")
                        genralListArray.append(element)
                    }
                    for (index, element) in productList.enumerated() {
                        print("Item \(index): \(element)")
                        genralListArray.append(element)
                    }
                    for (index, element) in brandList.enumerated() {
                        print("Item \(index): \(element)")
                        genralListArray.append(element)
                    }
                    
                    genralListArray.shuffle()
                    
                    completionBlock(genralListArray,nbPagesUser,nbPagesUProduct,nbPagesBrand)
                    
                } catch let error as NSError {
                    print(error.localizedDescription)
                    completionBlock([],0,0,0)
                }
            } else if let error = error {
                print(error.localizedDescription)
                completionBlock([],0,0,0)
            }
        })
    }

    public func queryResults(search: String, searchType:String, completionBlock: @escaping (Any,UInt) -> () = { _ in }) {
        //  Load all objects from index "Product" matched with textField query
        let index = self.client!.index(withName: searchType)
        self.query?.query = search
        if searchType == THSearchQueryManager.kStringConstantBrand {
            
        }
        self.query?.hitsPerPage = 15
        index.search(self.query!, completionHandler: { (content, error) -> Void in
            if error == nil {
                do {
                    guard let hits = content!["hits"] as? [[String: AnyObject]] else { return }
                    guard let nbPages = content!["nbPages"] as? UInt else { return }
                    let hitsData =  try JSONSerialization.data(withJSONObject: hits, options:JSONSerialization.WritingOptions.prettyPrinted)
                    let convertedString = String(data: hitsData, encoding: String.Encoding.utf8)
                    
                    switch(searchType) {
                    case THSearchQueryManager.kStringConstantAll:
                        return
                    case THSearchQueryManager.kStringConstantUser:
                        let list: Array<SearchUser> = Mapper<SearchUser>().mapArray(JSONString:convertedString!)!;
                        completionBlock(list,nbPages)
                        break
                    case THSearchQueryManager.kStringConstantProduct:
                        let list: Array<SearchProduct> = Mapper<SearchProduct>().mapArray(JSONString:convertedString!)!;
                        completionBlock(list,nbPages)
                        break
                    case THSearchQueryManager.kStringConstantBrand:
                        let list: Array<SearchBrand> = Mapper<SearchBrand>().mapArray(JSONString:convertedString!)!;
                        completionBlock(list,nbPages)
                        break
                    case THSearchQueryManager.kStringConstantSong:
                        let list: Array<SearchLyrics> = Mapper<SearchLyrics>().mapArray(JSONString:convertedString!)!;
                        completionBlock(list,nbPages)
                    default:
                        return
                    }
                    
                } catch let error as NSError {
                    print(error.localizedDescription)
                    completionBlock([],0)
                }
            } else if let error = error {
                print(error.localizedDescription)
                completionBlock([],0)
            }
        })
    }
    
    
    
    public func filtersQueryREsult(search: String, brandID:Int, minPrice:Int, maxPrice:Int, closetCategory: String, gender: String, completionBlock: @escaping (Any,UInt) -> () = { _ in }) {
        let index = self.client!.index(withName: "Product")
        self.query?.query = search
        var filterString:String? = nil
        if(brandID != -1) {
            filterString = String(format: "brand_id:%i", brandID)
        }
        
        if minPrice != -1 {
            if(filterString != "") {
                filterString = String(format: "%@ AND current_price>%i AND current_price<%i",filterString! , minPrice, maxPrice)
            } else {
                filterString = String(format: "current_price>%i AND current_price<%i", minPrice, maxPrice)
            }
        }
        /*if(filterString != nil) {
            filterString = String(format: "%@ AND current_price>%i AND current_price<%i",filterString! , minPrice, maxPrice)
        } else {
            filterString = String(format: "current_price>%i AND current_price<%i", minPrice, maxPrice)
        }*/
        
        if closetCategory != "" {
            filterString = String(format: "%@ AND closet_category:%@", filterString!, closetCategory)
        }
        
        if gender != "" {
            filterString = String(format: "%@ AND gender_classification:%@", filterString!, gender)
        }
        query?.filters = filterString
        
        self.query?.hitsPerPage = 15
        
        index.search(self.query!, completionHandler: { (content, error) -> Void in
            if error == nil {
                do {
                    guard let hits = content!["hits"] as? [[String: AnyObject]] else { return }
                    guard let nbPages = content!["nbPages"] as? UInt else { return }
                    let hitsData =  try JSONSerialization.data(withJSONObject: hits, options:JSONSerialization.WritingOptions.prettyPrinted)
                    let convertedString = String(data: hitsData, encoding: String.Encoding.utf8)
                    let list: Array<SearchProduct> = Mapper<SearchProduct>().mapArray(JSONString:convertedString!)!;
                    completionBlock(list,nbPages)
                } catch let error as NSError {
                    print(error.localizedDescription)
                    completionBlock([],0)
                }
            } else if let error = error {
                print(error.localizedDescription)
                completionBlock([],0)
            }
        })
    }
    
    public func filterQueryBrandName(searchText: String, brandName: Array<String>, category: String, gender: String, minPrice: Int, maxPrice: Int, completionBlock: @escaping (Any,UInt) -> () = { _ in }) {
        let index = self.client!.index(withName: "Product")
        self.query?.query = ""
        self.query?.query = searchText
        var filterString:String = ""
        
        if brandName.count != 0 {
            filterString = "("
            for ( i, brand) in brandName.enumerated() {
                filterString = String(format: " %@ brand_name: '%@' %@", filterString ,brand, brandName.count-1 > i ? "OR":")")
            }
        }
        
        if minPrice != -1 {
            if(filterString != "") {
                filterString = String(format: "%@ AND current_price>%i AND current_price<%i",filterString , minPrice, maxPrice)
            } else {
                filterString = String(format: "current_price>%i AND current_price<%i", minPrice, maxPrice)
            }
        }
        
        if filterString == "" {
            if category != "" {
                filterString = String(format: "tag_list:'%@'", category.lowercased())
            }
        } else {
            if category != "" {
                filterString = String(format: "%@ AND tag_list:'%@'", filterString, category.lowercased())
            }
        }
        
        if filterString == "" {
            if gender != "" {
                filterString = String(format: "gender_classification:%@", gender)
            }
        } else {
            if gender != "" {
                filterString = String(format: "%@ AND gender_classification:%@", filterString, gender)
            }
        }
        debugPrint(filterString)
        query?.filters = filterString
        
        self.query?.hitsPerPage = 15
        
        index.search(self.query!, completionHandler: { (content, error) -> Void in
            if error == nil {
                do {
                    guard let hits = content!["hits"] as? [[String: AnyObject]] else { return }
                    guard let nbPages = content!["nbPages"] as? UInt else { return }
                    let hitsData =  try JSONSerialization.data(withJSONObject: hits, options:JSONSerialization.WritingOptions.prettyPrinted)
                    let convertedString = String(data: hitsData, encoding: String.Encoding.utf8)
                    let list: Array<SearchProduct> = Mapper<SearchProduct>().mapArray(JSONString:convertedString!)!;
                    completionBlock(list,nbPages)
                } catch let error as NSError {
                    print(error.localizedDescription)
                    completionBlock([],0)
                }
            } else if let error = error {
                print(error.localizedDescription)
                completionBlock([],0)
            }
        })
    }
}

extension MutableCollection where Index == Int {
    /// Shuffle the elements of `self` in-place.
    mutating func shuffle() {
        // empty and single-element collections don't shuffle
        if count < 2 { return }
        
        for i in startIndex ..< endIndex - 1 {
            let j = Int(arc4random_uniform(UInt32(endIndex - i))) + i
            if i != j {
                swap(&self[i], &self[j])
            }
        }
    }
}
