//
//  THProductManager.swift
//  Threadest
//
//  Created by Synnapps on 11/04/2017.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

typealias ProdcutManagerProductCompletion = (_ product: Product?, _ error: Error?) -> Void

class THProductManager: NSObject {
    static let sharedInstance = THProductManager()
    func retrieveProduct(for id: Int, brand: Int, completion: @escaping ProdcutManagerProductCompletion) {
        THNetworkManager.sharedInstance.retrieveProduct(for: id,brand: brand) { (json, error) in
            guard let json = json else {
                completion(nil ,error)
                return
            }
            
            if json["status"].boolValue {
                completion(Product.fromJSON(json: json["data"]), nil)
            }
            
        }
    }
}
