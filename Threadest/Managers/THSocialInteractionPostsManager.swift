//
//  THSocialInteractionPostsManager.swift
//  Threadest
//
//  Created by Jaydeep on 28/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

typealias RetrieveSocialInterationCompletion = (_ error: Error?) -> Void

class THSocialInteractionPostsManager: NSObject {
    static let sharedInstance = THSocialInteractionPostsManager()
    
    var socialInteractionPosts = [SocialInteraction]()
    
    var likedPostPeopleList = [User]()
    var followPeopleList = [FollowPeople]()
    var currentFollowSuggestions = [FollowPeople]()
    var followedPeopleList = [FollowPeople]()
    
    var didLoadData = false
    var pullToRefresh = false
    var page = 1
    
    
    func retrieveSocialInteractionPosts(completion: @escaping RetrieveSocialInterationCompletion) {
        THNetworkManager.sharedInstance.retrieveSocialInteractionPosts(for: page) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            debugPrint(json)
            var resultSocialInteractions = [SocialInteraction]()
            
            
            let socialInteractionArray = json["data"]
            
            // for follow people array
            let followPeopleArray = json["data2"]
            
            // social interaction your feed
            if socialInteractionArray != JSON.null {
                if let socialInteractionArray = socialInteractionArray.array {
                    resultSocialInteractions = socialInteractionArray.map {return SocialInteraction.fromJSON(json: $0)}
                }
            }
            
            // follow people         
            
            if self.pullToRefresh {
                self.pullToRefresh = false
                self.socialInteractionPosts = [SocialInteraction]()
                
                if followPeopleArray != JSON.null {
                    if let followPeopleArray = followPeopleArray.array {
                        self.followPeopleList = followPeopleArray.map {return FollowPeople.fromJSON(json: $0)}
                    }
                }
                
            }else{
                
                if followPeopleArray != JSON.null {
                    if let followPeopleArray = followPeopleArray.array {
                        
                        for item in followPeopleArray {
                            
                            self.followPeopleList.append(FollowPeople.fromJSON(json: item))
                        }
                    }
                }
                
            }
            
            
            if resultSocialInteractions.count > 0 {
                // At least one of the results was correctly parsed, hence we received a result
                self.page += 1
                self.socialInteractionPosts.append(contentsOf: resultSocialInteractions)
                
                self.didLoadData = true
                completion(nil)
            } else {
                self.didLoadData = false
                // TODO: Add corresponding error
                completion(nil)
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                if json["status"].boolValue == false {
                    THUserManager.sharedInstance.logoutCurrentuser(isNavigateToLogin: false, completion: { (error) in
                        
                    })
                }
            }
        }
    }   
    
    func retrieveLikedPostUserList(for postId: Int, completion: @escaping RetrieveSocialInterationCompletion) {
        THNetworkManager.sharedInstance.retrievePostVotesCount(postId:postId) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            let voteList = json["data"]
             if json["status"].boolValue {
                if voteList != JSON.null {
                    if let voteListArray = voteList.array {
                        self.likedPostPeopleList = voteListArray.map {return User.fromJSON(json: $0)}
                    }
                }
            }
            completion(nil)
        }
    }
    
    func performFollowUser(for id:Int, completion: @escaping RetrieveSocialInterationCompletion) {
        THNetworkManager.sharedInstance.performFollowuser(for: id) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            if json["status"].boolValue {
                if let i = self.followPeopleList.index(where: { $0.id == id }) {
                    self.followPeopleList.remove(at: i)
                    
                }
                completion(error)
                
            }
        }
    }
    
    func performUnfollowUser(for id:Int, completion: @escaping RetrieveSocialInterationCompletion) {
        
        THNetworkManager.sharedInstance.performUnFollowuser(for: id) { (json, error) in
            
            guard let json = json else {
                completion(error)
                return
            }
            
            if json["status"].boolValue {
                completion(error)
                }
                
            }
        }
    
    func performRethreadPost(for postId: Int, completion: @escaping RetrieveSocialInterationCompletion) {
        THNetworkManager.sharedInstance.performRethreadPost(for: postId) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            if json["status"].boolValue {
                completion(error)
            }
        }
    }
    
    func performFollowBrand(for id:Int, completion: @escaping RetrieveSocialInterationCompletion) {
        THNetworkManager.sharedInstance.performFollowBrand(for: id) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            if json["status"].boolValue {

                completion(error)
                
            }
        }
    }
    
    func performunFollowBrand(for id:Int, completion: @escaping RetrieveSocialInterationCompletion) {
        THNetworkManager.sharedInstance.performunFollowBrand(for: id) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            if json["status"].boolValue {
                
                completion(error)
                
            }
        }
    }
}

    
    
    


