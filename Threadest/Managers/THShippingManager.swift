//
//  THShippingManager.swift
//  Threadest
//
//  Created by Nasrullah Khan on 10/04/2017.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

typealias ShippingManagerGenericCompletion = (_ error: Error?) -> Void

class THShippingManager: NSObject {
    
    static let sharedInstance = THShippingManager()
    var shippingAddress: ShippingAddress?
    
    // edit or save user shipping address
    func saveUserShippingAddress(with name:String, address1:String, address2:String, city:String, state:String, zipCode:String, completion: @escaping CartManagerGenericCompletion) {
        
        THNetworkManager.sharedInstance.addUserShippingAddress(for: name, address1: address1, address2: address2, city: city, state: state, zipCode: zipCode) { (json, error ) in
            guard let _ = json else {
                completion(error)
                return
            }
            
            completion(nil)
        }
    }
    
    //For user products in cart
    func retrieveShippingAddress(completion: @escaping CartManagerGenericCompletion) {
        
        THNetworkManager.sharedInstance.retrieveShippingAddress { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            let shippingAddressData = json["data"]
            
            if shippingAddressData != JSON.null {
                
                self.shippingAddress = ShippingAddress.fromJSON(json: shippingAddressData)
                completion(nil)
            } else {
                // TODO: Add corresponding error
                completion(nil)
            }
        }
    }
    
}
