//
//  THUserManager.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 13/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import KeychainSwift
import SwiftyJSON
import FacebookCore
import FacebookLogin
import UserNotifications
import FBSDKCoreKit

typealias UserManagerGenericCompletion = (_ error: Error?) -> Void
typealias UserManagerCheckIfUserExistsCompletion = (_ userExists: Bool?, _ error: Error?) -> Void
typealias UserManagerFacebookAuthCompletion = (_ userExists: Bool?, _ authToken: String, _ email: String, _ userID: String, _ gender: String, _ error: Error?) -> Void
typealias UserManagerUpvoteCompletion = (_ votedata: Vote?, _ error: Error?) -> Void

class THUserManager: NSObject {
    static let sharedInstance = THUserManager()

    //var currentUser: User?
    var searchedUser: User?
    var usersToFollow: [FollowPeople] = [FollowPeople]()
    var usersToFollowFromPhone: [FollowPeople] = [FollowPeople]()
    var usersToFollowFromFB: [FollowPeople] = [FollowPeople]()
    var usersToBrandFollowers: [FollowPeople] = [FollowPeople]()
    
    var brandToFollow: [Brand] = [Brand]()
    let userdefault = UserDefaults.standard
    var retailLocation: [RetailLocation] = [RetailLocation]()
    
    var inviteFriendNeededToGetPrice: Int = 0
    var loyaltyStatusBrand: Int = 0
    var pageUserFollow = 1
    var pageBrandFollow = 1

    fileprivate let authTokenKeychainKey = "authTokenKeychainKey"
    fileprivate let currentUserdefaultKey = "currentUserdefaultKey"
    fileprivate let followBrandUserPassedDefaultKey = "followBrandUserPassedDefaultKey"
    fileprivate let currentDeviceTokenKey = "currentDeviceTokenKey"
    fileprivate let isSignupKey = "isSignupKey"


    fileprivate let facebookAuthError = NSError(domain: "ThreadestDomain", code: -1, userInfo: [NSLocalizedDescriptionKey : "Facebook Auth Error"])
    
    func retrieveInviteFriendNeededToGetPrice(userId: Int, completion: @escaping UserManagerGenericCompletion) {
        THNetworkManager.sharedInstance.retrieveInviteFriend(userId: userId) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            if json["status"].boolValue {
                self.inviteFriendNeededToGetPrice = json["data"].intValue
                completion(nil)
            }
        }
    }
    
    func retrieveLoyaltyStatusBrand(brandId: Int,completion: @escaping UserManagerGenericCompletion) {
        THNetworkManager.sharedInstance.retrieveLoyaltyStatusWithBrand(brandId: brandId) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            if json["status"].boolValue {
                self.loyaltyStatusBrand = json["data"]["total_points"].intValue
                completion(nil)
            } else {
                self.loyaltyStatusBrand = 0
                completion(nil)
            }
        }
    }
    
   
    
    func performReedemFromPromoCode(userId: Int, promoCode: String, completion:@escaping UserManagerGenericCompletion) {
        THNetworkManager.sharedInstance.performReedemPromoCode(userId: userId, promoCode: promoCode) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            if json["status"].boolValue {
                //self.inviteFriendNeededToGetPrice = json["data"].intValue
                completion(nil)
            }
        }
        
    }

    func performFacebookAuth(forViewController viewController: UIViewController, completion: @escaping UserManagerFacebookAuthCompletion) {
        LoginManager().logIn([.publicProfile, .email,.userFriends], viewController: viewController) { (loginResult) in
            print(loginResult)

            switch loginResult {
            case .success(_, _, let accessToken):
                let request = GraphRequest(graphPath: "me", parameters: ["fields":"id, email, name, gender, picture"], accessToken: AccessToken.current, httpMethod: .GET, apiVersion: FacebookCore.GraphAPIVersion.defaultVersion)
                
                request.start { (response, result) in
                    switch result {
                    case .success(let value):
                        if let fbData = value.dictionaryValue {
                            if let emailAddress = fbData["email"] as? String {
                                var userId = ""

                                if let inUserId = accessToken.userId {
                                    userId = inUserId
                                }
                                GeneralMethods.sharedInstance.loadLoading(view: viewController.view)
                                

                                THUserManager.sharedInstance.performCheckIfAuthUserExists(with: emailAddress, authToken: accessToken.authenticationToken, userID: userId, completion: { (userExists, err) in
                                    GeneralMethods.sharedInstance.dismissLoading(view: viewController.view)
                                    // Request fails
                                    if let err = err {
                                        completion(nil, "", "", "", "", err)
                                        return
                                    }

                                    var gender = ""

                                    if let genderData = fbData["gender"] as? String {
                                        gender = genderData
                                    }

                                    // Request succeeds
                                    if let userExists = userExists {
                                        if userExists {
                                            // Regular flow -> user exists, go to closet - token saved in performCheck endpoint
                                            completion(true, accessToken.authenticationToken, emailAddress, userId, gender, nil)
                                        } else {
                                            // Redirect to register
                                            completion(false, accessToken.authenticationToken, emailAddress, userId, gender, nil)
                                        }
                                    } else {
                                        completion(nil, "", "", "", "", self.facebookAuthError)
                                    }
                                })
                            } else {
                                var userId = ""
                                
                                if let inUserId = accessToken.userId {
                                    userId = inUserId
                                }
                                
                                let authtoken = accessToken.authenticationToken
                                
                                var gender = ""
                                
                                if let genderData = fbData["gender"] as? String {
                                    gender = genderData
                                }
                                
                                if userId != "" && authtoken != "" && gender != "" {
                                    completion(nil, authtoken, "", userId, gender, self.facebookAuthError)
                                } else {
                                    completion(nil, "", "", "", "", self.facebookAuthError)
                                }
                                
                            }
                        } else {
                            completion(nil, "", "", "", "", self.facebookAuthError)
                        }
                    case .failed(let error):
                        completion(nil, "", "", "", "", error)
                    }
                }
            default:
                completion(nil, "", "", "", "", self.facebookAuthError)
                return
            }
        }
    }

    func createFacebookUser(with authToken: String, email em: String, userID uid: String, gender gd: String, registration rg: Registration, completion: @escaping UserManagerGenericCompletion) {
        THNetworkManager.sharedInstance.createFacebookUser(for: authToken, email: em, userId: uid, gender: gd, registration: rg) { (json, error) in
            guard let json = json else {
                completion(error)

                return
            }

            let loginNetworkResponse = LoginNetworkResponse.fromJSON(json: json)

            if loginNetworkResponse.hasSucceeded {
                self.setFollowBrandUserPassed(strValue: "true")

                self.saveAuthToken(authToken: authToken)
                self.saveCurrentUser(currentuser: json["data"])
                completion(nil)
            } else {
                let error = NSError(domain: "Error", code: 0, userInfo: [NSLocalizedDescriptionKey : loginNetworkResponse.message])
                completion(error)
            }
        }
    }

    // TODO: Add all user related functions
    func registerUser(with registration: Registration, tempAccount: Bool, completion: @escaping UserManagerGenericCompletion) {
        THNetworkManager.sharedInstance.performRegistration(withRegistration: registration, tempAccount: tempAccount) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }

            let registerResponse = RegisterNetworkResponse.fromJSON(json: json)

            if registerResponse.hasSucceeded {
                self.setFollowBrandUserPassed(strValue: "true")

                self.saveAuthToken(authToken: registerResponse.authToken)
                self.saveCurrentUser(currentuser: json["data"])
                completion(nil)
            } else {
                let error = NSError(domain: "Error", code: 0, userInfo: [NSLocalizedDescriptionKey : registerResponse.message])
                completion(error)
            }
        }
    }


    func loginUser(withEmail email: String, withPassword password: String, completion: @escaping UserManagerGenericCompletion) {
        let login = Login(email: email, password: password)

        THNetworkManager.sharedInstance.performLogin(withLogin: login) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }

            let loginResponse = LoginNetworkResponse.fromJSON(json: json)

            if loginResponse.hasSucceeded {
                self.setFollowBrandUserPassed(strValue: "true")

                self.saveAuthToken(authToken: loginResponse.authToken)
                self.saveCurrentUser(currentuser: json["data"])

                completion(nil)
            } else {
                let error = NSError(domain: "Error", code: 0, userInfo: [NSLocalizedDescriptionKey : loginResponse.message])
                completion(error)
            }
        }
    }

    func performUpdateUser(with id: Int, user: User, completion: @escaping
        UserManagerGenericCompletion) {
        THNetworkManager.sharedInstance.performUpdateUser(with: id, user: user) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            if json["status"].boolValue {
                self.saveCurrentUser(currentuser: json["data"])
                completion(nil)
            } else {
                let error = NSError(domain: "Error", code: 0, userInfo: [NSLocalizedDescriptionKey : json["message"].stringValue])
                completion(error)
            }
        }
    }
    
    func performUpdateUserLocaltion(with latitude: Double, logitude: Double, completion:@escaping UserManagerGenericCompletion) {
        THNetworkManager.sharedInstance.updateUserLocationRetailStore(for: latitude, longitude: logitude) { (json, error) in
            guard let json = json else {
                completion(error)
                self.retailLocation = [RetailLocation]()
                return
            }
            debugPrint(json)
            if json["status"].boolValue {
                let retailLocationArray = json["data"]
                // social interaction your feed
                if retailLocationArray != JSON.null {
                    if let retaillocationArray = retailLocationArray.array {
                        self.retailLocation = retaillocationArray.map {return RetailLocation.fromJSON(json: $0)}
                    }
                    completion(nil)
                }
            } else {
                self.retailLocation = [RetailLocation]()
                let error = NSError(domain: "Error", code: 0, userInfo: [NSLocalizedDescriptionKey : json["message"].stringValue])
                completion(error)
            }
        }
    }
    
    func performUpdateStylePreferenceTaglist(with id: Int, tagList: String, completion: @escaping UserManagerGenericCompletion ) {
        THNetworkManager.sharedInstance.updateStylePreferenceTagList(for: id, tagList: tagList) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            if json["status"].boolValue {
                self.saveCurrentUser(currentuser: json["data"])
                completion(nil)
            } else {
                let error = NSError(domain: "Error", code: 0, userInfo: [NSLocalizedDescriptionKey : json["message"].stringValue])
                completion(error)
            }
        }
    }

    func performCheckIfAuthUserExists(with email: String, authToken: String, userID: String, completion:@escaping UserManagerCheckIfUserExistsCompletion) {
        THNetworkManager.sharedInstance.performCheckIfAuthUserExists(with: email) { (json, error) in
            guard let json = json else {
                completion(nil, error)
                return
            }

            if json["user_exists"].boolValue {
                self.setFollowBrandUserPassed(strValue: "true")

                self.saveAuthToken(authToken: authToken)
                self.saveCurrentUser(currentuser: json["data"])

                THNetworkManager.sharedInstance.saveNewAuthToken(for: authToken, email: email, userId: userID, completion: { (json2, error2) in
                    guard let _ = json2 else {
                        completion(nil, error)
                        return
                    }

                    completion(true, nil)
                })
            } else {
                completion(false, nil)
            }
        }
    }
    
    func performUpdateUserAccount(for id: Int, user: User, completion: @escaping UserManagerGenericCompletion) {
        THNetworkManager.sharedInstance.updateUserAccount(for: id, user: user) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            if json["status"].boolValue {
                self.saveCurrentUser(currentuser: json["data"])
                completion(nil)
            } else {
                let error = NSError(domain: "Error", code: 0, userInfo: [NSLocalizedDescriptionKey : json["message"].stringValue])
                completion(error)
            }
        }
    }

    func performUpvote(for id: Int, votableId: Int, votableType: String, completion: @escaping UserManagerUpvoteCompletion) {
        THNetworkManager.sharedInstance.performUpvote(for: id, voterId: votableId, voterType: votableType) { (json, error) in
            guard let json = json else {
                completion(nil ,error)
                return
            }

            if json["status"].boolValue {
                completion(Vote.fromJSON(json: json["data"]), nil)
            }

        }
    }
    
    func performUserBrandFollowers(brandId: Int,completion: @escaping UserManagerGenericCompletion) {
        THNetworkManager.sharedInstance.performBrandFollowers(brandId: brandId, page: pageBrandFollow) { (json, error) in
            debugPrint(json)
            guard let json = json else {
                completion(error)
                return
            }
            
            var resultFollowPeople = [FollowPeople]()
            
            if json["status"].boolValue {
                if let userstofollow = json["data"].array {
                    resultFollowPeople = userstofollow.map { return FollowPeople.fromJSON(json: $0) }
                }
                //completion(nil)
            }
            else {
                completion(error)
            }
            
            if resultFollowPeople.count > 0 {
                // At least one of the results was correctly parsed, hence we received a result
                self.pageBrandFollow += 1
                self.usersToBrandFollowers = resultFollowPeople
                //self.usersToBrandFollowers.append(contentsOf: resultFollowPeople)
                completion(nil)
            } else {
                self.pageBrandFollow += 1
                self.usersToBrandFollowers = [FollowPeople]()
                // TODO: Add corresponding error
                completion(nil)
            }
            
        }
    }


    // retrieving list of users to follow
    func performUsertoFollow(completion: @escaping UserManagerGenericCompletion) {
        THNetworkManager.sharedInstance.performUsersToFollow(page: pageUserFollow) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            var resultFollowPeople = [FollowPeople]()

            if json["status"].boolValue {
                if let userstofollow = json["data"].array {
                    resultFollowPeople = userstofollow.map { return FollowPeople.fromJSON(json: $0) }
                }
                completion(nil)
            }
            else {
                completion(error)
            }
            
            if resultFollowPeople.count > 0 {
                // At least one of the results was correctly parsed, hence we received a result
                self.pageUserFollow += 1
                self.usersToFollow.append(contentsOf: resultFollowPeople)
                
                completion(nil)
            } else {
                // TODO: Add corresponding error
                completion(nil)
            }

        }
    }

    // retrieving list of brands to follow
    func retrieveBrandsToFollow(completion: @escaping UserManagerGenericCompletion) {
        THNetworkManager.sharedInstance.retrieveBrandsToFollow { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }

            if json["status"].boolValue {
                if let brandtofollow = json["data"].array {
                    self.brandToFollow = brandtofollow.map { return Brand.fromJSON(json: $0) }
                }
                completion(nil)
            }
            else {
                completion(error)
            }
        }
    }

    
    // retrieving list of users to follow  that are in your phone book
    func performUsertoFollowFromPhoneBook(for phoneList: String ,completion: @escaping UserManagerGenericCompletion) {
        THNetworkManager.sharedInstance.followUserFromPhoneBook(for:phoneList, completion: { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            if json["status"].boolValue {
                if let userstofollow = json["data"].array {
                    self.usersToFollowFromPhone = userstofollow.map { return FollowPeople.fromJSON(json: $0) }
                }
                completion(nil)
            }
            else {
                completion(error)
            }
            
        })
    }
    
    // retrieving list of users to follow  that are in your facebook
    func performUsertoFollowFromFB(for fbAuthToken: String ,completion: @escaping UserManagerGenericCompletion) {
    
        THNetworkManager.sharedInstance.followUserFromFB(for:fbAuthToken, completion: { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            if json["status"].boolValue {
                if let userstofollow = json["data"].array {
                    self.usersToFollowFromFB = userstofollow.map { return FollowPeople.fromJSON(json: $0) }
                }
                completion(nil)
            }
            else {
                completion(error)
            }
            
        })
    }
    
    //follow all users
    func performFollowAllUser(completion: @escaping UserManagerGenericCompletion) {
        THNetworkManager.sharedInstance.performFollowAllUser { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            if json["status"].boolValue {
                completion(nil)
            }
            else {
                completion(error)
            }
        }
    }

    //follow all brands
    func performFollowAllBrands(completion: @escaping UserManagerGenericCompletion) {
        THNetworkManager.sharedInstance.performFollowAllBrands { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            if json["status"].boolValue {
                completion(nil)
            }
            else {
                completion(error)
            }
        }
    }

    func performFollowUser(with id:Int, completion: @escaping UserManagerGenericCompletion) {
        THNetworkManager.sharedInstance.performFollowuser(for: id, completion: { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            if json["status"].boolValue {
                completion(nil)
            }
            else {
                completion(error)
            }
        })
    }

    //follow brand
    func performFollowBrand(with id:Int, completion: @escaping UserManagerGenericCompletion) {
        THNetworkManager.sharedInstance.performFollowBrand(with: id) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            if json["status"].boolValue {
                completion(nil)
            }
            else {
                completion(error)
            }
        }
    }

    //unfollow brand
    func performUnfollowBrand(with id:Int, completion: @escaping UserManagerGenericCompletion) {
        THNetworkManager.sharedInstance.performUnfollowBrand(with: id) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            if json["status"].boolValue {
                completion(nil)
            }
            else {
                completion(error)
            }
        }
    }
    
    //Notify when stock available
    func performNotifyWhenStockAvailable(with brandId: Int, productId: Int, completion: @escaping UserManagerGenericCompletion) {
        THNetworkManager.sharedInstance.performNotifyMeWhenInStock(brandId: brandId, productId: productId) { (json, error) in
            debugPrint(json)
            guard let json = json else {
                completion(error)
                return
            }
            if json["status"].boolValue {
                completion(nil)
            }
            else {
                completion(error)
            }
         }
    }
    
    
    func perfomCurrentInfluencerScore(with userId:Int) {
        THNetworkManager.sharedInstance.retrieveCurrentInfluencer(with: userId) { (json, error) in
            guard let json = json else {
                return
            }
            
            let currentInfluencerScore = json["data"]["influencer_score"].intValue
            if currentInfluencerScore > 0 {
                self.userdefault.set(currentInfluencerScore, forKey: userDefaultKey.currentInfluencerScoreDefault.rawValue)
                self.userdefault.synchronize()
            }
        }
    }



    fileprivate func saveAuthToken(authToken: String) {
        let keychain = KeychainSwift()
        keychain.set(authToken, forKey: authTokenKeychainKey)
    }

    func authToken() -> String {
        let keychain = KeychainSwift()
        return keychain.get(authTokenKeychainKey) ?? ""
    }

    func saveCurrentUser(currentuser: JSON) {
        let currentUserString = currentuser.description
        userdefault.set(currentUserString, forKey: currentUserdefaultKey)
        
        
        let influncerScore = User.fromJSON(json: JSON(parseJSON: currentUserString )).influencerScore
        userdefault.set(influncerScore, forKey: userDefaultKey.influencerScoreDefault.rawValue)
        userdefault.synchronize()

        // We need to sync the new users with the old device token
        if self.hasRegisteredForPushNotifications() {
            THUserManager.sharedInstance.requestPushNotificationPermissions(completion: {granted in
                debugPrint(granted)
                })
        }
    }

    func currentUserdata() -> User? {
        if let currentUserString = userdefault.object(forKey: currentUserdefaultKey) {
            return User.fromJSON(json: JSON(parseJSON: currentUserString as! String))
        }
        else {
            return nil
        }
    }

    func setFollowBrandUserPassed(strValue: String) {
        userdefault.set(strValue, forKey: followBrandUserPassedDefaultKey)
        userdefault.set(true, forKey: isSignupKey)
        userdefault.synchronize()
    }

    func getFollowBrandUserPassed() -> String {
        return userdefault.object(forKey: followBrandUserPassedDefaultKey) as! String
    }

    func requestPushNotificationPermissions(completion: @escaping (Bool) -> Void) {
        // iOS 10 support
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in
                completion(granted)
            }
            UIApplication.shared.registerForRemoteNotifications()
        }
        // iOS 9 support
        else if #available(iOS 9, *) {
           completion(true)
        UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
    }

    func hasRegisteredForPushNotifications() -> Bool {
        return UIApplication.shared.isRegisteredForRemoteNotifications
    }

    func syncPNToken(token: String, completion: GenericNetworkCompletion?) {
        if token.characters.count > 0 {
            let keychain = KeychainSwift()

            if let _ = currentUserdata() {
                if let deviceToken = keychain.get(currentDeviceTokenKey) {
                    if deviceToken != token {
                        THNetworkManager.sharedInstance.updateDeviceToken(for: deviceToken, newToken: token, completion: { (json, error) in
                            guard let json = json else {
                                completion?(nil, error)
                                return
                            }

                            if json["status"].boolValue {
                                keychain.set(token, forKey: self.currentDeviceTokenKey)
                            }
                        })
                    }
                } else {
                    THNetworkManager.sharedInstance.saveDeviceToken(for: token) { (json, error) in
                        guard let json = json else {
                            completion?(nil, error)
                            return
                        }

                        if json["status"].boolValue {
                            keychain.set(token, forKey: self.currentDeviceTokenKey)
                        }
                    }
                }
            } else {
                THNetworkManager.sharedInstance.saveDeviceToken(for: token) { (json, error) in
                    guard let json = json else {
                        completion?(nil, error)
                        return
                    }

                    if json["status"].boolValue {
                        keychain.set(token, forKey: self.currentDeviceTokenKey)
                    }
                }
            }
        }
    }

    func logoutCurrentuser(isNavigateToLogin: Bool, completion: @escaping UserManagerGenericCompletion) {
        let keychain = KeychainSwift()

        if let deviceToken = keychain.get(currentDeviceTokenKey) {
            THNetworkManager.sharedInstance.deleteDeviceToken(for: deviceToken, completion: { (json, error) in
                guard let _ = json else {
                    completion(error)
                    
                    return
                }

                keychain.delete(self.currentDeviceTokenKey)
                
                self.performLogout(isNavigateToLogin: isNavigateToLogin)
                THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts = [SocialInteraction]()
                completion(nil)
            })
        } else {
            performLogout(isNavigateToLogin: isNavigateToLogin)
        }
    }

    fileprivate func performLogout(isNavigateToLogin: Bool) {
        let keychain = KeychainSwift()
        keychain.delete(authTokenKeychainKey)
        keychain.clear()

        userdefault.removeObject(forKey: currentUserdefaultKey)
        userdefault.removeObject(forKey: "cartCount")
        userdefault.removeObject(forKey: userDefaultKey.influencerScoreDefault.rawValue)
        userdefault.removeObject(forKey: userDefaultKey.currentInfluencerScoreDefault.rawValue)
        userdefault.removeObject(forKey: userDefaultKey.lastSharePostDateDefault.rawValue)
        userdefault.removeSuite(named: userDefaultKey.ARONOFF.rawValue)
        userdefault.removeObject(forKey: userDefaultKey.tapMeShowFirstDefault.rawValue)
        userdefault.removeObject(forKey: userDefaultKey.bulletinVideoTutorial.rawValue)
        userdefault.synchronize()

        appdelegate.startScreenView(isNavigateToLogin: isNavigateToLogin)
        self.setFollowBrandUserPassed(strValue: "false")
    }
}

@available(iOS 10.0, *)
extension THUserManager: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        completionHandler(
            [UNNotificationPresentationOptions.alert,
             UNNotificationPresentationOptions.sound,
             UNNotificationPresentationOptions.badge])
    }
}
