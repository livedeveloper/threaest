//
//  THCartManager.swift
//  Threadest
//
//  Created by Nasrullah Khan on 05/04/2017.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

typealias CartManagerGenericCompletion = (_ error: Error?) -> Void

class THCartManager: NSObject {

    static let sharedInstance = THCartManager()
    var cart: Cart?
    
    // add product to cart
    func addToCart(with productID:Int, productVariationId:Int, postId: Int,shareCode: String = "", completion: @escaping CartManagerGenericCompletion) {
        THNetworkManager.sharedInstance.performAddToCart(for: productID, productVarID: productVariationId, postId: postId, shareCode: shareCode) { (json, error) in
            debugPrint(json)
            guard let _ = json else {
                completion(error)
                return
            }
            if let cartCount = UserDefaults.standard.value(forKey: "cartCount")
            {
                let currentCount = (cartCount as! Int) + 1
                UserDefaults.standard.set(currentCount, forKey: "cartCount")
                UserDefaults.standard.synchronize()
                
            }
            
            completion(nil)
        }
    }
    
    //For user products in cart
    func retrieveCartProducts(with lat:Double, long:Double, completion: @escaping CartManagerGenericCompletion) {
        
        THNetworkManager.sharedInstance.retrieveCartProducts(for: lat, long: long) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            let cartData = json["data"]
            
            if cartData != JSON.null {
                // Cart
                self.cart = Cart.fromJSON(json: cartData)
                completion(nil)
            } else {
                // TODO: Add corresponding error
                completion(nil)
            }
        }
    }
    
    //For user products in cart
    func deleteCartProduct(with cartItemID:Int, completion: @escaping CartManagerGenericCompletion) {
        
        THNetworkManager.sharedInstance.deleteProductFromCart(for: cartItemID) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            let cartData = json["data"]
            
            if cartData != JSON.null {
                
                if let cartCount = UserDefaults.standard.value(forKey: "cartCount")
                {
                    let currentCount = (cartCount as! Int) - 1
                    UserDefaults.standard.set(currentCount, forKey: "cartCount")
                    UserDefaults.standard.synchronize()
                }
                
                // Cart
                self.cart = Cart.fromJSON(json: cartData)
                completion(nil)
            } else {
                // TODO: Add corresponding error
                completion(nil)
            }
        }
    }
}
