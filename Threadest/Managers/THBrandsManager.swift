//
//  THBrandsManager.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 21/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

typealias BrandManagerCartBrandCompletion = (_ cartBrand: CartBrand?, _ error: Error?) -> Void

typealias RetrieveAllBrandsCompletion = (_ error: Error?) -> Void

class THBrandsManager: NSObject {
    static let sharedInstance = THBrandsManager()
    
    var brands = [Brand]()
    var products = [ClosetCategory]()
    var suggestionProduct = [ClosetCategory]()
    
    var didLoadData = false
    var current_user_following_brand = false
    var page = 1
    
    func retrieveBrand(for id: Int, isFromCloset:Bool,completion: @escaping BrandManagerCartBrandCompletion) {
        THNetworkManager.sharedInstance.retrieveBrand(for: id, page: page, isFromCloset:isFromCloset) { (json, error) in
            guard let json = json else {
                completion(nil ,error)
                return
            }
            
            var resultProducts = [ClosetCategory]()
            
            if json["status"].boolValue {
                //completion(CartBrand.fromJSON(json: json["data"]), nil)
            }
            
            let productData = json["data2"]
            self.current_user_following_brand = json["current_user_following_brand"].boolValue
            if productData != JSON.null {
                //Closet Category
                if let productArray = productData.array {
                    resultProducts = productArray.map { return ClosetCategory.fromJSON(json: $0) }
                }
            }
            
            if resultProducts.count > 0 {
                // At least one of the results was correctly parsed, hence we received a result
                self.page += 1
                self.products.append(contentsOf: resultProducts)
                
                self.didLoadData = true
                completion(CartBrand.fromJSON(json: json["data"]),nil)
            } else {
                self.didLoadData = false
                // TODO: Add corresponding error
                completion(CartBrand.fromJSON(json: json["data"]),nil)
            }
        }
    }
    
    func retrieveSuggestionOfProduct(brandId: Int, productId: Int, page: Int, completion: @escaping RetrieveAllBrandsCompletion) {
        THNetworkManager.sharedInstance.retrieveSuggestionProduct(for: brandId, productId: productId, page: page) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            if json["status"].boolValue {
                let suggestionProductData = json["data"]
                if suggestionProductData != JSON.null {
                    if let productArray = suggestionProductData.array {
                        self.suggestionProduct = productArray.map { return ClosetCategory.fromJSON(json: $0) }
                    }
                }
                completion(nil)
            }
        }
    }
}
