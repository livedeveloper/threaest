
//  THNetworkManager.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 09/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

typealias GenericNetworkCompletion = (_ json: JSON?, _ error: Error?) -> Void
typealias ProgressCompletion = (_ progress: Double) -> Void

class THNetworkManager: NSObject {
    static let sharedInstance = THNetworkManager()
    let concurrentRequestsQueue = OperationQueue()
    let dispatchQueue = DispatchQueue.global()

    static let cannotParseJSONError = NSError(domain: "LocalDomain", code: 0, userInfo: [NSLocalizedDescriptionKey : "Cannot parse JSON"])

    func performRegistration(withRegistration registration: Registration, tempAccount: Bool, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .createUser(registration, tempAccount)) { (json, error) in
            completion(json, error)
        }
    }

    func performCheckIfAuthUserExists(with emailAddress: String, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .checkIfAuthUserExists(emailAddress)) { (json, error) in
            completion(json, error)
        }
    }

    func performUsersToFollow(page: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .userToFollow(page)) { (json, error) in
            completion(json, error)
        }
    }
    
    func performBrandFollowers(brandId: Int, page: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .brandFollower(brandId, page)) { (json, error) in
            completion(json, error)
        }
    }

    func performFollowAllUser(completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .followAllUser) { (json, error) in
            completion(json, error)
        }
    }
    
    func performisFollowingBrand(brandId: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .isFollwingBrand(brandId)) { (json, error) in
            completion(json, error)
        }
    }

    func retrieveBrandsToFollow(completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .brandsToFollow) { (json, error) in
            completion(json, error)
        }
    }
    
    //Retrieve number of invite needed to get price
    func retrieveInviteFriend(userId: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .inviteFriend(userId)) { (json, error) in
            completion(json, error)
        }
    }
    
    func retrieveLoyaltyStatusWithBrand(brandId: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .checkLoyaltyStatusBrand(brandId)) { (json, error) in
            completion(json, error)
        }
    }
    
    //Reedeem from promocode
    func performReedemPromoCode(userId:Int, promoCode: String, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .redeemFromPromoCode(userId, promoCode)) { (json, error) in
            completion(json, error)
        }
    }
    
    //Retrieve Current influencer score
    func retrieveCurrentInfluencer(with userId: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .retrieveInfluencerScore(userId)) { (json, error) in
            completion(json, error)
        }
    }

    //follow brand
    func performFollowBrand(with id:Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .followBrand(id)) { (json, error) in
            completion(json, error)
        }
    }

    //unfollow brand
    func performUnfollowBrand(with id:Int , completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .unFollowBrand(id)) { (json, error) in
            completion(json, error)
        }
    }

    //follow all brands
    func performFollowAllBrands(completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .followAllBrands) { (json, error) in
            completion(json, error)
        }
    }
    
    //add product
    func performAddProduct(with addProduct: AddProduct, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .addProduct(addProduct)) { (json, error) in
            completion(json, error)
        }
    }

    func retrieveAllBrands(completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .retrieveBrands) { (json, error) in
            completion(json, error)
        }
    }

    func retrievePosts(for page: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .searchDiscoverPosts(page)) { (json, error) in
            completion(json, error)
        }
    }

    func saveNewAuthToken(for authToken: String, email em: String, userId uid: String, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .saveNewAuthToken(em, authToken, uid)) { (json, error) in
            completion(json, error)
        }
    }

    func createFacebookUser(for authToken: String, email em: String, userId uid: String, gender gd: String, registration reg: Registration, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .createFacebookUser(em, authToken, uid, gd, reg)) { (json, error) in
            completion(json, error)
        }
    }

    func saveDeviceToken(for deviceToken: String, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .sendPNToken(deviceToken), completion: completion)
    }

    func updateDeviceToken(for oldToken: String, newToken newTk: String, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .updatePNToken(oldToken, newTk), completion: completion)
    }

    func deleteDeviceToken(for deviceToken: String, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .deletePNToken(deviceToken), completion: completion)
    }

    //for retrieve Profilecloset data

    func retrieveProfileCloset(for id: Int, page: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .userCloset(id, page)) { (json, error) in
            completion(json, error)
        }
    }
    
    //for retrieve Profile closet favorite data
    func retrieveProfileClosetFavorite(for id: Int, page: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .closetFavorite(id, page)) { (json, error) in
            completion(json, error)
        }
    }
    

    //for retrieve ProfilePosts data
    func retrieveProfilePosts(for id: Int, page: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .userProfilePosts(id, page)) { (json, error) in
            completion(json, error)
        }
    }

    //for retrieve closetCategory data
    func retrieveClosetCategory(for id: Int, category: String, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .closetCategory(id, category)) { (json, error) in
            completion(json, error)
        }
    }


    //for add product to cart
    func performAddToCart(for productId: Int, productVarID: Int, postId: Int, shareCode: String, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .addToCart(productId, productVarID, postId, shareCode)) { (json, error) in
            completion(json, error)
        }
    }

    //  for retrieve products of cart
    func retrieveCartProducts(for lat: Double, long: Double, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .retrieveCartProducts(lat, long)) { (json, error) in

        completion(json, error)
        }
    }

    //  for delete product from cart
    func deleteProductFromCart(for cartItemID: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .deleteCartProduct(cartItemID)) { (json, error) in
            completion(json, error)
        }
    }

    //  for save user payment method
    func addUserPaymentMethod(for token:String, paymentType:String, brand:String, last4:Int, paymentObjectId:String, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .saveUserPaymentMethod(token, paymentType, brand, last4, paymentObjectId)) { (json, error) in
            completion(json, error)
        }
    }

    //  for retrieve user payment methods
    func retrieveUserPaymentMethods(completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .retrieveUserPaymentMethods) { (json, error) in
            completion(json, error)
        }
    }

    //  for edit user shipping address
    func addUserShippingAddress(for name:String, address1:String, address2:String, city:String, state:String, zipCode:String, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .saveUserShippingAddress(name, address1, address2, city, state, zipCode)) { (json, error) in
            completion(json, error)
        }
    }

    //  retrieve shipping address
    func retrieveShippingAddress(completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .retrieveUserShippingAddress) { (json, error) in
            completion(json, error)
        }
    }

    //  for apple pay instant individual item payment request
    func instantBuyNativeiOS(for productId:Int, productVarId:Int, tokenId: String, cardToken:String, firstName:String, lastName:String, streetAddress1:String, streetAddress2:String, state:String, city:String, zipCode:String,phone: String, email: String, postId: Int, shareCode: String,completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .instantBuyNativeiOS(productId, productVarId, tokenId, cardToken, firstName, lastName,streetAddress1, streetAddress2, state, city, zipCode, phone, email, postId, shareCode)) { (json, error) in
            completion(json, error)
        }
    }

    //  for apple pay payment from cart (total items) request
    func instantBuyFromCartNativeiOS(for tokenId:String, cardToken:String, firstName:String, lastName:String, streetAddress1:String, streetAddress2:String, state:String, city:String, zipCode:String,phone: String, email: String, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .instantBuyFromCartNativeiOS(tokenId, cardToken, firstName, lastName,streetAddress1, streetAddress2, state, city, zipCode, phone, email)) { (json, error) in
            completion(json, error)
        }
    }

    //  for tax
    func instantBuyTaxCalculation(for productId:Int, zipCode:String, city:String, state:String,completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .instantBuyTaxCalculations(productId, zipCode, city, state)) { (json, error) in
            completion(json, error)
        }
    }

    //  for tax in cart
    func instantBuyFromCartTaxCalculation(for zipCode:String, city:String, state:String,completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .instantBuyFromCartTaxCalculations(zipCode, city, state)) { (json, error) in
            completion(json, error)
        }
    }
    
    //for Create Native iOS order
    func createNativeiOSOrder(for payNowOrder: PayNowOrder, completion: @escaping GenericNetworkCompletion) {
            performAPIRequest(request: .createOrderNativeiOS(payNowOrder)) { (json, error) in
                completion(json, error)
        }
    }
    
    
    //Featured Feed By Category
    func retrieveFeaturedFeedByCategory(for page: Int, category: String, id: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .featuredFeedByCategory(page, category, id)) { (json, error) in
            completion(json, error)
        }
    }
    
    // update user current location retail store
    func updateUserLocationRetailStore(for latitude: Double, longitude: Double, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .retailLocation(latitude, longitude)) { (json, error) in
            completion(json, error)
        }
    }
    
    //retrieve hashTag Product
    func retrieveHashTagProduct(for hashTag: String, page: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .retrieveHashTagProduct(hashTag, page)) { (json, error) in
            completion(json, error)
        }
    }

    //for retrieve socialInteraction post data
    func retrieveSocialInteractionPosts(for page: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .socialInteractionPosts(page)) { (json, error) in
            completion(json, error)
        }
    }

    //for Follow user
    func performFollowuser(for id:Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .followUser(id)) { (json, error) in
            completion(json, error)
        }
    }


    // for upvote
    func performUpvote(for id:Int, voterId:Int, voterType: String, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .likeUpVotes(id, voterId, voterType)) { (json, error) in
            completion(json, error)
        }
    }

    // for rethread Post
    func performRethreadPost(for postId: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .rethreadPost(postId)) { (json, error) in
            completion(json, error)
        }
    }

    // for retrieve comments
    func retrieveComments(for postId: Int, page: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .retrieveComment(postId, page)) { (json, error) in
            completion(json, error)
        }
    }

    // for retrieve Product Comments
    func retrieveProductComments(for brandId: Int, productId: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .retrieveProductComment(brandId, productId)) { (json, error) in
            completion(json, error)
        }
    }

    // for post comment
    func performPostComment(for postcomment: postComment, completion:@escaping GenericNetworkCompletion) {
        performAPIRequest(request: .postComment(postcomment)) { (json, error) in
            completion(json, error)
        }
    }
    
    //for delete post comment
    func perfomDeletePostComment(with postId: Int, commentId: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .deletePostComment(postId, commentId)) { (json, error) in
            completion(json, error)
        }
    }

    //retrieve Brand Product
    func retrieveBrandProduct(for brandId: Int, productId: Int, geofence: Bool, shareCode: String,completion:@escaping GenericNetworkCompletion) {
        performAPIRequest(request: .retrieveBrandProduct(brandId, productId, geofence, shareCode)) { (json, error) in
            completion(json,error)
        }
    }
    
    //retrieve Brand Product from blue dots
    func retrieveBrandProductFromDots(for brandId: Int, productId: Int, sharePostCode: String, postId: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .retrieveBrandProductFromBlueDots(brandId, productId, sharePostCode, postId)) { (json, error) in
            completion(json, error)
        }
    }
    
    //retrieve retail location brand product
    func retrieveRetailLocationNearByProduct(for locationId: Int, page: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .retailLocationNearProduct(page, locationId)) { (json, error) in
            completion(json, error)
        }
    }
    
    //retrieve Suggestion product
    func retrieveSuggestionProduct(for brandId: Int, productId: Int, page: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .suggestionProduct(brandId, productId, page)) { (json, error) in
            completion(json, error)
        }
    }

    // for post Product Comment
    func performPostProductComment(for postProductComment: PostProductComment, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .postProductComment(postProductComment)) { (json, error) in
            completion(json, error)
        }
    }
    
    // for delte product Comment 
    func performDeleteProductComment(with brandId: Int, productId: Int, commentId: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .deleteProductComment(brandId, productId, commentId)) { (json, error) in
            completion(json, error)
        }
    }

    func performLogin(withLogin login: Login, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .login(login)) { (json, error) in
            completion(json, error)
        }
    }

    func performUpdateUser(with id: Int, user: User, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .updateUser(id, user)) { (json, error) in
            completion(json, error)
        }
    }

    //Notifications

    func retrieveNotifications(forPage:Int,completion: @escaping GenericNetworkCompletion) {

        performAPIRequest(request: .notifications(forPage)) { (json, error) in
            completion(json, error)
        }
    }
    func thEarlyAccessByEmail(for email: String, completion: @escaping GenericNetworkCompletion){
        performAPIRequest(request: .earlyAccess(email)) { (json, error) in

            completion(json, error)
        }
    }

    //Unfollow

    func performUnFollowuser(for id:Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .unFollowUser(id)) { (json, error) in
            completion(json, error)
        }
    }

    //Notification post

    func retrieveCurrentPost(for id: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .notificationPost(id)) { (json, error) in
            completion(json, error)
        }
    }

    // Create Product

    func createProduct(product: CreateProduct, completion: @escaping GenericNetworkCompletion,progressCompletion: @escaping ProgressCompletion){
        performUploadRequest(request: .createProduct(product), completion: { (json, error) in
            completion(json, error)
        }) { (progress) in
            progressCompletion(progress)
        }
    }
    
    func createBrand(brandName: String, brandImage: UIImage, completion: @escaping GenericNetworkCompletion, progressCompletion: @escaping ProgressCompletion) {
        performUploadRequest(request: .createBrand(brandName, brandImage), completion: { (json, error) in
            completion(json, error)
        }) { (progress) in
            progressCompletion(progress)
        }
    }
    
    //upload image to google cloud plateform
    func uploadImageToCloudPlateform(image: UIImage, completion: @escaping GenericNetworkCompletion, progressCompletion: @escaping ProgressCompletion) {
        performUploadRequest(request: .googleCloudVisionMatch(image), completion: { (json, error) in
            completion(json, error)
        }) { (progress) in
            progressCompletion(progress)
        }
    }
    
    //upload image to google cloud plateform
    func uploadImageBrandCloundPlateform(image: UIImage, brandId: Int, completion: @escaping GenericNetworkCompletion, progressCompletion: @escaping ProgressCompletion) {
        performUploadRequest(request: .googleCloudVisionMatchByBrandId(image, brandId), completion: { (json, error) in
            completion(json, error)
        }) { (progress) in
            progressCompletion(progress)
        }
    }

    // Update User Profile Image
    
    func updateUserImage(id:Int! , image : UIImage, completion: @escaping GenericNetworkCompletion,progressCompletion: @escaping ProgressCompletion){
        
        performUploadRequest(request: .userUpdateImage(id, image), completion: { (json, error) in
            completion(json, error)
        }) { (progress) in
            progressCompletion(progress)
        }
    }
    
    //Delete post

    func deleteCurrentPost(for id: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .deletePost(id)) { (json, error) in
            completion(json, error)
        }
    }

    func retrieveBrand(for id: Int, page: Int,isFromCloset: Bool ,completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .retrieveBrand(id, page, isFromCloset)) { (json, error) in
            completion(json, error)
        }
    }

    func retrieveProduct(for id: Int, brand:Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .retrieveProduct(id,brand)) { (json, error) in
            completion(json, error)
        }
    }

    // Update size

    func updateUserClosetSize(for id: Int,withParam: String,value: String,completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .updateClosetSize(withParam, value, id)) { (json, error) in
            completion(json, error)
        }
    }
    
    // Update Style Preference
    func updateStylePreferenceTagList(for id: Int, tagList: String, completion:
        @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .updateUserTagList(id, tagList)) { (json, error) in
            completion(json, error)
        }
    }
    
    //Update User Profilecompletion
    func updateUserAccount(for id: Int, user: User, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .updateUserAccount(id, user)) { (json, error) in
            completion(json, error)
        }
    }

    //Update location

    func updateUserLocation(for id: Int,latitude:Double,longitude:Double,completion: @escaping GenericNetworkCompletion)
    {
        performAPIRequest(request: .addUserLocation(latitude, longitude, id)) { (json, error) in
            completion(json, error)
        }
    }

    //for Follow brand
    func performFollowBrand(for id:Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .followBrand(id)) { (json, error) in
            completion(json, error)
        }
    }

    //for UnFollow brand
    func performunFollowBrand(for id:Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .unFollowBrand(id)) { (json, error) in
            completion(json, error)
        }
    }


    //User Purchases and orders

    func retrievePurchaseOrders(for id:Int, page: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .userPurchaseOrders(id, page)) { (json, error) in
            
            completion(json, error)
        }
    }
    
    //for retrieve Current User Followers
    func retrieveUserFollowers(for id: Int, page: Int,completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .userFollowers(id, page)) { (json, error) in
                completion(json, error)
            }
    }

    //for retrieve Current User Following
    func retrieveUserFollowing(for id: Int, page: Int,completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .userFollowing(id, page)) { (json, error) in
            completion(json, error)
        }
    }

    //for retrieve Current User Brand Following
    func retrieveUserBrandFollowing(for id: Int, page:Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .userBrandFollowing(id, page)) { (json, error) in
            completion(json, error)
        }
    }
    
    //for Follow users that are in your phone book
    func followUserFromPhoneBook(for phones: String, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .followUsersFromPhone(phones)) { (json, error) in
            completion(json, error)
        }
    }
    
    //for Follow users that are in your facebook
    func followUserFromFB(for fbAuthToken: String, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .followUsersFromFB(fbAuthToken)) { (json, error) in
            completion(json, error)
        }
    }
    
    //for retrieve the list of suggesstion users
    func retriveSuggestions(phoneNumbers: [String]?, facebookToken: String?, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .getSuggestions(phoneNumbers: phoneNumbers, facebookToken: facebookToken)) { (json, error) in
            completion(json, error)
        }
    }
    
    //for retrieve posts votes
    func retrievePostVotesCount(postId: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .postVotesCount(postId: postId)) { (json, error) in
            completion(json, error)
        }
    }
    
    //for reset password via email
    func retrieveResetViaEmail(email: String, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .resetPasswordEmail(email)) { (json, error) in
            completion(json, error)
        }
    }
    
    //for reset password via sms
    func retrieveResetViaSMS(email: String, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .resetPasswordSMS(email)) { (json, error) in
            completion(json, error)
        }
    }
    
    //for notify me when in stock
    func performNotifyMeWhenInStock(brandId: Int, productId: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .notifyMeWhenInStock(brandId, productId)) { (json, error) in
            completion(json, error)
        }
    }
    
    func retrieveCheckSMSCode(smsCode: String, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .checkSMSCode(smsCode)) { (json, error) in
            completion(json, error)
        }
    }
    
    func retrieveResetPassword(passwordToken: String, password: String, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .resetUserPassword(passwordToken, password)) { (json, error) in
            completion(json, error)
        }
    }
    
    func retrievePopularSearch(indices: String, completion: @escaping GenericNetworkCompletion) {
        performAlgoliaAPIRequest(request: .popularSearch(indices)) { (json, error) in
            completion(json, error)
        }
    }
    
    func retrieveGiveAwayConstestants(brandId: Int, ProductId: Int, completion: @escaping GenericNetworkCompletion) {
        performAPIRequest(request: .giveAwayContestant(brandId, ProductId)) { (json, error) in
            completion(json, error)
        }
    }
    
    private func performAPIRequest(request: Threadest, completion: @escaping GenericNetworkCompletion) {
        concurrentRequestsQueue.addOperation {
            ThreadestProvider.request(request) { (result) in
                switch result {
                case let .success(response):
                    do {
                        if let dictJSON = try response.mapJSON() as? [String : Any] {
                            completion(JSON(dictJSON), nil)
                        } else {
                            completion(nil, THNetworkManager.cannotParseJSONError)
                        }
                    } catch {
                        completion(nil, THNetworkManager.cannotParseJSONError)
                    }
                case let .failure(error):
                    completion(nil, error)
                }
            }
        }
    }
    
    
    private func performAlgoliaAPIRequest(request: Algolia, completion: @escaping GenericNetworkCompletion) {
        concurrentRequestsQueue.addOperation {
            ThreadestAlgoliaProvider.request(request) { (result) in
                switch result {
                case let .success(response):
                    do {
                        if let dictJSON = try response.mapJSON() as? [String : Any] {
                            completion(JSON(dictJSON), nil)
                        } else {
                            completion(nil, THNetworkManager.cannotParseJSONError)
                        }
                    } catch {
                        completion(nil, THNetworkManager.cannotParseJSONError)
                    }
                case let .failure(error):
                    completion(nil, error)
                }
            }
        }
    }

    
  

    private func performUploadRequest(request: Threadest, completion: @escaping GenericNetworkCompletion,progressCompletion: @escaping ProgressCompletion)
    {
        ThreadestProvider.request(request, queue: self.dispatchQueue, progress: { (ProgressResponse) in
            progressCompletion(ProgressResponse.progress)
            print("item posting")
        }, completion: { (result) in
            debugPrint(result)
            switch result {
            case let .success(response):
                do {
                    if let dictJSON = try response.mapJSON() as? [String : Any] {
                        completion(JSON(dictJSON), nil)
                    } else {
                        completion(nil, THNetworkManager.cannotParseJSONError)
                    }
                } catch {
                    completion(nil, THNetworkManager.cannotParseJSONError)
                }
            case let .failure(error):
                completion(nil, error)
            }
        })


    }
    
    

}

