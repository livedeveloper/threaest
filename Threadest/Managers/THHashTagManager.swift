//
//  THHashTagManager.swift
//  Threadest
//
//  Created by Jaydeep on 20/06/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

typealias RetrieveHashTagProductCompletion = (_ error: Error?) -> Void


class THHashTagManager: NSObject {
    static let sharedInstance = THHashTagManager()
    
    var socialInteractionPosts = [SocialInteraction]()
    
    var didLoadData = false
    public var page = 1
    var pullToRefresh = false
    
    func retriveHashTagManager(with hashTag: String, completion: @escaping RetrieveHashTagProductCompletion) {
        THNetworkManager.sharedInstance.retrieveHashTagProduct(for: hashTag, page: page) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            
            var resultpostData = [SocialInteraction]()
            
            let postTagData = json["data2"]
            
            if postTagData != JSON.null {
                //Closet Category
                if let postArray = postTagData.array {
                    resultpostData = postArray.map { return SocialInteraction.fromJSON(json: $0) }
                }
            }
            
            if self.pullToRefresh {
                self.pullToRefresh = false
                self.socialInteractionPosts = [SocialInteraction]()
            }
            
            if resultpostData.count > 0 {
                // At least one of the results was correctly parsed, hence we received a result
                self.page += 1
                self.socialInteractionPosts.append(contentsOf: resultpostData)
                
                self.didLoadData = true
                completion(nil)
            } else {
                self.didLoadData = false
                // TODO: Add corresponding error
                completion(nil)
            }
        }
    }
}
