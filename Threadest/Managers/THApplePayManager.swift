//
//  THApplePayManager.swift
//  Threadest
//
//  Created by Nasrullah Khan on 21/04/2017.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

typealias ApplePayManagerGenericCompletion = (_ error: Error?,_ message: String?) -> Void
typealias ApplePayTaxCalculationCompletion = (_ error: Error?, _ salesTax: Double?) -> Void

class THApplePayManager: NSObject {

    static let sharedInstance = THApplePayManager()

    func instantBuyNative(with productId:Int, productVarId:Int,tokenId: String, cardToken:String, firstName:String, lastName:String, streetAddress1:String, streetAddress2:String, state:String, city:String, zipCode:String, phone:String, email:String, postId: Int, shareCode: String = "",completion: @escaping ApplePayManagerGenericCompletion) {

        THNetworkManager.sharedInstance.instantBuyNativeiOS(for: productId, productVarId:productVarId,tokenId: tokenId, cardToken:cardToken, firstName:firstName, lastName:lastName, streetAddress1:streetAddress1, streetAddress2:streetAddress2, state:state, city:city, zipCode:zipCode, phone: phone, email: email, postId: postId, shareCode: shareCode) { (json, error ) in
            guard let json = json else {
                completion(error, "")
                return
            }

            if json["status"].boolValue == true {
                completion(nil, "")
            } else if json["status"].boolValue == false {
                completion(nil, json["message"].stringValue)
            }
            
        }
    }


    func instantBuyFromCartNative(with tokenId:String, cardToken:String, firstName:String, lastName:String, streetAddress1:String, streetAddress2:String, state:String, city:String, zipCode:String, phone:String, email:String, completion: @escaping ApplePayManagerGenericCompletion) {

        THNetworkManager.sharedInstance.instantBuyFromCartNativeiOS(for: tokenId, cardToken:cardToken, firstName:firstName, lastName:lastName, streetAddress1:streetAddress1, streetAddress2:streetAddress2, state:state, city:city, zipCode:zipCode, phone: phone, email: email) { (json, error ) in
            guard let json = json else {
                completion(error, "")
                return
            }
            if json["status"].boolValue == true {
                UserDefaults.standard.set(json["cart_item_count"].intValue, forKey: "cartCount")
                UserDefaults.standard.synchronize()
                completion(nil, "")
            } else if json["status"].boolValue == false {
                completion(nil, json["message"].stringValue)
            }
        }
    }

    func instantBuyTaxCalculation(with productId:Int, zipCode:String, city:String, state:String, completion: @escaping ApplePayTaxCalculationCompletion) {

        THNetworkManager.sharedInstance.instantBuyTaxCalculation(for: productId, zipCode: zipCode, city: city, state: state) { (json, error ) in

            guard let json = json else {
                completion(error , nil)
                return
            }

            let salesTax = json["data"].double
            completion(nil, salesTax)
        }
    }


    func instantBuyFromCartTaxCalculation(with zipCode:String, city:String, state:String, completion: @escaping ApplePayTaxCalculationCompletion) {

        THNetworkManager.sharedInstance.instantBuyFromCartTaxCalculation(for: zipCode, city: city, state: state) { (json, error ) in

            guard let json = json else {
                completion(error , nil)
                return
            }

            let salesTax = json["data"].double
            completion(nil, salesTax)
        }
    }


}
