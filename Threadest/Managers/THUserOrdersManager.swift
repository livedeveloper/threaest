//
//  THUserOrdersManager.swift
//  Threadest
//
//  Created by pratik on 13/05/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

typealias RetrieveShippedOrderCompletion = (_ error: Error?) -> Void

class THUserOrdersManager: NSObject {
    static let sharedInstance = THUserOrdersManager()
    
    var didLoadData = false
    var page = 1
    
    var userShippedProducts = [PurchasesOrders]()
    func retrieveShippedOrder(with userId: Int,completion:@escaping RetrieveShippedOrderCompletion) {
        
        THNetworkManager.sharedInstance.retrievePurchaseOrders(for: userId, page: page) { (json, error) in
            var resultShippedProduct = [PurchasesOrders]()
            guard let json = json else {
                completion(error)
                return
            }
            
            let ordersData = json
            if json["status"].boolValue {
                if ordersData != JSON.null {
                    //Closet Category
                    if let orderArray = ordersData["data"].array {
                        resultShippedProduct = orderArray.map { return PurchasesOrders.fromJSON(json: $0) }
                    }
                }
            }
            
            if resultShippedProduct.count > 0 {
                self.page += 1
                self.userShippedProducts.append(contentsOf: resultShippedProduct)
                self.didLoadData = true
                completion(nil)
            } else {
                self.didLoadData = false
                completion(nil)
            }
        }
        
    }
}
