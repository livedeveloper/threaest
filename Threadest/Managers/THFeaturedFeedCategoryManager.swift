//
//  THFeaturedFeedCategoryManager.swift
//  Threadest
//
//  Created by Jaydeep on 04/04/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

typealias RetrieveFeaturedFeedCategoryCompletion = (_ error: Error?) -> Void

class THFeaturedFeedCategoryManager: NSObject {
    static let sharedInstance = THFeaturedFeedCategoryManager()
    
    var shopCategory = [ClosetCategory]()
    
    var didLoadData = false
    public var page = 1
    var pullToRefresh = false
    
    func retrieveFeaturedFeedCategory(with category: String, id: Int,completion: @escaping RetrieveFeaturedFeedCategoryCompletion) {
        THNetworkManager.sharedInstance.retrieveFeaturedFeedByCategory(for: page, category: category, id: id) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            
            var resultshopCategory = [ClosetCategory]()
            
            let shopCategoryData = json["data"]
            
            if shopCategoryData != JSON.null {
                //Closet Category
                if let shopCategoryArray = shopCategoryData.array {
                    resultshopCategory = shopCategoryArray.map { return ClosetCategory.fromJSON(json: $0) }
                }
            }
            
            if self.pullToRefresh {
                self.pullToRefresh = false
                self.shopCategory = [ClosetCategory]()
            }
            
            if resultshopCategory.count > 0 {
                // At least one of the results was correctly parsed, hence we received a result
                self.page += 1
                self.shopCategory.append(contentsOf: resultshopCategory)
                
                self.didLoadData = true
                completion(nil)
            } else {
                self.didLoadData = false
                // TODO: Add corresponding error
                completion(nil)
            }
            
            
        }
    }
}
