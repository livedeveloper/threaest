//
//  THEarlyAccess.swift
//  Threadest
//
//  Created by jigar on 23/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

typealias EarlyAccessManagerGenericCompletion = (_ error: Error?) -> Void

class THEarlyAccess:NSObject{
    
    static let sharedInstance = THEarlyAccess()
    
    var subscriber : SubScriber?
    
    func subscribeToEarlyAccess(toEmail:String , completion: @escaping EarlyAccessManagerGenericCompletion) {
        
        THNetworkManager.sharedInstance.thEarlyAccessByEmail(for: toEmail) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            let earlyAccessResponse = THEarlyAccessResponse.fromJSON(json: json)
            
            if earlyAccessResponse.hasSucceeded {
                self.subscriber = SubScriber(userID: earlyAccessResponse.userID, earlyAccessPosition: earlyAccessResponse.earlyAccessPosition, email: earlyAccessResponse.email, shareUrl: earlyAccessResponse.shareUrl, thanksUrl: earlyAccessResponse.thanksUrl)
                
                completion(nil)
            } else {
                let error = NSError(domain: "Error", code: 0, userInfo: [NSLocalizedDescriptionKey : earlyAccessResponse.message])
                completion(error)
            }
        }

        
    }

    
}
