//
//  THPayNowOrderManager.swift
//  Threadest
//
//  Created by Jaydeep on 21/05/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

public struct PayNowOrder {
    let orderToken: String
    let orderStreetAddress1: String
    let orderStreetAddress2: String
    let orderZipCode: String
    let orderCity: String
    let orderState: String
    let orderFirstName: String
    let orderLastName: String
    let orderPhoneNumber: String
}

typealias CreateNativeiOSManagerGenericCompletion = (_ error: Error?, _ message: String?) -> Void

class THPayNowOrderManager: NSObject {
    static let sharedInstance = THPayNowOrderManager()

    func payNowCreateNativeiosOrder(with payNowOrder: PayNowOrder, completion: @escaping CreateNativeiOSManagerGenericCompletion) {
        THNetworkManager.sharedInstance.createNativeiOSOrder(for: payNowOrder) { (json, error) in
            guard let json = json else {
                completion(error, "")
                return
            }
            if json["status"].boolValue == true {
                UserDefaults.standard.set(json["cart_item_count"].intValue, forKey: "cartCount")
                UserDefaults.standard.synchronize()
                completion(nil, "")
            } else if json["status"].boolValue == false {
                completion(nil, json["message"].stringValue)
            }
        }
    }
}
