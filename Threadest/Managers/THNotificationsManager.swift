//
//  THNotificationsManager.swift
//  Threadest
//
//  Created by jigar on 29/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

typealias RetrieveNotificationsCompletion = (_ error: Error?) -> Void

class THNotificationsManager: NSObject {
    static let sharedInstance = THNotificationsManager()
    
    var notificationsData = [Notifications]()
    fileprivate var page = 1
    var didLoadData = false
   
    func retrieveNotifications(completion: @escaping RetrieveNotificationsCompletion) {
        THNetworkManager.sharedInstance.retrieveNotifications(forPage: page, completion: {(json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            var resultNotifications = [Notifications]()
            
            let notificationsData = json["data"]
            
            if notificationsData != JSON.null {
                if let notificationsArray = notificationsData.array {
                    resultNotifications = notificationsArray.map {return Notifications.fromJSON(json: $0)}
                }
                if(resultNotifications.count > 0) {
                    self.page += 1
                    self.notificationsData.append(contentsOf: resultNotifications)
                    self.didLoadData = true
                    completion(nil)
                } else {
                    self.didLoadData = false
                    // TODO: Add corresponding error
                    completion(nil)
                }
            } else {
                // TODO: Add corresponding error
                completion(nil)
            }
        })
    }
}
