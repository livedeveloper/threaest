//
//  THTransactionAndPayment.swift
//  Threadest
//
//  Created by Nasrullah Khan on 07/04/2017.
//  Copyright © 2017 JC. All rights reserved.
//

//
//  THCartManager.swift
//  Threadest
//
//  Created by Nasrullah Khan on 05/04/2017.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

typealias TransactionManagerGenericCompletion = (_ error: Error?) -> Void

class THTransactionManager: NSObject {
    
    static let sharedInstance = THTransactionManager()
    var paymentMethods = [PaymentMethod]()

    // add payment method
    func saveUserPaymentMethod(with token:String, paymentType:String, brand:String, last4:Int, paymentObjectId:String, completion: @escaping CartManagerGenericCompletion) {
        
        THNetworkManager.sharedInstance.addUserPaymentMethod(for: token, paymentType: paymentType, brand: brand, last4: last4, paymentObjectId: paymentObjectId) { (json, error) in
            
            guard let _ = json else {
                completion(error)
                return
            }
            
            completion(nil)
        }
    }
    
    func retrieUserPaymentMethods(with completion: @escaping CartManagerGenericCompletion) {
        
        THNetworkManager.sharedInstance.retrieveUserPaymentMethods { (json, error) in
            
            guard let json = json else {
                completion(error)
                return
            }
            
            let paymentMethodData = json["data"]
            
            if paymentMethodData != JSON.null {
                // payment Method
                if let paymentMethodArray = paymentMethodData.array {
                    self.paymentMethods = paymentMethodArray.map { return PaymentMethod.fromJSON(json: $0) }
                }
                
                completion(nil)
            } else {
                
                completion(nil)
            }
            
        }
    }
    

}
