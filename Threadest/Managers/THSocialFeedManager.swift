//
//  THSocialFeedManager.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 21/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

typealias RetrieveSocialFeedCompletion = (_ error: Error?) -> Void

class THSocialFeedManager: NSObject {
    static let sharedInstance = THSocialFeedManager()
    
    var brands = [Brand]()
    var shopCategories = [ShopCategory]()
    var products = [Product]()
    
    var didLoadData = false
    var pullToRefresh = false
    var page = 1
    
    func retrieveNextPosts(completion: @escaping RetrieveSocialFeedCompletion) {
        THNetworkManager.sharedInstance.retrievePosts(for: page) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            var resultBrands = [Brand]()
            var resultProducts = [Product]()
            var resultShopCategories = [ShopCategory]()
            
            // Brands
            let brandsArray = json["data"]
            
            if brandsArray != JSON.null {
                if let brandsArray = brandsArray.array {
                    resultBrands = brandsArray.map { return Brand.fromJSON(json: $0) }
                }
            }
            
            // Shop categories
            let shopCategoriesArray = json["data2"]
            
            if shopCategoriesArray != JSON.null {
                if let shopCategoriesArray = shopCategoriesArray.array {
                    resultShopCategories = shopCategoriesArray.map { return ShopCategory.fromJSON(json: $0) }
                }
            }
            
            // Products
            let productsArray = json["data3"]
            
            if productsArray != JSON.null {
                if let productsArray = productsArray.array {
                    resultProducts = productsArray.map { return Product.fromJSON(json: $0) }
                }
            }
            
            if self.pullToRefresh {
                self.pullToRefresh = false
                self.shopCategories = [ShopCategory]()
            }
            
            if resultBrands.count > 0 ||
                resultProducts.count > 0 ||
                resultShopCategories.count > 0 {
                // At least one of the results was correctly parsed, hence we received a result
                self.page += 1
                self.brands.append(contentsOf: resultBrands)
                self.products.append(contentsOf: resultProducts)
                self.shopCategories.append(contentsOf: resultShopCategories)
                
                self.didLoadData = true
                completion(nil)
            } else {
                self.didLoadData = false
                // TODO: Add corresponding error
                completion(nil)
            }
        }
    }}
