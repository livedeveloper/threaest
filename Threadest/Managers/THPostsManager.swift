//
//  THPostsManager.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 22/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

typealias RetrievePostsCompletion = (_ error: Error?) -> Void

class THPostsManager: NSObject {
    static let sharedInstance = THPostsManager()
    
    var posts = [ThreadestItem]()
    fileprivate var page = 1
    
    func retrieveNextPosts(completion: @escaping RetrievePostsCompletion) {
        THNetworkManager.sharedInstance.retrievePosts(for: page) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            let threadestItem = ThreadestItem.fromJSON(json: json)
            
            self.page += 1
            self.posts.append(threadestItem)
            
            completion(nil)
        }
    }
}
