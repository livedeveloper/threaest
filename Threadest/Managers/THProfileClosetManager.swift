//
//  THProfileClosetManager.swift
//  Threadest
//
//  Created by Jaydeep on 25/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

typealias RetrieveProfileClosetCompletion = (_ error: Error?) -> Void

class THProfileClosetManager: NSObject {
    static let sharedInstance = THProfileClosetManager()

    var closetItems = [ClosetItem]()
    var profile:Profile?
    var  closetRecommendations = [ClosetCategory]()
    
    //for closet favorite
    var closetFavorite = [ClosetCategory]()
    var didLoadDataFavorite = false
    var pullToRefreshFavorite = false
    var pageFavorite = 1
    
    var profileFavorite = [ClosetCategory]()
    var didLoadProfileFavorite = false
    var pageProfileFavorite = 1

    //for closetCategory
    var closetCategory = [ClosetCategory]()
    
    var didLoadData = false
    var pullToRefresh = false
    var page = 1

    func retrieveProfileCloset(with id: Int, completion: @escaping RetrieveProfileClosetCompletion) {
        THNetworkManager.sharedInstance.retrieveProfileCloset(for: id, page: page) { (json, error) in
            
            guard let json = json else {
                completion(error)
                return
            }

            var profileClosetdata = JSON.null
            if self.page == 1 {
                profileClosetdata = json["data"]
            } else {
                profileClosetdata = json["data4"]
            }
            var resultClosetRecomedations = [ClosetCategory]()

            if profileClosetdata != JSON.null {
                //Profile
                self.profile = Profile.fromJSON(json: profileClosetdata)

                //Closet Item
                if let postsArray = profileClosetdata["posts"].array {
                    self.closetItems = postsArray.map { return ClosetItem.fromJSON(json: $0) }
                }

                
                let closetCategoryData = json["data3"]

                if closetCategoryData != JSON.null {
                    //Closet Category
                    if let closetCategoryArray = closetCategoryData.array {
                        resultClosetRecomedations = closetCategoryArray.map { return ClosetCategory.fromJSON(json: $0) }
                    }
                }
                
                if self.pullToRefresh {
                    self.pullToRefresh = false
                    self.closetRecommendations = [ClosetCategory]()
                }
                
                if resultClosetRecomedations.count > 0 {
                    // At least one of the results was correctly parsed, hence we received a result
                    self.page += 1
                    self.closetRecommendations.append(contentsOf: resultClosetRecomedations)
                    
                    self.didLoadData = true
                    completion(nil)
                } else {
                    self.didLoadData = false
                    // TODO: Add corresponding error
                    completion(nil)
                }
            } else {
            // TODO: Add corresponding error
            completion(nil)
        }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                if json["status"].boolValue == false {
                    THUserManager.sharedInstance.logoutCurrentuser(isNavigateToLogin: false, completion: { (error) in
                        
                    })
                }
            }
        }
    }

    //Closet favorite
    func retrieveProfileClosetFavorite(with id: Int, completion: @escaping RetrieveProfileClosetCompletion) {
        THNetworkManager.sharedInstance.retrieveProfileClosetFavorite(for: id, page: pageFavorite) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            var profileClosetFavoritedata = JSON.null
            profileClosetFavoritedata = json["data"]
        
            var resultClosetFavorite = [ClosetCategory]()
            
            if profileClosetFavoritedata != JSON.null {
                //Profile
        
                //Closet Category
                if let closetFavoriteArray = profileClosetFavoritedata.array {
                    resultClosetFavorite = closetFavoriteArray.map { return ClosetCategory.fromJSON(json: $0) }
                }
    
                
                if self.pullToRefreshFavorite {
                    self.pullToRefreshFavorite = false
                    self.closetFavorite = [ClosetCategory]()
                }
                
                if resultClosetFavorite.count > 0 {
                    self.pageFavorite += 1
                    self.closetFavorite.append(contentsOf: resultClosetFavorite)
                    
                    self.didLoadDataFavorite = true
                    completion(nil)
                } else {
                    self.didLoadDataFavorite = false
                    // TODO: Add corresponding error
                    completion(nil)
                }
            } else {
                // TODO: Add corresponding error
                completion(nil)
            }
        }
    }
    
    //Profile favorite
    func retrieveProfileFavorite(with id: Int, completion: @escaping RetrieveProfileClosetCompletion) {
        THNetworkManager.sharedInstance.retrieveProfileClosetFavorite(for: id, page: pageProfileFavorite) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            var profileClosetFavoritedata = JSON.null
            profileClosetFavoritedata = json["data"]
            
            var resultClosetFavorite = [ClosetCategory]()
            
            if profileClosetFavoritedata != JSON.null {
                //Profile
                
                //Closet Category
                if let closetFavoriteArray = profileClosetFavoritedata.array {
                    resultClosetFavorite = closetFavoriteArray.map { return ClosetCategory.fromJSON(json: $0) }
                }
                
                
                /*if self.pullToRefreshFavorite {
                    self.pullToRefreshFavorite = false
                    self.closetFavorite = [ClosetCategory]()
                }*/
                
                if resultClosetFavorite.count > 0 {
                    self.pageProfileFavorite += 1
                    self.profileFavorite.append(contentsOf: resultClosetFavorite)
                    
                    self.didLoadProfileFavorite = true
                    completion(nil)
                } else {
                    self.didLoadProfileFavorite = false
                    // TODO: Add corresponding error
                    completion(nil)
                }
            } else {
                // TODO: Add corresponding error
                completion(nil)
            }
        }
    }

    

    //For Closet category
    func retrieveClosetCategory(with id:Int, category:String, completion: @escaping RetrieveProfileClosetCompletion) {
        THNetworkManager.sharedInstance.retrieveClosetCategory(for: id, category: category){ (json, error) in
            guard let json = json else {
                completion(error)
                return
            }

            let closetCategoryData = json["data"]

            if closetCategoryData != JSON.null {
                //Closet Category
                if let closetCategoryArray = closetCategoryData.array {
                    self.closetCategory = closetCategoryArray.map { return ClosetCategory.fromJSON(json: $0) }
                }
                completion(nil)
            } else {
                // TODO: Add corresponding error
                completion(nil)
            }
        }
    }

    func updateUserClosetSize(for id: Int,withParam: String,value: String,completion: @escaping RetrieveProfileClosetCompletion) {
        THNetworkManager.sharedInstance.updateUserClosetSize(for: id, withParam: withParam, value: value){ (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            let profileClosetdata = json["data"]

            if profileClosetdata != JSON.null {
                //Profile
                self.profile = Profile.fromJSON(json: profileClosetdata)

                completion(nil)
            } else {
                // TODO: Add corresponding error
                completion(nil)
            }

        }
    }

    func updateUserLatLong(for id:Int,latitude:Double,longitude:Double,completion: @escaping RetrieveProfileClosetCompletion)
    {
        THNetworkManager.sharedInstance.updateUserLocation(for: id, latitude: latitude, longitude: longitude) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            let profileClosetdata = json["data"]

            if profileClosetdata != JSON.null {
                //Profile
                self.profile = Profile.fromJSON(json: profileClosetdata)
            }
            completion(nil)
        }

    }




}
