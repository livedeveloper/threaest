//
//  THBrandProductManager.swift
//  Threadest
//
//  Created by Jaydeep on 10/04/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

typealias RetrieveBrandProductCompletion = (_ error: Error?) -> Void

class THBrandProductManager: NSObject {
    static let sharedInstance = THBrandProductManager()
    
    var brandProduct:ClosetCategory?
    
    func performAddProduct(with addProduct: AddProduct, completion:@escaping RetrieveBrandProductCompletion) {
        THNetworkManager.sharedInstance.performAddProduct(with: addProduct) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            if json["status"].boolValue {
                completion(nil)
            } else {
                let error = NSError(domain: "Error", code: 0, userInfo: [NSLocalizedDescriptionKey : "error"])
                completion(error)
            }
        }
    }
    
    func performGiveAwayContestants(with brandId: Int, productId: Int, completion: @escaping RetrieveBrandProductCompletion) {
        THNetworkManager.sharedInstance.retrieveGiveAwayConstestants(brandId: brandId, ProductId: productId) { (json, error) in
            debugPrint(json)
            guard let json = json else {
                completion(error)
                return
            }
            let responce = json["data"]
            
            if responce != JSON.null {
                completion(nil)
            }
        }
    }
    
    func retrieveBrandProfile(with brandId: Int, productId: Int, geofence: Bool, shareCode: String = "",completion:@escaping RetrieveBrandProductCompletion) {
        THNetworkManager.sharedInstance.retrieveBrandProduct(for: brandId, productId: productId, geofence: geofence, shareCode: shareCode) { (json, error) in
            debugPrint(json)
            guard let json = json else {
                completion(error)
                return
            }
            
            let brandproduct = json["data"]
            
             if brandproduct != JSON.null {
                self.brandProduct = ClosetCategory.fromJSON(json: brandproduct)
                completion(nil)
            }
        }
    }
    
    func retrieveBrandProfileFromDots(with brandId: Int, productId: Int, sharePostCode: String, postId: Int, completion: @escaping RetrieveBrandProductCompletion) {
        THNetworkManager.sharedInstance.retrieveBrandProductFromDots(for: brandId, productId: productId, sharePostCode: sharePostCode, postId: postId) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            let brandproduct = json["data"]
            
            if brandproduct != JSON.null {
                self.brandProduct = ClosetCategory.fromJSON(json: brandproduct)
                completion(nil)
            }
        }
    }
    
    
}
