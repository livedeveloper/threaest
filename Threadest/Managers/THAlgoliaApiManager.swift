//
//  THAlgoliaApiManager.swift
//  Threadest
//
//  Created by Jaydeep on 21/11/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

typealias RetrieveAlgoliaApiCompletion = (_ error: Error?) -> Void

class THAlgoliaApiManager: NSObject {
    static let sharedInstance = THAlgoliaApiManager()
    
    var popularSearch = [PopularSearch]()
    
    func retrievePopularAlgoliaSearch(indices: String, completion: @escaping RetrieveAlgoliaApiCompletion) {
        THNetworkManager.sharedInstance.retrievePopularSearch(indices: indices) { (json, error) in
            debugPrint(json)
            guard let json = json else {
                completion(error)
                return
            }
            
            let popularSearchData = json["topSearches"]
            if popularSearchData != JSON.null {
                if let popularSearchArray = popularSearchData.array {
                    self.popularSearch = popularSearchArray.map { return PopularSearch.fromJSON(json: $0) }
                }
                completion(nil)
            }
        }
    }
}
