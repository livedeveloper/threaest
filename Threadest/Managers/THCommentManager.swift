//
//  THCommentManager.swift
//  Threadest
//
//  Created by Jaydeep on 07/04/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

typealias RetrievecommentCompletion = (_ error: Error?) -> Void
typealias RetrievepostcommentCompletion = (_ comment: Comment?, _ error: Error?) -> Void

class THCommentManager: NSObject {
    static let sharedInstance = THCommentManager()

    var comments = [Comment]()

    var didLoadData = false
    var page = 1

    func retrieveComments(with postId: Int, completion: @escaping RetrievePostsCompletion) {
        THNetworkManager.sharedInstance.retrieveComments(for: postId, page: page) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }

            var resultsComments = [Comment]()

            let commentData = json["data"]

            if commentData != JSON.null {
                if let commentArray = commentData.array {
                    resultsComments = commentArray.map {
                        return Comment.fromJSON(json:  $0)}
                }
            }


            if resultsComments.count > 0 {
                // At least one of the results was correctly parsed, hence we received a result
                self.page += 1
                self.comments.append(contentsOf: resultsComments)

                self.didLoadData = true
                completion(nil)
            } else {
                // TODO: Add corresponding error
                completion(nil)
            }
        }
    }

    // for retrieve product comments
    func retrieveProductComments(with brandId:Int, productId: Int, completion: @escaping RetrievePostsCompletion) {
        THNetworkManager.sharedInstance.retrieveProductComments(for: brandId, productId: productId) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }

            var resultsComments = [Comment]()

            let commentData = json["data"]

            if commentData != JSON.null {
                if let commentArray = commentData.array {
                    resultsComments = commentArray.map {
                        return Comment.fromJSON(json:  $0)}
                }
            }


            if resultsComments.count > 0 {
                // At least one of the results was correctly parsed, hence we received a result
                self.page += 1
                self.comments.append(contentsOf: resultsComments)

                self.didLoadData = true
                completion(nil)
            } else {
                // TODO: Add corresponding error
                completion(nil)
            }
        }
    }

    func perform(with postcomment:postComment, postCompletion: @escaping RetrievepostcommentCompletion) {
        THNetworkManager.sharedInstance.performPostComment(for: postcomment) { (json, error) in
            guard let json = json else {
                postCompletion(nil, error)
                return
            }

            if json["status"].boolValue {
                postCompletion(Comment.fromJSON(json: json["data"]), nil)
            }
        }
    }
    
    func performDeletePostComment(with postId: Int, commentId:Int, completion:@escaping RetrievecommentCompletion) {
        THNetworkManager.sharedInstance.perfomDeletePostComment(with: postId, commentId: commentId) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            if json["status"].boolValue {
                completion(nil)
            }
        }
    }
    
    
    
    func performPostProductComment(with postProductComment: PostProductComment , postCompletion: @escaping RetrievepostcommentCompletion) {
        THNetworkManager.sharedInstance.performPostProductComment(for: postProductComment) { (json, error) in
            guard let json = json else {
                postCompletion(nil, error)
                return
            }

            if json["status"].boolValue {
                postCompletion(Comment.fromJSON(json: json["data"]), nil)
            }
        }
    }
    
    func performDeleteProductComment(with brandId: Int, productId: Int, commentId: Int, completion: @escaping RetrievecommentCompletion) {
        THNetworkManager.sharedInstance.performDeleteProductComment(with: brandId, productId: productId, commentId: commentId) { (json, error) in
            guard let json = json else {
                completion(error)
                return
            }
            
            if json["status"].boolValue {
                completion(nil)
            }
        }
    }
}
