//
//  AddProduct.swift
//  Threadest
//
//  Created by Jaydeep on 04/09/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation

public struct AddProduct {
    let productTitle: String
    let productURL: String
    let productBrandId: Int
    let productDefaultPrice: String
    let productDescription: String
}
