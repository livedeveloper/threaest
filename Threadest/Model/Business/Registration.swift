//
//  Registration.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 09/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation

public struct Registration {
    let username: String
    let password: String
    let passwordConfirmation: String
    let mobilePhoneNumber: String
    let email: String
}
