//
//  SearchProduct.swift
//  Threadest
//
//  Created by Synnapps on 06/04/2017.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import ObjectMapper

class SearchProduct: Mappable {
    var objectID: String?
    var brand_id: Int?
    var city: String?
    var default_thumbnail_image_url: String!
    var score: Int?
    var title: String?
    var default_price: Float?
    var current_price: Float?
    var sale_price: Float?
    var brandName: String?
    var default_medium_image_url: String!
    var default_medium_image_height: Float!
    var default_medium_image_width: Float!
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        objectID                    <- map["objectID"]
        brand_id                    <- map["brand_id"]
        city                        <- map["city"]
        default_thumbnail_image_url <- map["default_thumbnail_image_url"]
        score                       <- map["score"]
        title                       <- map["title"]
        default_price               <- map["default_price"]
        current_price               <- map["current_price"]
        sale_price                  <- map["sale_price"]
        brandName                   <- map["brand_name"]
        default_medium_image_url    <- map["default_medium_image_url"]
        default_medium_image_height <- map["default_medium_image_height"]
        default_medium_image_width  <- map["default_medium_image_width"]
    }
}
