//
//  CreateProduct.swift
//  Threadest
//
//  Created by sagar r on 4/12/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

public struct CreateProduct {
    let content: String
    let photo_tags: String
    let product_image: UIImage
    let comment: String
    let usreid: Int
    let facebook: Bool
    let twitter: Bool
    let tagType: String
}

extension CreateProduct{
    
    static func generateTag(){
    }
}
