//
//  User.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 10/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation
import SwiftyJSON
public struct User {
    let userId: Int
    var about: String
    var username: String
    var email: String
    var phoneNumber: String
    let imageURL: String
    let thumbnailImageURL: String
    var password: String = ""
    var height: String = ""
    var age: String = ""
    var gender: String = ""
    var shirtSize: String = ""
    var waistSize: String = ""
    var shoe_size: String = ""
    var neckSize: String = ""
    var braSize: String = ""
    var hipsSize: String = ""
    var cartCount : Int = 0
    let closetSetupOne: Bool
    let closetSetupTwo: Bool
    let closetSetupThree: Bool
    let closetSetupComplete: Bool
    let needsToFollowBrands: Bool
    let needsToFollowUsers:Bool
    var isFollowing:Bool = false
    var selectedImage : UIImage!
    var influencerScore: Int
    let sharePostCode: Int
    var tempAccount: Bool
    let invitePromoCode: String
    var website: String
    var verifiedAccount: Bool
    // TODO: Add more fields if needed
}


extension User: JSONConvertible {
    static func fromJSON(json: JSON) -> User {

        var userId = -1

        if json != JSON.null {
            userId = json["id"].intValue
        }

        return User(userId: userId,
                    about: json["about"].stringValue,
                    username: json["username"].stringValue,
                    email: json["email"].stringValue, phoneNumber: json["mobile_phone_number"].stringValue,
                    imageURL: json["image_url"].stringValue,
                    thumbnailImageURL:json["thumbnail_image_url"].stringValue,
                    password: json["password"].stringValue,
                    height: json["height"].stringValue,
                    age: json["age"].stringValue,
                    gender: json["gender"].stringValue,
                    shirtSize: json["shirt_size"].stringValue,
                    waistSize: json["waist_size"].stringValue,
                    shoe_size: json["shoe_size"].stringValue,
                    neckSize: json["neck_size"].stringValue,
                    braSize: json["bra_size"].stringValue,
                    hipsSize: json["hips_size"].stringValue,
                    cartCount : json["cart_items_count"].intValue,
                    closetSetupOne: json["closet_setup_step_one_complete"].boolValue,
                    closetSetupTwo: json["closet_setup_step_two_complete"].boolValue,
                    closetSetupThree: json["closet_setup_step_three_complete"].boolValue,
                    closetSetupComplete: json["closet_setup_complete"].boolValue,
                    needsToFollowBrands: json["needs_to_follow_brands"].boolValue,
                    needsToFollowUsers: json["needs_to_follow_users"].boolValue,
                    isFollowing: json["is_following"].boolValue,
                    selectedImage:nil,
                    influencerScore: json["influencer_score"].intValue,
                    sharePostCode: json["share_post_code"].intValue,
                    tempAccount: json["temp_account"].boolValue,
                    invitePromoCode: json["user_invite_promo_code"].stringValue,
                    website: json["website"].stringValue,
                    verifiedAccount : json["verified_account"].boolValue)
    }
}
