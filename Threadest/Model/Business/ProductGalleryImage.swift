//
//  ProductGalleryImage.swift
//  Threadest
//
//  Created by Jaydeep on 28/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

public struct ProductGalleryImage {
    let id: Int
    let mediumImageUrl: String
    let mediumImageHeight: Float
    let mediumImageWidth: Float
    let image_type: String
    let three_dimensional_object_url: String
    let is_3d_object: Bool
}

extension ProductGalleryImage: JSONConvertible {
    static func fromJSON(json: JSON) -> ProductGalleryImage {
        return ProductGalleryImage(id: json["id"].intValue,
                                   mediumImageUrl: json["medium_image_url"].stringValue,
                                   mediumImageHeight: json["medium_image_height"].floatValue,
                                   mediumImageWidth: json["medium_image_width"].floatValue,
                                   image_type: json["image_type"].stringValue,
                                   three_dimensional_object_url: json["three_dimensional_object_url"].stringValue,
                                   is_3d_object: json["is_3d_object"].boolValue)
    }
}
