//
//  FollowPeople.swift
//  Threadest
//
//  Created by Jaydeep on 30/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct FollowPeople {
    let id: Int
    let imageUrl: String
    let username: String
    let name: String
    let email: String
    let thumbnailImageUrl: String
    var isFollow:Bool
    let influencerScore: Int
    var isVerified: Bool
}

extension FollowPeople: JSONConvertible {
    static func fromJSON(json: JSON) -> FollowPeople {
        debugPrint(json)
        return FollowPeople(id: json["id"].intValue,
                            imageUrl: json["image_url"].stringValue,
                            username: json["username"].stringValue,
                            name: json["name"].stringValue,
                            email: json["email"].stringValue,
                            thumbnailImageUrl: json["thumbnail_image_url"].stringValue,
                            isFollow: json["is_following"].boolValue,
                            influencerScore: json["influencer_score"].intValue,
                            isVerified: json["verified_account"].boolValue)
    }
}
