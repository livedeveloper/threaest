//
//  SearchBrand.swift
//  Threadest
//
//  Created by Synnapps on 08/04/2017.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import ObjectMapper

class SearchBrand: Mappable {
    
    var objectID: String?
    var founding_year: Int?
    var id: Int?
    var state: String?
    var name: String?
    var brand_logo_image_url: String?
    var products_count: Int?
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        objectID                    <- map["objectID"]
        founding_year               <- map["founding_year"]
        id                          <- map["id"]
        state                       <- map["state"]
        name                        <- map["name"]
        brand_logo_image_url        <- map["brand_logo_image_url"]
        products_count              <- map["products_count"]
    }
}
