//
//  Login.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 15/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

public struct Login {
    let email: String
    let password: String
}
