//
//  CartProduct.swift
//  Threadest
//
//  Created by Nasrullah Khan on 06/04/2017.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

public struct CartProduct {
    let shipsWithin: String
    let id: Int
    let title: String
    let url: String
    let brandID: Int
    let userID: Int
    let disabled: Bool
    let commentCount: Int
    let upvotesCount: Int
    let score: Int
    let approved: Bool
    let approvedForDate: Bool
    let clicksCount: Int
    let desc: String
    let createdAt: String
    let updatedAt: String
    let defaultPrice: Int
    let streetAddress1: String
    let streetAddress2: String
    let city: String
    let state: String
    let zipCode: String
    let shippingOptionID: Int
    let genderClassification: String
    let closetCategory: String
    let soldOut: Bool
    let defaultImageURL: String
    let brandName: String
    let brand: CartBrand
}

extension CartProduct: JSONConvertible {
    static func fromJSON(json: JSON) -> CartProduct {
        
        let brand = CartBrand.fromJSON(json: json["brand"])
        
        return CartProduct(shipsWithin: json["ships_within"].stringValue,
                           id: json["id"].intValue,
                           title: json["title"].stringValue,
                           url: json["url"].stringValue,
                           brandID: json["brand_id"].intValue,
                           userID: json["user_id"].intValue,
                           disabled: json["disabled"].boolValue,
                           commentCount: json["comments_count"].intValue,
                           upvotesCount: json["upvotes_count"].intValue,
                           score: json["score"].intValue,
                           approved: json["approved"].boolValue,
                           approvedForDate: json["approved_for_date"].boolValue,
                           clicksCount: json["clicks_count"].intValue,
                           desc: json["description"].stringValue,
                           createdAt: json["created_at"].stringValue,
                           updatedAt: json["updated_at"].stringValue,
                           defaultPrice: json["default_price"].intValue,
                           streetAddress1: json["street_address_1"].stringValue,
                           streetAddress2: json["street_address_2"].stringValue,
                           city: json["city"].stringValue,
                           state: json["state"].stringValue,
                           zipCode: json["zip_code"].stringValue,
                           shippingOptionID: json["shipping_option_id"].intValue,
                           genderClassification: json["gender_classification"].stringValue,
                           closetCategory: json["closet_category"].stringValue,
                           soldOut: json["sold_out"].boolValue,
                           defaultImageURL: json["default_image_url"].stringValue,
                           brandName: json["brand_name"].stringValue,
                           brand: brand)
    }
}
