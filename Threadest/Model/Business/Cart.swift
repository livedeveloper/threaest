//
//  Cart.swift
//  Threadest
//
//  Created by Nasrullah Khan on 06/04/2017.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

public struct Cart {
    let userid: Int
    let id: Int
    let totalPrice: Double
    let createdAt: String
    let updatedAt: String
    let cartItemQuantity: Int
    let subtotal: Double
    let shippingCost: Double
    let flatRateShippingFee: Double
    let taxes: Double
    let cartItems: [CartItem]
}

extension Cart: JSONConvertible {
    static func fromJSON(json: JSON) -> Cart {
        
        var cartItems = [CartItem]()
        
        if let cartItemsArray = json["cart_items"].array {
            cartItems = cartItemsArray.map({return CartItem.fromJSON(json: $0)})
        }
        
        return Cart(userid: json["user_id"].intValue,
                       id: json["id"].intValue,
                       totalPrice: json["total_price"].doubleValue,
                       createdAt: json["created_at"].stringValue,
                       updatedAt: json["updated_at"].stringValue,
                       cartItemQuantity: json["cart_item_quantity"].intValue,
                       subtotal: json["sub_total"].doubleValue,
                       shippingCost: json["shipping_cost"].doubleValue,
                       flatRateShippingFee: json["flat_rate_shipping_fee"].doubleValue,
                       taxes: json["taxes"].doubleValue,
                       cartItems: cartItems)
    }
}
