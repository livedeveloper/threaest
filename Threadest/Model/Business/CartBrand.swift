//
//  CartBrand.swift
//  Threadest
//
//  Created by Nasrullah Khan on 06/04/2017.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

public struct CartBrand {
    let userid: Int
    let name: String
    let id: Int
    let brandPicFileName: String
    let brandPicContentType: String
    let brandPicFileSize: Int
    let brandPicUpdatedAt: String
    let brandLogoImage: String
    let createdAt: String
    let updatedAt: String
    let brandLogoFileName: String
    let brandLogoContentType: String
    let brandLogoFileSize: Int
    let brandLogoUpdated_at: String
    let desc: String
    let followerCount: Int
    let productCount: Int
    let foundingMonth: String
    let foundingYear: Int
    let state: String
    let city: String
    let website: String
    let bankAccountNumber: String
    let bankRoutingNumber: String
    let streetAddress1: String
    let streetAddress2: String
    let zipcode: Int
    let defaultEmail: String
    let accessToken: String
    let stripeCustomer_id: String
    let refreshToken: String
    let stripeUserId: String
}

extension CartBrand: JSONConvertible {
    static func fromJSON(json: JSON) -> CartBrand {
        return CartBrand(userid: json["user_id"].intValue,
                       name: json["name"].stringValue,
                       id: json["id"].intValue,
                       brandPicFileName: json["brand_pic_file_name"].stringValue,
                       brandPicContentType: json["brand_pic_content_type"].stringValue,
                       brandPicFileSize: json["brand_pic_file_size"].intValue,
                       brandPicUpdatedAt: json["brand_pic_updated_at"].stringValue,
                       brandLogoImage: json["brand_logo_image_url"].stringValue,
                       createdAt: json["created_at"].stringValue,
                       updatedAt: json["updated_at"].stringValue,
                       brandLogoFileName: json["brand_logo_file_name"].stringValue,
                       brandLogoContentType: json["brand_logo_content_type"].stringValue,
                       brandLogoFileSize: json["brand_logo_file_size"].intValue,
                       brandLogoUpdated_at: json["brand_logo_updated_at"].stringValue,
                       desc: json["description"].stringValue,
                       followerCount: json["followers_count"].intValue,
                       productCount: json["product_count"].intValue,
                       foundingMonth: json["founding_month"].stringValue,
                       foundingYear: json["founding_year"].intValue,
                       state: json["state"].stringValue,
                       city: json["city"].stringValue,
                       website: json["website"].stringValue,
                       bankAccountNumber: json["bank_account_number"].stringValue,
                       bankRoutingNumber: json["bank_routing_number"].stringValue,
                       streetAddress1: json["street_address_1"].stringValue,
                       streetAddress2: json["street_address_2"].stringValue,
                       zipcode: json["zip_code"].intValue,
                       defaultEmail: json["default_email"].stringValue,
                       accessToken: json["access_token"].stringValue,
                       stripeCustomer_id: json["stripe_customer_id"].stringValue,
                       refreshToken: json["refresh_token"].stringValue,
                       stripeUserId: json["stripe_user_id"].stringValue)
    }
}
