//
//  SettingInfo.swift
//  Threadest
//
//  Created by Viktor Nesic on 3/18/18.
//  Copyright © 2018 JC. All rights reserved.
//

import UIKit

enum settingType : String {
    case logout
    case setupCloset
    case none
}

class SettingInfo: NSObject {
    
    var settingTitle : String = ""
    var settingSection : String = ""
    var settingType : settingType = .none
    
    init(title : String, section : String, type : settingType) {
        settingTitle = title
        settingSection = section
        settingType = type
    }
}

