//
//  Product.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 22/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

public struct Product {
    let id: Int
    let title: String
    let url: String
    let brandID: Int
    let userID: Int
    let disabled: Bool
    let commentCount: Int
    let upvotesCount: Int
    let score: Int
    let approved: Bool
    let approvedForDate: Bool
    let clicksCount: Int
    let itemDescription: String
    let createdAt: String
    let updatedAt: String
    let defaultPrice: Int
    let streetAddress1: String
    let streetAddress2: String
    let city: String
    let state: String
    let zipCode: String
    let shippingOptionID: Int
    let shipsWithin: String
    let genderClassification: String
    let closetCategory: String
    let soldOut: Bool
}

extension Product: JSONConvertible {
    static func fromJSON(json: JSON) -> Product {
        return Product(id: json["id"].intValue,
                             title: json["title"].stringValue,
                             url: json["url"].stringValue,
                             brandID: json["brand_id"].intValue,
                             userID: json["user_id"].intValue,
                             disabled: json["disabled"].boolValue,
                             commentCount: json["comments_count"].intValue,
                             upvotesCount: json["upvotes_count"].intValue,
                             score: json["score"].intValue,
                             approved: json["approved"].boolValue,
                             approvedForDate: json["approved_for_date"].boolValue,
                             clicksCount: json["clicks_count"].intValue,
                             itemDescription: json["description"].stringValue,
                             createdAt: json["created_at"].stringValue,
                             updatedAt: json["updated_at"].stringValue,
                             defaultPrice: json["default_price"].intValue,
                             streetAddress1: json["street_address_1"].stringValue,
                             streetAddress2: json["street_address_2"].stringValue,
                             city: json["city"].stringValue,
                             state: json["state"].stringValue,
                             zipCode: json["zip_code"].stringValue,
                             shippingOptionID: json["shipping_option_id"].intValue,
                             shipsWithin: json["ships_within"].stringValue,
                             genderClassification: json["gender_classification"].stringValue,
                             closetCategory: json["closet_category"].stringValue,
                             soldOut: json["sold_out"].boolValue)
    }
}
