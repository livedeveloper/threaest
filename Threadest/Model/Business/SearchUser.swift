//
//  SearchUser.swift
//  Threadest
//
//  Created by Synnapps on 07/04/2017.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import ObjectMapper

class SearchUser: Mappable {
    
    var objectID: String?
    var username: String?
    var name: String?
    var thumbnail_image_url: String!
    var isVerified: Bool?
    
    
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        objectID                    <- map["objectID"]
        username                    <- map["username"]
        name                        <- map["name"]
        thumbnail_image_url         <- map["thumbnail_image_url"]
        isVerified                  <- map["verified_account"]
    }
    
}
