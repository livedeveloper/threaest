import Foundation
import UIKit
import SwiftyJSON

public struct Profile {
    let username: String
    let thumbnailImageUrl: String
    let mediumImageUrl: String
    let currentUserInfluencerScore: Int
    let followersCount: Int
    let brandsCount: Int
    let followingsCount: Int
    let gender: String
    let shirtSize: String
    let waistSize: String?
    let neckSize : String?
    let hipSize : String?
    let shoeSize : String?
    let height : String?
    let age : String?
    let braSize : String?
    var latitude : Double?
    var longitude : Double?
    var currentUserFollowing : Bool?
    let website : String?
    let about : String?
    
    // TODO: Add more if needed
}

extension Profile: JSONConvertible {
    static func fromJSON(json: JSON) -> Profile{
        var username = ""
        var thumbnailImageUrl = ""
        var mediumImageUrl = ""
        var currentUserInfluencerScore = 0
        var followersCount = 0
        var brandsCount = 0
        var followingsCount = 0
        var gender = ""
        var shirtSize = ""
        var waistSize = ""
        var neckSize = ""
        var hipSize = ""
        var shoeSize = ""
        var height = ""
        var age  = ""
        var braSize = ""
        var latitude = 0.0
        var longitude = 0.0
        let currentUserFollowing = false
        var website = ""
        var about = ""
        
        if json != JSON.null {
            username = json["username"].stringValue
            thumbnailImageUrl = json["thumbnail_image_url"].stringValue
            mediumImageUrl = json["medium_image_url"].stringValue
            currentUserInfluencerScore = json["influencer_score"].intValue
            followersCount = json["followers_count"].intValue
            brandsCount = json["brand_followings_count"].intValue
            followingsCount = json["followings_count"].intValue
            gender = json["gender"].stringValue
            shirtSize = json["shirt_size"].stringValue
            waistSize = json["waist_size"].stringValue
            neckSize = json["neck_size"].stringValue
            hipSize = json["hips_size"].stringValue
            shoeSize = json["shoe_size"].stringValue
            height = json["height"].stringValue
            age = json["age"].stringValue
            braSize = json["bra_size"].stringValue
            latitude = json["estimated_latitude"].doubleValue
            longitude = json["estimated_longitude"].doubleValue
            website = json["website"].stringValue
            about = json["about"].stringValue
            
        }
        
        return Profile(username: username,
                     thumbnailImageUrl: thumbnailImageUrl,
                     mediumImageUrl: mediumImageUrl,
                     currentUserInfluencerScore: currentUserInfluencerScore,
                     followersCount:followersCount,
                     brandsCount:brandsCount,
                     followingsCount:followingsCount,
                     gender:gender,
                     shirtSize: shirtSize,waistSize:waistSize,neckSize:neckSize,hipSize:hipSize,shoeSize:shoeSize,height:height,age:age,braSize:braSize,latitude:latitude,longitude:longitude,currentUserFollowing:currentUserFollowing, website:website, about: about)
    }
}
