//
//  ProductVariation.swift
//  Threadest
//
//  Created by Jaydeep on 28/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

public struct ProductVariation {
    let productId: Int
    let id: Int
    let quantityAvailable: Int
    let size: String
    let createdAt: String
    let updatedAt: String
    let price: Float
}

extension ProductVariation: JSONConvertible {
    static func fromJSON(json: JSON) -> ProductVariation {
        return ProductVariation(productId: json["product_id"].intValue,
                                 id: json["id"].intValue,
                                 quantityAvailable:json["quantity_available"].intValue,
                                 size:json["size"].stringValue,
                                 createdAt:json["created_at"].stringValue,
                                 updatedAt:json["updated_at"].stringValue, price:json["price"].floatValue)
    }
}
