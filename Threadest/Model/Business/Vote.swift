//
//  Vote.swift
//  Threadest
//
//  Created by Jaydeep on 28/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct Vote {
    let votableId: Int
    let id: Int
    let userId: Int
    let votableType: String
    let createdAt: String
    let updatedAt: String
}

extension Vote: JSONConvertible {
    static func fromJSON(json: JSON) -> Vote {
        
        return Vote(votableId: json["votable_id"].intValue,
                    id: json["id"].intValue,
                    userId: json["user_id"].intValue,
                    votableType: json["votable_type"].stringValue,
                    createdAt: json["created_at"].stringValue,
                    updatedAt: json["updated_at"].stringValue)
    }
}

