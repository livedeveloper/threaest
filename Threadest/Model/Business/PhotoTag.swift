import Foundation
import SwiftyJSON

public struct PhotoTag {
    let postId: Int
    let id: Int
    let x: Int
    let y: Int
    let productId: Int
    let createdAt: String
    let updatedAt: String
    let brandId: Int
    let tagType: String
    let hashTag: String
    let productName: String
    let brandName: String
    let brandLogo: String
    let productImage: String
    let productDefaultPrice: String
    let productSalePrice:String
}

extension PhotoTag: JSONConvertible {
    static func fromJSON(json: JSON) -> PhotoTag {
        return PhotoTag(postId: json["post_id"].intValue,
                        id: json["id"].intValue,
                        x: json["x"].intValue,
                        y: json["y"].intValue,
                        productId: json["product_id"].intValue,
                        createdAt: json["created_at"].stringValue,
                        updatedAt: json["updated_at"].stringValue,
                        brandId: json["brand_id"].intValue,
                        tagType: json["tag_type"].stringValue,
                        hashTag: json["hash_tag"].stringValue,
                        productName: json["product_name"].stringValue,
                        brandName: json["brand_name"].stringValue,
                        brandLogo: json["brand_logo"].stringValue,
                        productImage: json["product_image"].stringValue,
                        productDefaultPrice : json["product_default_price"].stringValue,
                        productSalePrice : json["product_sale_price"].stringValue)
    }
}
