import Foundation
import UIKit
import SwiftyJSON

public struct PurchasesOrders {
    let Id: Int
    let productId: Int
    let orderId: Int
    var price: Int
    let productVariationId: Int
    let quantity: Int
    let size: String
    let tax: Float
    let shippingAddressId: Int
    var productData:ClosetCategory
}

extension PurchasesOrders: JSONConvertible {
    static func fromJSON(json: JSON) -> PurchasesOrders{
        
        let product = json["product"]
        var data1 :ClosetCategory?
        if product != JSON.null {
            //Closet Category
             data1 = ClosetCategory.fromJSON(json: product)
        }
        

        
        return PurchasesOrders(Id: json["id"].intValue,
                              productId: json["product_id"].intValue,
                              orderId: json["order_id"].intValue,
                              price: json["price"].intValue,
                              productVariationId:json["product_variation_id"].intValue,
                              quantity:json["quantity"].intValue,
                              size:json["size"].stringValue,
                              tax: json["tax"].floatValue,
                              shippingAddressId: json["shipping_address_id"].intValue,
                              productData:data1!)
    }
}
