//
//  Comment.swift
//  Threadest
//
//  Created by Jaydeep on 28/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct Comment {
    let commentableId: Int
    let id: Int
    let title: String
    let comment: String
    let commentableType: String
    let userId: Int
    let role: String
    let createdAt: String
    let updatedAt: String
    let commentOwnerUsername: String
    let commentOwnerImage: String
}

extension Comment: JSONConvertible {
    static func fromJSON(json: JSON) -> Comment {
        
        return Comment(commentableId: json["commentable_id"].intValue,
                       id: json["id"].intValue,
                       title: json["title"].stringValue,
                       comment: json["comment"].stringValue,
                       commentableType: json["commentable_type"].stringValue,
                       userId: json["user_id"].intValue,
                       role: json["role"].stringValue,
                       createdAt: json["created_at"].stringValue,
                       updatedAt: json["updated_at"].stringValue,
                       commentOwnerUsername: json["comment_owner_username"].stringValue,
                       commentOwnerImage: json["comment_owner_image"].stringValue)
    }
}

public struct postComment {
    var postId: Int
    var userId: Int
    var title: String
    var comment: String
}

public struct PostProductComment {
    var comment: String
    var brandId: Int
    var productId: Int
    var userId: Int
    var commentTitle: String
}

