//
//  PopularSearch.swift
//  Threadest
//
//  Created by Jaydeep on 21/11/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct PopularSearch {
    let avgHitcountWithoutTypos: Int
    let query: String
    let count: Int
    let avgHitCount: Int
}

extension PopularSearch: JSONConvertible {
    static func fromJSON(json: JSON) -> PopularSearch {
        return PopularSearch(avgHitcountWithoutTypos: json["avgHitCountWithoutTypos"].intValue,
            query: json["query"].stringValue,
            count: json["count"].intValue,
            avgHitCount: json["avgHitCount"].intValue)
    }
}
