//
//  ShippingAddress.swift
//  Threadest
//
//  Created by Nasrullah Khan on 10/04/2017.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

public struct ShippingAddress {
    let name: String
    let address1: String
    let address2: String
    let city: String
    let state: String
    let zipcode: String
}

extension ShippingAddress: JSONConvertible {
    static func fromJSON(json: JSON) -> ShippingAddress {
        
        return ShippingAddress(name: json["name"].stringValue,
                               address1: json["street_address_1"].stringValue,
                             address2: json["street_address_2"].stringValue,
                             city: json["city"].stringValue,
                             state: json["state"].stringValue,
                             zipcode: json["zip_code"].stringValue)
    }
}
