//
//  RetailLocation.swift
//  Threadest
//
//  Created by Jaydeep on 24/08/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct RetailLocation {
    let id: Int
    let latitude: Float
    let longitude: Float
    let city: String
    let state: String
    let streetAddress1: String
    let zipCode: String
    let streetAddress2: String
    let brandId: Int
    let distance: Float
    let bearing: Float
    let brandLogoURL: String
    let brandName: String
    let media_url: String
    let description: String
    let logo_url: String
    let title: String
}

extension RetailLocation: JSONConvertible {
    static func fromJSON(json: JSON) -> RetailLocation {
        debugPrint(json)
        return RetailLocation(id: json["id"].intValue, latitude: json["latitude"].floatValue, longitude: json["longitude"].floatValue, city: json["city"].stringValue, state: json["state"].stringValue, streetAddress1: json["street_address_1"].stringValue, zipCode: json["zip_code"].stringValue, streetAddress2: json["street_address_2"].stringValue, brandId: json["brand_id"].intValue, distance: json["distance"].floatValue, bearing: json["bearing"].floatValue,
                              brandLogoURL: json["brand_logo_image_url"].stringValue,
                              brandName: json["brand_name"].stringValue,
                              media_url: json["media_url"].stringValue,
                              description: json["description"].stringValue,
                              logo_url: json["logo_url"].stringValue,
                              title: json["title"].stringValue)
    }
}
