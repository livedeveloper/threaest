//
//  NotificationCreator.swift
//  Threadest
//
//  Created by jigar on 30/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct NotificationCreator {
   
    let id : Int
    let username : String
    let thumbnailImgUrl : String
}

    extension NotificationCreator: JSONConvertible {
        static func fromJSON(json: JSON) -> NotificationCreator {
            
            return NotificationCreator(id: json["id"].intValue,
                                       username: json["username"].stringValue,thumbnailImgUrl: json["thumbnail_image_url_small"].stringValue)

            
        }

}
