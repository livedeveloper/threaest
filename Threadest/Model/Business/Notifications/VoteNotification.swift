//
//  VoteNotification.swift
//  Threadest
//
//  Created by jigar on 30/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct VoteNotification {
    
    let id : Int
    let notificationCreatorId : Int
    let read : Bool
    let voteId : Int
    let notificationObjectOwnerId : Int
    let postId : Int
    let commentId : Int
    let notificationObjectId :Int
    let postImgUrl : String
    let createdAt : String
    
    
}

extension VoteNotification: JSONConvertible {
    static func fromJSON(json: JSON) -> VoteNotification {
        
        return VoteNotification(id : json["id"].intValue,notificationCreatorId : json["notification_creator_id"].intValue,read : json["read"].boolValue,voteId : json["vote_id"].intValue,notificationObjectOwnerId : json["notification_object_owner_id"].intValue,postId : json["post_id"].intValue,commentId : json["comment_id"].intValue,notificationObjectId : json["notification_object_id"].intValue,postImgUrl : json["post_image_url_small"].stringValue,createdAt : json["created_at"].stringValue)
    }
    
}
