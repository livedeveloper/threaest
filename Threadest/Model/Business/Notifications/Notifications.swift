//
//  Notifications.swift
//  Threadest
//
//  Created by jigar on 30/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct Notifications {
    let notificationType: String
    let notificationCreator : NotificationCreator?
    let voteNotification : VoteNotification?
    var followNotification : FollowNotifications?
    
}

extension Notifications: JSONConvertible {
    static func fromJSON(json: JSON) -> Notifications {
        
       let notificationTypeString = json["notification_type"].stringValue
        
        var notificationCreatorData : NotificationCreator?
        var voteNotificationData : VoteNotification?
        var followNotificationData : FollowNotifications?
        
        if(notificationTypeString == "Vote")
        {
            notificationCreatorData = NotificationCreator.fromJSON(json: json["notification_creator"])
            voteNotificationData = VoteNotification.fromJSON(json: json)
        }
        else if(notificationTypeString == "Follow")
        {
            notificationCreatorData = NotificationCreator.fromJSON(json: json)
            followNotificationData = FollowNotifications.fromJSON(json: json)
        }
        else
        {
            notificationCreatorData = NotificationCreator.fromJSON(json: json["notification_creator"])
            voteNotificationData = VoteNotification.fromJSON(json: json)
        }
        
        return Notifications(notificationType: notificationTypeString, notificationCreator: notificationCreatorData, voteNotification: voteNotificationData, followNotification: followNotificationData)
        
}
}
