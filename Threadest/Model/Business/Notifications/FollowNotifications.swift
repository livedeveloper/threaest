//
//  FollowNotifications.swift
//  Threadest
//
//  Created by jigar on 29/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct FollowNotifications {
    var isFollowing: Bool
    let createdAt: String
    
}

extension FollowNotifications: JSONConvertible {
    static func fromJSON(json: JSON) -> FollowNotifications {
        
        return FollowNotifications(isFollowing: json["is_following"].bool!,createdAt: json["created_at"].stringValue)
}
}
