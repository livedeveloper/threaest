//
//  SocialInteraction.swift
//  Threadest
//
//  Created by Jaydeep on 28/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct SocialInteraction {
    let id: Int
    let brandId: Int
    let userId: Int
    let description: String
    let createdAt: String
    let updatedAt: String
    let score: Int
    var upvotesCount: Int
    var commentsCount: Int
    let productId: Int
    var rethreadCount: Int
    var comments: [Comment]
    var votes: [Vote]
    var photoTags: [PhotoTag]
    var largeImageUrl: String
    var mediumImageUrl: String
    let largeImageHeight: Float
    let largeImageWidth: Float
    let largeImageRatio: Float
    var ownerImageUrl: String
    let ownerUsername: String
    let ownerSharePostCode: String
    let watermarkImageUrl: String
    var likedByCurrentUser: Bool
}


extension SocialInteraction: JSONConvertible {
    static func fromJSON(json: JSON) -> SocialInteraction {
        var comments = [Comment]()
        
        if let commentArray = json["comments"].array {
            comments = commentArray.map({return Comment.fromJSON(json: $0)})
        }
        
        var votes = [Vote]()
        
        if let voteArray = json["votes"].array {
            votes = voteArray.map({return Vote.fromJSON(json: $0)})
        }
        
        var photoTags = [PhotoTag]()
        if let photoTagsArray = json["photo_tags"].array {
            photoTags = photoTagsArray.map({return PhotoTag.fromJSON(json: $0)})
        }
        
        return SocialInteraction(id: json["id"].intValue,
                                 brandId: json["brand_id"].intValue,
                                 userId: json["user_id"].intValue,
                                 description: json["description"].stringValue,
                                 createdAt: json["created_at"].stringValue,
                                 updatedAt: json["updated_at"].stringValue,
                                 score: json["score"].intValue,
                                 upvotesCount: json["upvotes_count"].intValue,
                                 commentsCount: json["comments_count"].intValue,
                                 productId: json["product_id"].intValue,
                                 rethreadCount: json["rethread_count"].intValue,
                                 comments: comments,
                                 votes: votes,
                                 photoTags: photoTags,
                                 largeImageUrl: json["large_image_url"].stringValue,
                                 mediumImageUrl : json["medium_image_url"].stringValue,
                                 largeImageHeight: json["large_image_height"].floatValue,
                                 largeImageWidth: json["large_image_width"].floatValue,
                                 largeImageRatio: json["large_image_aspect_ratio"].floatValue,
                                 ownerImageUrl: json["owner_image_url"].stringValue,
                                 ownerUsername: json["owner_username"].stringValue,
                                 ownerSharePostCode:json["owner_share_post_code"].stringValue,
                                 watermarkImageUrl: json["watermark_image_url"].stringValue,
                                 likedByCurrentUser: json["liked_by_current_user"].boolValue)
    }
}

extension SocialInteraction: Equatable {
    public static func == (lhs: SocialInteraction, rhs: SocialInteraction) -> Bool {
        return lhs.id == rhs.id
    }
}

