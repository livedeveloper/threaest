//
//  Brand.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 21/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

public struct Brand {
    let id: Int
    let name: String
    let logoURL: String
    var isFollowing: Bool
    var isFollow: Bool
}

extension Brand: JSONConvertible {
    static func fromJSON(json: JSON) -> Brand {
        return Brand(id: json["id"].intValue,
                            name: json["name"].stringValue,
                            logoURL: json["brand_logo_image_url"].stringValue, isFollowing: json["is_following"].boolValue,isFollow:false)
    }
}
