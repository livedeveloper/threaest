//
//  CartItems.swift
//  Threadest
//
//  Created by Nasrullah Khan on 06/04/2017.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

public struct CartItem {
    let cartId: Int
    let price: Double
    let quantity: Int
    let tax: Double
    let id: Int
    let productid: Int
    let createdAt: String
    let updatedAt: String
    let productVariationId: Int
    let size: String
    let ownerId: Int
    let ownerType: Int
    let estimatedDelivery: String
    let product: CartProduct
}

extension CartItem: JSONConvertible {
    static func fromJSON(json: JSON) -> CartItem {

        let product = CartProduct.fromJSON(json: json["product"])

        return CartItem(cartId: json["cart_id"].intValue,
                         price: json["price"].doubleValue,
                         quantity: json["quantity"].intValue,
                         tax: json["tax"].doubleValue,
                         id: json["id"].intValue,
                         productid: json["product_id"].intValue,
                         createdAt: json["created_at"].stringValue,
                         updatedAt: json["updated_at"].stringValue,
                         productVariationId: json["product_variation_id"].intValue,
                         size: json["size"].stringValue,
                         ownerId: json["owner_id"].intValue,
                         ownerType: json["owner_type"].intValue,
                         estimatedDelivery: json["estimated_delivery"].stringValue,
                         product: product)
    }
}
