//
//  Suggestion.swift
//  Threadest
//
//  Created by Vladimir Goncharov on 08/06/2017.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

enum SuggestionType: Int {
    case unknown
    case facebook
    case contacts
    case popular
    
    private enum Key: String {
        case facebook = "facebook"
        case contacts = "contacts"
        case popular = "popular"
    }
    
    init(string: String) {
        switch string {
        case Key.facebook.rawValue: self = .facebook
        case Key.contacts.rawValue: self = .contacts
        case Key.popular.rawValue: self = .popular
        default: self = .unknown
        }
    }
    
    var stringValue: String {
        switch self {
        case .facebook: return Key.facebook.rawValue
        case .contacts: return Key.contacts.rawValue
        case .popular: return Key.popular.rawValue
        case .unknown: return ""
        }
    }
}

public struct Suggestion {
    let user: User
    let type: SuggestionType
}

extension Suggestion: JSONConvertible {
    static func fromJSON(json: JSON) -> Suggestion {
        return Suggestion(user: User.fromJSON(json: json["user"]),
                          type: SuggestionType(string: json["type"].stringValue))
    }
}
