//
//  Subscriber.swift
//  Threadest
//
//  Created by jigar on 24/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation

public struct SubScriber{
    
    let userID: Int
    let earlyAccessPosition : Int
    let email : String
    let shareUrl : String
    let thanksUrl : String
}
