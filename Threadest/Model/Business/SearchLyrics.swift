//
//  SearchLyrics.swift
//  Threadest
//
//  Created by Jaydeep on 29/06/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import ObjectMapper

class SearchLyrics: Mappable {
    var objectID: String?
    var title: String?
    var lyrics: String?
    var artist: String?
    var artwork: String?
    
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        objectID                    <- map["objectID"]
        title                       <- map["title"]
        lyrics                      <- map["lyrics"]
        artist                      <- map["artist"]
        artwork                     <- map["artwork"]
    }
}

