//
//  ShopCategory.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 24/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

public struct ShopCategory {
    let id: Int
    let text: String
    let title: String
    let category: String
    let createdAt: String
    let updatedAt: String
    let imageURL: String
}

extension ShopCategory: JSONConvertible {
    static func fromJSON(json: JSON) -> ShopCategory {
        return ShopCategory(id: json["id"].intValue,
                            text: json["text"].stringValue,
                            title: json["title"].stringValue,
                            category: json["category"].stringValue,
                            createdAt: json["created_at"].stringValue,
                            updatedAt: json["updated_at"].stringValue,
                            imageURL: json["image_url"].stringValue)
    }
}
