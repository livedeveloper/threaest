//
//  File.swift
//  Threadest
//
//  Created by Nasrullah Khan on 07/04/2017.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

public struct PaymentMethod {
    let userid: Int
    let id: Int
    let token: String
    let createdAt: String
    let updatedAt: String
    let defaultBool: Bool
    let thirdPartyAccountId: Int
    let thirdPartyAccessToken: String
    let brand: String
    let last4: String
    let paymentType: String
    let paymentObjectId: String
}

extension PaymentMethod: JSONConvertible {
    static func fromJSON(json: JSON) -> PaymentMethod {
        
        return PaymentMethod(userid: json["user_id"].intValue,
                    id: json["id"].intValue,
                    token: json["token"].stringValue,
                    createdAt: json["created_at"].stringValue,
                    updatedAt: json["updated_at"].stringValue,
                    defaultBool: json["default"].boolValue,
                    thirdPartyAccountId: json["third_party_account_id"].intValue,
                    thirdPartyAccessToken: json["third_party_access_token"].stringValue,
                    brand: json["brand"].stringValue,
                    last4: json["last4"].stringValue,
                    paymentType: json["payment_type"].stringValue,
                    paymentObjectId: json["payment_object_id"].stringValue)
    }
}
