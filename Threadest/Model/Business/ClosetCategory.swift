import Foundation
import UIKit
import SwiftyJSON

public struct ClosetCategory {
    let Id: Int
    let title: String
    let brandId: Int
    var commentsCount: Int
    let defaultPrice: Float
    let salePrice: Float
    let soldOut: Bool
    let brandName: String
    let brandLogoImageUrl: String
    let defaultMediumImageUrl: String
    let defaultMediumImageHeight: Float
    let defaultMediumImageWidth: Float
    let defaultImageUrl: String
    var votesCount: Int
    var likedByCurrentUser: Bool
    let productGalleryImages: [ProductGalleryImage]
    let productVariations: [ProductVariation]
    var comments: [Comment]
    var impressionsCount: Int
    let description: String
    let currentUserDrivenViews: Int
    let currentUserLoyaltyStatus: Int
    let currentUserDiscountAmount: Float
    let popup_exclusive: String
    let product_type: String
    let giveaway_button_text: String
    let winner_ids: [Int]
    // TODO: Add more if needed
}

extension ClosetCategory: JSONConvertible {
    static func fromJSON(json: JSON) -> ClosetCategory {
        debugPrint(json)
        var productGalleryImages = [ProductGalleryImage]()
        var comments = [Comment]()
        var winnerIds = [Int]()
    
        if let productGalleryArray = json["product_image_gallery"]["product_gallery_images"].array {
            productGalleryImages = productGalleryArray.map({return ProductGalleryImage.fromJSON(json: $0)})
        }
        
        var productVariations = [ProductVariation]()
        
        if let productVariationsArray = json["product_variations"].array {
            productVariations = productVariationsArray.map({return ProductVariation.fromJSON(json: $0)})
        }
        
        if let commentsArray = json["comments"].array {
            comments = commentsArray.map({return Comment.fromJSON(json: $0)})
        }
        
        if let winnderIDsArray = json["winner_ids"].array {
            winnerIds = winnderIDsArray.map({return $0.intValue})
        }
        
        return ClosetCategory(Id: json["id"].intValue,
                       title: json["title"].stringValue,
                       brandId: json["brand_id"].intValue,
                       commentsCount: json["comments_count"].intValue,
                       defaultPrice:json["default_price"].floatValue,
                       salePrice:json["sale_price"].floatValue,
                       soldOut:json["sold_out"].boolValue,
                       brandName:json["brand_name"].stringValue,
                       brandLogoImageUrl:json["brand_logo_image_url"].stringValue,
                       defaultMediumImageUrl: json["default_medium_image_url"].stringValue,
                       defaultMediumImageHeight: json["default_medium_image_height"].floatValue,
                       defaultMediumImageWidth: json["default_medium_image_width"].floatValue,
                       defaultImageUrl: json["default_image_url"].stringValue,
                       votesCount:json["votes_count"].intValue,
                       likedByCurrentUser: json["liked_by_current_user"].boolValue,
                       productGalleryImages:productGalleryImages,
                       productVariations:productVariations,
                       comments: comments, impressionsCount: json["impressions_count"].intValue,
                       description: json["description"].stringValue,
                       currentUserDrivenViews: json["current_user_driven_views"].intValue,
                       currentUserLoyaltyStatus: json["current_user_loyalty_status"].intValue, currentUserDiscountAmount: json["current_user_discount_amount"].floatValue,
                       popup_exclusive: json["popup_exclusive"].stringValue,
                       product_type: json["product_type"].stringValue,
                       giveaway_button_text: json["giveaway_button_text"].stringValue,
                       winner_ids: winnerIds)
    }
}
