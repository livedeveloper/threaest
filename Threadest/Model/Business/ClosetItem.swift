//
//  profileCloset.swift
//  Threadest
//
//  Created by Jaydeep on 25/03/17.
//  Copyright © 2017 JC. All rights reserved.
//


import UIKit
import SwiftyJSON

public struct ClosetItem {
    let userID: Int
    let id: Int
    let createdAt: String
    let updatedAt: String
    let upvotesCount: Int
    let mediumImageUrl: String
}

extension ClosetItem: JSONConvertible {
    static func fromJSON(json: JSON) -> ClosetItem {
        
        return ClosetItem(userID: json["user_id"].intValue,
                          id: json["id"].intValue,createdAt: json["created_at"].stringValue,updatedAt: json["updated_at"].stringValue,upvotesCount: json["upvotes_count"].intValue,
                             mediumImageUrl: json["medium_image_url"].stringValue)
    }
}
