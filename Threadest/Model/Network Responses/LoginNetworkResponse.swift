//
//  LoginNetworkResponse.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 15/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct LoginNetworkResponse {
    let hasSucceeded: Bool
    let message: String
    let hasPaymentMethod: Bool
    let userWaistSize: String
    let userID: Int
    let userImageURL: String
    let userMobilePhoneNumber: String
    let userShoeSize: String
    let username: String
    let userEmail: String
    let authToken: String
    let userShirtSize: String
    let userInvitePromoCode: String
    let userCartItemCount : Int
}

extension LoginNetworkResponse: JSONConvertible {
    static func fromJSON(json: JSON) -> LoginNetworkResponse {
        let success = json["status"].boolValue
        var message = ""
        
        // The message can be either an array or a single string
        if let serverMessage = json["message"].string {
            message = serverMessage
        } else if let serverMessageArray = json["message"].array {
            message = serverMessageArray.flatMap { return $0.stringValue }.joined(separator: "\n")
        }
        
        let data = json["data"]
        
        if data != JSON.null {
           
            UserDefaults.standard.set(data["cart_items_count"].intValue, forKey: "cartCount")
            UserDefaults.standard.synchronize()
            return LoginNetworkResponse(hasSucceeded: success,
                                        message: message,
                                        hasPaymentMethod: data["user_has_payment_method"].boolValue,
                                        userWaistSize: data["waist_size"].stringValue,
                                        userID: data["id"].intValue,
                                        userImageURL: data["image_url"].stringValue,
                                        userMobilePhoneNumber: data["mobile_phone_number"].stringValue,
                                        userShoeSize: data["shoe_size"].stringValue,
                                        username: data["username"].stringValue,
                                        userEmail: data["email"].stringValue,
                                        authToken: data["auth_token"].stringValue,
                                        userShirtSize: data["shirt_size"].stringValue,
                                        userInvitePromoCode: data["user_invite_promo_code"].stringValue,userCartItemCount: data["cart_items_count"].intValue)
        }
        
        return LoginNetworkResponse(hasSucceeded: false,
                                    message: message,
                                    hasPaymentMethod: false,
                                    userWaistSize: "",
                                    userID: -1,
                                    userImageURL: "",
                                    userMobilePhoneNumber: "",
                                    userShoeSize: "",
                                    username: "",
                                    userEmail: "",
                                    authToken: "",
                                    userShirtSize: "",
                                    userInvitePromoCode: "",userCartItemCount:0)
    }
}
