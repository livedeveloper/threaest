//
//  SuggestionsListResponse.swift
//  Threadest
//
//  Created by Vladimir Goncharov on 08/06/2017.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

public struct SuggestionsListResponse {
    let suggestions: [Suggestion]
}

extension SuggestionsListResponse: JSONConvertible {
    static func fromJSON(json: JSON) -> SuggestionsListResponse {
        let suggestions: [Suggestion] = json.array?.map({ return Suggestion.fromJSON(json: $0) }) ?? []
        return SuggestionsListResponse(suggestions: suggestions)
    }
}
