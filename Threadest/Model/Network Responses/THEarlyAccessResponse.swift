//
//  THEarlyAccessResponse.swift
//  Threadest
//
//  Created by jigar on 24/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

public struct THEarlyAccessResponse {

    let hasSucceeded: Bool
    let message: String
    let userID: Int
    let earlyAccessPosition : Int
    let email : String
    let shareUrl : String
    let thanksUrl : String
}

    extension THEarlyAccessResponse: JSONConvertible {
        static func fromJSON(json: JSON) -> THEarlyAccessResponse {
            let success = json["status"].boolValue
            var message = ""
            
            
            if let serverMessage = json["message"].string {
                message = serverMessage
            } else if let serverMessageArray = json["message"].array {
                message = serverMessageArray.flatMap { return $0.stringValue }.joined(separator: "\n")
            }
            
            var userID = 0
            var earlyAccessPosition = 0
            var email = ""
            var shareUrl = ""
            var thanksUrl = ""
            
            
            let data = json["data"]
            
            if data != JSON.null {
                userID = data["id"].intValue
                earlyAccessPosition = data["early_access_queue_position"].intValue
                email = data["email"].stringValue
                shareUrl = data["unique_share_url"].stringValue
                thanksUrl = data["thank_you_page_url"].stringValue
                
            }
            
            return THEarlyAccessResponse(hasSucceeded: success, message: message, userID: userID,earlyAccessPosition: earlyAccessPosition,email: email,shareUrl: shareUrl,thanksUrl: thanksUrl)
        
}
}
