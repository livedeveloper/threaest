//
//  RegisterNetworkResponse.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 13/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

public struct RegisterNetworkResponse {
    let hasSucceeded: Bool
    let message: String
    let userID: Int
    let username: String
    let userEmail: String
    let userPhoneNumber: String
    let authToken: String
    let userInvitePromoCode: String
    let userImageURL: String
    
    // TODO: Add more if needed
}

extension RegisterNetworkResponse: JSONConvertible {
    static func fromJSON(json: JSON) -> RegisterNetworkResponse {
        let success = json["status"].boolValue
        var message = ""
        
        // The message can be either an array or a single string
        if let serverMessage = json["message"].string {
            message = serverMessage
        } else if let serverMessageArray = json["message"].array {
            message = serverMessageArray.flatMap { return $0.stringValue }.joined(separator: "\n")
        }
        
        var userID = 0
        var username = ""
        var userEmail = ""
        var userPhoneNumber = ""
        var authToken = ""
        var userInvitePromoCode = ""
        var userImageURL = ""
        
        let data = json["data"]
        
        if data != JSON.null {
            userID = data["id"].intValue
            username = data["username"].stringValue
            userEmail = data["email"].stringValue
            userPhoneNumber = data["mobile_phone_number"].stringValue
            authToken = data["auth_token"].stringValue
            userInvitePromoCode = data["user_invite_promo_code"].stringValue
            userImageURL = data["image_url"].stringValue
        }
        
        return RegisterNetworkResponse(hasSucceeded: success, message: message, userID: userID, username: username, userEmail: userEmail, userPhoneNumber: userPhoneNumber, authToken: authToken, userInvitePromoCode: userInvitePromoCode, userImageURL: userImageURL)
    }
}
