//
//  THWelcomeRetailLocationVC.swift
//  Threadest
//
//  Created by Jaydeep on 05/03/18.
//  Copyright © 2018 JC. All rights reserved.
//

import UIKit
import Lottie
import SDWebImage


class THWelcomeRetailLocationVC: THBaseViewController {

    @IBOutlet weak var welcomeMessageLabel: UILabel!
    
    @IBOutlet weak var logoImageView: UIImageView!
    
    @IBOutlet weak var eventLabel: UILabel!
    
    
    @IBOutlet weak var subView: UIView!
    var locationId: Int!
    var retailLocation: RetailLocation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        subView.layer.cornerRadius = 10
        subView.layer.masksToBounds = true
        showAnimate()
        // Do any additional setup after loading the view.
        THUserManager.sharedInstance.retailLocation.first { (retail) -> Bool in
            if retail.id == locationId {
                retailLocation = retail
                let eventName = retailLocation.title == "" ? "Popup Shop":retailLocation.title
                welcomeMessageLabel.text = "Welcome to\n \(eventName)"
                logoImageView.sd_setImage(with: URL(string: retailLocation.brandLogoURL), placeholderImage: nil, options: .retryFailed, completed: nil)
                logoImageView.layer.cornerRadius = logoImageView.frame.size.width/2
                logoImageView.layer.masksToBounds = true
                eventLabel.text = "by \(retailLocation.brandName)"
                return true
            }
            return false
        }
    }
    
    @IBAction func didTapOnOkayButton(_ sender: Any) {
        self.removeAnimate()
    }
    
    
}
