//
//  THShareDetailTableViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 16/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SDWebImage
import CircleProgressView
import AVKit

class THShareDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
   // @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var socialPostImage: UIImageView!
    @IBOutlet weak var commentCountLabel: UILabel!
    @IBOutlet weak var upvoteCountLabel: UILabel!
    @IBOutlet weak var rethreadCountLabel: UILabel!
    
    @IBOutlet weak var commentLabel: UILabel!
    
    @IBOutlet weak var rethreadButtonTop: NSLayoutConstraint!
    @IBOutlet weak var rethreadButtonCenterY: NSLayoutConstraint!
    
    @IBOutlet weak var likeCountButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var rethreadButton: UIButton!
  
    @IBOutlet weak var trashButton: UIButton!
    @IBOutlet weak var circularProgressView: CircleProgressView!

    @IBOutlet weak var profile_nameButton: UIButton!

    var tagButton: UIButton = UIButton()

    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        profileImage.layer.cornerRadius = profileImage.frame.size.width/2
        profileImage.layer.masksToBounds = true
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }

    func configure(withSocialInteraction socialInteraction: SocialInteraction) {
        profileImage.sd_setImage(with: URL(string: socialInteraction.ownerImageUrl), placeholderImage: nil, options: .retryFailed)
        usernameLabel.text = socialInteraction.ownerUsername
        commentCountLabel.isHidden = socialInteraction.commentsCount > 0 ? false:true
        commentCountLabel.text = "\(socialInteraction.commentsCount)"
        upvoteCountLabel.isHidden = socialInteraction.upvotesCount > 0 ? false:true
        upvoteCountLabel.text = "\(socialInteraction.upvotesCount)"
        rethreadCountLabel.isHidden = socialInteraction.rethreadCount > 0 ? false: true
        rethreadCountLabel.text = "\(socialInteraction.rethreadCount)"
        likeButton.isSelected = socialInteraction.likedByCurrentUser ? true: false
        commentLabel.layer.cornerRadius = 5
        commentLabel.layer.masksToBounds = true
         commentLabel.text = ""
        if socialInteraction.comments.count > 0 {
            commentLabel.text = " \(socialInteraction.comments[0].comment) "
        }
    }
    
    func hideButtons() {
        self.likeButton.isHidden = true
        self.commentButton.isHidden = true
        self.rethreadButton.isHidden = true
        self.profile_nameButton.isHidden = true
        self.likeCountButton.isHidden = true
        self.commentLabel.isHidden = true
        self.commentCountLabel.isHidden = true
        self.profileImage.isHidden = true
        self.rethreadCountLabel.isHidden = true
        self.upvoteCountLabel.isHidden = true
    }
    
    func clearSocialSubview() {
        socialPostImage.subviews.forEach({ (button) in
            button.removeFromSuperview()
        })
    }

    @IBAction func likeActionButton(_ sender: UIButton) {
        if !sender.isSelected {
            sender.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                sender.transform = CGAffineTransform(rotationAngle: CGFloat(-179.9).degreesToRadians)
            })
            UIView.animate(withDuration: 0.3, delay: 0.2, options: UIViewAnimationOptions(), animations: {
                sender.transform = CGAffineTransform(rotationAngle: CGFloat(1.1).degreesToRadians)
            }, completion: { (completed) in
                var socialInteraction:SocialInteraction = THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[sender.tag]
                if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    THUserManager.sharedInstance.performUpvote(for: usercurrent.userId, votableId: socialInteraction.id, votableType: "Post", completion: { (vote, error) in
                        guard error != nil else {
                            socialInteraction.likedByCurrentUser = true
                            socialInteraction.upvotesCount = socialInteraction.upvotesCount + 1
                            socialInteraction.votes.append(vote!)
                            THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[sender.tag] = socialInteraction

                             self.upvoteCountLabel.text = "\(socialInteraction.upvotesCount)"
                            self.upvoteCountLabel.isHidden = false
                            sender.isSelected  = true
                            return
                        }
                    })
                }
                else {

                }
            })
        }
    }   
}

