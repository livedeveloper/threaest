//
//  THFollowPeopleTableViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 16/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THFollowPeopleTableViewCell: UITableViewCell {
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!


    func initLoad() {
        profileImage.layer.cornerRadius = profileImage.frame.size.width/2
        profileImage.layer.masksToBounds = true
    }


    func configure(withFollowPeople followPeople: FollowPeople) {
        profileImage.sd_setImage(with: URL(string: followPeople.thumbnailImageUrl), placeholderImage: nil, options: .retryFailed)
        nameLabel.text = followPeople.username
        usernameLabel.text = "@\(followPeople.username)"
        usernameLabel.text = "Threadsetter Score:\(followPeople.influencerScore)"
    }
}
