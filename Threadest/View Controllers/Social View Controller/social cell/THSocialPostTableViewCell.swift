//
//  THSocialPostTableViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 02/12/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SDWebImage
import CircleProgressView

class THSocialPostTableViewCell: UITableViewCell {
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var socialPostImage: UIImageView!
    @IBOutlet weak var commentCountLabel: UILabel!
    @IBOutlet weak var upvoteCountLabel: UILabel!
    @IBOutlet weak var rethreadCountLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    
    @IBOutlet weak var commentLabel1Top: NSLayoutConstraint!
    @IBOutlet weak var commentLabel1: UILabel!
    @IBOutlet weak var commentLabel2Top: NSLayoutConstraint!
    
    @IBOutlet weak var commentLabel2: UILabel!
    @IBOutlet weak var hashTagLabelTop: NSLayoutConstraint!
    @IBOutlet weak var hashTagLabel: UILabel!
    
    @IBOutlet weak var likeCountButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var rethreadButton: UIButton!
    @IBOutlet weak var rethreadButtonTop: UIButton!
    
    
    @IBOutlet weak var lblTrailingConstant: NSLayoutConstraint!
    @IBOutlet weak var trashButton: UIButton!
    @IBOutlet weak var circularProgressView: CircleProgressView!
    
    @IBOutlet weak var heightConstraintImage: NSLayoutConstraint!
    
    @IBOutlet weak var profile_nameButton: UIButton!
    
    var tagButton: UIButton = UIButton()
    
    
    //@IBOutlet weak var heightConstraintSocialImage: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        profileImage.layer.cornerRadius = profileImage.frame.size.width/2
        profileImage.layer.masksToBounds = true
        // Initialization code
    }
    
    func configure(withSocialInteraction socialInteraction: SocialInteraction) {
        profileImage.sd_setImage(with: URL(string: socialInteraction.ownerImageUrl), placeholderImage: nil, options: .retryFailed)
        usernameLabel.text = socialInteraction.ownerUsername
        commentCountLabel.isHidden = socialInteraction.commentsCount > 0 ? false:true
        commentCountLabel.text = "\(socialInteraction.commentsCount)"
        upvoteCountLabel.isHidden = socialInteraction.upvotesCount > 0 ? false:true
        upvoteCountLabel.text = "\(socialInteraction.upvotesCount)"
        rethreadCountLabel.isHidden = socialInteraction.rethreadCount > 0 ? false: true
        rethreadCountLabel.text = "\(socialInteraction.rethreadCount)"
        likeButton.isSelected = socialInteraction.likedByCurrentUser ? true: false
        timeLabel.text = GeneralMethods.sharedInstance.getTimeAgoStringFrom(dateString: socialInteraction.createdAt)
        
        switch socialInteraction.comments.count {
        case 0:
            commentLabel.text = ""
            commentLabel1.text = ""
            commentLabel2.text = ""
            break
        case 1:
            commentLabel1.text = ""
            commentLabel2.text = ""
            commentLabel1Top.constant = 0
            commentLabel2Top.constant = 0
            commentLabel.attributedText = GeneralMethods.sharedInstance.setAttributedStringWithColor_Font(text: "\(socialInteraction.comments[0].commentOwnerUsername) \(socialInteraction.comments[0].comment)", partOfString: socialInteraction.comments[0].commentOwnerUsername, color: UIColor.AppFontBlack(), font: UIFont.AppFontSemiBold(16))
            break
        case 2:
            commentLabel2.text = ""
            commentLabel1Top.constant = 5
            commentLabel2Top.constant = 0
            commentLabel.attributedText = GeneralMethods.sharedInstance.setAttributedStringWithColor_Font(text: "\(socialInteraction.comments[0].commentOwnerUsername) \(socialInteraction.comments[0].comment)", partOfString: socialInteraction.comments[0].commentOwnerUsername, color: UIColor.AppFontBlack(), font: UIFont.AppFontSemiBold(16))
            commentLabel1.attributedText = GeneralMethods.sharedInstance.setAttributedStringWithColor_Font(text: "\(socialInteraction.comments[1].commentOwnerUsername) \(socialInteraction.comments[1].comment)", partOfString: socialInteraction.comments[1].commentOwnerUsername, color: UIColor.AppFontBlack(), font: UIFont.AppFontSemiBold(16))
            break
        case 3:
            commentLabel1Top.constant = 5
            commentLabel2Top.constant = 5
            commentLabel.attributedText = GeneralMethods.sharedInstance.setAttributedStringWithColor_Font(text: "\(socialInteraction.comments[0].commentOwnerUsername) \(socialInteraction.comments[0].comment)", partOfString: socialInteraction.comments[0].commentOwnerUsername, color: UIColor.AppFontBlack(), font: UIFont.AppFontSemiBold(16))
            commentLabel1.attributedText = GeneralMethods.sharedInstance.setAttributedStringWithColor_Font(text: "\(socialInteraction.comments[1].commentOwnerUsername) \(socialInteraction.comments[1].comment)", partOfString: socialInteraction.comments[1].commentOwnerUsername, color: UIColor.AppFontBlack(), font: UIFont.AppFontSemiBold(16))
            commentLabel2.attributedText = GeneralMethods.sharedInstance.setAttributedStringWithColor_Font(text: "\(socialInteraction.comments[2].commentOwnerUsername) \(socialInteraction.comments[2].comment)", partOfString: socialInteraction.comments[2].commentOwnerUsername, color: UIColor.AppFontBlack(), font: UIFont.AppFontSemiBold(16))
            break
        default:
            break
        }
    }
    
    func clearSocialSubview() {
        socialPostImage.subviews.forEach({ (button) in
            button.removeFromSuperview()
        })
    }
    
    @IBAction func likeActionButton(_ sender: UIButton) {
        if !sender.isSelected {
            
            sender.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                sender.transform = CGAffineTransform(rotationAngle: CGFloat(-179.9).degreesToRadians)
            })
            UIView.animate(withDuration: 0.3, delay: 0.2, options: UIViewAnimationOptions(), animations: {
                sender.transform = CGAffineTransform(rotationAngle: CGFloat(1.1).degreesToRadians)
            }, completion: { (completed) in
                var socialInteraction:SocialInteraction = THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[sender.tag]
                if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    THUserManager.sharedInstance.performUpvote(for: usercurrent.userId, votableId: socialInteraction.id, votableType: "Post", completion: { (vote, error) in
                        guard error != nil else {
                            socialInteraction.likedByCurrentUser = true
                            socialInteraction.upvotesCount = socialInteraction.upvotesCount + 1
                            socialInteraction.votes.append(vote!)
                            THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[sender.tag] = socialInteraction
                            
                            self.upvoteCountLabel.text = "\(socialInteraction.upvotesCount)"
                            self.upvoteCountLabel.isHidden = false
                            sender.isSelected  = true
                            return
                        }
                    })
                }
                else {
                    
                }
            })
        }
    }
}
