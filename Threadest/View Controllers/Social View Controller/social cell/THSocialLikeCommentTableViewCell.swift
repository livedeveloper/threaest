//
//  THSocialLikeCommentTableViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 16/03/18.
//  Copyright © 2018 JC. All rights reserved.
//

import UIKit
import IoniconsSwift

class THSocialLikeCommentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var likeButton: DesignableButton!
    @IBOutlet weak var commentButton: DesignableButton!
    @IBOutlet weak var shareButton: DesignableButton!
    @IBOutlet weak var dollarButton: UIButton!
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var comment2: UILabel!
    @IBOutlet weak var comment3: UILabel!
    @IBOutlet weak var comment1Top: NSLayoutConstraint!
    @IBOutlet weak var comment2Top: NSLayoutConstraint!
    @IBOutlet weak var comment3Top: NSLayoutConstraint!
    @IBOutlet weak var profile_nameButton: UIButton!
    
    @IBOutlet weak var shareButtonLeading: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        likeButton.setImage(Ionicons.androidFavoriteOutline.image(30, color: .gray), for: .normal)
        likeButton.setImage(Ionicons.androidFavorite.image(30, color: .custColor(r: 233, g: 0, b: 55, a: 1)), for: .selected)
        commentButton.setImage(Ionicons.chatbubble.image(30, color: .gray), for: .normal)
        dollarButton.setImage(Ionicons.socialUsdOutline.image(25, color: .gray), for: .normal)
        commentLabel.addLineSpacing(spacing: 3)
        comment2.addLineSpacing(spacing: 3)
        comment3.addLineSpacing(spacing: 3)
        profileImage.layer.cornerRadius = profileImage.frame.size.width/2
        profileImage.layer.masksToBounds = true
    }
    
    func configureSocial(socialInteraction: SocialInteraction) {
         profileImage.sd_setImage(with: URL(string: socialInteraction.ownerImageUrl), placeholderImage: nil, options: .retryFailed)
        likeButton.isSelected = socialInteraction.likedByCurrentUser
        likeLabel.text = socialInteraction.upvotesCount == 0 ? "": socialInteraction.upvotesCount == 1 ? "\(socialInteraction.upvotesCount) Like" : "\(socialInteraction.upvotesCount) Likes"
        if socialInteraction.comments.count > 0 {
            commentLabel.numberOfLines = 2
            comment2.numberOfLines = 2
            comment3.numberOfLines = 2
            commentLabel.attributedText = GeneralMethods.sharedInstance.setAttributedStringWithColor_Font(text: "\(socialInteraction.comments[0].commentOwnerUsername) \(socialInteraction.comments[0].comment)", partOfString: socialInteraction.comments[0].commentOwnerUsername, color: UIColor.AppFontBlack(), font: UIFont.AppFontSemiBold(16))
            comment1Top.constant = 7
            comment2Top.constant = 0
            comment3Top.constant = 0
        } else {
            commentLabel.text = ""
            comment1Top.constant = 0
            comment2Top.constant = 0
            comment3Top.constant = 0
        }
        comment2.text = ""
        comment3.text = ""
        
        switch socialInteraction.comments.count {
        case 2:
            comment2.attributedText = GeneralMethods.sharedInstance.setAttributedStringWithColor_Font(text: "\(socialInteraction.comments[1].commentOwnerUsername) \(socialInteraction.comments[1].comment)", partOfString: socialInteraction.comments[1].commentOwnerUsername, color: UIColor.AppFontBlack(), font: UIFont.AppFontSemiBold(16))
            comment3.text = ""
            comment1Top.constant = 7
            comment2Top.constant = 7
            comment3Top.constant = 0
        case 3:
            comment2.attributedText = GeneralMethods.sharedInstance.setAttributedStringWithColor_Font(text: "\(socialInteraction.comments[1].commentOwnerUsername) \(socialInteraction.comments[1].comment)", partOfString: socialInteraction.comments[1].commentOwnerUsername, color: UIColor.AppFontBlack(), font: UIFont.AppFontSemiBold(16))
            comment3.attributedText = GeneralMethods.sharedInstance.setAttributedStringWithColor_Font(text: "\(socialInteraction.comments[2].commentOwnerUsername) \(socialInteraction.comments[2].comment)", partOfString: socialInteraction.comments[2].commentOwnerUsername, color: UIColor.AppFontBlack(), font: UIFont.AppFontSemiBold(16))
            comment1Top.constant = 7
            comment2Top.constant = 7
            comment3Top.constant = 7
        default:
            break
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
