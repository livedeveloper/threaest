//
//  THRethreadTableViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 11/04/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THRethreadTableViewCell: UITableViewCell {

    @IBOutlet weak var imageThreadest: UIImageView!
    @IBOutlet weak var nameLabel: UILabel! 
    @IBOutlet weak var rethreadSwitch: UISwitch!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
}
