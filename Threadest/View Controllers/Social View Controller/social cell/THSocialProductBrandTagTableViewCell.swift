//
//  THSocialProductBrandTagTableViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 02/12/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SDWebImage

class THSocialProductBrandTagTableViewCell: UITableViewCell {

    @IBOutlet weak var brandProductImage: UIImageView!
    
    @IBOutlet weak var brandProductNameLabel: UILabel!
    
    @IBOutlet weak var brandProductPriceLabel: UILabel!
    
    @IBOutlet weak var brandProductSalePriceLabel: UILabel!
    
    @IBOutlet weak var nextButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        brandProductImage.layer.cornerRadius = brandProductImage.frame.size.width/2
        brandProductImage.layer.masksToBounds = true
        brandProductImage.layer.borderWidth = 1
        brandProductImage.layer.borderColor = UIColor.lightGray.cgColor
        // Initialization code
    }
    
    func configure(photosTags: PhotoTag) {
        if photosTags.tagType == "Product Tag" {
            brandProductImage.sd_setImage(with: URL(string: photosTags.productImage), placeholderImage: nil, options: .retryFailed, completed: nil)
            brandProductNameLabel.text = photosTags.productName
            nextButton.isHidden = false
            brandProductPriceLabel.isHidden = false
            
            brandProductSalePriceLabel.textColor = UIColor(red: 47/255, green: 160/255, blue: 214/255, alpha: 1)
            
            if photosTags.productDefaultPrice != "" {
                brandProductPriceLabel.text = "$" + photosTags.productDefaultPrice
            }else{
                brandProductPriceLabel.text = photosTags.productDefaultPrice
            }
            
            if photosTags.productSalePrice != "" {
                
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(format: "$%@", photosTags.productSalePrice))
                attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
                brandProductSalePriceLabel.attributedText = attributeString
            }else{
                brandProductSalePriceLabel.text = photosTags.productSalePrice
            }
            
            
        } else {
            brandProductImage.sd_setImage(with: URL(string: photosTags.brandLogo), placeholderImage: nil, options: .retryFailed, completed: nil)
            brandProductNameLabel.text = photosTags.brandName
            nextButton.isHidden = false
            brandProductPriceLabel.isHidden = true
            brandProductSalePriceLabel.isHidden = true
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
