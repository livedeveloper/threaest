//
//  THVisionProductViewViewController.swift
//  Threadest
//
//  Created by Jaydeep on 24/12/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON
import ARKit

class THVisionProductViewViewController: UIViewController {
    @IBOutlet weak var objCollectionView: UICollectionView!
    @IBOutlet weak var imageViewOfVision: UIImageView!
    var brandProduct = [ClosetCategory]()
    var visionImage: UIImage!
    var brandId: Int = -1
    
    @IBOutlet weak var progressBar: UIProgressView!
    override func viewDidLoad() {
        super.viewDidLoad()
        objCollectionView.setCollectionViewLayout(CHTCollectionViewWaterfallLayout(), animated: true)
        objCollectionView.register(UINib(nibName: THSearchProductCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: THSearchProductCollectionViewCell.className)
        self.objCollectionView.keyboardDismissMode = .onDrag
        initUI()
        if visionImage == nil {
            return
        }
        
        if brandId == -1 {
            THNetworkManager.sharedInstance.uploadImageToCloudPlateform(image: visionImage, completion: { (json, error) in
                guard let json1 = json else {
                    return
                }
                let productData = json1["data"]
                if productData != JSON.null {
                    //Closet Category
                    if let productArray = productData.array {
                        self.brandProduct = productArray.map { return ClosetCategory.fromJSON(json: $0) }
                    }
                    DispatchQueue.main.async {
                        self.objCollectionView.reloadData()
                    }
                    
                }
            }, progressCompletion: { (progress) in
                DispatchQueue.main.async {
                    self.progressBar.progress = Float(progress)
                }
            })
        } else {
            THNetworkManager.sharedInstance.uploadImageBrandCloundPlateform(image: visionImage, brandId: brandId, completion: { (json, error) in
                guard let json1 = json else {
                    return
                }
                let productData = json1["data"]
                if productData != JSON.null {
                    //Closet Category
                    if let productArray = productData.array {
                        self.brandProduct = productArray.map { return ClosetCategory.fromJSON(json: $0) }
                    }
                    DispatchQueue.main.async {
                        self.objCollectionView.reloadData()
                    }
                    
                }
            }, progressCompletion: { (progress) in
                DispatchQueue.main.async {
                    self.progressBar.progress = Float(progress)
                }
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        appdelegate.verticle.scrollEnabled(isEnable: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//        appdelegate.verticle.scrollEnabled(isEnable: true)
    }

    
    func initUI() {
        imageViewOfVision.image = visionImage
        imageViewOfVision.layer.cornerRadius = 5
        imageViewOfVision.layer.borderColor = UIColor.hex("178dcd", alpha: 1).cgColor
        imageViewOfVision.layer.borderWidth = 1
    }

    
    @IBAction func didTapOnBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension THVisionProductViewViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedBrandId = brandProduct[indexPath.item].brandId
        let selectedProductId = brandProduct[indexPath.item].Id
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THBrandProductManager.sharedInstance.retrieveBrandProfile(with: selectedBrandId, productId: selectedProductId, geofence: false) { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            let threeDAvailable = THBrandProductManager.sharedInstance.brandProduct?.productGalleryImages.contains(where: { (imgGallery) -> Bool in
                if imgGallery.image_type == "3D" {
                    return true
                } else {
                    return false
                }
            })
            
            if threeDAvailable! {
                if #available(iOS 11.0, *) {
                    if (ARConfiguration.isSupported) {
                        let ARShopShowController = THARShopShowViewController(nibName: THARShopShowViewController.className, bundle: nil)
                        ARShopShowController.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                        self.navigationController?.pushViewController(ARShopShowController, animated: true)
                        return
                    }
                }
            }
            let shopshowcontroller = THShopshowViewController(nibName: THShopshowViewController.className, bundle: nil)
            shopshowcontroller.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
            self.navigationController?.pushViewController(shopshowcontroller, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
       /* if isSearch == false {
            if indexPath.item == brandProduct.count - 1 {
                if THBrandsManager.sharedInstance.didLoadData {
                    THBrandsManager.sharedInstance.retrieveBrand(for: brandId, isFromCloset: false, completion: { (cartBrand, error) in
                        if(error==nil) {
                            self.brandProduct = THBrandsManager.sharedInstance.products
                            self.objCollectionview.reloadData()
                        }
                    })
                }
            }
        }*/
    }
}


extension THVisionProductViewViewController: UICollectionViewDataSource, CHTCollectionViewDelegateWaterfallLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return brandProduct.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: THSearchProductCollectionViewCell.className, for: indexPath) as! THSearchProductCollectionViewCell
        cell.configure(closetCategory: brandProduct[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        let imgHeight = CGFloat(brandProduct[indexPath.row].defaultMediumImageHeight)
        let imgWidth = CGFloat(brandProduct[indexPath.row].defaultMediumImageWidth)
        let newHeight = (imgHeight/imgWidth)*gridWidth
        return CGSize(width: gridWidth, height: newHeight+48)
    }
    
    func colletionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, heightForHeaderInSection section: NSInteger) -> CGFloat {
        return 0
    }
}

