//
//  THVisionSearchBrandViewController.swift
//  Threadest
//
//  Created by Jaydeep on 24/12/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import AlgoliaSearch
import ObjectMapper
import Crashlytics
import IoniconsSwift
import SDWebImage
import IQKeyboardManagerSwift

class THVisionSearchBrandViewController: UIViewController {
    @IBOutlet weak var objSearchTextField: UITextField!
    @IBOutlet weak var objTableView: UITableView!
    
    var visionImage: UIImage!
    
    var page:UInt = 0
    var maxNoOfPages:UInt = 0
    var maxNoOfPagesAll:UInt = 0
    var pageAll:UInt = 0
    var searchedResuls:NSArray = []
    var searchQuery:THSearchQueryManager?
    var selectedSearchType:String = "Brand"
    let kStringConstantAll:String = "All"
    var postTagCompletion : PostTagCompletion?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        appdelegate.verticle.scrollEnabled(isEnable: false)
        IQKeyboardManager.sharedManager().enable = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//        appdelegate.verticle.scrollEnabled(isEnable: true)
        IQKeyboardManager.sharedManager().enable = true
    }
    
    private func initUI() {
        objTableView.register(UINib(nibName: THSearchResultTableViewCell.className, bundle: nil), forCellReuseIdentifier: THSearchResultTableViewCell.className)
        objTableView.rowHeight = 60;
        self.objTableView.keyboardDismissMode = .onDrag
        self.searchQuery = THSearchQueryManager()
        self.searchResults(search: "", searchType: self.selectedSearchType)
        loadMore()
    }
    
    func searchResults(search:String, searchType:String){
        self.page = 0
        self.searchQuery?.query?.page = self.page
        self.searchQuery?.query?.filters = ""
        self.searchQuery?.queryResults(search: search, searchType: searchType) { (searchList, nbPage) in
            self.searchedResuls = []
            self.searchedResuls = searchList as! NSArray
            if(self.searchedResuls.count>0) {
                self.objTableView.isHidden = false
            } else {
                self.objTableView.isHidden = true
            }
            self.maxNoOfPages = nbPage
            self.objTableView.reloadData()
        }
    }
    
    func fetchMoreResults() {
        self.searchQuery?.query?.page = self.page
        self.searchQuery?.query?.filters = ""
        self.searchQuery?.queryResults(search: self.objSearchTextField.text!, searchType: self.selectedSearchType) { (searchList, nbPage) in
            let moreResults:NSArray = searchList as! NSArray
            let finalArray:NSMutableArray = []
            for (index, element) in self.searchedResuls.enumerated() {
                print("Item \(index): \(element)")
                finalArray.add(element)
            }
            for (index, element) in moreResults.enumerated() {
                print("Item \(index): \(element)")
                finalArray.add(element)
            }
            self.searchedResuls = finalArray as NSArray
            self.maxNoOfPages = nbPage
            self.objTableView.reloadData()
            self.objTableView.infiniteScrollingView.stopAnimating();
        }
    }
    
    func loadMore() {
        self.objTableView.addInfiniteScrolling {
            self.page+=1
            if (self.page  >= self.maxNoOfPages) {
                self.objTableView.infiniteScrollingView.stopAnimating();
                return // All pages already loaded
            } else {
                self.fetchMoreResults()
            }
        }
    }
    
    @IBAction func didTaponCloseButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - TableView Delegate Methods
extension THVisionSearchBrandViewController:UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tappedObj = self.searchedResuls[indexPath.row] as! SearchBrand
        if tappedObj is SearchBrand {
            let brand = tappedObj as! SearchBrand
            let visionVC = THVisionProductViewViewController(nibName: THVisionProductViewViewController.className, bundle: nil)
            visionVC.visionImage = self.visionImage
            visionVC.brandId = Int(tappedObj.objectID!)!
            self.navigationController?.pushViewController(visionVC, animated: true)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - TableView DataSource Methods
extension THVisionSearchBrandViewController:UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.searchedResuls as AnyObject).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: THSearchResultTableViewCell.className, for: indexPath) as! THSearchResultTableViewCell
        let obj = self.searchedResuls[indexPath.row] as! SearchBrand
        cell.nameLabel.text = obj.name
        if let brandLogo = obj.brand_logo_image_url {
            cell.searchImageView.sd_setImage(with: URL(string: brandLogo), placeholderImage: nil, options: .retryFailed, completed: nil)
        }
        return cell
    }
}


extension THVisionSearchBrandViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let search = textFieldText.replacingCharacters(in: range, with: string)
        self.loadMore()
        self.searchResults(search: search, searchType: self.selectedSearchType)
        return true;
    }
}

