//
//  THSocialDropdownMenu.swift
//  Threadest
//
//  Created by Blue Star on 3/2/18.
//  Copyright © 2018 JC. All rights reserved.
//

import UIKit

class THSocialDropdownMenu: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var menuListTable: UITableView!
    @IBOutlet weak var popUpView: UIView!
    
    var popupPosition = CGPoint(x: 16, y: -20)
    
    var menuList = [
        ["menu_icon_profile", "Profile"],
        ["menu_icon_addfriends", "Add Friends"],
        ["menu_icon_score", "Your Score"],
        ["menu_icon_shop", "Pop-Up Shops"],
        ["menu_icon_settings", "Settings"]
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        menuListTable.estimatedRowHeight = 42
        menuListTable.rowHeight = 42
        menuListTable.register(UINib(nibName: THSocialDropDownMenuCell.className, bundle: nil), forCellReuseIdentifier: THSocialDropDownMenuCell.className)        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showAnimate() {
        popUpView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        popUpView.frame.origin = popupPosition
        UIView.animate(withDuration: 0.25, animations: {
            self.popUpView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            self.popUpView.frame.origin = self.popupPosition
        }, completion:{(finished : Bool)  in
            if finished {
                if let parentVC = self.parent as? THSocialViewController {
                    parentVC.dropdownShowing = true                    
                }
            }
        });
    }
    
    func removeAnimate() {
        UIView.animate(withDuration: 0.25, animations: {
            self.popUpView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            self.popUpView.frame.origin = self.popupPosition
        }, completion:{(finished : Bool)  in
            if finished {
                if let parentVC = self.parent as? THSocialViewController {
                    parentVC.dropdownShowing = false
                    parentVC.dropdownViewController = nil
                    self.view.removeFromSuperview()
                    self.removeFromParentViewController()
                }
            }
        });
    }
   
    @IBAction func didTapOnRemoveButton(_ sender: Any) {
        removeAnimate()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = menuListTable.dequeueReusableCell(withIdentifier: THSocialDropDownMenuCell.className, for: indexPath) as! THSocialDropDownMenuCell
        var score = -1
        if indexPath.row == 2 {
            if let currentUser = THUserManager.sharedInstance.currentUserdata() {
                if let infuencerScoreDefault = UserDefaults.standard.object(forKey: userDefaultKey.influencerScoreDefault.rawValue) {
                    score = infuencerScoreDefault as! Int
                } else {
                    score = currentUser.influencerScore
                }
            }
        }
        cell.setupWith(imageName: menuList[indexPath.row][0], title: menuList[indexPath.row][1], score: score)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == menuList.count - 1 {
            cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, cell.bounds.size.width)
        } else {
            cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0: // show profile
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let profileVC = storyBoard.instantiateViewController(withIdentifier: PZProfileTableViewController.className)
            let navprofilePostVC = UINavigationController(rootViewController: profileVC)
            self.present(navprofilePostVC, animated: true, completion: nil)
            break
        case 1: // add friend
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                if usercurrent.tempAccount {
                    appdelegate.setupAccountPopup(viewController: self)
                    break
                }
            }
            let inviteNewVC = THFindFriendInviteViewController(nibName: THFindFriendInviteViewController.className, bundle: nil)
            let navInvite = UINavigationController(rootViewController: inviteNewVC)
            self.present(navInvite, animated: true, completion: nil)
            break
        case 2: // leader board
            let BrandVC = THBlackBrandViewController(nibName: THBlackBrandViewController.className, bundle: nil)
            BrandVC.isMyBrand = "my"
            let navBrandVC = UINavigationController(rootViewController: BrandVC)
            self.present(navBrandVC, animated: true, completion: nil)
            break
        case 3:
            if let parentVC = self.parent as? THSocialViewController {
                parentVC.popupShops()
                return
            }
        case 4: // show settings
            let settingVC = THSettingsViewController(nibName: THSettingsViewController.className, bundle: nil)
            let navSettingVC = UINavigationController(rootViewController: settingVC)
            self.present(navSettingVC, animated: true, completion: nil)
            break
        case 5: //show notification
            let noficationController = THNotificationController(nibName: THNotificationController.className, bundle: nil)
            let navNotificationController = UINavigationController(rootViewController: noficationController)
            self.present(navNotificationController, animated: true, completion: nil)
            break
        default:
            break
        }
        removeAnimate()
    }
}
