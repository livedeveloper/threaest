//
//  THSocialDropDownMenuCell.swift
//  Threadest
//
//  Created by Blue Star on 3/4/18.
//  Copyright © 2018 JC. All rights reserved.
//

import UIKit

class THSocialDropDownMenuCell: UITableViewCell {    

    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWith(imageName: String, title: String, score: Int) {
        iconImage.image = UIImage(named: imageName)
        titleLabel.text = title
        if score < 0 {
            scoreLabel.isHidden = true
        } else {
            scoreLabel.isHidden = false
            scoreLabel.text = "+\(score)"
        }
    }
    
}
