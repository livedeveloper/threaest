//
//  THSocialPostPopup.swift
//  Threadest
//
//  Created by Jaydeep on 09/06/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THSocialPostPopup: THBaseViewController {

    open var sharePostPopupCompletionHander : (()->())?
    @IBOutlet weak var sharePostView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        sharePostView.layer.cornerRadius = 8
        sharePostView.layer.masksToBounds = true
        self.showAnimate()
        
        userdefault.set(Date(), forKey: userDefaultKey.lastSharePostDateDefault.rawValue)
        userdefault.synchronize()
        
    }

    @IBAction func didTapOnCloseButton(_ sender: Any) {
        self.removeAnimate()
    }

    @IBAction func didTapOutside(_ sender: Any) {
        self.removeAnimate()
    }

    @IBAction func didTapOnCameraButton(_ sender: Any) {
        self.removeAnimate()
        sharePostPopupCompletionHander!()
    }
    
    
    @IBAction func didTapOnSkipButton(_ sender: Any) {
        self.removeAnimate()
    }
}
