//
//  THSocialViewController.swift
//  Threadest
//
//  Created by Jaydeep on 10/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import Social
import SDWebImage
import SwiftyJSON
import CoreLocation
import Pulsator
import TwitterKit
import Crashlytics
import UserNotifications
import Contacts
import SVPullToRefresh
import StoreKit
import ARKit
import Lottie

class THSocialViewController: UIViewController, UIGestureRecognizerDelegate
{
    //outlet tool tip view
    @IBOutlet var toolTipView: UIView!
    @IBOutlet weak var toolTipLabel: UILabel!
    var popOverTagToolTip = Popover()

    @IBOutlet var productToolTipView: UIView!
    @IBOutlet var brandToolTipView: UIView!
    @IBOutlet weak var productToolTipLabel: UILabel!
    @IBOutlet weak var priceToolTipLabel: UILabel!
    @IBOutlet weak var salePriceToolTipLabel: UILabel!

    @IBOutlet weak var productToolTipLogo: UIImageView!

    @IBOutlet weak var brandToolTipLabel: UILabel!
    @IBOutlet weak var brandToolTipLogo: UIImageView!

    @IBOutlet weak var badgeLabel: UILabel!

    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!

    var popOverProductTagToolTip = Popover()
    var popOverBrandTagToolTip = Popover()

    enum ScrollDirectionType {
        case ScrollDirectionNone
        case ScrollDirectionRight
        case ScrollDirectionLeft
    }
    var scrollDirection : ScrollDirectionType = .ScrollDirectionNone
    var selectedCollectionView : UICollectionView?

    var pageUser:Int = 1
    var countUser:Int = 0

    @IBOutlet weak var hiddenTabbarMiddleButton: UIButton!

    @IBOutlet weak var cameraButton: UIButton!

    //Constraint constant
    @IBOutlet weak var headerTop: NSLayoutConstraint!
    @IBOutlet weak var addFriendCountLabel: UILabel!
    @IBOutlet weak var notificationCountLabel: UILabel!
    @IBOutlet weak var leadershipCountLabel: UILabel!
    @IBOutlet weak var cartCountLabel: UILabel!

    @IBOutlet weak var shirtButton: UIButton!

    @IBOutlet weak var objTableView: UITableView!
    public var productObject : CreateProduct!
    public var isFromCamera:Bool = false
    var locationManager : CLLocationManager!

    @IBOutlet weak var postedImage: UIImageView!
    @IBOutlet weak var retryCreatePostButton: DesignableButton!

    @IBOutlet weak var progressImgage: UIProgressView!

    @IBOutlet weak var postingView: UIView!
    var refreshControl: UIRefreshControl!
    //outlet threadest bar
    @IBOutlet weak var threadsPostsSwitch: UISwitch!

    //variable for drop down
    var dropdownShowing = false
    var dropdownViewController: THSocialDropdownMenu!

    var postsSocialInteraction = [SocialInteraction]()
    var threadsPostsIndexPath = IndexPath(row: 0, section: 1)
    var postsIndexPath = IndexPath(row: 0, section: 1)


    //variable for tabbar
    var lastContentOffset: CGFloat = 0.0
    var tabbarOffset: CGFloat!

    //variable for header
    var lasContentHeaderOffset: CGFloat!
    var headerOffset: CGFloat!
    var infuencerTimer: Timer!
    var influencerScore = 0
    var newInfluencerScore = 0
    var pulsator = Pulsator()

    //bulletin board
    var bulletinManager: BulletinManager!

    var isCurrentLocationDone : Bool!
    var isShirtOn = true
    var isShowTag = false
    var selectedPostIndex = 0
    var selectedPhotoTag: PhotoTag?
    var selectedSocialInteraction: SocialInteraction?
    let delegate = UIApplication.shared.delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()
        tabbarOffset = screenheight - 49
        headerOffset = 0
        objTableView.register(UINib(nibName: THFollowPeopleTableViewCell.className, bundle: nil), forCellReuseIdentifier: THFollowPeopleTableViewCell.className)
        objTableView.register(UINib(nibName: THShareDetailTableViewCell.className, bundle: nil), forCellReuseIdentifier: THShareDetailTableViewCell.className)
        objTableView.register(UINib(nibName: THSocialProductBrandTagTableViewCell.className, bundle: nil), forCellReuseIdentifier: THSocialProductBrandTagTableViewCell.className)
        objTableView.register(UINib(nibName: THListOfSuggestionUsersCell.className, bundle: nil), forCellReuseIdentifier: THListOfSuggestionUsersCell.className)
        objTableView.register(UINib(nibName: THSocialLikeCommentTableViewCell.className, bundle: nil), forCellReuseIdentifier: THSocialLikeCommentTableViewCell.className)

        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(THSocialViewController.refresh(sender:)), for: UIControlEvents.valueChanged)
        objTableView.addSubview(refreshControl)
        initUI()

        postingView.isHidden = true

        if UserDefaults.standard.bool(forKey: "isSignupKey"){
            DispatchQueue.main.async {
                self.bulletinWelcomePopup()
            }
        }else{
            self.socialScrollViewWillAppear()
            self.updateNotificationCount()
        }

        //setup influencer score
        if let currentUser = THUserManager.sharedInstance.currentUserdata() {
            if let infuencerScoreDefault = UserDefaults.standard.object(forKey: userDefaultKey.influencerScoreDefault.rawValue) {
                self.leadershipCountLabel.text = "+\(infuencerScoreDefault)  "
                influencerScore = infuencerScoreDefault as! Int
            } else {
                self.leadershipCountLabel.text = "+\(currentUser.influencerScore)  "
                influencerScore = currentUser.influencerScore
            }
        }

        THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts = [SocialInteraction]()
            THSocialInteractionPostsManager.sharedInstance.page = 1
            THSocialInteractionPostsManager.sharedInstance.didLoadData = false
        THSocialInteractionPostsManager.sharedInstance.retrieveSocialInteractionPosts { (error) in
            guard let error = error else {
                self.saparatePostsBasedOntags()
                return
            }
            debugPrint(error)
        }
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self

        NotificationCenter.default.removeObserver(self, name: Notification.Name(postNotificationType.notificationPost.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(THSocialViewController.notificationPostLikeComment(notification:)), name: Notification.Name(postNotificationType.notificationPost.rawValue), object: nil)
        NotificationCenter.default.addObserver(forName:NSNotification.Name(rawValue: "MyNotification"), object:nil, queue:nil, using:scrollToTop)
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        NotificationCenter.default.addObserver(self, selector: #selector(THSocialViewController.postNoficationInfluencerScore(notification:)), name: Notification.Name(postNotificationType.notificationInfluenceScore.rawValue), object: nil)
        self.checkCurrentInfluencerScore()
        saparatePostsBasedOntags()
        //for updating location

        if(isFromCamera == true) {
            self.isFromCamera = false
            postingView.isHidden = false
            self.postingView.alpha = 1.0
            retryCreatePostButton.isHidden = true
            postedImage.image = productObject.product_image
            self.objTableView.setContentOffset(.zero, animated: true)
            createPost()
        } else {
            postingView.isHidden = true
        }

        if let cartCount = UserDefaults.standard.value(forKey: "cartCount") {
            if (cartCount as! Int) == 0 {
                self.cartCountLabel.isHidden = true
            } else {
                self.cartCountLabel.isHidden = false
                self.cartCountLabel.text = "\(cartCount as! Int)"
            }
        } else {
            self.cartCountLabel.isHidden = true
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        self.isFromCamera = false
        leadershipCountLabel.layer.cornerRadius = leadershipCountLabel.frame.width / 2.0
        leadershipCountLabel.layer.masksToBounds = true
        if userdefault.object(forKey: userDefaultKey.lastSharePostDateDefault.rawValue) == nil {
            userdefault.set(Date(), forKey: userDefaultKey.lastSharePostDateDefault.rawValue)
            userdefault.synchronize()
        } else {
            let lastSavedPostDate = (userdefault.object(forKey: userDefaultKey.lastSharePostDateDefault.rawValue) as! Date).addingTimeInterval(86400)
            if (Date().compare(lastSavedPostDate) == .orderedDescending ){
                self.sharePostPopup()
            }
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        if self.popOverTagToolTip.frame != CGRect.zero {
            self.popOverTagToolTip.dismiss()
        }
        NotificationCenter.default.removeObserver(self, name: Notification.Name(postNotificationType.notificationInfluenceScore.rawValue), object: nil)
        let imageCache = SDImageCache.shared()
        imageCache.clearMemory()
    }

    override func didReceiveMemoryWarning() {
        let imageCache = SDImageCache.shared()
        imageCache.clearMemory()
    }

    func scrollToTop(notification:Notification) -> Void {

        if self.objTableView.numberOfSections > 0 {
            let top : IndexPath = IndexPath.init(row:NSNotFound, section: 0)
            self.objTableView.scrollToRow(at: top, at: .top, animated: true)
        }
    }

    func bulletinWelcomePopup() {
        self.bulletinManager = BulletinManager(rootItem: GeneralMethods.sharedInstance.bulletinWelcomePopup(FirstCompletion: {
            DispatchQueue.main.async {
                self.bulletinManager.dismissBulletin(animated: true, completion: {

                    DispatchQueue.main.async {
                        self.bulletinFirstPopup()
                    }
                })
            }
        }, SecondCompletion: {
            DispatchQueue.main.async {
                self.bulletinManager.dismissBulletin(animated: true, completion: {
                    self.bulletinSecondPopup()
                })
            }
        }, ThirdCompletion: {
            DispatchQueue.main.async {
                self.bulletinManager.dismissBulletin(animated: true, completion: {
                    self.bulletinThirdPopup()
                })
            }
        }))

        self.bulletinManager.prepare()
        self.bulletinManager.presentBulletin(above: self.tabBarController ?? self)
    }

    func bulletinFirstPopup() {
        self.bulletinManager = BulletinManager(rootItem: GeneralMethods.sharedInstance.bulletinFirstPopup(ContinueCompletion: {
            DispatchQueue.main.async {
                self.bulletinManager.dismissBulletin(animated: true, completion: {
                    userdefault.set(false, forKey: "isSignupKey")
                    userdefault.synchronize()
                    DispatchQueue.main.async {
                        self.socialScrollViewWillAppear()
                        self.updateNotificationCount()
                    }
                })
            }
        }))

        self.bulletinManager.prepare()
        self.bulletinManager.presentBulletin(above: self.tabBarController ?? self)
    }

    func bulletinSecondPopup() {
        self.bulletinManager = BulletinManager(rootItem: GeneralMethods.sharedInstance.bulletinSecondPopup(ContinueCompletion: {
            DispatchQueue.main.async {
                self.bulletinManager.dismissBulletin(animated: true, completion: {
                    userdefault.set(false, forKey: "isSignupKey")
                    userdefault.synchronize()
                    DispatchQueue.main.async {
                        self.socialScrollViewWillAppear()
                        self.updateNotificationCount()
                    }
                })
            }
        }))

        self.bulletinManager.prepare()
        self.bulletinManager.presentBulletin(above: self.tabBarController ?? self)
    }

    func bulletinThirdPopup() {
        self.bulletinManager = BulletinManager(rootItem: GeneralMethods.sharedInstance.bulletinThirdPopup(ContinueCompletion: {
            DispatchQueue.main.async {
                self.bulletinManager.dismissBulletin(animated: true, completion: {
                    userdefault.set(false, forKey: "isSignupKey")
                    userdefault.synchronize()
                    DispatchQueue.main.async {
                        self.socialScrollViewWillAppear()
                        self.updateNotificationCount()
                    }
                })
            }
        }))

        self.bulletinManager.prepare()
        self.bulletinManager.presentBulletin(above: self.tabBarController ?? self)
    }


    func socialScrollViewWillAppear() {
        /*if self.presentedViewController == nil {
         if self.popOverTagToolTip.frame == CGRect.zero {
         if self.postsSocialInteraction.count > 0 {
         if #available(iOS 10.0, *) {
         let current = UNUserNotificationCenter.current()
         current.getNotificationSettings(completionHandler: { (settings) in
         if settings.authorizationStatus == .notDetermined {
         } else {
         self.showToolTip()
         }
         })
         } else {
         // Fallback on earlier versions
         if UIApplication.shared.isRegisteredForRemoteNotifications {
         self.showToolTip()
         }
         }
         }
         }
         }*/

        /* self.updateNotificationCount()
         //setup influencer score
         if let currentUser = THUserManager.sharedInstance.currentUserdata() {
         if let infuencerScoreDefault = UserDefaults.standard.object(forKey: userDefaultKey.influencerScoreDefault.rawValue) {
         self.leadershipCountLabel.text = "+\(infuencerScoreDefault)  "
         influencerScore = infuencerScoreDefault as! Int
         } else {
         self.leadershipCountLabel.text = "+\(currentUser.influencerScore)  "
         influencerScore = currentUser.influencerScore
         }
         }*/


        if userdefault.object(forKey: userDefaultKey.bulletinVideoTutorial.rawValue) == nil {
            DispatchQueue.main.async {
                self.bulletinVideoOnBoardPopup()//bulletinNotificationBoardPopup()
            }
            return
            /*if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                if usercurrent.tempAccount {

                }
            }*/
        }
        if #available(iOS 10.0, *) {
            let current = UNUserNotificationCenter.current()
            current.getNotificationSettings(completionHandler: { (settings) in
                if settings.authorizationStatus == .notDetermined {
                    DispatchQueue.main.async {
                        self.bulletinNotificationBoardPopup()
                    }
                }
            })
        } else {
            // Fallback on earlier versions
            if UIApplication.shared.isRegisteredForRemoteNotifications {
                print("APNS-YES")
            } else {
                DispatchQueue.main.async {
                    self.bulletinNotificationBoardPopup()
                }
            }
        }
    }

    //get post notification of infuence score
    func postNoficationInfluencerScore(notification: NSNotification) {
        let postNotificationJson = JSON(notification.object!)

        newInfluencerScore = postNotificationJson["new_influencer_score"].intValue
        if influencerScore >= newInfluencerScore {
            return
        }
        if infuencerTimer == nil {
            infuencerTimer = Timer.scheduledTimer(timeInterval: 0.08, target: self, selector: #selector(THSocialViewController.timercounterOfInfluencerScore), userInfo: nil, repeats: true)
            leadershipCountLabel.backgroundColor = UIColor.red
            pulsator.numPulse = 3
            pulsator.radius = 50.0
            pulsator.backgroundColor = UIColor.white.cgColor
            leadershipCountLabel.layer.addSublayer(pulsator)
            pulsator.start()
        }
    }

    func bulletinWelcomeSocialContactPopup() {
        self.bulletinManager = BulletinManager(rootItem: GeneralMethods.sharedInstance.bulletinWelcomeSocialToAccessContact(AllowCompletion: {
            DispatchQueue.main.async {
                self.bulletinManager.dismissBulletin(animated: true, completion: {
                    let inviteNewVC = THFindFriendInviteViewController(nibName: THFindFriendInviteViewController.className, bundle: nil)
                    inviteNewVC.isFromContactBulletin = true
                     inviteNewVC.completionHander = {
                        DispatchQueue.main.async {
                            //self.bulletinNotificationBoardPopup()
                        }
                    }
                    let navInvite = UINavigationController(rootViewController: inviteNewVC)
                    navInvite.setNavigationBarHidden(true, animated: false)
                    self.present(navInvite, animated: true, completion: nil)
                })
            }
        }, dontAllowCompletion: {
            DispatchQueue.main.async {
                self.bulletinManager.dismissBulletin(animated: true, completion: {
                    /*let userPickerFollowViewController = THUserPickerFollowViewController(nibName: THUserPickerFollowViewController.className, bundle: nil)
                    userPickerFollowViewController.completionHander = {
                        DispatchQueue.main.async {
                            //self.bulletinNotificationBoardPopup()
                        }
                    }
                    self.navigationController?.pushViewController(userPickerFollowViewController, animated: true)*/
                })
            }
        }))
        self.bulletinManager.prepare()
        self.bulletinManager.presentBulletin(above: self.tabBarController ?? self)
    }

    //play video bulletin
    func bulletinVideoOnBoardPopup() {
        self.bulletinManager = BulletinManager(rootItem: GeneralMethods.sharedInstance.bulletinPlayVideo(WatchCompletion: {
            DispatchQueue.main.async {
                self.bulletinManager.dismissBulletin(animated: true, completion: {
                    userdefault.set(true, forKey: userDefaultKey.bulletinVideoTutorial.rawValue)
                    userdefault.synchronize()
                    DispatchQueue.main.async {
                        self.presentNotification()
                    }
                })
            }
        }, SkipCompletion: {
            DispatchQueue.main.async {
                self.bulletinManager.dismissBulletin(animated: true, completion: {
                    self.presentNotification()
                })
            }
        }))

        self.bulletinManager.prepare()
        self.bulletinManager.presentBulletin(above: self.tabBarController ?? self)
    }

    //bulletin board popup
    func bulletinNotificationBoardPopup()  {
        self.bulletinManager = BulletinManager(rootItem: GeneralMethods.sharedInstance.bulletinNotificationThreadest(AllowCompletion: {
            DispatchQueue.main.async {
                self.bulletinManager.dismissBulletin(animated: true, completion: {
                    THUserManager.sharedInstance.requestPushNotificationPermissions(completion: { granted in
                        self.presentContactNotificationBulletin()
                    })

                })
            }
        }, dontAllowCompletion: {
            DispatchQueue.main.async {
                self.bulletinManager.dismissBulletin(animated: true, completion: {
                    self.presentContactNotificationBulletin()
                })
            }
        }))

        self.bulletinManager.prepare()
        self.bulletinManager.presentBulletin(above: self.tabBarController ?? self)
    }

    func presentNotification() {
        if #available(iOS 10.0, *) {
            let current = UNUserNotificationCenter.current()
            current.getNotificationSettings(completionHandler: { (settings) in
                if settings.authorizationStatus == .notDetermined {
                    DispatchQueue.main.async {
                        self.bulletinNotificationBoardPopup()
                    }
                } else {
                    self.presentContactNotificationBulletin()
                }

            })
        } else {
            // Fallback on earlier versions
            if UIApplication.shared.isRegisteredForRemoteNotifications {
                print("APNS-YES")
                self.presentContactNotificationBulletin()
            } else {
                DispatchQueue.main.async {
                    self.bulletinNotificationBoardPopup()
                }
            }
        }
    }

    func presentContactNotificationBulletin() {
        let status = CNContactStore.authorizationStatus(for: .contacts)
        if status == .notDetermined {
            DispatchQueue.main.async {
                self.bulletinWelcomeSocialContactPopup()
            }
        } else {
            if #available(iOS 10.0, *) {
                let current = UNUserNotificationCenter.current()
                current.getNotificationSettings(completionHandler: { (settings) in
                    if settings.authorizationStatus == .notDetermined {
                        DispatchQueue.main.async {
                            //self.bulletinNotificationBoardPopup()
                        }
                    }
                })
            } else {
                // Fallback on earlier versions
                if UIApplication.shared.isRegisteredForRemoteNotifications {
                    print("APNS-YES")
                } else {
                    DispatchQueue.main.async {
                        //self.bulletinNotificationBoardPopup()
                    }
                }
            }
        }
    }



    // check current influencer score
    func checkCurrentInfluencerScore() {
        if let currentInfuencerScoreDefault = UserDefaults.standard.object(forKey: userDefaultKey.currentInfluencerScoreDefault.rawValue) {
            newInfluencerScore = currentInfuencerScoreDefault as! Int
        }

        if influencerScore >= newInfluencerScore {
            return
        }
        if infuencerTimer == nil {
            infuencerTimer = Timer.scheduledTimer(timeInterval: 0.08, target: self, selector: #selector(THSocialViewController.timercounterOfInfluencerScore), userInfo: nil, repeats: true)
            leadershipCountLabel.backgroundColor = UIColor.red
            pulsator.numPulse = 3
            pulsator.radius = 50.0
            pulsator.backgroundColor = UIColor.white.cgColor
            leadershipCountLabel.layer.addSublayer(pulsator)
            pulsator.start()
        }
    }



    //timer for influecer score animation when trigger notification
    func timercounterOfInfluencerScore() {
        influencerScore = influencerScore + 1
        DispatchQueue.main.async {
            self.leadershipCountLabel.text = " +\(self.influencerScore)  "
            self.leadershipCountLabel.layer.cornerRadius = self.leadershipCountLabel.frame.width / 2.0
            self.leadershipCountLabel.layer.masksToBounds = true
        }
        if (influencerScore == newInfluencerScore - 5) {
            infuencerTimer.invalidate()
            infuencerTimer = nil
            infuencerTimer = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(THSocialViewController.timercounterOfInfluencerScore), userInfo: nil, repeats: true)
        }

        if (influencerScore == newInfluencerScore || newInfluencerScore < influencerScore) {
            infuencerTimer.invalidate()
            infuencerTimer = nil
            pulsator.stop()
        }
        UserDefaults.standard.set(influencerScore, forKey: userDefaultKey.influencerScoreDefault.rawValue)
        UserDefaults.standard.synchronize()
    }


    func saparatePostsBasedOntags() {
        postsSocialInteraction = THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts
        self.objTableView.reloadData()
    }



    func notificationPostLikeComment(notification: NSNotification) {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
        }
        self.didTapOnNotificationButton(self)
    }

    private func initUI(){
        cameraButton.layer.cornerRadius = cameraButton.frame.size.width/2
        cameraButton.layer.borderWidth = 0.5
        cameraButton.layer.borderColor = UIColor.black.cgColor
        cameraButton.layer.masksToBounds = true
        notificationCountLabel.layer.cornerRadius = notificationCountLabel.frame.width / 2.0
        notificationCountLabel.layer.masksToBounds = true
        leadershipCountLabel.layer.cornerRadius = leadershipCountLabel.frame.width / 2.0
        leadershipCountLabel.layer.masksToBounds = true
        addFriendCountLabel.layer.cornerRadius = addFriendCountLabel.frame.width / 2.0
        addFriendCountLabel.layer.masksToBounds = true

        cartCountLabel.layer.cornerRadius = cartCountLabel.frame.width / 2.0
        cartCountLabel.layer.masksToBounds = true
    }

    fileprivate func updateNotificationCount() {
        notificationCountLabel.isHidden = UIApplication.shared.applicationIconBadgeNumber == 0 ? true:false
        notificationCountLabel.text = ""
    }

    func getUserLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }


    func refresh(sender:AnyObject) {
        THSocialInteractionPostsManager.sharedInstance.page = 1
        THSocialInteractionPostsManager.sharedInstance.didLoadData = true
        THSocialInteractionPostsManager.sharedInstance.pullToRefresh = true
        THSocialInteractionPostsManager.sharedInstance.retrieveSocialInteractionPosts { (error) in
            self.refreshControl.endRefreshing()
            guard let error = error else {
                self.saparatePostsBasedOntags()
                return
            }
            debugPrint(error)
        }
    }

    public func loadMore() {
            self.pageUser += 1

            if self.countUser != THSocialInteractionPostsManager.sharedInstance.followPeopleList.count{
                self.refresh()
                countUser = THSocialInteractionPostsManager.sharedInstance.followPeopleList.count
            }
    }

    func refresh() {

        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THSocialInteractionPostsManager.sharedInstance.page = pageUser
        THSocialInteractionPostsManager.sharedInstance.didLoadData = false
        THSocialInteractionPostsManager.sharedInstance.pullToRefresh = false
        THSocialInteractionPostsManager.sharedInstance.retrieveSocialInteractionPosts { (error) in
            guard let error = error else {

                    self.selectedCollectionView?.reloadData()
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)

                return
            }
            debugPrint(error)
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
        }

    }


    @IBAction func didTapOnToolTip(_ sender: Any) {
        popOverProductTagToolTip.dismiss()
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THBrandProductManager.sharedInstance.retrieveBrandProfileFromDots(with: (self.selectedPhotoTag?.brandId)!, productId: (self.selectedPhotoTag?.productId)!, sharePostCode: (selectedSocialInteraction?.ownerSharePostCode)!, postId: (selectedSocialInteraction?.id)!, completion: { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            let threeDAvailable = THBrandProductManager.sharedInstance.brandProduct?.productGalleryImages.contains(where: { (imgGallery) -> Bool in
                if imgGallery.image_type == "3D" {
                    return true
                } else {
                    return false
                }
            })

            if threeDAvailable! {
                if #available(iOS 11.0, *) {
                    if (ARConfiguration.isSupported) {
                        let ARShopShowController = THARShopShowViewController(nibName: THARShopShowViewController.className, bundle: nil)
                        ARShopShowController.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                        ARShopShowController.postId = (self.selectedSocialInteraction?.id)!
                        self.navigationController?.pushViewController(ARShopShowController, animated: true)
                        return
                    }
                }
            }
            let shopshowcontroller = THShopshowViewController(nibName: THShopshowViewController.className, bundle: nil)
            shopshowcontroller.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
            shopshowcontroller.postId = (self.selectedSocialInteraction?.id)!
            self.navigationController?.pushViewController(shopshowcontroller, animated: true)

            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Tap on Product Tag", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "product_name":self.selectedPhotoTag?.productName as Any])
            }

        })
    }

    @IBAction func didTapOnBrandToolTip(_ sender: Any) {
        popOverBrandTagToolTip.dismiss()
        let brandProfileVC:THBrandProfileViewController = THBrandProfileViewController()
        brandProfileVC.isFrom = "brandlogo"
        brandProfileVC.brandId = self.selectedPhotoTag?.brandId
        self.navigationController?.pushViewController(brandProfileVC, animated: true)
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Tap on Brand Tag", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "brand_name":self.selectedPhotoTag?.brandName as Any])
        }

    }

    @IBAction func didTapOnShirt(_ sender: Any) {

        if !isShirtOn {
            self.shirtButton.setImage(#imageLiteral(resourceName: "ic_shirt_selected"), for: .normal)
        }else{
            self.shirtButton.setImage(#imageLiteral(resourceName: "ic_shirt"), for: .normal)
        }
        isShirtOn = !isShirtOn
        selectedPostIndex = 0
        objTableView.reloadData()

        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Shirt button", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "From": "AR Closet"])
        }
    }
    @IBAction func didTapOnToggleDropdown(_ sender: Any) {
        if !dropdownShowing {
            if let _ = dropdownViewController {
                return
            }

            var yPos : CGFloat = 0
            if UIDevice().userInterfaceIdiom == .phone {
                switch UIScreen.main.nativeBounds.height {
                case 2436:
                    yPos = 88
                default:
                    yPos = 64
                }
            }

            dropdownViewController = THSocialDropdownMenu(nibName: THSocialDropdownMenu.className, bundle: nil)
            self.addChildViewController(dropdownViewController)
            dropdownViewController.view.frame = CGRect(x: 0, y: headerTop.constant + yPos, width: screenwidth, height: screenheight-yPos)
            self.view.addSubview(dropdownViewController.view)
            dropdownViewController.showAnimate()
        } else {
            if let dropdown = dropdownViewController {
                dropdown.removeAnimate()
            }
        }
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "DropDown Menu", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "From": "Social"])
        }
    }

    @IBAction func didTapOnRetryCreatePost(_ sender: Any) {
        if let _ = productObject {
            retryCreatePostButton.isHidden = true
            createPost()
        }
    }

    @IBAction func didTapOnShowCart(_ sender: Any) {
        if let dropdown = dropdownViewController {
            dropdown.removeAnimate()
        }

        let shoppingcartController = THYourCartViewController(nibName: THYourCartViewController.className, bundle: nil)
        self.navigationController?.pushViewController(shoppingcartController, animated: true)
    }

    @IBAction func didTapOnAddFriend(_ sender: Any) {
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }
        let inviteNewVC = THFindFriendInviteViewController(nibName: THFindFriendInviteViewController.className, bundle: nil)
        let navInvite = UINavigationController(rootViewController: inviteNewVC)
        navInvite.setNavigationBarHidden(true, animated: false)
        self.present(navInvite, animated: true, completion: nil)

        /*let findFriendConnectController = THFindFriendsConnectViewController(nibName: THFindFriendsConnectViewController.className, bundle: nil)
        let navFindFriendConnectController = UINavigationController(rootViewController: findFriendConnectController)
        self.present(navFindFriendConnectController, animated: true, completion: nil)*/
    }

    @IBAction func didTapOnNotificationButton(_ sender: Any) {
        let noficationController = THNotificationController(nibName: THNotificationController.className, bundle: nil)
        let navNotificationController = UINavigationController(rootViewController: noficationController)
        self.present(navNotificationController, animated: true, completion: nil)
    }

    @IBAction func didTapOnLeadershipButton(_ sender: Any) {
        self.leadershipCountLabel.backgroundColor = UIColor.blueTabbar
        let BrandVC = THBlackBrandViewController(nibName: THBlackBrandViewController.className, bundle: nil)
        BrandVC.isMyBrand = "my"
        BrandVC.blackBrandCompletionHander = {
            self.didTapOnCameraButton(self)
        }
        let navBrandVC = UINavigationController(rootViewController: BrandVC)
        self.present(navBrandVC, animated: true, completion: nil)
    }

    @IBAction func didTapOnCameraButton(_ sender: Any) {
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }
        let swiftyCameraVC = THSwiftyCameraViewController(nibName: THSwiftyCameraViewController.className, bundle: nil)
        swiftyCameraVC.view.frame.size.width = screenwidth
        swiftyCameraVC.view.frame.size.height = screenheight
        swiftyCameraVC.completionHander = {
            object, isfrom in
            self.productObject = object
            self.isFromCamera = true
            if(self.isFromCamera == true) {
                self.isFromCamera = false
                self.postingView.isHidden = false
                self.postingView.alpha = 1.0
                self.retryCreatePostButton.isHidden = true
                self.postedImage.image = self.productObject.product_image
                self.objTableView.setContentOffset(.zero, animated: true)
                self.createPost()
            }
        }
        self.present(swiftyCameraVC, animated: true, completion: nil)
    }

    @IBAction func didTapOnCameraVisionButton(_ sender: Any) {
        let swiftyCameraVC = THSwiftyCameraViewController(nibName: THSwiftyCameraViewController.className, bundle: nil)
        swiftyCameraVC.view.frame.size.width = screenwidth
        swiftyCameraVC.view.frame.size.height = screenheight
        swiftyCameraVC.isVisionCamera = true
        swiftyCameraVC.imageCompletionHandler =  {
            image in
            self.bulletinManager = BulletinManager(rootItem: GeneralMethods.sharedInstance.bulletinNotificationVisionBrandSelection(YesCompletion: {
                DispatchQueue.main.async {
                    self.bulletinManager.dismissBulletin(animated: true, completion: {
                        let visionBrandVC = THVisionSearchBrandViewController(nibName: THVisionSearchBrandViewController.className, bundle: nil)
                        visionBrandVC.visionImage = image
                        self.navigationController?.pushViewController(visionBrandVC, animated: true)
                    })
                }
            }, NoCompletion: {
                DispatchQueue.main.async {
                    self.bulletinManager.dismissBulletin(animated: true, completion: {
                            let visionVC = THVisionProductViewViewController(nibName: THVisionProductViewViewController.className, bundle: nil)
                            visionVC.visionImage = image
                            self.navigationController?.pushViewController(visionVC, animated: true)
                        })
                }
            }))
            self.bulletinManager.prepare()
            self.bulletinManager.presentBulletin(above: self.tabBarController ?? self)
        }
        self.present(swiftyCameraVC, animated: true, completion: nil)
    }


    //MARK: Follow and Skip button Action

    @IBAction func didTapOnClosetButton(_ sender: Any) {
//        appdelegate.verticle.scrollUP(isAnimated: true)
    }

    func didTapFollowButton(_ sender: UIButton) {
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }
        let id = THSocialInteractionPostsManager.sharedInstance.followPeopleList[sender.tag].id
        if let suggestionCell = objTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? THListOfSuggestionUsersCell {

            suggestionCell.collectionView.performBatchUpdates({
                suggestionCell.collectionView.deleteItems(at: [IndexPath(row: sender.tag, section: 0)])
                THSocialInteractionPostsManager.sharedInstance.followPeopleList.remove(at: sender.tag)
            }, completion: { (true) in
                suggestionCell.collectionView.reloadData()
            })
            if THSocialInteractionPostsManager.sharedInstance.followPeopleList.count == 0 {
                self.objTableView.reloadSections(IndexSet(integer: 0), with: .none)
            }
        }
        THSocialInteractionPostsManager.sharedInstance.performFollowUser(for: id) { (error) in
        }
    }

    func didTapSkipButton(_ sender: UIButton) {
        if let suggestionCell = objTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? THListOfSuggestionUsersCell {

            suggestionCell.collectionView.performBatchUpdates({
                suggestionCell.collectionView.deleteItems(at: [IndexPath(row: sender.tag, section: 0)])
                THSocialInteractionPostsManager.sharedInstance.followPeopleList.remove(at: sender.tag)
            }, completion: { (true) in
                suggestionCell.collectionView.reloadData()
            })
            if THSocialInteractionPostsManager.sharedInstance.followPeopleList.count == 0 {
                self.objTableView.reloadSections(IndexSet(integer: 0), with: .none)
            }
        }
    }

    @IBAction func didTapOnCommentButton(_ sender: UIButton) {
        let commentController = THCommentViewController(nibName: THCommentViewController.className, bundle: nil)
        commentController.socialInteraction = postsSocialInteraction[sender.tag]
        self.navigationController?.pushViewController(commentController, animated: true)
    }

    func didTapOnHashTagButton(_ sender: UITapGestureRecognizer) {
        let socialInteraction = postsSocialInteraction[(sender.view?.tag)!]
        var hashTags = ""
        for tags in socialInteraction.photoTags {
            if tags.tagType == "Hash Tag" {
                hashTags = hashTags + tags.hashTag
            }
        }
        let hashTagController = THHashTagViewController(nibName: THHashTagViewController.className, bundle: nil)
        hashTagController.hashTagString = hashTags
        self.navigationController?.pushViewController(hashTagController, animated: true)
    }

    @IBAction func didTapOnPhotoTagButton(_ sender: UIButton) {
        let socialInteraction = postsSocialInteraction[sender.tag]
        let photoTag = socialInteraction.photoTags[Int("\(sender.accessibilityIdentifier!)")!]
        if photoTag.tagType == "Brand Tag"{
            let brandProfileVC:THBrandProfileViewController = THBrandProfileViewController()
            brandProfileVC.isFrom = "brandlogo"
            brandProfileVC.brandId = photoTag.brandId
            self.navigationController?.pushViewController(brandProfileVC, animated: true)
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Tap on Brand Tag", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "brand_name":photoTag.brandName])
            }
        } else if photoTag.tagType == "Product Tag" {

                GeneralMethods.sharedInstance.loadLoading(view: self.view)
                THBrandProductManager.sharedInstance.retrieveBrandProfileFromDots(with: photoTag.brandId, productId: photoTag.productId, sharePostCode: socialInteraction.ownerSharePostCode, postId: socialInteraction.id, completion: { (error) in
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    let threeDAvailable = THBrandProductManager.sharedInstance.brandProduct?.productGalleryImages.contains(where: { (imgGallery) -> Bool in
                        if imgGallery.image_type == "3D" {
                            return true
                        } else {
                            return false
                        }
                    })

                    if threeDAvailable! {
                        if #available(iOS 11.0, *) {
                            if (ARConfiguration.isSupported) {
                                let ARShopShowController = THARShopShowViewController(nibName: THARShopShowViewController.className, bundle: nil)
                                ARShopShowController.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                                ARShopShowController.postId = (self.selectedSocialInteraction?.id)!
                                self.navigationController?.pushViewController(ARShopShowController, animated: true)
                                return
                            }
                        }
                    }
                    let shopshowcontroller = THShopshowViewController(nibName: THShopshowViewController.className, bundle: nil)
                    shopshowcontroller.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                    shopshowcontroller.postId = socialInteraction.id
                    self.navigationController?.pushViewController(shopshowcontroller, animated: true)

                    if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                        Answers.logCustomEvent(withName: "Tap on Product Tag", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "product_name":photoTag.productName])
                    }
                })

        } else if photoTag.tagType == "Hash Tag" {
            let hashTagController = THHashTagViewController(nibName: THHashTagViewController.className, bundle: nil)
            hashTagController.hashTagString = photoTag.hashTag
            self.navigationController?.pushViewController(hashTagController, animated: true)
        }
    }

    @IBAction func didTapOnToolTipPhotoTagButton(_ sender: UIButton) {
        let socialInteraction = postsSocialInteraction[sender.tag]
        let photoTag = socialInteraction.photoTags[Int("\(sender.accessibilityIdentifier!)")!]
        if photoTag.tagType == "Brand Tag"{

            let options = [
                .type(.up),
                .arrowSize(CGSize.zero),
                .animationIn(0.3),
                .blackOverlayColor(UIColor.clear),
                .color(UIColor.blueTabbar)
                ] as [PopoverOption]
            self.brandToolTipView.sizeToFit()

            DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {

                    self.selectedPhotoTag = photoTag
                    self.selectedSocialInteraction = socialInteraction

                    self.brandToolTipLabel.text = photoTag.brandName
                    self.brandToolTipLogo.sd_setImage(with: URL(string: photoTag.brandLogo),placeholderImage: nil)
                    self.brandToolTipLogo.layer.cornerRadius = 19
                    self.brandToolTipLogo.layer.masksToBounds = true

                    self.brandToolTipView.frame = CGRect(x: 0, y: 0, width: Int(self.brandToolTipView.frame.width), height: self.lines(label: self.brandToolTipLabel) * 21 + 25)


                    self.popOverBrandTagToolTip = Popover(options: options, showHandler: nil, dismissHandler: nil)
                    let point : CGPoint = CGPoint(x:sender.frame.origin.x,y:sender.frame.origin.y + 40)
                    self.popOverBrandTagToolTip.show(self.brandToolTipView, point: point, inView: sender.superview!)

            }

        } else if photoTag.tagType == "Product Tag" {

                let options = [
                    .type(.up),
                    .animationIn(0.3),
                    .arrowSize(CGSize.zero),
                    .blackOverlayColor(UIColor.clear),
                    .color(UIColor.blueTabbar)
                    ] as [PopoverOption]
                self.productToolTipView.sizeToFit()

                DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {

                        self.selectedPhotoTag = photoTag
                        self.selectedSocialInteraction = socialInteraction

                        self.productToolTipLabel.text = photoTag.productName
                        self.productToolTipLogo.sd_setImage(with: URL(string: photoTag.productImage),placeholderImage: nil)
                        self.productToolTipLogo.layer.cornerRadius = 19
                        self.productToolTipLogo.layer.masksToBounds = true

                        self.productToolTipView.frame = CGRect(x: 0, y: 0, width: Int(self.productToolTipView.frame.width), height: self.lines(label: self.productToolTipLabel) * 21 + 25)
                        if photoTag.productSalePrice != ""{
                            self.salePriceToolTipLabel.text = "$" + photoTag.productSalePrice

                            if photoTag.productDefaultPrice != "" {

                                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(format: "$%@", photoTag.productDefaultPrice))
                                attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
                                self.priceToolTipLabel.attributedText = attributeString
                            }else{
                                self.priceToolTipLabel.text = ""
                            }

                        }else{
                            self.salePriceToolTipLabel.text = ""

                            if photoTag.productDefaultPrice != "" {
                                self.priceToolTipLabel.text = "$" + photoTag.productDefaultPrice
                            }else{
                                self.priceToolTipLabel.text = ""
                            }


                        }

                        self.popOverProductTagToolTip = Popover(options: options, showHandler: nil, dismissHandler: nil)
                        //self.popOverProductTagToolTip.show(self.productToolTipView, fromView: sender)
                        let point : CGPoint = CGPoint(x:sender.frame.origin.x,y:sender.frame.origin.y + 45)
                        self.popOverProductTagToolTip.show(self.productToolTipView, point: point, inView: sender.superview!)

                }




        } else if photoTag.tagType == "Hash Tag" {
            let hashTagController = THHashTagViewController(nibName: THHashTagViewController.className, bundle: nil)
            hashTagController.hashTagString = photoTag.hashTag
            self.navigationController?.pushViewController(hashTagController, animated: true)
        }
    }


    func lines(label: UILabel) -> Int {
        let textSize = CGSize(width: label.frame.size.width, height: CGFloat(Float.infinity))
        let rHeight = lroundf(Float(label.sizeThatFits(textSize).height))
        let charSize = lroundf(Float(label.font.lineHeight))
        let lineCount = rHeight/charSize
        return lineCount
    }

    @IBAction func didTapOnRethreadButton(_ sender: UIButton) {
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }
        let cell = objTableView.cellForRow(at: IndexPath(row: 1, section:sender.tag+1)) as! THSocialLikeCommentTableViewCell
        let socialInteraction:SocialInteraction = postsSocialInteraction[sender.tag]
        if !cell.shareButton.isSelected {
            let rethreadpopup = THRethreadPopupViewController(nibName: THRethreadPopupViewController.className, bundle: nil)
            rethreadpopup.productLinkString = "https://www.threadest.com/posts/\(socialInteraction.id)"
            self.addChildViewController(rethreadpopup)
            rethreadpopup.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
            self.view.addSubview(rethreadpopup.view)
            rethreadpopup.didMove(toParentViewController: self)
            rethreadpopup.completionHander = {selectedRethreadArray in
                for selectedRethread in selectedRethreadArray! {
                    switch selectedRethread as! String {
                    case "Threadest":
                        self.rethreadPost(selectedRow: sender.tag)
                        break
                    case "Facebook":
                        self.rethreadPostOnFacebook(selectedRow: sender.tag)
                        break
                    case "Twitter":
                        self.rethreadPostOnTwitter(socialInteration:self.postsSocialInteraction[sender.tag], comment: "")
                        break
                    default:
                        break
                    }
                }
            }
            /*cell.shareButton.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                cell.shareButton.transform = CGAffineTransform(rotationAngle: CGFloat(-179.9).degreesToRadians)
            })
            UIView.animate(withDuration: 0.3, delay: 0.2, options: UIViewAnimationOptions(), animations: {
                cell.shareButton.transform = CGAffineTransform(rotationAngle: CGFloat(1.1).degreesToRadians)
            }, completion: { (completed) in

            })*/
        }
    }

    @IBAction func didTapOnLikeButton(_ sender: UIButton) {

        if !sender.isSelected {
            let cell = objTableView.cellForRow(at: IndexPath(row: 1 , section: sender.tag + 1)) as! THSocialLikeCommentTableViewCell
            sender.isSelected  = true
            if let socialPostCell = objTableView.cellForRow(at: IndexPath(row: 0 , section: sender.tag + 1)) as? THShareDetailTableViewCell {
                let animationView = LOTAnimationView(name: "like")
                animationView.animationProgress = 100
                animationView.center = socialPostCell.socialPostImage.center
                animationView.frame = socialPostCell.socialPostImage.frame
                animationView.backgroundColor = UIColor.clear
                animationView.contentMode = .scaleAspectFill
                animationView.animationSpeed = 2
                socialPostCell.socialPostImage.addSubview(animationView)
                animationView.loopAnimation = false
                animationView.play { (sucess) in
                    animationView.pause()
                    animationView.removeFromSuperview()
                }
            }

            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                THUserManager.sharedInstance.performUpvote(for: usercurrent.userId, votableId: self.postsSocialInteraction[sender.tag].id, votableType: "Post", completion: { (vote, error) in
                    guard let _ = error else {
                        if let i = THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts.index(where: { $0.id == self.postsSocialInteraction[sender.tag].id}){
                            THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[i].likedByCurrentUser = true
                            THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[i].upvotesCount = THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[i].upvotesCount + 1
                        }
                        self.postsSocialInteraction[sender.tag].likedByCurrentUser = true
                        self.postsSocialInteraction[sender.tag].upvotesCount = self.postsSocialInteraction[sender.tag].upvotesCount + 1
                        self.postsSocialInteraction[sender.tag].votes.append(vote!)

                        cell.likeLabel.text = self.postsSocialInteraction[sender.tag].upvotesCount == 0 ? "": self.postsSocialInteraction[sender.tag].upvotesCount == 1 ? "\(self.postsSocialInteraction[sender.tag].upvotesCount) Like" : "\(self.postsSocialInteraction[sender.tag].upvotesCount) Likes"
                        cell.likeLabel.isHidden = false
                        //Add funnel for Like post
                        Answers.logCustomEvent(withName: "Like post", customAttributes: ["userid":usercurrent.userId,"username": usercurrent.username, "post_Id":"\(self.postsSocialInteraction[sender.tag].id)"])
                        return
                    }
                })
            }
        }
    }


    @IBAction func didTapOnPostImage(_ sender: UIButton) {
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }
    }

    @IBAction func didTapOnLikescountButton(_ sender: UIButton) {
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        let socialInteraction = postsSocialInteraction[sender.tag]
        THSocialInteractionPostsManager.sharedInstance.retrieveLikedPostUserList(for: socialInteraction.id) { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            guard let _ = error else {
                let followingController = THUserFollowerViewController(nibName: THUserFollowerViewController.className, bundle: nil)
                followingController.profileType = "Likes"
                self.navigationController?.pushViewController(followingController, animated: true)
                return
            }

        }
    }

    @IBAction func didTapOnProfileUserNameButton(_ sender: UIButton) {
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        let socialInteraction = postsSocialInteraction[sender.tag]
        THProfilePostsManager.sharedInstance.profilePosts = [SocialInteraction]()
        THProfilePostsManager.sharedInstance.page = 1
        THProfilePostsManager.sharedInstance.didLoadData = false
        THProfilePostsManager.sharedInstance.retrieveProfilePosts(with: Int(socialInteraction.userId)) { (error) in
            guard error != nil else {
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let userPostProfileVC = storyBoard.instantiateViewController(withIdentifier: PZProfileTableViewController.className) as! PZProfileTableViewController
                userPostProfileVC.profile = THProfilePostsManager.sharedInstance.profile
                userPostProfileVC.isSearch = true
                userPostProfileVC.userId = socialInteraction.userId
                let profileNavVC = UINavigationController(rootViewController: userPostProfileVC)
//                self.navigationController?.pushViewController(userPostProfileVC, animated: true)
                self.present(profileNavVC, animated: true, completion: nil)

                return
            }
         }
    }

    @IBAction func didTapOnDeletePostButton(_ sender: UIButton) {
        self.present(GeneralMethods.sharedInstance.AlertviewGeneral(title: "Threadest", body: "Are you sure you want to remove post?", cancelbutton: "NO", okbutton: "Yes", completion: {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            let socialInteraction = self.postsSocialInteraction[sender.tag] as SocialInteraction
            THProfilePostsManager.sharedInstance.deletePost(with: Int(socialInteraction.id)) { (error) in
                guard error != nil else {
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    self.postsSocialInteraction.remove(at: sender.tag)
                    self.objTableView.reloadData()
                    return
                }
            }
        }), animated: true, completion: nil)
    }

    @IBAction func didTapOnViewAllButton(_ sender: UIButton) {
        let inviteNewVC = THFindFriendInviteViewController(nibName: THFindFriendInviteViewController.className, bundle: nil)
        let navInvite = UINavigationController(rootViewController: inviteNewVC)
        navInvite.setNavigationBarHidden(true, animated: false)
        self.present(navInvite, animated: true, completion: nil)
        /*let userPickerFollowViewController = THUserPickerFollowViewController(nibName: THUserPickerFollowViewController.className, bundle: nil)
         self.navigationController?.pushViewController(userPickerFollowViewController, animated: true)*/
    }


    @IBAction func didTapOnThreadsPostsSwitch(_ sender: UISwitch) {
        if THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts.count == 0 {
            return
        }
        self.saparatePostsBasedOntags()
        if self.postsSocialInteraction.count == 0 {
            return
        }
        let visibleCells: Array = objTableView.indexPathsForVisibleRows!
        let currentIndexPath: IndexPath = visibleCells[0]
        if threadsPostsSwitch.isOn {
            postsIndexPath = currentIndexPath
            self.objTableView.scrollToRow(at: threadsPostsIndexPath, at: .none, animated: false)
        } else {
            threadsPostsIndexPath = currentIndexPath
            self.objTableView.scrollToRow(at: postsIndexPath, at: .none, animated: false)
        }
    }


    @IBAction func didTapOnToolTipClose(_ sender: Any) {
        popOverTagToolTip.dismiss()
        if userdefault.object(forKey: userDefaultKey.toolTipSocialTDefault.rawValue) == nil {
            userdefault.set("true", forKey: userDefaultKey.toolTipSocialTDefault.rawValue)
            userdefault.synchronize()
        } else if userdefault.object(forKey: userDefaultKey.toolTipSocialRethreadDefault.rawValue) == nil {
            userdefault.set("true", forKey: userDefaultKey.toolTipSocialRethreadDefault.rawValue)
            userdefault.synchronize()
        } else if userdefault.object(forKey: userDefaultKey.toolTipSocialScoreDefault.rawValue) == nil {
            userdefault.set("true", forKey: userDefaultKey.toolTipSocialScoreDefault.rawValue)
            userdefault.synchronize()
        }
    }

    @IBAction func didTapOnToolTipNextButton(_ sender: Any) {
        if userdefault.object(forKey: userDefaultKey.toolTipSocialTDefault.rawValue) == nil {
        if postsSocialInteraction.count > 0 {
          /*  DispatchQueue.main.async {
             self.popOverTagToolTip.dismiss()
            }*/
            userdefault.set("true", forKey: userDefaultKey.toolTipSocialTDefault.rawValue)
            userdefault.synchronize()
            self.showToolTip()
            }
        } else if userdefault.object(forKey: userDefaultKey.toolTipSocialRethreadDefault.rawValue) == nil {
            if postsSocialInteraction.count > 0 {
                /*DispatchQueue.main.async {
                    self.popOverTagToolTip.dismiss()
                }*/
                userdefault.set("true", forKey: userDefaultKey.toolTipSocialRethreadDefault.rawValue)
                userdefault.synchronize()
                self.showToolTip()
            }
        } else if userdefault.object(forKey: userDefaultKey.toolTipSocialScoreDefault.rawValue) == nil {
            self.popOverTagToolTip.dismiss()
            userdefault.set("true", forKey: userDefaultKey.toolTipSocialScoreDefault.rawValue)
            userdefault.synchronize()
        }
    }

    func popupShops() {
        if let dropdown = dropdownViewController {
            dropdown.removeFromParentViewController()
            dropdown.view.removeFromSuperview()
            dropdownViewController = nil
            dropdownShowing = false
            self.tabBarController?.selectedIndex = 1
        }
    }

    func sharePostPopup() {
        let sharePostpopup = THSocialPostPopup(nibName: THSocialPostPopup.className, bundle: nil)
        self.addChildViewController(sharePostpopup)
        sharePostpopup.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
        sharePostpopup.sharePostPopupCompletionHander = {
            self.didTapOnCameraButton(self)
        }
        self.view.addSubview(sharePostpopup.view)
    }


    func showToolTip() {
        if #available(iOS 10.0, *) {
            let current = UNUserNotificationCenter.current()
            current.getNotificationSettings(completionHandler: { (settings) in
                if settings.authorizationStatus == .notDetermined {
                    return
                }
            })
        } else {
            // Fallback on earlier versions
            if UIApplication.shared.isRegisteredForRemoteNotifications {
            } else {
                return
            }
        }

        let options = [
            .type(.up),
            .animationIn(0.3),
            .blackOverlayColor(UIColor.clear),
            .color(UIColor.blueTabbar)
            ] as [PopoverOption]
        self.toolTipView.sizeToFit()

        if userdefault.object(forKey: userDefaultKey.toolTipSocialTDefault.rawValue) == nil {
            //self.popOverTagToolTip.dismiss()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                self.toolTipLabel.text = "Tap blue dots to discover products & brands avaialble on Threadest. 😎"
                self.popOverTagToolTip = Popover(options: options, showHandler: nil, dismissHandler: nil)
                self.popOverTagToolTip.show(self.toolTipView, fromView: self.hiddenTabbarMiddleButton)
            }
        } else if userdefault.object(forKey: userDefaultKey.toolTipSocialRethreadDefault.rawValue) == nil {
            if postsSocialInteraction.count > 0 {

                if let socialCell = self.objTableView.cellForRow(at: IndexPath(row: 0, section: 1)) as? THShareDetailTableViewCell{
                    self.popOverTagToolTip.dismiss()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                        self.toolTipLabel.text = "Tap the rethread button to share style with friends & earn status by sharing photos"
                        self.popOverTagToolTip = Popover(options: options, showHandler: nil, dismissHandler: nil)
                        self.popOverTagToolTip.show(self.toolTipView, fromView: socialCell.rethreadButton)
                    }
                }
            }
        } else if userdefault.object(forKey: userDefaultKey.toolTipSocialScoreDefault.rawValue) == nil {
            self.popOverTagToolTip.dismiss()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                let options = [
                    .type(.down),
                    .animationIn(0.3),
                    .blackOverlayColor(UIColor.clear),
                    .color(UIColor.blueTabbar)
                    ] as [PopoverOption]
                self.toolTipLabel.text = "This is your influencer score. Tap your score to learn more! 😎"
                self.popOverTagToolTip = Popover(options: options, showHandler: nil, dismissHandler: nil)
                self.popOverTagToolTip.show(self.toolTipView, fromView: self.leadershipCountLabel)
            }

        }
    }


    func createPost() {
        THNetworkManager.sharedInstance.createProduct(product: productObject, completion: { (json, error) in
            guard let json = json else {
                self.postingView.alpha = 1.0
                self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "", body: (error?.localizedDescription)!, completion: {
                }), animated: true, completion: nil)
                if let errorDescription = error?.localizedDescription {
                    if errorDescription == "The Internet connection appears to be offline." {
                        self.retryCreatePostButton.isHidden = false
                    } else {
                        UIView.animate(withDuration: 0.5, animations: {
                            self.retryCreatePostButton.isHidden = true
                            self.postingView.alpha = 0
                            self.postingView.isHidden = true
                        }, completion: { (true) in})
                    }

                }
                return
            }

            let createProductResponse = CreateProductResponse.fromJSON(json: json)

            if createProductResponse.hasSucceeded {
                //Add funnel for sucessfull post
                if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    Answers.logCustomEvent(withName: "Successful post", customAttributes: ["userid":usercurrent.userId,"username": usercurrent.username, "post_Id":"\(createProductResponse.socialPost!.id)"])
                }
                DispatchQueue.main.async{
                self.progressImgage.setProgress(Float(1), animated: true)
                THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts.insert(createProductResponse.socialPost!, at: 0)
                self.postsSocialInteraction.insert(createProductResponse.socialPost!, at: 0)
                self.postingView.alpha = 1.0
               UIView.animate(withDuration: 0.5, animations: {
                self.postingView.alpha = 0
                self.postingView.isHidden = true
                self.retryCreatePostButton.isHidden = true
               }, completion: { (true) in
                self.objTableView.reloadData()
                self.objTableView.setNeedsLayout()
                self.objTableView.layoutIfNeeded()
                self.objTableView.updateConstraints()

               })
                }
                //for rethreading facebook
                if self.productObject.facebook {
                    if FBSDKAccessToken.current() != nil {
                        if FBSDKAccessToken.current().hasGranted("publish_actions") {
                            GeneralMethods.sharedInstance.publishMessage(message: "https://www.threadest.com/posts/\(createProductResponse.socialPost!.id)   \n \(self.productObject.comment)", link: createProductResponse.socialPost!.largeImageUrl, completion: { () in
                                GeneralMethods.sharedInstance.RethreadPostSucessPopup(message: "Post to Facebook - Successful", viewController: self)
                            })
                        } else {
                            self.requestPublishPermissions(socialInteration:createProductResponse.socialPost!)
                        }
                    } else {
                        self.requestPublishPermissions(socialInteration:createProductResponse.socialPost!)
                    }
                }
                //for rethreading twitter
                if self.productObject.twitter {
                    self.rethreadPostOnTwitter(socialInteration:createProductResponse.socialPost!, comment:self.productObject.comment)
                }

            } else {
                self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "", body: "Not created", completion: {
                }), animated: true, completion: nil)
            }
        }) { (progress) in
            DispatchQueue.main.async {
                self.progressImgage.setProgress(Float(progress), animated: true)
            }
            print("progress")
        }

    }

    // for Rethread post Threadest
    func rethreadPost(selectedRow: Int) {
        let socialInteraction:SocialInteraction = postsSocialInteraction[selectedRow]
        THSocialInteractionPostsManager.sharedInstance.performRethreadPost(for: socialInteraction.id) { (error) in
             guard let error = error else {
                let socialCell = self.objTableView.cellForRow(at: IndexPath(row: 0, section: selectedRow + 1)) as! THShareDetailTableViewCell

                if let i = THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts.index(where: { $0.id == self.postsSocialInteraction[selectedRow].id}){
                    THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[i].rethreadCount = THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[i].rethreadCount + 1
                }
                socialCell.rethreadCountLabel.text = "\(socialInteraction.rethreadCount + 1)"
                socialCell.rethreadCountLabel.isHidden = false
                self.postsSocialInteraction[selectedRow].rethreadCount = socialInteraction.rethreadCount + 1
                //Add funnel for Rethread Threadest Post
                if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    Answers.logCustomEvent(withName: "Rethread post", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "post_id":"\(socialInteraction.id)", "rethread_type":"Threadest"])
                }
                return
            }
            debugPrint(error)
        }
    }

    // for Rethread post on Facebook
    func rethreadPostOnFacebook(selectedRow: Int) {
        let socialInteraction:SocialInteraction = postsSocialInteraction[selectedRow]
        if FBSDKAccessToken.current() != nil {
        if FBSDKAccessToken.current().hasGranted("publish_actions") {
            GeneralMethods.sharedInstance.publishMessage(message: "https://www.threadest.com/posts/\(socialInteraction.id)", link: socialInteraction.largeImageUrl, completion: { () in
                //Add funnel for Rethread Threadest Post
                if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    Answers.logCustomEvent(withName: "Rethread post", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "post_id":"\(socialInteraction.id)", "rethread_type":"Facebook"])
                }
                GeneralMethods.sharedInstance.RethreadPostSucessPopup(message: "Post to Facebook - Successful", viewController: self)
            })
        } else {
            requestPublishPermissions(socialInteration:socialInteraction)
        }
        } else {
            requestPublishPermissions(socialInteration:socialInteraction)
        }
    }

    func requestPublishPermissions(socialInteration: SocialInteraction) {
        let login: FBSDKLoginManager = FBSDKLoginManager()

        login.logIn(withPublishPermissions: ["publish_actions"], from: self) { (result, error) in
            if (error != nil) {
                print(error!)
            } else if (result?.isCancelled)! {
                print("Canceled")
            } else if (result?.grantedPermissions.contains("publish_actions"))! {
                print("permissions granted")
                GeneralMethods.sharedInstance.publishMessage(message: "https://www.threadest.com/posts/\(socialInteration.id)", link: socialInteration.largeImageUrl, completion: { () in
                    //Add funnel for Rethread Threadest Post
                    if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                        Answers.logCustomEvent(withName: "Rethread post", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "post_id":"\(socialInteration.id)", "rethread_type":"Facebook"])
                    }
                    GeneralMethods.sharedInstance.RethreadPostSucessPopup(message: "Post to Facebook - Successful", viewController: self)
                })
            }
        }
    }


    func rethreadPostOnTwitter(socialInteration: SocialInteraction, comment: String) {
        if let twitterAccountId = TWTRTwitter.sharedInstance().sessionStore.session() {
            GeneralMethods.sharedInstance.uploadTwitterMedia(viewController: self, imageURL: socialInteration.largeImageUrl, status: "https://www.threadest.com/posts/\(socialInteration.id) \n \(comment)", token: twitterAccountId.authToken, userId: twitterAccountId.userID, id: socialInteration.id, type: "Post")
        } else {
            // Not logged in
            TWTRTwitter.sharedInstance().logIn { (session, error) in
                if let _ = session {
                    if let twitterAccountId = TWTRTwitter.sharedInstance().sessionStore.session() {
                        GeneralMethods.sharedInstance.uploadTwitterMedia(viewController: self, imageURL: socialInteration.largeImageUrl, status: "https://www.threadest.com/posts/\(socialInteration.id) \n \(comment)", token: twitterAccountId.authToken, userId: twitterAccountId.userID, id: socialInteration.id, type: "Post")
                    }
                }else{

                }

                if let error = error{
                    print(error)
                }

            }
        }
    }

    func updateUserCurrentLocation(latitude:Double,longitude:Double)
    {
        if let currentuser = THUserManager.sharedInstance.currentUserdata() {
            THProfileClosetManager.sharedInstance.updateUserLatLong(for: currentuser.userId, latitude: latitude, longitude: longitude, completion: { (error) in
                self.delegate.isLocationUpdated = true
            })
        }
    }

    func pinchZoomSocialPostImage(sender: UIPinchGestureRecognizer) {
        let tapLocation = sender.location(in: self.objTableView)
        if let indexPath : IndexPath = self.objTableView.indexPathForRow(at: tapLocation) {
            if let socialCell = self.objTableView.cellForRow(at: indexPath) as? THShareDetailTableViewCell {
                TMImageZoom.shared().gestureStateChanged(sender, withZoom: socialCell.socialPostImage)
            } else {
                TMImageZoom.shared().resetImageZoom()
            }

        } else {
            TMImageZoom.shared().resetImageZoom()
        }
    }

    func tagShow(sender:UIView){

            UIView.animate(withDuration: 0, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                sender.alpha = 1.0
            }, completion: {
                (finished: Bool) -> Void in
            })

    }

    func tagHide(sender:UIView){

            UIView.animate(withDuration: 0, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                sender.alpha = 0.0
            }, completion: {
                (finished: Bool) -> Void in
            })

    }

    func doubleTapOnSocialPostImage(sender: UITapGestureRecognizer) {
        let tapLocation = sender.location(in: self.objTableView)
        if let indexPath = self.objTableView.indexPathForRow(at: tapLocation) {
            if let socialCell = self.objTableView.cellForRow(at: indexPath) as? THShareDetailTableViewCell {
                self.didTapOnLikeButton(socialCell.likeButton)
            }

        }
    }
}


extension THSocialViewController: UITableViewDelegate{
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
         self.lastContentOffset = scrollView.contentOffset.y
//        appdelegate.verticle.scrollEnabled(isEnable: false)
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //self.lastContentOffset = scrollView.contentOffset.y
//        appdelegate.verticle.scrollEnabled(isEnable: true)

        if (scrollView.tag != 100 ) {
            return
        }

        if scrollDirection == .ScrollDirectionRight {

              selectedCollectionView = scrollView as? UICollectionView
              self.loadMore()

        }

    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {

//        var offsets : CGFloat = -44
//        if UIDevice().userInterfaceIdiom == .phone {
//            switch UIScreen.main.nativeBounds.height {
//            case 2436:
//                offsets = -56
//            default:
//                offsets = -44
//            }
//        }
//
//        if self.lastContentOffset < scrollView.contentOffset.y && scrollView.contentOffset.y > 0 {
//            if headerOffset > offsets {
//                headerTop.constant = self.headerOffset
//                self.headerOffset = self.headerOffset - 2.5
//            } else if headerOffset == offsets{
//                headerTop.constant = offsets
//            }
//        } else if self.lastContentOffset > scrollView.contentOffset.y {
//            if headerOffset == 0{
//                headerTop.constant = 0
//            }else if headerOffset < 0 {
//                headerTop.constant = self.headerOffset
//                self.headerOffset = self.headerOffset + 2.5
//            }
//        }

        if (scrollView.tag != 100) {
            return
        }

        if(scrollView.panGestureRecognizer.translation(in: scrollView.superview).x > 0)
        {
            scrollDirection = .ScrollDirectionLeft
        }
        else if (scrollView.panGestureRecognizer.translation(in: scrollView.superview).x < 0)
        {
            scrollDirection = .ScrollDirectionRight
        }else {
            scrollDirection = .ScrollDirectionNone
        }


    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            let suggestUserId = THSocialInteractionPostsManager.sharedInstance.followPeopleList[indexPath.row].id
            THProfilePostsManager.sharedInstance.profilePosts = [SocialInteraction]()
            THProfilePostsManager.sharedInstance.page = 1
            THProfilePostsManager.sharedInstance.didLoadData = false
            THProfilePostsManager.sharedInstance.retrieveProfilePosts(with: suggestUserId) { (error) in
                guard error != nil else {
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let userPostProfileVC = storyBoard.instantiateViewController(withIdentifier: PZProfileTableViewController.className) as! PZProfileTableViewController
                    userPostProfileVC.profile = THProfilePostsManager.sharedInstance.profile
                    userPostProfileVC.isSearch = true
                    userPostProfileVC.userId = suggestUserId

                    let profileNavVC = UINavigationController(rootViewController: userPostProfileVC)
//                    self.navigationController?.pushViewController(userPostProfileVC, animated: true)
                    self.present(profileNavVC, animated: true, completion: nil)
                    return
                }
            }
            break
        default:
            switch indexPath.row {
            case 0:
                //   let shareDetailCell = objTableView.cellForRow(at: indexPath) as! THShareDetailTableViewCell

                if !isShirtOn {
                    if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                        if usercurrent.tempAccount {
                            appdelegate.setupAccountPopup(viewController: self)
                        }else{
                            if selectedPostIndex != indexPath.section{
                                self.isShowTag = false
                                self.selectedPostIndex = indexPath.section
                                self.objTableView.reloadData()
                            }else{
                                self.objTableView.reloadData()
                            }
                        }
                    }
                }else{
                    if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                        if usercurrent.tempAccount {
                        appdelegate.setupAccountPopup(viewController: self)
                            return
                        }
                    }
                }
                //let shareDetailCell = objTableView.cellForRow(at: indexPath) as! THShareDetailTableViewCell
            //self.didTapOnLikeButton(shareDetailCell.likeButton)
            case 1:
                break
            default:
                let btnTag = UIButton()
                btnTag.tag = indexPath.section - 1
                btnTag.accessibilityIdentifier = String(format: "%d", indexPath.row-2)
                self.didTapOnPhotoTagButton(btnTag)
                break
            }
        }
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section != 0 {
            if indexPath.section-1 == postsSocialInteraction.count-3 {
                if THSocialInteractionPostsManager.sharedInstance.didLoadData {
                    THSocialInteractionPostsManager.sharedInstance.didLoadData = false
                    THSocialInteractionPostsManager.sharedInstance.retrieveSocialInteractionPosts { (error) in
                            guard let error = error else {
                            //self.objTableView.reloadData()
                            self.saparatePostsBasedOntags()
                            return
                            }
                            debugPrint(error)
                        }
                }
            }
        }
    }
}

extension THSocialViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int{
        return postsSocialInteraction.count+1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        switch section {
        case 0:
            return THSocialInteractionPostsManager.sharedInstance.followPeopleList.count > 0 ? 1:0
            //let totalFollowPeople = THSocialInteractionPostsManager.sharedInstance.followPeopleList.count
            //totalFollowPeople <= 2 ? totalFollowPeople:2
        default:
            return postsSocialInteraction[section-1].photoTags.count + 2
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        switch indexPath.section {
        case 0:
            let suggestionCell = tableView.dequeueReusableCell(withIdentifier: THListOfSuggestionUsersCell.className, for: indexPath) as! THListOfSuggestionUsersCell
            suggestionCell.selectionStyle = UITableViewCellSelectionStyle.none
            suggestionCell.collectionView.delegate = self
            suggestionCell.collectionView.dataSource = self
            suggestionCell.collectionView.reloadData()
            suggestionCell.selectionStyle = .none
            suggestionCell.collectionView.tag = 100
            suggestionCell.seeAllButton.addTarget(self, action: #selector(THSocialViewController.didTapOnViewAllButton(_:)), for: .touchUpInside)
            return suggestionCell
        default:
            switch indexPath.row {
            case 0:
                let shareDetailCell = tableView.dequeueReusableCell(withIdentifier: THShareDetailTableViewCell.className, for: indexPath) as! THShareDetailTableViewCell
                let socialInteraction = postsSocialInteraction[indexPath.section - 1] as SocialInteraction
                if let currentUser = THUserManager.sharedInstance.currentUserdata() {
                    if socialInteraction.userId == currentUser.userId {
                        shareDetailCell.trashButton.isHidden = false
                    } else {
                        shareDetailCell.trashButton.isHidden = true
                    }
                }

                shareDetailCell.selectionStyle = UITableViewCellSelectionStyle.none
               /* shareDetailCell.likeButton.tag = indexPath.section - 1
                shareDetailCell.commentButton.tag = indexPath.section - 1*/
                shareDetailCell.configure(withSocialInteraction: socialInteraction)
                shareDetailCell.likeButton.isHidden = true
                shareDetailCell.commentButton.isHidden = true
                shareDetailCell.rethreadButton.isHidden = true
                shareDetailCell.profile_nameButton.isHidden = true
                shareDetailCell.likeCountButton.isHidden = true
                shareDetailCell.commentLabel.isHidden = true
                shareDetailCell.commentCountLabel.isHidden = true
                shareDetailCell.profileImage.isHidden = true
                shareDetailCell.rethreadCountLabel.isHidden = true
                shareDetailCell.upvoteCountLabel.isHidden = true
               // shareDetailCell.profile_nameButton.tag = indexPath.section - 1
                /*shareDetailCell.commentButton.addTarget(self, action: #selector(THSocialViewController.didTapOnCommentButton(_:)), for: UIControlEvents.touchUpInside)
                shareDetailCell.rethreadButton.tag = indexPath.section - 1
                shareDetailCell.rethreadButton.addTarget(self, action: #selector(THSocialViewController.didTapOnRethreadButton(_:)), for: .touchUpInside)

                shareDetailCell.likeButton.tag = indexPath.section - 1
                shareDetailCell.likeButton.addTarget(self, action: #selector(THSocialViewController.didTapOnLikeButton(_:)), for: .touchUpInside)*/


                shareDetailCell.trashButton.tag = indexPath.section - 1
                shareDetailCell.trashButton.addTarget(self, action: #selector(THSocialViewController.didTapOnDeletePostButton(_:)), for: UIControlEvents.touchUpInside)

                //shareDetailCell.profile_nameButton.addTarget(self, action: #selector(THSocialViewController.didTapOnProfileUserNameButton(_:)), for: UIControlEvents.touchUpInside)

                //shareDetailCell.likeCountButton.tag = indexPath.section - 1
                //shareDetailCell.likeCountButton.addTarget(self, action: #selector(THSocialViewController.didTapOnLikescountButton(_:)), for: .touchUpInside)

                shareDetailCell.socialPostImage.subviews.forEach({ (button) in
                    button.removeFromSuperview()
                })

                let pinchZoomSocilPostImage = UIPinchGestureRecognizer.init(target: self, action: #selector(THSocialViewController.pinchZoomSocialPostImage(sender:)))
                shareDetailCell.socialPostImage.addGestureRecognizer(pinchZoomSocilPostImage)
                let doubleTap = UITapGestureRecognizer(target: self, action: #selector(THSocialViewController.doubleTapOnSocialPostImage(sender:)))
                doubleTap.numberOfTapsRequired = 2
                shareDetailCell.socialPostImage.addGestureRecognizer(doubleTap)
                shareDetailCell.circularProgressView.isHidden = false
                shareDetailCell.socialPostImage.sd_setImage(with: URL(string: socialInteraction.largeImageUrl), placeholderImage: nil, options: .retryFailed, progress: { (block1, block2, imageurl) in
                    let progress = Double(block1)/Double(block2)
                    DispatchQueue.main.async {
                        shareDetailCell.circularProgressView.setProgress(progress, animated: true)
                    }
                }) { (image, error, imageCacheType, imageUrl) in
                    if image != nil {
                        shareDetailCell.circularProgressView.isHidden = true
                        //for tag view
                        if socialInteraction.photoTags.count > 0 {
                            var i = 0
                            for tags in socialInteraction.photoTags.filter({$0.tagType == "Product Tag" || $0.tagType == "Brand Tag"}) {
                                let imgHeight = CGFloat(socialInteraction.largeImageHeight)
                                let imgWidth = CGFloat(socialInteraction.largeImageWidth)
                                let height = (imgHeight/imgWidth) * screenwidth
                                if (height-30) >= CGFloat(tags.y) {
                                    shareDetailCell.tagButton = UIButton(frame: CGRect(x: tags.x, y: tags.y, width: 30, height: 30))
                                } else {
                                    shareDetailCell.tagButton = UIButton(frame: CGRect(x: tags.x, y: tags.y-20, width: 30, height: 30))
                                }
                                shareDetailCell.tagButton.isUserInteractionEnabled = true
                                shareDetailCell.tagButton.backgroundColor = UIColor.white
                                shareDetailCell.tagButton.layer.borderColor = UIColor.white.cgColor
                                shareDetailCell.tagButton.layer.borderWidth = 1
                                shareDetailCell.tagButton.layer.cornerRadius = shareDetailCell.tagButton.frame.size.width / 2
                                shareDetailCell.tagButton.accessibilityIdentifier = String(format: "%d", i)
                                shareDetailCell.tagButton.layer.masksToBounds = true
                                shareDetailCell.tagButton.tag = indexPath.section - 1
                                if tags.tagType == "Product Tag" {
                                    shareDetailCell.tagButton.sd_setImage(with: URL(string: tags.productImage), for: .normal, placeholderImage: nil, options: .retryFailed, completed: nil)

                                } else {
                                    shareDetailCell.tagButton.sd_setImage(with: URL(string: tags.brandLogo), for: .normal, placeholderImage: nil, options: .retryFailed, completed: nil)
                                }
                                shareDetailCell.tagButton.imageView?.contentMode = .scaleAspectFit
                                shareDetailCell.tagButton.addTarget(self, action: #selector(THSocialViewController.didTapOnToolTipPhotoTagButton(_:)), for: .touchUpInside)
                                shareDetailCell.socialPostImage.addSubview(shareDetailCell.tagButton)
                                i = i + 1

                                if !self.isShirtOn {
                                    if indexPath.section == self.selectedPostIndex {

                                        if self.isShowTag {
                                            self.tagHide(sender: shareDetailCell.tagButton)
                                        }else{
                                            self.tagShow(sender: shareDetailCell.tagButton)

                                        }
                                        if i == socialInteraction.photoTags.filter({$0.tagType == "Product Tag" || $0.tagType == "Brand Tag"}).count {
                                            self.isShowTag = !self.isShowTag
                                        }

                                    }else{

                                        self.tagHide(sender: shareDetailCell.tagButton)
                                    }
                                }else{
                                    self.tagShow(sender: shareDetailCell.tagButton)
                                }
                            }
                        }
                    }
                }
                return shareDetailCell
            case 1:
                let socialLikeCommentCell = tableView.dequeueReusableCell(withIdentifier: THSocialLikeCommentTableViewCell.className, for: indexPath) as! THSocialLikeCommentTableViewCell
                socialLikeCommentCell.selectionStyle = UITableViewCellSelectionStyle.none
                 let socialInteraction = postsSocialInteraction[indexPath.section - 1] as SocialInteraction
                socialLikeCommentCell.configureSocial(socialInteraction: socialInteraction)

                socialLikeCommentCell.likeButton.tag = indexPath.section - 1
                socialLikeCommentCell.commentButton.tag = indexPath.section - 1

                socialLikeCommentCell.commentButton.addTarget(self, action: #selector(THSocialViewController.didTapOnCommentButton(_:)), for: UIControlEvents.touchUpInside)
                socialLikeCommentCell.shareButton.tag = indexPath.section - 1
                socialLikeCommentCell.shareButton.addTarget(self, action: #selector(THSocialViewController.didTapOnRethreadButton(_:)), for: .touchUpInside)

                socialLikeCommentCell.likeButton.tag = indexPath.section - 1
                socialLikeCommentCell.likeButton.addTarget(self, action: #selector(THSocialViewController.didTapOnLikeButton(_:)), for: .touchUpInside)

                socialLikeCommentCell.profile_nameButton.tag = indexPath.section - 1
                socialLikeCommentCell.profile_nameButton.addTarget(self, action: #selector(THSocialViewController.didTapOnProfileUserNameButton(_:)), for: UIControlEvents.touchUpInside)
                return socialLikeCommentCell
            default:
                let socialTagCell = tableView.dequeueReusableCell(withIdentifier: THSocialProductBrandTagTableViewCell.className, for: indexPath) as! THSocialProductBrandTagTableViewCell
                socialTagCell.configure(photosTags: postsSocialInteraction[indexPath.section - 1].photoTags[indexPath.row-2])
                return socialTagCell
            }
        }
    }

    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            break
        default:
            switch indexPath.row {
                case 0:
                    let shareDetailCell = tableView.dequeueReusableCell(withIdentifier: THShareDetailTableViewCell.className, for: indexPath) as! THShareDetailTableViewCell
                    shareDetailCell.trashButton.removeTarget(nil, action: nil, for: .touchUpInside)

                    shareDetailCell.likeCountButton.removeTarget(nil, action: nil, for: .touchUpInside)
                    shareDetailCell.tagButton.removeTarget(nil, action: nil, for: .touchUpInside)
                    if let pinchGesture = shareDetailCell.socialPostImage.gestureRecognizers?[0] as? UIPinchGestureRecognizer{
                        shareDetailCell.socialPostImage.removeGestureRecognizer(pinchGesture)
                    }

                    break
                case 1:
                    let likeCell = tableView.dequeueReusableCell(withIdentifier: THSocialLikeCommentTableViewCell.className, for: indexPath) as! THSocialLikeCommentTableViewCell
                    likeCell.commentButton.removeTarget(nil, action: nil, for: .touchUpInside)
                    likeCell.shareButton.removeTarget(nil, action: nil, for: .touchUpInside)
                    likeCell.likeButton.removeTarget(nil, action: nil, for: .touchUpInside)
                    likeCell.profile_nameButton.removeTarget(nil, action: nil, for: .touchUpInside)
                    break
                default:
                    break
                }
            }
        }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 202
        default:
            switch indexPath.row {
            case 0:
                let socialInteraction = postsSocialInteraction[indexPath.section - 1] as SocialInteraction
                let imgHeight = CGFloat(socialInteraction.largeImageHeight)
                let imgWidth = CGFloat(socialInteraction.largeImageWidth)
                return (imgHeight/imgWidth) * screenwidth
            case 1:
                return UITableViewAutomaticDimension
            default:
                return 50
            }
        }
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 202
        default:
            switch indexPath.row {
            case 0:
                let socialInteraction = postsSocialInteraction[indexPath.section - 1] as SocialInteraction
                let imgHeight = CGFloat(socialInteraction.largeImageHeight)
                let imgWidth = CGFloat(socialInteraction.largeImageWidth)
                return (imgHeight/imgWidth) * screenwidth
            case 1:
                return 100
            default:
                return 50
            }
        }
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 0//40
        default:
            return 0
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
        /*let viewheader = UIView(frame: CGRect(x: 0, y: 0, width: screenwidth, height: 40))
        viewheader.backgroundColor = UIColor(red: 244/255.0, green: 248/255.0, blue: 249/255.0, alpha: 1)
        let followpeoplelabel:UILabel = UILabel(frame: CGRect(x: 15, y: 0, width: screenwidth - 80, height: viewheader.frame.height))
        followpeoplelabel.font = UIFont(name: "SFUIText-Semibold", size: 14)
        followpeoplelabel.textColor = UIColor(red: 74/255.0, green: 74/255.0, blue: 74/255.0, alpha: 1)
        followpeoplelabel.text = "Follow people"
        viewheader.addSubview(followpeoplelabel)

        let viewallbutton:UIButton = UIButton(frame: CGRect(x: screenwidth - 80, y: 0, width: 60, height: viewheader.frame.size.height))
        viewallbutton.setTitle("View all", for: UIControlState.normal)
        viewallbutton.titleLabel?.font = UIFont(name: "SFUIText-Regular", size: 14)
        viewallbutton.setTitleColor(UIColor(red: 0, green: 138/255.0, blue: 208/255.0, alpha: 1), for: UIControlState.normal)
        viewallbutton.addTarget(self, action: #selector(THSocialViewController.didTapOnViewAllButton(_:)), for: .touchUpInside)
        viewheader.addSubview(viewallbutton)
        return viewheader*/
    }
}

extension THSocialViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let user = THSocialInteractionPostsManager.sharedInstance.followPeopleList[indexPath.item]
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THProfilePostsManager.sharedInstance.profilePosts = [SocialInteraction]()
        THProfilePostsManager.sharedInstance.page = 1
        THProfilePostsManager.sharedInstance.didLoadData = false
        THProfilePostsManager.sharedInstance.retrieveProfilePosts(with: user.id) { (error) in
            guard error != nil else {
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber - 1
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let userPostProfileVC = storyBoard.instantiateViewController(withIdentifier: PZProfileTableViewController.className) as! PZProfileTableViewController
                userPostProfileVC.profile = THProfilePostsManager.sharedInstance.profile
                userPostProfileVC.isSearch = true
                userPostProfileVC.userId = user.id
                let profileNavVC = UINavigationController(rootViewController: userPostProfileVC)
//                self.navigationController?.pushViewController(userPostProfileVC, animated: true)
                self.present(profileNavVC, animated: true, completion: nil)
                return
            }
        }

    }


}

extension THSocialViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return THSocialInteractionPostsManager.sharedInstance.followPeopleList.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: THSuggestionUserCell.cellIdentifier(), for: indexPath) as! THSuggestionUserCell
        let userFollow = THSocialInteractionPostsManager.sharedInstance.followPeopleList[indexPath.item]

        cell.imageView.sd_setImage(with: URL(string: userFollow.thumbnailImageUrl), placeholderImage: nil, options: .retryFailed, completed: nil)
        cell.nameLabel.text = userFollow.username
        cell.followButton.tag = indexPath.item
        cell.closeButton.tag = indexPath.item
        cell.closeButton.addTarget(self, action: #selector(THSocialViewController.didTapSkipButton(_:)), for: .touchUpInside)
        cell.followButton.addTarget(self, action: #selector(THSocialViewController.didTapFollowButton(_:)), for: UIControlEvents.touchUpInside)
        cell.verifyCheckImageView.isHidden = !userFollow.isVerified

        return cell
    }
}

extension THSocialViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 130, height: 150)
    }
}

extension THSocialViewController : CLLocationManagerDelegate{

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let currentLocation = locations.first
        let latitude = currentLocation?.coordinate.latitude
        let longitude = currentLocation?.coordinate.longitude
        self.locationManager.stopUpdatingLocation()
        manager.stopUpdatingLocation()

        if(delegate.isLocationUpdated)! && (isCurrentLocationDone)! {

        }
        else {
            isCurrentLocationDone = true
            updateUserCurrentLocation(latitude: latitude!, longitude: longitude!)
        }
    }
}

extension THSocialViewController: SocialSwipeDelegate {

}



