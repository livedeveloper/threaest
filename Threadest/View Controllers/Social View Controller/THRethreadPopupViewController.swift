//
//  THRethreadPopupViewController.swift
//  Threadest
//
//  Created by Jaydeep on 11/04/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import Lottie

class THRethreadPopupViewController: THBaseViewController {
    var completionHander : ((_ rethreadPlatform: NSMutableArray?)->())?
    
    
    @IBOutlet weak var objTableView: UITableView!
    @IBOutlet weak var rethreadButton: DesignableButton!
    @IBOutlet weak var rethreadPopupView: UIView!
    
    let imageArray = ["rethread_thredest_icon","facebook","twitter"]
    let nameArray = ["Threadest","Facebook","Twitter"];
    
    //Variable
    var selectedIndexArray:NSMutableArray = ["Threadest","Facebook","Twitter"]
    var productLinkString: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        rethreadPopupView.layer.cornerRadius = 8
        rethreadPopupView.layer.masksToBounds = true
        objTableView.register(UINib(nibName: THRethreadTableViewCell.className, bundle: nil), forCellReuseIdentifier: THRethreadTableViewCell.className)
        objTableView.delegate = self
        objTableView.dataSource = self
        self.showAnimate()
    }
    
    


    @IBAction func didTapOnRethreadButton(_ sender: Any) {
        if selectedIndexArray.count > 0 {
            completionHander!(selectedIndexArray)
            self.removeAnimate()
        }
    }
    
    @IBAction func didTapOnCopyLink(_ sender: Any) {
        let pasteBoard = UIPasteboard.general
        pasteBoard.string = productLinkString
        
        let animationDoneView = LOTAnimationView(name: "done_button")
        animationDoneView.animationProgress = 100
        animationDoneView.backgroundColor = UIColor.clear
        animationDoneView.frame = CGRect(x: 0, y: 0, width: 400, height: 400)
        animationDoneView.center = view.center
        animationDoneView.contentMode = .scaleAspectFill
        animationDoneView.animationSpeed = 0.8
        view.addSubview(animationDoneView)
        animationDoneView.loopAnimation = false
        animationDoneView.play { (sucess) in
            animationDoneView.pause()
            animationDoneView.removeFromSuperview()
        }
        
    }
    
    @IBAction func didTapOnCloseButton(_ sender: Any) {
        self.removeAnimate()
    }
    
    @IBAction func didTapOnOutside(_ sender: Any) {
        self.removeAnimate()
    }
    
    func didTapOnRethreadSwitch(_ sender: UISwitch) {
        if selectedIndexArray.contains(nameArray[sender.tag]) {
            selectedIndexArray.remove(nameArray[sender.tag])
        } else {
            selectedIndexArray.add(nameArray[sender.tag])
        }
        objTableView.reloadData()
    }
}

extension THRethreadPopupViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension THRethreadPopupViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let rethreadCell = tableView.dequeueReusableCell(withIdentifier: THRethreadTableViewCell.className, for: indexPath) as! THRethreadTableViewCell
        rethreadCell.selectionStyle = UITableViewCellSelectionStyle.none
        rethreadCell.nameLabel.text = nameArray[indexPath.row]
        rethreadCell.imageThreadest.image = UIImage(named: imageArray[indexPath.row])
        rethreadCell.rethreadSwitch.isOn = (selectedIndexArray.contains(nameArray[indexPath.row])) ? true: false
        rethreadCell.rethreadSwitch.tag = indexPath.row
        rethreadCell.rethreadSwitch.addTarget(self, action: #selector(THRethreadPopupViewController.didTapOnRethreadSwitch(_:)), for: .valueChanged)
        return rethreadCell
    }
}
