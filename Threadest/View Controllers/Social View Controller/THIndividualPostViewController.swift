//
//  THIndividualPostViewController.swift
//  Threadest
//
//  Created by Jaydeep on 22/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THIndividualPostViewController: UIViewController {

    //Outlet
    @IBOutlet weak var cartCountLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var postImage: UIImageView!

    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var commentCountLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var rethreadButton: UIButton!
    @IBOutlet weak var rethreadCountLabel: UILabel!

    @IBOutlet weak var commentLabel: UILabel!

    @IBOutlet weak var individualPostLabel: UILabel!


    var isFromNotification:Bool = false


    var selectedIndex:Int = 0


    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        initData()
    }

    private func initUI() {
        cartCountLabel.layer.cornerRadius = cartCountLabel.frame.width / 2.0
        cartCountLabel.layer.masksToBounds = true

        profileImage.layer.cornerRadius = profileImage.frame.size.width/2
        profileImage.layer.masksToBounds = true
        
        if let cartCount = UserDefaults.standard.value(forKey: "cartCount")
        {
            if (cartCount as! Int) == 0 {
                self.cartCountLabel.isHidden = true
            }
            else {
                self.cartCountLabel.isHidden = false
                self.cartCountLabel.text = "\(cartCount as! Int)"
            }
        } else {
            self.cartCountLabel.isHidden = true
        }
    }

    private func initData() {
        var socialInteraction : SocialInteraction!

        if(isFromNotification)
        {
            socialInteraction = THProfilePostsManager.sharedInstance.notificationPost
        }
        else
        {
            socialInteraction = THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[selectedIndex]
        }


        profileImage.sd_setImage(with: URL(string: socialInteraction.ownerImageUrl), placeholderImage: nil, options: .retryFailed)
        usernameLabel.text = socialInteraction.ownerUsername
        commentCountLabel.text = "\(socialInteraction.commentsCount)"
        likeCountLabel.text = "\(socialInteraction.upvotesCount)"
        likeButton.isSelected = socialInteraction.likedByCurrentUser ? true: false
        timeLabel.text = GeneralMethods.sharedInstance.getTimeAgoStringFrom(dateString: socialInteraction.createdAt)

        commentLabel.attributedText = GeneralMethods.sharedInstance.setAttributedStringWithColor_Font(text: "\(socialInteraction.ownerUsername) \(socialInteraction.comments[0].comment)", partOfString: socialInteraction.ownerUsername, color: UIColor.AppFontBlack(), font: UIFont.AppFontSemiBold(16))
        postImage.sd_setImage(with: URL(string: socialInteraction.largeImageUrl), placeholderImage: nil, options: .retryFailed)
        individualPostLabel.isHidden = isFromNotification
    }

    @IBAction func didTapBackButton(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }

    @IBAction func didShoppingCartButton(_ sender: Any) {
        
        let shoppingcartController = THYourCartViewController(nibName: THYourCartViewController.className, bundle: nil)
        self.navigationController?.pushViewController(shoppingcartController, animated: true)
    }

    @IBAction func didTapLikeButton(_ sender: Any) {
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            let socialInteraction = THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[selectedIndex]
            THUserManager.sharedInstance.performUpvote(for: usercurrent.userId, votableId: socialInteraction.id, votableType: "Post", completion: { (vote, error) in
                guard error != nil else {
                    self.likeCountLabel.text = "\(socialInteraction.upvotesCount + 1)"
                    self.likeButton.isSelected  = true
                    return
                }
            })
        }

    }


}


