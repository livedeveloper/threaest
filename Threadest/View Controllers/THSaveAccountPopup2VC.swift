//
//  THSaveAccountPopup2VC.swift
//  Threadest
//
//  Created by Jaydeep on 26/07/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THSaveAccountPopup2VC: THBaseViewController {
    @IBOutlet weak var usernameTextField: DesignableTextField!
    @IBOutlet weak var phoneTextField: DesignableTextField!
    fileprivate var textFieldsArray = [UITextField]()
    @IBOutlet weak var subView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showAnimate()
        textFieldsArray = [usernameTextField, phoneTextField]
        subView.layer.cornerRadius = 10
        subView.layer.masksToBounds = true
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(gesture:))))
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        appdelegate.verticle.scrollEnabled(isEnable: false)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
//        appdelegate.verticle.scrollEnabled(isEnable: true)
    }
    
    fileprivate func validateFields() -> Bool {
        guard let usernameTextFieldText = usernameTextField.text, usernameTextFieldText.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
            showAlert(withMessage: "Please insert the username!")
            
            return false
        }
        
        guard let phoneTextFieldText = phoneTextField.text, phoneTextFieldText.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
            showAlert(withMessage: "Please insert the phone number!")
            
            return false
        }
        return true
    }
    
    @objc fileprivate func dismissKeyboard(gesture: UITapGestureRecognizer) {
        view.endEditing(true)
    }

   
    @IBAction func didTapOnCloseButton(_ sender: Any) {
        removeAnimate()
    }
    
    @IBAction func didTapOnSave(_ sender: Any) {
        guard validateFields() else {
            return
        }
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            var user: User = usercurrent
            user.username = usernameTextField.text!
            user.phoneNumber = phoneTextField.text!
            user.tempAccount = false
            THUserManager.sharedInstance.performUpdateUserAccount(for: usercurrent.userId, user: user, completion: { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                guard let error = error else {
                    self.removeAnimate()
                    return
                }
                print(error.localizedDescription)
            })
        }
    }
}

extension THSaveAccountPopup2VC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let index = textFieldsArray.index { $0 === textField }
        if let index = index, index < textFieldsArray.count - 1 {
            let nextTextField = textFieldsArray[index + 1]
            nextTextField.becomeFirstResponder()
        } else {
            self.view.endEditing(true)
        }
        
        return true
    }
}

