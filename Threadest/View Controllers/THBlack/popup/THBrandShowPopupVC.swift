//
//  THBrandShowPopupVC.swift
//  Threadest
//
//  Created by Jaydeep on 26/07/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SDWebImage
class THBrandShowPopupVC: THBaseViewController {
    @IBOutlet weak var viewOut: UIView!
    @IBOutlet weak var brandNameLabel: UILabel!
    @IBOutlet weak var brandImage: UIImageView!
    @IBOutlet weak var brandScoreLabel: UILabel!
    @IBOutlet weak var shadowView: UIView!
    
    @IBOutlet weak var brandNamefooterLabel: UILabel!
    open var blackBrandPopupCompletionHander : (()->())?
    
    var brand: Brand!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializeView()
        showAnimate()
        THUserManager.sharedInstance.retrieveLoyaltyStatusBrand(brandId: brand.id) { (error) in
            if THUserManager.sharedInstance.loyaltyStatusBrand == 0 {
                self.brandScoreLabel.isHidden = true
            } else {
                self.brandScoreLabel.text = " +\(THUserManager.sharedInstance.loyaltyStatusBrand) "
                self.brandScoreLabel.alpha = 0
                self.brandScoreLabel.isHidden = false
                UIView.animate(withDuration: 0.5, animations: {
                    self.brandScoreLabel.alpha = 1
                })
            }
        }
        // Do any additional setup after loading the view.
    }

    private func initializeView() {
        viewOut.layer.cornerRadius = 2
        brandNameLabel.text = brand.name
        brandImage.sd_setImage(with: URL(string: brand.logoURL), placeholderImage: nil, options: .retryFailed)
        brandImage.layer.borderWidth = 1
        brandImage.layer.borderColor = UIColor.lightGray.cgColor
        brandImage.layer.cornerRadius = brandImage.frame.size.width/2
        shadowView.layer.shadowColor = UIColor.lightGray.cgColor
        shadowView.layer.shadowOpacity = 0.8
        shadowView.layer.shadowRadius = 3
        shadowView.layer.shadowPath = UIBezierPath(roundedRect: shadowView.bounds, cornerRadius: 100).cgPath
        brandImage.layer.masksToBounds = true
        brandScoreLabel.layer.cornerRadius = 15
        brandScoreLabel.layer.masksToBounds = true
        brandNamefooterLabel.text = "Share a photo with \(brand.name) tagged in it to earn 1 point."
    }

    @IBAction func didTapOnClosetButton(_ sender: Any) {
        removeAnimate()
    }
    
    @IBAction func didTapOnShareNowButton(_ sender: Any) {
        removeAnimate()
        blackBrandPopupCompletionHander!()
    }
    
    @IBAction func didTapBrandButton(_ sender: Any) {
        self.view.removeFromSuperview()
        
        let brandProfileVC:THBrandProfileViewController = THBrandProfileViewController()
        brandProfileVC.isFrom = "brandlogo"
        brandProfileVC.brandId = brand.id
        brandProfileVC.isPopup = true
        let navigationController = UINavigationController(rootViewController: brandProfileVC)
        navigationController.isNavigationBarHidden = true
        
        self.parent?.present(navigationController, animated: true, completion: nil)

    }
}
