//
//  THBrandFollowUnFollowPopupVC.swift
//  Threadest
//
//  Created by Jaydeep on 14/08/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THBrandFollowUnFollowPopupVC: THBaseViewController {

    
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var brandView: UIView!
    
    @IBOutlet weak var brandImageView: UIImageView!
    @IBOutlet weak var brandNameLabel: UILabel!
    @IBOutlet weak var followUnFollow: DesignableButton!
    
    @IBOutlet weak var objActivityIndicator: UIActivityIndicatorView!
    var brand: SearchBrand!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        showAnimate()
        
        self.showViewIndicator()
        THNetworkManager.sharedInstance.performisFollowingBrand(brandId: brand.id!) { (json, error) in
            self.dismissViewIndicator()
            guard let json = json else {
                self.followUnFollow.setTitle("Follow", for: .normal)
                return
            }
            if json["data"].boolValue {
                self.followUnFollow.setTitle("UnFollow", for: .normal)
            } else {
                self.followUnFollow.setTitle("Follow", for: .normal)
            }
        }

        // Do any additional setup after loading the view.
    }
    
    private func initializeView() {
        self.objActivityIndicator.hidesWhenStopped = true
        subView.layer.shadowRadius = 2
        brandNameLabel.text = brand.name
        if let brandLogo = brand.brand_logo_image_url {
            brandImageView.sd_setImage(with: URL(string: brandLogo), placeholderImage: nil, options: .retryFailed)
        }
        brandImageView.layer.borderWidth = 1
        brandImageView.layer.borderColor = UIColor.lightGray.cgColor
        brandImageView.layer.cornerRadius = brandImageView.frame.size.width/2
        brandView.layer.shadowColor = UIColor.lightGray.cgColor
        brandImageView.layer.shadowOffset = CGSize.zero
        brandView.layer.shadowOpacity = 0.8
        brandView.layer.shadowRadius = 3
        brandView.layer.shadowPath = UIBezierPath(roundedRect: brandView.bounds, cornerRadius: 100).cgPath
        brandImageView.layer.masksToBounds = true
    }

    private func showViewIndicator() {
        self.objActivityIndicator.startAnimating()
        self.objActivityIndicator.isHidden = false
    }
    
    private func dismissViewIndicator() {
        self.objActivityIndicator.stopAnimating()
        self.objActivityIndicator.isHidden = true
    }

    @IBAction func didTapOnFollowUnFollowButton(_ sender: Any) {
       /* if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }*/
        if followUnFollow.titleLabel?.text == "Follow" {
            self.showViewIndicator()
            THSocialInteractionPostsManager.sharedInstance.performFollowBrand(for:brand.id!) { (error) in
                self.dismissViewIndicator()
                self.followUnFollow.setTitle("UnFollow", for: .normal)
                self.removeAnimate()
            }
        }else {
            self.showViewIndicator()
            THSocialInteractionPostsManager.sharedInstance.performunFollowBrand(for: brand.id!, completion: { (error) in
                self.dismissViewIndicator()
                self.followUnFollow.setTitle("Follow", for: .normal)
            })
        }
    }
    
    @IBAction func didTapOnCloseButton(_ sender: Any) {
        removeAnimate()
    }
    @IBAction func didTapBrandButton(_ sender: Any) {
        self.view.removeFromSuperview()
        
        let brandProfileVC:THBrandProfileViewController = THBrandProfileViewController()
        brandProfileVC.isFrom = "brandlogo"
        brandProfileVC.brandId = brand.id
        brandProfileVC.isPopup = true
        let navigationController = UINavigationController(rootViewController: brandProfileVC)
        navigationController.isNavigationBarHidden = true
        
        self.parent?.present(navigationController, animated: true, completion: nil)
        
    }
    
}
