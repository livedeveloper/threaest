//
//  THBrandClosetPopupVC.swift
//  Threadest
//
//  Created by Jaydeep on 27/07/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THBrandClosetPopupVC: THBaseViewController {
    @IBOutlet weak var threadestPopup: UIView!
    
    open var BrandClosetCompletionHander : (()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        threadestPopup.layer.cornerRadius = 10
        threadestPopup.layer.masksToBounds = true
        showAnimate()
        // Do any additional setup after loading the view.
    }

    @IBAction func didTapOnNextButton(_ sender: Any) {
        removeAnimate()
        BrandClosetCompletionHander!()
    }
    
    @IBAction func didTapOnClosetButton(_ sender: Any) {
        removeAnimate()
        BrandClosetCompletionHander!()
    }
    
}
