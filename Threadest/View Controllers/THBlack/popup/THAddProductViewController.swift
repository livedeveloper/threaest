//
//  THAddProductViewController.swift
//  Threadest
//
//  Created by Jaydeep on 04/09/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THAddProductViewController: THBaseViewController {

    var AddProductcompletionHander : (()->())?
    
    @IBOutlet weak var titleTextField: DesignableTextField!
    
    @IBOutlet weak var productUrlTextField: DesignableTextField!
    
    @IBOutlet weak var defaultPriceTextField: DesignableTextField!
    
    @IBOutlet weak var descriptionTextField: DesignableTextField!
    
    var brandId = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showAnimate()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func didTapOnSaveButton(_ sender: Any) {
        guard validateFields() else {
            return
        }
        
        THBrandProductManager.sharedInstance.performAddProduct(with: AddProduct(productTitle: titleTextField.text!, productURL: productUrlTextField.text!, productBrandId: brandId, productDefaultPrice: defaultPriceTextField.text!, productDescription: descriptionTextField.text!)) { (error) in
            guard let error = error else {
                return
            }
            
            self.removeAnimate()
            self.AddProductcompletionHander!()
        }
    }
    
    @IBAction func didTapOnClosetButton(_ sender: Any) {
        self.removeAnimate()
    }
    
    fileprivate func validateFields() -> Bool {
        guard let titleTextFieldText = titleTextField.text, titleTextFieldText.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
            showAlert(withMessage: "Please insert title!")
            
            return false
        }
        
        
        guard let productUrlTextFieldText = productUrlTextField.text, productUrlTextFieldText.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
            showAlert(withMessage: "Please insert a product url!")
            
            return false
        }
        
        guard let defaultPriceTextFieldText = defaultPriceTextField.text, defaultPriceTextFieldText.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
            showAlert(withMessage: "Please insert the product default price!")
            
            return false
        }
        
        guard let descriptionTextFieldText = descriptionTextField.text, descriptionTextFieldText.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
            showAlert(withMessage: "Please insert product description")
            
            return false
        }
        return true
    }
}
