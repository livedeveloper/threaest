//
//  THBlackIntroViewController.swift
//  Threadest
//
//  Created by Sabir Shah on 4/28/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THBlackIntroViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapOnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTapOnAgreeButton(_ sender: Any) {
        if let currentUser = THUserManager.sharedInstance.currentUserdata() {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THEarlyAccess.sharedInstance.subscribeToEarlyAccess(toEmail: currentUser.email) { (error) in
                
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                if let error = error {
                    self.showAlert(title: "", message: error.localizedDescription, okClick: { 
                        
                    })
                } else
                {
                    let ahead = THEarlyAccess.sharedInstance.subscriber?.earlyAccessPosition
                    print(ahead as Any)
                    
                    let thBlackShareController = THThreadestBlackShareViewController(nibName: THThreadestBlackShareViewController.className, bundle: nil)
                    thBlackShareController.subscriberInfo = THEarlyAccess.sharedInstance.subscriber
                    self.navigationController?.pushViewController(thBlackShareController, animated: true)
                }
                
            }

        }
    }
 
}
