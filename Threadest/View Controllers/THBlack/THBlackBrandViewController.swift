//
//  THBlackBrandViewController.swift
//  Threadest
//
//  Created by Sabir Shah on 4/29/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THBlackBrandViewController: UIViewController {
    @IBOutlet weak var SearchBoxTextField: UITextField!
    @IBOutlet weak var objColletionView: UICollectionView!
    @IBOutlet weak var DiscoverButton: UIButton!
    @IBOutlet weak var MyBrandsButton: UIButton!
    @IBOutlet weak var EmergingBrand: UIButton!
    
    
    @IBOutlet weak var containerView: UIView!
    
    open var blackBrandCompletionHander : (()->())?
    
    //search variable
    var page:UInt = 0
    var maxNoOfPages:UInt = 0
    
    var searchedResuls:NSArray = []
    var searchQuery:THSearchQueryManager?
    var isMyBrand = "my"
    var myBrand = [Brand]()
    var isBrandClosetPopup = false
    var isAddProduct = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDoneOnKeyboard()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        setupData()
        
        SearchBoxTextField.layer.cornerRadius = 0.5 * SearchBoxTextField.bounds.size.height
        SearchBoxTextField.clipsToBounds = true
        SearchBoxTextField.attributedPlaceholder = NSAttributedString(string: "Search",attributes: [NSForegroundColorAttributeName: UIColor.white])
        let searchImageView = UIImageView(image: UIImage(named: "search"))
        searchImageView.contentMode = .scaleAspectFit
        searchImageView.frame.size.width = 50
        SearchBoxTextField.rightView  = searchImageView
        SearchBoxTextField.rightViewMode = .always
        
        MyBrandsButton.layer.cornerRadius = 0.5 * MyBrandsButton.bounds.size.height
        MyBrandsButton.clipsToBounds = true
        DiscoverButton.layer.cornerRadius = 0.5 * DiscoverButton.bounds.size.height
        DiscoverButton.clipsToBounds = true
        EmergingBrand.layer.cornerRadius = 0.5 * EmergingBrand.bounds.size.height
        EmergingBrand.clipsToBounds = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if isBrandClosetPopup {
            let brandPopup = THBrandClosetPopupVC(nibName: THBrandClosetPopupVC.className, bundle: nil)
            brandPopup.BrandClosetCompletionHander = {
                self.SearchBoxTextField.becomeFirstResponder()
            }
            self.addChildViewController(brandPopup)
            brandPopup.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
            self.view.addSubview(brandPopup.view)
        } else {
            if ThreadestStorage.isCoachMarkBrandDefault() == true {
                self.view.endEditing(true)
                let coach = THCoachMarkPageViewController(nibName: THCoachMarkPageViewController.className, bundle: nil)
                coach.isFromCloset = false
                self.addChildViewController(coach)
                coach.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
                self.view.addSubview(coach.view)
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
         self.view.endEditing(true)
    }
    
    func setDoneOnKeyboard() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(THBlackBrandViewController.dismissKeyboard))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        self.SearchBoxTextField.inputAccessoryView = keyboardToolbar
    }
    
    override func dismissKeyboard() {
        view.endEditing(true)
    }

    func setupData() {
        switch isMyBrand {
        case "my":
            MyBrandsButton.tag = 1
            MyBrandsButton.backgroundColor = #colorLiteral(red: 0.2049376667, green: 0.2085670829, blue: 0.261128664, alpha: 1)
            DiscoverButton.backgroundColor = UIColor.clear
            EmergingBrand.backgroundColor = UIColor.clear
        case "discover":
            MyBrandsButton.tag = 2
            MyBrandsButton.backgroundColor = UIColor.clear
            DiscoverButton.backgroundColor = #colorLiteral(red: 0.2049376667, green: 0.2085670829, blue: 0.261128664, alpha: 1)
            EmergingBrand.backgroundColor = UIColor.clear
        case "emerging":
            MyBrandsButton.tag = 3
            MyBrandsButton.backgroundColor = UIColor.clear
            DiscoverButton.backgroundColor = UIColor.clear
            EmergingBrand.backgroundColor = #colorLiteral(red: 0.2049376667, green: 0.2085670829, blue: 0.261128664, alpha: 1)
        default:
            break
        }
        objColletionView.register(UINib(nibName: BrandBlackCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: BrandBlackCollectionViewCell.className)
        self.objColletionView.keyboardDismissMode = .onDrag
        self.searchQuery = THSearchQueryManager()
        loadMore()
        switch isMyBrand {
        case "my":
            loadMyBrand()
        case "discover":
            searchResults(search: SearchBoxTextField.text!, searchType: "Brand")
            if !isBrandClosetPopup {
                if userdefault.object(forKey: userDefaultKey.coachMarkBrandDefault.rawValue) != nil {
                    self.SearchBoxTextField.becomeFirstResponder()
                }
            }
        case "emerging":
            break
        default:
            break
        }
    }
    
    func loadMore() {
        self.objColletionView.addInfiniteScrolling {
            self.page+=1
            if (self.page  >= self.maxNoOfPages) {
                self.objColletionView.infiniteScrollingView.stopAnimating();
                return // All pages already loaded
            } else {
                self.fetchMoreResults()
            }
        }
    }
    
    func loadMyBrand() {
        THProfilePostsManager.sharedInstance.profileBrandFollowing = [Brand]()
        THProfilePostsManager.sharedInstance.pageBrand = 1
        THProfilePostsManager.sharedInstance.didLoadDataBrand = false
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            THProfilePostsManager.sharedInstance.retrieveProfileBrandFollowing(with: usercurrent.userId) { (error) in
                guard let error = error else {
                    self.myBrand = THProfilePostsManager.sharedInstance.profileBrandFollowing
                    self.objColletionView.reloadData()
                    return
                }
                debugPrint(error)
            }
        }
    }
    
    //MARK: Search result from algolia
    private func fetchMoreResults() {
        self.searchQuery?.query?.page = self.page
        if isMyBrand == "emerging" {
            self.searchQuery?.query?.filters = "emerging_brand:true"
        } else {
            self.searchQuery?.query?.filters = ""
        }
        self.searchQuery?.queryResults(search: SearchBoxTextField.text!, searchType: "Brand") { (searchList, nbPage) in
            let moreResults:NSArray = searchList as! NSArray
            let finalArray:NSMutableArray = []
            for (index, element) in self.searchedResuls.enumerated() {
                print("Item \(index): \(element)")
                finalArray.add(element)
            }
            for (index, element) in moreResults.enumerated() {
                print("Item \(index): \(element)")
                finalArray.add(element)
            }
            self.searchedResuls = finalArray as NSArray
            self.maxNoOfPages = nbPage
            self.objColletionView.reloadData()
            self.objColletionView.infiniteScrollingView.stopAnimating();
        }
    }
    
    func searchResults(search:String, searchType:String){
        self.page = 0
        self.searchQuery?.query?.page = self.page
        self.searchQuery?.query?.filters = ""
        self.searchQuery?.queryResults(search: search, searchType: searchType) { (searchList, nbPage) in
            self.searchedResuls = []
            self.searchedResuls = searchList as! NSArray
            if(self.searchedResuls.count>0) {
                self.objColletionView.isHidden = false
            } else {
                self.objColletionView.isHidden = true
            }
            self.maxNoOfPages = nbPage
            self.objColletionView.reloadData()
        }
    }
    
    func searchResultsBrandFilter(search:String, searchType:String){
        self.page = 0
        self.searchQuery?.query?.page = self.page
        self.searchQuery?.query?.filters = "emerging_brand:true"
        self.searchQuery?.queryResults(search: search, searchType: searchType) { (searchList, nbPage) in
            self.searchedResuls = []
            self.searchedResuls = searchList as! NSArray
            if(self.searchedResuls.count>0) {
                self.objColletionView.isHidden = false
            } else {
                self.objColletionView.isHidden = true
            }
            self.maxNoOfPages = nbPage
            self.objColletionView.reloadData()
        }
    }
    
    @IBAction func didTapOnClose(_ sender: Any) {
        //self.popoverPresentationController?.dismissalTransitionDidEnd(true)
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func tapOnMyBrandButton(_ sender: Any) {
        MyBrandsButton.tag = 1
        MyBrandsButton.backgroundColor = #colorLiteral(red: 0.2049376667, green: 0.2085670829, blue: 0.261128664, alpha: 1)
        DiscoverButton.backgroundColor = UIColor.clear
        EmergingBrand.backgroundColor = UIColor.clear
        isMyBrand = "my"
        objColletionView.reloadData()
        loadMyBrand()
        self.view.endEditing(true)
    }
    
    @IBAction func tapOnMyDiscoverButton(_ sender: Any) {
        MyBrandsButton.tag = 2
        MyBrandsButton.backgroundColor = UIColor.clear
        DiscoverButton.backgroundColor = #colorLiteral(red: 0.2049376667, green: 0.2085670829, blue: 0.261128664, alpha: 1)
        EmergingBrand.backgroundColor = UIColor.clear
        isMyBrand = "discover"
        objColletionView.reloadData()
        searchResults(search: SearchBoxTextField.text!, searchType: "Brand")
        SearchBoxTextField.becomeFirstResponder()
    }
    
    
    @IBAction func tapOnEmergingButton(_ sender: Any) {
        MyBrandsButton.tag = 3
        MyBrandsButton.backgroundColor = UIColor.clear
        DiscoverButton.backgroundColor = UIColor.clear
        EmergingBrand.backgroundColor =  #colorLiteral(red: 0.2049376667, green: 0.2085670829, blue: 0.261128664, alpha: 1)
        isMyBrand = "emerging"
        objColletionView.reloadData()
        self.searchResultsBrandFilter(search: "", searchType: "Brand")
        SearchBoxTextField.becomeFirstResponder()
    }
    
    @IBAction func didTapOnDoneButton(_ sender: Any) {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    override func constrainViewEqual(holderView: UIView, view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        //pin 100 points from the top of the super
        let pinTop = NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal,
                                        toItem: holderView, attribute: .top, multiplier: 1.0, constant: 0)
        let pinBottom = NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal,
                                           toItem: holderView, attribute: .bottom, multiplier: 1.0, constant: 0)
        let pinLeft = NSLayoutConstraint(item: view, attribute: .left, relatedBy: .equal,
                                         toItem: holderView, attribute: .left, multiplier: 1.0, constant: 0)
        let pinRight = NSLayoutConstraint(item: view, attribute: .right, relatedBy: .equal,
                                          toItem: holderView, attribute: .right, multiplier: 1.0, constant: 0)
        
        holderView.addConstraints([pinTop, pinBottom, pinLeft, pinRight])
    }
}

extension THBlackBrandViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if isMyBrand == "my"{
            if indexPath.item == self.myBrand.count - 1 {
                if THProfilePostsManager.sharedInstance.didLoadDataBrand {
                    THProfilePostsManager.sharedInstance.didLoadDataBrand = false
                    if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                        THProfilePostsManager.sharedInstance.retrieveProfileBrandFollowing(with: usercurrent.userId) { (error) in
                            guard let error = error else {
                                self.myBrand = THProfilePostsManager.sharedInstance.profileBrandFollowing
                                self.objColletionView.reloadData()
                                return
                            }
                            debugPrint(error)
                        }
                    }
                }
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.view.endEditing(true)
        if isAddProduct {
            let addProductPopup = THAddProductViewController(nibName: THAddProductViewController.className, bundle: nil)
            self.addChildViewController(addProductPopup)
            addProductPopup.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
            let brandId = isMyBrand == "my" ? self.myBrand[indexPath.row].id : (self.searchedResuls[indexPath.row] as! SearchBrand).id
            addProductPopup.brandId = brandId!
            addProductPopup.AddProductcompletionHander = {
                self.dismiss(animated: true, completion: nil)
            }
            self.view.addSubview(addProductPopup.view)
        } else {
            if isMyBrand == "discover" || isMyBrand == "emerging"{
                let brandFollowpopup = THBrandFollowUnFollowPopupVC(nibName: THBrandFollowUnFollowPopupVC.className, bundle: nil)
                brandFollowpopup.brand = self.searchedResuls[indexPath.row] as! SearchBrand
                self.addChildViewController(brandFollowpopup)
                brandFollowpopup.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
                self.view.addSubview(brandFollowpopup.view)
                brandFollowpopup.didMove(toParentViewController: self)
            } else {
                let brandShowpopup = THBrandShowPopupVC(nibName: THBrandShowPopupVC.className, bundle: nil)
                brandShowpopup.blackBrandPopupCompletionHander = {
                    self.dismiss(animated: true, completion: {
                        self.blackBrandCompletionHander!()
                    })
                }
                brandShowpopup.brand = self.myBrand[indexPath.row]
                self.addChildViewController(brandShowpopup)
                brandShowpopup.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
                self.view.addSubview(brandShowpopup.view)
                brandShowpopup.didMove(toParentViewController: self)
            }
        }
    }
}

extension THBlackBrandViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return isMyBrand == "my" ? myBrand.count:(self.searchedResuls as AnyObject).count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let brandCell = collectionView.dequeueReusableCell(withReuseIdentifier: BrandBlackCollectionViewCell.className, for: indexPath) as! BrandBlackCollectionViewCell
        if isMyBrand == "my" {
            brandCell.configureMyBrand(brand: myBrand[indexPath.row], isAddBrand: isAddProduct)
        } else {
            let brandObj = self.searchedResuls[indexPath.row] as! SearchBrand
            brandCell.configure(searchBrand: brandObj)
        }
        return brandCell
    }
}

extension THBlackBrandViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if MyBrandsButton.tag == 1 {
            if textField.text == "" {
                isMyBrand = "my"
                objColletionView.reloadData()
            }
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let search = textFieldText.replacingCharacters(in: range, with: string)
        //isMyBrand = "discover"
        self.loadMore()
        if isMyBrand == "discover" {
            searchResults(search: search, searchType: "Brand")
        } else if isMyBrand == "emerging" {
            searchResultsBrandFilter(search: search, searchType: "Brand")
        }
        
        return true;
    }
}
