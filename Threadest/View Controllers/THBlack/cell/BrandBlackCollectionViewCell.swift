//
//  BrandBlackCollectionViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 08/07/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import IoniconsSwift

class BrandBlackCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var brandImageView: UIImageView!
   
    @IBOutlet weak var scoreButton: UIButton!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var greenCheckMarkView: UIView!
    @IBOutlet weak var viewBrand: UIView!
    
    @IBOutlet weak var blackOverlay: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewBrand.layer.cornerRadius = 65
        viewBrand.layer.masksToBounds = true
        greenCheckMarkView.layer.cornerRadius = 15
        greenCheckMarkView.layer.masksToBounds = true
        scoreButton.setImage(Ionicons.trophy.image(20, color: .yellow), for: .normal)
      //  greenCheckMarkImageView.isHidden = true
        
    }
    
    func configure(searchBrand: SearchBrand) {
        if let brandLogo = searchBrand.brand_logo_image_url {
            brandImageView.sd_setImage(with: URL(string: brandLogo), placeholderImage: nil, options: .retryFailed)
        }
        blackOverlay.isHidden = false
        greenCheckMarkView.isHidden = false
        scoreLabel.isHidden = true
        greenCheckMarkView.isHidden = true
    }
    
    func configureMyBrand(brand: Brand, isAddBrand: Bool) {
        greenCheckMarkView.isHidden = false
        brandImageView.sd_setImage(with: URL(string: brand.logoURL), placeholderImage: nil, options: .retryFailed)
        scoreLabel.isHidden = true
        blackOverlay.isHidden = true
        greenCheckMarkView.isHidden = isAddBrand ? true : false
        greenCheckMarkView.backgroundColor = UIColor.white
    }

}
