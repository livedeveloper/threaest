//
//  THProfileSetupViewController.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 17/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import Crashlytics

class THProfileSetupViewController: UIViewController {
    open var setUPcompletionHander : (()->())?

    @IBOutlet weak var maleBubbleView: UIView!
    @IBOutlet weak var femaleBubbleView: UIView!
    @IBOutlet weak var otherBubbleView: UIView!

    //constraint outlet
    @IBOutlet weak var topMaleConstratint: NSLayoutConstraint!
    @IBOutlet weak var topFemaleConstraint: NSLayoutConstraint!
    @IBOutlet weak var topOtherConstraint: NSLayoutConstraint!


    @IBOutlet weak var profileSetupTableView: UITableView!

    fileprivate var bubbleViews = [UIView]()
    fileprivate var profileSetupItems = [ProfileSetupItem]()

    //picker view outlet
    @IBOutlet weak var ViewPicker: UIView!
    @IBOutlet weak var objPickerView: UIPickerView!

    let userHeightArray = ["4'5","4'6","4'7","4'8","4'9","4'10","4'11","5'0","5'1","5'2","5'3","5'4","5'5","5'6","5'7","5'8","5'9","5'10","5'11","6'0","6'1","6'2","6'3","6'4","6'5","6'6","6'7","6'8","6'9","6'20","6'11","7'0","7'1","7'2","7'3","7'4"]

    var userAgeArray = ["10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80"]

    let userShirtSizesArray = ["XS","S","M","L","XL","XXL","XXXL"]

    let userMaleShoeSizeArray = ["6","6.5","7","7.5","8","8.5","9","9.5","10","10.5","11","11.5","12","12.5","13","13.5","14","14.5","15","15.5","16"]
    let userFemaleShoeSizeArray = ["1","1.5","2","2.5","3","3.5","4","4.5","5","5.5","6","6.5","7","7.5","8","8.5","9","9.5","10","10.5","11","11.5","12","12.5","13"]

    let userWaistSizeArray = ["28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60"]

    let userNeckSizeArray = ["14","14.5","15","15.5","16","16.5","17","17.5","18","18.5","19","19.5","20","20.5","21","22","23","24","25","26"]

    var femaleShoeSizeArray = ["4","4.5","5","5.5","6","6.5","7","7.5","8","8.5","9","9.5","10","10.5","11","11.5","12"]
    var femaleHipsSizeArray = ["35.5","36.5","37.5","38.5","40","41.5","43.5","45.5","47.5"]
    var femaleBustSizeArray = ["32A", "32B", "32C", "32D", "32DD", "32DDD", "34A", "34B", "34C", "34D", "34DD", "34DDD", "36A", "36B", "36C", "36D", "36DD", "36DDD", "38A", "38B", "38C", "38D", "38DD", "38DDD", "40C", "40D", "40DD", "XS", "S", "M", "L", "XL"]
    var femaleWaistSizeArray = ["23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40"]

    let userHipsSizeArray = ["37.75","40.5","42.25","43.75","45.5","47.5","49.5"]

    var userPickerArray:[String] = [String]()
    var selectedRowTableView:Int = -1
    var selectedGender = ""

    var user:User = User(userId: 0, about: "", username: "", email: "", phoneNumber: "", imageURL: "",thumbnailImageURL:"" ,password: "", height: "", age: "", gender: "", shirtSize: "", waistSize: "", shoe_size: "", neckSize: "", braSize: "", hipsSize: "", cartCount: 0, closetSetupOne: false, closetSetupTwo: false, closetSetupThree: false, closetSetupComplete: false, needsToFollowBrands: false, needsToFollowUsers: false, isFollowing:false, selectedImage:nil, influencerScore: 0, sharePostCode:0, tempAccount: false, invitePromoCode: "", website: "", verifiedAccount: false)




    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func viewWillAppear(_ animated: Bool) {
    }

    fileprivate func setupUI() {
        bubbleViews = [maleBubbleView, femaleBubbleView, otherBubbleView]
        selectGenderToChangeQuestions(maleBubbleView)

        bubbleViews.forEach {
            self.addShadow(to: $0)
            $0.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapBubble(_:))))
        }

        profileSetupTableView.register(UINib(nibName: THProfileSetupTableViewCell.className, bundle: nil), forCellReuseIdentifier: THProfileSetupTableViewCell.className)
        profileSetupTableView.delegate = self
        profileSetupTableView.dataSource = self

        profileSetupTableView.reloadData()
        ViewPicker.isHidden = true
        
        guard let usercurrent = THUserManager.sharedInstance.currentUserdata() else {
            return
        }
        
        switch usercurrent.gender {
        case "male":
            selectBubble(maleBubbleView)
        case "female":
            selectBubble(femaleBubbleView)
        default:
            selectBubble(maleBubbleView)
        }
    }

    fileprivate func addShadow(to view: UIView) {
        view.layer.shadowOpacity = 0.1
        view.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        view.layer.shadowRadius = 5.0
        view.layer.shadowColor = UIColor.black.cgColor
    }

    @objc fileprivate func didTapBubble(_ recognizer: UITapGestureRecognizer) {
        let sender = recognizer.view!
        selectBubble(sender)
    }

    fileprivate func selectBubble(_ sender: UIView) {
        let otherViews = bubbleViews.filter { $0 != sender }
        selectGenderToChangeQuestions(sender)
        switch sender {
        case maleBubbleView:
            selectedGender = "male"
            self.setUserSelectedSizeData(itemHeader: "gender", selectedSize: "male")
            topMaleConstratint.constant = 14
            topFemaleConstraint.constant = 7
            topOtherConstraint.constant = 7
            break
        case femaleBubbleView:
            selectedGender = "female"
            self.setUserSelectedSizeData(itemHeader: "gender", selectedSize: "female")
            topMaleConstratint.constant = 7
            topFemaleConstraint.constant = 14
            topOtherConstraint.constant = 7
            break
        case otherBubbleView:
            selectedGender = "other"
            self.setUserSelectedSizeData(itemHeader: "gender", selectedSize: "other")
            topMaleConstratint.constant = 7
            topFemaleConstraint.constant = 7
            topOtherConstraint.constant = 14
            break
        default:
            break
        }
        otherViews.forEach { view in
            view.layer.shadowOpacity = 0.1
            view.backgroundColor = .white

            for subview in view.subviews {
                if let subview = subview as? UILabel {
                    subview.textColor = .lightGray
                }
            }
        }

        sender.backgroundColor = UIColor.richElectricBlue
        sender.layer.shadowOpacity = 0.0
        for subview in sender.subviews {
            if let subview = subview as? UILabel {
                subview.textColor = .white
            }
        }
    }

    func selectGenderToChangeQuestions(_ sender: UIView) {
        guard let usercurrent = THUserManager.sharedInstance.currentUserdata() else {
            return
        }
        switch sender {
        case maleBubbleView:
            profileSetupItems = [ProfileSetupItem(itemTitle: "What is your height?", itemOptions: userHeightArray, itemHeader: "height", selectItem: usercurrent.height),
                                 ProfileSetupItem(itemTitle: "What is your waist size?", itemOptions: userWaistSizeArray, itemHeader: "waist", selectItem: usercurrent.waistSize),
                                 ProfileSetupItem(itemTitle: "What is your neck size?", itemOptions: userNeckSizeArray, itemHeader: "neck", selectItem: usercurrent.neckSize),
                                 ProfileSetupItem(itemTitle: "What is your shirt size?", itemOptions: userShirtSizesArray, itemHeader: "shirt", selectItem: usercurrent.shirtSize),
                                 ProfileSetupItem(itemTitle: "What is your shoe size?", itemOptions: userMaleShoeSizeArray, itemHeader: "shoes", selectItem: usercurrent.shoe_size)]
            profileSetupTableView.reloadData()
            break
        case femaleBubbleView:
            profileSetupItems = [ProfileSetupItem(itemTitle: "What is your height?", itemOptions: userHeightArray, itemHeader: "height", selectItem: usercurrent.height),
                                 ProfileSetupItem(itemTitle: "What is your waist size?", itemOptions: femaleWaistSizeArray, itemHeader: "waist", selectItem: usercurrent.waistSize),
                                 ProfileSetupItem(itemTitle: "What is your bra size?", itemOptions: femaleBustSizeArray, itemHeader: "bra", selectItem: usercurrent.braSize),
                                 ProfileSetupItem(itemTitle: "What is your hips size?", itemOptions: femaleHipsSizeArray, itemHeader: "hips", selectItem: usercurrent.hipsSize),
                                 ProfileSetupItem(itemTitle: "What is your shoe size?", itemOptions: femaleShoeSizeArray, itemHeader: "shoes", selectItem: usercurrent.shoe_size)]
            profileSetupTableView.reloadData()
            break
        case otherBubbleView:
            profileSetupItems = [ProfileSetupItem(itemTitle: "What is your height?", itemOptions: userHeightArray, itemHeader: "height", selectItem: ""),
                                 ProfileSetupItem(itemTitle: "What is your age?", itemOptions: userAgeArray, itemHeader: "age", selectItem: ""),
                                 ProfileSetupItem(itemTitle: "What is your neck size?", itemOptions: userNeckSizeArray, itemHeader: "neck", selectItem: ""),
                                 ProfileSetupItem(itemTitle: "What is your shirt size?", itemOptions: userShirtSizesArray, itemHeader: "shirt", selectItem: ""),
                                 ProfileSetupItem(itemTitle: "What is your shoes size?", itemOptions: userMaleShoeSizeArray, itemHeader: "shoes", selectItem: ""),
                                 ProfileSetupItem(itemTitle: "What is your waist size?", itemOptions: userWaistSizeArray, itemHeader: "waist", selectItem: "")]
            profileSetupTableView.reloadData()
            break
        default:
            break
        }
    }

    @IBAction func didTapOnCloseButton(_ sender: Any) {
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    @IBAction func didTapNextButton(_ sender: Any) {
        for profileSetupItem in profileSetupItems {
            if profileSetupItem.selectItem != "" {
               self.setUserSelectedSizeData(itemHeader: profileSetupItem.itemHeader, selectedSize: profileSetupItem.selectItem)
            }
        }
        guard let usercurrent = THUserManager.sharedInstance.currentUserdata() else {
            return
        }
        user.website = usercurrent.website
        user.about = usercurrent.about
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THUserManager.sharedInstance.performUpdateUser(with: usercurrent.userId, user: user) { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            guard let error = error else {
                //Add funnel for Setup Closet
                Answers.logCustomEvent(withName: "Complete closet setup", customAttributes: ["userid":(THUserManager.sharedInstance.currentUserdata()?.userId)!, "gender":self.user.gender])
                self.setUPcompletionHander!()
                 _ = self.navigationController?.popToRootViewController(animated: true)
                return
            }
            debugPrint(error)
        }
    }


    @IBAction func didTapOnDonePickerButton(_ sender: Any) {
        let pickerselected = objPickerView.selectedRow(inComponent: 0)
        let selectedSize = profileSetupItems[selectedRowTableView].itemOptions[pickerselected]
         profileSetupItems[selectedRowTableView].selectItem = selectedSize
        profileSetupTableView.reloadRows(at: [IndexPath(row: selectedRowTableView, section: 0)], with: .fade)
        self.setUserSelectedSizeData(itemHeader: profileSetupItems[selectedRowTableView].itemHeader, selectedSize: selectedSize)
        ViewPicker.isHidden = true
        
        //Add funnel for Refresh button
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Measurements arrow on Setup Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }
    }

    func setUserSelectedSizeData(itemHeader: String, selectedSize: String) {
        switch itemHeader {
        case "gender":
            user.gender = selectedSize
            break
        case "height":
            user.height = selectedSize
            break
        case "age":
            user.age = selectedSize
            break
        case "neck":
            user.neckSize = selectedSize
            break
        case "shirt":
            user.shirtSize = selectedSize
            break
        case "shoes":
            user.shoe_size = selectedSize
            break
        case "waist":
            user.waistSize = selectedSize
            break
        case "bra":
            user.braSize = selectedSize
            break
        case "hips":
            user.hipsSize = selectedSize
            break
        default:
            break
        }
    }
}

extension THProfileSetupViewController: UIPickerViewDelegate {

}

extension THProfileSetupViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return userPickerArray.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return userPickerArray[row]
    }

}

extension THProfileSetupViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profileSetupItems.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 74.0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: THProfileSetupTableViewCell.className, for: indexPath) as! THProfileSetupTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.configure(with: profileSetupItems[indexPath.row])
        return cell
    }
}

extension THProfileSetupViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedRowTableView = indexPath.row
        ViewPicker.isHidden = false
        let profileSetupItem = profileSetupItems[indexPath.row]
        userPickerArray = profileSetupItem.itemOptions
        objPickerView.reloadAllComponents()
    }
}
