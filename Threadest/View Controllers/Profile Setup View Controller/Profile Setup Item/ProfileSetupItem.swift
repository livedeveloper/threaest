//
//  ProfileSetupItem.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 17/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

public struct ProfileSetupItem {
    var itemTitle: String
    let itemOptions: [String]
    let itemHeader: String
    var selectItem: String
}
