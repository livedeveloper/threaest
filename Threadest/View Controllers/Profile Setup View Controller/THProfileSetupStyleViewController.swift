//
//  THProfileSetupStyleViewController.swift
//  Threadest
//
//  Created by Jaydeep on 02/04/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit


class THProfileSetupStyleViewController: UIViewController {
    @IBOutlet weak var objCollectionView: UICollectionView!
    let styleImageArray = ["urban","casual","dapper","business","style_formal","athletic"]
    let styleNameArray = ["Urban","Casual","Dapper","Business","Formal","Athletic"]
    let selectedStyleArray: NSMutableArray = []
    override func viewDidLoad() {
        super.viewDidLoad()

        objCollectionView.register(UINib(nibName: THProfileSetupStyleCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: THProfileSetupStyleCollectionViewCell.className)
    }

    @IBAction func didTapComplete(_ sender: Any) {
        if selectedStyleArray.count != 0 {
            let tagList = selectedStyleArray.componentsJoined(by: ", ")
            if let currentUser = THUserManager.sharedInstance.currentUserdata() {
                GeneralMethods.sharedInstance.loadLoading(view: self.view)
                THUserManager.sharedInstance.performUpdateStylePreferenceTaglist(with: currentUser.userId, tagList: tagList, completion: { (error) in
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    self.dismiss(animated: true, completion: nil)
                })
            } else {
            self.dismiss(animated: true, completion: nil)
            }
        } else {
            self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "", body: "Please select at least one style", completion: { () in
            }), animated: true, completion: nil)
        }
    }
}

extension THProfileSetupStyleViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selectedStyleArray.contains(styleNameArray[indexPath.row]) {
            selectedStyleArray.remove(styleNameArray[indexPath.row])
        } else {
            selectedStyleArray.add(styleNameArray[indexPath.row])
        }
        objCollectionView.reloadData()
    }
}

extension THProfileSetupStyleViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return styleImageArray.count
    }

     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let styleCell = collectionView.dequeueReusableCell(withReuseIdentifier: THProfileSetupStyleCollectionViewCell.className, for: indexPath) as! THProfileSetupStyleCollectionViewCell
        styleCell.styleImage.image = UIImage(named: styleImageArray[indexPath.row])
        styleCell.styleSelectNameLabel.text = styleNameArray[indexPath.row]
        styleCell.styleUnselectNameLabel.text = styleNameArray[indexPath.row]
        if selectedStyleArray.contains(styleNameArray[indexPath.row]) {
            styleCell.styleUnselectNameLabel.isHidden = true
            styleCell.unSelectedTransperantview.isHidden = true
            styleCell.selectedView.isHidden = false
        }else {
            styleCell.styleUnselectNameLabel.isHidden = false
            styleCell.unSelectedTransperantview.isHidden = false
            styleCell.selectedView.isHidden = true
        }
        return styleCell
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{
        let nbCol = 2
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(nbCol - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(nbCol))

        return CGSize(width: size, height: Int(100*screenscale))
    }



}
