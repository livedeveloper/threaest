//
//  THProfileSetupTableViewCell.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 17/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THProfileSetupTableViewCell: UITableViewCell {
    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var selectItemLabel: UILabel!
    @IBOutlet weak var optionalLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if screenheight <= 568 {
            itemLabel.font = UIFont.AppFontRegular(12)
            selectItemLabel.font = UIFont.AppFontRegular(12)
            optionalLabel.font = UIFont.AppFontRegular(12)
        }
    }
    
    
    func configure(with profileSetupItem: ProfileSetupItem) {
        if profileSetupItem.selectItem == "" {
        itemLabel.text = profileSetupItem.itemTitle
        selectItemLabel.text = ""
        } else {
            itemLabel.text = "\(profileSetupItem.itemTitle)"
            selectItemLabel.text = "\(profileSetupItem.selectItem)"
        }
    }
}
