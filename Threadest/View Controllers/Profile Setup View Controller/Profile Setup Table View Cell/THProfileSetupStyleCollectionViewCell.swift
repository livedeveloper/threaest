//
//  THProfileSetupStyleCollectionViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 02/04/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THProfileSetupStyleCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var styleImage: UIImageView!
    @IBOutlet weak var styleUnselectNameLabel: UILabel!
    @IBOutlet weak var selectedView: UIView!
    @IBOutlet weak var unSelectedTransperantview: UIView!
    @IBOutlet weak var styleSelectNameLabel: UILabel!

}
