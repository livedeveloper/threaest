//
//  THShoppingProductCollectionViewCell.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 24/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SDWebImage

class THShoppingProductCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var productTitleLabel: UILabel!
    
    func configure(withProduct product: Product) {
        productTitleLabel.text = product.title
    }
}
