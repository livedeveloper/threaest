//
//  THShopBrandCollectionViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 12/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THShopBrandCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgBrand: UIImageView!

    @IBOutlet weak var lblBrandName: UILabel!
    
    
    func configure(withBrand brand: Brand) {
        // TODO: Add placeholder
        imgBrand.sd_setImage(with: URL(string: brand.logoURL), placeholderImage: nil, options: .retryFailed)
        lblBrandName.text = brand.name
    }
}
