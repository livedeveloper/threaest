//
//  THShopCollectionViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 11/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import CircleProgressView
class THShopCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgShopping: UIImageView!
    @IBOutlet weak var lblShopping: UILabel!
    
    @IBOutlet weak var circularView: CircleProgressView!
    

    func configure(withCategory category: ShopCategory) {
        // TODO: Add placeholder
        circularView.isHidden = false
        imgShopping.sd_setImage(with: URL(string: category.imageURL), placeholderImage: nil, options: .retryFailed, progress: { (block1, block2, imageurl) in
            let progress = Double(block1)/Double(block2)
            DispatchQueue.main.async {
                self.circularView.setProgress(progress, animated: true)
            }
        }) { (image, error, imageCacheType, imageUrl) in
            if image != nil {
                self.circularView.isHidden = true
                //for tag view
            }else {
                //image not found
            }
        }
        lblShopping.text = category.title
    }

}
