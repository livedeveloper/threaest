//
//  THFilterTrendCollectionViewCell.swift
//  Threadest
//
//  Created by Synnapps on 17/05/2017.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THFilterTrendCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var lblShopCategory: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    class func fromNib() -> THFilterTrendCollectionViewCell?
    {
        var cell: THFilterTrendCollectionViewCell?
        let nibViews = Bundle.main.loadNibNamed(THFilterTrendCollectionViewCell.className, owner: nil, options: nil)
        for nibView in nibViews! {
            if let cellView = nibView as? THFilterTrendCollectionViewCell {
                cell = cellView
            }
        }
        return cell
    }

}
