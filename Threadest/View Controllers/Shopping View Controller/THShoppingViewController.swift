//
//  THShoppingViewController.swift
//  Threadest
//
//  Created by Jaydeep on 10/03/17.
//  Copyright © 2017 JC. All rights reserved.


import UIKit
import NMRangeSlider
import ActionSheetPicker_3_0
import SwiftyJSON
import Pulsator


class THShoppingViewController: THBaseViewController, UIGestureRecognizerDelegate
{
    @IBOutlet weak var objCollectionView: UICollectionView!
    @IBOutlet weak var cartCountLabel: UILabel!
    @IBOutlet weak var addFriendCountLabel: UILabel!
    @IBOutlet weak var notificationCountLabel: UILabel!
    
    @IBOutlet weak var textFieldBrand: DesignableTextField!
    @IBOutlet weak var textFieldCategory: DesignableTextField!
    
     var refreshControl: UIRefreshControl!

    //picker view outlet
    @IBOutlet weak var ViewPicker: UIView!
    @IBOutlet weak var objPickerView: UIPickerView!
    
   // var shoppingcompletionHander : ((_ viewControllerName: String?)->())?
    
    //SearchBar header outlet
    @IBOutlet weak var searchBarView: UIView!
    weak var delegate: SearchBarDelegate?
    @IBOutlet weak var searchHeaderTextField: DesignableTextField!
    
     let searchTypeArray = ["All","User","Product", "Brand"]
    
    //variable for tabbar
    var lastContentOffset: CGFloat = 0
    var tabbarOffset: CGFloat!
    var searchQuery:THSearchQueryManager?
    var page:UInt = 0
    var selectedBrandIndex:Int = -1
    var searchedResuls:NSArray = []
    
    var headerOffset: CGFloat!
    var infuencerTimer: Timer!
    var influencerScore = 0
    var newInfluencerScore = 0
    var pulsator = Pulsator()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        setDoneOnKeyboard()
        self.tabBarController?.view.gestureRecognizers?[1].delegate = self
        self.tabBarController?.view.gestureRecognizers?[0].delegate = self
        tabbarOffset = screenheight - 49
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        objCollectionView.register(UINib(nibName: THShopCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: THShopCollectionViewCell.className)
        objCollectionView.register(UINib(nibName: THShopBrandCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: THShopBrandCollectionViewCell.className)
        objCollectionView.register(UINib(nibName: THShoppingProductCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: THShoppingProductCollectionViewCell.className)
        objCollectionView.register(UINib(nibName: THShopBrandHeaderCollectionReusableView.className, bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: THShopBrandHeaderCollectionReusableView.className)

        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(THShoppingViewController.refresh(sender:)), for: UIControlEvents.valueChanged)
        objCollectionView.addSubview(refreshControl)

        //Declare the view which is going to be added as header view
        let requiredView = UIView()

        //Add your required view as subview of uicollectionview backgroundView view like as
        objCollectionView.backgroundView = UIView()
        objCollectionView.backgroundView?.addSubview(requiredView)

        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        if THSocialFeedManager.sharedInstance.shopCategories.count == 0 {
            THSocialFeedManager.sharedInstance.shopCategories = [ShopCategory]()
            THSocialFeedManager.sharedInstance.page = 1
            THSocialFeedManager.sharedInstance.didLoadData = false
            THSocialFeedManager.sharedInstance.retrieveNextPosts(completion: { (error) in
                guard error != nil else {
                    self.objCollectionView.reloadData()
                    self.loadMoreShopFeed()
                    return
                }
                // TODO: Display load error
            })
        } else {
            self.objCollectionView.reloadData()
            self.loadMoreShopFeed()
        }
       

        initUI()
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateNotificationCount()
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.frame = CGRect(x: 0, y: tabbarOffset, width: screenwidth, height: 49)
        if let cartCount = UserDefaults.standard.value(forKey: "cartCount")
        {
            if (cartCount as! Int) == 0 {
                self.cartCountLabel.isHidden = true
            }
            else {
                self.cartCountLabel.isHidden = false
                self.cartCountLabel.text = "\(cartCount as! Int)"
            }
        } else {
            self.cartCountLabel.isHidden = true
        }
    }

    func loadMoreShopFeed() {
        objCollectionView.addInfiniteScrolling(actionHandler: {
            THSocialFeedManager.sharedInstance.retrieveNextPosts(completion: { (error) in
                
                self.objCollectionView.infiniteScrollingView.stopAnimating()
                //Just reload the data if there is no error
                if error == nil {
                    self.objCollectionView.reloadData()
                }
            })
        })
    }
    
    func setDoneOnKeyboard() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(THShoppingViewController.dismissKeyboard))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        self.searchHeaderTextField.inputAccessoryView = keyboardToolbar
    }
    
    override func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func refresh(sender:AnyObject) {
        THSocialFeedManager.sharedInstance.page = 1
        THSocialFeedManager.sharedInstance.didLoadData = true
        THSocialFeedManager.sharedInstance.pullToRefresh = true
        THSocialFeedManager.sharedInstance.retrieveNextPosts(completion: { (error) in
            self.refreshControl.endRefreshing()
            //Just reload the data if there is no error
            if error == nil {
                self.objCollectionView.reloadData()
            }
        })
    }

    private func initUI() {
        cartCountLabel.layer.cornerRadius = cartCountLabel.frame.width / 2.0
        cartCountLabel.layer.masksToBounds = true
        addFriendCountLabel.layer.cornerRadius = addFriendCountLabel.frame.width / 2.0
        addFriendCountLabel.layer.masksToBounds = true
        notificationCountLabel.layer.cornerRadius = notificationCountLabel.frame.width / 2.0
        notificationCountLabel.layer.masksToBounds = true
        self.searchQuery = THSearchQueryManager()
    }
    
    
    func doneButtonAction() {
        self.searchHeaderTextField.resignFirstResponder()
    }

    fileprivate func updateNotificationCount() {
        notificationCountLabel.isHidden = UIApplication.shared.applicationIconBadgeNumber == 0 ? true:false
        notificationCountLabel.text = "\(UIApplication.shared.applicationIconBadgeNumber)"
    }
    
    fileprivate func searchBarHideShow() {
        if searchBarView.isHidden {
            let searchPopup = THSearchResultAllViewController(nibName: THSearchResultAllViewController.className, bundle: nil)
            delegate = searchPopup
            searchHeaderTextField.delegate = searchPopup
            self.addChildViewController(searchPopup)
            searchPopup.view.frame = CGRect(x: 0, y: 64, width: screenwidth, height: screenheight-64)
            self.view.addSubview(searchPopup.view)
            searchBarView.alpha = 0
            searchBarView.isHidden = false
            UIView.animate(withDuration: 0.25, animations: {
                self.searchBarView.alpha = 1
            }, completion: { (completed) in
                
            })
        } else {
            self.searchHeaderTextField.text = ""
            self.view.endEditing(true)
            self.delegate?.didTapOnCloseButton(view: self.searchBarView)
           /* UIView.animate(withDuration: 0.25, animations: {
                self.searchBarView.alpha = 0
            }, completion: { (completed) in
                self.searchBarView.isHidden = true
            })*/
        }
    }


    @IBAction func didTapOnCartButton(_ sender: Any) {
        let shoppingcartController = THYourCartViewController(nibName: THYourCartViewController.className, bundle: nil)
        self.navigationController?.pushViewController(shoppingcartController, animated: true)
    }

    @IBAction func didTapOnAddFriendButton(_ sender: Any) {
        let findFriendConnectController = THFindFriendsConnectViewController(nibName: THFindFriendsConnectViewController.className, bundle: nil)
        let navFindFriendConnectController = UINavigationController(rootViewController: findFriendConnectController)
        self.present(navFindFriendConnectController, animated: true, completion: nil)
    }

    @IBAction func didTapOnNotificationButton(_ sender: Any) {
        let noficationController = THNotificationController(nibName: THNotificationController.className, bundle: nil)
        let navNotificationController = UINavigationController(rootViewController: noficationController)
        self.present(navNotificationController, animated: true, completion: nil)
    }

    @IBAction func didTapOnLeaderBoard(_ sender: Any) {
        let threadestBlackController = THBlackIntroViewController(nibName: THBlackIntroViewController.className, bundle: nil)

        let navigationController = UINavigationController(rootViewController: threadestBlackController)
        
        self.present(navigationController, animated: true, completion: {
        });
    }

    func preferredLayoutSizeFittingSize(targetSize:CGSize , label:UILabel) -> CGSize {
        
        // save original frame and preferredMaxLayoutWidth
        let originalFrame = label.frame
        let originalPreferredMaxLayoutWidth = label.preferredMaxLayoutWidth
        
        // assert: targetSize.width has the required width of the cell
        
        // step1: set the cell.frame to use that width
        var frame = label.frame
        frame.size = targetSize
        label.frame = frame
        
        // step2: layout the cell
        label.setNeedsLayout()
        label.layoutIfNeeded()
        label.preferredMaxLayoutWidth = label.bounds.size.width
        
        let computedSize = label.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        
        let newSize = CGSize(width:targetSize.width,height:computedSize.height)
        
        // restore old frame and preferredMaxLayoutWidth
        label.frame = originalFrame
        label.preferredMaxLayoutWidth = originalPreferredMaxLayoutWidth
        return newSize
    }
    
    
    @IBAction func didTapOnSearchHeader(_ sender: Any) {
        searchBarHideShow()
    }
    
    @IBAction func didTapOnSearchHideHeader(_ sender: Any) {
        searchBarHideShow()
    }
    
    
    @IBAction func didTapOnFilterButton(_ sender: Any) {
        delegate?.didTapOnFilterButton()
    }
    
    
}


extension THShoppingViewController:UICollectionViewDelegate{
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.lastContentOffset < scrollView.contentOffset.y && scrollView.contentOffset.y > 0 {
            if tabbarOffset < screenheight{
                self.tabbarOffset = self.tabbarOffset + 1.5
            } else if tabbarOffset == screenheight{
                self.tabbarOffset = screenheight
            } else if tabbarOffset == (screenheight - 49) {
                self.tabbarOffset = screenheight - 49
            }
            DispatchQueue.main.async {
                self.tabBarController?.tabBar.frame = CGRect(x: 0, y: self.tabbarOffset, width: screenwidth, height: 49)
            }
        } else if self.lastContentOffset > scrollView.contentOffset.y {
            if tabbarOffset == screenheight{
                self.tabbarOffset = self.tabbarOffset - 1.5
            }else if tabbarOffset > (screenheight - 49){
                self.tabbarOffset = self.tabbarOffset - 1.5
            } else if tabbarOffset == (screenheight - 49) {
                self.tabbarOffset = screenheight - 49
            } else if scrollView.contentOffset.y == 0 || scrollView.contentOffset.y < 0 {
                self.tabbarOffset = screenheight - 49
            }
            DispatchQueue.main.async {
                self.tabBarController?.tabBar.frame = CGRect(x: 0, y: self.tabbarOffset, width: screenwidth, height: 49)
            }
        }
        self.lastContentOffset = scrollView.contentOffset.y
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let shopFeedController = THShopThreadestBlackViewController(nibName: THShopThreadestBlackViewController.className, bundle: nil)
        shopFeedController.cartCategoryString = THSocialFeedManager.sharedInstance.shopCategories[indexPath.row].category
        shopFeedController.categoryID = THSocialFeedManager.sharedInstance.shopCategories[indexPath.row].id
        shopFeedController.shopTitle = THSocialFeedManager.sharedInstance.shopCategories[indexPath.row].title
        shopFeedController.navigationFrom = "shop"
        self.navigationController?.pushViewController(shopFeedController, animated: true)
    }
}

extension THShoppingViewController:UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
            return THSocialFeedManager.sharedInstance.shopCategories.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
            let categoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: THShopCollectionViewCell.className, for: indexPath) as! THShopCollectionViewCell
            categoryCell.configure(withCategory: THSocialFeedManager.sharedInstance.shopCategories[indexPath.row])
            // Shop category
            return categoryCell
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{
            return CGSize(width: screenwidth, height: screenwidth)
    }
}


protocol SearchBarDelegate: class {
    func didTapOnCloseButton(view: UIView)
    func didTapOnFilterButton()
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String)
    func textFieldDidBeginEdit(_ textField: UITextField)
    func showSearchBar(textField: UITextField)
}
