//
//  THNewExperiencePopupVC.swift
//  Threadest
//
//  Created by Jaydeep on 19/07/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THNewExperiencePopupVC: THBaseViewController {
    @IBOutlet weak var experiencePopup: UIView!
    open var newExperiencedCompletionHander : (()->())?
    override func viewDidLoad() {
        super.viewDidLoad()

        experiencePopup.layer.cornerRadius = 10
        experiencePopup.layer.masksToBounds = true
        showAnimate()
    }

    @IBAction func didTapOnClosetButton(_ sender: Any) {
        self.removeAnimate()
    }
    @IBAction func didTapOnNextButton(_ sender: Any) {
        self.removeAnimate()
        newExperiencedCompletionHander!()
    }
    
}
