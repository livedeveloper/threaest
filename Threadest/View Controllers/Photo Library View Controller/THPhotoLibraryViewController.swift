//
//  THPhotoLibraryViewController.swift
//  Threadest
//
//  Created by Jaydeep on 10/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THPhotoLibraryViewController: UIViewController {

    override func viewDidLoad() {
        let fusuma = FusumaViewController()
        fusuma.delegate = self
        fusuma.hasVideo = false // If you want to let the users allow to use video.
        self.configureChildViewController(childController: fusuma, onView: self.view)

    }
}

extension UIViewController {
    func configureChildViewController(childController: UIViewController, onView: UIView

        ) {
        let holderView = onView //?? self.view
        addChildViewController(childController)
        holderView.addSubview(childController.view)
        constrainViewEqual(holderView: holderView, view: childController.view)
        childController.didMove(toParentViewController: self)
    }


    func constrainViewEqual(holderView: UIView, view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        //pin 100 points from the top of the super
        let pinTop = NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal,
                                        toItem: holderView, attribute: .top, multiplier: 1.0, constant: 0)
        let pinBottom = NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal,
                                           toItem: holderView, attribute: .bottom, multiplier: 1.0, constant: 0)
        let pinLeft = NSLayoutConstraint(item: view, attribute: .left, relatedBy: .equal,
                                         toItem: holderView, attribute: .left, multiplier: 1.0, constant: 0)
        let pinRight = NSLayoutConstraint(item: view, attribute: .right, relatedBy: .equal,
                                          toItem: holderView, attribute: .right, multiplier: 1.0, constant: 0)

        holderView.addConstraints([pinTop, pinBottom, pinLeft, pinRight])
    }
}
extension THPhotoLibraryViewController:FusumaDelegate{


    // MARK: Fusuma Delegate

    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
        switch source {
        case .camera:
            print("Image captured from Camera")
        case .library:
            self.navigatToFilterScreen(selectedImage: image)
        default:
            print("Image selected")
        }

    }



    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        print("video completed and output to file: \(fileURL)")
    }

    func fusumaDismissedWithImage(_ image: UIImage, source: FusumaMode) {
        switch source {
        case .camera:
            print("Called just after dismissed FusumaViewController using Camera")
        case .library:
            print("Called just after dismissed FusumaViewController using Camera Roll")
        default:
            print("Called just after dismissed FusumaViewController")
        }
    }

    func navigatToFilterScreen(selectedImage:UIImage){
        let image = UIImage(cgImage: selectedImage.cgImage!, scale: 1.0, orientation:  .up)

        let tHCreateProductVC = THCreateProductVC(nibName: THCreateProductVC.className, bundle: nil)
        tHCreateProductVC.image = image
        let navigationController = UINavigationController(rootViewController: tHCreateProductVC)
        navigationController.isNavigationBarHidden = true

        DispatchQueue.main.async {
            self.present(navigationController, animated: true, completion: {
            });
        }
    }

    func fusumaCameraRollUnauthorized() {

        print("Camera roll unauthorized")

        let alert = UIAlertController(title: "Access Requested", message: "Saving image needs to access your photo album", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { (action) -> Void in

            if let url = URL(string:UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(url)
            }

        }))

        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in

        }))

        self.present(alert, animated: true, completion: nil)
    }

    func fusumaClosed() {
        print("Called when the FusumaViewController disappeared")
    }

    func fusumaWillClosed() {
        print("Called when the close button is pressed")
    }



}
