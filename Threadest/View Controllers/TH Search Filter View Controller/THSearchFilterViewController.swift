//
//  THSearchFilterViewController.swift
//  Threadest
//
//  Created by Synnapps on 21/05/2017.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import NMRangeSlider
import ActionSheetPicker_3_0

class THSearchFilterViewController: UIViewController {
    
    @IBOutlet weak var priceRangeSliderView: NMRangeSlider!
    @IBOutlet weak var priceRangeLabel: UILabel!
    @IBOutlet weak var trendsCollectionView: UICollectionView!
    @IBOutlet weak var searchFilterContainerView: UIView!
    
    @IBOutlet weak var textFieldGender: DesignableTextField!
    @IBOutlet weak var textFieldCategory: DesignableTextField!
    
    var filterCompletionHandler: ((_ closetCategory: String, _ minPrice: Int, _ maxPrice: Int, _ gender: String)->())?
    //temporary variable
    let genderArray = ["All","Male","Female"]
    let categoryArray = ["Accessories","shirts","Outerwear","shoes","watches","jewelry","sunglasses","briefs and boxers","ties","bags","t-shirts and sweatshirts","scarves and hats","jackets","trousers","belts","jeans","loungewear collection","leather goods","suits","knitwear","basic","bras","ea remix","briefs, panties and thongs","all collections","fine jewelry","key holders, bag charms & more","mon monogram","Pants/Shorts","sandals","scarves, shawls & more","boots & booties","fashion show selection","flats","fashion jewelry","hobos","leather bracelets","personalization","totes","Swimwear","pumps","books","mini bags","Formal wear","backpacks","discover the collection","ea beverly","hardsided luggage","technical cases","softsided luggage","ultimate luxury","clutches & evening","sneakers","timeless pieces","top handles","shoulder bags","travel accessories","van gogh","writing","cross body bags","fashion forward","fashion shows","neverfull alma & speedy","new this season","all masters","da vinci","icons","key & card holders","office essentials","rolling luggage","wallets","agendas & covers","fragonard","hotstamping","all handbags","home decor","rubens","timepieces","titian"]
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(THSearchFilterViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
        configurePriceSlider()
        trendsCollectionView.register(UINib(nibName: THFilterTrendCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: THFilterTrendCollectionViewCell.className)
        showAnimate()
    }
    
    override func dismissKeyboard() {
        self.view.superview?.superview?.endEditing(true)
    }

    func showAnimate() {
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
        });
    }
    
    func removeAnimate() {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    private func configurePriceSlider() {
        self.priceRangeSliderView.minimumValue = 0
        self.priceRangeSliderView.maximumValue = 5000
        self.priceRangeSliderView.lowerValue = 0
        self.priceRangeSliderView.upperValue = 5000
        self.priceRangeSliderView.minimumRange = 100
        self.priceRangeSliderView.stepValue = 100
    }
    
    // MARK: - Action Methods
    
    @IBAction func didChangeSliderValue(_ sender: Any) {
        self.priceRangeLabel.text = String(format: "Price ( $%i - $%i )", Int(self.priceRangeSliderView.lowerValue), Int(self.priceRangeSliderView.upperValue))
    }
    
    
    @IBAction func didTapOnCategory(_ sender: Any) {
        ActionSheetStringPicker.show(withTitle: "Select a Category", rows:categoryArray
            , initialSelection: 0, doneBlock: {
                picker, indexes, values in
                self.textFieldCategory.text = String(format: "%@",(values as! String?)!)
                return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
    }
    
    @IBAction func didTapOnSelectGender(_ sender: Any) {
        ActionSheetStringPicker.show(withTitle: "Select Gender", rows:genderArray
            , initialSelection: 0, doneBlock: {
                picker, indexes, values in
                self.textFieldGender.text = String(format: "%@",(values as! String?)!)
                return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
    }
    

    @IBAction func didTapOnCloseButton(_ sender: Any) {
        removeAnimate()
    }
    
    @IBAction func didTapFilterSearch(_ sender: Any) {
        removeAnimate()
        if textFieldGender.text == "All" {
            textFieldGender.text = ""
        }
        filterCompletionHandler!(textFieldCategory.text!, Int(self.priceRangeSliderView.lowerValue), Int(self.priceRangeSliderView.upperValue), textFieldGender.text!)
    }
}

extension THSearchFilterViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let shopFeedController = THShopThreadestBlackViewController(nibName: THShopThreadestBlackViewController.className, bundle: nil)
        shopFeedController.cartCategoryString = THSocialFeedManager.sharedInstance.shopCategories[indexPath.item].category
        shopFeedController.categoryID = THSocialFeedManager.sharedInstance.shopCategories[indexPath.row].id
        shopFeedController.shopTitle = THSocialFeedManager.sharedInstance.shopCategories[indexPath.row].title
        shopFeedController.navigationFrom = "shop"
        self.navigationController?.pushViewController(shopFeedController, animated: true)
    }
}

extension THSearchFilterViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1;
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return THSocialFeedManager.sharedInstance.shopCategories.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let trendCell = collectionView.dequeueReusableCell(withReuseIdentifier: THFilterTrendCollectionViewCell.className, for: indexPath) as! THFilterTrendCollectionViewCell
        trendCell.lblShopCategory.text = THSocialFeedManager.sharedInstance.shopCategories[indexPath.item].category.capitalized
        
        trendCell.lblShopCategory.layer.borderWidth = 1
        trendCell.lblShopCategory.layer.borderColor = UIColor.darkGray.cgColor
        // trendCell.lblShopCategory.backgroundColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0)
        return trendCell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{
        if let cell = THFilterTrendCollectionViewCell.fromNib() {
            cell.lblShopCategory.text = THSocialFeedManager.sharedInstance.shopCategories[indexPath.item].category.capitalized
            return cell.contentView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        }
        return CGSize.zero
    }
    
}


