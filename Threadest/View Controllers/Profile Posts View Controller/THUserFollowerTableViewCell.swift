//
//  THUserFollowerTableViewCell.swift
//  Threadest
//
//  Created by pratik on 23/05/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THUserFollowerTableViewCell: UITableViewCell {

    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var followUnfollowBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
