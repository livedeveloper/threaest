//
//  THUserFollowerViewController.swift
//  Threadest
//
//  Created by pratik on 23/05/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THUserFollowerViewController: UIViewController {

    @IBOutlet weak var objTableView: UITableView!

    @IBOutlet weak var lblNavTitle: UILabel!
    var profileType: String = "Followers"
    var userId:Int!
    var currentUserId: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let currentUser = THUserManager.sharedInstance.currentUserdata() {
            currentUserId = currentUser.userId
        }
        objTableView.register(UINib(nibName: THUserFollowerTableViewCell.className, bundle: nil), forCellReuseIdentifier: THUserFollowerTableViewCell.className)

        objTableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))

        if(self.profileType == "Following") {
            self.lblNavTitle.text = "Following"
        }
        else if(self.profileType == "Followers") {
            self.lblNavTitle.text = "Followers"
        }
        else if(self.profileType == "Brand") {
            self.lblNavTitle.text = "Brands"
        }
        else if(self.profileType == "Likes") {
            self.lblNavTitle.text = "Likes"
        }
        
        if(self.profileType == "Following") {
            self.callUserFollowing()
        }
        else if(self.profileType == "Followers") {
            self.callUserFollowers()
        }
        else if(self.profileType == "Brand") {
            self.callUserBrandFollowing()
        }
        loadMore()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        appdelegate.verticle.scrollEnabled(isEnable: false)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
//        appdelegate.verticle.scrollEnabled(isEnable: true)
    }
    
    func loadMore() {
        switch self.profileType {
        case "Brand":
            self.objTableView.addInfiniteScrolling(actionHandler: {
                THProfilePostsManager.sharedInstance.retrieveProfileBrandFollowing(with: self.userId) { (error) in
                    guard let error = error else {
                        self.objTableView.infiniteScrollingView.stopAnimating();
                        
                        self.objTableView.reloadData()
                        return
                    }
                    debugPrint(error)
                }
            })
        case "Followers":
           self.objTableView.addInfiniteScrolling(actionHandler: {
            THProfilePostsManager.sharedInstance.retrieveProfileFollowers(with:self.userId) { (error) in
                guard let error = error else {
                    self.objTableView.infiniteScrollingView.stopAnimating();
                    self.objTableView.reloadData()
                    return
                }
                debugPrint(error)
            }
            })
        case "Following":
           self.objTableView.addInfiniteScrolling(actionHandler: {
            THProfilePostsManager.sharedInstance.retrieveProfileFollowing(with:self.userId) { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                guard let error = error else {
                    self.objTableView.infiniteScrollingView.stopAnimating()
                    self.objTableView.reloadData()
                    return
                }
                debugPrint(error)
            }
            })
        default:
            break
        }
    }

    public func callUserFollowers() {
        THProfilePostsManager.sharedInstance.profileFollowers = [User]()
        THProfilePostsManager.sharedInstance.pageFollowers = 1
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THProfilePostsManager.sharedInstance.retrieveProfileFollowers(with:userId) { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            guard let error = error else {
                self.objTableView.reloadData()
                return
            }
            debugPrint(error)
        }
    }

    public func callUserFollowing() {
        THProfilePostsManager.sharedInstance.profileFollowing = [User]()
        THProfilePostsManager.sharedInstance.pageFollowing = 1
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THProfilePostsManager.sharedInstance.retrieveProfileFollowing(with:userId) { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            guard let error = error else {

                self.objTableView.reloadData()
                return
            }
            debugPrint(error)
        }
    }

    public func callUserBrandFollowing() {
        THProfilePostsManager.sharedInstance.profileBrandFollowing = [Brand]()
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THProfilePostsManager.sharedInstance.pageBrand = 1
        THProfilePostsManager.sharedInstance.retrieveProfileBrandFollowing(with: userId) { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            guard let error = error else {
                
                self.objTableView.reloadData()
                return
            }
            debugPrint(error)
        }
    }

    //MARK: Follow button Action
    @IBAction func didTapFollowButton(_ sender: UIButton) {
        
        if(self.profileType == "Following") {
           let objUser = THProfilePostsManager.sharedInstance.profileFollowing[sender.tag] as User
             if sender.titleLabel?.text == "UnFollow"{
                GeneralMethods.sharedInstance.loadLoading(view: self.view)
                THSocialInteractionPostsManager.sharedInstance.performUnfollowUser(for: objUser.userId, completion: { (error) in
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    THProfilePostsManager.sharedInstance.profileFollowing.remove(at: sender.tag)
                    self.objTableView.reloadData()
                })
            }
        }
        else if(self.profileType == "Followers"){
           var objUser = THProfilePostsManager.sharedInstance.profileFollowers[sender.tag] as User
            if sender.titleLabel?.text == "Follow" {
                GeneralMethods.sharedInstance.loadLoading(view: self.view)
                THSocialInteractionPostsManager.sharedInstance.performFollowUser(for:objUser.userId) { (error) in
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    objUser.isFollowing = true
                    THProfilePostsManager.sharedInstance.profileFollowers[sender.tag] = objUser
                    self.objTableView.reloadData()
                }
            }
            else if sender.titleLabel?.text == "UnFollow"{
                GeneralMethods.sharedInstance.loadLoading(view: self.view)
                THSocialInteractionPostsManager.sharedInstance.performUnfollowUser(for: objUser.userId, completion: { (error) in
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    THProfilePostsManager.sharedInstance.profileFollowers.remove(at: sender.tag)
                    self.objTableView.reloadData()
                })
            }
        } else if(self.profileType == "Likes") {
            var objUser = THSocialInteractionPostsManager.sharedInstance.likedPostPeopleList[sender.tag] as User
            if sender.titleLabel?.text == "Follow" {
                GeneralMethods.sharedInstance.loadLoading(view: self.view)
                THSocialInteractionPostsManager.sharedInstance.performFollowUser(for:objUser.userId) { (error) in
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    objUser.isFollowing = true
                    THSocialInteractionPostsManager.sharedInstance.likedPostPeopleList[sender.tag] = objUser
                    self.objTableView.reloadData()
                }
            }
            else if sender.titleLabel?.text == "UnFollow"{
                GeneralMethods.sharedInstance.loadLoading(view: self.view)
                THSocialInteractionPostsManager.sharedInstance.performUnfollowUser(for: objUser.userId, completion: { (error) in
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    objUser.isFollowing = false
                    THSocialInteractionPostsManager.sharedInstance.likedPostPeopleList[sender.tag] = objUser
                    self.objTableView.reloadData()
                })
            }
        }
        else {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            var objBrand = THProfilePostsManager.sharedInstance.profileBrandFollowing[sender.tag] as Brand
            if sender.titleLabel?.text == "Follow" {
                THSocialInteractionPostsManager.sharedInstance.performFollowBrand(for:objBrand.id) { (error) in
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    objBrand.isFollowing = true
                    THProfilePostsManager.sharedInstance.profileBrandFollowing[sender.tag] = objBrand
                    self.objTableView.reloadData()
                }
            }
            else {
                THSocialInteractionPostsManager.sharedInstance.performunFollowBrand(for:objBrand.id) { (error) in
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    objBrand.isFollowing = false
                    THProfilePostsManager.sharedInstance.profileBrandFollowing[sender.tag] = objBrand
                    self.objTableView.reloadData()
                }
            }
        }
    }
    
    @IBAction func didTapCloseButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension THUserFollowerViewController:UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 56
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if(self.profileType == "Following") {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            let objUser = THProfilePostsManager.sharedInstance.profileFollowing[indexPath.row] as User
            THProfilePostsManager.sharedInstance.profilePosts = [SocialInteraction]()
            THProfilePostsManager.sharedInstance.page = 1
            THProfilePostsManager.sharedInstance.didLoadData = false
            THProfilePostsManager.sharedInstance.retrieveProfilePosts(with: objUser.userId) { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                guard error != nil else {
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let userPostProfileVC = storyBoard.instantiateViewController(withIdentifier: PZProfileTableViewController.className) as! PZProfileTableViewController
                    userPostProfileVC.profile = THProfilePostsManager.sharedInstance.profile
                    userPostProfileVC.isSearch = true
                    userPostProfileVC.userId = objUser.userId
                    self.navigationController?.pushViewController(userPostProfileVC, animated: true)
                    return
                }
            }
        }
        else if(self.profileType == "Followers") {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            let objUser = THProfilePostsManager.sharedInstance.profileFollowers[indexPath.row] as User
            THProfilePostsManager.sharedInstance.profilePosts = [SocialInteraction]()
            THProfilePostsManager.sharedInstance.page = 1
            THProfilePostsManager.sharedInstance.didLoadData = false
            THProfilePostsManager.sharedInstance.retrieveProfilePosts(with: objUser.userId) { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                guard error != nil else {
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let userPostProfileVC = storyBoard.instantiateViewController(withIdentifier: PZProfileTableViewController.className) as! PZProfileTableViewController
                    userPostProfileVC.profile = THProfilePostsManager.sharedInstance.profile
                    userPostProfileVC.isSearch = true
                    userPostProfileVC.userId = objUser.userId
                 self.navigationController?.pushViewController(userPostProfileVC, animated: true)
                    return
                }
            }

        } else if(self.profileType == "Likes") {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            let objUser = THSocialInteractionPostsManager.sharedInstance.likedPostPeopleList[indexPath.row] as User
            THProfilePostsManager.sharedInstance.profilePosts = [SocialInteraction]()
            THProfilePostsManager.sharedInstance.page = 1
            THProfilePostsManager.sharedInstance.didLoadData = false
            THProfilePostsManager.sharedInstance.retrieveProfilePosts(with: objUser.userId) { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                guard error != nil else {
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let userPostProfileVC = storyBoard.instantiateViewController(withIdentifier: PZProfileTableViewController.className) as! PZProfileTableViewController
                    userPostProfileVC.profile = THProfilePostsManager.sharedInstance.profile
                    userPostProfileVC.isSearch = true
                    userPostProfileVC.userId = objUser.userId
                     self.navigationController?.pushViewController(userPostProfileVC, animated: true)
                    return
                }
            }
        }
         else {
            let objBrand = THProfilePostsManager.sharedInstance.profileBrandFollowing[indexPath.row] as Brand
            let brandProfileVC:THBrandProfileViewController = THBrandProfileViewController()
            brandProfileVC.isFrom = "brandlogo"
            brandProfileVC.brandId = objBrand.id
            self.navigationController?.pushViewController(brandProfileVC, animated: true)
        }
    }
}

extension THUserFollowerViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.profileType == "Following") {
             return THProfilePostsManager.sharedInstance.profileFollowing.count
        }
        else if(self.profileType == "Followers") {
            return THProfilePostsManager.sharedInstance.profileFollowers.count
        }
        else if(self.profileType == "Brand") {
            return THProfilePostsManager.sharedInstance.profileBrandFollowing.count
        } else if(self.profileType == "Likes") {
            return THSocialInteractionPostsManager.sharedInstance.likedPostPeopleList.count
        }
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: THUserFollowerTableViewCell.className, for: indexPath) as! THUserFollowerTableViewCell
        cell.followUnfollowBtn.layer.cornerRadius = 17

        if(self.profileType == "Following" || self.profileType == "Followers" || self.profileType == "Likes") {
            let followerUser : User
            if (self.profileType == "Followers") {
                followerUser = THProfilePostsManager.sharedInstance.profileFollowers[indexPath.row] as User
            } else if (self.profileType == "Likes") {
                followerUser = THSocialInteractionPostsManager.sharedInstance.likedPostPeopleList[indexPath.row] as User
            }
            else {
                followerUser = THProfilePostsManager.sharedInstance.profileFollowing[indexPath.row] as User
            }

            cell.lblUserName.text = followerUser.username

            cell.followUnfollowBtn.isHidden = followerUser.userId == currentUserId ? true: false
            if(followerUser.isFollowing) {
                cell.followUnfollowBtn.setTitle("UnFollow", for: .normal)
                cell.followUnfollowBtn.backgroundColor = UIColor(red:0.09, green:0.55, blue:0.80, alpha:1.0)
                cell.followUnfollowBtn.layer.borderWidth = 0
                cell.followUnfollowBtn.setTitleColor(UIColor.white, for: .normal)
            }
            else {
                cell.followUnfollowBtn.setTitle("Follow", for: .normal)
                cell.followUnfollowBtn.backgroundColor = UIColor.white
                cell.followUnfollowBtn.layer.borderWidth = 1
                cell.followUnfollowBtn.layer.borderColor = UIColor.black.cgColor
                cell.followUnfollowBtn.setTitleColor(UIColor.black, for: .normal)
            }
            
             cell.followUnfollowBtn.tag = indexPath.row
             cell.followUnfollowBtn.addTarget(self, action: #selector(THUserFollowerViewController.didTapFollowButton(_:)), for: .touchUpInside)
            
            cell.profileImage.sd_setImage(with: URL(string:followerUser.thumbnailImageURL), placeholderImage: nil, options: .retryFailed, progress: { (block1, block2, imageurl) in

            }) { (image, error, imageCacheType, imageUrl) in
                if image != nil {
                    // self.circularView.isHidden = true
                }else {
                    print("image not found")
                }
            }

        }
        else {
            let followingBrand : Brand
            followingBrand = THProfilePostsManager.sharedInstance.profileBrandFollowing[indexPath.row] as Brand

            cell.lblUserName.text = followingBrand.name

            if(followingBrand.isFollowing) {
                cell.followUnfollowBtn.setTitle("UnFollow", for: .normal)
                cell.followUnfollowBtn.backgroundColor = UIColor(red:0.09, green:0.55, blue:0.80, alpha:1.0)
                cell.followUnfollowBtn.layer.borderWidth = 0
                cell.followUnfollowBtn.setTitleColor(UIColor.white, for: .normal)
            }
            else {
                cell.followUnfollowBtn.setTitle("Follow", for: .normal)
                cell.followUnfollowBtn.backgroundColor = UIColor.white
                cell.followUnfollowBtn.layer.borderWidth = 1
                cell.followUnfollowBtn.layer.borderColor = UIColor.black.cgColor
                cell.followUnfollowBtn.setTitleColor(UIColor.black, for: .normal)
            }
            
            cell.followUnfollowBtn.tag = indexPath.row
            cell.followUnfollowBtn.addTarget(self, action: #selector(THUserFollowerViewController.didTapFollowButton(_:)), for: .touchUpInside)
            
            cell.profileImage.sd_setImage(with: URL(string:followingBrand.logoURL), placeholderImage: nil, options: .retryFailed, progress: { (block1, block2, imageurl) in

            }) { (image, error, imageCacheType, imageUrl) in
                if image != nil {
                    // self.circularView.isHidden = true
                }else {
                    print("image not found")
                }
            }
        }

        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
}
