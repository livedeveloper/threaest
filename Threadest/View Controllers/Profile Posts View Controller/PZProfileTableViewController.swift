//
//  PZProfileTableViewController.swift
//  Threadest
//
//  Created by Jaydeep on 06/01/18.
//  Copyright © 2018 JC. All rights reserved.
//

import UIKit
import SwiftyJSON
import FBSDKLoginKit
import FBSDKCoreKit
import Social
import Accounts
import SDWebImage
import IoniconsSwift
import Pulsator
import Crashlytics
import ARKit
import Lottie

class PZProfileTableViewController: UITableViewController, UIGestureRecognizerDelegate,UIImagePickerControllerDelegate,
UINavigationControllerDelegate {
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var gridButton: UIButton!
    @IBOutlet weak var listButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var objFavoriteCollectionView: UICollectionView!
    @IBOutlet weak var objScrollView: UIScrollView!
    @IBOutlet weak var singleTableView: UITableView!
    @IBOutlet weak var gridCollectionView: UICollectionView!
    @IBOutlet weak var btnProfileImage: UIButton!
    @IBOutlet weak var threadestScoreLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var followersCountLabel: UILabel!
    @IBOutlet weak var brandsCountLabel: UILabel!
    @IBOutlet weak var followingCountLabel: UILabel!
    @IBOutlet weak var btnFollowUnfollow: DesignableButton!

    @IBOutlet weak var webSiteLabel: UILabel!
    @IBOutlet weak var aboutYouLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!

    @IBOutlet var cameraButtonView: UIView!



    var isSearch: Bool = false
    var isPopup = true
    var findUser:SearchUser?
    var profile:Profile!
    var isFromNotification:Bool!
    var userId:Int!
    let picker = UIImagePickerController()
    var chosenImage : UIImage!
    var currentPage = 0

    var dataSource = DataSource()
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        singleTableView.delegate = dataSource
        singleTableView.dataSource = dataSource
        gridCollectionView.isScrollEnabled = false
        singleTableView.isScrollEnabled = false
        initUI()

        dataSource.singleTableView = self.singleTableView
        dataSource.profileDelegate = self
        dataSource.profileViewController = self
        self.automaticallyAdjustsScrollViewInsets = false
    }

    func initUI() {
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width / 2
        self.profileImageView.layer.borderWidth = 0.5
        self.profileImageView.layer.borderColor = UIColor.lightGray.cgColor
        self.profileImageView.layer.masksToBounds = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: Ionicons.close.image(20, color: UIColor.black), style: UIBarButtonItemStyle.plain, target: self, action: #selector(PZProfileTableViewController.didTapOnCloseButton(_:)))

        let listBlack = Ionicons.navicon.image(35, color: UIColor.black)
        let listBlue = Ionicons.navicon.image(35, color: UIColor(red:0.09, green:0.55, blue:0.80, alpha:1.0))

        let likeBlack = Ionicons.androidFavorite.image(30, color: .lightGray)//Ionicons.iosStarOutline.image(30, color: .hex("fcdf4e", alpha: 1))

        let likeRed = Ionicons.androidFavorite.image(30, color: .custColor(r: 233, g: 0, b: 55, a: 1))//Ionicons.star.image(30, color: .hex("fcdf4e", alpha: 1))

        self.likeButton.setImage(likeBlack, for: .normal)
        self.likeButton.setImage(likeRed, for: .selected)
        self.gridButton.setImage(UIImage(named: "cascanding_black"), for: .normal)
        self.gridButton.setImage(UIImage(named: "cascanding_blue"), for: .selected)
        self.listButton.setImage(listBlack, for: .normal)
        self.listButton.setImage(listBlue, for: .selected)
        self.gridButton.isSelected = true

        objFavoriteCollectionView.register(UINib(nibName: THSuggestionProductCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: THSuggestionProductCollectionViewCell.className)
        objFavoriteCollectionView.setCollectionViewLayout(CHTCollectionViewWaterfallLayout(), animated: false)

        gridCollectionView.setCollectionViewLayout(CHTCollectionViewWaterfallLayout(), animated: false)
        gridCollectionView.register(UINib(nibName: THProfilePostCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: THProfilePostCollectionViewCell.className)

        singleTableView.register(UINib(nibName: THShareDetailTableViewCell.className, bundle: nil), forCellReuseIdentifier: THShareDetailTableViewCell.className)
        singleTableView.register(UINib(nibName: THSocialProductBrandTagTableViewCell.className, bundle: nil), forCellReuseIdentifier: THSocialProductBrandTagTableViewCell.className)
        singleTableView.register(UINib(nibName: THSocialLikeCommentTableViewCell.className, bundle: nil), forCellReuseIdentifier: THSocialLikeCommentTableViewCell.className)

        if isSearch {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: Ionicons.close.image(20, color: UIColor.black), style: UIBarButtonItemStyle.plain, target: self, action: #selector(PZProfileTableViewController.didTapOnCloseButton(_:)))
            btnFollowUnfollow.isHidden = false
        }
        else {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: Ionicons.close.image(20, color: UIColor.black), style: UIBarButtonItemStyle.plain, target: self, action: #selector(PZProfileTableViewController.didTapOnCloseButton(_:)))
            btnFollowUnfollow.isHidden = false
        }
        if !isPopup {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: Ionicons.iosArrowBack.image(20, color: UIColor.black), style: UIBarButtonItemStyle.plain, target: self, action: #selector(PZProfileTableViewController.didTapOnCloseButton(_:)))
            btnFollowUnfollow.isHidden = false
        }

        if let usercurrent = THUserManager.sharedInstance.currentUserdata() , findUser == nil, profile == nil {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: Ionicons.logOut.image(20, color: UIColor.black), style: UIBarButtonItemStyle.plain, target: self, action: #selector(PZProfileTableViewController.didTapOnSettingButton(_:)))
            userId = usercurrent.userId
            dataSource.userId = userId
            self.btnProfileImage.isEnabled = true
            self.btnFollowUnfollow.setTitle("Edit profile",for: .normal)
            THProfilePostsManager.sharedInstance.profilePosts = [SocialInteraction]()
            THProfilePostsManager.sharedInstance.page = 1
            THProfilePostsManager.sharedInstance.didLoadData = false
            THProfilePostsManager.sharedInstance.retrieveProfilePosts(with: userId) { (error) in
                guard error != nil else {
                    self.retrievePostsData()
                    return
                }
            }
        } else {
            self.btnProfileImage.isEnabled = false
        }


        if(self.profile != nil) {
            THProfilePostsManager.sharedInstance.profile = self.profile
            self.title = profile.username
            usernameLabel.text = "@" + profile.username
            self.retrievePostsData()
        }
        else if(self.findUser != nil) {
            userId = Int(self.findUser!.objectID!)!
            dataSource.userId = userId
            THProfilePostsManager.sharedInstance.profilePosts = [SocialInteraction]()
            THProfilePostsManager.sharedInstance.page = 1
            THProfilePostsManager.sharedInstance.didLoadData = false
            THProfilePostsManager.sharedInstance.retrieveProfilePosts(with: Int(self.findUser!.objectID!)!) { (error) in
                guard error != nil else {
                    self.profile = THProfilePostsManager.sharedInstance.profile
                    self.title = self.profile.username
                    self.usernameLabel.text = "@" + self.profile.username
                    self.retrievePostsData()
                    return
                }
            }
        }

        THProfileClosetManager.sharedInstance.pageProfileFavorite = 1
        THProfileClosetManager.sharedInstance.profileFavorite = [ClosetCategory]()
        THProfileClosetManager.sharedInstance.didLoadProfileFavorite = false
        THProfileClosetManager.sharedInstance.retrieveProfileFavorite(with: userId, completion: { (error) in
            if(error==nil) {
                self.objFavoriteCollectionView.reloadData()
            }
        })
    }

    fileprivate func retrievePostsData() {
        let profil = THProfilePostsManager.sharedInstance.profile
        self.title = profil!.username
        self.usernameLabel.text = "@" + profil!.username
        self.threadestScoreLabel.text = "Threadsetter Score: \(profil!.currentUserInfluencerScore)"
        self.profileImage.sd_setImage(with: URL(string: (profil!.mediumImageUrl)))
        self.followersCountLabel.text = "\(profil!.followersCount)"
        self.brandsCountLabel.text = "\(profil!.brandsCount)"
        self.followingCountLabel.text = "\(profil!.followingsCount)"
        self.webSiteLabel.text = profil!.website
        self.aboutYouLabel.text = profil!.about
        if(profile != nil) {
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                if usercurrent.username == profile.username {
                    self.btnFollowUnfollow.setTitle("Edit profile",for: .normal)
                } else {
                    if profile.currentUserFollowing == true  {
                        btnFollowUnfollow.setTitle("UnFollow",for: .normal)
                    }
                    else {
                        btnFollowUnfollow.setTitle("Follow",for: .normal)
                    }
                }
            }
        }
        self.gridCollectionView.reloadData()
        self.tableView.reloadData()
    }

    override func viewDidLayoutSubviews() {
        self.objScrollView.contentSize = CGSize(width: screenwidth*3, height: self.objScrollView.frame.size.height)
    }

    override func viewWillAppear(_ animated: Bool) {
//        appdelegate.verticle.scrollEnabled(isEnable: false)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() , findUser == nil, profile == nil {
            webSiteLabel.text = usercurrent.website
            aboutYouLabel.text = usercurrent.about
            self.tableView.reloadData()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func viewDidAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .default
        self.objScrollView.contentSize = CGSize(width: screenwidth*3, height: self.objScrollView.frame.size.height)
    }

    override func viewDidDisappear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
//        appdelegate.verticle.scrollEnabled(isEnable: true)
        //self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }

    @IBAction func didTapOnGridButton(_ sender: Any) {
        objScrollView.scrollRectToVisible(CGRect(x: 0, y: 0, width: objScrollView.frame.size.width, height: objScrollView.frame.size.height), animated: true)
    }

    @IBAction func didTapOnListButton(_ sender: Any) {
        objScrollView.scrollRectToVisible(CGRect(x: screenwidth, y: 0, width: objScrollView.frame.size.width, height: objScrollView.frame.size.height), animated: true)
    }

    @IBAction func didTapOnLikeButton(_ sender: Any) {
        objScrollView.scrollRectToVisible(CGRect(x: screenwidth*2, y: 0, width: objScrollView.frame.size.width, height: objScrollView.frame.size.height), animated: true)
    }


    @IBAction func didTapFollowButton(_ sender: UIButton) {
        if sender.titleLabel?.text == "Follow" {
            self.btnFollowUnfollow.setTitle("UnFollow",for: .normal)
            THSocialInteractionPostsManager.sharedInstance.performFollowUser(for:userId) { (error) in
                guard let error = error else {
                    self.followersCountLabel.text = "\((Int(self.followersCountLabel.text!)! + 1))"
                    return
                }
                debugPrint(error)
            }
        }
        else if sender.titleLabel?.text == "UnFollow" {
            self.btnFollowUnfollow.setTitle("Follow",for: .normal)
            THSocialInteractionPostsManager.sharedInstance.performUnfollowUser(for: userId, completion: { (error) in
                guard let error = error else {
                    self.followersCountLabel.text = "\((Int(self.followersCountLabel.text!)! - 1))"
                    return
                }
                debugPrint(error)
            })
        }
        else if sender.titleLabel?.text == "Edit profile"{
            let editProfileVC = self.storyboard?.instantiateViewController(withIdentifier:THEditViewController.className) as! THEditViewController
            if let imageURL = THProfilePostsManager.sharedInstance.profile?.mediumImageUrl {
                editProfileVC.profileImageURL = imageURL
            }
             self.navigationController?.pushViewController(editProfileVC, animated: true)
               /* let editProfileVC = self.storyboard?.instantiateViewController(withIdentifier: PZEditProfileTableViewController.className) as! PZEditProfileTableViewController
            if let imageURL = THProfilePostsManager.sharedInstance.profile?.mediumImageUrl {
                editProfileVC.profileURL = imageURL
            }
            self.navigationController?.pushViewController(editProfileVC, animated: true)*/
        }
    }

    @IBAction func didTapFollowing(_ sender: Any) {
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }
        let followingController = THUserFollowerViewController(nibName: THUserFollowerViewController.className, bundle: nil)
        followingController.profileType = "Following"
        if(self.findUser != nil) {
            followingController.userId = Int(self.findUser!.objectID!)
        }
        else if(userId != nil) {
            followingController.userId = userId
        }
        else {
            followingController.userId = THUserManager.sharedInstance.currentUserdata()?.userId
        }
        self.navigationController?.pushViewController(followingController, animated: true)
    }


    @IBAction func didTapFollowers(_ sender: Any) {
        let followerController = THUserFollowerViewController(nibName: THUserFollowerViewController.className, bundle: nil)
        followerController.profileType = "Followers"
        if(self.findUser != nil) {
            followerController.userId = Int(self.findUser!.objectID!)
        }
        else if(userId != nil) {
            followerController.userId = userId
        }
        else {
            followerController.userId = THUserManager.sharedInstance.currentUserdata()?.userId
        }
        self.navigationController?.pushViewController(followerController, animated: true)
    }

    @IBAction func didTapBrandFollowing(_ sender: Any) {
        let brandFollowingController = THUserFollowerViewController(nibName: THUserFollowerViewController.className, bundle: nil)
        brandFollowingController.profileType = "Brand"
        if(self.findUser != nil) {
            brandFollowingController.userId = Int(self.findUser!.objectID!)
        }
        else if(userId != nil) {
            brandFollowingController.userId = userId
        }
        else {
            
            brandFollowingController.userId = THUserManager.sharedInstance.currentUserdata()?.userId
        }
        self.navigationController?.pushViewController(brandFollowingController, animated: true)
    }

    @IBAction func didTapOnBackButton(_ sender: Any) {
        UIApplication.shared.statusBarStyle = .lightContent
        self.navigationController?.dismiss(animated: true, completion: nil)
    }

    @IBAction func didTapOnCloseButton(_ sender: Any) {
        UIApplication.shared.statusBarStyle = .lightContent
        if isPopup {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
        
    }

    @IBAction func didTapOnSettingButton(_ sender: Any) {
        self.present(GeneralMethods.sharedInstance.AlertviewGeneral(title: "Logout", body: "Are you sure you would like to log out?", cancelbutton: "No", okbutton: "Yes", completion: { () in
            THUserManager.sharedInstance.logoutCurrentuser(isNavigateToLogin: false, completion: { (error) in
                if error != nil {
                    let alertController = UIAlertController(title: "Error", message: "An error occured. Try again later!", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: .destructive, handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                }
            })
        }), animated: false, completion: nil)
    }

    @IBAction func didTapProfileImage(_ sender: Any) {

        let actionSheetController = UIAlertController(title:nil, message:nil, preferredStyle: .actionSheet)

        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in

        }
        actionSheetController.addAction(cancelActionButton)

        let TakeActionButton = UIAlertAction(title: "Take from Camera", style: .default) { action -> Void in
            self.picker.allowsEditing = false
            self.picker.sourceType = .camera
            self.present( self.picker, animated: true, completion: nil)
            print("Camera")
        }
        actionSheetController.addAction(TakeActionButton)

        let saveActionButton = UIAlertAction(title: "Pick from Photos", style: .default) { action -> Void in
            self.picker.allowsEditing = false
            self.picker.sourceType = .photoLibrary
            self.self.present( self.picker, animated: true, completion: nil)
            print("Photos")
        }
        actionSheetController.addAction(saveActionButton)

        self.present(actionSheetController, animated: true, completion: nil)

    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.profileImage.image = chosenImage
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THNetworkManager.sharedInstance.updateUserImage(id: THUserManager.sharedInstance.currentUserdata()?.userId, image: chosenImage, completion: { (json, error) in
            guard let json = json else {
                self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "", body: (error?.localizedDescription)!, completion: {

                }), animated: true, completion: nil)
                return
            }
            DispatchQueue.main.async {
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            }
            if json["status"].boolValue {
                THUserManager.sharedInstance.saveCurrentUser(currentuser:json["data"])
                DispatchQueue.main.async {
                    /*THProfilePostsManager.sharedInstance.profilePosts.forEach{ anyPost in
                     var temp = anyPost
                     temp.ownerImageUrl = (THUserManager.sharedInstance.currentUserdata()?.thumbnailImageURL)!
                     }
                     //self.retrievePostsData()
                     self.objTableView.reloadData()*/
                    THProfilePostsManager.sharedInstance.retrieveProfilePosts(with: (THUserManager.sharedInstance.currentUserdata()?.userId)!) { (error) in
                        guard error != nil else {
                            self.retrievePostsData()
                            return
                        }
                    }

                    self.profileImage.image = info[UIImagePickerControllerOriginalImage] as? UIImage
                }
            } else {
                //                let error = NSError(domain: "Error", code: 0, userInfo: [NSLocalizedDescriptionKey : json["message"].stringValue])
            }
        }) { (progress) in
            print("progress \(progress)")
        }

        dismiss(animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 40
        } else {
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            return headerView
        } else {
            return nil
        }
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        cell.selectionStyle = .none
        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 {
            return self.tableView.frame.size.height
        } else {
            if indexPath.section == 0 {
                if indexPath.row == 0 {
                    return UITableViewAutomaticDimension
                }
            }
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }

    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 {
            return self.tableView.frame.size.height
        } else {
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
}

extension PZProfileTableViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == gridCollectionView {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THProfilePostsManager.sharedInstance.retrieveNotificationPost(with: THProfilePostsManager.sharedInstance.profilePosts[indexPath.row].id, completion: { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                if THProfilePostsManager.sharedInstance.notificationPost == nil {
                    return
                }
                guard error != nil else {
                    let commentController = THCommentViewController(nibName: THCommentViewController.className, bundle: nil)
                    commentController.socialInteraction = THProfilePostsManager.sharedInstance.notificationPost
                    self.navigationController?.pushViewController(commentController, animated: true)
                    return
                }
            })
        } else {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
           let closetCategory = THProfileClosetManager.sharedInstance.profileFavorite[indexPath.row]

            THBrandProductManager.sharedInstance.retrieveBrandProfile(with: closetCategory.brandId, productId: closetCategory.Id, geofence: true) { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                let shopshowcontroller = THShopshowViewController(nibName: THShopshowViewController.className, bundle: nil)
                shopshowcontroller.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                self.navigationController?.pushViewController(shopshowcontroller, animated: true)
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == self.gridCollectionView {
            if indexPath.item == THProfilePostsManager.sharedInstance.profilePosts.count-1 {
                if THProfilePostsManager.sharedInstance.didLoadData {
                    THProfilePostsManager.sharedInstance.didLoadData = false
                    THProfilePostsManager.sharedInstance.retrieveProfilePosts(with:userId) { (error) in
                        guard error != nil else {
                            self.gridCollectionView.reloadData()
                            return
                        }
                    }
                }
            }
        } else {
            if indexPath.item == THProfileClosetManager.sharedInstance.profileFavorite.count-1 {
                if THProfileClosetManager.sharedInstance.didLoadProfileFavorite {
                    THProfileClosetManager.sharedInstance.didLoadProfileFavorite = false
                    THProfileClosetManager.sharedInstance.retrieveProfileFavorite(with: userId, completion: { (error) in
                        guard error != nil else {
                            self.objFavoriteCollectionView.reloadData()
                            return
                        }
                    })
                }
            }
        }
    }
}

extension PZProfileTableViewController: profileTableDelegate {
    func scrollViewDidScrollProfile(_ scrollView: UIScrollView) {
        switch scrollView {
        case self.tableView:
            if scrollView.contentOffset.y > 330 {
                singleTableView.isScrollEnabled = true
            } else {
                singleTableView.isScrollEnabled = false
            }
            break
        case self.singleTableView:
            if scrollView.contentOffset.y <= 0 {
                singleTableView.isScrollEnabled = false
            }
        default:
            break
        }
    }
}


extension PZProfileTableViewController: UICollectionViewDataSource, CHTCollectionViewDelegateWaterfallLayout {
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        switch scrollView {
        case self.tableView:
            if scrollView.contentOffset.y > 330 {
                gridCollectionView.isScrollEnabled = true
                singleTableView.isScrollEnabled = true
                objFavoriteCollectionView.isScrollEnabled = true
            } else {
                gridCollectionView.isScrollEnabled = false
                singleTableView.isScrollEnabled = false
                objFavoriteCollectionView.isScrollEnabled = false
            }
            break
        case self.gridCollectionView:
            if scrollView.contentOffset.y <= 0 {
                gridCollectionView.isScrollEnabled = false
            }
            break
        case self.objFavoriteCollectionView:
            if scrollView.contentOffset.y <= 0 {
                objFavoriteCollectionView.isScrollEnabled = false
            }
            break
        case self.objScrollView:
            switch scrollView.contentOffset.x {
            case 0:
                gridButton.isSelected = true
                listButton.isSelected = false
                likeButton.isSelected = false
                gridCollectionView.reloadData()
                currentPage = 0
                break
            case screenwidth:
                gridButton.isSelected = false
                listButton.isSelected = true
                likeButton.isSelected = false
                singleTableView.reloadData()
                currentPage = 1
                break
            case screenwidth*2:
                gridButton.isSelected = false
                listButton.isSelected = false
                likeButton.isSelected = true
                objFavoriteCollectionView.reloadData()
                currentPage = 2
                break
            default:
                break
            }
        default:
            break
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == gridCollectionView {
            return THProfilePostsManager.sharedInstance.profilePosts.count
        } else {
            return THProfileClosetManager.sharedInstance.profileFavorite.count
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        if collectionView == gridCollectionView {
            let profilePostCell = collectionView.dequeueReusableCell(withReuseIdentifier: THProfilePostCollectionViewCell.className, for: indexPath) as! THProfilePostCollectionViewCell
            profilePostCell.configure(withPost: THProfilePostsManager.sharedInstance.profilePosts[indexPath.row])
            return profilePostCell
        } else {
            let suggestionCell = collectionView.dequeueReusableCell(withReuseIdentifier: THSuggestionProductCollectionViewCell.className, for: indexPath) as! THSuggestionProductCollectionViewCell
            suggestionCell.configure(closetCategory: THProfileClosetManager.sharedInstance.profileFavorite[indexPath.row])
            return suggestionCell
        }
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize{
        if collectionView == gridCollectionView {
            let nbCol = 2
            let flowLayout = collectionViewLayout as! CHTCollectionViewWaterfallLayout
            let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(nbCol - 1))
            let size = (collectionView.bounds.width - totalSpace) / CGFloat(nbCol)
            let imgHeight = CGFloat(THProfilePostsManager.sharedInstance.profilePosts[indexPath.row].largeImageHeight)
            let imgWidth = CGFloat(THProfilePostsManager.sharedInstance.profilePosts[indexPath.row].largeImageWidth)
            let newHeight = (imgHeight/imgWidth)*size
            return CGSize(width: size, height: newHeight)
        } else {
            let nbCol = 2
            let flowLayout = collectionViewLayout as! CHTCollectionViewWaterfallLayout
            let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(nbCol - 1))
            let size = (collectionView.bounds.width - totalSpace) / CGFloat(nbCol)
            let imgHeight = CGFloat(THProfileClosetManager.sharedInstance.profileFavorite[indexPath.row].defaultMediumImageHeight)
            let imgWidth = CGFloat(THProfileClosetManager.sharedInstance.profileFavorite[indexPath.row].defaultMediumImageWidth)
            let newHeight = (imgHeight/imgWidth)*size
            return CGSize(width: size, height: newHeight+63)
            /*let imgHeight = CGFloat(THProfileClosetManager.sharedInstance.profileFavorite[indexPath.row].defaultMediumImageHeight)
            let imgWidth = CGFloat(THProfileClosetManager.sharedInstance.profileFavorite[indexPath.row].defaultMediumImageWidth)
            let newHeight = (imgHeight/imgWidth)*gridWidth
            return CGSize(width: gridWidth, height: newHeight+63)*/
        }

    }
}

class DataSource: NSObject, UITableViewDelegate, UITableViewDataSource {
    var userId: Int = 0
    var singleTableView: UITableView = UITableView()
    var profileDelegate: profileTableDelegate?
    var profileViewController: PZProfileTableViewController?

   /* @IBAction func didTapOnLikeButton(_ sender: UIButton) {
        if !sender.isSelected {
            let cell = singleTableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! THSocialPostTableViewCell
            sender.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                sender.transform = CGAffineTransform(rotationAngle: CGFloat(-179.9).degreesToRadians)
            })
            UIView.animate(withDuration: 0.3, delay: 0.2, options: UIViewAnimationOptions(), animations: {
                sender.transform = CGAffineTransform(rotationAngle: CGFloat(1.1).degreesToRadians)
            }, completion: { (completed) in
                if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    THUserManager.sharedInstance.performUpvote(for: usercurrent.userId, votableId: THProfilePostsManager.sharedInstance.profilePosts[sender.tag].id, votableType: "Post", completion: { (vote, error) in
                        guard error != nil else {
                            if let i = THProfilePostsManager.sharedInstance.profilePosts.index(where: { $0.id == THProfilePostsManager.sharedInstance.profilePosts[sender.tag].id}){
                                THProfilePostsManager.sharedInstance.profilePosts[i].likedByCurrentUser = true
                                THProfilePostsManager.sharedInstance.profilePosts[i].upvotesCount = THProfilePostsManager.sharedInstance.profilePosts[i].upvotesCount + 1
                            }
                            THProfilePostsManager.sharedInstance.profilePosts[sender.tag].votes.append(vote!)

                            cell.upvoteCountLabel.text = "\(THProfilePostsManager.sharedInstance.profilePosts[sender.tag].upvotesCount)"
                            cell.upvoteCountLabel.isHidden = false
                            sender.isSelected  = true
                            return
                        }
                    })
                }
                else {

                }
            })
        }
    }

    @IBAction func didTapOnCommentButton(_ sender: UIButton) {
        let commentController = THCommentViewController(nibName: THCommentViewController.className, bundle: nil)
        commentController.socialInteraction = THProfilePostsManager.sharedInstance.profilePosts[sender.tag]
        profileViewController?.navigationController?.pushViewController(commentController, animated: true)
    }

    @IBAction func didTapOnRethreadButton(_ sender: UIButton) {
        guard let profileVC = profileViewController else {
            return
        }
        let cell = singleTableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! THSocialPostTableViewCell
        let socialInteraction:SocialInteraction = THProfilePostsManager.sharedInstance.profilePosts[sender.tag]
        if !cell.rethreadButton.isSelected {
            cell.rethreadButton.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                cell.rethreadButton.transform = CGAffineTransform(rotationAngle: CGFloat(-179.9).degreesToRadians)
            })
            UIView.animate(withDuration: 0.3, delay: 0.2, options: UIViewAnimationOptions(), animations: {
                cell.rethreadButton.transform = CGAffineTransform(rotationAngle: CGFloat(1.1).degreesToRadians)
            }, completion: { (completed) in
                let rethreadpopup = THRethreadPopupViewController(nibName: THRethreadPopupViewController.className, bundle: nil)
                rethreadpopup.productLinkString = "https://www.threadest.com/posts/\(socialInteraction.id)"
                appdelegate.window?.rootViewController?.addChildViewController(rethreadpopup)
                rethreadpopup.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
                appdelegate.window?.addSubview(rethreadpopup.view)
                rethreadpopup.didMove(toParentViewController: appdelegate.window?.rootViewController)
                rethreadpopup.completionHander = {selectedRethreadArray in
                    for selectedRethread in selectedRethreadArray! {
                        switch selectedRethread as! String {
                        case "Threadest":
                            self.rethreadPost(selectedRow: sender.tag)
                            break
                        case "Facebook":
                            self.rethreadPostOnFacebook(selectedRow: sender.tag)
                            break
                        case "Twitter":
                            self.rethreadPostOnTwitter(socialInteration:THProfilePostsManager.sharedInstance.profilePosts[sender.tag], comment: "")
                            break
                        default:
                            break
                        }
                    }
                }
            })
        } else {

        }
    }


    @IBAction func didTapOnDeletePostButton(_ sender: UIButton) {
        guard let profileVC = profileViewController else {
            return
        }
        let alert = UIAlertController(title: "Threadest", message: "Are you sure you want to remove post?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
            GeneralMethods.sharedInstance.loadLoading(view: profileVC.view)
            let socialInteraction = THProfilePostsManager.sharedInstance.profilePosts[sender.tag] as SocialInteraction
            THProfilePostsManager.sharedInstance.deletePost(with: Int(socialInteraction.id)) { (error) in
                guard error != nil else {
                    GeneralMethods.sharedInstance.dismissLoading(view: profileVC.view)
                    THProfilePostsManager.sharedInstance.profilePosts.remove(at: sender.tag)
                    self.singleTableView.reloadData()
                    //self.objCollectionview.reloadData()
                    return
                }
            }
        }))

        alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: { (action) -> Void in

        }))

        profileVC.present(alert, animated: true, completion: nil)
    }

    @IBAction func didTapOnPhotoTagButton(_ sender: UIButton) {
        guard let profileVC = profileViewController else {
            return
        }
        let socialInteraction = THProfilePostsManager.sharedInstance.profilePosts[sender.tag]
        let photoTag = socialInteraction.photoTags[Int("\(sender.accessibilityIdentifier!)")!]
        if photoTag.tagType == "Brand Tag"{
            let brandProfileVC:THBrandProfileViewController = THBrandProfileViewController()
            brandProfileVC.isFrom = "brandlogo"
            brandProfileVC.brandId = photoTag.brandId
            profileViewController?.navigationController?.pushViewController(brandProfileVC, animated: true)
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Tap on Brand Tag", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "brand_name":photoTag.brandName])
            }
        } else if photoTag.tagType == "Product Tag" {
            GeneralMethods.sharedInstance.loadLoading(view: profileVC.view)
            THBrandProductManager.sharedInstance.retrieveBrandProfileFromDots(with: photoTag.brandId, productId: photoTag.productId, sharePostCode: socialInteraction.ownerSharePostCode, postId: socialInteraction.id, completion: { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: profileVC.view)
                let shopshowcontroller = THShopshowViewController(nibName: THShopshowViewController.className, bundle: nil)
                shopshowcontroller.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                shopshowcontroller.postId = socialInteraction.id
                profileVC.navigationController?.pushViewController(shopshowcontroller, animated: true)

                if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    Answers.logCustomEvent(withName: "Tap on Product Tag", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "product_name":photoTag.productName])
                }

            })
        } else if photoTag.tagType == "Hash Tag" {
            let hashTagController = THHashTagViewController(nibName: THHashTagViewController.className, bundle: nil)
            hashTagController.hashTagString = photoTag.hashTag
            profileVC.navigationController?.pushViewController(hashTagController, animated: true)
        }
        /* GeneralMethods.sharedInstance.loadLoading(view: self.view)
         THBrandProductManager.sharedInstance.retrieveBrandProfile(with: photoTag.brandId, productId: photoTag.productId, geofence: false) { (error) in
         GeneralMethods.sharedInstance.dismissLoading(view: self.view)
         let shopshowcontroller = THShopshowViewController(nibName: THShopshowViewController.className, bundle: nil)
         shopshowcontroller.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
         self.navigationController?.pushViewController(shopshowcontroller, animated: true)
         }*/
    }*/

    // for Rethread post Threadest
    func rethreadPost(selectedRow: Int) {
        let socialInteraction:SocialInteraction = THProfilePostsManager.sharedInstance.profilePosts[selectedRow]

        THSocialInteractionPostsManager.sharedInstance.performRethreadPost(for: socialInteraction.id) { (error) in
            guard let error = error else {
                let socialCell = self.singleTableView.cellForRow(at: IndexPath(row: selectedRow, section: 0)) as! THShareDetailTableViewCell

                if let i = THProfilePostsManager.sharedInstance.profilePosts.index(where: { $0.id == THProfilePostsManager.sharedInstance.profilePosts[selectedRow].id}){
                    THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[i].rethreadCount = THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[i].rethreadCount + 1
                }
                socialCell.rethreadCountLabel.text = "\(socialInteraction.rethreadCount + 1)"
                socialCell.rethreadCountLabel.isHidden = false
                THProfilePostsManager.sharedInstance.profilePosts[selectedRow].rethreadCount = socialInteraction.rethreadCount + 1
                return
            }
            debugPrint(error)
        }
    }

    // for Rethread post on Facebook
    func rethreadPostOnFacebook(selectedRow: Int) {
        guard let profileVC = profileViewController else {
            return
        }
        let socialInteraction:SocialInteraction = THProfilePostsManager.sharedInstance.profilePosts[selectedRow]
        if FBSDKAccessToken.current() != nil {
            if FBSDKAccessToken.current().hasGranted("publish_actions") {
                GeneralMethods.sharedInstance.publishMessage(message: "https://www.threadest.com/posts/\(socialInteraction.id)", link: socialInteraction.largeImageUrl, completion: { () in
                    GeneralMethods.sharedInstance.RethreadPostSucessPopup(message: "Post to Facebook - Successful", viewController: profileVC)
                })
            } else {
                requestPublishPermissions(socialInteration:socialInteraction)
            }
        } else {
            requestPublishPermissions(socialInteration:socialInteraction)
        }
    }

    func requestPublishPermissions(socialInteration: SocialInteraction) {
        guard let profileVC = profileViewController else {
            return
        }
        let login: FBSDKLoginManager = FBSDKLoginManager()
        login.logIn(withPublishPermissions: ["publish_actions"], from: self.profileViewController) { (result, error) in
            if (error != nil) {
                print(error!)
            } else if (result?.isCancelled)! {
                print("Canceled")
            } else if (result?.grantedPermissions.contains("publish_actions"))! {
                print("permissions granted")
                GeneralMethods.sharedInstance.publishMessage(message: "https://www.threadest.com/posts/\(socialInteration.id)", link: socialInteration.largeImageUrl, completion: { () in
                    GeneralMethods.sharedInstance.RethreadPostSucessPopup(message: "Post to Facebook - Successful", viewController: profileVC)
                })
            }
        }
    }


    func rethreadPostOnTwitter(socialInteration: SocialInteraction, comment: String) {
        let accountStore = ACAccountStore()
        let accountType = accountStore.accountType(withAccountTypeIdentifier: ACAccountTypeIdentifierTwitter)

        accountStore.requestAccessToAccounts(with: accountType, options: nil) { (granted, error) in
            if granted, let accounts = accountStore.accounts(with: accountType) {
                // This will default to the first account if they have more than one
                if let account = accounts.first as? ACAccount {
                    let _ = SDWebImageManager.shared().imageDownloader?.downloadImage(with: URL(string: socialInteration.watermarkImageUrl), options: .allowInvalidSSLCertificates, progress: { (block1, block2, url) in
                    }, completed: { (image, data, error, bool) in
                        let requestURL = URL(string: "https://upload.twitter.com/1/statuses/update_with_media.json")
                        let parameters = ["status" : "https://www.threadest.com/posts/\(socialInteration.id) \n \(comment)"]
                        guard let request = SLRequest(forServiceType: SLServiceTypeTwitter, requestMethod: .POST, url: requestURL, parameters: parameters) else { return }
                        request.addMultipartData(UIImageJPEGRepresentation(image!, 1), withName: "media", type: nil, filename: nil)
                        request.account = account
                        request.perform(handler: { (data, response, error) in
                            if response?.statusCode == 200 {
                                DispatchQueue.main.async {
                                    GeneralMethods.sharedInstance.RethreadPostSucessPopup(message: "Post to Twitter - Successful", viewController: self.profileViewController!)
                                }
                            }
                            // Check to see if tweet was successful
                        })
                    })
                } else {
                    debugPrint("User does not have an available Twitter account")
                }
            }
        }
    }

    @IBAction func didTapOnCommentButton(_ sender: UIButton) {
        guard let profileVC = profileViewController else {
            return
        }
        let commentController = THCommentViewController(nibName: THCommentViewController.className, bundle: nil)
        commentController.socialInteraction = THProfilePostsManager.sharedInstance.profilePosts[sender.tag]
        profileVC.navigationController?.pushViewController(commentController, animated: true)
    }

    func didTapOnHashTagButton(_ sender: UITapGestureRecognizer) {
        guard let profileVC = profileViewController else {
            return
        }
        let socialInteraction = THProfilePostsManager.sharedInstance.profilePosts[(sender.view?.tag)!]
        var hashTags = ""
        for tags in socialInteraction.photoTags {
            if tags.tagType == "Hash Tag" {
                hashTags = hashTags + tags.hashTag
            }
        }
        let hashTagController = THHashTagViewController(nibName: THHashTagViewController.className, bundle: nil)
        hashTagController.hashTagString = hashTags
        profileVC.navigationController?.pushViewController(hashTagController, animated: true)
    }

    @IBAction func didTapOnPhotoTagButton(_ sender: UIButton) {
        guard let profileVC = profileViewController else {
            return
        }
        let socialInteraction = THProfilePostsManager.sharedInstance.profilePosts[sender.tag]
        let photoTag = socialInteraction.photoTags[Int("\(sender.accessibilityIdentifier!)")!]
        if photoTag.tagType == "Brand Tag"{
            let brandProfileVC:THBrandProfileViewController = THBrandProfileViewController()
            brandProfileVC.isFrom = "brandlogo"
            brandProfileVC.brandId = photoTag.brandId
            profileVC.navigationController?.pushViewController(brandProfileVC, animated: true)
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Tap on Brand Tag", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "brand_name":photoTag.brandName])
            }
        } else if photoTag.tagType == "Product Tag" {
            GeneralMethods.sharedInstance.loadLoading(view: profileVC.view)
            THBrandProductManager.sharedInstance.retrieveBrandProfileFromDots(with: photoTag.brandId, productId: photoTag.productId, sharePostCode: socialInteraction.ownerSharePostCode, postId: socialInteraction.id, completion: { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: profileVC.view)
                let threeDAvailable = THBrandProductManager.sharedInstance.brandProduct?.productGalleryImages.contains(where: { (imgGallery) -> Bool in
                    if imgGallery.image_type == "3D" {
                        return true
                    } else {
                        return false
                    }
                })
                
                if threeDAvailable! {
                    if #available(iOS 11.0, *) {
                        if (ARConfiguration.isSupported) {
                            let ARShopShowController = THARShopShowViewController(nibName: THARShopShowViewController.className, bundle: nil)
                            ARShopShowController.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                            ARShopShowController.postId = socialInteraction.id
                            profileVC.navigationController?.pushViewController(ARShopShowController, animated: true)
                            return
                        }
                    }
                }
                let shopshowcontroller = THShopshowViewController(nibName: THShopshowViewController.className, bundle: nil)
                shopshowcontroller.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                shopshowcontroller.postId = socialInteraction.id
                profileVC.navigationController?.pushViewController(shopshowcontroller, animated: true)
                
                

                if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    Answers.logCustomEvent(withName: "Tap on Product Tag", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "product_name":photoTag.productName])
                }

            })
        } else if photoTag.tagType == "Hash Tag" {
            let hashTagController = THHashTagViewController(nibName: THHashTagViewController.className, bundle: nil)
            hashTagController.hashTagString = photoTag.hashTag
            profileVC.navigationController?.pushViewController(hashTagController, animated: true)
        }
    }

    @IBAction func didTapOnRethreadButton(_ sender: UIButton) {
        guard let profileVC = profileViewController else {
            return
        }
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: profileVC)
                return
            }
        }
        let cell = self.singleTableView.cellForRow(at: IndexPath(row: 1, section:sender.tag)) as! THSocialLikeCommentTableViewCell
        let socialInteraction:SocialInteraction = THProfilePostsManager.sharedInstance.profilePosts[sender.tag]
        if !cell.shareButton.isSelected {
            let rethreadpopup = THRethreadPopupViewController(nibName: THRethreadPopupViewController.className, bundle: nil)
            rethreadpopup.productLinkString = "https://www.threadest.com/posts/\(socialInteraction.id)"
            appdelegate.window?.rootViewController?.addChildViewController(rethreadpopup)
            rethreadpopup.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
            appdelegate.window?.addSubview(rethreadpopup.view)
            rethreadpopup.didMove(toParentViewController: appdelegate.window?.rootViewController)
            
            rethreadpopup.completionHander = {selectedRethreadArray in
                for selectedRethread in selectedRethreadArray! {
                    switch selectedRethread as! String {
                    case "Threadest":
                        self.rethreadPost(selectedRow: sender.tag)
                        break
                    case "Facebook":
                        self.rethreadPostOnFacebook(selectedRow: sender.tag)
                        break
                    case "Twitter":
                        self.rethreadPostOnTwitter(socialInteration:THProfilePostsManager.sharedInstance.profilePosts[sender.tag], comment: "")
                        break
                    default:
                        break
                    }
                }
            }
           /* cell.shareButton.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                cell.shareButton.transform = CGAffineTransform(rotationAngle: CGFloat(-179.9).degreesToRadians)
            })
            UIView.animate(withDuration: 0.3, delay: 0.2, options: UIViewAnimationOptions(), animations: {
                cell.shareButton.transform = CGAffineTransform(rotationAngle: CGFloat(1.1).degreesToRadians)
            }, completion: { (completed) in
            })*/
        }
    }

    @IBAction func didTapOnLikeButton(_ sender: UIButton) {
        guard let profileVC = self.profileViewController else {
            return
        }
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: profileVC)
                return
            }
        }
        if !sender.isSelected {
            sender.isSelected  = true
            if let cell = singleTableView.cellForRow(at: IndexPath(row: 0 , section: sender.tag)) as? THShareDetailTableViewCell {
                let animationView = LOTAnimationView(name: "like")
                animationView.animationProgress = 100
                animationView.center = cell.socialPostImage.center
                animationView.frame = cell.socialPostImage.frame
                animationView.backgroundColor = UIColor.clear
                animationView.contentMode = .scaleAspectFill
                animationView.animationSpeed = 2
                cell.socialPostImage.addSubview(animationView)
                animationView.loopAnimation = false
                animationView.play { (sucess) in
                    animationView.pause()
                    animationView.removeFromSuperview()
                }
            }
            
            let likeCell = singleTableView.cellForRow(at: IndexPath(row: 1 , section: sender.tag)) as? THSocialLikeCommentTableViewCell
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                THUserManager.sharedInstance.performUpvote(for: usercurrent.userId, votableId: THProfilePostsManager.sharedInstance.profilePosts[sender.tag].id, votableType: "Post", completion: { (vote, error) in
                    guard let _ = error else {
                        if let i = THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts.index(where: { $0.id == THProfilePostsManager.sharedInstance.profilePosts[sender.tag].id}){
                            THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[i].likedByCurrentUser = true
                            THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[i].upvotesCount = THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[i].upvotesCount + 1
                        }
                        THProfilePostsManager.sharedInstance.profilePosts[sender.tag].likedByCurrentUser = true
                        THProfilePostsManager.sharedInstance.profilePosts[sender.tag].upvotesCount = THProfilePostsManager.sharedInstance.profilePosts[sender.tag].upvotesCount + 1
                        THProfilePostsManager.sharedInstance.profilePosts[sender.tag].votes.append(vote!)
                        
                        likeCell?.likeLabel.text = THProfilePostsManager.sharedInstance.profilePosts[sender.tag].upvotesCount == 0 ? "": THProfilePostsManager.sharedInstance.profilePosts[sender.tag].upvotesCount == 1 ? "\(THProfilePostsManager.sharedInstance.profilePosts[sender.tag].upvotesCount) Like" : "\(THProfilePostsManager.sharedInstance.profilePosts[sender.tag].upvotesCount) Likes"
                        
                        likeCell?.likeLabel.isHidden = false
                        //Add funnel for Like post
                        Answers.logCustomEvent(withName: "Like post", customAttributes: ["userid":usercurrent.userId,"username": usercurrent.username, "post_Id":"\(THProfilePostsManager.sharedInstance.profilePosts[sender.tag].id)"])
                        return
                    }
                })
            }
        }
    }

    @IBAction func didTapOnLikescountButton(_ sender: UIButton) {
        guard let profileVC = self.profileViewController else {
            return
        }
        GeneralMethods.sharedInstance.loadLoading(view: profileVC.view)
        let socialInteraction = THProfilePostsManager.sharedInstance.profilePosts[sender.tag]
        THSocialInteractionPostsManager.sharedInstance.retrieveLikedPostUserList(for: socialInteraction.id) { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: profileVC.view)
            guard let _ = error else {
                let followingController = THUserFollowerViewController(nibName: THUserFollowerViewController.className, bundle: nil)
                followingController.profileType = "Likes"
                profileVC.navigationController?.pushViewController(followingController, animated: true)
                return
            }

        }
    }

    @IBAction func didTapOnProfileUserNameButton(_ sender: UIButton) {
        guard let profileVC = self.profileViewController else {
            return
        }
        GeneralMethods.sharedInstance.loadLoading(view: profileVC.view)
        let socialInteraction = THProfilePostsManager.sharedInstance.profilePosts[sender.tag]
        THProfilePostsManager.sharedInstance.profilePosts = [SocialInteraction]()
        THProfilePostsManager.sharedInstance.page = 1
        THProfilePostsManager.sharedInstance.didLoadData = false
        THProfilePostsManager.sharedInstance.retrieveProfilePosts(with: Int(socialInteraction.userId)) { (error) in
            guard error != nil else {
                GeneralMethods.sharedInstance.dismissLoading(view: profileVC.view)
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let userPostProfileVC = storyBoard.instantiateViewController(withIdentifier: PZProfileTableViewController.className) as! PZProfileTableViewController
                userPostProfileVC.profile = THProfilePostsManager.sharedInstance.profile
                userPostProfileVC.isSearch = true
                userPostProfileVC.userId = socialInteraction.userId
                profileVC.navigationController?.pushViewController(userPostProfileVC, animated: true)
                return
            }
        }
    }

    @IBAction func didTapOnDeletePostButton(_ sender: UIButton) {
        guard let profileVC = self.profileViewController else {
            return
        }
        profileVC.present(GeneralMethods.sharedInstance.AlertviewGeneral(title: "Threadest", body: "Are you sure you want to remove post?", cancelbutton: "NO", okbutton: "Yes", completion: {
            GeneralMethods.sharedInstance.loadLoading(view: profileVC.view)
            let socialInteraction = THProfilePostsManager.sharedInstance.profilePosts[sender.tag] as SocialInteraction
            THProfilePostsManager.sharedInstance.deletePost(with: Int(socialInteraction.id)) { (error) in
                guard error != nil else {
                    GeneralMethods.sharedInstance.dismissLoading(view: profileVC.view)
                    THProfilePostsManager.sharedInstance.profilePosts.remove(at: sender.tag)
                    self.singleTableView.reloadData()
                    return
                }
            }
        }), animated: true, completion: nil)
    }

    func pinchZoomSocialPostImage(sender: UIPinchGestureRecognizer) {
        let tapLocation = sender.location(in: self.singleTableView)
        if let indexPath : IndexPath = self.singleTableView.indexPathForRow(at: tapLocation) {
            if let socialCell = self.singleTableView.cellForRow(at: indexPath) as? THShareDetailTableViewCell {
                TMImageZoom.shared().gestureStateChanged(sender, withZoom: socialCell.socialPostImage)
            } else {
                TMImageZoom.shared().resetImageZoom()
            }

        } else {
            TMImageZoom.shared().resetImageZoom()
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let shareDetailCell = tableView.cellForRow(at: indexPath) as! THShareDetailTableViewCell
            self.didTapOnLikeButton(shareDetailCell.likeButton)
        case 1:
            break
        default:
            let btnTag = UIButton()
            btnTag.tag = indexPath.section
            btnTag.accessibilityIdentifier = String(format: "%d", indexPath.row-2)
            self.didTapOnPhotoTagButton(btnTag)
            break
        }
    }


    func numberOfSections(in tableView: UITableView) -> Int{
        return THProfilePostsManager.sharedInstance.profilePosts.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return THProfilePostsManager.sharedInstance.profilePosts[section].photoTags.count + 2
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
            case 0:
                let shareDetailCell = tableView.dequeueReusableCell(withIdentifier: THShareDetailTableViewCell.className, for: indexPath) as! THShareDetailTableViewCell
                let socialInteraction = THProfilePostsManager.sharedInstance.profilePosts[indexPath.section] as SocialInteraction
                if let currentUser = THUserManager.sharedInstance.currentUserdata() {
                    if socialInteraction.userId == currentUser.userId {
                        shareDetailCell.trashButton.isHidden = false
                    } else {
                        shareDetailCell.trashButton.isHidden = true
                    }
                }
                
                shareDetailCell.selectionStyle = UITableViewCellSelectionStyle.none
                //shareDetailCell.likeButton.tag = indexPath.section
                //shareDetailCell.commentButton.tag = indexPath.section
                //shareDetailCell.profile_nameButton.tag = indexPath.section
                //shareDetailCell.commentButton.addTarget(self, action: #selector(DataSource.didTapOnCommentButton(_:)), for: UIControlEvents.touchUpInside)
                //shareDetailCell.rethreadButton.tag = indexPath.section
                //shareDetailCell.rethreadButton.addTarget(self, action: #selector(DataSource.didTapOnRethreadButton(_:)), for: .touchUpInside)
                
                //shareDetailCell.likeButton.tag = indexPath.section
                //shareDetailCell.likeButton.addTarget(self, action: #selector(DataSource.didTapOnLikeButton(_:)), for: .touchUpInside)
                shareDetailCell.configure(withSocialInteraction: socialInteraction)
                shareDetailCell.likeButton.isHidden = true
                shareDetailCell.commentButton.isHidden = true
                shareDetailCell.rethreadButton.isHidden = true
                shareDetailCell.profile_nameButton.isHidden = true
                shareDetailCell.likeCountButton.isHidden = true
                shareDetailCell.commentLabel.isHidden = true
                shareDetailCell.commentCountLabel.isHidden = true
                shareDetailCell.profileImage.isHidden = true
                shareDetailCell.rethreadCountLabel.isHidden = true
                shareDetailCell.upvoteCountLabel.isHidden = true
                
                shareDetailCell.trashButton.tag = indexPath.section
                shareDetailCell.trashButton.addTarget(self, action: #selector(DataSource.didTapOnDeletePostButton(_:)), for: UIControlEvents.touchUpInside)
                
                //shareDetailCell.profile_nameButton.addTarget(self, action: #selector(DataSource.didTapOnProfileUserNameButton(_:)), for: UIControlEvents.touchUpInside)
                
                //shareDetailCell.likeCountButton.tag = indexPath.section
                //shareDetailCell.likeCountButton.addTarget(self, action: #selector(DataSource.didTapOnLikescountButton(_:)), for: .touchUpInside)
                
                shareDetailCell.socialPostImage.subviews.forEach({ (button) in
                    button.removeFromSuperview()
                })
                
                let pinchZoomSocilPostImage = UIPinchGestureRecognizer.init(target: self, action: #selector(DataSource.pinchZoomSocialPostImage(sender:)))
                shareDetailCell.socialPostImage.addGestureRecognizer(pinchZoomSocilPostImage)
                shareDetailCell.circularProgressView.isHidden = false
                shareDetailCell.socialPostImage.sd_setImage(with: URL(string: socialInteraction.largeImageUrl), placeholderImage: nil, options: .retryFailed, progress: { (block1, block2, imageurl) in
                    let progress = Double(block1)/Double(block2)
                    DispatchQueue.main.async {
                        shareDetailCell.circularProgressView.setProgress(progress, animated: true)
                    }
                }) { (image, error, imageCacheType, imageUrl) in
                    if image != nil {
                        shareDetailCell.circularProgressView.isHidden = true
                        //for tag view
                        if socialInteraction.photoTags.count > 0 {
                            var i = 0
                            for tags in socialInteraction.photoTags.filter({$0.tagType == "Product Tag" || $0.tagType == "Brand Tag"}) {
                                let imgHeight = CGFloat(socialInteraction.largeImageHeight)
                                let imgWidth = CGFloat(socialInteraction.largeImageWidth)
                                let height = (imgHeight/imgWidth) * screenwidth
                                if (height-30) >= CGFloat(tags.y) {
                                    shareDetailCell.tagButton = UIButton(frame: CGRect(x: tags.x, y: tags.y, width: 30, height: 30))
                                } else {
                                    shareDetailCell.tagButton = UIButton(frame: CGRect(x: tags.x, y: tags.y-20, width: 30, height: 30))
                                }
                                shareDetailCell.tagButton.isUserInteractionEnabled = true
                                shareDetailCell.tagButton.backgroundColor = UIColor.white
                                shareDetailCell.tagButton.layer.borderColor = UIColor.white.cgColor
                                shareDetailCell.tagButton.layer.borderWidth = 1
                                shareDetailCell.tagButton.layer.cornerRadius = shareDetailCell.tagButton.frame.size.width / 2
                                shareDetailCell.tagButton.accessibilityIdentifier = String(format: "%d", i)
                                shareDetailCell.tagButton.layer.masksToBounds = true
                                shareDetailCell.tagButton.tag = indexPath.section
                                if tags.tagType == "Product Tag" {
                                    shareDetailCell.tagButton.sd_setImage(with: URL(string: tags.productImage), for: .normal, placeholderImage: nil, options: .retryFailed, completed: nil)
                                    
                                } else {
                                    shareDetailCell.tagButton.sd_setImage(with: URL(string: tags.brandLogo), for: .normal, placeholderImage: nil, options: .retryFailed, completed: nil)
                                }
                                shareDetailCell.tagButton.imageView?.contentMode = .scaleAspectFit
                                shareDetailCell.tagButton.addTarget(self, action: #selector(DataSource.didTapOnPhotoTagButton(_:)), for: .touchUpInside)
                                shareDetailCell.socialPostImage.addSubview(shareDetailCell.tagButton)
                                i = i+1
                            }
                        }
                    }
                }
                return shareDetailCell
            case 1:
                let socialLikeCommentCell = tableView.dequeueReusableCell(withIdentifier: THSocialLikeCommentTableViewCell.className, for: indexPath) as! THSocialLikeCommentTableViewCell
                socialLikeCommentCell.selectionStyle = UITableViewCellSelectionStyle.none
                let socialInteraction = THProfilePostsManager.sharedInstance.profilePosts[indexPath.section] as SocialInteraction
                socialLikeCommentCell.configureSocial(socialInteraction: socialInteraction)
                
                socialLikeCommentCell.likeButton.tag = indexPath.section
                socialLikeCommentCell.commentButton.tag = indexPath.section
                
                socialLikeCommentCell.commentButton.addTarget(self, action: #selector(DataSource.didTapOnCommentButton(_:)), for: UIControlEvents.touchUpInside)
                socialLikeCommentCell.shareButton.tag = indexPath.section
                socialLikeCommentCell.shareButton.addTarget(self, action: #selector(DataSource.didTapOnRethreadButton(_:)), for: .touchUpInside)
                
                socialLikeCommentCell.likeButton.tag = indexPath.section
                socialLikeCommentCell.likeButton.addTarget(self, action: #selector(DataSource.didTapOnLikeButton(_:)), for: .touchUpInside)
                
                socialLikeCommentCell.profile_nameButton.tag = indexPath.section 
                socialLikeCommentCell.profile_nameButton.addTarget(self, action: #selector(DataSource.didTapOnProfileUserNameButton(_:)), for: UIControlEvents.touchUpInside)
                return socialLikeCommentCell
            default:
                let socialTagCell = tableView.dequeueReusableCell(withIdentifier: THSocialProductBrandTagTableViewCell.className, for: indexPath) as! THSocialProductBrandTagTableViewCell
                socialTagCell.selectionStyle = .none
                socialTagCell.configure(photosTags: THProfilePostsManager.sharedInstance.profilePosts[indexPath.section].photoTags[indexPath.row-2])
                return socialTagCell
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            break
        default:
            switch indexPath.row {
                case 0:
                    let shareDetailCell = tableView.dequeueReusableCell(withIdentifier: THShareDetailTableViewCell.className, for: indexPath) as! THShareDetailTableViewCell
                    shareDetailCell.trashButton.removeTarget(nil, action: nil, for: .touchUpInside)
                    
                    //shareDetailCell.likeCountButton.removeTarget(nil, action: nil, for: .touchUpInside)
                    shareDetailCell.tagButton.removeTarget(nil, action: nil, for: .touchUpInside)
                    if let pinchGesture = shareDetailCell.socialPostImage.gestureRecognizers?[0] as? UIPinchGestureRecognizer{
                        shareDetailCell.socialPostImage.removeGestureRecognizer(pinchGesture)
                }
                case 1:
                    let shareDetailCell = tableView.dequeueReusableCell(withIdentifier: THShareDetailTableViewCell.className, for: indexPath) as! THShareDetailTableViewCell
                    shareDetailCell.commentButton.removeTarget(nil, action: nil, for: .touchUpInside)
                    shareDetailCell.rethreadButton.removeTarget(nil, action: nil, for: .touchUpInside)
                    shareDetailCell.likeButton.removeTarget(nil, action: nil, for: .touchUpInside)
                shareDetailCell.profile_nameButton.removeTarget(nil, action: nil, for: .touchUpInside)
                default:
                    break
            }
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            let socialInteraction = THProfilePostsManager.sharedInstance.profilePosts[indexPath.section] as SocialInteraction
            let imgHeight = CGFloat(socialInteraction.largeImageHeight)
            let imgWidth = CGFloat(socialInteraction.largeImageWidth)
            return (imgHeight/imgWidth) * screenwidth
        case 1:
            return UITableViewAutomaticDimension
        default:
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            let socialInteraction = THProfilePostsManager.sharedInstance.profilePosts[indexPath.section] as SocialInteraction
            let imgHeight = CGFloat(socialInteraction.largeImageHeight)
            let imgWidth = CGFloat(socialInteraction.largeImageWidth)
            return (imgHeight/imgWidth) * screenwidth
        case 1:
            return 100
        default:
            return 50
        }

    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == THProfilePostsManager.sharedInstance.profilePosts.count-1 {
            if THProfilePostsManager.sharedInstance.didLoadData {
                THProfilePostsManager.sharedInstance.didLoadData = false
                THProfilePostsManager.sharedInstance.retrieveProfilePosts(with: userId) { (error) in
                    guard error != nil else {
                        self.singleTableView.reloadData()
                        return
                    }
                }
            }
        }
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        profileDelegate?.scrollViewDidScrollProfile(scrollView)
    }
}

protocol profileTableDelegate: class {
    func scrollViewDidScrollProfile(_ scrollView: UIScrollView)
}




