//
//  THEditViewController.swift
//  Threadest
//
//  Created by Jaydeep on 13/01/18.
//  Copyright © 2018 JC. All rights reserved.
//

import UIKit
import IoniconsSwift

class THEditViewController: UIViewController {
    var profileImageURL = ""
    var profileEdit: PZEditProfileTableViewController?
    
    @IBOutlet var styleSelectionPickerView: UIView!
    @IBOutlet weak var objPickerView: UIPickerView!
    var selectSize = ""
   
    
    let userHeightArray = ["4'5","4'6","4'7","4'8","4'9","4'10","4'11","5'0","5'1","5'2","5'3","5'4","5'5","5'6","5'7","5'8","5'9","5'10","5'11","6'0","6'1","6'2","6'3","6'4","6'5","6'6","6'7","6'8","6'9","6'20","6'11","7'0","7'1","7'2","7'3","7'4"]
    
    var userAgeArray = ["10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80"]
    
    let userShirtSizesArray = ["XS","S","M","L","XL","XXL","XXXL"]
    
    let userMaleShoeSizeArray = ["6","6.5","7","7.5","8","8.5","9","9.5","10","10.5","11","11.5","12","12.5","13","13.5","14","14.5","15","15.5","16"]
    let userFemaleShoeSizeArray = ["1","1.5","2","2.5","3","3.5","4","4.5","5","5.5","6","6.5","7","7.5","8","8.5","9","9.5","10","10.5","11","11.5","12","12.5","13"]
    
    let userWaistSizeArray = ["28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60"]
    
    let userNeckSizeArray = ["14","14.5","15","15.5","16","16.5","17","17.5","18","18.5","19","19.5","20","20.5","21","22","23","24","25","26"]
    
    var femaleShoeSizeArray = ["4","4.5","5","5.5","6","6.5","7","7.5","8","8.5","9","9.5","10","10.5","11","11.5","12"]
    var femaleHipsSizeArray = ["35.5","36.5","37.5","38.5","40","41.5","43.5","45.5","47.5"]
    var femaleBustSizeArray = ["32A", "32B", "32C", "32D", "32DD", "32DDD", "34A", "34B", "34C", "34D", "34DD", "34DDD", "36A", "36B", "36C", "36D", "36DD", "36DDD", "38A", "38B", "38C", "38D", "38DD", "38DDD", "40C", "40D", "40DD", "XS", "S", "M", "L", "XL"]
    var femaleWaistSizeArray = ["23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40"]
    
    let userHipsSizeArray = ["37.75","40.5","42.25","43.75","45.5","47.5","49.5"]
    
    var userPickerArray:[String] = [String]()
    
    var user:User = User(userId: 0, about: "", username: "", email: "", phoneNumber: "", imageURL: "",thumbnailImageURL:"" ,password: "", height: "", age: "", gender: "", shirtSize: "", waistSize: "", shoe_size: "", neckSize: "", braSize: "", hipsSize: "", cartCount: 0, closetSetupOne: false, closetSetupTwo: false, closetSetupThree: false, closetSetupComplete: false, needsToFollowBrands: false, needsToFollowUsers: false, isFollowing:false, selectedImage:nil, influencerScore: 0, sharePostCode:0, tempAccount: false, invitePromoCode: "", website: "", verifiedAccount: false)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Edit Profile"
        styleSelectionPickerView.isHidden = true
        self.view.addSubview(styleSelectionPickerView)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: Ionicons.closeRound.image(20, color: UIColor.black), style: UIBarButtonItemStyle.plain, target: self, action: #selector(THEditViewController.didTapOnBackButton(_:)))
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.plain, target: self, action: #selector(THEditViewController.didTapOnSaveButton(_:)))
        // Do any additional setup after loading the view.
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(THEditViewController.dismissViewController))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editprofile" {
            if let editProfile = segue.destination as? PZEditProfileTableViewController {
                profileEdit = editProfile
                    editProfile.profileURL = profileImageURL
                editProfile.delegate = self
            }
        }
    }
    
    @objc func dismissViewController() {
        self.view.endEditing(true)
    }
    
    @IBAction func didTapOnBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapOnSaveButton(_ sender: Any) {
        self.view.endEditing(true)
        guard let isMale = profileEdit?.isMale else {
            return
        }
        guard let profile = profileEdit else {
            return
        }
        user.website = profile.websiteTextField.text!
        user.about = profile.bioTextView.text!
        user.gender = isMale ? "male":"female"
        user.height = profile.heightLabel.text!
        user.neckSize = profile.neckLabel.text!
        user.shirtSize = profile.shirtLabel.text!
        user.shoe_size = profile.shoeLabel.text!
        user.waistSize = profile.waistLabel.text!
        user.braSize = profile.braLabel.text!
        user.hipsSize = profile.hipsSizeLabel.text!
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THUserManager.sharedInstance.performUpdateUser(with: (THUserManager.sharedInstance.currentUserdata()?.userId)!, user: user) { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            guard let error = error else {
                //Add funnel for Setup Closet
                self.navigationController?.popViewController(animated: true)
                return
            }
            debugPrint(error)
        }
    }
    
    @IBAction func didTapOnDoneButton(_ sender: Any) {
        styleSelectionPickerView.isHidden = true
        let pickerselected = objPickerView.selectedRow(inComponent: 0)
        let selectedSize = userPickerArray[pickerselected]
        self.setUserSelectedSizeData(itemHeader: selectSize, selectedSize: selectedSize)
    }
    
    func setUserSelectedSizeData(itemHeader: String, selectedSize: String) {
        switch itemHeader {
        case "gender":
            user.gender = selectedSize
            break
        case "height":
            user.height = selectedSize
            profileEdit?.heightLabel.text = selectedSize
            break
        case "age":
            user.age = selectedSize
            break
        case "neck":
            user.neckSize = selectedSize
            profileEdit?.neckLabel.text = selectedSize
            break
        case "shirt":
            user.shirtSize = selectedSize
            profileEdit?.shirtLabel.text = selectedSize
            break
        case "shoes":
            user.shoe_size = selectedSize
            profileEdit?.shoeLabel.text = selectedSize
            break
        case "waist":
            user.waistSize = selectedSize
            profileEdit?.waistLabel.text = selectedSize
            break
        case "bra":
            user.braSize = selectedSize
            profileEdit?.braLabel.text = selectedSize
            break
        case "hips":
            user.hipsSize = selectedSize
            profileEdit?.hipsSizeLabel.text = selectedSize
            break
        default:
            break
        }
    }
}

extension THEditViewController: editProfileDelegate {
    func openPicker(strStyle: String) {
        styleSelectionPickerView.isHidden = false
        debugPrint(strStyle)
        selectSize = strStyle
        switch strStyle {
        case "height":
            userPickerArray = userHeightArray
            break
        case "age":
            userPickerArray = userAgeArray
            break
        case "neck":
            userPickerArray = userNeckSizeArray
            break
        case "shirt":
            userPickerArray = userShirtSizesArray
            break
        case "shoes":
            guard let isMale = profileEdit?.isMale else {
                return
            }
            if isMale {
                userPickerArray = userMaleShoeSizeArray
            } else {
                userPickerArray = femaleShoeSizeArray
            }
            break
        case "waist":
            guard let isMale = profileEdit?.isMale else {
                return
            }
            if isMale {
                userPickerArray = userWaistSizeArray
            } else {
                userPickerArray = femaleWaistSizeArray
            }
            break
        case "bra":
            userPickerArray = femaleBustSizeArray
            break
        case "hips":
            userPickerArray = femaleHipsSizeArray
            break
        default:
            break
        }
        objPickerView.reloadAllComponents()
    }
}

extension THEditViewController: UIPickerViewDelegate {
    
}

extension THEditViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return userPickerArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return userPickerArray[row]
    }
    
}


