//
//  THSettingsViewController.swift
//  Threadest
//
//  Created by Jaydeep on 22/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THSettingsViewController: UIViewController {
    //IBOutlet
    @IBOutlet weak var objTableView: UITableView!
    
    //Variable
    let sectionNames = [[SettingInfo(title: "Logout", section: "Your account", type: .logout),
                         SettingInfo(title: "Setup Closet", section: "Your account", type: .setupCloset)]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        objTableView.register(UINib(nibName: THSettingsTableViewCell.className, bundle: nil), forCellReuseIdentifier: THSettingsTableViewCell.className)
    }
    
    @IBAction func didTapCloseButton(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    //MARK: - Helper methods
    
    func logoutConfirmation() {
        self.present(GeneralMethods.sharedInstance.AlertviewGeneral(title: "Logout", body: "Are you sure you would like to log out?", cancelbutton: "No", okbutton: "Yes", completion: { () in
            THUserManager.sharedInstance.logoutCurrentuser(isNavigateToLogin: false, completion: { (error) in
                if error != nil {
                    let alertController = UIAlertController(title: "Error", message: "An error occured. Try again later!", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: .destructive, handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                }
            })
        }), animated: false, completion: nil)
    }
    
    func showSetupClosest() {
        let profileSetupVC = THProfileSetupViewController(nibName: THProfileSetupViewController.className, bundle: nil)
        profileSetupVC.setUPcompletionHander = {
        }
        self.navigationController?.pushViewController(profileSetupVC, animated: true)
    }
    
}

extension THSettingsViewController : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let listItems = sectionNames[indexPath.section]
        let selectedSetting = listItems[indexPath.row]
        switch selectedSetting.settingType {
        case .logout:
            logoutConfirmation()
            break
        case .setupCloset:
            showSetupClosest()
            break
        default:
            break
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: screenwidth, height: 50))
        let settingLabel = UILabel(frame: CGRect(x: 10, y: 0, width: screenwidth-10, height: 50))
        settingLabel.font = UIFont(name: "SFUIText-Regular", size: 16)
        settingLabel.textColor = UIColor.hexa("949494", alpha: 1)
        //All items in same section have the same section title. That's why we just get first item.
        let firstItemInSection = sectionNames[section].first
        settingLabel.text = firstItemInSection?.settingSection
        headerView.addSubview(settingLabel)
        return headerView
    }
}

extension THSettingsViewController:UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionNames.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let listItems = sectionNames[section]
        return listItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: THSettingsTableViewCell.className, for: indexPath) as! THSettingsTableViewCell
        let listItems = sectionNames[indexPath.section]
        //Fill up data to cell
        cell.configCell(settingInfo: listItems[indexPath.row])
        
        return cell
    }
    
    
}

