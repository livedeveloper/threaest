//
//  THSettingsTableViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 22/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THSettingsTableViewCell: UITableViewCell {
    @IBOutlet weak var settingNameLabel: UILabel!
 
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }
    
    func configCell(settingInfo : SettingInfo) {
        settingNameLabel.text = settingInfo.settingTitle
    }
}
