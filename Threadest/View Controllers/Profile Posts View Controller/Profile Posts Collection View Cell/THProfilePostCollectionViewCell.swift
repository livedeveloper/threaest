//
//  THProfilePostCollectionViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 10/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THProfilePostCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var commentCountLabel: UILabel!
    @IBOutlet weak var rethreadCountLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    
    func configure(withPost posts: SocialInteraction) {
        imgProfile.sd_setImage(with: URL(string: posts.mediumImageUrl), placeholderImage: nil, options: .retryFailed)
        commentCountLabel.text = "\(posts.commentsCount)"
        likeCountLabel.text = "\(posts.upvotesCount)"
//        likeButton.isSelected = posts.likedByCurrentUser ? true: false
    }


}
