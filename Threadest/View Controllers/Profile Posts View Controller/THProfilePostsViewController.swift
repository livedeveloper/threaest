//
//  THProfilePostsViewController.swift
//  Threadest
//
//  Created by Jaydeep on 10/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SwiftyJSON
import FBSDKLoginKit
import FBSDKCoreKit
import Social
import Accounts
import SDWebImage
import IoniconsSwift
import Pulsator
import Crashlytics
import ARKit

class THProfilePostsViewController: UIViewController, UIGestureRecognizerDelegate,UIImagePickerControllerDelegate,
UINavigationControllerDelegate {
    // Outlets
    @IBOutlet weak var objCollectionview: UICollectionView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var threadestScoreLabel: UILabel!
    @IBOutlet weak var followersCountLabel: UILabel!
    @IBOutlet weak var brandsCountLabel: UILabel!
    @IBOutlet weak var followingCountLabel: UILabel!

    @IBOutlet weak var userProfileNameLabel: UILabel!
    @IBOutlet weak var searchNavView: UIView!
    @IBOutlet weak var searchStatusView: UIView!
    //outlet
    @IBOutlet weak var addFriendCountLabel: UILabel!
    @IBOutlet weak var notificationCountLabel: UILabel!
    @IBOutlet weak var leadershipCountLabel: UILabel!
    @IBOutlet weak var cartCountLabel: UILabel!
    var isSearch: Bool = false

    @IBOutlet weak var objTableView: UITableView!
    @IBOutlet weak var btnFollowUnfollow: UIButton!
    @IBOutlet weak var btnProfileImage: UIButton!
    @IBOutlet weak var gridBtn: UIButton!
    @IBOutlet weak var listBtn: UIButton!
    @IBOutlet weak var viewGridList: UIView!
    @IBOutlet weak var noPostBubblebutton: DesignableButton!

    @IBOutlet weak var topConstraint: NSLayoutConstraint!


    // Variables
    var refreshControlTableView: UIRefreshControl!
    var refreshControlCollectionView: UIRefreshControl!
    var findUser:SearchUser?
    var profile:Profile!

    //variable for tabbar
    var lastContentOffset: CGFloat = 0
    var profileOffset:CGFloat = 0
    //var tabbarOffset: CGFloat!

    //variable for influencer score
    var infuencerTimer: Timer!
    var influencerScore = 0
    var newInfluencerScore = 0
    var pulsator = Pulsator()

    var isFromNotification:Bool!
    var userId:Int!
    let picker = UIImagePickerController()

    var chosenImage : UIImage!

    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self

        profileOffset = 0
        //tabbarOffset = screenheight - 49
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    objCollectionview.setCollectionViewLayout(CHTCollectionViewWaterfallLayout(), animated: false)
        objCollectionview.register(UINib(nibName: THProfilePostCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: THProfilePostCollectionViewCell.className)

        objTableView.register(UINib(nibName: THSocialPostTableViewCell.className, bundle: nil), forCellReuseIdentifier: THSocialPostTableViewCell.className)

        refreshControlTableView = UIRefreshControl()
        refreshControlTableView.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControlTableView.addTarget(self, action: #selector(THProfilePostsViewController.refresh(sender:)), for: UIControlEvents.valueChanged)
        objTableView.addSubview(refreshControlTableView)
        refreshControlCollectionView = UIRefreshControl()
        refreshControlCollectionView.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControlCollectionView.addTarget(self, action: #selector(THProfilePostsViewController.refresh(sender:)), for: UIControlEvents.valueChanged)
        objCollectionview.addSubview(refreshControlCollectionView)
        if isSearch {
            searchNavView.isHidden = false
            searchStatusView.isHidden = false
        }
        else {
            searchNavView.isHidden = true
            searchStatusView.isHidden = true
        }
        initUI()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self

        self.viewGridList.layer.borderColor = UIColor.lightGray.cgColor
        self.viewGridList.layer.borderWidth = 0.5

        let gridBlack = Ionicons.grid.image(30, color: UIColor.black)
        let gridBlue = Ionicons.grid.image(30, color: UIColor(red:0.09, green:0.55, blue:0.80, alpha:1.0))
        let listBlack = Ionicons.navicon.image(35, color: UIColor.black)
        let listBlue = Ionicons.navicon.image(35, color: UIColor(red:0.09, green:0.55, blue:0.80, alpha:1.0))

        self.gridBtn.setImage(gridBlack, for: .normal)
        self.gridBtn.setImage(gridBlue, for: .selected)
        self.listBtn.setImage(listBlack, for: .normal)
        self.listBtn.setImage(listBlue, for: .selected)

        self.gridBtn.isSelected = true

        if let usercurrent = THUserManager.sharedInstance.currentUserdata() , findUser == nil, profile == nil {
            userId = usercurrent.userId
            self.btnProfileImage.isEnabled = true
            if let cartCount = UserDefaults.standard.value(forKey: "cartCount")
            {
                if (cartCount as! Int) == 0 {
                    self.cartCountLabel.isHidden = true
                }
                else {
                    self.cartCountLabel.isHidden = false
                    self.cartCountLabel.text = "\(cartCount as! Int)"
                }
            } else {
                self.cartCountLabel.isHidden = true
            }
             self.btnFollowUnfollow.setTitle("Settings",for: .normal)
            THProfilePostsManager.sharedInstance.profilePosts = [SocialInteraction]()
            THProfilePostsManager.sharedInstance.page = 1
            THProfilePostsManager.sharedInstance.didLoadData = false
            THProfilePostsManager.sharedInstance.retrieveProfilePosts(with: userId) { (error) in
                guard error != nil else {
                    self.retrievePostsData()
                    self.noPostBubblebutton.isHidden = THProfilePostsManager.sharedInstance.profilePosts.count == 0 ? false : true
                    return
                }
            }
        } else {
            self.btnProfileImage.isEnabled = false
        }


        if(self.profile != nil) {
            THProfilePostsManager.sharedInstance.profile = self.profile
            userProfileNameLabel.text = "\(profile.username)"
            self.retrievePostsData()
        }
        else if(self.findUser != nil) {
            userId = Int(self.findUser!.objectID!)!
            THProfilePostsManager.sharedInstance.profilePosts = [SocialInteraction]()
            THProfilePostsManager.sharedInstance.page = 1
            THProfilePostsManager.sharedInstance.didLoadData = false
            THProfilePostsManager.sharedInstance.retrieveProfilePosts(with: Int(self.findUser!.objectID!)!) { (error) in
                guard error != nil else {
                    self.profile = THProfilePostsManager.sharedInstance.profile
                    self.userProfileNameLabel.text = "\(self.profile.username)"
                    self.retrievePostsData()
                    return
                }
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
//        appdelegate.verticle.scrollEnabled(isEnable: false)
        
        self.updateNotificationCount()
        //setup influencer score
        if let currentUser = THUserManager.sharedInstance.currentUserdata() {
            if let infuencerScoreDefault = UserDefaults.standard.object(forKey: userDefaultKey.influencerScoreDefault.rawValue) {
                self.leadershipCountLabel.text = "+\(infuencerScoreDefault)  "
                influencerScore = infuencerScoreDefault as! Int
            } else {
                self.leadershipCountLabel.text = "+\(currentUser.influencerScore)  "
                influencerScore = currentUser.influencerScore
            }
        }

        if isSearch {
            btnFollowUnfollow.isHidden = false
                self.tabBarController?.tabBar.isHidden = true
        }
        else {
            btnFollowUnfollow.isHidden = false
            self.tabBarController?.tabBar.isHidden = true
        }

        NotificationCenter.default.addObserver(self, selector: #selector(THProfilePostsViewController.postNoficationInfluencerScore(notification:)), name: Notification.Name(postNotificationType.notificationInfluenceScore.rawValue), object: nil)
        self.checkCurrentInfluencerScore()

    }

    override func viewDidAppear(_ animated: Bool) {
        leadershipCountLabel.layer.cornerRadius = leadershipCountLabel.frame.width / 2.0
        leadershipCountLabel.layer.masksToBounds = true
    }

    override func viewWillDisappear(_ animated: Bool) {
//        appdelegate.verticle.scrollEnabled(isEnable: true)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(postNotificationType.notificationInfluenceScore.rawValue), object: nil)
    }

    fileprivate func initUI() {
        profileImage.layer.cornerRadius = profileImage.frame.size.width / 2
        profileImage.layer.masksToBounds = true

        addFriendCountLabel.layer.cornerRadius = addFriendCountLabel.frame.size.width / 2
        addFriendCountLabel.layer.masksToBounds = true
        notificationCountLabel.layer.cornerRadius = notificationCountLabel.frame.size.width / 2
        notificationCountLabel.layer.masksToBounds = true
        leadershipCountLabel.layer.cornerRadius = leadershipCountLabel.frame.size.width / 2
        leadershipCountLabel.layer.masksToBounds = true
        cartCountLabel.layer.cornerRadius = cartCountLabel.frame.size.width / 2
        cartCountLabel.layer.masksToBounds = true
        btnFollowUnfollow.layer.cornerRadius = 17.5
    }

    func refresh(sender:AnyObject) {
        THProfilePostsManager.sharedInstance.page = 1
        THProfilePostsManager.sharedInstance.didLoadData = false
        THProfilePostsManager.sharedInstance.pullToRefresh = true
        THProfilePostsManager.sharedInstance.retrieveProfilePosts(with: userId) { (error) in
            if self.objTableView.isHidden {
                self.refreshControlCollectionView.endRefreshing()
            } else {
                self.refreshControlTableView.endRefreshing()
            }
            guard error != nil else {
                self.profile = THProfilePostsManager.sharedInstance.profile
                self.userProfileNameLabel.text = "\(self.profile.username)"
                self.retrievePostsData()
                return
            }
        }
    }

    //get post notification of infuence score
    func postNoficationInfluencerScore(notification: NSNotification) {
        let postNotificationJson = JSON(notification.object!)
        
        newInfluencerScore = postNotificationJson["new_influencer_score"].intValue
        if influencerScore >= newInfluencerScore {
            return
        }

        if infuencerTimer == nil {
            infuencerTimer = Timer.scheduledTimer(timeInterval: 0.08, target: self, selector: #selector(THSocialViewController.timercounterOfInfluencerScore), userInfo: nil, repeats: true)
            leadershipCountLabel.backgroundColor = UIColor.red
            pulsator.numPulse = 3
            pulsator.radius = 50.0
            pulsator.backgroundColor = UIColor.white.cgColor
            leadershipCountLabel.layer.addSublayer(pulsator)
            pulsator.start()
        }
    }

    // check current influencer score
    func checkCurrentInfluencerScore() {
        if let currentInfuencerScoreDefault = UserDefaults.standard.object(forKey: userDefaultKey.currentInfluencerScoreDefault.rawValue) {
            newInfluencerScore = currentInfuencerScoreDefault as! Int
        }

        if influencerScore >= newInfluencerScore {
            return
        }
        if infuencerTimer == nil {
            infuencerTimer = Timer.scheduledTimer(timeInterval: 0.08, target: self, selector: #selector(THSocialViewController.timercounterOfInfluencerScore), userInfo: nil, repeats: true)
            leadershipCountLabel.backgroundColor = UIColor.red
            pulsator.numPulse = 3
            pulsator.radius = 50.0
            pulsator.backgroundColor = UIColor.white.cgColor
            leadershipCountLabel.layer.addSublayer(pulsator)
            pulsator.start()
        }
    }


    //timer for influecer score animation when trigger notification
    func timercounterOfInfluencerScore() {
        influencerScore = influencerScore + 1
        DispatchQueue.main.async {
            self.leadershipCountLabel.text = " +\(self.influencerScore)  "
            self.leadershipCountLabel.layer.cornerRadius = self.leadershipCountLabel.frame.width / 2.0
            self.leadershipCountLabel.layer.masksToBounds = true
        }
        if (influencerScore == newInfluencerScore - 5) {
            infuencerTimer.invalidate()
            infuencerTimer = nil
            infuencerTimer = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(THSocialViewController.timercounterOfInfluencerScore), userInfo: nil, repeats: true)
        }

        if (influencerScore == newInfluencerScore || newInfluencerScore < influencerScore) {
            infuencerTimer.invalidate()
            infuencerTimer = nil
            pulsator.stop()
        }
        UserDefaults.standard.set(influencerScore, forKey: userDefaultKey.influencerScoreDefault.rawValue)
        UserDefaults.standard.synchronize()
    }

    fileprivate func updateNotificationCount() {
        notificationCountLabel.isHidden = UIApplication.shared.applicationIconBadgeNumber == 0 ? true:false
        notificationCountLabel.text = ""
    }

    fileprivate func retrievePostsData() {
        let profil = THProfilePostsManager.sharedInstance.profile
        self.usernameLabel.text = profil!.username
        self.threadestScoreLabel.text = "Threadsetter Score: \(profil!.currentUserInfluencerScore)"
        self.profileImage.sd_setImage(with: URL(string: (profil!.mediumImageUrl)))
        self.followersCountLabel.text = "\(profil!.followersCount)"
        self.brandsCountLabel.text = "\(profil!.brandsCount)"
        self.followingCountLabel.text = "\(profil!.followingsCount)"
        self.objCollectionview.reloadData()
        self.objTableView.reloadData()
        self.objTableView.setNeedsLayout()
        self.objTableView.layoutIfNeeded()
        self.objTableView.layoutSubviews()
        self.objTableView.updateConstraints()
        self.objTableView.setNeedsUpdateConstraints()
        self.objTableView.reloadData()
        if(profile != nil) {
            if profile.currentUserFollowing == true  {
                btnFollowUnfollow.setTitle("UnFollow",for: .normal)
            }
            else {
                btnFollowUnfollow.setTitle("Follow",for: .normal)
            }
        }
    }

    //MARK: All Action methods
    @IBAction func didTapOnCartButton(_ sender: Any) {
        let shoppingcartController = THYourCartViewController(nibName: THYourCartViewController.className, bundle: nil)
        self.navigationController?.pushViewController(shoppingcartController, animated: true)
    }

    @IBAction func didTapOnAddFriendButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func didTapOnNotificationButton(_ sender: Any) {
        let noficationController = THNotificationController(nibName: THNotificationController.className, bundle: nil)
        let navNotificationController = UINavigationController(rootViewController: noficationController)
        self.present(navNotificationController, animated: true, completion: nil)
    }


    @IBAction func didTapOnLeadershipButton(_ sender: Any) {
        self.leadershipCountLabel.backgroundColor = UIColor.blueTabbar
        let threadestBlackController = THBlackIntroViewController(nibName: THBlackIntroViewController.className, bundle: nil)
        let navigationController = UINavigationController(rootViewController: threadestBlackController)
        self.present(navigationController, animated: true, completion: {
        });


    }

    @IBAction func didTapFollowing(_ sender: Any) {
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }
        let followingController = THUserFollowerViewController(nibName: THUserFollowerViewController.className, bundle: nil)
        followingController.profileType = "Following"
        if(self.findUser != nil) {
            followingController.userId = Int(self.findUser!.objectID!)
        }
        else if(userId != nil) {
            followingController.userId = userId
        }
        else {
            followingController.userId = THUserManager.sharedInstance.currentUserdata()?.userId
        }
        self.navigationController?.pushViewController(followingController, animated: true)
    }


    @IBAction func didTapFollowers(_ sender: Any) {
        let followerController = THUserFollowerViewController(nibName: THUserFollowerViewController.className, bundle: nil)
        followerController.profileType = "Followers"
        if(self.findUser != nil) {
            followerController.userId = Int(self.findUser!.objectID!)
        }
        else if(userId != nil) {
            followerController.userId = userId
        }
        else {
            followerController.userId = THUserManager.sharedInstance.currentUserdata()?.userId
        }
        self.navigationController?.pushViewController(followerController, animated: true)
    }

    @IBAction func didTapBrandFollowing(_ sender: Any) {
        let brandFollowingController = THUserFollowerViewController(nibName: THUserFollowerViewController.className, bundle: nil)
        brandFollowingController.profileType = "Brand"
        if(self.findUser != nil) {
            brandFollowingController.userId = Int(self.findUser!.objectID!)
        }
        else if(userId != nil) {
            brandFollowingController.userId = userId
        }
        else {
            brandFollowingController.userId = THUserManager.sharedInstance.currentUserdata()?.userId
        }
        self.navigationController?.pushViewController(brandFollowingController, animated: true)
    }
    @IBAction func didTapbackbutton(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func didTapListBtn(_ sender: Any) {
        if(!self.listBtn.isSelected) {
            self.listBtn.isSelected = true
        }
        self.gridBtn.isSelected = false
        self.objCollectionview.isHidden = true
        self.objTableView.isHidden = false
        objTableView.reloadData()
    }
    @IBAction func didTapGridBtn(_ sender: Any) {
        if(!self.gridBtn.isSelected) {
            self.gridBtn.isSelected = true
        }
        self.listBtn.isSelected = false
        self.objCollectionview.isHidden = false
        self.objTableView.isHidden = true
        objCollectionview.reloadData()
    }


    @IBAction func didTapOnNoPostBubble(_ sender: Any) {
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
            } else {
                let swiftyCameraVC = THSwiftyCameraViewController(nibName: THSwiftyCameraViewController.className, bundle: nil)
                swiftyCameraVC.view.frame.size.width = screenwidth
                swiftyCameraVC.view.frame.size.height = screenheight
                swiftyCameraVC.completionHander = {
                    object, isfrom in
                    if !self.isSearch {
                        self.dismiss(animated: true, completion: {
                            if let thsocial = self.tabBarController?.viewControllers?.first as? THSocialViewController {
                                thsocial.productObject = object
                                thsocial.isFromCamera = true
//                                appdelegate.verticle.scrollDown(isAnimated: false)
                            }
                        })
                    } else {
                        
                    }
                   
                }
                self.present(swiftyCameraVC, animated: true, completion: nil)
            }
        }
    }

    //MARK: Follow button Action
    @IBAction func didTapFollowButton(_ sender: UIButton) {

        if sender.titleLabel?.text == "Follow" {
            self.btnFollowUnfollow.setTitle("UnFollow",for: .normal)
            THSocialInteractionPostsManager.sharedInstance.performFollowUser(for:userId) { (error) in
                guard let error = error else {
                    self.followersCountLabel.text = "\((Int(self.followersCountLabel.text!)! + 1))"
                    return
                }
                debugPrint(error)
            }
        }
        else if sender.titleLabel?.text == "UnFollow"{
            self.btnFollowUnfollow.setTitle("Follow",for: .normal)

            THSocialInteractionPostsManager.sharedInstance.performUnfollowUser(for: userId, completion: { (error) in
                guard let error = error else {
                    self.followersCountLabel.text = "\((Int(self.followersCountLabel.text!)! - 1))"
                    return
                }
                debugPrint(error)
            })
        }
        else if sender.titleLabel?.text == "Settings"{
            let settingVC = THSettingsViewController(nibName: THSettingsViewController.className, bundle: nil)
            let navSettingVC = UINavigationController(rootViewController: settingVC)
            self.present(navSettingVC, animated: true, completion: nil)
        }
    }

    @IBAction func didTapOnCommentButton(_ sender: UIButton) {
        let commentController = THCommentViewController(nibName: THCommentViewController.className, bundle: nil)
        commentController.socialInteraction = THProfilePostsManager.sharedInstance.profilePosts[sender.tag]
        self.navigationController?.pushViewController(commentController, animated: true)
    }

    @IBAction func didTapProfileImage(_ sender: Any) {

        let actionSheetController = UIAlertController(title:nil, message:nil, preferredStyle: .actionSheet)

        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            
        }
        actionSheetController.addAction(cancelActionButton)
        
        let TakeActionButton = UIAlertAction(title: "Take from Camera", style: .default) { action -> Void in
            self.picker.allowsEditing = false
            self.picker.sourceType = .camera
            self.present( self.picker, animated: true, completion: nil)
            print("Camera")
        }
        actionSheetController.addAction(TakeActionButton)

        let saveActionButton = UIAlertAction(title: "Pick from Photos", style: .default) { action -> Void in
             self.picker.allowsEditing = false
             self.picker.sourceType = .photoLibrary
             self.self.present( self.picker, animated: true, completion: nil)
            print("Photos")
        }
        actionSheetController.addAction(saveActionButton)

        self.present(actionSheetController, animated: true, completion: nil)

    }

    @IBAction func didTapOnRethreadButton(_ sender: UIButton) {
        let cell = objTableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! THSocialPostTableViewCell
        let socialInteraction:SocialInteraction = THProfilePostsManager.sharedInstance.profilePosts[sender.tag]
        if !cell.rethreadButton.isSelected {
            cell.rethreadButton.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                cell.rethreadButton.transform = CGAffineTransform(rotationAngle: CGFloat(-179.9).degreesToRadians)
            })
            UIView.animate(withDuration: 0.3, delay: 0.2, options: UIViewAnimationOptions(), animations: {
                cell.rethreadButton.transform = CGAffineTransform(rotationAngle: CGFloat(1.1).degreesToRadians)
            }, completion: { (completed) in
                let rethreadpopup = THRethreadPopupViewController(nibName: THRethreadPopupViewController.className, bundle: nil)
                rethreadpopup.productLinkString = "https://www.threadest.com/posts/\(socialInteraction.id)"
                self.addChildViewController(rethreadpopup)
                rethreadpopup.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
                self.view.addSubview(rethreadpopup.view)
                rethreadpopup.didMove(toParentViewController: self)
                rethreadpopup.completionHander = {selectedRethreadArray in
                    for selectedRethread in selectedRethreadArray! {
                        switch selectedRethread as! String {
                        case "Threadest":
                            self.rethreadPost(selectedRow: sender.tag)
                            break
                        case "Facebook":
                            self.rethreadPostOnFacebook(selectedRow: sender.tag)
                            break
                        case "Twitter":
                            self.rethreadPostOnTwitter(socialInteration:THProfilePostsManager.sharedInstance.profilePosts[sender.tag], comment: "")
                            break
                        default:
                            break
                        }
                    }
                }
            })
        } else {

        }
    }

    @IBAction func didTapOnLikeButton(_ sender: UIButton) {
        if !sender.isSelected {
            let cell = objTableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! THSocialPostTableViewCell
            sender.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                sender.transform = CGAffineTransform(rotationAngle: CGFloat(-179.9).degreesToRadians)
            })
            UIView.animate(withDuration: 0.3, delay: 0.2, options: UIViewAnimationOptions(), animations: {
                sender.transform = CGAffineTransform(rotationAngle: CGFloat(1.1).degreesToRadians)
            }, completion: { (completed) in
                if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    THUserManager.sharedInstance.performUpvote(for: usercurrent.userId, votableId: THProfilePostsManager.sharedInstance.profilePosts[sender.tag].id, votableType: "Post", completion: { (vote, error) in
                        guard error != nil else {
                            if let i = THProfilePostsManager.sharedInstance.profilePosts.index(where: { $0.id == THProfilePostsManager.sharedInstance.profilePosts[sender.tag].id}){
                                THProfilePostsManager.sharedInstance.profilePosts[i].likedByCurrentUser = true
                                THProfilePostsManager.sharedInstance.profilePosts[i].upvotesCount = THProfilePostsManager.sharedInstance.profilePosts[i].upvotesCount + 1
                            }
                            THProfilePostsManager.sharedInstance.profilePosts[sender.tag].votes.append(vote!)

                            cell.upvoteCountLabel.text = "\(THProfilePostsManager.sharedInstance.profilePosts[sender.tag].upvotesCount)"
                            cell.upvoteCountLabel.isHidden = false
                            sender.isSelected  = true
                            return
                        }
                    })
                }
                else {

                }
            })
        }
    }



    @IBAction func didTapOnDeletePostButton(_ sender: UIButton) {

        let alert = UIAlertController(title: "Threadest", message: "Are you sure you want to remove post?", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            let socialInteraction = THProfilePostsManager.sharedInstance.profilePosts[sender.tag] as SocialInteraction
            THProfilePostsManager.sharedInstance.deletePost(with: Int(socialInteraction.id)) { (error) in
                guard error != nil else {
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    THProfilePostsManager.sharedInstance.profilePosts.remove(at: sender.tag)
                    self.objTableView.reloadData()
                    self.objCollectionview.reloadData()
                    return
                }
            }
        }))

        alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: { (action) -> Void in

        }))

        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func didTapOnPhotoTagButton(_ sender: UIButton) {
        let socialInteraction = THProfilePostsManager.sharedInstance.profilePosts[sender.tag]
        let photoTag = socialInteraction.photoTags[Int("\(sender.accessibilityIdentifier!)")!]
        if photoTag.tagType == "Brand Tag"{
            let brandProfileVC:THBrandProfileViewController = THBrandProfileViewController()
            brandProfileVC.isFrom = "brandlogo"
            brandProfileVC.brandId = photoTag.brandId
            self.navigationController?.pushViewController(brandProfileVC, animated: true)
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Tap on Brand Tag", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "brand_name":photoTag.brandName])
            }
        } else if photoTag.tagType == "Product Tag" {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THBrandProductManager.sharedInstance.retrieveBrandProfileFromDots(with: photoTag.brandId, productId: photoTag.productId, sharePostCode: socialInteraction.ownerSharePostCode, postId: socialInteraction.id, completion: { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                let threeDAvailable = THBrandProductManager.sharedInstance.brandProduct?.productGalleryImages.contains(where: { (imgGallery) -> Bool in
                    if imgGallery.image_type == "3D" {
                        return true
                    } else {
                        return false
                    }
                })
                
                if threeDAvailable! {
                    if #available(iOS 11.0, *) {
                        if (ARConfiguration.isSupported) {
                            let ARShopShowController = THARShopShowViewController(nibName: THARShopShowViewController.className, bundle: nil)
                            ARShopShowController.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                            self.navigationController?.pushViewController(ARShopShowController, animated: true)
                            return
                        }
                    }
                }
                let shopshowcontroller = THShopshowViewController(nibName: THShopshowViewController.className, bundle: nil)
                shopshowcontroller.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                self.navigationController?.pushViewController(shopshowcontroller, animated: true)
                
                if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    Answers.logCustomEvent(withName: "Tap on Product Tag", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "product_name":photoTag.productName])
                }
                
            })
        } else if photoTag.tagType == "Hash Tag" {
            let hashTagController = THHashTagViewController(nibName: THHashTagViewController.className, bundle: nil)
            hashTagController.hashTagString = photoTag.hashTag
            self.navigationController?.pushViewController(hashTagController, animated: true)
        }
       /* GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THBrandProductManager.sharedInstance.retrieveBrandProfile(with: photoTag.brandId, productId: photoTag.productId, geofence: false) { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            let shopshowcontroller = THShopshowViewController(nibName: THShopshowViewController.className, bundle: nil)
            shopshowcontroller.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
            self.navigationController?.pushViewController(shopshowcontroller, animated: true)
        }*/
    }

    // for Rethread post Threadest
    func rethreadPost(selectedRow: Int) {
        let socialInteraction:SocialInteraction = THProfilePostsManager.sharedInstance.profilePosts[selectedRow]

        THSocialInteractionPostsManager.sharedInstance.performRethreadPost(for: socialInteraction.id) { (error) in
            guard let error = error else {
                let socialCell = self.objTableView.cellForRow(at: IndexPath(row: selectedRow, section: 0)) as! THSocialPostTableViewCell

                if let i = THProfilePostsManager.sharedInstance.profilePosts.index(where: { $0.id == THProfilePostsManager.sharedInstance.profilePosts[selectedRow].id}){
                    THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[i].rethreadCount = THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[i].rethreadCount + 1
                }
                socialCell.rethreadCountLabel.text = "\(socialInteraction.rethreadCount + 1)"
                socialCell.rethreadCountLabel.isHidden = false
                THProfilePostsManager.sharedInstance.profilePosts[selectedRow].rethreadCount = socialInteraction.rethreadCount + 1
                return
            }
            debugPrint(error)
        }
    }

    // for Rethread post on Facebook
    func rethreadPostOnFacebook(selectedRow: Int) {
        let socialInteraction:SocialInteraction = THProfilePostsManager.sharedInstance.profilePosts[selectedRow]
        if FBSDKAccessToken.current() != nil {
            if FBSDKAccessToken.current().hasGranted("publish_actions") {
                GeneralMethods.sharedInstance.publishMessage(message: "https://www.threadest.com/posts/\(socialInteraction.id)", link: socialInteraction.largeImageUrl, completion: { () in
                    GeneralMethods.sharedInstance.RethreadPostSucessPopup(message: "Post to Facebook - Successful", viewController: self)
                })
            } else {
                requestPublishPermissions(socialInteration:socialInteraction)
            }
        } else {
            requestPublishPermissions(socialInteration:socialInteraction)
        }
    }

    func requestPublishPermissions(socialInteration: SocialInteraction) {
        let login: FBSDKLoginManager = FBSDKLoginManager()

        login.logIn(withPublishPermissions: ["publish_actions"], from: self) { (result, error) in
            if (error != nil) {
                print(error!)
            } else if (result?.isCancelled)! {
                print("Canceled")
            } else if (result?.grantedPermissions.contains("publish_actions"))! {
                print("permissions granted")
                GeneralMethods.sharedInstance.publishMessage(message: "https://www.threadest.com/posts/\(socialInteration.id)", link: socialInteration.largeImageUrl, completion: { () in
                    GeneralMethods.sharedInstance.RethreadPostSucessPopup(message: "Post to Facebook - Successful", viewController: self)
                })
            }
        }
    }


    func rethreadPostOnTwitter(socialInteration: SocialInteraction, comment: String) {
        let accountStore = ACAccountStore()
        let accountType = accountStore.accountType(withAccountTypeIdentifier: ACAccountTypeIdentifierTwitter)

        accountStore.requestAccessToAccounts(with: accountType, options: nil) { (granted, error) in
            if granted, let accounts = accountStore.accounts(with: accountType) {
                // This will default to the first account if they have more than one
                if let account = accounts.first as? ACAccount {
                    let _ = SDWebImageManager.shared().imageDownloader?.downloadImage(with: URL(string: socialInteration.watermarkImageUrl), options: .allowInvalidSSLCertificates, progress: { (block1, block2, url) in
                    }, completed: { (image, data, error, bool) in
                        let requestURL = URL(string: "https://upload.twitter.com/1/statuses/update_with_media.json")
                        let parameters = ["status" : "https://www.threadest.com/posts/\(socialInteration.id) \n \(comment)"]
                        guard let request = SLRequest(forServiceType: SLServiceTypeTwitter, requestMethod: .POST, url: requestURL, parameters: parameters) else { return }
                        request.addMultipartData(UIImageJPEGRepresentation(image!, 1), withName: "media", type: nil, filename: nil)
                        request.account = account
                        request.perform(handler: { (data, response, error) in
                            if response?.statusCode == 200 {
                                DispatchQueue.main.async {
                                    GeneralMethods.sharedInstance.RethreadPostSucessPopup(message: "Post to Twitter - Successful", viewController: self)
                                }
                            }
                            // Check to see if tweet was successful
                        })
                    })
                } else {
                    debugPrint("User does not have an available Twitter account")
                }
            }
        }
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.profileImage.image = chosenImage
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THNetworkManager.sharedInstance.updateUserImage(id: THUserManager.sharedInstance.currentUserdata()?.userId, image: chosenImage, completion: { (json, error) in
            guard let json = json else {
                self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "", body: (error?.localizedDescription)!, completion: {
                    
                }), animated: true, completion: nil)
                return
            }
            DispatchQueue.main.async {
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            }
            if json["status"].boolValue {
                THUserManager.sharedInstance.saveCurrentUser(currentuser:json["data"])
                    DispatchQueue.main.async {
                    /*THProfilePostsManager.sharedInstance.profilePosts.forEach{ anyPost in
                        var temp = anyPost
                            temp.ownerImageUrl = (THUserManager.sharedInstance.currentUserdata()?.thumbnailImageURL)!
                        }
                    //self.retrievePostsData()
                    self.objTableView.reloadData()*/
                        THProfilePostsManager.sharedInstance.retrieveProfilePosts(with: (THUserManager.sharedInstance.currentUserdata()?.userId)!) { (error) in
                            guard error != nil else {
                                self.retrievePostsData()
                                return
                            }
                        }

                    self.profileImage.image = info[UIImagePickerControllerOriginalImage] as? UIImage
                }
            } else {
//                let error = NSError(domain: "Error", code: 0, userInfo: [NSLocalizedDescriptionKey : json["message"].stringValue])
            }
        }) { (progress) in
            print("progress \(progress)")
        }

        dismiss(animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension THProfilePostsViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item == THProfilePostsManager.sharedInstance.profilePosts.count-1 {
            if THProfilePostsManager.sharedInstance.didLoadData {
                THProfilePostsManager.sharedInstance.didLoadData = false
                THProfilePostsManager.sharedInstance.retrieveProfilePosts(with: userId) { (error) in
                    guard error != nil else {
                        self.objCollectionview.reloadData()
                        return
                    }
                }
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THProfilePostsManager.sharedInstance.retrieveNotificationPost(with: THProfilePostsManager.sharedInstance.profilePosts[indexPath.row].id, completion: { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                if THProfilePostsManager.sharedInstance.notificationPost == nil {
                    return
                }
                guard error != nil else {
                    let commentController = THCommentViewController(nibName: THCommentViewController.className, bundle: nil)
                    commentController.socialInteraction = THProfilePostsManager.sharedInstance.notificationPost
                    self.navigationController?.pushViewController(commentController, animated: true)
                    return
                }
            })
        }
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.lastContentOffset < scrollView.contentOffset.y && scrollView.contentOffset.y > 0 {
            if profileOffset > -130 {
                topConstraint.constant = self.profileOffset
                self.profileOffset = self.profileOffset - 3.5
            } else if profileOffset == -130 {
                topConstraint.constant = -130
            }
        } else if self.lastContentOffset > scrollView.contentOffset.y {
            if profileOffset == 0 {
                topConstraint.constant = 0
            }else if profileOffset < 0 {
                topConstraint.constant = self.profileOffset
                self.profileOffset = self.profileOffset + 3.5
            }
        }
        self.lastContentOffset = scrollView.contentOffset.y
    }
}


extension THProfilePostsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == THProfilePostsManager.sharedInstance.profilePosts.count-1 {
            if THProfilePostsManager.sharedInstance.didLoadData {
                THProfilePostsManager.sharedInstance.didLoadData = false
                THProfilePostsManager.sharedInstance.retrieveProfilePosts(with: userId) { (error) in
                    guard error != nil else {
                        self.objTableView.reloadData()
                        return
                    }
                }
            }
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THProfilePostsManager.sharedInstance.retrieveNotificationPost(with: THProfilePostsManager.sharedInstance.profilePosts[indexPath.row].id, completion: { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                if THProfilePostsManager.sharedInstance.notificationPost == nil {
                    return
                }
                guard error != nil else {
                    let commentController = THCommentViewController(nibName: THCommentViewController.className, bundle: nil)
                    commentController.socialInteraction = THProfilePostsManager.sharedInstance.notificationPost
                    self.navigationController?.pushViewController(commentController, animated: true)
                    return
                }
            })
        }
    }
}

extension THProfilePostsViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{

            return THProfilePostsManager.sharedInstance.profilePosts.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let shareDetailCell = tableView.dequeueReusableCell(withIdentifier: THSocialPostTableViewCell.className, for: indexPath) as! THSocialPostTableViewCell
        let socialInteraction = THProfilePostsManager.sharedInstance.profilePosts[indexPath.row] as SocialInteraction

        if let currentUser = THUserManager.sharedInstance.currentUserdata() {
            if socialInteraction.userId == currentUser.userId {
                shareDetailCell.trashButton.isHidden = false
                shareDetailCell.lblTrailingConstant.constant = 32
            }
            else {
                shareDetailCell.trashButton.isHidden = true
                shareDetailCell.lblTrailingConstant.constant = 5
            }
        }

        shareDetailCell.selectionStyle = UITableViewCellSelectionStyle.none
        shareDetailCell.likeButton.tag = indexPath.row
        shareDetailCell.commentButton.tag = indexPath.row
        shareDetailCell.profile_nameButton.tag = indexPath.row
        shareDetailCell.commentButton.addTarget(self, action: #selector(THProfilePostsViewController.didTapOnCommentButton(_:)), for: UIControlEvents.touchUpInside)
        shareDetailCell.rethreadButtonTop.tag = indexPath.row
        shareDetailCell.rethreadButtonTop.addTarget(self, action: #selector(THProfilePostsViewController.didTapOnRethreadButton(_:)), for: .touchUpInside)
        shareDetailCell.likeButton.tag = indexPath.row
        shareDetailCell.likeButton.addTarget(self, action: #selector(THProfilePostsViewController.didTapOnLikeButton(_:)), for: .touchUpInside)
        shareDetailCell.configure(withSocialInteraction: socialInteraction)

        shareDetailCell.trashButton.tag = indexPath.row
        shareDetailCell.trashButton.addTarget(self, action: #selector(THProfilePostsViewController.didTapOnDeletePostButton(_:)), for: UIControlEvents.touchUpInside)

       // shareDetailCell.profile_nameButton.addTarget(self, action: #selector(THSocialViewController.didTapOnProfileUserNameButton(_:)), for: UIControlEvents.touchUpInside)

        for button in shareDetailCell.socialPostImage.subviews {
            button.removeFromSuperview()
        }

        let imgHeight = CGFloat(socialInteraction.largeImageHeight)
        let imgWidth = CGFloat(socialInteraction.largeImageWidth)
        shareDetailCell.heightConstraintImage.constant = (imgHeight/imgWidth) * screenwidth
        self.updateViewConstraints()
        self.updateFocusIfNeeded()
        shareDetailCell.circularProgressView.isHidden = false
        shareDetailCell.socialPostImage.sd_setImage(with: URL(string: socialInteraction.largeImageUrl), placeholderImage: nil, options: .retryFailed, progress: { (block1, block2, imageurl) in
            let progress = Double(block1)/Double(block2)
            DispatchQueue.main.async {
                shareDetailCell.circularProgressView.setProgress(progress, animated: true)
            }
        }) { (image, error, imageCacheType, imageUrl) in
            if image != nil {
                shareDetailCell.circularProgressView.isHidden = true
                //for tag view
                if socialInteraction.photoTags.count > 0 {
                    var i = 0
                    for tags in socialInteraction.photoTags {
                        shareDetailCell.tagButton = UIButton(frame: CGRect(x: tags.x, y: tags.y, width: 30, height: 30))
                        shareDetailCell.tagButton.isUserInteractionEnabled = true
                        if tags.tagType == "Hash Tag" {
                            shareDetailCell.tagButton.backgroundColor = UIColor.black
                            shareDetailCell.tagButton.layer.borderColor = UIColor.blueTabbar.cgColor
                        } else {
                            shareDetailCell.tagButton.backgroundColor = UIColor.hexa("178dcd", alpha: 1)
                            shareDetailCell.tagButton.layer.borderColor = UIColor.white.cgColor
                        }
                        shareDetailCell.tagButton.layer.borderWidth = 4
                        shareDetailCell.tagButton.layer.cornerRadius = shareDetailCell.tagButton.frame.size.width / 2
                        shareDetailCell.tagButton.accessibilityIdentifier = String(format: "%d", i)
                        shareDetailCell.tagButton.layer.masksToBounds = true
                        shareDetailCell.tagButton.tag = indexPath.row
                        shareDetailCell.tagButton.addTarget(self, action: #selector(THSocialViewController.didTapOnPhotoTagButton(_:)), for: .touchUpInside)
                        shareDetailCell.socialPostImage.addSubview(shareDetailCell.tagButton)
                        i = i+1
                    }
                }
            }else {
            }
        }
        return shareDetailCell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
            return 437
    }
}


extension THProfilePostsViewController: UICollectionViewDataSource, CHTCollectionViewDelegateWaterfallLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return THProfilePostsManager.sharedInstance.profilePosts.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let profilePostCell = collectionView.dequeueReusableCell(withReuseIdentifier: THProfilePostCollectionViewCell.className, for: indexPath) as! THProfilePostCollectionViewCell
        profilePostCell.configure(withPost: THProfilePostsManager.sharedInstance.profilePosts[indexPath.row])
        return profilePostCell
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize{
            let nbCol = 2
            let flowLayout = collectionViewLayout as! CHTCollectionViewWaterfallLayout
            let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(nbCol - 1))
            let size = (collectionView.bounds.width - totalSpace) / CGFloat(nbCol)
            let imgHeight = CGFloat(THProfilePostsManager.sharedInstance.profilePosts[indexPath.row].largeImageHeight)
            let imgWidth = CGFloat(THProfilePostsManager.sharedInstance.profilePosts[indexPath.row].largeImageWidth)
            let newHeight = (imgHeight/imgWidth)*size
            return CGSize(width: size, height: newHeight)
    }
}


