//
//  PZEditProfileTableViewController.swift
//  Threadest
//
//  Created by Jaydeep on 09/01/18.
//  Copyright © 2018 JC. All rights reserved.
//

import UIKit
import SDWebImage
import IQKeyboardManagerSwift

protocol editProfileDelegate: class {
    func openPicker(strStyle: String)
}

class PZEditProfileTableViewController: UITableViewController, UIGestureRecognizerDelegate,UIImagePickerControllerDelegate,
UINavigationControllerDelegate {
    
    @IBOutlet weak var maleBubbleView: UIView!
    @IBOutlet weak var femaleBubbleView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    
    var delegate: editProfileDelegate?
    
    var profileURL = ""
    var isMale = true
    
    //constraint outlet
    @IBOutlet weak var topMaleConstratint: NSLayoutConstraint!
    @IBOutlet weak var topFemaleConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var waistImageView: UIImageView!
    fileprivate var bubbleViews = [UIView]()
    @IBOutlet weak var bioTextView: UITextView!
    @IBOutlet weak var websiteTextField: UITextField!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var waistLabel: UILabel!
    @IBOutlet weak var neckLabel: UILabel!
    @IBOutlet weak var shirtLabel: UILabel!
    @IBOutlet weak var shoeLabel: UILabel!
    @IBOutlet weak var braLabel: UILabel!
    @IBOutlet weak var hipsSizeLabel: UILabel!
    let picker = UIImagePickerController()
    var chosenImage : UIImage!
    var selectedRowTableView:Int = -1
    var user:User = User(userId: 0, about: "", username: "", email: "", phoneNumber: "", imageURL: "",thumbnailImageURL:"" ,password: "", height: "", age: "", gender: "", shirtSize: "", waistSize: "", shoe_size: "", neckSize: "", braSize: "", hipsSize: "", cartCount: 0, closetSetupOne: false, closetSetupTwo: false, closetSetupThree: false, closetSetupComplete: false, needsToFollowBrands: false, needsToFollowUsers: false, isFollowing:false, selectedImage:nil, influencerScore: 0, sharePostCode:0, tempAccount: false, invitePromoCode: "", website: "", verifiedAccount:false)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Edit Profile"
        self.picker.delegate = self
        //self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "cart_blue_back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(PZEditProfileTableViewController.didTapOnBackButton(_:)))
        
        //self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.plain, target: self, action: #selector(PZEditProfileTableViewController.didTapOnSaveButton(_:)))
        setupUI()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func viewWillAppear(_ animated: Bool) {
//        appdelegate.verticle.scrollEnabled(isEnable: false)
        IQKeyboardManager.sharedManager().enable = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .default
        //styleSelectionPickerView.frame = CGRect(x: 0, y: screenheight - styleSelectionPickerView.frame.size.height, width: screenwidth, height: styleSelectionPickerView.frame.size.height)
        //appdelegate.window?.addSubview(styleSelectionPickerView)
    }
    
    override func viewDidLayoutSubviews() {
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
//        appdelegate.verticle.scrollEnabled(isEnable: true)
        //self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    func setupUI() {
        profileImageView.layer.cornerRadius = profileImageView.frame.size.width/2
        profileImageView.layer.masksToBounds = true
        profileImageView.layer.borderColor = UIColor.lightGray.cgColor
        profileImageView.layer.borderWidth = 1
        bubbleViews = [maleBubbleView, femaleBubbleView]
        
        bubbleViews.forEach {
            self.addShadow(to: $0)
            $0.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapBubble(_:))))
        }
        
        guard let usercurrent = THUserManager.sharedInstance.currentUserdata() else {
            return
        }
        
        bioTextView.text = usercurrent.about
        if bioTextView.text.characters.count == 0 {
            bioTextView.text = "About You"
        }
        websiteTextField.text = usercurrent.website
        heightLabel.text = usercurrent.height
        waistLabel.text = usercurrent.waistSize
        neckLabel.text = usercurrent.neckSize
        shirtLabel.text = usercurrent.shirtSize
        shoeLabel.text = usercurrent.shoe_size
        braLabel.text = usercurrent.braSize
        hipsSizeLabel.text = usercurrent.hipsSize
        switch usercurrent.gender {
        case "male":
            isMale = true
            waistImageView.image = UIImage(named: "waistmaleicon")
            selectBubble(maleBubbleView)
            self.tableView.reloadData()
        case "female":
            isMale = false
            waistImageView.image = UIImage(named: "waisticon")
            selectBubble(femaleBubbleView)
            self.tableView.reloadData()
        default:
            isMale = false
            selectBubble(maleBubbleView)
            self.tableView.reloadData()
        }
        profileImageView.sd_setImage(with: URL(string: profileURL), placeholderImage: nil, options: .retryFailed, completed: nil)
    }
    
    fileprivate func addShadow(to view: UIView) {
        view.layer.shadowOpacity = 0.1
        view.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        view.layer.shadowRadius = 5.0
        view.layer.shadowColor = UIColor.black.cgColor
    }
    
    @objc fileprivate func didTapBubble(_ recognizer: UITapGestureRecognizer) {
        let sender = recognizer.view!
        selectBubble(sender)
    }
    
    fileprivate func selectBubble(_ sender: UIView) {
        let otherViews = bubbleViews.filter { $0 != sender }
        switch sender {
        case maleBubbleView:
            isMale = true
            waistImageView.image = UIImage(named: "waistmaleicon")
            topMaleConstratint.constant = 14
            topFemaleConstraint.constant = 7
            self.tableView.reloadData()
            break
        case femaleBubbleView:
            isMale = false
            waistImageView.image = UIImage(named: "waisticon")
            topMaleConstratint.constant = 7
            topFemaleConstraint.constant = 14
            self.tableView.reloadData()
            break
        default:
            break
        }
        
        otherViews.forEach { view in
            view.layer.shadowOpacity = 0.1
            view.backgroundColor = .white
            for subview in view.subviews {
                if let subview = subview as? UILabel {
                    subview.textColor = .lightGray
                }
            }
        }
        
        sender.backgroundColor = UIColor.richElectricBlue
        sender.layer.shadowOpacity = 0.0
        for subview in sender.subviews {
            if let subview = subview as? UILabel {
                subview.textColor = .white
            }
        }
    }

    
    @IBAction func didTapOnBackButton(_ sender: Any) {
        UIApplication.shared.statusBarStyle = .lightContent
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapOnEditProfileButton(_ sender: Any) {
        let actionSheetController = UIAlertController(title:nil, message:nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelActionButton)
        
        let TakeActionButton = UIAlertAction(title: "Take from Camera", style: .default) { action -> Void in
            self.picker.allowsEditing = false
            self.picker.sourceType = .camera
            self.present( self.picker, animated: true, completion: nil)
            print("Camera")
        }
        actionSheetController.addAction(TakeActionButton)
        
        let saveActionButton = UIAlertAction(title: "Pick from Photos", style: .default) { action -> Void in
            self.picker.allowsEditing = false
            self.picker.sourceType = .photoLibrary
            self.self.present( self.picker, animated: true, completion: nil)
            print("Photos")
        }
        actionSheetController.addAction(saveActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    @IBAction func didTapOnSaveButton(_ sender: Any) {
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THUserManager.sharedInstance.performUpdateUser(with: (THUserManager.sharedInstance.currentUserdata()?.userId)!, user: user) { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            guard let error = error else {
                //Add funnel for Setup Closet
                
                return
            }
            debugPrint(error)
        }
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.profileImageView.image = chosenImage
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THNetworkManager.sharedInstance.updateUserImage(id: THUserManager.sharedInstance.currentUserdata()?.userId, image: chosenImage, completion: { (json, error) in
            guard let json = json else {
                self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "", body: (error?.localizedDescription)!, completion: {
                    
                }), animated: true, completion: nil)
                return
            }
            DispatchQueue.main.async {
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            }
            if json["status"].boolValue {
                THUserManager.sharedInstance.saveCurrentUser(currentuser:json["data"])
                DispatchQueue.main.async {
                    /*THProfilePostsManager.sharedInstance.profilePosts.forEach{ anyPost in
                     var temp = anyPost
                     temp.ownerImageUrl = (THUserManager.sharedInstance.currentUserdata()?.thumbnailImageURL)!
                     }
                     //self.retrievePostsData()
                     self.objTableView.reloadData()*/
                    THProfilePostsManager.sharedInstance.retrieveProfilePosts(with: (THUserManager.sharedInstance.currentUserdata()?.userId)!) { (error) in
                        guard error != nil else {
                            //self.retrievePostsData()
                            return
                        }
                    }
                    
                    self.profileImageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
                }
            } else {
                //                let error = NSError(domain: "Error", code: 0, userInfo: [NSLocalizedDescriptionKey : json["message"].stringValue])
            }
        }) { (progress) in
            print("progress \(progress)")
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 11
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        cell.selectionStyle = .none
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isMale {
            return (indexPath.row == 9 || indexPath.row == 10) ? 0:super.tableView(tableView, heightForRowAt: indexPath)
        } else {
            return (indexPath.row == 6 || indexPath.row == 7) ? 0:super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            break
        case 1:
            break
        case 2:
            break
        case 3:
            break
        case 4:
            delegate?.openPicker(strStyle: "height")
            break
        case 5:
            delegate?.openPicker(strStyle: "waist")
            break
        case 6:
            delegate?.openPicker(strStyle: "neck")
            break
        case 7:
            delegate?.openPicker(strStyle: "shirt")
            break
        case 8:
            delegate?.openPicker(strStyle: "shoes")
            break
        case 9:
            delegate?.openPicker(strStyle: "bra")
            break
        case 10:
            delegate?.openPicker(strStyle: "hips")
            break
        default:
            break
        }
    }
}

extension PZEditProfileTableViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "About You" {
            bioTextView.text = ""
        }
    }
}

extension PZEditProfileTableViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.tableView.setContentOffset(CGPoint(x: 0, y: 100), animated: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
         self.tableView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}



