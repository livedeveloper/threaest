//
//  THLyricsDetailViewController.swift
//  Threadest
//
//  Created by Jaydeep on 29/06/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THLyricsDetailViewController: UIViewController {

    @IBOutlet weak var lyricsTextView: UITextView!
    @IBOutlet weak var nameLabel: UILabel!
    var searchLyrics: SearchLyrics?
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        
    }
    
    func initUI() {
        nameLabel.text = searchLyrics?.artist
        lyricsTextView.dataDetectorTypes = .link
        lyricsTextView.text = searchLyrics?.lyrics
        // Range: 3 to 7
      /*  let startPosition = lyricsTextView.position(from: lyricsTextView.beginningOfDocument, offset: 3)
        
        let endPosition = lyricsTextView.position(from: lyricsTextView.beginningOfDocument, offset: 7)
        
        
        if startPosition != nil && endPosition != nil {
            lyricsTextView.//lyricsTextView.textRange(from: startPosition!, to: endPosition!)
           // lyricsTextView.selectionRects(for: !)
        }*/
    }

    
    @IBAction func didTapOnBackButton(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }

}
