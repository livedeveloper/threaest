//
//  THSearchLyricsViewController.swift
//  Threadest
//
//  Created by Jaydeep on 21/06/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import ObjectMapper
import AlgoliaSearch


class THSearchLyricsViewController: UIViewController {

    @IBOutlet weak var objtableview: UITableView!
    
    
    //Instance variable
    @IBOutlet weak var txtSearch: UITextField!
    var page:UInt = 0
    var maxNoOfPages:UInt = 0
    
    var searchedResuls:NSArray = []
    var searchQuery:THSearchQueryManager?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData()
        // Do any additional setup after loading the view.
    }
    
    private func setupData() {
        objtableview.register(UINib(nibName: THSearchLyricsTableViewCell.className, bundle: nil), forCellReuseIdentifier: THSearchLyricsTableViewCell.className)
        objtableview.rowHeight = 101;
        self.objtableview.keyboardDismissMode = .onDrag
        self.searchQuery = THSearchQueryManager()
        searchResults(search: txtSearch.text!, searchType: "Song")
        loadMore()
    }
    
    func loadMore() {
        self.objtableview.addInfiniteScrolling {
            self.page+=1
            if (self.page  >= self.maxNoOfPages) {
                self.objtableview.infiniteScrollingView.stopAnimating();
                return // All pages already loaded
            } else {
                self.fetchMoreResults()
            }
        }
    }

    
    
    //MARK: Search result from algolia
    private func fetchMoreResults() {
        self.searchQuery?.query?.page = self.page
        self.searchQuery?.query?.filters = ""
        self.searchQuery?.queryResults(search: txtSearch.text!, searchType: "Song") { (searchList, nbPage) in
            let moreResults:NSArray = searchList as! NSArray
            let finalArray:NSMutableArray = []
            for (index, element) in self.searchedResuls.enumerated() {
                print("Item \(index): \(element)")
                finalArray.add(element)
            }
            for (index, element) in moreResults.enumerated() {
                print("Item \(index): \(element)")
                finalArray.add(element)
            }
            self.searchedResuls = finalArray as NSArray
            self.maxNoOfPages = nbPage
            self.objtableview.reloadData()
            self.objtableview.infiniteScrollingView.stopAnimating();
        }
    }
    
    func searchResults(search:String, searchType:String){
        self.page = 0
        self.searchQuery?.query?.page = self.page
        self.searchQuery?.query?.filters = ""
        self.searchQuery?.queryResults(search: search, searchType: searchType) { (searchList, nbPage) in
            self.searchedResuls = []
            self.searchedResuls = searchList as! NSArray
            if(self.searchedResuls.count>0) {
                self.objtableview.isHidden = false
            } else {
                self.objtableview.isHidden = true
            }
            self.maxNoOfPages = nbPage
            self.objtableview.reloadData()
        }
    }

    @IBAction func didTapOncloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension THSearchLyricsViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let search = textFieldText.replacingCharacters(in: range, with: string)
        self.loadMore()
        searchResults(search: search, searchType: "Song")
        return true;
    }
}


extension THSearchLyricsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let lyricsObj = self.searchedResuls[indexPath.row] as! SearchLyrics
        
        let lyricsDescriptionVC = THLyricsDetailViewController(nibName: THLyricsDetailViewController.className, bundle: nil)
        lyricsDescriptionVC.searchLyrics = lyricsObj
        self.navigationController?.pushViewController(lyricsDescriptionVC, animated: true)
    }
}

extension THSearchLyricsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.searchedResuls as AnyObject).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: THSearchLyricsTableViewCell.className, for: indexPath) as! THSearchLyricsTableViewCell
        cell.selectionStyle = .none
        let lyricsObj = self.searchedResuls[indexPath.row] as! SearchLyrics
        cell.configure(searchLyrics: lyricsObj)
        return cell
    }
}
