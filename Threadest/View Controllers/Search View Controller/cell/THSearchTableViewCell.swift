//
//  THSearchTableViewCell.swift
//  Threadest
//
//  Created by Synnapps on 17/03/2017.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THSearchTableViewCell: UITableViewCell {
    @IBOutlet weak var imgSearchResult: UIImageView!
    @IBOutlet weak var lblSearchDescription: UILabel!
    @IBOutlet weak var lblSearchType: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func setObject(obj:Any) {
        if obj is SearchUser {
            self.setUserObj(user: obj as! SearchUser)
        } else if obj is SearchProduct {
            self.setProductObj(product: obj as! SearchProduct)
        } else if obj is SearchBrand {
            self.setBrandObj(brand: obj as! SearchBrand)
        }
    }
    
    private func setUserObj(user:SearchUser) {
        self.lblSearchType.text = "User"
        self.lblSearchDescription.text = user.username
        if(user.thumbnail_image_url != nil) {
            self.imgSearchResult.sd_setImage(with: NSURL(string:user.thumbnail_image_url) as URL?)
        }
    }
    
    private func setProductObj(product:SearchProduct) {
        self.lblSearchType.text = "Fulfilled by: \(product.brandName ?? "")\n\(String(format: "$%.2f", product.current_price!))\nFree Shipping"
        self.lblSearchDescription.text = product.title
        if(product.default_thumbnail_image_url != nil) {
            imgSearchResult.sd_setImage(with: NSURL(string:product.default_thumbnail_image_url) as URL?)
        }
    }
    
    private func setBrandObj(brand:SearchBrand) {
        self.lblSearchType.text = "Brand"
        self.lblSearchDescription.text = brand.name
        if let brandLogo = brand.brand_logo_image_url {
            self.imgSearchResult.sd_setImage(with: URL(string: brandLogo), placeholderImage: nil, options: .retryFailed, completed: nil)
        }
    }
}


