//
//  THSearchLyricsTableViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 29/06/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SDWebImage


class THSearchLyricsTableViewCell: UITableViewCell {
    @IBOutlet weak var albumImageView: UIImageView!

    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var lyricsName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configure(searchLyrics: SearchLyrics) {
        albumImageView.sd_setImage(with: URL(string: searchLyrics.artwork!), placeholderImage: nil, options: .retryFailed)
        artistName.text = searchLyrics.artist
        lyricsName.text = searchLyrics.title
    }
    
}
