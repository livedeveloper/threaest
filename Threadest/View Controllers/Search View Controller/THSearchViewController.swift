
//
//  THSearchViewController.swift
//  Threadest
//
//  Created by Synnapps on 15/03/2017.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import AlgoliaSearch
import ObjectMapper
import Crashlytics
import IoniconsSwift
import ARKit

class THSearchViewController: UIViewController, UITextFieldDelegate {

    //Outlet
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var btnAll: DesignableButton!
    @IBOutlet weak var btnUsers: DesignableButton!
    @IBOutlet weak var btnProduct: DesignableButton!
    @IBOutlet weak var btnBrand: DesignableButton!
    @IBOutlet weak var viewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var objtableview: UITableView!
    
    @IBOutlet weak var viewAddBrand: UIView!
    @IBOutlet weak var plusButton: UIButton!
    //Instance Variables
    var btnsArray:[DesignableButton] = []
    var page:UInt = 0
    var maxNoOfPages:UInt = 0
    var maxNoOfPagesAll:UInt = 0
    var pageAll:UInt = 0
    var searchedResuls:NSArray = []
    var searchQuery:THSearchQueryManager?
    var selectedSearchType:String?
    public var isTagging = false
    let kStringConstantAll:String = "All"
    var postTagCompletion : PostTagCompletion?

    // MARK: - UIViewController Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.txtSearch.becomeFirstResponder()
        NotificationCenter.default.addObserver(self, selector: #selector(THSearchViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(THSearchViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        initUI()
        appendBtnsInArray()
        
        if self.isTagging{
            self.didTapType(btnProduct)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    private func initUI() {
        objtableview.register(UINib(nibName: THSearchTableViewCell.className, bundle: nil), forCellReuseIdentifier: THSearchTableViewCell.className)
        objtableview.rowHeight = 101;
        self.objtableview.keyboardDismissMode = .onDrag
        self.searchQuery = THSearchQueryManager()
        self.selectedSearchType = "All"
        loadMore()
    }
    
    // MARK: - Search Method
    
    @IBAction func didTapOnPlusButton(_ sender: Any) {
        let BrandVC = THBlackBrandViewController(nibName: THBlackBrandViewController.className, bundle: nil)
        BrandVC.isMyBrand = "my"
        BrandVC.isAddProduct = true
        self.present(BrandVC, animated: true, completion: nil)
    }
    
    private func searchResults(search:String, searchType:String){
        self.page = 0
        self.searchQuery?.query?.page = self.page
        self.searchQuery?.query?.filters = ""
        self.searchQuery?.queryResults(search: search, searchType: searchType) { (searchList, nbPage) in
            self.searchedResuls = []
            self.searchedResuls = searchList as! NSArray
            if(self.searchedResuls.count>0) {
                self.objtableview.isHidden = false
                self.viewAddBrand.isHidden = true
            } else {
                if searchType == "Brand" {
                    self.viewAddBrand.isHidden = false
                } else {
                    self.viewAddBrand.isHidden = true
                }
                self.objtableview.isHidden = true
            }
            self.maxNoOfPages = nbPage
            self.objtableview.reloadData()
        }
    }
    
    private func searchResultsAll(search:String){
        self.pageAll = 0
        self.searchQuery?.indexQueryUser?.query.page = self.pageAll
        self.searchQuery?.indexQueryProduct?.query.page = self.pageAll
        self.searchQuery?.indexQueryBrand?.query.page = self.pageAll
        
        self.searchQuery?.queryResultsAll(search: search, userPage: true, productPage: true, brandPage: true) { (searchList, nbPageUser, nbPageProduct, nbPageBrand) in
            self.searchedResuls = []
            self.searchedResuls = searchList as! NSArray
            if(self.searchedResuls.count>0) {
                self.objtableview.isHidden = false
            } else {
                self.objtableview.isHidden = true
            }
            self.objtableview.reloadData()
            let pagesArray = [nbPageUser, nbPageProduct, nbPageBrand]
            self.maxNoOfPagesAll = pagesArray.max()!
        }
    }
     
    private func fetchMoreResultsAll() {
        self.searchQuery?.indexQueryUser?.query.page = self.pageAll;
        self.searchQuery?.indexQueryProduct?.query.page = self.pageAll;
        self.searchQuery?.indexQueryBrand?.query.page = self.pageAll;
        
        self.searchQuery?.queryResultsAll(search: self.txtSearch.text!, userPage: true, productPage: true, brandPage: true) { (searchList, nbPageUser, nbPageProduct, nbPageBrand) in
            let moreResults:NSArray = searchList as! NSArray
            let finalArray:NSMutableArray = []
            for (index, element) in self.searchedResuls.enumerated() {
                print("Item \(index): \(element)")
                finalArray.add(element)
            }
            for (index, element) in moreResults.enumerated() {
                print("Item \(index): \(element)")
                finalArray.add(element)
            }
            self.searchedResuls = finalArray as NSArray
            let pagesArray = [nbPageUser, nbPageProduct, nbPageBrand]
            self.maxNoOfPagesAll = pagesArray.max()!
            self.objtableview.reloadData()
            self.objtableview.infiniteScrollingView.stopAnimating();
        }
    }
    
    private func fetchMoreResults() {
        self.searchQuery?.query?.page = self.page
        self.searchQuery?.query?.filters = ""
        self.searchQuery?.queryResults(search: self.txtSearch.text!, searchType: self.selectedSearchType!) { (searchList, nbPage) in
            let moreResults:NSArray = searchList as! NSArray
            let finalArray:NSMutableArray = []
            for (index, element) in self.searchedResuls.enumerated() {
                print("Item \(index): \(element)")
                finalArray.add(element)
            }
            for (index, element) in moreResults.enumerated() {
                print("Item \(index): \(element)")
                finalArray.add(element)
            }
            self.searchedResuls = finalArray as NSArray
            self.maxNoOfPages = nbPage
            self.objtableview.reloadData()
            self.objtableview.infiniteScrollingView.stopAnimating();
        }
    }
    
    private func appendBtnsInArray() {
        btnsArray = [btnAll, btnUsers, btnProduct, btnBrand];
    }
    
    // MARK: - Action Methods
    @IBAction func didTapOnDismiss(_ sender: Any) {
        self.view.endEditing(true);
        dismiss(animated: true) { 
            
        }
    }
    
    @IBAction func didTapType(_ sender: Any) {
        let btnBackgroudColor = UIColor(red: 0/255, green: 138/255, blue: 208/255, alpha: 1.0)
        
        let tappedBtn = sender as! DesignableButton
        for btn in btnsArray {
            if(tappedBtn.tag == btn.tag) {
                btn.alpha = 1.0;
                btn.setTitleColor(UIColor.white, for: .normal)
                btn.backgroundColor = btnBackgroudColor
                btn.titleLabel?.font = UIFont(name: "SFUIText-Semibold", size: 13)
                self.selectedSearchType = btn.titleLabel?.text
            } else {
                btn.setTitleColor(UIColor.black, for: .normal)
                btn.alpha = 0.4;
                btn.backgroundColor = UIColor.white
                btn.titleLabel?.font = UIFont(name: "SFUIText-Regular", size: 13)
            }
        }
        if(self.selectedSearchType == kStringConstantAll) {
            self.searchResultsAll(search: self.txtSearch.text!)
        }
        else {
            self.searchResults(search: self.txtSearch.text!, searchType: self.selectedSearchType!)
        }
    }
    
    @IBAction func didTapOnAddBrand(_ sender: Any) {
        self.view.endEditing(true)
        let brandpopup = THAddBrandPopupViewController(nibName: THAddBrandPopupViewController.className, bundle: nil)
        brandpopup.newBrandcompletionHander = {
            brandID in
            /*if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Selection of Brand Tag", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "brand_id":brandID, "brand_name":brand.name ?? ""])
            }*/
            let postTag = PostTag(postId: brandID, tagType: "Brand Tag")
            if self.postTagCompletion != nil{
                self.postTagCompletion!(postTag)
            }
        }
        self.addChildViewController(brandpopup)
        brandpopup.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
        self.view.addSubview(brandpopup.view)
        brandpopup.didMove(toParentViewController: self)
    }
    
    
    // MARK: - Custom Methods
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.viewBottomConstraint.constant += keyboardSize.height
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.bottomView.frame.origin.y != 0{
                if(self.viewBottomConstraint.constant>5) {
                    self.viewBottomConstraint.constant -= keyboardSize.height
                }
            }
        }
    }
    
    private func loadMore() {
        self.objtableview.addInfiniteScrolling {
            if(self.selectedSearchType == self.kStringConstantAll) {
                self.pageAll+=1
                if (self.pageAll  >= self.maxNoOfPagesAll) {
                    self.objtableview.infiniteScrollingView.stopAnimating();
                    return // All pages already loaded
                } else {
                    self.fetchMoreResultsAll()
                }
            } else {
                self.page+=1
                if (self.page  >= self.maxNoOfPages) {
                    self.objtableview.infiniteScrollingView.stopAnimating();
                    return // All pages already loaded
                } else {
                    self.fetchMoreResults()
                }
            }
        }
    }
    
    // MARK: - UITextFieldDelegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let search = textFieldText.replacingCharacters(in: range, with: string)
        self.loadMore()
        if(self.selectedSearchType == kStringConstantAll) {
            self.searchResultsAll(search: search)
        }
        else {
            self.searchResults(search: search, searchType: self.selectedSearchType!)
        }
        return true;
    }
}

// MARK: - TableView Delegate Methods
extension THSearchViewController:UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tappedObj = self.searchedResuls[indexPath.row]
        
        if tappedObj is SearchProduct && self.isTagging {
            //Add funnel for Successfully selection of product
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                let product = tappedObj as! SearchProduct
                Answers.logCustomEvent(withName: "Selectio of product", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "product_id":"\(product.objectID!)", "product_name":product.title ?? "", "brand_name": product.brandName ?? ""])
            }
            let postTag = PostTag(postId: (tappedObj as! SearchProduct).objectID!, tagType: "Product Tag")
            if self.postTagCompletion != nil {
                self.postTagCompletion!(postTag)
            }
        }
        else if tappedObj is SearchProduct && !self.isTagging {
        
            THBrandProductManager.sharedInstance.retrieveBrandProfile(with:(tappedObj as! SearchProduct).brand_id!, productId:Int((tappedObj as! SearchProduct).objectID!)!, geofence: false) { (error) in
                let threeDAvailable = THBrandProductManager.sharedInstance.brandProduct?.productGalleryImages.contains(where: { (imgGallery) -> Bool in
                    if imgGallery.image_type == "3D" {
                        return true
                    } else {
                        return false
                    }
                })
                
                if threeDAvailable! {
                    if #available(iOS 11.0, *) {
                        if (ARConfiguration.isSupported) {
                            let ARShopShowController = THARShopShowViewController(nibName: THARShopShowViewController.className, bundle: nil)
                            ARShopShowController.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                            self.navigationController?.pushViewController(ARShopShowController, animated: true)
                            return
                        }
                    }
                }
                let shopshowcontroller = THShopshowViewController(nibName: THShopshowViewController.className, bundle: nil)
                shopshowcontroller.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                self.navigationController?.pushViewController(shopshowcontroller, animated: true)
            }
        }
        else if tappedObj is SearchUser {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let userPostProfileVC = storyBoard.instantiateViewController(withIdentifier: PZProfileTableViewController.className) as! PZProfileTableViewController
            userPostProfileVC.findUser = (tappedObj as! SearchUser)
            userPostProfileVC.isSearch = true
            userPostProfileVC.userId = Int((tappedObj as! SearchUser).objectID!)
            self.navigationController?.pushViewController(userPostProfileVC, animated: true)
        } else if tappedObj is SearchBrand && self.isTagging {
            let brand = tappedObj as! SearchBrand
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Selection of Brand Tag", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "brand_id":"\(brand.objectID!)", "brand_name":brand.name ?? ""])
            }
            let postTag = PostTag(postId: brand.objectID!, tagType: "Brand Tag")
            if self.postTagCompletion != nil{
                self.postTagCompletion!(postTag)
            }
        } else if tappedObj is SearchBrand && !self.isTagging {
            let brandProfileVC:THBrandProfileViewController = THBrandProfileViewController()
            brandProfileVC.brand = (tappedObj as! SearchBrand)
           self.navigationController?.pushViewController(brandProfileVC, animated: true)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - TableView DataSource Methods
extension THSearchViewController:UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.searchedResuls as AnyObject).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: THSearchTableViewCell.className, for: indexPath) as! THSearchTableViewCell
        let obj = self.searchedResuls[indexPath.row]
        cell.setObject(obj: obj)
        return cell
    }
}
