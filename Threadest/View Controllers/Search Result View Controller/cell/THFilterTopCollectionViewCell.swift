//
//  THFilterTopCollectionViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 22/11/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THFilterTopCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var filterTypeLabel: UILabel!
    @IBOutlet weak var filterNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
