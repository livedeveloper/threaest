//
//  THSearchPopularTableViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 21/11/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THSearchPopularTableViewCell: UITableViewCell {

    @IBOutlet weak var popularLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
