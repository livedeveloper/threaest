//
//  THSearchResultTableViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 27/07/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit


class THSearchResultTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var searchImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureBrand(searchBrand: SearchBrand) {
        nameLabel.text = searchBrand.name
        if let brandLogo = searchBrand.brand_logo_image_url {
            searchImageView.sd_setImage(with: URL(string: brandLogo), placeholderImage: nil, options: .retryFailed)
        }
    }
    
    func configureProduct(searchProduct: SearchProduct) {
        nameLabel.text = searchProduct.title
        searchImageView.sd_setImage(with: URL(string: searchProduct.default_thumbnail_image_url), placeholderImage: nil, options: .retryFailed)
    }
    
    func configureUser(searchUser: SearchUser) {
        nameLabel.text = searchUser.username
        if let profileUrl = searchUser.thumbnail_image_url {
        searchImageView.sd_setImage(with: URL(string: profileUrl), placeholderImage: nil, options: .retryFailed)
        } else {
            searchImageView.image = nil
        }
    }
}
