//
//  THFilterLeftTableViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 23/11/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THFilterLeftTableViewCell: UITableViewCell {

    @IBOutlet weak var saparatorLabel: UILabel!
    @IBOutlet weak var filterNameLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        countLabel.layer.cornerRadius = countLabel.frame.size.width/2
        countLabel.layer.borderColor = UIColor.hex("178dcd", alpha: 1).cgColor
        countLabel.layer.borderWidth = 1
        countLabel.layer.masksToBounds = true
    }
}
