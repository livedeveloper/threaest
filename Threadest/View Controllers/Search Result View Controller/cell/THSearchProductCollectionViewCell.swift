//
//  THSearchProductCollectionViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 22/11/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SDWebImage

class THSearchProductCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var brandNameLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    
    @IBOutlet weak var priceLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(closetCategory: ClosetCategory) {
         productImageView.sd_setImage(with: URL(string: closetCategory.defaultImageUrl), placeholderImage: nil, options: .retryFailed, completed: nil)
        
        productImageView.backgroundColor = UIColor.gray.withAlphaComponent(0.4)
        productImageView.layer.cornerRadius = 5
        productImageView.layer.masksToBounds = true
        brandNameLabel.text = closetCategory.brandName
        productNameLabel.text = closetCategory.title
        
        if closetCategory.salePrice == 0.0 {
            priceLabel.attributedText = NSAttributedString()
            priceLabel.text = String(format: " $%.2f    ", closetCategory.defaultPrice)
        } else {
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(format: " $%.2f ", closetCategory.defaultPrice))
            attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
            let salePriceString: NSMutableAttributedString = NSMutableAttributedString(string: String(format: "  $%.2f    ", closetCategory.salePrice))
            
            salePriceString.addAttribute(NSForegroundColorAttributeName, value: UIColor.hexa("178dcd", alpha: 1), range: NSMakeRange(0, salePriceString.length))
            salePriceString.addAttribute(NSFontAttributeName, value: UIFont.AppFontBold(12), range: NSMakeRange(0, salePriceString.length))
            
            attributeString.append(salePriceString)
            priceLabel.text = ""
            priceLabel.attributedText = attributeString
        }
    }
    
    func configure(searchProduct: SearchProduct) {
        if searchProduct.default_medium_image_url != nil {
        productImageView.sd_setImage(with: URL(string: searchProduct.default_medium_image_url), placeholderImage: nil, options: .retryFailed, completed: nil)
        }
        productImageView.backgroundColor = UIColor.gray.withAlphaComponent(0.4)
        productImageView.layer.cornerRadius = 5
        productImageView.layer.masksToBounds = true
        brandNameLabel.text = searchProduct.brandName
        productNameLabel.text = searchProduct.title
        
        if searchProduct.sale_price == nil {
            priceLabel.attributedText = NSAttributedString()
            priceLabel.text = String(format: " $%.2f    ", searchProduct.default_price!)
        } else {
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(format: " $%.2f ", searchProduct.default_price!))
            attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
            let salePriceString: NSMutableAttributedString = NSMutableAttributedString(string: String(format: "  $%.2f    ", searchProduct.sale_price!))
            
            salePriceString.addAttribute(NSForegroundColorAttributeName, value: UIColor.hexa("178dcd", alpha: 1), range: NSMakeRange(0, salePriceString.length))
            salePriceString.addAttribute(NSFontAttributeName, value: UIFont.AppFontBold(12), range: NSMakeRange(0, salePriceString.length))
            
            attributeString.append(salePriceString)
            priceLabel.text = ""
            priceLabel.attributedText = attributeString
        }
    }

}
