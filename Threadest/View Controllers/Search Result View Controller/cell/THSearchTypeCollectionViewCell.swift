//
//  THSearchTypeCollectionViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 27/07/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THSearchTypeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var saparatorLabel: UILabel!
    
    @IBOutlet weak var imageCategories: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        imageCategories.layer.cornerRadius = imageCategories.frame.size.width/2
        imageCategories.layer.masksToBounds = true
        // Initialization code
    }
}
