//
//  THFilterNewViewController.swift
//  Threadest
//
//  Created by Jaydeep on 23/11/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import AlgoliaSearch
import NMRangeSlider

class THFilterNewViewController: UIViewController {
    
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchHeight: NSLayoutConstraint!
    @IBOutlet weak var filterLeftTableView: UITableView!
    @IBOutlet weak var filterRightTableView: UITableView!
    @IBOutlet weak var searchBrandTextField: UITextField!
    @IBOutlet weak var searchBrandLabel: UILabel!
    
    @IBOutlet weak var startFilterPrice: UITextField!
    @IBOutlet weak var lastFilterPrice: UITextField!
    
    @IBOutlet weak var priceRangeSliderView: NMRangeSlider!
    
    weak var delegate: filterDelegate?
    
    let categoryOfFilter = ["Gender","Category","Brand","Price"]
    var gender = ["Men", "Women"]
    var selectedGender = ["Men"]
    var selectedCategory = [String]()
    var selectedBrand = [String]()
    var selectedPrice = [0,5000]
    
    var selectedCategoryIndex = 0
    
    var searchText = ""
    var page:UInt = 0
    var searchedResuls:NSArray = []
    var searchQuery:THSearchQueryManager?
    var index:Index?
    var query:Query?
    
    var category = [String]()
    var filterCategory = [String]()
    
    var searchBrand = [SearchBrand]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        filterLeftTableView.register(UINib(nibName: THFilterLeftTableViewCell.className, bundle: nil), forCellReuseIdentifier: THFilterLeftTableViewCell.className)
        filterRightTableView.register(UINib(nibName: THFilterRightTableViewCell.className, bundle: nil), forCellReuseIdentifier: THFilterRightTableViewCell.className)
        
        self.filterRightTableView.keyboardDismissMode = .onDrag
        self.searchQuery = THSearchQueryManager()
        self.searchResultsLoadMore(search: "", searchType: "Brand")
        self.loadMore()
        configurePriceSlider()
        self.showHideSearchBrand(isShow: selectedCategoryIndex == 2 || selectedCategoryIndex == 1 ? true:false)
        self.showHidePriceView(isShow: selectedCategoryIndex == 3 ? true:false)
        self.configureCategory()
      
    }
    
    func searchResultsLoadMore(search:String, searchType:String){
        self.filterRightTableView.reloadData()
        self.searchQuery?.query?.page = self.page
        self.searchQuery?.query?.filters = ""
        self.searchQuery?.queryResults(search: search, searchType: searchType) { (searchList, nbPage) in
            self.searchedResuls = []
            self.searchedResuls = searchList as! NSArray
            if(self.searchedResuls.count>0) {
                for (_, element) in self.searchedResuls.enumerated() {
                    if element is SearchBrand {
                        self.searchBrand.append(element as! SearchBrand)
                    }
                }
            }
            self.filterRightTableView.reloadData()
            self.filterRightTableView.infiniteScrollingView.stopAnimating();
        }
    }
    
    func loadMore() {
        self.filterRightTableView.addInfiniteScrolling {
            if self.selectedCategoryIndex != 2 {
                self.filterRightTableView.infiniteScrollingView.stopAnimating()
                return
            }
            self.page+=1
            self.searchResultsLoadMore(search: self.searchText, searchType: "Brand")
        }
    }
    
    private func configurePriceSlider() {
        self.priceRangeSliderView.minimumValue = 0
        self.priceRangeSliderView.maximumValue = 5000
        self.priceRangeSliderView.lowerValue = 0
        self.priceRangeSliderView.upperValue = 5000
        self.priceRangeSliderView.minimumRange = 100
        self.priceRangeSliderView.stepValue = 100
    }
    
    // MARK: - Action Methods
    
    @IBAction func didChangeSliderValue(_ sender: Any) {
        selectedPrice[0] = Int(self.priceRangeSliderView.lowerValue)
        selectedPrice[1] = Int(self.priceRangeSliderView.upperValue)
        startFilterPrice.text = "\(Int(self.priceRangeSliderView.lowerValue))"
        lastFilterPrice.text = "\(Int(self.priceRangeSliderView.upperValue))"
    }
    
    @IBAction func didTapOnCloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func didTapOnApplyButton(_ sender: Any) {
        delegate?.filterData(gender: selectedGender, brand: selectedBrand, category: selectedCategory, price: selectedPrice)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    func configureCategory() {
        if selectedGender.count == 0 || selectedGender.count == 2 {
            category = ["Shirt", "Shoes","Watch", "Bracelet","Sweater", "Pants", "Coat","Jacket" ,"Blazer", "Suit","Dress","Hat","Jewelry","Blouse", "Watch", "Purse", "Handbag", "Heel", "Skirt","Earrings", "Bag"]
        } else if selectedGender[0] == "Men" {
            category = ["Shirt","Watch", "Bracelet","Sweater", "Pants", "Coat","Jacket" ,"Blazer", "Suit"]
        } else {
             category = ["Dress","Hat","Shirt","Jewelry","Bracelet","Blouse","Sweater", "Watch", "Purse", "Handbag", "Heel", "Skirt", "Earrings", "Bag"]
        }
        filterCategory = category
    }
    
    
    func showHideSearchBrand(isShow:Bool) {
        if selectedCategoryIndex == 2 {
            searchBrandTextField.placeholder = "Search Brands"
            searchBrandLabel.text = "TOP BRANDS"
        } else if selectedCategoryIndex == 1 {
            searchBrandTextField.placeholder = "Search Category"
            searchBrandLabel.text = "TOP CATEGORY"
        }
        if isShow {
            searchView.isHidden = false
            searchHeight.constant = 75
        } else {
            searchView.isHidden = true
            searchHeight.constant = 0
        }
    }
    
    func showHidePriceView(isShow:Bool) {
        if isShow {
            priceView.isHidden = false
            searchView.isHidden = true
        } else {
            priceView.isHidden = true
            searchView.isHidden = false
        }
    }
    
}


extension THFilterNewViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == filterLeftTableView {
            selectedCategoryIndex = indexPath.row
            filterLeftTableView.reloadData()
            filterRightTableView.reloadData()
            self.showHideSearchBrand(isShow: selectedCategoryIndex == 2 || selectedCategoryIndex == 1 ? true:false)
            self.showHidePriceView(isShow: selectedCategoryIndex == 3 ? true:false)
        } else {
            let filterRight = tableView.cellForRow(at: indexPath) as! THFilterRightTableViewCell
            switch selectedCategoryIndex {
                case 0:
                    let strgender = gender[indexPath.row]
                    if selectedGender.contains(strgender) {
                        selectedGender.remove(at: selectedGender.index(of: strgender)!)
                        filterRight.radioButton.isSelected = false
                    } else {
                        selectedGender.append(strgender)
                        filterRight.radioButton.isSelected = true
                    }
                    configureCategory()
                    break
                case 1:
                    let strCategory = category[indexPath.row]
                    if selectedCategory.contains(strCategory) {
                        selectedCategory.remove(at: selectedCategory.index(of: strCategory)!)
                        filterRight.radioButton.isSelected = false
                    } else {
                        selectedCategory.append(strCategory)
                        filterRight.radioButton.isSelected = true
                    }
                    break
                case 2:
                    let strBrand = searchBrand[indexPath.row].name
                    if selectedBrand.contains(strBrand!) {
                        if let i = selectedBrand.index(of: strBrand!) {
                            selectedBrand.remove(at: i)
                        }
                        filterRight.radioButton.isSelected = false
                    } else {
                        selectedBrand.append(strBrand!)
                        filterRight.radioButton.isSelected = true
                    }
                    break
                default:
                    break
            }
        }
    }
}

extension THFilterNewViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == filterLeftTableView {
            return categoryOfFilter.count
        } else {
            switch selectedCategoryIndex {
                case 0:
                    return gender.count
                case 1:
                    return category.count
                case 2:
                    return searchBrand.count
                default:
                    return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == filterLeftTableView {
            let filterLeftCell = tableView.dequeueReusableCell(withIdentifier: THFilterLeftTableViewCell.className, for: indexPath) as! THFilterLeftTableViewCell
            filterLeftCell.filterNameLabel.text = categoryOfFilter[indexPath.row]
            filterLeftCell.selectionStyle = .none
            if selectedCategoryIndex == indexPath.row {
                filterLeftCell.saparatorLabel.isHidden = false
                filterLeftCell.backgroundColor = UIColor.white
            } else {
                filterLeftCell.saparatorLabel.isHidden = true
                filterLeftCell.backgroundColor = UIColor.custColor(r: 246, g: 247, b: 248, a: 1)
            }
            filterLeftCell.countLabel.isHidden = true
            switch indexPath.row {
            case 1:
                filterLeftCell.countLabel.isHidden = selectedCategory.count > 0 ? false:true
                filterLeftCell.countLabel.text = "\(selectedCategory.count)"
                break
            case 2:
                filterLeftCell.countLabel.isHidden = selectedBrand.count > 0 ? false:true
                filterLeftCell.countLabel.text = "\(selectedBrand.count)"
                break
            default:
                filterLeftCell.countLabel.isHidden = true
                break
            }
            return filterLeftCell
        } else {
            let filterRightCell = tableView.dequeueReusableCell(withIdentifier: THFilterRightTableViewCell.className, for: indexPath) as! THFilterRightTableViewCell
            filterRightCell.selectionStyle = .none
            switch selectedCategoryIndex {
            case 0:
                filterRightCell.nameLabel.text = gender[indexPath.row]
                if selectedGender.contains(gender[indexPath.row]) {
                    filterRightCell.radioButton.isSelected = true
                } else {
                    filterRightCell.radioButton.isSelected = false
                }
                break
            case 1:
                filterRightCell.nameLabel.text = GeneralMethods.sharedInstance.getPural(strCategory: category[indexPath.row])
                if selectedCategory.contains(category[indexPath.row]) {
                    filterRightCell.radioButton.isSelected = true
                } else {
                    filterRightCell.radioButton.isSelected = false
                }
                break
            case 2:
                if searchBrand[indexPath.row].products_count == nil {
                    filterRightCell.nameLabel.text = searchBrand[indexPath.row].name
                } else {
                    filterRightCell.nameLabel.text = String(format: "%@ (%d)", searchBrand[indexPath.row].name!, searchBrand[indexPath.row].products_count!)
                }
                if selectedBrand.contains(searchBrand[indexPath.row].name!) {
                    filterRightCell.radioButton.isSelected = true
                } else {
                    filterRightCell.radioButton.isSelected = false
                }
                break
            default:
                break
            }
            return filterRightCell
        }
    }
}

extension THFilterNewViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case startFilterPrice:
            return true
        case lastFilterPrice:
            return true
        case searchBrandTextField:
            textField.resignFirstResponder()
            return true
        default:
            return true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == startFilterPrice {
            let allowedCharacters = CharacterSet.decimalDigits
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }
        
        if textField == lastFilterPrice {
            let allowedCharacters = CharacterSet.decimalDigits
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }
        if textField == searchBrandTextField {
            let textFieldText: NSString = (textField.text ?? "") as NSString
            searchText = textFieldText.replacingCharacters(in: range, with: string)
            self.loadMore()
            switch selectedCategoryIndex {
            case 1:
                configureCategory()
                //let predicate = NSPredicate(format: "ANY CONTAINS %@", searchText)
                //self.category = (filterCategory as NSArray).filtered(using: predicate) as! [String]
                self.category = filterCategory.filter {
                    return $0.range(of: searchText, options: .caseInsensitive) != nil
                }
                if searchText == "" {
                    self.category = filterCategory
                }
                filterRightTableView.reloadData()
                break
            case 2:
                self.searchBrand = [SearchBrand]()
                self.page = 0
                self.searchResultsLoadMore(search: searchText, searchType: "Brand")
            case 3:
                break
            default:
                break
            }
        } else {
            
        }
        return true
    }
}

protocol filterDelegate: class {
    func filterData(gender: Array<String>,brand: Array<String>, category: Array<String>, price: Array<Int>)
}
