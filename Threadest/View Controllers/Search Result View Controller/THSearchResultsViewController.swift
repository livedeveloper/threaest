//
//  THSearchResultsViewController.swift
//  Threadest
//
//  Created by Synnapps on 17/03/2017.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SVPullToRefresh
import AlgoliaSearch
import ObjectMapper
import ARKit

class THSearchResultsViewController: UIViewController {
    
    //IBOutlets
    @IBOutlet weak var viewFilter: UIView!
    @IBOutlet weak var objtableview: UITableView!
    @IBOutlet weak var lblSearchTitle: UILabel!
    
    //Instance Variables
    var strSearchTitle:String = ""
    var searchQuery:THSearchQueryManager?
    var searchedResuls:NSArray = []
    var page:UInt = 0
    var maxNoOfPages:UInt = 0
    var index:Index?
    var query:Query?
    
    var minPrice:Int?
    var maxPrice: Int?
    var brandId: Int?
    
    // MARK: - UIViewController Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        // Do any additional setup after loading the view.
        initUI()
        loadMore()
    }
    
    private func loadMore() {
       // self.objtableview.showsInfiniteScrolling = true
        self.objtableview.addInfiniteScrolling {
            self.page+=1
            if (self.page  >= self.maxNoOfPages) {
                self.objtableview.showsInfiniteScrolling = false;
                return // All pages already loaded
            } else {
                //self.query?.page = self.page
                self.fetchMoreResults()
            }
        }
    }
    
    private func initUI() {
        lblSearchTitle.text = String(format: "Result \"%@\"",strSearchTitle)
        viewFilter.layer.cornerRadius = viewFilter.frame.size.width/2;
        viewFilter.layer.masksToBounds = true
        objtableview.register(UINib(nibName: THSearchTableViewCell.className, bundle: nil), forCellReuseIdentifier: THSearchTableViewCell.className)
        objtableview.rowHeight = 101;
        self.searchQuery = THSearchQueryManager()
        //self.tabBarController?.tabBar.isHidden = true
    }
    
    private func fetchMoreResults() {
        self.searchQuery?.query?.page = self.page
        self.searchQuery?.filtersQueryREsult(search: strSearchTitle, brandID: Int(brandId!), minPrice:Int(minPrice!), maxPrice:Int(maxPrice!), closetCategory: "", gender: "", completionBlock: { (searchList, nbPage) in
            print(searchList)
            let moreResults:NSArray = searchList as! NSArray
            let finalArray:NSMutableArray = []
            for (index, element) in self.searchedResuls.enumerated() {
                print("Item \(index): \(element)")
                finalArray.add(element)
            }
            for (index, element) in moreResults.enumerated() {
                print("Item \(index): \(element)")
                finalArray.add(element)
            }

            self.searchedResuls = finalArray as NSArray
            self.maxNoOfPages = nbPage
            self.objtableview.reloadData()
            self.objtableview.infiniteScrollingView.stopAnimating();
        })
    }

    // MARK: - Action Methods 
    
    @IBAction func didTapBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}

// MARK: - TableView Delegate Methods
extension THSearchResultsViewController:UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        let product:SearchProduct = self.searchedResuls[indexPath.row] as! SearchProduct
        THBrandProductManager.sharedInstance.retrieveBrandProfile(with: product.brand_id!, productId: Int(product.objectID!)!, geofence: false) { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            let threeDAvailable = THBrandProductManager.sharedInstance.brandProduct?.productGalleryImages.contains(where: { (imgGallery) -> Bool in
                if imgGallery.image_type == "3D" {
                    return true
                } else {
                    return false
                }
            })
            
            if threeDAvailable! {
                if #available(iOS 11.0, *) {
                    if (ARConfiguration.isSupported) {
                        let ARShopShowController = THARShopShowViewController(nibName: THARShopShowViewController.className, bundle: nil)
                        ARShopShowController.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                        self.navigationController?.pushViewController(ARShopShowController, animated: true)
                        return
                    }
                }
            }
            let shopshowcontroller = THShopshowViewController(nibName: THShopshowViewController.className, bundle: nil)
            shopshowcontroller.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
            self.navigationController?.pushViewController(shopshowcontroller, animated: true)
        }
    }
}

// MARK: - TableView DataSource Methods
extension THSearchResultsViewController:UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchedResuls.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: THSearchTableViewCell.className, for: indexPath) as! THSearchTableViewCell
        cell.selectionStyle = .none
        let product:SearchProduct = self.searchedResuls[indexPath.row] as! SearchProduct
        cell.lblSearchType.text = "Fulfilled by: \(product.brandName ?? "")\n$\(product.current_price ?? 0)\nFree Shipping"
        //cell.lblSearchType.text = "$\(product.current_price ?? 0)"
        cell.lblSearchDescription.text = product.title
        cell.imgSearchResult .sd_setImage(with: NSURL(string:product.default_thumbnail_image_url) as URL?)
        return cell
    }
}
