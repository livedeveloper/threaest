//
//  SearchProductViewController.swift
//  Threadest
//
//  Created by Jaydeep on 22/11/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import AlgoliaSearch
import ARKit

class SearchProductViewController: UIViewController {
    @IBOutlet weak var objCollectionView: UICollectionView!
    
    @IBOutlet weak var searchTextField: UITextField!
    
    @IBOutlet weak var filterCollectionView: UICollectionView!
    let categoryOfFilter = ["Gender","Category","Brand", "Price"]
    
    var page:UInt = 0
    var maxNoOfPages:UInt = 0
    var maxNoOfPagesAll:UInt = 0
    var pageAll:UInt = 0
    var searchedResuls:NSArray = []
    var searchQuery:THSearchQueryManager?
    var index:Index?
    var query:Query?
    
    var selectedSearchType:String?
    let kStringConstantAll:String = "All"
    var nbPages = 0
    
    var searchText = ""
    var brandName = Array<String>()
    var category = ""
    var gender = ""

    var minPriceProduct = 0
    var maxPriceProduct = 5000
    
    var searchProduct = [SearchProduct]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchQuery = THSearchQueryManager()
        self.searchProduct = [SearchProduct]()
        filterCollectionView.register(UINib(nibName: THFilterTopCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: THFilterTopCollectionViewCell.className)
        objCollectionView.register(UINib(nibName: THSearchProductCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: THSearchProductCollectionViewCell.className)
        objCollectionView.setCollectionViewLayout(CHTCollectionViewWaterfallLayout(), animated: true)
        objCollectionView.keyboardDismissMode = .onDrag
        
        fetchResultBrandName(searchText: searchText, brandName: brandName, category: category, gender: getGender())
        self.loadMore()
        initUI()
    }
    
    func initUI() {
        searchTextField.text = searchText
    }
    
    func getGender() -> String {
        switch gender {
        case "Men":
            return "male"
        case "Women":
            return "female"
        default:
            return ""
        }
    }

   
    public func fetchResultBrandName(searchText: String, brandName: Array<String>, category: String, gender: String) {
        self.objCollectionView.reloadData()
        self.searchQuery?.query?.page = self.page
        self.searchQuery?.filterQueryBrandName(searchText: searchText, brandName: brandName, category: category, gender: gender, minPrice: minPriceProduct, maxPrice: maxPriceProduct, completionBlock: { (searchList, nbPage) in
            self.searchedResuls = []
            self.searchedResuls = searchList as! NSArray
            for (_, element) in self.searchedResuls.enumerated() {
                if element is SearchProduct {
                    self.searchProduct.append(element as! SearchProduct)
                }
            }
            self.objCollectionView.reloadData()
            self.objCollectionView.infiniteScrollingView.stopAnimating()
            self.maxNoOfPages = nbPage
        })
    }
    
    func loadMore() {
        self.objCollectionView.addInfiniteScrolling {
            self.page+=1
            self.fetchResultBrandName(searchText: self.searchText, brandName: self.brandName, category: self.category, gender: self.getGender())
        }
    }
    
    @IBAction func didTapOnBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

extension SearchProductViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == filterCollectionView {
            let filterNewVC = THFilterNewViewController(nibName: THFilterNewViewController.className, bundle: nil)
            filterNewVC.delegate = self
            filterNewVC.selectedCategoryIndex = indexPath.row
            filterNewVC.selectedGender = gender == "" ? ["Men","Women"] : [gender]
            filterNewVC.category = category == "" ? [String]() : [category]
            self.present(filterNewVC, animated: true, completion: nil)
        } else {
            let productObj = self.searchProduct[indexPath.row]
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THBrandProductManager.sharedInstance.retrieveBrandProfile(with:productObj.brand_id!, productId: Int(productObj.objectID!)!, geofence: false) { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                let threeDAvailable = THBrandProductManager.sharedInstance.brandProduct?.productGalleryImages.contains(where: { (imgGallery) -> Bool in
                    if imgGallery.image_type == "3D" {
                        return true
                    } else {
                        return false
                    }
                })
                
                if threeDAvailable! {
                    if #available(iOS 11.0, *) {
                        if (ARConfiguration.isSupported) {
                            let ARShopShowController = THARShopShowViewController(nibName: THARShopShowViewController.className, bundle: nil)
                            ARShopShowController.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                            self.navigationController?.pushViewController(ARShopShowController, animated: true)
                            return
                        }
                    }
                }
                let shopshowcontroller = THShopshowViewController(nibName: THShopshowViewController.className, bundle: nil)
                shopshowcontroller.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                self.navigationController?.pushViewController(shopshowcontroller, animated: true)
        }
        }
    }
}

extension SearchProductViewController: UICollectionViewDataSource, CHTCollectionViewDelegateWaterfallLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == filterCollectionView {
            return categoryOfFilter.count
        } else {
            return searchProduct.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == filterCollectionView {
            let filterCell = collectionView.dequeueReusableCell(withReuseIdentifier: THFilterTopCollectionViewCell.className, for: indexPath) as! THFilterTopCollectionViewCell
            filterCell.filterTypeLabel.text = categoryOfFilter[indexPath.row]
            switch indexPath.row {
            case 0 :
                filterCell.filterNameLabel.text = gender == "" ? "All" : gender
                break
            case 1:
                filterCell.filterNameLabel.text = category == "" ? "All" : GeneralMethods.sharedInstance.getPural(strCategory: category)
                break
            case 2:
                filterCell.filterNameLabel.text = brandName.count == 0 ? "All" : brandName.joined(separator: ",")
            case 3:
                filterCell.filterNameLabel.text = (minPriceProduct == 0 && maxPriceProduct == 5000) ? "All" : "$\(minPriceProduct) to $\(maxPriceProduct)"
                break
            default:
                break
            }
            return filterCell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: THSearchProductCollectionViewCell.className, for: indexPath) as! THSearchProductCollectionViewCell
            cell.configure(searchProduct: searchProduct[indexPath.row])
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        if collectionView == filterCollectionView {
            var stringProductVariable:NSString = ""
            switch indexPath.row {
            case 0 :
                stringProductVariable = gender == "" ? "All   " : gender as NSString
                break
            case 1:
                stringProductVariable = category == "" ? "All    " : category as NSString
                break
            case 2:
                stringProductVariable = brandName.count == 0 ? "All   " : brandName.joined(separator: ",") as NSString
            case 3:
                stringProductVariable = (minPriceProduct == 0 && maxPriceProduct == 5000) ? "All" as NSString : "$\(minPriceProduct) to $\(maxPriceProduct)" as NSString
                break
            default:
                break
            }
            
            let sringWidth = stringProductVariable.size(attributes: [NSFontAttributeName: UIFont.AppFontSemiBold(14)])
            return CGSize(width: sringWidth.width + 25, height: 50)
        } else {
            if searchProduct[indexPath.row].default_medium_image_height == nil || searchProduct[indexPath.row].default_medium_image_width == nil {
                return CGSize(width: gridWidth, height: gridWidth)
            }
            let nbCol = 2
            let flowLayout = collectionViewLayout as! CHTCollectionViewWaterfallLayout
            let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(nbCol - 1))
            let size = (collectionView.bounds.width - totalSpace) / CGFloat(nbCol)
            let imgHeight = CGFloat(searchProduct[indexPath.row].default_medium_image_height)
            let imgWidth = CGFloat(searchProduct[indexPath.row].default_medium_image_width)
            let newHeight = (imgHeight/imgWidth)*size
            return CGSize(width: size, height: newHeight+48)
            /*let imgHeight = CGFloat(searchProduct[indexPath.row].default_medium_image_height)
            let imgWidth = CGFloat(searchProduct[indexPath.row].default_medium_image_width)
            let newHeight = (imgHeight/imgWidth)*gridWidth
            return CGSize(width: gridWidth, height: newHeight+48)*/
        }
    }
}


extension SearchProductViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        searchText = textFieldText.replacingCharacters(in: range, with: string)
        self.searchProduct = [SearchProduct]()
        self.page = 0
        self.loadMore()
        fetchResultBrandName(searchText: searchText, brandName: brandName, category: category, gender: getGender())
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension SearchProductViewController: filterDelegate {
    func filterData(gender: Array<String>, brand: Array<String>, category: Array<String>, price: Array<Int>) {
        var isUpdate = ""
        switch gender.count {
            case 1:
                isUpdate = "true"
                self.gender = gender[0]
            case 2:
                isUpdate = "true"
                self.gender = ""
                break
            default:
                break
        }
        
        if brand.count > 0 {
            isUpdate = "true"
            self.brandName = brand
        }
        if category.count > 0 {
            isUpdate = "true"
            self.category = category[0]
        }
        
        minPriceProduct = price[0]
        maxPriceProduct = price[1]
        filterCollectionView.reloadData()
        if isUpdate == "true" {
            self.searchProduct = [SearchProduct]()
            self.page = 0
            self.loadMore()
            fetchResultBrandName(searchText: searchText, brandName: self.brandName, category: self.category, gender: getGender())
        }
    }
}
