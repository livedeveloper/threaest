//
//  THSearchResultAllViewController.swift
//  Threadest
//
//  Created by Jaydeep on 27/07/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import AlgoliaSearch
import ObjectMapper
import ARKit

class THSearchResultAllViewController: UIViewController {
    @IBOutlet weak var searchTypeCollectionView: UICollectionView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchGirdCollectionView: UICollectionView!
    @IBOutlet weak var searchFilterCollectionView: UICollectionView!
    
    let searchTypeArray = ["Women","Men"]
    let categoryImageArray = ["women_category", "men_category"]
    var selectedSearchTypeIndex = 0
    
    let categoryOfFilter = ["Gender","Category","Brand", "Price"]
    var brandName = Array<String>()
    var category = ""
    var gender = ""
    var minPriceProduct = 0
    var maxPriceProduct = 5000
    
    var isFromScreen = ""
    var objSearchTextField: UITextField = UITextField()
    
    var page:UInt = 0
    var maxNoOfPages:UInt = 0
    var maxNoOfPagesAll:UInt = 0
    var pageAll:UInt = 0
    var searchedResuls:NSArray = []
    var searchQuery:THSearchQueryManager?
    var index:Index?
    var query:Query?
    var selectedSearchType:String?
    let kStringConstantAll:String = "All"
    var searchText = ""
    var nbPages = 0
    
    var searchUser = [SearchUser]()
    var searchBrand = [SearchBrand]()
    var searchProduct = [SearchProduct]()
    
    @IBOutlet weak var objTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchView.alpha = 0
        self.objTableView.tag = 1
        searchTypeCollectionView.register(UINib(nibName: THSearchTypeCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: THSearchTypeCollectionViewCell.className)
        objTableView.register(UINib(nibName: THSearchPopularTableViewCell.className, bundle: nil), forCellReuseIdentifier: THSearchPopularTableViewCell.className)
        searchFilterCollectionView.register(UINib(nibName: THFilterTopCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: THFilterTopCollectionViewCell.className)
        searchGirdCollectionView.register(UINib(nibName: THSearchProductCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: THSearchProductCollectionViewCell.className)
        searchGirdCollectionView.setCollectionViewLayout(CHTCollectionViewWaterfallLayout(), animated: true)
        // Do any additional setup after loading the view.
        self.searchGirdCollectionView.keyboardDismissMode = .onDrag
        self.objTableView.keyboardDismissMode = .onDrag
        self.searchQuery = THSearchQueryManager()
        THAlgoliaApiManager.sharedInstance.retrievePopularAlgoliaSearch(indices: "Product, Brand") { (error) in
            self.objTableView.reloadData()
            }
        
        self.loadMore()
        fetchResultBrandName(searchText: searchText, brandName: self.brandName, category: self.category, gender: getGender())
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        appdelegate.verticle.scrollEnabled(isEnable: false)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
//        appdelegate.verticle.scrollEnabled(isEnable: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let cellWidth : CGFloat = 80
        let edgeInsets = (self.view.frame.size.width - (CGFloat(searchTypeArray.count) * cellWidth)) / (CGFloat(searchTypeArray.count) )
        searchTypeCollectionView.contentInset = UIEdgeInsetsMake(0, edgeInsets, 0, edgeInsets)
        searchFilterCollectionView.contentInset = UIEdgeInsetsMake(0, 13, 0, 0)
    }

    func showAnimate() {
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
        });
    }
    
    func removeAnimate() {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    func getGender() -> String {
        switch gender {
        case "Men":
            return "male"
        case "Women":
            return "female"
        default:
            return ""
        }
    }
    
  
    public func fetchResultBrandName(searchText: String, brandName: Array<String>, category: String, gender: String) {
        self.searchGirdCollectionView.reloadData()
        self.searchQuery?.query?.page = self.page
        self.searchQuery?.filterQueryBrandName(searchText: searchText, brandName: brandName, category: category, gender: gender, minPrice: minPriceProduct, maxPrice: maxPriceProduct, completionBlock: { (searchList, nbPage) in
            self.searchedResuls = []
            self.searchedResuls = searchList as! NSArray
            for (_, element) in self.searchedResuls.enumerated() {
                if element is SearchProduct {
                    self.searchProduct.append(element as! SearchProduct)
                }
            }
            self.searchGirdCollectionView.reloadData()
            self.searchGirdCollectionView.infiniteScrollingView.stopAnimating()
            self.maxNoOfPages = nbPage
        })
    }

    
    func loadMore() {
        self.searchGirdCollectionView.addInfiniteScrolling {
            self.page+=1
            self.fetchResultBrandName(searchText: self.searchText, brandName: self.brandName, category: self.category, gender: self.getGender())
        }
    }
}

extension THSearchResultAllViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let searchProductVC = SearchProductViewController(nibName: SearchProductViewController.className, bundle: nil)
        searchProductVC.searchText = THAlgoliaApiManager.sharedInstance.popularSearch[indexPath.row].query
        self.navigationController?.pushViewController(searchProductVC, animated: true)
    }
    
}

extension THSearchResultAllViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView.tag {
        case 1:
            return THAlgoliaApiManager.sharedInstance.popularSearch.count
        case 2:
            return searchProduct.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView.tag {
            case 1:
                let popularCell = tableView.dequeueReusableCell(withIdentifier: THSearchPopularTableViewCell.className, for: indexPath) as! THSearchPopularTableViewCell
                popularCell.selectionStyle = .none
                popularCell.popularLabel.text = THAlgoliaApiManager.sharedInstance.popularSearch[indexPath.row].query
                return popularCell
            case 2:
                let searchCell = tableView.dequeueReusableCell(withIdentifier: THSearchResultTableViewCell.className, for: indexPath) as! THSearchResultTableViewCell
                searchCell.selectionStyle = .none
                searchCell.configureProduct(searchProduct: self.searchProduct[indexPath.row])
                return searchCell
            default:
                let popularCell = tableView.dequeueReusableCell(withIdentifier: THSearchPopularTableViewCell.className, for: indexPath) as! THSearchPopularTableViewCell
                popularCell.popularLabel.text = THAlgoliaApiManager.sharedInstance.popularSearch[indexPath.row].query
                return popularCell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
}

extension THSearchResultAllViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case searchFilterCollectionView:
            let filterNewVC = THFilterNewViewController(nibName: THFilterNewViewController.className, bundle: nil)
            filterNewVC.delegate = self
            filterNewVC.selectedCategoryIndex = indexPath.row
            filterNewVC.selectedGender = gender == "" ? ["Men","Women"] : [gender]
            filterNewVC.category = category == "" ? [String]() : [category]
            filterNewVC.selectedPrice = [minPriceProduct, maxPriceProduct]
            self.present(filterNewVC, animated: true, completion: nil)
        case searchGirdCollectionView:
            let productObj = self.searchProduct[indexPath.row]
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THBrandProductManager.sharedInstance.retrieveBrandProfile(with:productObj.brand_id!, productId: Int(productObj.objectID!)!, geofence: false) { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                let threeDAvailable = THBrandProductManager.sharedInstance.brandProduct?.productGalleryImages.contains(where: { (imgGallery) -> Bool in
                    if imgGallery.image_type == "3D" {
                        return true
                    } else {
                        return false
                    }
                })
                
                if threeDAvailable! {
                    if #available(iOS 11.0, *) {
                        if (ARConfiguration.isSupported) {
                            let ARShopShowController = THARShopShowViewController(nibName: THARShopShowViewController.className, bundle: nil)
                            ARShopShowController.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                            self.navigationController?.pushViewController(ARShopShowController, animated: true)
                            return
                        }
                    }
                }
                let shopshowcontroller = THShopshowViewController(nibName: THShopshowViewController.className, bundle: nil)
                shopshowcontroller.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                self.navigationController?.pushViewController(shopshowcontroller, animated: true)
            }
            break
        case searchTypeCollectionView:
            selectedSearchTypeIndex = indexPath.item
            searchTypeCollectionView.reloadData()
            switch indexPath.item {
            case 0:
                let searchCategory = THSearchCategoryViewController(nibName: THSearchCategoryViewController.className, bundle: nil)
                searchCategory.type = "Women"
                let navSearchCategory = UINavigationController(rootViewController: searchCategory)
                navSearchCategory.setNavigationBarHidden(true, animated: false)
                self.present(navSearchCategory, animated: false, completion: nil)
            case 1:
                let searchCategory = THSearchCategoryViewController(nibName: THSearchCategoryViewController.className, bundle: nil)
                searchCategory.type = "Men"
                let navSearchCategory = UINavigationController(rootViewController: searchCategory)
                navSearchCategory.setNavigationBarHidden(true, animated: false)
                self.present(navSearchCategory, animated: false, completion: nil)
            default:
                break
            }
        default:
            break
        }
        
    }
}

extension THSearchResultAllViewController: UICollectionViewDataSource, CHTCollectionViewDelegateWaterfallLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case searchFilterCollectionView:
            return categoryOfFilter.count
        case searchGirdCollectionView:
            return searchProduct.count
        case searchTypeCollectionView:
            return searchTypeArray.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case searchFilterCollectionView:
            let filterCell = collectionView.dequeueReusableCell(withReuseIdentifier: THFilterTopCollectionViewCell.className, for: indexPath) as! THFilterTopCollectionViewCell
            filterCell.filterTypeLabel.text = categoryOfFilter[indexPath.row]
            switch indexPath.row {
            case 0 :
                filterCell.filterNameLabel.text = gender == "" ? "All" : gender
                break
            case 1:
                filterCell.filterNameLabel.text = category == "" ? "All" : GeneralMethods.sharedInstance.getPural(strCategory: category)
                break
            case 2:
                filterCell.filterNameLabel.text = brandName.count == 0 ? "All" : brandName.joined(separator: ",")
            case 3:
                filterCell.filterNameLabel.text = (minPriceProduct == 0 && maxPriceProduct == 5000) ? "All" : "$\(minPriceProduct) to $\(maxPriceProduct)"
                break
            default:
                break
            }
            return filterCell
        case searchGirdCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: THSearchProductCollectionViewCell.className, for: indexPath) as! THSearchProductCollectionViewCell
            cell.configure(searchProduct: searchProduct[indexPath.row])
            return cell
        case searchTypeCollectionView:
            let searchTypeCell = collectionView.dequeueReusableCell(withReuseIdentifier: THSearchTypeCollectionViewCell.className, for: indexPath) as! THSearchTypeCollectionViewCell
            searchTypeCell.imageCategories.image = UIImage(named: categoryImageArray[indexPath.row])
            if (selectedSearchTypeIndex == indexPath.item) {
                searchTypeCell.imageCategories.layer.borderWidth = 1
                searchTypeCell.imageCategories.layer.borderColor = UIColor.white.cgColor
                searchTypeCell.nameLabel.font = UIFont.AppFontSemiBold(14)
            } else {
                searchTypeCell.imageCategories.layer.borderWidth = 0
                searchTypeCell.imageCategories.layer.borderColor = UIColor.clear.cgColor
                searchTypeCell.nameLabel.font = UIFont.AppFontRegular(14)
            }
            searchTypeCell.nameLabel.text = searchTypeArray[indexPath.item]
            return searchTypeCell
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        switch collectionView {
        case searchFilterCollectionView:
            var stringProductVariable:NSString = ""
            switch indexPath.row {
            case 0 :
                stringProductVariable = gender == "" ? "All   " : gender as NSString
                break
            case 1:
                stringProductVariable = category == "" ? "All    " : category as NSString
                break
            case 2:
                stringProductVariable = brandName.count == 0 ? "All   " : brandName.joined(separator: ",") as NSString
                break
            case 3:
                stringProductVariable = (minPriceProduct == 0 && maxPriceProduct == 5000) ? "All" as NSString : "$\(minPriceProduct) to $\(maxPriceProduct)" as NSString
                break
            default:
                break
            }
            let sringWidth = stringProductVariable.size(attributes: [NSFontAttributeName: UIFont.AppFontSemiBold(14)])
            return CGSize(width: sringWidth.width + 25, height: 50)
        case searchGirdCollectionView:
            if searchProduct[indexPath.row].default_medium_image_height == nil || searchProduct[indexPath.row].default_medium_image_width == nil {
                return CGSize(width: gridWidth, height: gridWidth)
            }
            let nbCol = 2
            let flowLayout = collectionViewLayout as! CHTCollectionViewWaterfallLayout
            let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(nbCol - 1))
            let size = (collectionView.bounds.width - totalSpace) / CGFloat(nbCol)
            let imgHeight = CGFloat(searchProduct[indexPath.row].default_medium_image_height)
            let imgWidth = CGFloat(searchProduct[indexPath.row].default_medium_image_width)
            let newHeight = (imgHeight/imgWidth)*size
            return CGSize(width: size, height: newHeight+48)
           /* let imgHeight = CGFloat(searchProduct[indexPath.row].default_medium_image_height)
            let imgWidth = CGFloat(searchProduct[indexPath.row].default_medium_image_width)
            let newHeight = (imgHeight/imgWidth)*gridWidth
            return CGSize(width: gridWidth, height: newHeight+48)*/
        case searchTypeCollectionView:
            return CGSize(width: 80, height: 80)
        default:
            return CGSize(width: 80, height: 80)
        }
    }
}

extension THSearchResultAllViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if isFromScreen == "Closet" {
            return
        }
        if searchView.alpha == 0 {
            self.searchView.isHidden = false
            self.searchTypeCollectionView.reloadData()
            UIView.animate(withDuration: 0.5, animations: {
                self.searchView.alpha = 1
            }, completion: { (complete) in
               
            })
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        searchText = textFieldText.replacingCharacters(in: range, with: string)
        if isFromScreen == "Category" {
            if searchText == "" {
                //removeAnimate()
                view.alpha = 0
                view.isHidden = true
            } else {
                view.alpha = 1
                view.isHidden = false
            }
        }
        
        if isFromScreen == "Closet" {
            if searchText == "" {
                if searchView.alpha == 1 {
                    UIView.animate(withDuration: 0.2, animations: {
                        self.searchView.alpha = 0
                    }, completion: { (complete) in
                        self.searchView.isHidden = true
                    })
                }
            } else {
                if searchView.alpha == 0 {
                    self.searchView.isHidden = false
                    self.searchTypeCollectionView.reloadData()
                    UIView.animate(withDuration: 0.2, animations: {
                        self.searchView.alpha = 1
                    }, completion: { (complete) in
                    })
                }
            }
        }
        
        self.searchProduct = [SearchProduct]()
        self.page = 0
        self.loadMore()
        fetchResultBrandName(searchText: searchText, brandName: brandName, category: category, gender: getGender())
        return true
    }
}

extension THSearchResultAllViewController: SearchBarDelegate {
    func showSearchBar(textField: UITextField) {
        objSearchTextField.becomeFirstResponder()
       // textField.becomeFirstResponder()
       /* self.searchView.isHidden = false
        self.searchTypeCollectionView.reloadData()*/
    }
    
    func textFieldDidBeginEdit(_ textField: UITextField) {
        if isFromScreen == "Closet" {
            return
        }
        if searchView.alpha == 0 {
            self.searchView.isHidden = false
            self.searchTypeCollectionView.reloadData()
            UIView.animate(withDuration: 0.5, animations: {
                self.searchView.alpha = 1
            }, completion: { (complete) in
                
            })
        }
    }
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        searchText = textFieldText.replacingCharacters(in: range, with: string)
        if isFromScreen == "Category" {
            if searchText == "" {
                //removeAnimate()
                view.alpha = 0
                view.isHidden = true
            } else {
                view.alpha = 1
                view.isHidden = false
            }
        }
        self.searchProduct = [SearchProduct]()
        self.page = 0
        self.loadMore()
        fetchResultBrandName(searchText: searchText, brandName: brandName, category: category, gender: getGender())
    }
    
    func didTapOnFilterButton() {
        if searchView.alpha == 0 {
            self.searchView.isHidden = false
            self.searchTypeCollectionView.reloadData()
            UIView.animate(withDuration: 0.5, animations: {
                self.searchView.alpha = 1
            }, completion: { (complete) in
                
            })
        }
        let filterNewVC = THFilterNewViewController(nibName: THFilterNewViewController.className, bundle: nil)
        filterNewVC.delegate = self
        filterNewVC.selectedGender = gender == "" ? ["Men","Women"] : [gender]
        filterNewVC.category = category == "" ? [String]() : [category]
        self.present(filterNewVC, animated: true, completion: nil)
    }
    
    func didTapOnCloseButton(view: UIView) {
        if searchView.alpha == 1 {
            UIView.animate(withDuration: 0.5, animations: {
                self.searchView.alpha = 0
            }, completion: { (complete) in
                self.searchView.isHidden = true
            })
        } else {
            removeAnimate()
            view.alpha = 0
            view.isHidden = true
        }
    }
}


extension THSearchResultAllViewController: filterDelegate {
    func filterData(gender: Array<String>, brand: Array<String>, category: Array<String>, price: Array<Int>) {
        var isUpdate = ""
        switch gender.count {
        case 1:
            isUpdate = "true"
            self.gender = gender[0]
        case 2:
            isUpdate = "true"
            self.gender = ""
            break
        default:
            break
        }
        
        if brand.count > 0 {
            isUpdate = "true"
            self.brandName = brand
        }
        if category.count > 0 {
            isUpdate = "true"
            self.category = category[0]
        }
        
        minPriceProduct = price[0]
        maxPriceProduct = price[1]
        searchFilterCollectionView.reloadData()
        if isUpdate == "true" {
            self.searchProduct = [SearchProduct]()
            self.page = 0
            self.loadMore()
            fetchResultBrandName(searchText: searchText, brandName: self.brandName, category: self.category, gender: getGender())
        }
    }
}


