//
//  THSearchCategoryViewController.swift
//  Threadest
//
//  Created by Jaydeep on 22/11/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THSearchCategoryViewController: UIViewController, UIGestureRecognizerDelegate {
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var objTableView: UITableView!
    @IBOutlet weak var categoryLabel: UILabel!
    var category = [""]
    var type = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        objTableView.register(UINib(nibName: THSearchPopularTableViewCell.className, bundle: nil), forCellReuseIdentifier: THSearchPopularTableViewCell.className)
        initUI()
        
        if type == "Men" {
            category = ["Shirt", "Shoe","Watch", "Bracelet","Sweater", "Pants", "Coat","Jacket" ,"Blazer", "Suit"]
            categoryLabel.text = "MEN"
            categoryImageView.image = UIImage(named: "men_category")
            
        } else {
            category = ["Dress","Hat","Shirt","Jewelry","Bracelet","Blouse","Sweater", "Watch", "Purse", "Handbag", "Heel", "Skirt", "Earrings", "Bag"]
            categoryLabel.text = "WOMEN"
            categoryImageView.image = UIImage(named: "women_category")
        }
        self.view.alpha = 0
        UIView.animate(withDuration: 0.6) {
            self.view.alpha = 1
        }
          self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }

    
    func initUI() {
        categoryImageView.layer.cornerRadius = categoryImageView.frame.size.width/2
        categoryImageView.layer.masksToBounds = true
    }
    
    
    
    @IBAction func didTapOnBackButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension THSearchCategoryViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let searchProductVC = SearchProductViewController(nibName: SearchProductViewController.className, bundle: nil)
        searchProductVC.category = category[indexPath.row]
        searchProductVC.gender = type
        self.navigationController?.pushViewController(searchProductVC, animated: true)
    }
    
}

extension THSearchCategoryViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return category.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let popularCell = tableView.dequeueReusableCell(withIdentifier: THSearchPopularTableViewCell.className, for: indexPath) as! THSearchPopularTableViewCell
        popularCell.selectionStyle = .none
        popularCell.backgroundColor = UIColor.clear
        popularCell.popularLabel.text = GeneralMethods.sharedInstance.getPural(strCategory: category[indexPath.row])
        popularCell.popularLabel.textColor = UIColor.white
        return popularCell
    }
}
