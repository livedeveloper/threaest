//
//  THSaveAccountPopupVC.swift
//  Threadest
//
//  Created by Jaydeep on 26/07/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
class THSaveAccountPopupVC: THBaseViewController {
    @IBOutlet weak var emailTextField: DesignableTextField!
    @IBOutlet weak var passwordTextField: DesignableTextField!
    @IBOutlet weak var subView: UIView!
    fileprivate var textFieldsArray = [UITextField]()
    open var saveAccountPopupCompletionHander : (()->())?
    override func viewDidLoad() {
        super.viewDidLoad()
        showAnimate()
        subView.layer.cornerRadius = 10
        subView.layer.masksToBounds = true
        textFieldsArray = [emailTextField, passwordTextField]
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(gesture:))))
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if !usercurrent.tempAccount {
                self.removeAnimate()
                return
            }
        }
    }
    
    fileprivate func validateFields() -> Bool {
        guard let emailTextFieldText = emailTextField.text, emailTextFieldText.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
            showAlert(withMessage: "Please insert an email!")
            
            return false
        }
        
        guard emailTextFieldText.isValidEmail() else {
            showAlert(withMessage: "Please insert a valid email!")
            
            return false
        }
        
        guard let passwordTextFieldText = passwordTextField.text, passwordTextFieldText.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
            showAlert(withMessage: "Please insert a password!")
            
            return false
        }
        
        return true
    }
    
    @objc fileprivate func dismissKeyboard(gesture: UITapGestureRecognizer) {
        view.endEditing(true)
    }

    @IBAction func didTapOnCloseButton(_ sender: Any) {
        removeAnimate()
    }
    
    @IBAction func didTapOnSaveButton(_ sender: Any) {
        guard validateFields() else {
            return
        }
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
         if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            var user: User = usercurrent
            user.email = emailTextField.text!
            user.password = passwordTextField.text!
            user.tempAccount = true
            THUserManager.sharedInstance.performUpdateUserAccount(for: usercurrent.userId, user: user, completion: { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                guard let error = error else {
                   self.removeAnimate()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.35, execute: {
                        self.saveAccountPopupCompletionHander!()
                    })
                    return
                }
                print(error.localizedDescription)
            })
        }
    }
    
    @IBAction func didTapOnFacebookLoginButton(_ sender: Any) {
        THUserManager.sharedInstance.performFacebookAuth(forViewController: self) { (userExists, authToken, email, userId, gender, error) in
            if let error = error {
                if userId != "" && gender != "" && authToken != "" {
                    let alertController = UIAlertController(title: "Email", message: "Please enter your email:", preferredStyle: .alert)
                    
                    let confirmAction = UIAlertAction(title: "Confirm", style: .default) { (_) in
                        if let field = alertController.textFields![0] as? UITextField {
                            // store your data
                            guard let emailAddress = field.text, emailAddress.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
                                return
                            }
                            
                            self.checkUserAlreadyExisted(token: authToken, userid: userId, emailAddress: emailAddress, gender: gender)
                        } else {
                            // user did not fill field
                        }
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
                    
                    alertController.addTextField { (textField) in
                        textField.placeholder = "Email"
                    }
                    
                    alertController.addAction(confirmAction)
                    alertController.addAction(cancelAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                } else {
                    let alertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                    
                    alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                }
                
            } else {
                if let userExists = userExists {
                    if userExists {
                        /*let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.swipeNavigationView()*/
                        self.removeAnimate()
                    } else {
                        self.showEmailSignUpController(usingFacebookAuth: true, authToken: authToken, emailAddress: email, userId: userId, gender: gender)
                    }
                } else {
                    assertionFailure("We should never get here")
                }
            }
        }
    }
    
    func showEmailSignUpController(usingFacebookAuth shouldUseFacebookAuth: Bool, authToken: String = "", emailAddress: String = "", userId: String = "", gender: String = "") {
        let emailSignUpController = THEmailSignUpViewController(nibName: THEmailSignUpViewController.className, bundle: nil)
        
        emailSignUpController.shouldUseFacebookAuth = shouldUseFacebookAuth
        emailSignUpController.facebookAuthToken = authToken
        emailSignUpController.facebookEmailAddress = emailAddress
        emailSignUpController.facebookUserID = userId
        emailSignUpController.facebookGender = gender
        emailSignUpController.isFromTempAccount = true
        navigationController?.pushViewController(emailSignUpController, animated: true)
    }
    
    func checkUserAlreadyExisted(token: String, userid: String, emailAddress: String, gender: String) {
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THUserManager.sharedInstance.performCheckIfAuthUserExists(with: emailAddress, authToken: token, userID: userid, completion: { (userExists, err) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            // Request fails
            if let err = err {
                print(err.localizedDescription)
                return
            }
            
            // Request succeeds
            if let userExists = userExists {
                if userExists {
                    // Regular flow -> user exists, go to closet - token saved in performCheck endpoint
                   /* let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.swipeNavigationView()*/
                    self.removeAnimate()
                } else {
                    // Redirect to register
                     self.showEmailSignUpController(usingFacebookAuth: true, authToken: token, emailAddress: emailAddress, userId: userid, gender: gender)
                }
            } else {
                
            }
        })
    }
    
}

extension THSaveAccountPopupVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let index = textFieldsArray.index { $0 === textField }
        if let index = index, index < textFieldsArray.count - 1 {
            let nextTextField = textFieldsArray[index + 1]
            nextTextField.becomeFirstResponder()
        } else {
            self.view.endEditing(true)
        }
        
        return true
    }
}
