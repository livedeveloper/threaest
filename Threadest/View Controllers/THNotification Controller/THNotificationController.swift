//
//  THNotificationController.swift
//  Threadest
//
//  Created by jigar on 18/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import Crashlytics
import UserNotifications

class THNotificationController: UIViewController {

    @IBOutlet weak var tblNotifications: UITableView!

    var indexOfPageToRequest = 1
    
    //bulletin board
    var bulletinManager: BulletinManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    tblNotifications.register(UINib(nibName: THNotificationCell.className, bundle: nil), forCellReuseIdentifier: THNotificationCell.className)
        

        //Add funnel for notification icon
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Notification icon", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }

        self.tblNotifications.contentInset = UIEdgeInsetsMake(0, 0, 44, 0)
        self.tblNotifications.tableFooterView = UIView.init()
        if THUserManager.sharedInstance.currentUserdata() != nil {
            callNotificationsService(forPage: indexOfPageToRequest)
        }
        
        if #available(iOS 10.0, *) {
            let current = UNUserNotificationCenter.current()
            current.getNotificationSettings(completionHandler: { (settings) in
                if settings.authorizationStatus == .notDetermined {
                    DispatchQueue.main.async {
                        self.bulletinNotificationBoardPopup()
                    }
                }
                
                if settings.authorizationStatus == .denied {
                    
                }
            })
        } else {
            // Fallback on earlier versions
            if UIApplication.shared.isRegisteredForRemoteNotifications {
                print("APNS-YES")
            } else {
                DispatchQueue.main.async {
                    self.bulletinNotificationBoardPopup()
                }
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    //bulletin board popup
    func bulletinNotificationBoardPopup() {
        self.bulletinManager = BulletinManager(rootItem: GeneralMethods.sharedInstance.bulletinNotificationThreadest(AllowCompletion: {
            THUserManager.sharedInstance.requestPushNotificationPermissions(completion: { granted in
                DispatchQueue.main.async {
                    self.bulletinManager.dismissBulletin(animated: true, completion: {
                    })
                }
            })
        }, dontAllowCompletion: {
            DispatchQueue.main.async {
                self.bulletinManager.dismissBulletin(animated: true, completion: {
                })
            }
        }))
        if #available(iOS 10, *) {
            self.bulletinManager.backgroundViewStyle = .blurred(style: .light, isDark: false)
        } else {
            self.bulletinManager.backgroundViewStyle = .dimmed
        }
        
        self.bulletinManager.prepare()
        self.bulletinManager.presentBulletin(above: self)
    }
    
       
    func callNotificationsService(forPage:Int) {
    GeneralMethods.sharedInstance.loadLoading(view: self.view)
     THNotificationsManager.sharedInstance.retrieveNotifications{ (error) in
        GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            guard error != nil else {
                UIApplication.shared.applicationIconBadgeNumber = 0
                self.tblNotifications.reloadData()
                return
                }
        }
    }
    
    func followCurrentUser(_ sender: UIButton) {
        let pointInTable: CGPoint =  sender.convert(sender.bounds.origin, to: tblNotifications)
        let cellIndexPath = tblNotifications.indexPathForRow(at: pointInTable)
        print((cellIndexPath! as NSIndexPath).row)
        
        let followNotification = THNotificationsManager.sharedInstance.notificationsData[(cellIndexPath! as NSIndexPath).row]
        
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THSocialInteractionPostsManager.sharedInstance.performFollowUser(for: (followNotification.notificationCreator?.id)!) { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            guard error != nil else {
                THNotificationsManager.sharedInstance.notificationsData[(cellIndexPath! as NSIndexPath).row].followNotification?.isFollowing = true
                self.tblNotifications.reloadRows(at: [cellIndexPath!], with: .fade)
                return
            }
        }
    }
    
    func unFollowCurrentUser(_ sender: UIButton) {
        let pointInTable: CGPoint =  sender.convert(sender.bounds.origin, to: tblNotifications)
        let cellIndexPath = tblNotifications.indexPathForRow(at: pointInTable)
        print((cellIndexPath! as NSIndexPath).row)
        
        let followNotification = THNotificationsManager.sharedInstance.notificationsData[(cellIndexPath! as NSIndexPath).row]
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THSocialInteractionPostsManager.sharedInstance.performUnfollowUser(for: (followNotification.notificationCreator?.id)!) { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            guard error != nil else {
                THNotificationsManager.sharedInstance.notificationsData[(cellIndexPath! as NSIndexPath).row].followNotification?.isFollowing = false
                self.tblNotifications.reloadRows(at: [cellIndexPath!], with: .fade)
                return
            }
        }
    }

    
    func openCurrentNotification(_ sender: UIButton) {
        let pointInTable: CGPoint =  sender.convert(sender.bounds.origin, to: tblNotifications)
        let cellIndexPath = tblNotifications.indexPathForRow(at: pointInTable)
        let followNotification = THNotificationsManager.sharedInstance.notificationsData[(cellIndexPath! as NSIndexPath).row]
        
        if(followNotification.notificationType != "Follow") {
            let postID = followNotification.voteNotification?.postId
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THProfilePostsManager.sharedInstance.retrieveNotificationPost(with: postID!, completion: { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                if THProfilePostsManager.sharedInstance.notificationPost == nil {
                    return
                }
                guard error != nil else {
                    let commentController = THCommentViewController(nibName: THCommentViewController.className, bundle: nil)
                    commentController.socialInteraction = THProfilePostsManager.sharedInstance.notificationPost
                    self.navigationController?.pushViewController(commentController, animated: true)
                    return
                }
            })
        }
        
    }
    
    func openUserProfile(_ sender: UIButton) {
        let pointInTable: CGPoint =  sender.convert(sender.bounds.origin, to: tblNotifications)
        guard let cellIndexPath = tblNotifications.indexPathForRow(at: pointInTable) else {
            return
        }
        
        let userInfo = THNotificationsManager.sharedInstance.notificationsData[(cellIndexPath as NSIndexPath).row]
        
        let userId = userInfo.notificationCreator?.id
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THProfilePostsManager.sharedInstance.profilePosts = [SocialInteraction]()
        THProfilePostsManager.sharedInstance.page = 1
        THProfilePostsManager.sharedInstance.didLoadData = false
        THProfilePostsManager.sharedInstance.retrieveProfilePosts(with: userId!) { (error) in
            guard error != nil else {
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let userPostProfileVC = storyBoard.instantiateViewController(withIdentifier: PZProfileTableViewController.className) as! PZProfileTableViewController
                userPostProfileVC.profile = THProfilePostsManager.sharedInstance.profile
                userPostProfileVC.isSearch = true
                userPostProfileVC.userId = userId!
                userPostProfileVC.isPopup = false
                self.navigationController?.pushViewController(userPostProfileVC, animated: true)
                return
            }
        }
    }
    

    @IBAction func didTapCloseButton(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }

}

extension THNotificationController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == THNotificationsManager.sharedInstance.notificationsData.count-1 {
            if THNotificationsManager.sharedInstance.didLoadData {
                THNotificationsManager.sharedInstance.retrieveNotifications { (error) in
                    guard let error = error else {
                        self.tblNotifications.reloadData()
                        return
                    }
                    debugPrint(error)
                }
            }
        }
    }
}


extension THNotificationController:UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return THNotificationsManager.sharedInstance.notificationsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: THNotificationCell.className, for: indexPath) as! THNotificationCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        let currentNotification = THNotificationsManager.sharedInstance.notificationsData[indexPath.row]
        
        let creatorImg = currentNotification.notificationCreator?.thumbnailImgUrl
        
       
        cell.imgCreatorBtn.sd_setImage(with: URL(string: creatorImg!),for: .normal) { (image, error, imageCacheType, imageUrl) in
            if image != nil {
            }
            else{
                print("not found")
            }
        }
        
        cell.imgCreatorBtn.addTarget(self, action: #selector(THNotificationController.openUserProfile(_:)), for: .touchUpInside)
        cell.tapNameBtn.addTarget(self, action: #selector(THNotificationController.openUserProfile(_:)), for: .touchUpInside)

        let creatorName = currentNotification.notificationCreator?.username
        let creatorNameSize = creatorName?.size(attributes: [NSFontAttributeName: UIFont.AppFontBold(14)])
        cell.tapNameBtnWidth.constant = (creatorNameSize?.width)!
        
        
        if(currentNotification.notificationType == "Follow")
        {
            
            cell.lblNotificationText.text = "@\(creatorName!) followed you"
            
           cell.lblNotificationText.attributedText = GeneralMethods.sharedInstance.setAttributedStringWithColor_Font(text: "@\(creatorName!) followed you", partOfString: creatorName!, color: UIColor.AppFontBlack(), font: UIFont.AppFontBold(14))

            
            cell.btnFollow.isHidden = (currentNotification.followNotification?.isFollowing)!
            cell.btnUnfollow.isHidden = !cell.btnFollow.isHidden
            cell.imgOwnerBtn.isHidden = true
            
            cell.lblBeforeTime.text = GeneralMethods.sharedInstance.getTimeAgoStringFrom(dateString: currentNotification.followNotification!.createdAt)
            
            cell.btnFollow.addTarget(self, action: #selector(THNotificationController.followCurrentUser(_:)), for: .touchUpInside)
            cell.btnUnfollow.addTarget(self, action: #selector(THNotificationController.unFollowCurrentUser(_:)), for: .touchUpInside)
            
        }
        else
        {
            let ownerImg = currentNotification.voteNotification?.postImgUrl
            
            cell.imgOwnerBtn.sd_setImage(with: URL(string: ownerImg!),for: .normal) { (image, error, imageCacheType, imageUrl) in
                if image != nil {
                }
                else{
                    print("not found")
                }
            }
            
            cell.btnFollow.isHidden = true
            cell.btnUnfollow.isHidden = true
            cell.imgOwnerBtn.isHidden = false
            
            let voteId = currentNotification.voteNotification!.voteId
            
            if(voteId != 0)
            {
                
                cell.lblNotificationText.attributedText = GeneralMethods.sharedInstance.setAttributedStringWithColor_Font(text: "@\(creatorName!) liked your post", partOfString: creatorName!, color: UIColor.AppFontBlack(), font: UIFont.AppFontBold(14))
            }
            else
            {
                 cell.lblNotificationText.attributedText = GeneralMethods.sharedInstance.setAttributedStringWithColor_Font(text: "@\(creatorName!) commented on your post", partOfString: creatorName!, color: UIColor.AppFontBlack(), font: UIFont.AppFontBold(14))
                
            }
            
            cell.lblBeforeTime.text = GeneralMethods.sharedInstance.getTimeAgoStringFrom(dateString: currentNotification.voteNotification!.createdAt)
            
            cell.imgOwnerBtn.addTarget(self, action: #selector(THNotificationController.openCurrentNotification(_:)), for: .touchUpInside)
        }
        
        
        return cell
    }
}
