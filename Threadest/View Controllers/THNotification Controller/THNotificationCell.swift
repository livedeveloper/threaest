//
//  THNotificationCell.swift
//  Threadest
//
//  Created by jigar on 18/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THNotificationCell: UITableViewCell {
    
    
    @IBOutlet weak var lblNotificationText: UILabel!
    
    
    @IBOutlet weak var btnUnfollow: UIButton!
    
    
    @IBOutlet weak var btnFollow: UIButton!
    
    @IBOutlet weak var lblBeforeTime: UILabel!
    
    
    @IBOutlet weak var imgOwnerBtn: UIButton!


    @IBOutlet weak var imgCreatorBtn: UIButton!
    
    
    @IBOutlet weak var tapNameBtn: UIButton!
    
    
    @IBOutlet weak var tapNameBtnWidth: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgCreatorBtn.layer.cornerRadius = imgCreatorBtn.frame.size.width / 2
        imgCreatorBtn.layer.masksToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //Demo data 
    
    
}
