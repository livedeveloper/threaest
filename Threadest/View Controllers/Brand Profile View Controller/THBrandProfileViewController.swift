//
//  THBrandProfileViewController.swift
//  Threadest
//
//  Created by Jaydeep on 10/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import AlgoliaSearch
import SDWebImage
import ARKit
import MessageUI

class THBrandProfileViewController: UIViewController, UIGestureRecognizerDelegate,MFMailComposeViewControllerDelegate
{
    @IBOutlet weak var objCollectionview: UICollectionView!
    @IBOutlet weak var productCount: UILabel!
    @IBOutlet weak var reviewCount: UILabel!
    @IBOutlet weak var followerCount: UILabel!
    @IBOutlet weak var brandName: UILabel!
    @IBOutlet weak var brandID: UILabel!
    @IBOutlet weak var brandImgView: UIImageView!

    @IBOutlet weak var noProductFoundLabel: UILabel!
    @IBOutlet weak var searchBrandTextField: UITextField!
    @IBOutlet weak var followbutton: UIButton!
    @IBOutlet weak var objTableView: UITableView!
    @IBOutlet weak var saparatorProducts: UIImageView!
    @IBOutlet weak var saparatorFollowers: UIImageView!
    @IBOutlet weak var productLabel: UILabel!
    @IBOutlet weak var followersCount: UILabel!

    var brandProduct = [ClosetCategory]()
    var brand:SearchBrand?
    var searchedBrand:CartBrand?
    var isFrom: String?
    var brandId: Int!
    var isPopup = false
    var isSearch = false

    var searchText = ""
    var page:UInt = 0
    var maxNoOfPages:UInt = 0
    var maxNoOfPagesAll:UInt = 0
    var pageAll:UInt = 0
    var searchedResuls:NSArray = []
    var searchQuery:THSearchQueryManager?
    var index:Index?
    var query:Query?
    var searchProduct = [SearchProduct]()

    //filter
    let categoryOfFilter = ["Gender","Category", "Price"]
    var category = ""
    var gender = ""
    var minPriceProduct = 0
    var maxPriceProduct = 5000


    override func viewDidLoad() {
        super.viewDidLoad()
        //for waterfall layout
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        objCollectionview.setCollectionViewLayout(CHTCollectionViewWaterfallLayout(), animated: true)
        objCollectionview.register(UINib(nibName: THSearchProductCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: THSearchProductCollectionViewCell.className)
        self.objCollectionview.keyboardDismissMode = .onDrag
        objTableView.register(UINib(nibName: THFriendMatchStyleTableViewCell.className, bundle: nil), forCellReuseIdentifier: THFriendMatchStyleTableViewCell.className)
        self.searchQuery = THSearchQueryManager()
        initUI()
        if isFrom == "brandlogo" {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            brandProduct = [ClosetCategory]()
            THBrandsManager.sharedInstance.products = [ClosetCategory]()
            THBrandsManager.sharedInstance.didLoadData = false
            THBrandsManager.sharedInstance.page = 1
            THBrandsManager.sharedInstance.retrieveBrand(for: brandId, isFromCloset: false) { (cartBrand, error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                if(error==nil) {
                    self.searchedBrand = cartBrand;
                    self.brandProduct = THBrandsManager.sharedInstance.products
                    self.populateValues()
                    self.objCollectionview.reloadData()
                    if THBrandsManager.sharedInstance.current_user_following_brand == true {
                        self.followbutton.setTitle("UnFollow",for: .normal)
                    }
                    else {
                        self.followbutton.setTitle("Follow",for: .normal)
                    }
                }
            }
        }
        else {
            brandId = Int((self.brand?.objectID)!)!
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            brandProduct = [ClosetCategory]()
            THBrandsManager.sharedInstance.products = [ClosetCategory]()
            THBrandsManager.sharedInstance.didLoadData = false
            THBrandsManager.sharedInstance.page = 1
            THBrandsManager.sharedInstance.retrieveBrand(for: Int((self.brand?.objectID)!)!, isFromCloset: false) { (cartBrand, error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                if(error==nil) {
                    self.searchedBrand = cartBrand;
                    self.brandProduct = THBrandsManager.sharedInstance.products
                    self.populateValues()
                    self.objCollectionview.reloadData()
                    if THBrandsManager.sharedInstance.current_user_following_brand == true {
                        self.followbutton.setTitle("UnFollow",for: .normal)
                    }
                    else {
                        self.followbutton.setTitle("Follow",for: .normal)
                    }
                }
            }
        }

        loadMoreBrandFollowers()
        THUserManager.sharedInstance.performUserBrandFollowers(brandId: brandId) { (error) in
            if error == nil {
                self.objTableView.reloadData()
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
//        appdelegate.verticle.scrollEnabled(isEnable: false)
        //self.tabBarController?.tabBar.isHidden = true
        super.viewWillAppear(animated)
    }

    override func viewDidDisappear(_ animated: Bool) {
//        appdelegate.verticle.scrollEnabled(isEnable: true)
        let imageCache = SDImageCache.shared()
        imageCache.clearMemory()
    }

    override func didReceiveMemoryWarning() {
        let imageCache = SDImageCache.shared()
        imageCache.clearMemory()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.brandImgView.layer.cornerRadius = self.brandImgView.frame.size.width/2
        self.brandImgView.layer.masksToBounds = true
    }

    fileprivate func initUI() {
        followbutton.layer.cornerRadius = 20
        noProductFoundLabel.isHidden = true
        saparatorProducts.isHidden = false
        saparatorFollowers.isHidden = true
    }

    func loadMoreBrandFollowers() {
        self.objTableView.addInfiniteScrolling {
            THUserManager.sharedInstance.performUserBrandFollowers(brandId: self.brandId) { (error) in
                if error == nil {
                    self.objTableView.infiniteScrollingView.stopAnimating()
                    self.objTableView.reloadData()
                }
            }
        }
    }

    private func populateValues() {
        self.productCount.text = String(format: "%i",(self.searchedBrand?.productCount)!)
        self.followerCount.text = String(format: "%i", (self.searchedBrand?.followerCount)!)
        self.brandImgView.sd_setImage(with: NSURL(string:(self.searchedBrand?.brandLogoImage)!) as URL?)
        self.brandName.text = self.searchedBrand?.name
        self.searchBrandTextField.placeholder = "Search " + (self.searchedBrand?.name)!
    }

    @IBAction func didTapOnProducts(_ sender: Any) {
        saparatorProducts.isHidden = false
        saparatorFollowers.isHidden = true
        productCount.textColor = UIColor.black
        productLabel.textColor = UIColor.black
        followerCount.textColor = UIColor.black.withAlphaComponent(0.49)
        followersCount.textColor = UIColor.black.withAlphaComponent(0.49)
        objCollectionview.isHidden = false
        objTableView.isHidden = true
    }

    @IBAction func didTapOnInfulencer(_ sender: Any) {

        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }

    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property

        mailComposerVC.setToRecipients(["hello@threadest.com"])
        mailComposerVC.setSubject("Influencer Request for " + brandName.text!)
        mailComposerVC.setMessageBody("Hello,<br><br>I am interested in earning money as an influencer of " + (self.searchedBrand?.name)! + ". Here is the best number to reach me at: *ADD YOUR PHONE NUMBER HERE* &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp.<br><br> Sincerely,<br>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp", isHTML: true)

        return mailComposerVC
    }

    func showSendMailErrorAlert() {

        let alertController = UIAlertController(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)

    }

    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }

    @IBAction func didTapOnFollowers(_ sender: Any) {
        self.view.endEditing(true)
        saparatorProducts.isHidden = true
        saparatorFollowers.isHidden = false

        productCount.textColor = UIColor.black.withAlphaComponent(0.49)
        productLabel.textColor = UIColor.black.withAlphaComponent(0.49)
        followerCount.textColor = UIColor.black
        followersCount.textColor = UIColor.black
        objCollectionview.isHidden = true
        objTableView.isHidden = false
    }


    @IBAction func didTappedFollowUnfollow(_ sender: UIButton) {
        /*if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }*/

        if sender.titleLabel?.text == "Follow" {
            self.followbutton.setTitle("UnFollow",for: .normal)
            THSocialInteractionPostsManager.sharedInstance.performFollowBrand(for:brandId) { (error) in
               self.followerCount.text = "\((Int(self.followerCount.text!)! + 1))"
            }
        } else {
            self.followbutton.setTitle("Follow",for: .normal)
            THSocialInteractionPostsManager.sharedInstance.performunFollowBrand(for:brandId) { (error) in
                self.followerCount.text = "\((Int(self.followerCount.text!)! - 1))"
            }
        }
    }

    @IBAction func didTapbackbutton(_ sender: Any) {
        if isPopup {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }

    @IBAction func didTapOnFilterButton(_ sender: Any) {
        let filterNewVC = THFilterNewViewController(nibName: THFilterNewViewController.className, bundle: nil)
        filterNewVC.delegate = self
        filterNewVC.selectedCategoryIndex = 3
        filterNewVC.selectedGender = gender == "" ? ["Men","Women"] : [gender]
        filterNewVC.category = category == "" ? [String]() : [category]
        filterNewVC.selectedPrice = [minPriceProduct, maxPriceProduct]
        self.present(filterNewVC, animated: true, completion: nil)
    }


    func loadMore() {
        self.objCollectionview.addInfiniteScrolling {
            self.page+=1
            self.fetchResultBrandId(searchText: self.searchText, brandId: self.brandId)
        }
    }

    public func fetchResultBrandId(searchText: String, brandId: Int) {
        self.objCollectionview.reloadData()
        self.searchQuery?.query?.page = self.page
        self.searchQuery?.filtersQueryREsult(search: searchText, brandID: brandId, minPrice:minPriceProduct, maxPrice:maxPriceProduct, closetCategory: "", gender: "", completionBlock: {(searchList, nbPage) in
            self.searchedResuls = []
            self.searchedResuls = searchList as! NSArray
            for (_, element) in self.searchedResuls.enumerated() {
                if element is SearchProduct {
                    self.searchProduct.append(element as! SearchProduct)
                }
            }
            if self.isSearch {
                 self.noProductFoundLabel.isHidden = self.searchProduct.count == 0 ? false:true
            }
            self.objCollectionview.reloadData()
            self.objCollectionview.infiniteScrollingView.stopAnimating()
            self.maxNoOfPages = nbPage
        })
    }

    @IBAction func didTapFollowUnFollowButton(_ sender: UIButton) {
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions(), animations: {

                sender.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)

            }, completion: {_ in

                UIView.animate(withDuration: 0.2, animations: {
                    sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                })
                if sender.isSelected {
                    THSocialInteractionPostsManager.sharedInstance.performUnfollowUser(for: THUserManager.sharedInstance.usersToBrandFollowers[sender.tag].id, completion: { (error) in
                        sender.isSelected = false
                        THUserManager.sharedInstance.usersToBrandFollowers[sender.tag].isFollow = false
                    })

                }
                else {
                    THUserManager.sharedInstance.performFollowUser(with: THUserManager.sharedInstance.usersToBrandFollowers[sender.tag].id, completion: { (error) in
                        guard let error = error else {
                            sender.isSelected = true
                            THUserManager.sharedInstance.usersToBrandFollowers[sender.tag].isFollow = true
                            return
                        }
                        debugPrint(error)
                    })
                }
            })
        }
    }

    @IBAction func followUnfollowTouchDragExist(_ sender: DesignableButton) {
        UIView.animate(withDuration: 0.1, delay: 0.0, options: UIViewAnimationOptions(), animations: {
            sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }, completion: { _ in
        })
    }


    @IBAction func followUnfollowTouchDown(_ sender: UIButton) {
        UIView.animate(withDuration: 0.0, animations: {
            sender.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        })
    }

}

extension THBrandProfileViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedBrandId = isSearch == false ? brandProduct[indexPath.item].brandId : searchProduct[indexPath.item].brand_id
        let selectedProductId = isSearch == false ? brandProduct[indexPath.item].Id : Int(searchProduct[indexPath.item].objectID!)
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THBrandProductManager.sharedInstance.retrieveBrandProfile(with: selectedBrandId!, productId: selectedProductId!, geofence: false) { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            let threeDAvailable = THBrandProductManager.sharedInstance.brandProduct?.productGalleryImages.contains(where: { (imgGallery) -> Bool in
                if imgGallery.image_type == "3D" {
                    return true
                } else {
                    return false
                }
            })

            if threeDAvailable! {
                if #available(iOS 11.0, *) {
                    if (ARConfiguration.isSupported) {
                        let ARShopShowController = THARShopShowViewController(nibName: THARShopShowViewController.className, bundle: nil)
                        ARShopShowController.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                        self.navigationController?.pushViewController(ARShopShowController, animated: true)
                        return
                    }
                }
            }
            let shopshowcontroller = THShopshowViewController(nibName: THShopshowViewController.className, bundle: nil)
            shopshowcontroller.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
            self.navigationController?.pushViewController(shopshowcontroller, animated: true)
        }
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if isSearch == false {
            if indexPath.item == brandProduct.count - 1 {
                if THBrandsManager.sharedInstance.didLoadData {
                    THBrandsManager.sharedInstance.retrieveBrand(for: brandId, isFromCloset: false, completion: { (cartBrand, error) in
                        if(error==nil) {
                            self.brandProduct = THBrandsManager.sharedInstance.products
                            self.objCollectionview.reloadData()
                        }
                    })
                }
            }
        }
    }
}


extension THBrandProfileViewController: UICollectionViewDataSource, CHTCollectionViewDelegateWaterfallLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return isSearch == false ? brandProduct.count : searchProduct.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: THSearchProductCollectionViewCell.className, for: indexPath) as! THSearchProductCollectionViewCell
        if isSearch == true {
            cell.configure(searchProduct: searchProduct[indexPath.item])
        } else {
            cell.configure(closetCategory: brandProduct[indexPath.item])
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        if isSearch == true {
            let nbCol = 2
            let flowLayout = collectionViewLayout as! CHTCollectionViewWaterfallLayout
            let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(nbCol - 1))
            let size = (collectionView.bounds.width - totalSpace) / CGFloat(nbCol)
            let imgHeight = CGFloat(searchProduct[indexPath.row].default_medium_image_height)
            let imgWidth = CGFloat(searchProduct[indexPath.row].default_medium_image_width)
            let newHeight = (imgHeight/imgWidth)*size
            return CGSize(width: size, height: newHeight+48)
            /*let imgHeight = CGFloat(searchProduct[indexPath.row].default_medium_image_height)
            let imgWidth = CGFloat(searchProduct[indexPath.row].default_medium_image_width)
            let newHeight = (imgHeight/imgWidth)*gridWidth
            return CGSize(width: gridWidth, height: newHeight+48)*/
        } else {
            let nbCol = 2
            let flowLayout = collectionViewLayout as! CHTCollectionViewWaterfallLayout
            let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(nbCol - 1))
            let size = (collectionView.bounds.width - totalSpace) / CGFloat(nbCol)
            let imgHeight = CGFloat(brandProduct[indexPath.row].defaultMediumImageHeight)
            let imgWidth = CGFloat(brandProduct[indexPath.row].defaultMediumImageWidth)
            let newHeight = (imgHeight/imgWidth)*size
            return CGSize(width: size, height: newHeight+48)
            /*let imgHeight = CGFloat(brandProduct[indexPath.row].defaultMediumImageHeight)
            let imgWidth = CGFloat(brandProduct[indexPath.row].defaultMediumImageWidth)
            let newHeight = (imgHeight/imgWidth)*gridWidth
            return CGSize(width: gridWidth, height: newHeight+48)*/
        }
    }

    func colletionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, heightForHeaderInSection section: NSInteger) -> CGFloat {
            return 0
    }
}

extension THBrandProfileViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = THUserManager.sharedInstance.usersToBrandFollowers[indexPath.row]
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THProfilePostsManager.sharedInstance.profilePosts = [SocialInteraction]()
        THProfilePostsManager.sharedInstance.page = 1
        THProfilePostsManager.sharedInstance.didLoadData = false
        THProfilePostsManager.sharedInstance.retrieveProfilePosts(with: user.id) { (error) in
            guard error != nil else {
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let userPostProfileVC = storyBoard.instantiateViewController(withIdentifier: PZProfileTableViewController.className) as! PZProfileTableViewController
                userPostProfileVC.profile = THProfilePostsManager.sharedInstance.profile
                userPostProfileVC.isSearch = true
                userPostProfileVC.userId = user.id
                self.navigationController?.pushViewController(userPostProfileVC, animated: true)
                return
            }
        }

    }
}

extension THBrandProfileViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return THUserManager.sharedInstance.usersToBrandFollowers.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let matchStyleCell = tableView.dequeueReusableCell(withIdentifier: THFriendMatchStyleTableViewCell.className, for: indexPath) as! THFriendMatchStyleTableViewCell
        let user = THUserManager.sharedInstance.usersToBrandFollowers[indexPath.row]
        matchStyleCell.profileImageView.sd_setImage(with: URL(string: user.thumbnailImageUrl), placeholderImage: nil, options: .retryFailed, completed: nil)
        matchStyleCell.followButton.tag = indexPath.row
        matchStyleCell.followButton.addTarget(self, action: #selector(THBrandProfileViewController.didTapFollowUnFollowButton(_:)), for: .touchUpInside)
        matchStyleCell.followButton.addTarget(self, action: #selector(THBrandProfileViewController.followUnfollowTouchDown(_:)), for: .touchDown)
        matchStyleCell.followButton.addTarget(self, action: #selector(THBrandProfileViewController.followUnfollowTouchDragExist(_:)), for: .touchDragExit)
        matchStyleCell.usernameLabel.text = user.username
        matchStyleCell.followButton.isSelected = user.isFollow
        matchStyleCell.selectionStyle = .none
        return matchStyleCell
    }
}

extension THBrandProfileViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if searchText == "" {
            isSearch = false
        } else {
            isSearch = true
        }
        objCollectionview.reloadData()
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if objCollectionview.isHidden == true {
            self.didTapOnProducts(self)
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if searchText == "" {
            isSearch = false
        } else {
            isSearch = true
        }
        objCollectionview.reloadData()
    }

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        isSearch = false
        objCollectionview.reloadData()
        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        searchText = textFieldText.replacingCharacters(in: range, with: string)
        if searchText == "" {
            noProductFoundLabel.isHidden = true
            isSearch = false
            objCollectionview.reloadData()
        } else {
            isSearch = true
        }
        self.searchProduct = [SearchProduct]()
        self.page = 0
        self.loadMore()
        self.fetchResultBrandId(searchText: self.searchText, brandId: self.brandId)
        return true
    }
}

extension THBrandProfileViewController: filterDelegate {
    func filterData(gender: Array<String>, brand: Array<String>, category: Array<String>, price: Array<Int>) {
        var isUpdate = ""
        switch gender.count {
        case 1:
            isUpdate = "true"
            self.gender = gender[0]
        case 2:
            isUpdate = "true"
            self.gender = ""
            break
        default:
            break
        }

        if category.count > 0 {
            isUpdate = "true"
            self.category = category[0]
        }
        isUpdate = "true"
        minPriceProduct = price[0]
        maxPriceProduct = price[1]
        if isUpdate == "true" {
            self.searchProduct = [SearchProduct]()
            self.page = 0
            self.loadMore()
            self.fetchResultBrandId(searchText: self.searchText, brandId: self.brandId)
        }
    }
}



