//
//  THThreadestWelcomePopupVC.swift
//  Threadest
//
//  Created by Jaydeep on 19/07/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import Crashlytics

class THThreadestWelcomePopupVC: THBaseViewController {
    @IBOutlet weak var threadestPopup: UIView!
    @IBOutlet weak var alradyHaveAnAccountButton: DesignableButton!
    
    @IBOutlet weak var heightConstraintPopup: NSLayoutConstraint!
    
    
    
    open var closetCompletionHander : (()->())?
    override func viewDidLoad() {
        super.viewDidLoad()
        //Add funnel for Setup Closet
        Answers.logCustomEvent(withName: "Setup closet", customAttributes: ["userid":(THUserManager.sharedInstance.currentUserdata()?.userId)!])
        threadestPopup.layer.cornerRadius = 10
        threadestPopup.layer.masksToBounds = true
        showAnimate()
        
        guard let usercurrent = THUserManager.sharedInstance.currentUserdata() else {
            return
        }
        
        if !usercurrent.tempAccount {
            alradyHaveAnAccountButton.isHidden = true
            heightConstraintPopup.constant = 208
        }
    }

    @IBAction func didTapOnClosetButton(_ sender: Any) {
        self.removeAnimate()
    }
    
    @IBAction func didTapOnSetupCloset(_ sender: Any) {
        self.removeAnimate()
        closetCompletionHander!()
        //Add funnel for Setup Closet
        Answers.logCustomEvent(withName: "Setup closet button", customAttributes: ["userid":(THUserManager.sharedInstance.currentUserdata()?.userId)!])
    }
    
    @IBAction func didTapIAlreadyHaveAccount(_ sender: Any) {
        //Add funnel for Setup Closet
        Answers.logCustomEvent(withName: "Already have account", customAttributes: ["userid":(THUserManager.sharedInstance.currentUserdata()?.userId)!])
        THUserManager.sharedInstance.logoutCurrentuser(isNavigateToLogin: true, completion: { (error) in
            
        })
    }
}
