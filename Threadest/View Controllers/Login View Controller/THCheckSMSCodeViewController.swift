//
//  THCheckSMSCodeViewController.swift
//  Threadest
//
//  Created by Jaydeep on 25/07/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THCheckSMSCodeViewController: THBaseViewController {
    @IBOutlet weak var smsCodeTextField: DesignableTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func didTapOnVerifyButton(_ sender: Any) {
        if smsCodeTextField.text != "" {

            self.showWaitView()
            THNetworkManager.sharedInstance.retrieveCheckSMSCode(smsCode: smsCodeTextField.text!, completion: { (json, error) in
                self.dismissWaitView()
                guard let json = json else {
                    return
                }
                let message = json["message"].stringValue
                if json["status"].boolValue {
                    self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "Success", body: message, completion: { (done) in
                        let resetPasswordVC = THResetPasswordViewController(nibName: THResetPasswordViewController.className, bundle: nil)
                        resetPasswordVC.resetPasswordToken = self.smsCodeTextField.text!
                        self.navigationController?.pushViewController(resetPasswordVC, animated: true)
                    }), animated: true, completion: nil)
                }
                else {
                    self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "Success", body: message, completion: { (done) in
                        
                    }), animated: true, completion: nil)
                }
            })
        }
    }

    @IBAction func didTapOnBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
