//
//  THLoginViewController.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 15/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import SwiftyJSON

class THLoginViewController: THBaseViewController {
    // MARK: Outlets
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    var isPopup = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //temporary login crediationials
//        emailTextField.text = ""
//        passwordTextField.text = ""
//        if let token = AccessToken.current {
//            print(token)
//        }
        clearDataOnTextFields()
        initializeStyle()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    // MARK: IB Actions
    @IBAction func didTapClose(sender: UIButton) {
        if isPopup {
            dismiss(animated: true, completion: nil)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }

    @IBAction func didTapNext(sender: UIButton) {
        if validateFields() {
            self.view.endEditing(true)
            
            self.showWaitView()
            THUserManager.sharedInstance.loginUser(withEmail: emailTextField.text!, withPassword: passwordTextField.text!) { (error) in
                self.dismissWaitView()
                if let error = error {
                    self.showAlert(withMessage: error.localizedDescription)
                } else {
                    appdelegate.swipeNavigationView()
                }
            }
        }
    }

    @IBAction func didTapFacebookLogin(sender: UIButton) {
       THUserManager.sharedInstance.performFacebookAuth(forViewController: self) { (userExists, authToken, email, userId, gender, error) in
            if let error = error {
                if userId != "" && gender != "" && authToken != "" {
                    let alertController = UIAlertController(title: "Email", message: "Please enter your email:", preferredStyle: .alert)
                    
//                    let confirmAction = UIAlertAction(title: "Confirm", style: .default) { (_) in
//                        if let field = alertController.textFields![0] as? UITextField {
//                            // store your data
//                            guard let emailAddress = field.text, emailAddress.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
//                                return
//                            }
//
//                            self.checkUserAlreadyExisted(token: authToken, userid: userId, emailAddress: emailAddress, gender: gender)
//                        } else {
//                            // user did not fill field
//                        }
//                    }
                    let confirmAction = UIAlertAction(title: "Confirm", style: .default) { (_) in
                        
                        if let textField = alertController.textFields?[0] {
                            
                            if let emailAddress = textField.text, emailAddress.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                                
                                self.checkUserAlreadyExisted(token: authToken, userid: userId, emailAddress: emailAddress, gender: gender)
                                
                            }
                        }
                    }
                    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
                    
                    alertController.addTextField { (textField) in
                        textField.placeholder = "Email"
                    }
                    
                    alertController.addAction(confirmAction)
                    alertController.addAction(cancelAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                } else {
                    self.showAlert(withMessage: error.localizedDescription)
                }
               
            } else {
                if let userExists = userExists {
                    if userExists {
                        appdelegate.swipeNavigationView()
                    } else {
                         self.showEmailSignUpController(usingFacebookAuth: true, authToken: authToken, emailAddress: email, userId: userId, gender: gender)
                    }
                } else {
                    assertionFailure("We should never get here")
                }
            }
        }
    }

    // MARK: Private functions
    private func validateFields() -> Bool {
        guard let emailTextFieldText = emailTextField.text, emailTextFieldText.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
            showAlert(withMessage: "Please insert an email!")

            return false
        }

        guard emailTextFieldText.isValidEmail() else {
            showAlert(withMessage: "Please insert a valid email!")

            return false
        }

        guard let passwordTextFieldText = passwordTextField.text, passwordTextFieldText.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
            showAlert(withMessage: "Please insert a password!")

            return false
        }

        return true
    }

    func notificationsOfUser()
    {
        if THUserManager.sharedInstance.currentUserdata() != nil {

            THNotificationsManager.sharedInstance.retrieveNotifications(completion: { (error) in
                guard let error = error else {
                    //self.retrieveClosetData()
                    return
                }
                debugPrint(error)
            })
        }
    }
    
    @IBAction func didTapOnForgotPassword(_ sender: Any) {
        let forgotVC = THForgotPasswordViewController(nibName: THForgotPasswordViewController.className, bundle: nil)
        navigationController?.pushViewController(forgotVC, animated: true)
    }
    
    func showEmailSignUpController(usingFacebookAuth shouldUseFacebookAuth: Bool, authToken: String = "", emailAddress: String = "", userId: String = "", gender: String = "") {
        let emailSignUpController = THEmailSignUpViewController(nibName: THEmailSignUpViewController.className, bundle: nil)
        
        emailSignUpController.shouldUseFacebookAuth = shouldUseFacebookAuth
        emailSignUpController.facebookAuthToken = authToken
        emailSignUpController.facebookEmailAddress = emailAddress
        emailSignUpController.facebookUserID = userId
        emailSignUpController.facebookGender = gender
        
        navigationController?.pushViewController(emailSignUpController, animated: true)
    }
    
    func checkUserAlreadyExisted(token: String, userid: String, emailAddress: String, gender: String) {
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THUserManager.sharedInstance.performCheckIfAuthUserExists(with: emailAddress, authToken: token, userID: userid, completion: { (userExists, err) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            // Request fails
            if let _ = err {
                
                return
            }
            
            
            // Request succeeds
            if let userExists = userExists {
                if userExists {
                    // Regular flow -> user exists, go to closet - token saved in performCheck endpoint
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.swipeNavigationView()
                } else {
                    // Redirect to register
                    self.showEmailSignUpController(usingFacebookAuth: true, authToken: token, emailAddress: emailAddress, userId: userid, gender: gender)
                }
            } else {
                
            }
        })
    }
    
    //MARK: - Helper methods
    
    private func initializeStyle() {
        
        let toolbar = self.generateToolbar()
        emailTextField.inputAccessoryView = toolbar
        passwordTextField.inputAccessoryView = toolbar
        
    }
    
    private func clearDataOnTextFields() {
        emailTextField.text = ""
        passwordTextField.text = ""
    }

    
}
