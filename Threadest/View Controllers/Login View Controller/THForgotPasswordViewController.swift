//
//  THForgotPasswordViewController.swift
//  Threadest
//
//  Created by Jaydeep on 25/07/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THForgotPasswordViewController: THBaseViewController {
    @IBOutlet weak var emailTextField: DesignableTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeStyle()
        // Do any additional setup after loading the view.
    }

    @IBAction func didTapViaEmail(_ sender: Any) {
        if validateFields() {
            self.showWaitView()
            
            THNetworkManager.sharedInstance.retrieveResetViaEmail(email: emailTextField.text!) { (json, error) in
                self.dismissWaitView()
                guard let json = json else {
                    return
                }
                if json["status"].boolValue {
                    self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "Success", body: json["message"].stringValue, completion: { (done) in
                        self.navigationController?.popViewController(animated: true)
                    }), animated: true, completion: nil)
                }
                else {
                    self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "Error", body: json["message"].stringValue, completion: { (done) in
                        
                    }), animated: true, completion: nil)
                }
            }
        }
    }


    @IBAction func didTapViaSMS(_ sender: Any) {
        if validateFields() {
            self.showWaitView()
            THNetworkManager.sharedInstance.retrieveResetViaSMS(email: emailTextField.text!) { (json, error) in
                self.dismissWaitView()
                guard let json = json else {
                    return
                }
                let message = json["message"].stringValue
                if json["status"].boolValue {
                    self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "Success", body: message, completion: { (done) in
                        let smsVC = THCheckSMSCodeViewController(nibName: THCheckSMSCodeViewController.className, bundle: nil)
                        self.navigationController?.pushViewController(smsVC, animated: true)
                    }), animated: true, completion: nil)
                }
                else {
                    self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "Error", body: message, completion: { (done) in
                        self.navigationController?.popViewController(animated: true)
                    }), animated: true, completion: nil)
                }
            }
        }
    }

    @IBAction func didTapOnBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    // MARK: Private functions
    private func validateFields() -> Bool {
        guard let emailTextFieldText = emailTextField.text, emailTextFieldText.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
            showAlert(withMessage: "Please insert an email!")

            return false
        }

        guard emailTextFieldText.isValidEmail() else {
            showAlert(withMessage: "Please insert a valid email!")

            return false
        }

        return true
    }
    
    private func initializeStyle() {
        
        let keyboardToolbar = generateToolbar()
        emailTextField.inputAccessoryView = keyboardToolbar
        
    }

}
