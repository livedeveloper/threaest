//
//  THResetPasswordViewController.swift
//  Threadest
//
//  Created by Jaydeep on 25/07/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THResetPasswordViewController: THBaseViewController {
    @IBOutlet weak var newPasswordTextField: DesignableTextField!

    @IBOutlet weak var confirmNewPasswordTextField: DesignableTextField!

    var resetPasswordToken = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initializeStyle()
    }

    //MARK: - IBActions
    
    @IBAction func didTapOnChangeButton(_ sender: Any) {
        if newPasswordTextField.text == "" {
            self.showAlert(withMessage: "Please enter your password.")
        } else {
            if newPasswordTextField.text == confirmNewPasswordTextField.text {
                self.showWaitView()
                THNetworkManager.sharedInstance.retrieveResetPassword(passwordToken: resetPasswordToken, password: newPasswordTextField.text!, completion: { (json, error) in
                    self.dismissWaitView()
                    guard let json = json else {
                        return
                    }
                    let message = json["message"].stringValue
                    if json["status"].boolValue {
                        self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "Success", body: message, completion: { (done) in
                            self.navigationController?.popToRootViewController(animated: true)
                        }), animated: true, completion: nil)
                    }
                    else {
                        self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "Success", body: message, completion: { (done) in
                            
                        }), animated: true, completion: nil)
                    }
                })
            }
        }
    }
    
    @IBAction func didTapOnBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Helper methods
    
    
    func initializeStyle() {
        
        let keyboardToolbar = generateToolbar()
        newPasswordTextField.inputAccessoryView = keyboardToolbar
        confirmNewPasswordTextField.inputAccessoryView = keyboardToolbar
    }

}
