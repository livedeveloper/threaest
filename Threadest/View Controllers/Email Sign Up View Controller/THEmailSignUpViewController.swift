//
//  THEmailSignUpViewController.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 09/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import Crashlytics


class THEmailSignUpViewController: THBaseViewController {
    
    //IBOutlet
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var CheckTermsButton: UIButton!
    @IBOutlet weak var termsOfServiceLabel: UILabel!
    //Owner variable define
    fileprivate var textFieldsArray = [UITextField]()
    var shouldUseFacebookAuth = false
    var facebookAuthToken = ""
    var facebookEmailAddress = ""
    var facebookUserID = ""
    var facebookGender = ""
    var isFromTempAccount = false
    var  arrPhones = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldsArray = [emailTextField, passwordTextField, usernameTextField, phoneTextField]
        for textField in textFieldsArray {
            textField.delegate = self
        }
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(gesture:)))
        view.addGestureRecognizer(tapGestureRecognizer)
        let tapTermsofService = UITapGestureRecognizer(target: self, action: #selector(THEmailSignUpViewController.tapTermsOfService))
        termsOfServiceLabel.addGestureRecognizer(tapTermsofService)
        setDoneOnKeyboard()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Set data to email
        emailTextField.text = facebookEmailAddress
    }
    
    //MARK: IBActions
    
    @objc fileprivate func dismissKeyboard(gesture: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    @IBAction func didTapOnCheckMarkTerms(_ sender: Any) {
        CheckTermsButton.isSelected = !CheckTermsButton.isSelected
    }
    
    
    @IBAction func didTapNext(sender: UIButton) {
        
        if validateFields() {
            let registration = Registration(username: usernameTextField.text!, password: passwordTextField.text!, passwordConfirmation: passwordTextField.text!, mobilePhoneNumber: phoneTextField.text!, email: emailTextField.text!)
            
            self.showWaitView()
            let completion: UserManagerGenericCompletion = { (error) in
                self.dismissWaitView()
                if let error = error {
                    self.showAlert(withMessage: error.localizedDescription)
                } else {
                    
                    ThreadestStorage.clearCoachMarkIfNeeded()
                    Answers.logSignUp(withMethod: "Digits",
                                      success: true,
                                      customAttributes: nil)
                    if self.isFromTempAccount {
                        self.navigationController?.popViewController(animated: false)
                    } else {
                        appdelegate.swipeNavigationView()
                    }
                }
            }
            
            if shouldUseFacebookAuth {
                THUserManager.sharedInstance.createFacebookUser(with: facebookAuthToken, email: facebookEmailAddress, userID: facebookUserID, gender: facebookGender, registration: registration, completion: completion)
            } else {
                THUserManager.sharedInstance.registerUser(with: registration, tempAccount: false, completion: completion)
            }
        }
        
    }
    
    @IBAction func didTapCloseButton(sender: UIButton) {
        view.endEditing(true)
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Helper methods
    
    private func setDoneOnKeyboard() {
        let keyboardToolbar = generateToolbar()
        emailTextField.inputAccessoryView = keyboardToolbar
        passwordTextField.inputAccessoryView = keyboardToolbar
        usernameTextField.inputAccessoryView = keyboardToolbar
        phoneTextField.inputAccessoryView = keyboardToolbar
    }
    
        
    
    func tapTermsOfService() {
        UIApplication.shared.openURL(URL(string: PRIVACY_POLICY)!)
    }
        
    fileprivate func validateFields() -> Bool {
        guard let emailTextFieldText = emailTextField.text, emailTextFieldText.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
            showAlert(withMessage: "Please insert an email!")
            
            return false
        }
        
        guard emailTextFieldText.isValidEmail() else {
            showAlert(withMessage: "Please insert a valid email!")
            
            return false
        }
        
        guard let passwordTextFieldText = passwordTextField.text, passwordTextFieldText.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
            showAlert(withMessage: "Please insert a password!")
            
            return false
        }
        
        guard let usernameTextFieldText = usernameTextField.text, usernameTextFieldText.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
            showAlert(withMessage: "Please insert the username!")
            
            return false
        }
        
        guard let phoneTextFieldText = phoneTextField.text, phoneTextFieldText.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
            showAlert(withMessage: "Please insert the phone number!")
            
            return false
        }
        
        if CheckTermsButton.isSelected == false {
            showAlert(withMessage: "Please agree the terms of service!")
            
            return false
        }
        
        return true
    }
}

extension THEmailSignUpViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let index = textFieldsArray.index { $0 === textField }
        
        if let index = index, index < textFieldsArray.count - 1 {
            let nextTextField = textFieldsArray[index + 1]
            
            nextTextField.becomeFirstResponder()
        } else {
            self.view.endEditing(true)
        }
        
        return true
    }
}

