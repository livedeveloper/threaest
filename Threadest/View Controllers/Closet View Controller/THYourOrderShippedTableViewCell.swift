//
//  THYourOrderShippedTableViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 23/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THYourOrderShippedTableViewCell: UITableViewCell {

    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblTrackingCode: UILabel!
    @IBOutlet weak var lblShippedBy: UILabel!
    @IBOutlet weak var lblShippingAddress: UILabel!
    @IBOutlet weak var lblSize: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var prodictImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
