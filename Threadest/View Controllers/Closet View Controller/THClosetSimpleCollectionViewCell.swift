//
//  THClosetSimpleCollectionViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 13/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THClosetSimpleCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgCloset: UIImageView!
    
    func configure(withClosetItem closetItem: ClosetCategory) {
        // TODO: Add placeholder
        imgCloset.sd_setImage(with: URL(string: closetItem.defaultImageUrl), placeholderImage: nil, options: .retryFailed)
    }
}
