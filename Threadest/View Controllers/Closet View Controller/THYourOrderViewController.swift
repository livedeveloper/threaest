//
//  THYourOrderViewController.swift
//  Threadest
//
//  Created by Jaydeep on 22/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import ARKit

class THYourOrderViewController: UIViewController {
    @IBOutlet weak var objTableView: UITableView!

    @IBOutlet weak var deliverButton: UIButton!
    @IBOutlet weak var saparatorPendingLine: UIImageView!

    @IBOutlet weak var shippedButton: UIButton!
    @IBOutlet weak var saparatorShippedLine: UIImageView!


    //variable
    var selectedOrderType = "shipped"
    var isOrderSucessFull: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        objTableView.register(UINib(nibName: THYourOrderPendingTableViewCell.className, bundle: nil), forCellReuseIdentifier: THYourOrderPendingTableViewCell.className)
        objTableView.register(UINib(nibName: THYourOrderShippedTableViewCell.className, bundle: nil), forCellReuseIdentifier: THYourOrderShippedTableViewCell.className)
        self.callShippedProducts()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isOrderSucessFull {
            GeneralMethods.sharedInstance.RethreadPostSucessPopup(message: "Your order has been completed. +5 points! 😎", viewController: self)
        }
    }

    @IBAction func didTapCloseButton(_ sender: Any) {
        //self.dismiss(animated: true, completion: nil)
        _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func didTapDeliveredButton(_ sender: Any) {
        selectedOrderType = "delivered"
        //saparatorPendingLine.isHidden = false
        //saparatorShippedLine.isHidden = true

        deliverButton.titleLabel?.font = UIFont(name: "SFUIText-Semibold", size: 16)
        shippedButton.titleLabel?.font = UIFont(name: "SFUIText-Regular", size: 14)
        objTableView.reloadData()
    }

    @IBAction func didTapShippedButton(_ sender: Any) {
        selectedOrderType = "shipped"
        objTableView.reloadData()
        //self.callShippedProducts()
    }

   public func callShippedProducts() {
        guard let user = THUserManager.sharedInstance.currentUserdata() else {
        return
        }
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THUserOrdersManager.sharedInstance.userShippedProducts = [PurchasesOrders]()
        THUserOrdersManager.sharedInstance.retrieveShippedOrder(with: user.userId) { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
        guard let error = error else {
            self.deliverButton.titleLabel?.font = UIFont(name: "SFUIText-Regular", size: 14)
            self.shippedButton.titleLabel?.font = UIFont(name: "SFUIText-Semibold", size: 16)
            self.objTableView.reloadData()
            return
        }
        debugPrint(error)
        }
    }
}


extension THYourOrderViewController:UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch selectedOrderType {
        case "delivered":
            return 115
        case "shipped":
            return 135
        default:
            return 115
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(selectedOrderType == "shipped"){
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            let shippedProduct = THUserOrdersManager.sharedInstance.userShippedProducts[indexPath.row] as PurchasesOrders
            THBrandProductManager.sharedInstance.retrieveBrandProfile(with: shippedProduct.productData.brandId, productId: shippedProduct.productId, geofence: false) { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                let threeDAvailable = THBrandProductManager.sharedInstance.brandProduct?.productGalleryImages.contains(where: { (imgGallery) -> Bool in
                    if imgGallery.image_type == "3D" {
                        return true
                    } else {
                        return false
                    }
                })
                
                if threeDAvailable! {
                    if #available(iOS 11.0, *) {
                        if (ARConfiguration.isSupported) {
                            let ARShopShowController = THARShopShowViewController(nibName: THARShopShowViewController.className, bundle: nil)
                            ARShopShowController.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                            self.navigationController?.pushViewController(ARShopShowController, animated: true)
                            return
                        }
                    }
                }
                let shopshowcontroller = THShopshowViewController(nibName: THShopshowViewController.className, bundle: nil)
                shopshowcontroller.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                self.navigationController?.pushViewController(shopshowcontroller, animated: true)
            }
        }
    
    }
}
extension THYourOrderViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch selectedOrderType {
        case "delivered":
            return 0
        case "shipped":
            return THUserOrdersManager.sharedInstance.userShippedProducts.count
        default:
            return 115
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch selectedOrderType {
        case "delivered":
            let cell = tableView.dequeueReusableCell(withIdentifier: THYourOrderPendingTableViewCell.className, for: indexPath) as! THYourOrderPendingTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        case "shipped":
            let shippedProduct = THUserOrdersManager.sharedInstance.userShippedProducts[indexPath.row] as PurchasesOrders
            let cell = tableView.dequeueReusableCell(withIdentifier: THYourOrderShippedTableViewCell.className, for: indexPath) as! THYourOrderShippedTableViewCell

            cell.lblSize.text = "Size :\(shippedProduct.size)"
            cell.lblPrice.text = "$\(shippedProduct.price)"
            cell.lblProductName.text = shippedProduct.productData.title
            cell.lblShippingAddress.text = "\(shippedProduct.productData.brandName)"
            //cell.lblTrackingCode.text = shippedProduct.

            cell.prodictImage.sd_setImage(with: URL(string: shippedProduct.productData.productGalleryImages[0].mediumImageUrl), placeholderImage: nil, options: .retryFailed, progress: { (block1, block2, imageurl) in

            }) { (image, error, imageCacheType, imageUrl) in
                if image != nil {
                   // self.circularView.isHidden = true
                }else {
                    print("image not found")
                }
            }

            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: THYourOrderShippedTableViewCell.className, for: indexPath) as! THYourOrderShippedTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
    }
}
