//
//  UserClosetSize.swift
//  Threadest
//
//  Created by jigar on 10/04/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation

struct UserClosetSize {
    var gender:String?
    var shoesSize : String?
    var waistSize : String?
    var height : String?
    var braSize : String?
    var hipSize : String?
    var shirtSize : String?
    var neckSize : String?
}
