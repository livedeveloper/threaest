//
//  THCameraPermissionPopup.swift
//  Threadest
//
//  Created by Jaydeep on 26/10/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import Crashlytics

class THCameraPermissionPopup: THBaseViewController {
    @IBOutlet weak var viewOut: UIView!
    open var cameraPermissionPopupCompletionHander : ((_ type: String?)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewOut.layer.cornerRadius = 5
        viewOut.layer.masksToBounds = true
        showAnimate()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func didTapOnCloseButton(_ sender: Any) {
        cameraPermissionPopupCompletionHander!("")
        removeAnimate()
    }
    
    @IBAction func didTapOnYesButton(_ sender: Any) {
        //Add funnel for Refresh button
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Accept Camera&Location Popup", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }
        cameraPermissionPopupCompletionHander!("yes")
        removeAnimate()
    }
    
    @IBAction func didTapOnNoButton(_ sender: Any) {
        //Add funnel for Refresh button
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Decline Camera&Location Popup", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }
        removeAnimate()
        userdefault.set(false, forKey: userDefaultKey.ARONOFF.rawValue)
        userdefault.synchronize()
        appdelegate.swipeNavigationView()
        NotificationCenter.default.removeObserver(self, name: Notification.Name(postNotificationType.notificationOrderSucessfully.rawValue), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name(postNotificationType.notificationNearBy.rawValue), object: nil)
    }
    
}
