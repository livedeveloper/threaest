//
//  THUserPickerTableViewCell.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 10/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SDWebImage
import SwiftyJSON

class THUserPickerTableViewCell: UITableViewCell {
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userUsernameLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var followUnfollowButton: DesignableButton!

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    func configure(with followPeople: FollowPeople) {
        // TODO: Add placeholder here
        userImageView.sd_setImage(with: URL(string: followPeople.thumbnailImageUrl), placeholderImage: nil, options: .retryFailed)
        userUsernameLabel.text = "Threadsetter Score: \(followPeople.influencerScore)"
        userNameLabel.text = followPeople.username
    }

    func configureBrand(with followbrand: Brand) {
        // TODO: Add placeholder here
        userImageView.sd_setImage(with: URL(string: followbrand.logoURL), placeholderImage: nil, options: .retryFailed)
        userUsernameLabel.text = followbrand.name
        userNameLabel.text = followbrand.name
    }

}
