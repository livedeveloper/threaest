//
//  THUserPickerFollowBrandViewController.swift
//  Threadest
//
//  Created by Jaydeep on 20/04/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THUserPickerFollowBrandViewController: UIViewController {

@IBOutlet weak var subtitleLabel: UILabel!
@IBOutlet weak var usersTableView: UITableView!
@IBOutlet weak var stepLabel: UILabel!


    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        THUserManager.sharedInstance.retrieveBrandsToFollow { (error) in
            guard let error = error else {
                self.usersTableView.reloadData()
                return
            }
            debugPrint(error)
        }
    }
    
fileprivate func setupUI() {
    subtitleLabel.addLineSpacing(spacing: 12.0, textAlignment: .center)
    
    usersTableView.register(UINib(nibName: THUserPickerTableViewCell.className, bundle: nil), forCellReuseIdentifier: THUserPickerTableViewCell.className)
    usersTableView.tableFooterView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: usersTableView.frame.width, height: 75.0))
    usersTableView.contentInset = .zero
    usersTableView.delegate = self
    usersTableView.dataSource = self
}
    
@IBAction func followUnfollowTouchDragExist(_ sender: DesignableButton) {
    UIView.animate(withDuration: 0.1, delay: 0.0, options: UIViewAnimationOptions(), animations: {
        sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        
    }, completion: { _ in
    })
}


@IBAction func followUnfollowTouchDown(_ sender: UIButton) {
    UIView.animate(withDuration: 0.0, animations: {
        sender.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
    })
}

@IBAction func didTapFollowUnFollowButton(_ sender: UIButton) {
    DispatchQueue.main.async {
        UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions(), animations: {
            sender.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        }, completion: {_ in
            
            UIView.animate(withDuration: 0.2, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
            if sender.isSelected {
            THUserManager.sharedInstance.performUnfollowBrand(with: THUserManager.sharedInstance.brandToFollow[sender.tag].id, completion: { (error) in
                guard let error = error else {
                    sender.isSelected = false
                    THUserManager.sharedInstance.brandToFollow[sender.tag].isFollow = false
                    return
                }
                debugPrint(error)
            })
            }
            else {
                THUserManager.sharedInstance.performFollowBrand(with: THUserManager.sharedInstance.brandToFollow[sender.tag].id, completion: { (error) in
                    guard let error = error else {
                        sender.isSelected = true
                        THUserManager.sharedInstance.brandToFollow[sender.tag].isFollow = true
                        return
                    }
                    debugPrint(error)
                })
            }
        })
    }
}

@IBAction func didTapOnNextButton(_ sender: Any) {
    THUserManager.sharedInstance.setFollowBrandUserPassed(strValue: "false")
    let _ = self.navigationController?.popToRootViewController(animated: false)
}
    
@IBAction func didTapOnFollowAllButton(_ sender: Any) {
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THUserManager.sharedInstance.performFollowAllBrands { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            guard let error = error else {
                THUserManager.sharedInstance.setFollowBrandUserPassed(strValue: "false")
                _ = self.navigationController?.popToRootViewController(animated: true)
                return
            }
            debugPrint(error)
        }
    }
}

extension THUserPickerFollowBrandViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
            let objBrand = THUserManager.sharedInstance.brandToFollow[indexPath.row] as Brand
            let brandProfileVC:THBrandProfileViewController = THBrandProfileViewController()
            brandProfileVC.isFrom = "brandlogo"
            brandProfileVC.brandId = objBrand.id
            self.navigationController?.pushViewController(brandProfileVC, animated: true)
    }
}

extension THUserPickerFollowBrandViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return THUserManager.sharedInstance.brandToFollow.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let userPickerCell = tableView.dequeueReusableCell(withIdentifier: THUserPickerTableViewCell.className, for: indexPath) as! THUserPickerTableViewCell
        userPickerCell.userImageView.layer.cornerRadius = userPickerCell.userImageView.frame.size.width/2
        userPickerCell.userImageView.clipsToBounds = true
        userPickerCell.selectionStyle = UITableViewCellSelectionStyle.none
        userPickerCell.followUnfollowButton.tag = indexPath.row
        userPickerCell.followUnfollowButton.addTarget(self, action: #selector(THUserPickerFollowViewController.didTapFollowUnFollowButton(_:)), for: .touchUpInside)
        userPickerCell.followUnfollowButton.addTarget(self, action: #selector(THUserPickerFollowViewController.followUnfollowTouchDown(_:)), for: .touchDown)
        userPickerCell.followUnfollowButton.addTarget(self, action: #selector(THUserPickerFollowViewController.followUnfollowTouchDragExist(_:)), for: .touchDragExit)

        userPickerCell.configureBrand(with: THUserManager.sharedInstance.brandToFollow[indexPath.row])
        userPickerCell.followUnfollowButton.isSelected = THUserManager.sharedInstance.brandToFollow[indexPath.row].isFollow
        return userPickerCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110.0
    }
}

