//
//  THUserPickerFollowViewController.swift
//  Threadest
//
//  Created by Jaydeep on 02/04/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import Contacts
import FacebookCore
import FacebookLogin
import FBSDKCoreKit

class THUserPickerFollowViewController: UIViewController {

    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var usersTableView: UITableView!
    @IBOutlet weak var stepLabel: UILabel!
    @IBOutlet weak var viewTabOfContactFacebook: UIView!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var ContactsButton: UIButton!
    @IBOutlet weak var SuggestionButton: UIButton!
    @IBOutlet weak var saparatorSuggestions: UILabel!
    
    @IBOutlet weak var saparatorContact: UILabel!

    
    var isFromContactBulletin = false
    var usersToFollow: [FollowPeople] = [FollowPeople]()
    var completionHander : (()->())?


    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        self.didTapOnSuggestionsButton(self)
        let store = CNContactStore()
        store.requestAccess(for: .contacts) { granted, error in}
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    fileprivate func setupUI() {
        subtitleLabel.addLineSpacing(spacing: 12.0, textAlignment: .center)

        usersTableView.register(UINib(nibName: THUserPickerTableViewCell.className, bundle: nil), forCellReuseIdentifier: THUserPickerTableViewCell.className)
        usersTableView.tableFooterView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: usersTableView.frame.width, height: 75.0))
        usersTableView.contentInset = .zero
        usersTableView.delegate = self
        usersTableView.dataSource = self
        
        viewTabOfContactFacebook.layer.cornerRadius = 15
        viewTabOfContactFacebook.layer.masksToBounds = true
    }

    func scanContact() {
        var  arrPhones = [String]()
        if ScanContacts.shared.arrContacts.count == 0 {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THUserManager.sharedInstance.performUsertoFollow(completion: { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                guard let error = error else {
                    self.usersToFollow = THUserManager.sharedInstance.usersToFollow
                    self.usersTableView.reloadData()
                    ScanContacts.shared.findContactsOnBackgroundThread { (true, resultContacts) in
                        
                        for object: ContactInfo in resultContacts as [ContactInfo]
                        {
                            arrPhones.append(object.phone)
                        }
                        let trimmed = arrPhones.description.replacingOccurrences(of: "[", with:"").replacingOccurrences(of: "]", with:"").replacingOccurrences(of: " ", with: "")
                        self.usersFromContact(PhoneNumber: trimmed, isLoad: false)
                    }
                    return
                }
                debugPrint(error)
            })
        } else {
            for object: ContactInfo in ScanContacts.shared.arrContacts as [ContactInfo]
            {
                arrPhones.append(object.phone)
            }
            let trimmed = arrPhones.description.replacingOccurrences(of: "[", with:"").replacingOccurrences(of: "]", with:"").replacingOccurrences(of: " ", with: "")
            self.usersFromContact(PhoneNumber: trimmed, isLoad: true)
        }
    }
    
    func usersFromContact(PhoneNumber: String, isLoad: Bool) {
        if isLoad {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
        }
        THUserManager.sharedInstance.performUsertoFollowFromPhoneBook(for: PhoneNumber, completion: { (error) in
            if isLoad {
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            }
            if let error = error {
                self.showAlert(withMessage: error.localizedDescription)
            } else {
                self.usersToFollow = THUserManager.sharedInstance.usersToFollowFromPhone
                self.usersTableView.reloadData()
                if self.usersToFollow.count == 0 {
                    self.showAlert(title: "No contacts found", message: "Threadest is better with friends.", buttonText: "Invite", okClick: {
                        let findFriendConnectController = THFindFriendsConnectViewController(nibName: THFindFriendsConnectViewController.className, bundle: nil)
                        let navFindFriendConnectController = UINavigationController(rootViewController: findFriendConnectController)
                        self.present(navFindFriendConnectController, animated: true, completion: nil)
                    })
                }
            }
        })
    }
    
    func usersFromFacebook(FBAuthToken: String) {
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THUserManager.sharedInstance.performUsertoFollowFromFB(for: FBAuthToken, completion: { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            if let error = error {
                self.showAlert(withMessage: error.localizedDescription)
            } else {
                self.usersToFollow = THUserManager.sharedInstance.usersToFollowFromFB
                self.usersTableView.reloadData()
                if self.usersToFollow.count == 0 {
                    self.showAlert(title: "No friend found", message: "Threadest is better with friends.", buttonText: "Invite", okClick: {
                        let findFriendConnectController = THFindFriendsConnectViewController(nibName: THFindFriendsConnectViewController.className, bundle: nil)
                        let navFindFriendConnectController = UINavigationController(rootViewController: findFriendConnectController)
                        self.present(navFindFriendConnectController, animated: true, completion: nil)
                    })
                }
            }
        })
    }


    @IBAction func didTapNextButton(_ sender: Any) {
        if let currentUser = THUserManager.sharedInstance.currentUserdata() {
            if currentUser.needsToFollowBrands {
                let userpickerFollowVC = THUserPickerFollowBrandViewController(nibName: THUserPickerFollowBrandViewController.className, bundle: nil)
                self.navigationController?.pushViewController(userpickerFollowVC, animated: true)
            } else {
                THUserManager.sharedInstance.setFollowBrandUserPassed(strValue: "false")
                _ = self.navigationController?.popToRootViewController(animated: true)
                if isFromContactBulletin {
                    completionHander!()
                }
            }
        }
    }

    @IBAction func didTapOnFollowAll(_ sender: Any) {
       GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THUserManager.sharedInstance.performFollowAllUser { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            guard let error = error else {
                if let currentUser = THUserManager.sharedInstance.currentUserdata() {
                    if currentUser.needsToFollowBrands {
                        let userpickerFollowVC = THUserPickerFollowBrandViewController(nibName: THUserPickerFollowBrandViewController.className, bundle: nil)
                        self.navigationController?.pushViewController(userpickerFollowVC, animated: true)
                    } else {
                        THUserManager.sharedInstance.setFollowBrandUserPassed(strValue: "false")
                        _ = self.navigationController?.popToRootViewController(animated: true)
                        if self.isFromContactBulletin {
                            self.completionHander!()
                        }
                    }
                }
                return
            }
            debugPrint(error)
        }
    }

    @IBAction func followUnfollowTouchDragExist(_ sender: DesignableButton) {
        UIView.animate(withDuration: 0.1, delay: 0.0, options: UIViewAnimationOptions(), animations: {
            sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }, completion: { _ in
        })
    }


    @IBAction func followUnfollowTouchDown(_ sender: UIButton) {
        UIView.animate(withDuration: 0.0, animations: {
            sender.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        })
    }
    
    @IBAction func didTapOnFacebook(_ sender: Any) {
        saparatorContact.isHidden = true
        saparatorSuggestions.isHidden = true
        facebookButton.titleLabel?.font = UIFont.AppFontBold(14)
        ContactsButton.titleLabel?.font = UIFont.AppFontRegular(15)
        SuggestionButton.titleLabel?.font = UIFont.AppFontRegular(15)
        facebookButton.backgroundColor = UIColor.blueTabbar
        ContactsButton.backgroundColor = UIColor.black
        SuggestionButton.backgroundColor = UIColor.black
        if let accessToken = AccessToken.current {
            self.usersFromFacebook(FBAuthToken: accessToken.authenticationToken)
        } else {
            LoginManager().logIn([.publicProfile, .email,.userFriends], viewController: self) { (loginResult) in
                print(loginResult)
                switch loginResult {
                case .success(_, _, let accessToken):
                    self.usersFromFacebook(FBAuthToken: accessToken.authenticationToken)
                    break
                case .failed(let error):
                    break
                default:
                    break
                }
            }
        }
    }
    
    
    @IBAction func didTapOnContacts(_ sender: Any) {
        saparatorContact.isHidden = true
        saparatorSuggestions.isHidden = false
        facebookButton.titleLabel?.font = UIFont.AppFontRegular(15)
        ContactsButton.titleLabel?.font = UIFont.AppFontBold(14)
        SuggestionButton.titleLabel?.font = UIFont.AppFontRegular(15)
        facebookButton.backgroundColor = UIColor.black
        SuggestionButton.backgroundColor = UIColor.black
        ContactsButton.backgroundColor = UIColor.blueTabbar
        let authorizedStatus = CNContactStore.authorizationStatus(for: .contacts)
        if authorizedStatus == .authorized {
            self.scanContact()
        }
        else if authorizedStatus == .denied {
            /*SVProgressHUD.show()
            THUserManager.sharedInstance.performUsertoFollow(completion: { (error) in
                SVProgressHUD.dismiss()
                guard let error = error else {
                    self.usersToFollow = THUserManager.sharedInstance.usersToFollow
                    self.usersTableView.reloadData()
                    return
                }
                debugPrint(error)
            })*/
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                let acessContact = THContactAcessPopupVC(nibName: THContactAcessPopupVC.className, bundle: nil)
                self.addChildViewController(acessContact)
                acessContact.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
                acessContact.completionHandler = {
                    self.scanContact()
                }
                self.view.addSubview(acessContact.view)
                acessContact.didMove(toParentViewController: self)
            }
           /* SVProgressHUD.show()
            THUserManager.sharedInstance.performUsertoFollow(completion: { (error) in
                SVProgressHUD.dismiss()
                guard let error = error else {
                    self.usersToFollow = THUserManager.sharedInstance.usersToFollow
                    self.usersTableView.reloadData()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        let acessContact = THContactAcessPopupVC(nibName: THContactAcessPopupVC.className, bundle: nil)
                        self.addChildViewController(acessContact)
                        acessContact.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
                        acessContact.completionHandler = {
                            self.scanContact()
                        }
                        self.view.addSubview(acessContact.view)
                        acessContact.didMove(toParentViewController: self)
                    }
                    return
                }
                debugPrint(error)
            })*/
        }
    }
    
    @IBAction func didTapOnSuggestionsButton(_ sender: Any) {
        saparatorContact.isHidden = false
        saparatorSuggestions.isHidden = true
        facebookButton.titleLabel?.font = UIFont.AppFontRegular(15)
        ContactsButton.titleLabel?.font = UIFont.AppFontRegular(15)
        SuggestionButton.titleLabel?.font = UIFont.AppFontBold(14)
        facebookButton.backgroundColor = UIColor.black
        SuggestionButton.backgroundColor = UIColor.blueTabbar
        ContactsButton.backgroundColor = UIColor.black
        
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THUserManager.sharedInstance.performUsertoFollow(completion: { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            guard let error = error else {
                self.usersToFollow = THUserManager.sharedInstance.usersToFollow
                self.usersTableView.reloadData()
                return
            }
            debugPrint(error)
        })
    }
    
    
    @IBAction func didTapOnUserToFollow(_ sender: Any) {
        //self.tabBarController?.selectedIndex = 4
    }

    @IBAction func didTapFollowUnFollowButton(_ sender: UIButton) {
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions(), animations: {

                sender.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)

            }, completion: {_ in

                UIView.animate(withDuration: 0.2, animations: {
                    sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                })
                if sender.isSelected {
                    THSocialInteractionPostsManager.sharedInstance.performUnfollowUser(for: self.usersToFollow[sender.tag].id, completion: { (error) in
                        sender.isSelected = false
                        self.usersToFollow[sender.tag].isFollow = false
                    })

                }
                else {
                        THUserManager.sharedInstance.performFollowUser(with: self.usersToFollow[sender.tag].id, completion: { (error) in
                         guard let error = error else {
                            sender.isSelected = true
                            self.usersToFollow[sender.tag].isFollow = true
                            return
                        }
                        debugPrint(error)
                    })
                }
            })
        }
    }
    
    @IBAction func didTapOnCloseButton(_ sender: Any) {
        _ = self.navigationController?.popToRootViewController(animated: true)
        if isFromContactBulletin {
            completionHander!()
        }
    }
    
}

extension THUserPickerFollowViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
            let objUser = self.usersToFollow[indexPath.row] as FollowPeople
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THProfilePostsManager.sharedInstance.profilePosts = [SocialInteraction]()
        THProfilePostsManager.sharedInstance.page = 1
        THProfilePostsManager.sharedInstance.didLoadData = false
            THProfilePostsManager.sharedInstance.retrieveProfilePosts(with: objUser.id) { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                guard error != nil else {
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let userPostProfileVC = storyBoard.instantiateViewController(withIdentifier: PZProfileTableViewController.className) as! PZProfileTableViewController
                    userPostProfileVC.profile = THProfilePostsManager.sharedInstance.profile
                    userPostProfileVC.isSearch = true
                    userPostProfileVC.userId = objUser.id
                    self.navigationController?.pushViewController(userPostProfileVC, animated: true)
                    return
                }
            }
        }
}

extension THUserPickerFollowViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.usersToFollow.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let userPickerCell = tableView.dequeueReusableCell(withIdentifier: THUserPickerTableViewCell.className, for: indexPath) as! THUserPickerTableViewCell
        userPickerCell.userImageView.layer.cornerRadius = userPickerCell.userImageView.frame.size.width/2
        userPickerCell.userImageView.clipsToBounds = true
        userPickerCell.selectionStyle = UITableViewCellSelectionStyle.none
        userPickerCell.followUnfollowButton.tag = indexPath.row
        userPickerCell.followUnfollowButton.addTarget(self, action: #selector(THUserPickerFollowViewController.didTapFollowUnFollowButton(_:)), for: .touchUpInside)
        userPickerCell.followUnfollowButton.addTarget(self, action: #selector(THUserPickerFollowViewController.followUnfollowTouchDown(_:)), for: .touchDown)
        userPickerCell.followUnfollowButton.addTarget(self, action: #selector(THUserPickerFollowViewController.followUnfollowTouchDragExist(_:)), for: .touchDragExit)
        userPickerCell.configure(with: self.usersToFollow[indexPath.row])
        userPickerCell.followUnfollowButton.isSelected = self.usersToFollow[indexPath.row].isFollow
        return userPickerCell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
}

