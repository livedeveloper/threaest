//
//  THContactAcessPopupVC.swift
//  Threadest
//
//  Created by Jaydeep on 08/06/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THContactAcessPopupVC: THBaseViewController {
    @IBOutlet weak var viewPopup: UIView!

    open var completionHandler: (() -> ())?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewPopup.layer.cornerRadius = 10
        viewPopup.layer.masksToBounds = true
        showAnimate()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func didTapOnOutsidePopup(_ sender: Any) {
        removeAnimate()
    }
    @IBAction func didTapOnCloseButton(_ sender: Any) {
        removeAnimate()
    }
    @IBAction func didTapOnAllowButton(_ sender: Any) {
        completionHandler!()
        removeAnimate()
    }
    @IBAction func didTapOnDontAllowButton(_ sender: Any) {
        removeAnimate()
    }
}
