//
//  THCommentImageTableViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 08/04/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import CircleProgressView

class THCommentImageTableViewCell: UITableViewCell {
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var postImageHight: NSLayoutConstraint!

    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var commentCountLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var rethreadButton: UIButton!
    @IBOutlet weak var rethreadButtonTop: UIButton!
    
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var rethreadCountLabel: UILabel!
    
   // @IBOutlet weak var OwnerCommentTextLabel: UILabel!
    
    @IBOutlet weak var circularView: CircleProgressView!
    
    var tagButton: UIButton = UIButton()
    
    func configure(with socialInteraction:SocialInteraction) {
        commentCountLabel.isHidden = socialInteraction.commentsCount > 0 ? false:true
        commentCountLabel.text = "\(socialInteraction.commentsCount)"
        likeCountLabel.isHidden = socialInteraction.upvotesCount > 0 ? false: true
        likeCountLabel.text = "\(socialInteraction.upvotesCount)"
        likeButton.isSelected = socialInteraction.likedByCurrentUser ? true: false
        rethreadCountLabel.text = "\(socialInteraction.rethreadCount)"
        rethreadCountLabel.isHidden = socialInteraction.rethreadCount > 0 ? false: true
        
     /*   OwnerCommentTextLabel.isHidden = true
        if socialInteraction.comments.count != 0{
        OwnerCommentTextLabel.isHidden = false
        OwnerCommentTextLabel.attributedText = GeneralMethods.sharedInstance.setAttributedStringWithColor_Font(text: "\(socialInteraction.comments[0].commentOwnerUsername) \(socialInteraction.comments[0].comment)", partOfString: socialInteraction.comments[0].commentOwnerUsername, color: UIColor.AppFontBlack(), font: UIFont.AppFontSemiBold(16))
        }*/
    }
}
