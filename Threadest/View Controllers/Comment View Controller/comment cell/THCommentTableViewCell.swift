//
//  THCommentTableViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 06/04/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THCommentTableViewCell: UITableViewCell {
    //outlet
    @IBOutlet weak var commentImageProfile: UIImageView!
    @IBOutlet weak var commentTime: UILabel!
    @IBOutlet weak var commentTextLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commentImageProfile.layer.cornerRadius = commentImageProfile.frame.size.width / 2
        commentImageProfile.layer.masksToBounds = true
    }
    
    func configure(with comment:Comment) {
        commentImageProfile.sd_setImage(with: URL(string: comment.commentOwnerImage), placeholderImage: nil, options: .retryFailed)
        commentTextLabel.attributedText = GeneralMethods.sharedInstance.setAttributedStringWithColor_Font(text: "\(comment.commentOwnerUsername) \(comment.comment)", partOfString: comment.commentOwnerUsername, color: UIColor.AppFontBlack(), font: UIFont.AppFontSemiBold(16))
        commentTime.text = GeneralMethods.sharedInstance.getCommentTimeAgoStringFrom(dateString: comment.createdAt)
    }

    
}
