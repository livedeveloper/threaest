//
//  THCommentViewController.swift
//  Threadest
//
//  Created by Jaydeep on 06/04/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import SDWebImage
import TwitterKit
import Social
import Crashlytics
import IQKeyboardManagerSwift
import ARKit
import Lottie

class THCommentViewController: UIViewController, UIGestureRecognizerDelegate, UINavigationControllerDelegate {

    //outlelet
    @IBOutlet weak var cartCountLabel: UILabel!
    @IBOutlet weak var objTableView: UITableView!
    @IBOutlet weak var commentTextField: UITextField!
    @IBOutlet weak var commentView: UIView!

    @IBOutlet weak var bottomViewCommentConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var backButton: UIButton!
    var socialInteraction:SocialInteraction!
    var isPopup = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        objTableView.register(UINib(nibName: THCommentTableViewCell.className, bundle: nil), forCellReuseIdentifier: THCommentTableViewCell.className)
        objTableView.register(UINib(nibName: THShareDetailTableViewCell.className, bundle: nil), forCellReuseIdentifier: THShareDetailTableViewCell.className)
        objTableView.register(UINib(nibName: THSocialProductBrandTagTableViewCell.className, bundle: nil), forCellReuseIdentifier: THSocialProductBrandTagTableViewCell.className)
        objTableView.register(UINib(nibName: THSocialLikeCommentTableViewCell.className, bundle: nil), forCellReuseIdentifier: THSocialLikeCommentTableViewCell.className)
        initUI()

        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)

        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)

        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }


    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
               // let _ = self.navigationController?.popToRootViewController(animated: true)
                break
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
                break
            default:
                break
            }
        }
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.objTableView.setNeedsLayout()
        self.objTableView.layoutIfNeeded()
        self.objTableView.layoutSubviews()
    }

    override func viewWillAppear(_ animated: Bool) {
//        appdelegate.verticle.scrollEnabled(isEnable: false)
        //self.tabBarController?.tabBar.isHidden = true
        IQKeyboardManager.sharedManager().enable = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//        appdelegate.verticle.scrollEnabled(isEnable: true)
        IQKeyboardManager.sharedManager().enable = true
    }

    private func initUI() {
        if isPopup {
            backButton.setImage(UIImage(named: "cart_back"), for: .normal)
        } else {
            backButton.setImage(UIImage(named: "navBack"), for: .normal)
        }
        self.hideKeyboardWhenTappedAround()
        commentView.layer.shadowColor = UIColor.lightGray.cgColor
        commentView.layer.shadowOffset = CGSize.zero
        commentView.layer.shadowOpacity = 0.5
        commentView.layer.shadowRadius = 2

        cartCountLabel.layer.cornerRadius = cartCountLabel.frame.width / 2.0
        cartCountLabel.layer.masksToBounds = true

        let notiCenter = NotificationCenter.default
        notiCenter.addObserver(self, selector: #selector(THCommentViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        notiCenter.addObserver(self, selector: #selector(THCommentViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        if let cartCount = UserDefaults.standard.value(forKey: "cartCount")
        {
            if (cartCount as! Int) == 0 {
                self.cartCountLabel.isHidden = true
            }
            else {
                self.cartCountLabel.isHidden = false
                self.cartCountLabel.text = "\(cartCount as! Int)"
            }

        } else {
            self.cartCountLabel.isHidden = true
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        THCommentManager.sharedInstance.comments = [Comment]()
        THCommentManager.sharedInstance.didLoadData = false
        THCommentManager.sharedInstance.page = 1
    }
    @IBAction func didTapOnBack(_ sender: Any) {
        if isPopup {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }

    @IBAction func didTapOnCart(_ sender: Any) {
        let shoppingcartController = THYourCartViewController(nibName: THYourCartViewController.className, bundle: nil)
        self.navigationController?.pushViewController(shoppingcartController, animated: true)
    }

    @IBAction func didTapOnLikeButton(_ sender: UIButton) {
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }
       
        if !sender.isSelected {
            if let commentCell = objTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? THShareDetailTableViewCell {
                let animationView = LOTAnimationView(name: "like")
                animationView.animationProgress = 100
                animationView.center = commentCell.socialPostImage.center
                animationView.frame = commentCell.socialPostImage.frame
                animationView.backgroundColor = UIColor.clear
                animationView.contentMode = .scaleAspectFill
                animationView.animationSpeed = 2
                commentCell.socialPostImage.addSubview(animationView)
                animationView.loopAnimation = false
                animationView.play { (sucess) in
                    animationView.pause()
                    animationView.removeFromSuperview()
                }
            }
            
            sender.isSelected = true
            
            let likeCell = objTableView.cellForRow(at: IndexPath(row: 1, section: 0)) as? THSocialLikeCommentTableViewCell
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                THUserManager.sharedInstance.performUpvote(for: usercurrent.userId, votableId: self.socialInteraction.id, votableType: "Post", completion: { (vote, error) in
                    guard error != nil else {
                        if let i = THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts.index(where: { $0.id == self.socialInteraction.id}){
                            THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[i].likedByCurrentUser = true
                            THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[i].upvotesCount = THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[i].upvotesCount + 1
                        }
                        likeCell?.likeLabel.isHidden = false
                        likeCell?.likeLabel.text = self.socialInteraction.upvotesCount == 0 ? "": self.socialInteraction.upvotesCount == 1 ? "\(self.socialInteraction.upvotesCount) Like" : "\(self.socialInteraction.upvotesCount) Likes"
                        //Add funnel for Like post
                        Answers.logCustomEvent(withName: "Like post", customAttributes: ["userid":usercurrent.userId,"username": usercurrent.username, "post_Id":"\(self.socialInteraction.id)"])
                        return
                    }
                })
            }
        }
    }

    @IBAction func didTapOnCommentButton(_ sender: Any) {
        self.sendPostComment()
    }

    @IBAction func didTapOnPhotoTagButton(_ sender: UIButton) {
        let photoTag = socialInteraction.photoTags[sender.tag]
        if photoTag.tagType == "Brand Tag"{
            let brandProfileVC:THBrandProfileViewController = THBrandProfileViewController()
            brandProfileVC.isFrom = "brandlogo"
            brandProfileVC.brandId = photoTag.brandId
            self.navigationController?.pushViewController(brandProfileVC, animated: true)
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Tap on Brand Tag", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "brand_name":photoTag.brandName])
            }
        } else if photoTag.tagType == "Product Tag" {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THBrandProductManager.sharedInstance.retrieveBrandProfileFromDots(with: photoTag.brandId, productId: photoTag.productId, sharePostCode: socialInteraction.ownerSharePostCode, postId: socialInteraction.id, completion: { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                let threeDAvailable = THBrandProductManager.sharedInstance.brandProduct?.productGalleryImages.contains(where: { (imgGallery) -> Bool in
                    if imgGallery.image_type == "3D" {
                        return true
                    } else {
                        return false
                    }
                })
                
                if threeDAvailable! {
                    if #available(iOS 11.0, *) {
                        if (ARConfiguration.isSupported) {
                            let ARShopShowController = THARShopShowViewController(nibName: THARShopShowViewController.className, bundle: nil)
                            ARShopShowController.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                            self.navigationController?.pushViewController(ARShopShowController, animated: true)
                            return
                        }
                    }
                }
                let shopshowcontroller = THShopshowViewController(nibName: THShopshowViewController.className, bundle: nil)
                shopshowcontroller.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                self.navigationController?.pushViewController(shopshowcontroller, animated: true)
                if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    Answers.logCustomEvent(withName: "Tap on Product Tag", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "product_name":photoTag.productName])
                }
            })
         } else if photoTag.tagType == "Hash Tag" {
            let hashTagController = THHashTagViewController(nibName: THHashTagViewController.className, bundle: nil)
            hashTagController.hashTagString = photoTag.hashTag
            self.navigationController?.pushViewController(hashTagController, animated: true)
        }
    }
    
    @IBAction func didTapOnRethreadButton(_ sender: UIButton) {
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }
        let cell = objTableView.cellForRow(at: IndexPath(row: 1, section: 0)) as!THSocialLikeCommentTableViewCell
        if !cell.shareButton.isSelected {
            let rethreadpopup = THRethreadPopupViewController(nibName: THRethreadPopupViewController.className, bundle: nil)
            rethreadpopup.productLinkString = "https://www.threadest.com/posts/\(self.socialInteraction.id)"
            self.addChildViewController(rethreadpopup)
            rethreadpopup.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
            self.view.addSubview(rethreadpopup.view)
            rethreadpopup.didMove(toParentViewController: self)
            rethreadpopup.completionHander = {selectedRethreadArray in
                for selectedRethread in selectedRethreadArray! {
                    switch selectedRethread as! String {
                    case "Threadest":
                        self.rethreadPost()
                        break
                    case "Facebook":
                        self.rethreadPostOnFacebook()
                        break
                    case "Twitter":
                        self.rethreadPostOnTwitter()
                        break
                    default:
                        break
                    }
                }
            }
            /*cell.shareButton.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                cell.shareButton.transform = CGAffineTransform(rotationAngle: CGFloat(-179.9).degreesToRadians)
            })
            UIView.animate(withDuration: 0.3, delay: 0.2, options: UIViewAnimationOptions(), animations: {
                cell.shareButton.transform = CGAffineTransform(rotationAngle: CGFloat(1.1).degreesToRadians)
            }, completion: { (completed) in
            })*/
        } else {

        }
    }
    
    @IBAction func didTapOnDeletePostButton(_ sender: UIButton) {
        self.present(GeneralMethods.sharedInstance.AlertviewGeneral(title: "Threadest", body: "Are you sure you want to remove post?", cancelbutton: "NO", okbutton: "Yes", completion: {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THProfilePostsManager.sharedInstance.deletePost(with: Int(self.socialInteraction.id)) { (error) in
                guard error != nil else {
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    self.navigationController?.popViewController(animated: true)
                    return
                }
            }
        }), animated: true, completion: nil)
    }
    
    @IBAction func didTapOnLikescountButton(_ sender: UIButton) {
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THSocialInteractionPostsManager.sharedInstance.retrieveLikedPostUserList(for: self.socialInteraction.id) { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            guard let _ = error else {
                let followingController = THUserFollowerViewController(nibName: THUserFollowerViewController.className, bundle: nil)
                followingController.profileType = "Likes"
                self.navigationController?.pushViewController(followingController, animated: true)
                return
            }
            
        }
    }
    
    @IBAction func didTapOnProfileUserNameButton(_ sender: UIButton) {
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THProfilePostsManager.sharedInstance.profilePosts = [SocialInteraction]()
        THProfilePostsManager.sharedInstance.page = 1
        THProfilePostsManager.sharedInstance.didLoadData = false
        THProfilePostsManager.sharedInstance.retrieveProfilePosts(with: Int(self.socialInteraction.userId)) { (error) in
            guard error != nil else {
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let userPostProfileVC = storyBoard.instantiateViewController(withIdentifier: PZProfileTableViewController.className) as! PZProfileTableViewController
                userPostProfileVC.profile = THProfilePostsManager.sharedInstance.profile
                userPostProfileVC.userId = self.socialInteraction.userId
                let navProfileVC = UINavigationController(rootViewController: userPostProfileVC)
                self.present(navProfileVC, animated: true, completion: nil)
                return
            }
        }
    }

    func sendPostComment(){
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }
        if commentTextField.text == "" {
            return
        }
        let commentCell = objTableView.dequeueReusableCell(withIdentifier: THShareDetailTableViewCell.className, for: IndexPath(row: 0, section: 0)) as! THShareDetailTableViewCell
        let postcomment = postComment(postId: socialInteraction.id, userId: socialInteraction.userId, title: "", comment: commentTextField.text!)
        commentTextField.text = ""
        THCommentManager.sharedInstance.perform(with: postcomment) { (comment, error) in
            guard let error = error else {
                if let i = THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts.index(where: { $0.id == self.socialInteraction.id}){
                    THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[i].comments.append(comment!)
                    THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[i].commentsCount = THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[i].commentsCount + 1
                }
                self.socialInteraction.comments.append(comment!)
                commentCell.commentCountLabel.isHidden = false
                self.socialInteraction.commentsCount = self.socialInteraction.commentsCount + 1
                self.objTableView.beginUpdates()
                self.objTableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
                self.objTableView.insertRows(at: [IndexPath(row: self.socialInteraction.comments.count - 1, section: 2)], with: .fade)
                self.objTableView.endUpdates()
                self.objTableView.scrollToRow(at: IndexPath(row: self.socialInteraction.comments.count - 1, section: 2), at: UITableViewScrollPosition.bottom, animated: true)
                return
            }
            debugPrint(error)
        }
    }
    
    // for Rethread post
    func rethreadPost() {
        THSocialInteractionPostsManager.sharedInstance.performRethreadPost(for: socialInteraction.id) { (error) in
            guard let error = error else {
                 let commentImageCell = self.objTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! THShareDetailTableViewCell
                if let i = THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts.index(where: { $0.id == self.socialInteraction.id}){
                    THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[i].rethreadCount = THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[i].rethreadCount + 1
                }
                commentImageCell.rethreadCountLabel.text = "\(self.socialInteraction.rethreadCount + 1)"
                commentImageCell.isHidden = false
                self.socialInteraction.rethreadCount = self.socialInteraction.rethreadCount + 1
                self.objTableView.reloadData()
                //Add funnel for Rethread Threadest Post
                if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    Answers.logCustomEvent(withName: "Rethread post", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "post_id":"\(self.socialInteraction.id)", "rethread_type":"Threadest"])
                }
                return
            }
            debugPrint(error)
        }
    }

    // for Rethread post on Facebook
    func rethreadPostOnFacebook() {
        if FBSDKAccessToken.current() != nil {
            if FBSDKAccessToken.current().hasGranted("publish_actions") {
                GeneralMethods.sharedInstance.publishMessage(message: "https://www.threadest.com/posts/\(socialInteraction.id)", link: socialInteraction.watermarkImageUrl, completion: { () in
                    //Add funnel for Rethread Threadest Post
                    if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                        Answers.logCustomEvent(withName: "Rethread post", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "post_id":"\(self.socialInteraction.id)", "rethread_type":"Facebook"])
                    }
                    GeneralMethods.sharedInstance.RethreadPostSucessPopup(message: "Post to Facebook - Successful", viewController: self)
                })
            } else {
                requestPublishPermissions(socialInteration:socialInteraction)
            }
        } else {
            requestPublishPermissions(socialInteration:socialInteraction)
        }
    }

    func rethreadPostOnTwitter() {
        if let twitterAccountId = TWTRTwitter.sharedInstance().sessionStore.session() {
            GeneralMethods.sharedInstance.uploadTwitterMedia(viewController: self, imageURL: self.socialInteraction.watermarkImageUrl, status: "https://www.threadest.com/posts/\(self.socialInteraction.id)", token: twitterAccountId.authToken, userId: twitterAccountId.userID, id: self.socialInteraction.id, type: "Post")
        } else {
            // Not logged in
            TWTRTwitter.sharedInstance().logIn { (session, error) in
                if let _ = session {
                    if let twitterAccountId = TWTRTwitter.sharedInstance().sessionStore.session() {
                        GeneralMethods.sharedInstance.uploadTwitterMedia(viewController: self, imageURL: self.socialInteraction.watermarkImageUrl, status: "https://www.threadest.com/posts/\(self.socialInteraction.id)", token: twitterAccountId.authToken, userId: twitterAccountId.userID, id: self.socialInteraction.id, type: "Post")
                    }
                } else {
                    print("error: \(String(describing: error?.localizedDescription))");
                }
            }
        }
    }


    func requestPublishPermissions(socialInteration: SocialInteraction) {
        let login: FBSDKLoginManager = FBSDKLoginManager()

        login.logIn(withPublishPermissions: ["publish_actions"], from: self) { (result, error) in
            if (error != nil) {
                print(error!)
            } else if (result?.isCancelled)! {
                print("Canceled")
            } else if (result?.grantedPermissions.contains("publish_actions"))! {
                print("permissions granted")
                GeneralMethods.sharedInstance.publishMessage(message: "https://www.threadest.com/posts/\(socialInteration.id)", link: socialInteration.largeImageUrl, completion: { () in
                    //Add funnel for Rethread Threadest Post
                    if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                        Answers.logCustomEvent(withName: "Rethread post", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "post_id":"\(self.socialInteraction.id)", "rethread_type":"Facebook"])
                    }
                    GeneralMethods.sharedInstance.RethreadPostSucessPopup(message: "Post to Facebook - Successful", viewController: self)
                })
            }
        }
    }

    //MARK:- Show/HideKeyboard
    func keyboardWillShow(_ notification: Notification) {
        DispatchQueue.main.async {
            let info = notification.userInfo! as Dictionary
            let floatAnimationDuration:TimeInterval = info[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval

            UIView.animate(withDuration: floatAnimationDuration, animations: {
                if let keyboardRectValue = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                    self.bottomViewCommentConstraint.constant = keyboardRectValue.height
                    self.view.layoutIfNeeded()
                    if (self.socialInteraction.comments.count > 0) {
                        self.objTableView.scrollToRow(at: IndexPath(row: self.socialInteraction.comments.count - 1, section: 2), at: UITableViewScrollPosition.bottom, animated: true)
                    }
                    
                }
            })
        }
    }

    func keyboardWillHide(_ notification: Notification) {

        DispatchQueue.main.async {

            let info = notification.userInfo! as Dictionary
            let floatAnimationDuration:TimeInterval = info[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval

            UIView.animate(withDuration: floatAnimationDuration, animations: {
                self.bottomViewCommentConstraint.constant = 0
                self.view.layoutIfNeeded()
                self.objTableView.beginUpdates()
                self.objTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                self.objTableView.setContentOffset(CGPoint.zero, animated: true)
                self.objTableView.endUpdates()
            })
        }
    }

    func pinchZoomSocialPostImage(sender: UIPinchGestureRecognizer) {
        let tapLocation = sender.location(in: self.objTableView)
        if let indexPath : IndexPath = self.objTableView.indexPathForRow(at: tapLocation) {
            if let commentCell = self.objTableView.cellForRow(at: indexPath) as? THShareDetailTableViewCell {
                TMImageZoom.shared().gestureStateChanged(sender, withZoom: commentCell.profileImage)
            } else {
                TMImageZoom.shared().resetImageZoom()
            }

        } else {
            TMImageZoom.shared().resetImageZoom()
        }

    }

}

extension THCommentViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 1:
            let btnTag = UIButton()
            btnTag.tag = indexPath.row
            self.didTapOnPhotoTagButton(btnTag)
            break
        default:
            break
        }
    }
}

extension THCommentViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        case 1:
            return socialInteraction.photoTags.count
        case 2:
            return socialInteraction.comments.count
        default:
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            if indexPath.row == 0 {
                let shareDetailCell = tableView.dequeueReusableCell(withIdentifier: THShareDetailTableViewCell.className, for: indexPath) as! THShareDetailTableViewCell
                if let currentUser = THUserManager.sharedInstance.currentUserdata() {
                    if socialInteraction.userId == currentUser.userId {
                        shareDetailCell.trashButton.isHidden = false
                    } else {
                        shareDetailCell.trashButton.isHidden = true
                    }
                }
                
                shareDetailCell.commentButton.isHidden = true
                shareDetailCell.commentCountLabel.isHidden = true
                shareDetailCell.configure(withSocialInteraction: socialInteraction)
                shareDetailCell.hideButtons()
                shareDetailCell.trashButton.addTarget(self, action: #selector(THCommentViewController.didTapOnDeletePostButton(_:)), for: UIControlEvents.touchUpInside)
                shareDetailCell.clearSocialSubview()
                
                let pinchZoomSocilPostImage = UIPinchGestureRecognizer.init(target: self, action: #selector(THCommentViewController.pinchZoomSocialPostImage(sender:)))
                shareDetailCell.socialPostImage.addGestureRecognizer(pinchZoomSocilPostImage)
                shareDetailCell.circularProgressView.isHidden = false
                shareDetailCell.socialPostImage.sd_setImage(with: URL(string: socialInteraction.largeImageUrl), placeholderImage: nil, options: .retryFailed, progress: { (block1, block2, imageurl) in
                    let progress = Double(block1)/Double(block2)
                    DispatchQueue.main.async {
                        shareDetailCell.circularProgressView.setProgress(progress, animated: true)
                    }
                }) { (image, error, imageCacheType, imageUrl) in
                    if image != nil {
                        shareDetailCell.circularProgressView.isHidden = true
                        //for tag view
                        if self.socialInteraction.photoTags.count > 0 {
                            var i = 0
                            for tags in self.socialInteraction.photoTags.filter({$0.tagType == "Product Tag" || $0.tagType == "Brand Tag"}) {
                                let imgHeight = CGFloat(self.socialInteraction.largeImageHeight)
                                let imgWidth = CGFloat(self.socialInteraction.largeImageWidth)
                                let height = (imgHeight/imgWidth) * screenwidth
                                if (height-30) >= CGFloat(tags.y) {
                                    shareDetailCell.tagButton = UIButton(frame: CGRect(x: tags.x, y: tags.y, width: 30, height: 30))
                                } else {
                                    shareDetailCell.tagButton = UIButton(frame: CGRect(x: tags.x, y: tags.y-20, width: 30, height: 30))
                                }
                                shareDetailCell.tagButton.isUserInteractionEnabled = true
                                shareDetailCell.tagButton.backgroundColor = UIColor.white
                                shareDetailCell.tagButton.layer.borderColor = UIColor.white.cgColor
                                shareDetailCell.tagButton.layer.borderWidth = 1
                                shareDetailCell.tagButton.layer.cornerRadius = shareDetailCell.tagButton.frame.size.width / 2
                                shareDetailCell.tagButton.accessibilityIdentifier = String(format: "%d", i)
                                shareDetailCell.tagButton.layer.masksToBounds = true
                                shareDetailCell.tagButton.tag = i
                                if tags.tagType == "Product Tag" {
                                    shareDetailCell.tagButton.sd_setImage(with: URL(string: tags.productImage), for: .normal, placeholderImage: nil, options: .retryFailed, completed: nil)
                                    
                                } else {
                                    shareDetailCell.tagButton.sd_setImage(with: URL(string: tags.brandLogo), for: .normal, placeholderImage: nil, options: .retryFailed, completed: nil)
                                }
                                shareDetailCell.tagButton.imageView?.contentMode = .scaleAspectFit
                                shareDetailCell.tagButton.addTarget(self, action: #selector(THCommentViewController.didTapOnPhotoTagButton(_:)), for: .touchUpInside)
                                shareDetailCell.socialPostImage.addSubview(shareDetailCell.tagButton)
                                i = i+1
                            }
                        }
                    }
                }
                return shareDetailCell
            } else {
                let socialLikeCommentCell = tableView.dequeueReusableCell(withIdentifier: THSocialLikeCommentTableViewCell.className, for: indexPath) as! THSocialLikeCommentTableViewCell
                socialLikeCommentCell.selectionStyle = UITableViewCellSelectionStyle.none
                socialLikeCommentCell.configureSocial(socialInteraction: socialInteraction)
                socialLikeCommentCell.shareButtonLeading.constant = -30
                socialLikeCommentCell.shareButton.addTarget(self, action: #selector(THCommentViewController.didTapOnRethreadButton(_:)), for: .touchUpInside)
                socialLikeCommentCell.likeButton.addTarget(self, action: #selector(THCommentViewController.didTapOnLikeButton(_:)), for: .touchUpInside)
                
                socialLikeCommentCell.profile_nameButton.tag = indexPath.section - 1
                socialLikeCommentCell.profile_nameButton.addTarget(self, action: #selector(THCommentViewController.didTapOnProfileUserNameButton(_:)), for: UIControlEvents.touchUpInside)
                return socialLikeCommentCell
            }
        case 1:
            let socialTagCell = tableView.dequeueReusableCell(withIdentifier: THSocialProductBrandTagTableViewCell.className, for: indexPath) as! THSocialProductBrandTagTableViewCell
            socialTagCell.configure(photosTags: self.socialInteraction.photoTags[indexPath.row])
            return socialTagCell
        case 2:
            let commentCell = tableView.dequeueReusableCell(withIdentifier: THCommentTableViewCell.className, for: indexPath) as! THCommentTableViewCell
            commentCell.selectionStyle = UITableViewCellSelectionStyle.none
            commentCell.configure(with: socialInteraction.comments[indexPath.row])
            commentCell.commentImageProfile.isHidden = indexPath.row == 0 ? false : true
            return commentCell
        default:
            let commentCell = tableView.dequeueReusableCell(withIdentifier: THCommentTableViewCell.className, for: indexPath) as! THCommentTableViewCell
            commentCell.selectionStyle = UITableViewCellSelectionStyle.none
            commentCell.configure(with: socialInteraction.comments[indexPath.row])
            return commentCell
        }
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        switch indexPath.section {
        case 0:
            return false
        case 1:
            return false
        case 2:
            if let currentUser = THUserManager.sharedInstance.currentUserdata() {
                if currentUser.userId == socialInteraction.comments[indexPath.row].userId {
                    return true
                }
            }
            return false
        default:
            return false
        }
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            let comment = socialInteraction.comments[indexPath.row]
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THCommentManager.sharedInstance.performDeletePostComment(with: self.socialInteraction.id, commentId: comment.id, completion: { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                guard error != nil else {
                    self.socialInteraction.comments.remove(at: indexPath.row)
                    self.objTableView.deleteRows(at: [indexPath], with: .left)
                    return
                }
            })
        }
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {

        switch indexPath.section {
        case 0:
            if indexPath.row == 0 {
                return 400
            } else {
                return 100
            }
        default:
            return 40
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            if indexPath.row == 0 {
                let imgHeight = CGFloat(self.socialInteraction.largeImageHeight)
                let imgWidth = CGFloat(self.socialInteraction.largeImageWidth)
                return (imgHeight/imgWidth) * screenwidth
            } else {
                return UITableViewAutomaticDimension
            }
        case 1:
            return 50
        default:
            return 40
        }
    }
}

extension THCommentViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.sendPostComment()
        return true
    }
}
