//
//  THHashTagViewController.swift
//  Threadest
//
//  Created by Jaydeep on 20/06/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import SDWebImage
import TwitterKit
import Social
import Crashlytics


class THHashTagViewController: UIViewController {

    //outlelet
    @IBOutlet weak var headerHashTagLabel: UILabel!
    @IBOutlet weak var hashTagProductCountLabel: UILabel!
    @IBOutlet weak var objTableView: UITableView!


    @IBOutlet weak var bottomTableViewConstraint: NSLayoutConstraint!

    var hashTagString: String = ""


    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        objTableView.register(UINib(nibName: THSocialPostTableViewCell.className, bundle: nil), forCellReuseIdentifier: THSocialPostTableViewCell.className)

        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)

        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)

        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true


        THHashTagManager.sharedInstance.socialInteractionPosts =  [SocialInteraction]()
        THHashTagManager.sharedInstance.page = 1
        THHashTagManager.sharedInstance.didLoadData = false
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        let hash_tag = hashTagString.replacingOccurrences(of: "#", with: "").components(separatedBy: " ").first
        THHashTagManager.sharedInstance.retriveHashTagManager(with: hash_tag!) { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            guard let error = error else {
                self.objTableView.reloadData()
                self.objTableView.setNeedsLayout()
                self.objTableView.layoutIfNeeded()
                self.objTableView.updateConstraints()
                self.objTableView.setNeedsUpdateConstraints()
                self.objTableView.reloadData()
                return
            }
            debugPrint(error)
        }
    }

    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                // let _ = self.navigationController?.popToRootViewController(animated: true)
                break
            case UISwipeGestureRecognizerDirection.left:
                break
            default:
                break
            }
        }
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.objTableView.setNeedsLayout()
        self.objTableView.layoutIfNeeded()
        self.objTableView.layoutSubviews()
    }

    override func viewWillAppear(_ animated: Bool) {
       self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    private func initUI() {

        headerHashTagLabel.text = hashTagString
        hashTagProductCountLabel.text = hashTagString
    }

    override func viewDidDisappear(_ animated: Bool) {

    }
    @IBAction func didTapOnBack(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func didTapOnCommentButton(_ sender: UIButton) {
        let commentController = THCommentViewController(nibName: THCommentViewController.className, bundle: nil)
        commentController.socialInteraction = THHashTagManager.sharedInstance.socialInteractionPosts[sender.tag]
        self.navigationController?.pushViewController(commentController, animated: true)
    }

    @IBAction func didTapOnRethreadButton(_ sender: UIButton) {
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }
        let cell = objTableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! THSocialPostTableViewCell
        let socialInteraction:SocialInteraction = THHashTagManager.sharedInstance.socialInteractionPosts[sender.tag]
        if !cell.rethreadButton.isSelected {
            cell.rethreadButton.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                cell.rethreadButton.transform = CGAffineTransform(rotationAngle: CGFloat(-179.9).degreesToRadians)
            })
            UIView.animate(withDuration: 0.3, delay: 0.2, options: UIViewAnimationOptions(), animations: {
                cell.rethreadButton.transform = CGAffineTransform(rotationAngle: CGFloat(1.1).degreesToRadians)
            }, completion: { (completed) in
                let rethreadpopup = THRethreadPopupViewController(nibName: THRethreadPopupViewController.className, bundle: nil)
                rethreadpopup.productLinkString = "https://www.threadest.com/posts/\(socialInteraction.id)"
                self.addChildViewController(rethreadpopup)
                rethreadpopup.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
                self.view.addSubview(rethreadpopup.view)
                rethreadpopup.didMove(toParentViewController: self)
                rethreadpopup.completionHander = {selectedRethreadArray in
                    for selectedRethread in selectedRethreadArray! {
                        switch selectedRethread as! String {
                        case "Threadest":
                            self.rethreadPost(selectedRow: sender.tag)
                            break
                        case "Facebook":
                            self.rethreadPostOnFacebook(selectedRow: sender.tag)
                            break
                        case "Twitter":
                            self.rethreadPostOnTwitter(socialInteration:THHashTagManager.sharedInstance.socialInteractionPosts[sender.tag], comment: "")
                            break
                        default:
                            break
                        }
                    }
                }
            })
        } else {

        }
    }

    // for Rethread post Threadest
    func rethreadPost(selectedRow: Int) {
        let socialInteraction:SocialInteraction = THHashTagManager.sharedInstance.socialInteractionPosts[selectedRow]

        THSocialInteractionPostsManager.sharedInstance.performRethreadPost(for: socialInteraction.id) { (error) in
            guard let error = error else {
                let socialCell = self.objTableView.cellForRow(at: IndexPath(row: selectedRow, section: 0)) as! THSocialPostTableViewCell
                socialCell.rethreadCountLabel.text = "\(socialInteraction.rethreadCount + 1)"
                socialCell.rethreadCountLabel.isHidden = false
                THHashTagManager.sharedInstance.socialInteractionPosts[selectedRow].rethreadCount = socialInteraction.rethreadCount + 1
                //Add funnel for Rethread Threadest Post
                if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    Answers.logCustomEvent(withName: "Rethread post", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "post_id":"\(socialInteraction.id)", "rethread_type":"Threadest"])
                }
                return
            }
            debugPrint(error)
        }
    }

    // for Rethread post on Facebook
    func rethreadPostOnFacebook(selectedRow: Int) {
        let socialInteraction:SocialInteraction = THHashTagManager.sharedInstance.socialInteractionPosts[selectedRow]
        if FBSDKAccessToken.current() != nil {
            if FBSDKAccessToken.current().hasGranted("publish_actions") {
                GeneralMethods.sharedInstance.publishMessage(message: "https://www.threadest.com/posts/\(socialInteraction.id)", link: socialInteraction.watermarkImageUrl, completion: { () in
                    //Add funnel for Rethread Threadest Post
                    if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                        Answers.logCustomEvent(withName: "Rethread post", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "post_id":"\(socialInteraction.id)", "rethread_type":"Facebook"])
                    }
                    GeneralMethods.sharedInstance.RethreadPostSucessPopup(message: "Post to Facebook - Successful", viewController: self)
                })
            } else {
                requestPublishPermissions(socialInteration:socialInteraction)
            }
        } else {
            requestPublishPermissions(socialInteration:socialInteraction)
        }
    }

    func requestPublishPermissions(socialInteration: SocialInteraction) {
        let login: FBSDKLoginManager = FBSDKLoginManager()

        login.logIn(withPublishPermissions: ["publish_actions"], from: self) { (result, error) in
            if (error != nil) {
                print(error!)
            } else if (result?.isCancelled)! {
                print("Canceled")
            } else if (result?.grantedPermissions.contains("publish_actions"))! {
                print("permissions granted")
                GeneralMethods.sharedInstance.publishMessage(message: "https://www.threadest.com/posts/\(socialInteration.id)", link: socialInteration.largeImageUrl, completion: { () in
                    //Add funnel for Rethread Threadest Post
                    if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                        Answers.logCustomEvent(withName: "Rethread post", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "post_id":"\(socialInteration.id)", "rethread_type":"Facebook"])
                    }
                    GeneralMethods.sharedInstance.RethreadPostSucessPopup(message: "Post to Facebook - Successful", viewController: self)
                })
            }
        }
    }


    func rethreadPostOnTwitter(socialInteration: SocialInteraction, comment: String) {
        if let twitterAccountId = TWTRTwitter.sharedInstance().sessionStore.session() {
            GeneralMethods.sharedInstance.uploadTwitterMedia(viewController: self, imageURL: socialInteration.watermarkImageUrl, status: "https://www.threadest.com/posts/\(socialInteration.id) \n \(comment)", token: twitterAccountId.authToken, userId: twitterAccountId.userID, id: socialInteration.id, type: "Post")
        } else {
            // Not logged in
            TWTRTwitter.sharedInstance().logIn { (session, error) in
                if let _ = session {
                    if let twitterAccountId = TWTRTwitter.sharedInstance().sessionStore.session() {
                        GeneralMethods.sharedInstance.uploadTwitterMedia(viewController: self, imageURL: socialInteration.watermarkImageUrl, status: "https://www.threadest.com/posts/\(socialInteration.id) \n \(comment)", token: twitterAccountId.authToken, userId: twitterAccountId.userID, id: socialInteration.id, type: "Post")
                    }
                } else {
                    print("error: \(error?.localizedDescription)");
                }
            }
        }
    }


    @IBAction func didTapOnLikeButton(_ sender: UIButton) {
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }
        if !sender.isSelected {
            let cell = objTableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! THSocialPostTableViewCell
            sender.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                sender.transform = CGAffineTransform(rotationAngle: CGFloat(-179.9).degreesToRadians)
            })
            UIView.animate(withDuration: 0.3, delay: 0.2, options: UIViewAnimationOptions(), animations: {
                sender.transform = CGAffineTransform(rotationAngle: CGFloat(1.1).degreesToRadians)
            }, completion: { (completed) in
                if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    THUserManager.sharedInstance.performUpvote(for: usercurrent.userId, votableId: THHashTagManager.sharedInstance.socialInteractionPosts[sender.tag].id, votableType: "Post", completion: { (vote, error) in
                        guard let error = error else {
                            THHashTagManager.sharedInstance.socialInteractionPosts[sender.tag].likedByCurrentUser = true
                            THHashTagManager.sharedInstance.socialInteractionPosts[sender.tag].upvotesCount = THHashTagManager.sharedInstance.socialInteractionPosts[sender.tag].upvotesCount + 1
                            THHashTagManager.sharedInstance.socialInteractionPosts[sender.tag].votes.append(vote!)

                            cell.upvoteCountLabel.text = "\(THHashTagManager.sharedInstance.socialInteractionPosts[sender.tag].upvotesCount)"
                            cell.upvoteCountLabel.isHidden = false
                            sender.isSelected  = true
                            Answers.logCustomEvent(withName: "Like post", customAttributes: ["userid":usercurrent.userId,"username": usercurrent.username, "post_Id":"\(THHashTagManager.sharedInstance.socialInteractionPosts[sender.tag].id)"])
                            return
                        }
                    })
                }
                else {

                }
            })
        }
    }

    @IBAction func didTapOnProfileUserNameButton(_ sender: UIButton) {
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        let socialInteraction = THHashTagManager.sharedInstance.socialInteractionPosts[sender.tag]
        THProfilePostsManager.sharedInstance.profilePosts = [SocialInteraction]()
        THProfilePostsManager.sharedInstance.page = 1
        THProfilePostsManager.sharedInstance.didLoadData = false
        THProfilePostsManager.sharedInstance.retrieveProfilePosts(with: Int(socialInteraction.userId)) { (error) in
            guard error != nil else {
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let userPostProfileVC = storyBoard.instantiateViewController(withIdentifier: PZProfileTableViewController.className) as! PZProfileTableViewController
                userPostProfileVC.profile = THProfilePostsManager.sharedInstance.profile
                userPostProfileVC.userId = socialInteraction.userId
                let navProfilePostVC = UINavigationController(rootViewController: userPostProfileVC)
               self.present(navProfilePostVC, animated: true, completion: nil)
                return
            }
        }
    }

    @IBAction func didTapOnDeletePostButton(_ sender: UIButton) {

        let alert = UIAlertController(title: "Threadest", message: "Are you sure you want to remove post?", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            let socialInteraction = THHashTagManager.sharedInstance.socialInteractionPosts[sender.tag]
            THProfilePostsManager.sharedInstance.deletePost(with: Int(socialInteraction.id)) { (error) in
                guard error != nil else {
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    THHashTagManager.sharedInstance.socialInteractionPosts.remove(at: sender.tag)
                    self.objTableView.reloadData()
                    return
                }
            }
        }))

        alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: { (action) -> Void in

        }))

        self.present(alert, animated: true, completion: nil)
    }


    func pinchZoomSocialPostImage(sender: UIPinchGestureRecognizer) {
        let tapLocation = sender.location(in: self.objTableView)
        if let indexPath : IndexPath = self.objTableView.indexPathForRow(at: tapLocation) {
            if let shareCell = self.objTableView.cellForRow(at: indexPath) as? THSocialPostTableViewCell {
                    TMImageZoom.shared().gestureStateChanged(sender, withZoom: shareCell.socialPostImage)
            } else {
                TMImageZoom.shared().resetImageZoom()
            }
        } else {
            TMImageZoom.shared().resetImageZoom()
        }

    }


}

extension THHashTagViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THProfilePostsManager.sharedInstance.retrieveNotificationPost(with: THHashTagManager.sharedInstance.socialInteractionPosts[indexPath.row].id, completion: { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            if THProfilePostsManager.sharedInstance.notificationPost == nil {
                return
            }
            guard let error = error else {
                let commentController = THCommentViewController(nibName: THCommentViewController.className, bundle: nil)
                commentController.socialInteraction = THProfilePostsManager.sharedInstance.notificationPost
                self.navigationController?.pushViewController(commentController, animated: true)
                return
            }
        })
    }
}


extension THHashTagViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return THHashTagManager.sharedInstance.socialInteractionPosts.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        //Really bad code here
        let shareDetailCell = tableView.dequeueReusableCell(withIdentifier: THSocialPostTableViewCell.className, for: indexPath) as! THSocialPostTableViewCell
        let socialInteraction = THHashTagManager.sharedInstance.socialInteractionPosts[indexPath.row] as SocialInteraction

        if let currentUser = THUserManager.sharedInstance.currentUserdata() {
            if socialInteraction.userId == currentUser.userId {
                shareDetailCell.trashButton.isHidden = false
                shareDetailCell.lblTrailingConstant.constant = 32
            }
            else {
                shareDetailCell.trashButton.isHidden = true
                shareDetailCell.lblTrailingConstant.constant = 5
            }
        }

        shareDetailCell.selectionStyle = UITableViewCellSelectionStyle.none
        shareDetailCell.likeButton.tag = indexPath.row
        shareDetailCell.commentButton.tag = indexPath.row
        shareDetailCell.profile_nameButton.tag = indexPath.row
        shareDetailCell.commentButton.addTarget(self, action: #selector(THHashTagViewController.didTapOnCommentButton(_:)), for: UIControlEvents.touchUpInside)
        shareDetailCell.rethreadButtonTop.tag = indexPath.row
        shareDetailCell.rethreadButtonTop.addTarget(self, action: #selector(THHashTagViewController.didTapOnRethreadButton(_:)), for: .touchUpInside)
        shareDetailCell.likeButton.tag = indexPath.row
        shareDetailCell.likeButton.addTarget(self, action: #selector(THHashTagViewController.didTapOnLikeButton(_:)), for: .touchUpInside)
        shareDetailCell.configure(withSocialInteraction: socialInteraction)

        shareDetailCell.trashButton.tag = indexPath.row
        shareDetailCell.trashButton.addTarget(self, action: #selector(THHashTagViewController.didTapOnDeletePostButton(_:)), for: UIControlEvents.touchUpInside)

        shareDetailCell.profile_nameButton.addTarget(self, action: #selector(THHashTagViewController.didTapOnProfileUserNameButton(_:)), for: UIControlEvents.touchUpInside)

        shareDetailCell.clearSocialSubview()


        let imgHeight = CGFloat(socialInteraction.largeImageHeight)
        let imgWidth = CGFloat(socialInteraction.largeImageWidth)
        shareDetailCell.heightConstraintImage.constant = (imgHeight/imgWidth) * screenwidth
        shareDetailCell.circularProgressView.isHidden = false

        let pinchZoomSocilPostImage = UIPinchGestureRecognizer.init(target: self, action: #selector(THCommentViewController.pinchZoomSocialPostImage(sender:)))
        shareDetailCell.socialPostImage.addGestureRecognizer(pinchZoomSocilPostImage)

        shareDetailCell.socialPostImage.sd_setImage(with: URL(string: socialInteraction.largeImageUrl), placeholderImage: nil, options: .retryFailed, progress: { (block1, block2, imageurl) in
            let progress = Double(block1)/Double(block2)
            DispatchQueue.main.async {
                shareDetailCell.circularProgressView.setProgress(progress, animated: true)
            }
        }) { (image, error, imageCacheType, imageUrl) in
            if image != nil {
                shareDetailCell.circularProgressView.isHidden = true
            }
        }

        return shareDetailCell
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 320
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

