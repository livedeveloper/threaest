//
//  THStartViewController.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 05/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import Crashlytics
import AVKit

class THStartViewController: UIViewController {
    var player: AVPlayer!
    //the bulletin manager.
    var bulletinManager: BulletinManager!

    @IBOutlet weak var startBackgroundImageView: UIImageView!

    override func viewDidLoad() {

        DispatchQueue.main.async {
            self.player = AVPlayer(url: URL(string: "https://s3.amazonaws.com/threadest/threadest-intro-7.mp4")!)
            let playerLayer = AVPlayerLayer(player: self.player)
            playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
            playerLayer.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
            self.startBackgroundImageView.alpha = 0.9
            self.startBackgroundImageView.layer.addSublayer(playerLayer)
            playerLayer.zPosition = -1
            self.player.seek(to: kCMTimeZero)
            self.player.actionAtItemEnd = .none
            self.player.play()
            NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.player.currentItem, queue: nil, using: { (_) in
                DispatchQueue.main.async {
                    let t1 = CMTimeMake(0, 1);
                    self.player.seek(to: t1)
                    self.player.play()
                } })
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        if self.player != nil {
            if !self.player.isPlaying {
                self.player.play()
            }
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        if self.player != nil {
            if self.player.isPlaying {
                self.player.pause()
            }
        }
    }
    @IBAction func didTapSignUpEmail(sender: UIButton) {
       showEmailSignUpController(usingFacebookAuth: false)
        //present(emailSignUpController, animated: true, completion: nil)
    }

    @IBAction func didTapOnStartButton(_ sender: Any) {
        bulletinManager = BulletinManager(rootItem: GeneralMethods.sharedInstance.bulletinAlradyAccount(yesCompletion: {
            self.bulletinManager.dismissBulletin(animated: true, completion: {
                let loginViewController = THLoginViewController(nibName: THLoginViewController.className, bundle: nil)
                self.navigationController?.pushViewController(loginViewController, animated: true)
            })
        }, noCompletion: {
            self.bulletinManager.dismissBulletin(animated: true, completion: {
                GeneralMethods.sharedInstance.loadLoading(view: self.view)
                let completion: UserManagerGenericCompletion = { (error) in
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)

                    if let error = error {
                        self.showAlert(withMessage: error.localizedDescription)
                        return
                    } else {
                        //Add funnel for start button
                        Answers.logCustomEvent(withName: "Start", customAttributes: [:])
                        appdelegate.swipeNavigationView()
                    }
                }
                let registration = Registration(username: "", password: "", passwordConfirmation: "", mobilePhoneNumber: "", email: "")
                THUserManager.sharedInstance.registerUser(with: registration, tempAccount: true, completion: completion)
            })
        }))
        if #available(iOS 10, *) {
            self.bulletinManager.backgroundViewStyle = .blurred(style: .light, isDark: false)
        } else {
            self.bulletinManager.backgroundViewStyle = .dimmed
        }
        self.bulletinManager.prepare()
        self.bulletinManager.presentBulletin(above: self)
    }


    @IBAction func didTapAlreadyAccount(_ sender: Any) {
        let loginViewController = THLoginViewController(nibName: THLoginViewController.className, bundle: nil)
        navigationController?.pushViewController(loginViewController, animated: true)
    }

    @IBAction func didTapForBrandDesigner(sender: UIButton) {
        let loginViewController = THLoginViewController(nibName: THLoginViewController.className, bundle: nil)
        navigationController?.pushViewController(loginViewController, animated: true)
    }

    @IBAction func didTapOnConnectFacebook(_ sender: Any) {
        THUserManager.sharedInstance.performFacebookAuth(forViewController: self) { (userExists, authToken, emailAddress, userId, gender, error) in
            if let error = error {
                if userId != "" && gender != "" && authToken != "" {
                    let alertController = UIAlertController(title: "Email", message: "Please enter your email:", preferredStyle: .alert)
                    let confirmAction = UIAlertAction(title: "Confirm", style: .default) { (_) in
                        if let field = alertController.textFields![0] as? UITextField {
                            // store your data
                            guard let emailAddress = field.text, emailAddress.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
                                return
                            }

                            self.checkUserAlreadyExisted(token: authToken, userid: userId, emailAddress: emailAddress, gender: gender)
                        } else {
                            // user did not fill field
                        }
                    }
                    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
                    alertController.addTextField { (textField) in
                        textField.placeholder = "Email"
                    }
                    alertController.addAction(confirmAction)
                    alertController.addAction(cancelAction)
                    self.present(alertController, animated: true, completion: nil)
                } else {
                    self.showAlert(withMessage: error.localizedDescription)
                }
            } else {
                if let userExists = userExists {
                    if userExists {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.swipeNavigationView()
                    } else {
                        self.showEmailSignUpController(usingFacebookAuth: true, authToken: authToken, emailAddress: emailAddress, userId: userId, gender: gender)
                    }
                } else {
                    self.showAlert(withMessage: "There are some unexpected issues in our system. Please try again.")
                }
            }
        }
    }

    func showEmailSignUpController(usingFacebookAuth shouldUseFacebookAuth: Bool, authToken: String = "", emailAddress: String = "", userId: String = "", gender: String = "") {
        let emailSignUpController = THEmailSignUpViewController(nibName: THEmailSignUpViewController.className, bundle: nil)

        emailSignUpController.shouldUseFacebookAuth = shouldUseFacebookAuth
        emailSignUpController.facebookAuthToken = authToken
        emailSignUpController.facebookEmailAddress = emailAddress
        emailSignUpController.facebookUserID = userId
        emailSignUpController.facebookGender = gender

        navigationController?.pushViewController(emailSignUpController, animated: true)
    }

    func checkUserAlreadyExisted(token: String, userid: String, emailAddress: String, gender: String) {
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THUserManager.sharedInstance.performCheckIfAuthUserExists(with: emailAddress, authToken: token, userID: userid, completion: { (userExists, err) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            // Request fails
            if let err = err {
                print(err.localizedDescription)
                return
            }

            // Request succeeds
            if let userExists = userExists {
                if userExists {
                    // Regular flow -> user exists, go to closet - token saved in performCheck endpoint
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.swipeNavigationView()
                } else {
                    // Redirect to register
                    self.showEmailSignUpController(usingFacebookAuth: true, authToken: token, emailAddress: emailAddress, userId: userid, gender: gender)
                }
            } else {

            }
        })
    }
}

extension THStartViewController: OnboardingPageDelegate {
    func onboardingPageIndex(page: Int) {
        if self.player == nil {
            return
        }
        if page == 0 {
            if !self.player.isPlaying {
                self.player.play()
            }
        } else {
            if self.player.isPlaying {
                self.player.pause()
            }
        }
    }
}


