//
//  THImageCroppingViewController.swift
//  Threadest
//
//  Created by Jaydeep on 25/09/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import AKImageCropperView

class THImageCroppingViewController: UIViewController {
    //  MARK: - Properties
    
    var image: UIImage!
    
    // MARK: - Connections:
    
    // MARK: -- Outlets
    
    private var cropView: AKImageCropperView {
        return cropViewStoryboard
    }
    
    open var completionHander : ((_ createProduxt: CreateProduct, _ isfromCamera: String)->())?
    open var CroppingCompletionHander: ((_ image: UIImage) -> ())?
    
    @IBOutlet weak var cropViewStoryboard: AKImageCropperView!
    private var cropViewProgrammatically: AKImageCropperView!
    
    @IBOutlet weak var navigationView: UIView!
    
    var isVisionCamera:Bool = false
    
    open var yassi : ((_ croppedImage: UIImage)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.isNavigationBarHidden = true
        
        // Programmatically initialization
        
        /*
         cropViewProgrammatically = AKImageCropperView()
         */
        
        // iPhone 4.7"
        
        /*
         cropViewProgrammatically = AKImageCropperView(frame: CGRect(x: 0, y: 20.0, width: 375.0, height: 607.0))
         view.addSubview(cropViewProgrammatically)
         */
        
        // with constraints
        
        /*
         cropViewProgrammatically = AKImageCropperView()
         cropViewProgrammatically.translatesAutoresizingMaskIntoConstraints = false
         view.addSubview(cropViewProgrammatically)
         
         if #available(iOS 9.0, *) {
         
         cropViewProgrammatically.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
         cropViewProgrammatically.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
         topLayoutGuide.bottomAnchor.constraint(equalTo: cropViewProgrammatically.topAnchor).isActive = true
         cropViewProgrammatically.bottomAnchor.constraint(equalTo: navigationView.topAnchor).isActive = true
         
         } else {
         
         for attribute: NSLayoutAttribute in [.top, .left, .bottom, .right] {
         
         var toItem: Any?
         var toAttribute: NSLayoutAttribute!
         
         if attribute == .top {
         
         toItem = topLayoutGuide
         toAttribute = .bottom
         
         } else if attribute == .bottom {
         
         toItem = navigationView
         toAttribute = .top
         } else {
         toItem = view
         toAttribute = attribute
         }
         
         view.addConstraint(
         NSLayoutConstraint(
         item: cropViewProgrammatically,
         attribute: attribute,
         relatedBy: NSLayoutRelation.equal,
         toItem: toItem,
         attribute: toAttribute,
         multiplier: 1.0, constant: 0))
         }
         }
         */
        
        
        // Inset for overlay action view
        
        /*
         cropView.overlayView?.configuraiton.cropRectInsets.bottom = 50
         */
        
        // Custom overlay view configuration
        
        /*
         var customConfiguraiton = AKImageCropperCropViewConfiguration()
         customConfiguraiton.cropRectInsets.bottom = 50
         cropView.overlayView = CustomImageCropperOverlayView(configuraiton: customConfiguraiton)
         */
        
        cropView.delegate = self
        cropView.image = image
        self.showHideOverlayAction(self)
    }
    
    // MARK: -- Actions
    
    @IBAction func backAction(_ sender: AnyObject) {
        self.dismiss(animated: true) {
            
        }
    }
    
    @IBAction func cropRandomAction(_ sender: AnyObject) {
        //        cropView.setCropRectAnin(CGRect(x: 50, y: 200, width: 100, height: 100))
        /*
         let randomWidth = max(UInt32(cropView.configuration.cropRect.minimumSize.width), arc4random_uniform(UInt32(cropView.scrollView.frame.size.width)))
         let randomHeight = max(UInt32(cropView.configuration.cropRect.minimumSize.height), arc4random_uniform(UInt32(cropView.scrollView.frame.size.height)))
         let offsetX = CGFloat(arc4random_uniform(UInt32(cropView.scrollView.frame.size.width) - randomWidth))
         let offsetY = CGFloat(arc4random_uniform(UInt32(cropView.scrollView.frame.size.height) - randomHeight))
         cropView.cropRect(CGRectMake(offsetX, offsetY, CGFloat(randomWidth), CGFloat(randomHeight)))*/
    }
    
    
    @IBAction func cropImageAction(_ sender: AnyObject) {
        guard let croppedImage = cropView.croppedImage else {
            return
        }
        
        if isVisionCamera {
            self.dismiss(animated: false) {
                self.CroppingCompletionHander!(croppedImage)
            }
        } else {
            self.CroppingCompletionHander!(croppedImage)
            self.dismiss(animated: true) {
                
            }
        }
        
    }
    
    @IBAction func showHideOverlayAction(_ sender: AnyObject) {
        if cropView.isOverlayViewActive {
            cropView.hideOverlayView(animationDuration: 0.3)
        } else {
            cropView.showOverlayView(animationDuration: 0.3)
            UIView.animate(withDuration: 0.3, delay: 0.3, options: UIViewAnimationOptions.curveLinear, animations: {
            }, completion: nil)
        }
    }
    
    var angle: Double = 0.0
    
    @IBAction func rotateAction(_ sender: AnyObject) {
        angle += M_PI_2
        cropView.rotate(angle, withDuration: 0.3, completion: { _ in
            if self.angle == 2 * M_PI {
                self.angle = 0.0
            }
        })
    }
    
    @IBAction func resetAction(_ sender: AnyObject) {
        cropView.reset(animationDuration: 0.3)
        angle = 0.0
    }
    
    // MARK: -  Life Cycle
}
    

//  MARK: - AKImageCropperViewDelegate

extension THImageCroppingViewController: AKImageCropperViewDelegate {
    
    func imageCropperViewDidChangeCropRect(view: AKImageCropperView, cropRect rect: CGRect) {
        //        print("New crop rectangle: \(rect)")
    }
    
}

