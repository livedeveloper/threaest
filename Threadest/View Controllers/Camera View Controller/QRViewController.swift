//
//  QRViewController.swift
//  Threadest
//
//  Created by Herbert Jason on 2/27/18.
//  Copyright © 2018 JC. All rights reserved.
//

import UIKit
import AVFoundation
import Crashlytics
import ARKit

class QRViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {

    var h_scannedCode = String()
    var h_scannedItemType = String() // Type : found type, return type
    var h_photoFlag = Int() // Value will be 1 or 0
    
    
    @IBOutlet weak var scannerContentView: UIView!
    
    var bStartScan  = false
    var cntOfCalling = 0 // Count of invoker for scanner
    
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    var captureDevice:AVCaptureDevice?
    
    var lastCapturedCode = String()
    
    public var barcodeScanned:((String) -> ())?
    
    private var allowedTypes = [AVMetadataObjectTypeUPCECode,
                                AVMetadataObjectTypeCode39Code,
                                AVMetadataObjectTypeCode39Mod43Code,
                                AVMetadataObjectTypeEAN13Code,
                                AVMetadataObjectTypeEAN8Code,
                                AVMetadataObjectTypeCode93Code,
                                AVMetadataObjectTypeCode128Code,
                                AVMetadataObjectTypePDF417Code,
                                AVMetadataObjectTypeQRCode,
                                AVMetadataObjectTypeAztecCode]
    
    override public func loadView() {
        if let view = UINib(nibName: "QRViewController", bundle: Bundle(for: self.classForCoder)).instantiate(withOwner: self, options: nil).first as? UIView {
            self.view = view
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(QRViewController.Tap))
        self.view.addGestureRecognizer(tapGesture)
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "QR Code View", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }
    }

    @objc fileprivate func Tap() {
        bStartScan = true
    }
    
    public override func viewWillLayoutSubviews() {
        super.viewDidLayoutSubviews()

        // Setup Scanner View
        videoPreviewLayer?.frame = self.scannerContentView.layer.bounds

        let orientation = UIApplication.shared.statusBarOrientation
        
        switch(orientation) {
        case UIInterfaceOrientation.landscapeLeft:
            videoPreviewLayer?.connection.videoOrientation = AVCaptureVideoOrientation.landscapeLeft
            
        case UIInterfaceOrientation.landscapeRight:
            videoPreviewLayer?.connection.videoOrientation = AVCaptureVideoOrientation.landscapeRight
            
        case UIInterfaceOrientation.portrait:
            videoPreviewLayer?.connection.videoOrientation = AVCaptureVideoOrientation.portrait
            
        case UIInterfaceOrientation.portraitUpsideDown:
            videoPreviewLayer?.connection.videoOrientation = AVCaptureVideoOrientation.portraitUpsideDown
            
        default:
            print("Unknown orientation state")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
    
        // Start Scanning
        self.cntOfCalling = 0
        startScanning()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    //MARK: - Scanner
    func startScanning() {
        // Retrieve the default capturing device for using the camera
        self.captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        // Get an instance of the AVCaptureDeviceInput class using the previous device object.
        var error:NSError?
        let input: AnyObject!
        do {
            input = try AVCaptureDeviceInput(device:captureDevice)
        } catch let error1 as NSError {
            error = error1
            input = nil
        }
        
        if (error != nil) {
            // If any error occurs, simply log the description of it and don't continue any more.
            print("\(String(describing: error?.localizedDescription))")
            return
        }
        
        // Initialize the captureSession object and set the input device on the capture session.
        captureSession = AVCaptureSession()
        captureSession?.addInput(input as! AVCaptureInput)
        
        // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
        let captureMetadataOutput = AVCaptureMetadataOutput()
        captureSession?.addOutput(captureMetadataOutput)
        
        // Set delegate and use the default dispatch queue to execute the call back
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        captureMetadataOutput.metadataObjectTypes = self.allowedTypes
        
        // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravityResize
        videoPreviewLayer?.frame = scannerContentView.layer.bounds
        scannerContentView.layer.addSublayer(videoPreviewLayer!)
        
        // Start video capture.
        captureSession?.startRunning()
        
        // Move the message label to the top view
        //        view.bringSubview(toFront: messageLabel)
        
        // Initialize QR Code Frame to highlight the QR code
        qrCodeFrameView = UIView()
        qrCodeFrameView?.layer.borderColor = UIColor.red.cgColor
        qrCodeFrameView?.layer.borderWidth = 2
        qrCodeFrameView?.autoresizingMask = [UIViewAutoresizing.flexibleTopMargin, UIViewAutoresizing.flexibleBottomMargin, UIViewAutoresizing.flexibleLeftMargin, UIViewAutoresizing.flexibleRightMargin]
        
        scannerContentView.addSubview(qrCodeFrameView!)
        scannerContentView.bringSubview(toFront:qrCodeFrameView!)
        
    }
    
    
    //MARK: -  Scanner Delegate
    public func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects == nil || metadataObjects.count == 0 || self.bStartScan == false {
            qrCodeFrameView?.frame = CGRect.zero
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if self.allowedTypes.contains(metadataObj.type) {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj as AVMetadataMachineReadableCodeObject) as! AVMetadataMachineReadableCodeObject
            
            qrCodeFrameView?.frame = barCodeObject.bounds;
            
            if metadataObj.stringValue != nil {
                lastCapturedCode = metadataObj.stringValue!
                print("Scanned barcode: \(lastCapturedCode)")
                
                // Set some delay and show scan bar
                if ( self.bStartScan ){
                    self.cntOfCalling += 1
                }
                
                
                if self.cntOfCalling == 10 {
                    h_scannedCode = lastCapturedCode
                    
                    // Let calling this delegate fucntion only one time
                    captureSession?.stopRunning()
                    videoPreviewLayer?.removeFromSuperlayer()
                    qrCodeFrameView?.removeFromSuperview()
                    
                    //                    self.scanBtn.backgroundColor = UIColor.init(rgb: CONSTANTS.COLOR.COLOR_FOR_NORMAL_BUTTON)
                    //                    self.scanBtn.setTitle("SCAN", for: .normal)
                    //                    self.guideStrLbl.isHidden = false
                    //                    self.isScanning = false
                    
                    // Active Vibrate
                    AudioServicesPlayAlertSound(SystemSoundID(4095))
                    
                    let arr  = lastCapturedCode.components(separatedBy: "/")
                    let last  = arr.last
                    let subArr  = last?.components(separatedBy: "?")
                    let prodId  = subArr?.first
                    let str = subArr?.last
                    let subArr1  = str?.components(separatedBy: "&")
                    let first = subArr1?.first
                    let subArr2 = first?.components( separatedBy: "=")
                    let brandId  = subArr2?.last
                    
                    let nBrandId = Int(brandId!)!
                    let nProdId = Int(prodId!)!
                    
                    if nBrandId == nil || nProdId == nil {
                         let alertVC = UIAlertController(title: "Threadest App", message: "Please Try again, There is no Product.", preferredStyle: UIAlertControllerStyle.alert)
                         alertVC.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                             self.okClick()
                         }))
                        
                         self.present(alertVC, animated: true, completion: nil)
                    }else{
                        THBrandProductManager.sharedInstance.retrieveBrandProfile(with: Int(brandId!)!, productId: Int(prodId!)!, geofence: false) { (error) in
                            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                            let threeDAvailable = THBrandProductManager.sharedInstance.brandProduct?.productGalleryImages.contains(where: { (imgGallery) -> Bool in
                                if imgGallery.image_type == "3D" {
                                    return true
                                } else {
                                    return false
                                }
                            })
                            
                            if threeDAvailable! {
                                if #available(iOS 11.0, *) {
                                    if (ARConfiguration.isSupported) {
                                        let ARShopShowController = THARShopShowViewController(nibName: THARShopShowViewController.className, bundle: nil)
                                        ARShopShowController.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                                        self.navigationController?.pushViewController(ARShopShowController, animated: true)
                                        return
                                    }
                                }
                            }
                            let shopshowcontroller = THShopshowViewController(nibName: THShopshowViewController.className, bundle: nil)
                            shopshowcontroller.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                            self.navigationController?.pushViewController(shopshowcontroller, animated: true)
                        }
                    }
                    
                    
                    

                    return
                }
            }
        }
    }
    
    func okClick() {
        self.dismiss(animated: true, completion: nil)
    }
}
