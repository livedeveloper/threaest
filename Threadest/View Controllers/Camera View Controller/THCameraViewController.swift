//
//  THCameraViewController.swift
//  Threadest
//
//  Created by Sagar R on 09/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import AVFoundation
import Foundation
import Crashlytics


class THCameraViewController: UIViewController {

    // outlet of tool tip view
    @IBOutlet var toolTipView: UIView!
    @IBOutlet weak var toolTipLabel: UILabel!
    var  popover = Popover()

    open var completionHander : ((_ createProduxt: CreateProduct, _ isfromCamera: String)->())?

    @IBOutlet weak var btnCapture: UIButton!
    @IBOutlet weak var viewCameraContainer: UIView!
    @IBOutlet weak var labelBadge: UILabel!
    @IBOutlet weak var buttonFlash: UIButton!

    let captureQueue = OperationQueue()

    var isFrontCamera = false

    let captureSession = AVCaptureSession()
    let stillImageOutput = AVCaptureStillImageOutput()

    var previewLayer : AVCaptureVideoPreviewLayer?

    // If we find a device we'll store it here for later use
    var captureDevice : AVCaptureDevice?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Initialise camera session after view is loaded
        self.initCameraSession()

        //Add funnel for camera button
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Camera Button", customAttributes: ["userid":usercurrent.userId,"username": usercurrent.username])
        }

        labelBadge.layer.cornerRadius = 7
        labelBadge.layer.masksToBounds = true
        if userdefault.object(forKey: userDefaultKey.toolTipCameraLibraryDefault.rawValue) == nil {
            let options = [
                .type(.up),
                .animationIn(0.3),
                .blackOverlayColor(UIColor.clear),
                .color(UIColor.blueTabbar)
                ] as [PopoverOption]
            toolTipView.sizeToFit()
            toolTipLabel.text = "📸 \n Start by uploading or taking a photo! Next: Tag brands & products."
            popover = Popover(options: options, showHandler: nil, dismissHandler: nil)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                self.popover.show(self.toolTipView, fromView: self.btnCapture)
            }
        }
    }

    private func initCameraSession(){

        if let devices = AVCaptureDevice.devices() as? [AVCaptureDevice] {
            // Loop through all the capture devices on this phone
            for device in devices {
                // Make sure this particular device supports video
                if (device.hasMediaType(AVMediaTypeVideo)) {
                    // Finally check the position and confirm we've got the back camera
                    if(device.position == AVCaptureDevicePosition.front) {
                        captureDevice = device
                        if captureDevice != nil {
                            print("Capture device found")
                            self.beginSession()
                        }
                    }
                }
            }
        }
    }

    func beginSession() {

        do {
            try captureSession.addInput(AVCaptureDeviceInput(device: captureDevice))
            stillImageOutput.outputSettings = [AVVideoCodecKey:AVVideoCodecJPEG]
            stillImageOutput.isHighResolutionStillImageOutputEnabled = true

            if captureSession.canAddOutput(stillImageOutput) {
                captureSession.addOutput(stillImageOutput)
            }

        }
        catch {
            print("error: \(error.localizedDescription)")
        }

        self.initCapturelayer()
        captureSession.startRunning()

        btnCapture.addTarget(self, action: #selector(THCameraViewController.actionCameraCapture(_:)), for: .touchUpInside)
        self.view.addSubview(btnCapture)
    }

    func initCapturelayer(){
        guard let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession) else {
            print("no preview layer")
            return
        }
        self.previewLayer = previewLayer
        print(self.view.layer.frame.debugDescription)
        viewCameraContainer.layer.addSublayer(self.previewLayer!)
        self.previewLayer?.frame = UIScreen.main.bounds
    }

    override func viewDidAppear(_ animated: Bool) {
       // captureSession.startRunning()
    }

    override func viewWillAppear(_ animated: Bool) {
        //self.tabBarController?.tabBar.isHidden = true
       // self.tabBarController?.tabBar.frame = CGRect(x: 0, y: screenheight - 49, width: screenwidth, height: 49)
        captureSession.startRunning()
        //        if  previewLayer == nil{
        //
        //            self.initCapturelayer()
        //        }
        // captureSession.startRunning()

    }


    override func viewDidDisappear(_ animated: Bool) {

        captureSession.stopRunning()
//        previewLayer?.removeFromSuperlayer()
//        previewLayer = nil
    }

    func setupConstraintsForCaptureButton() {

        let bottomConstraint = NSLayoutConstraint(item: btnCapture,
                                                  attribute: .bottom,
                                                  relatedBy: .equal,
                                                  toItem: self.view,
                                                  attribute: .bottom,
                                                  multiplier: 1.0,
                                                  constant: -50.0)
        let centerXConstraint = NSLayoutConstraint(item: btnCapture,
                                                   attribute: .centerX,
                                                   relatedBy: .equal,
                                                   toItem: self.view,
                                                   attribute: .centerX,
                                                   multiplier: 1.0,
                                                   constant: 0.0)
        let widthConstraint = NSLayoutConstraint(item: btnCapture, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 100)
        let heightConstraint = NSLayoutConstraint(item: btnCapture, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 100)


        self.view.addConstraints([bottomConstraint,centerXConstraint,widthConstraint,heightConstraint])

    }

    // MARK: Camera


    func actionCameraCapture(_ sender: UIButton) {

        print("Camera button pressed")
        saveToCamera()
    }

    func saveToCamera() {

        if let videoConnection = stillImageOutput.connection(withMediaType: AVMediaTypeVideo) {

            stillImageOutput.captureStillImageAsynchronously(from: videoConnection, completionHandler: { (CMSampleBuffer, Error) in
                if let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(CMSampleBuffer) {

                    if let cameraImage = UIImage(data: imageData ) {
                        let image = UIImage(cgImage: cameraImage.cgImage!, scale: 2.0, orientation: self.isFrontCamera == true ? .leftMirrored : .right)

                        let tHCreateProductVC = THCreateProductVC(nibName: THCreateProductVC.className, bundle: nil)
                        tHCreateProductVC.image = image
                        let navigationController = UINavigationController(rootViewController: tHCreateProductVC)
                        navigationController.isNavigationBarHidden = true
                        tHCreateProductVC.completionHander = {
                            object, isfrom in
                            self.dismiss(animated: true, completion: {
                                 self.completionHander!(object, isfrom)
                            })
                        }

                        DispatchQueue.main.async {
                            self.present(navigationController, animated: true, completion: {
                            });
                        }
                    }
                }
            })
        }
    }

    func getFrontCamera() -> AVCaptureDevice? {
        if let devices = AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo) as? [AVCaptureDevice]{

            for device in devices{

                if device.position == .front{
                    return device
                }
            }

        }
        return nil;
    }

    func updateFlashlightState(){


        if let input = captureSession.inputs.first as? AVCaptureDeviceInput, let inputDevice = input.device{

            if inputDevice.hasFlash && inputDevice.isFlashAvailable{
                do{
                    try inputDevice.lockForConfiguration()

                    if buttonFlash.isSelected{
                        buttonFlash.isSelected = false;
                        inputDevice.flashMode = .off
                    }else{
                        buttonFlash.isSelected = true;
                        inputDevice.flashMode = .on
                    }


                    inputDevice.unlockForConfiguration()

                }catch{

                }
            }else{
                print("flash not available")
            }

        }else{
            print("device not available")
        }
    }

    // MARK: Actions

    @IBAction func onHelpTapped(_ sender: UIButton) {

        updateFlashlightState()
    }


    @IBAction func didTapOnToolTipCloseButton(_ sender: Any) {
        popover.dismiss()
        userdefault.set("true", forKey: userDefaultKey.toolTipCameraLibraryDefault.rawValue)
        userdefault.synchronize()
    }

    @IBAction func didTapOnToolTipNextButton(_ sender: UIButton) {
        popover.dismiss()
        userdefault.set("true", forKey: userDefaultKey.toolTipCameraLibraryDefault.rawValue)
        userdefault.synchronize()
        let fusuma = FusumaViewController()
        fusuma.delegate = self
        fusuma.hasVideo = false // If you want to let the users allow to use video.
        fusuma.completionHander = {
            object, isfrom in
            self.dismiss(animated: true, completion: {
                self.completionHander!(object, isfrom)
            })
        }
        self.present(fusuma, animated: true) {

        }
    }


    @IBAction func onCameraFlipTapped(_ sender: UIButton) {

        let operation = BlockOperation.init(block: {

            let frontCamera = self.getFrontCamera()
            let backCamera = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
            var newCamera : AVCaptureDevice?

            if let input = self.captureSession.inputs.first as? AVCaptureDeviceInput {

                if input.device.position == .back {
                    newCamera = frontCamera
                    self.isFrontCamera = true

                }else{
                    newCamera = backCamera
                    self.isFrontCamera = false
                }

                DispatchQueue.main.async {
                    self.captureSession.removeInput(input)

                    do{
                        let revisedInput = try AVCaptureDeviceInput(device: newCamera)
                        if (self.captureSession.inputs.count > 0){

                        }
                        else{
                            self.captureSession.addInput(revisedInput)

                            if newCamera?.hasFlash == true && newCamera?.isFlashAvailable == true{
                                do{
                                    try newCamera?.lockForConfiguration()

                                    if self.buttonFlash.isSelected{
                                        newCamera?.flashMode = .on
                                    }else{
                                        newCamera?.flashMode = .off
                                    }
                                    newCamera?.unlockForConfiguration()

                                }catch{

                                }
                            }else{
                                print("flash not available")
                            }
                        }
                    }
                    catch{
                        print("Error in caoture device")
                    }
                }
            }


        })

        operation.completionBlock = {
            self.captureSession.startRunning()
        }
        operation.queuePriority = .veryHigh
        captureQueue.addOperation(operation)

        UIView.transition(with: viewCameraContainer, duration: 1.0, options: [.transitionFlipFromLeft , .allowAnimatedContent], animations: {

        }) { (finished) in

        }
    }

    @IBAction func didTapOnBackButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onGalleryTapped(_ sender: UIButton) {
        let fusuma = FusumaViewController()
        let navFusuma = UINavigationController(rootViewController: fusuma)
        navFusuma.setNavigationBarHidden(true, animated: false)
        fusuma.delegate = self
        fusuma.hasVideo = false // If you want to let the users allow to use video.

        fusuma.completionHander = {
            object, isfrom in
            self.dismiss(animated: true, completion: {
                self.completionHander!(object, isfrom)
            })
        }
        self.present(navFusuma, animated: true) {

        }
    }

    func navigateToFilterScreen(selectedImage:UIImage){

        let image = UIImage(cgImage: selectedImage.cgImage!, scale: 2.0, orientation: self.isFrontCamera == true ? .rightMirrored : .right)

        let tHCreateProductVC = THCreateProductVC(nibName: THCreateProductVC.className, bundle: nil)
        tHCreateProductVC.image = image
        let navigationController = UINavigationController(rootViewController: tHCreateProductVC)
        navigationController.isNavigationBarHidden = true
        self.present(navigationController, animated: true, completion: {
                    });
    }




}

extension THCameraViewController:FusumaDelegate{


    // MARK: Fusuma Delegate

    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
        switch source {
        case .camera:
            print("Image captured from Camera")
        case .library:
            print("Image selected from Camera Roll")
            self.navigateToFilterScreen(selectedImage: image)
        default:
            print("Image selected")
        }

    }



    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        print("video completed and output to file: \(fileURL)")
    }

    func fusumaDismissedWithImage(_ image: UIImage, source: FusumaMode) {
        switch source {
        case .camera:
            print("Called just after dismissed FusumaViewController using Camera")
        case .library:
            print("Called just after dismissed FusumaViewController using Camera Roll")
        default:
            print("Called just after dismissed FusumaViewController")
        }
    }

    func fusumaCameraRollUnauthorized() {

        print("Camera roll unauthorized")

        let alert = UIAlertController(title: "Access Requested", message: "Saving image needs to access your photo album", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { (action) -> Void in

            if let url = URL(string:UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(url)
            }

        }))

        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in

        }))

        self.present(alert, animated: true, completion: nil)
    }

    func fusumaClosed() {
        print("Called when the FusumaViewController disappeared")
    }

    func fusumaWillClosed() {
        print("Called when the close button is pressed")
    }



}

extension THCameraViewController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .overFullScreen
    }

    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        // updateSettings()
    }
}
