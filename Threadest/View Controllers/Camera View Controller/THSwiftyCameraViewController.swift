//
//  THSwiftyCameraViewController.swift
//  Threadest
//
//  Created by Jaydeep on 04/12/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import Crashlytics
import TOCropViewController

class THSwiftyCameraViewController: SwiftyCamViewController {
    // outlet of tool tip view
    @IBOutlet var toolTipView: UIView!
    @IBOutlet weak var toolTipLabel: UILabel!
    var  popover = Popover()
    var isVisionCamera: Bool = false
    
    open var completionHander : ((_ createProduxt: CreateProduct, _ isfromCamera: String)->())?
    open var imageCompletionHandler : ((_ image: UIImage) -> ())?
    
    @IBOutlet weak var captureButton: SwiftyRecordButton!
    @IBOutlet weak var flipCameraButton: UIButton!
    @IBOutlet weak var flashButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Add funnel for camera button
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Camera Button", customAttributes: ["userid":usercurrent.userId,"username": usercurrent.username])
        }
        cameraDelegate = self
        maximumVideoDuration = 60.0
        shouldUseDeviceOrientation = true
        allowAutoRotate = true
        audioEnabled = true
        
        if userdefault.object(forKey: userDefaultKey.toolTipCameraLibraryDefault.rawValue) == nil {
            let options = [
                .type(.up),
                .animationIn(0.3),
                .blackOverlayColor(UIColor.clear),
                .color(UIColor.blueTabbar)
                ] as [PopoverOption]
            toolTipView.sizeToFit()
            toolTipLabel.text = "📸 \n Start by uploading or taking a photo! Next: Tag brands & products."
            popover = Popover(options: options, showHandler: nil, dismissHandler: nil)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                self.popover.show(self.toolTipView, fromView: self.captureButton)
            }
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        captureButton.delegate = self
    }
    
    
    @IBAction func cameraSwitchTapped(_ sender: Any) {
        switchCamera()
    }
    
    @IBAction func toggleFlashTapped(_ sender: Any) {
        flashEnabled = !flashEnabled
        
        if flashEnabled == true {
            flashButton.setImage(#imageLiteral(resourceName: "flash"), for: UIControlState())
        } else {
            flashButton.setImage(#imageLiteral(resourceName: "flashOutline"), for: UIControlState())
        }
    }
    
    func presentCropViewController(image: UIImage) {
        let cropViewController = TOCropViewController(croppingStyle: TOCropViewCroppingStyle.default, image: image)
        cropViewController.delegate = self
        present(cropViewController, animated: true, completion: nil)
    }
    
    @IBAction func didTapOnGalleryButton(_ sender: Any) {
        let fusuma = FusumaViewController()
        let navFusuma = UINavigationController(rootViewController: fusuma)
        navFusuma.setNavigationBarHidden(true, animated: false)
        fusuma.delegate = self
        fusuma.hasVideo = false // If you want to let the users allow to use video.
        if isVisionCamera {
            fusuma.isVisionCamera = true
            fusuma.isVisionImageCompletionHandler = {
                image in
                self.dismiss(animated: false, completion: {
                    self.imageCompletionHandler!(image)
                })
               /* let imageCroppingVC = THImageCroppingViewController(nibName: THImageCroppingViewController.className, bundle: nil)
                imageCroppingVC.image = image
                imageCroppingVC.isVisionCamera = true
                imageCroppingVC.CroppingCompletionHander = {
                    cropimage in
                    self.dismiss(animated: false, completion: {
                        self.imageCompletionHandler!(cropimage)
                    })
                }
                self.present(imageCroppingVC, animated: false, completion: nil)*/
                //self.presentCropViewController(image: image)
            }
        } else {
            fusuma.completionHander = {
                object, isfrom in
                if let navSocial = self.tabBarController?.viewControllers?.first as? UINavigationController {
                    if let thsocial = navSocial.viewControllers[0] as? THSocialViewController {
                        thsocial.productObject = object
                        thsocial.isFromCamera = true
                        self.tabBarController?.selectedIndex = 0
                    }
                    
                }
            }
        }
        self.present(navFusuma, animated: true) {
            
        }
    }
    
    
    @IBAction func didTapOnQRCode(_ sender: Any) {
        let qr = QRViewController()
        let navQR = UINavigationController(rootViewController: qr)
        navQR.setNavigationBarHidden(true, animated: false)
        self.present(navQR, animated: true, completion: nil)
    }
    
    @IBAction func didTapOnToolTipCloseButton(_ sender: Any) {
        popover.dismiss()
        userdefault.set("true", forKey: userDefaultKey.toolTipCameraLibraryDefault.rawValue)
        userdefault.synchronize()
    }
    
    @IBAction func didTapOnToolTipNextButton(_ sender: UIButton) {
        popover.dismiss()
        userdefault.set("true", forKey: userDefaultKey.toolTipCameraLibraryDefault.rawValue)
        userdefault.synchronize()
        let fusuma = FusumaViewController()
        fusuma.delegate = self
        fusuma.hasVideo = false // If you want to let the users allow to u
        if isVisionCamera {
            fusuma.isVisionCamera = true
            fusuma.isVisionImageCompletionHandler = {
                image in
                self.imageCompletionHandler!(image)
            }
        } else {
            fusuma.completionHander = {
                object, isfrom in
                if let navSocial = self.tabBarController?.viewControllers?.first as? UINavigationController {
                    if let thsocial = navSocial.viewControllers[0] as? THSocialViewController {
                        thsocial.productObject = object
                        thsocial.isFromCamera = true
                        self.tabBarController?.selectedIndex = 0
                    }
                    
                }
            }
        }
        
        self.present(fusuma, animated: true) {
            
        }
    }
    
    
    @IBAction func didTapOnCloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func navigateToFilterScreen(selectedImage:UIImage){
        let image = UIImage(cgImage: selectedImage.cgImage!, scale: 2.0, orientation: self.currentCamera == .front ? .rightMirrored : .right)
        
        let tHCreateProductVC = THCreateProductVC(nibName: THCreateProductVC.className, bundle: nil)
        tHCreateProductVC.image = image
        let navigationController = UINavigationController(rootViewController: tHCreateProductVC)
        navigationController.isNavigationBarHidden = true
        self.present(navigationController, animated: true, completion: {
        });
    }
    

}

extension THSwiftyCameraViewController: TOCropViewControllerDelegate {
    func cropViewController(_ cropViewController: TOCropViewController, didCropToImage image: UIImage, rect cropRect: CGRect, angle: Int) {
       // DispatchQueue.main.async {
           
            cropViewController.dismiss(animated: false) {
            }
            self.dismiss(animated: false, completion: {
                let images = UIImage(cgImage: (image.cgImage)!, scale: 2.0, orientation:  .up)
                 self.imageCompletionHandler!(images)
            })
       // }
    }
}

extension THSwiftyCameraViewController: SwiftyCamViewControllerDelegate {
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didTake photo: UIImage) {
        let image = UIImage(cgImage: photo.cgImage!, scale: 2.0, orientation: self.currentCamera == .front ? .leftMirrored : .right)
        
        if isVisionCamera {
            self.presentCropViewController(image: image)
            /*let imageCroppingVC = THImageCroppingViewController(nibName: THImageCroppingViewController.className, bundle: nil)
            imageCroppingVC.image = image
            imageCroppingVC.isVisionCamera = true
            imageCroppingVC.CroppingCompletionHander = {
                cropimage in
                self.dismiss(animated: false, completion: {
                    self.imageCompletionHandler!(cropimage)
                })
            }
            self.present(imageCroppingVC, animated: false, completion: nil)*/
        } else {
            let tHCreateProductVC = THCreateProductVC(nibName: THCreateProductVC.className, bundle: nil)
            tHCreateProductVC.image = image
            let navigationController = UINavigationController(rootViewController: tHCreateProductVC)
            navigationController.isNavigationBarHidden = true
            tHCreateProductVC.completionHander = {
                object, isfrom in
                if let navSocial = self.tabBarController?.viewControllers?.first as? UINavigationController {
                    if let thsocial = navSocial.viewControllers[0] as? THSocialViewController {
                        thsocial.productObject = object
                        thsocial.isFromCamera = true
                        self.tabBarController?.selectedIndex = 0
                    }
                    
                }
            }
            DispatchQueue.main.async {
                self.present(navigationController, animated: false, completion: {
                });
            }
        }
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didBeginRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
        print("Did Begin Recording")
        captureButton.growButton()
        UIView.animate(withDuration: 0.25, animations: {
            self.flashButton.alpha = 0.0
            self.flipCameraButton.alpha = 0.0
        })
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
        print("Did finish Recording")
        captureButton.shrinkButton()
        UIView.animate(withDuration: 0.25, animations: {
            self.flashButton.alpha = 1.0
            self.flipCameraButton.alpha = 1.0
        })
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishProcessVideoAt url: URL) {
        let newVC = VideoViewController(videoURL: url)
        self.present(newVC, animated: true, completion: nil)
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFocusAtPoint point: CGPoint) {
        let focusView = UIImageView(image: #imageLiteral(resourceName: "focus"))
        focusView.center = point
        focusView.alpha = 0.0
        view.addSubview(focusView)
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseInOut, animations: {
            focusView.alpha = 1.0
            focusView.transform = CGAffineTransform(scaleX: 1.25, y: 1.25)
        }, completion: { (success) in
            UIView.animate(withDuration: 0.15, delay: 0.5, options: .curveEaseInOut, animations: {
                focusView.alpha = 0.0
                focusView.transform = CGAffineTransform(translationX: 0.6, y: 0.6)
            }, completion: { (success) in
                focusView.removeFromSuperview()
            })
        })
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didChangeZoomLevel zoom: CGFloat) {
        print(zoom)
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didSwitchCameras camera: SwiftyCamViewController.CameraSelection) {
        print(camera)
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFailToRecordVideo error: Error) {
        print(error)
    }
}

extension THSwiftyCameraViewController:FusumaDelegate {
    
    
    // MARK: Fusuma Delegate
    
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
        switch source {
        case .camera:
            print("Image captured from Camera")
        case .library:
            print("Image selected from Camera Roll")
            self.navigateToFilterScreen(selectedImage: image)
        default:
            print("Image selected")
        }
        
    }
    
    
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        print("video completed and output to file: \(fileURL)")
    }
    
    func fusumaDismissedWithImage(_ image: UIImage, source: FusumaMode) {
        switch source {
        case .camera:
            print("Called just after dismissed FusumaViewController using Camera")
        case .library:
            print("Called just after dismissed FusumaViewController using Camera Roll")
        default:
            print("Called just after dismissed FusumaViewController")
        }
    }
    
    func fusumaCameraRollUnauthorized() {
        
        print("Camera roll unauthorized")
        
        let alert = UIAlertController(title: "Access Requested", message: "Saving image needs to access your photo album", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { (action) -> Void in
            
            if let url = URL(string:UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(url)
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func fusumaClosed() {
        print("Called when the FusumaViewController disappeared")
    }
    
    func fusumaWillClosed() {
        print("Called when the close button is pressed")
    }
}
