//
//  THAlbumTableViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 25/06/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THAlbumTableViewCell: UITableViewCell {
    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var image3: UIImageView!

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var countImages: UILabel!
    
}
