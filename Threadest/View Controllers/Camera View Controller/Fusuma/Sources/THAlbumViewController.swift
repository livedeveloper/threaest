//
//  THAlbumViewController.swift
//  Threadest
//
//  Created by Jaydeep on 25/06/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import Photos
class THAlbumViewController: UIViewController {
    var headerString:String = ""
    open var completionHander : ((_ collection: PHAssetCollection)->())?
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var HeaderButton: UIButton!
    
    @IBOutlet weak var objTableview: UITableView!
    var collectionResult:[PHAssetCollection] = [PHAssetCollection]()
    var imageManager: PHCachingImageManager?
     let cellSize = CGSize(width: 80, height: 80)
    override func viewDidLoad() {
        super.viewDidLoad()
        objTableview.register(UINib(nibName: THAlbumTableViewCell.className, bundle: nil), forCellReuseIdentifier: THAlbumTableViewCell.className)
        LoadView()
        HeaderButton.setTitle(headerString, for: .normal)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        headerView.isHidden = false
    }
    

    
    private func LoadView() {
        let collection = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: .smartAlbumUserLibrary, options: nil)
        collection.enumerateObjects({ (phAssetCollection, index, pointer) in
            if phAssetCollection.estimatedAssetCount > 0 {
                self.collectionResult.append(phAssetCollection)
            }
        })
        
        let allcollection = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .albumRegular, options: nil)
        allcollection.enumerateObjects({ (phAssetCollection, index, pointer) in
            if phAssetCollection.estimatedAssetCount > 0 {
                self.collectionResult.append(phAssetCollection)
            }
        })

        self.imageManager = PHCachingImageManager()
    }
    
    @IBAction func didTapOnHeaderorDropDown(_ sender: Any) {
        headerView.isHidden = true
        self.dismiss(animated: true) { 
            
        }
    }
   
    
    
}

extension THAlbumViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let collection = collectionResult[indexPath.row]
        completionHander!(collection)
        headerView.isHidden = true
        self.dismiss(animated: true) {
            
        }
    }
}

extension THAlbumViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return collectionResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let albumCell = tableView.dequeueReusableCell(withIdentifier: THAlbumTableViewCell.className, for: indexPath) as! THAlbumTableViewCell
        albumCell.selectionStyle = .none
        let collection = collectionResult[indexPath.row]
        albumCell.titleLabel.text = collection.localizedTitle
        albumCell.countImages.text = "\(collection.estimatedAssetCount)"
        if indexPath.row == 0 {
            albumCell.countImages.text = "\(PHAsset.fetchAssets(in: collection, options: nil).count)"
        }
        if collection.estimatedAssetCount == 0 {
            return albumCell
        }
        
        albumCell.image1.image = nil
        albumCell.image2.image = nil
        albumCell.image3.image = nil
        if let images = PHAsset.fetchKeyAssets(in: collection, options: nil) {
           // let asset = self.images[(indexPath as NSIndexPath).item]
            if images.count > 0 {
                albumCell.image3.layer.borderWidth = 0.5
                albumCell.image3.layer.borderColor = UIColor.white.cgColor
                self.imageManager?.requestImage(for: images[0],
                                                targetSize: cellSize,
                                                contentMode: .aspectFill,
                                                options: nil) {
                                                    result, info in
                                                    albumCell.image3.image = result
                                                    albumCell.image3.clipsToBounds = true
                }
            }
            
            if images.count > 1 {
                albumCell.image2.layer.borderWidth = 0.5
                albumCell.image2.layer.borderColor = UIColor.white.cgColor
                self.imageManager?.requestImage(for: images[1],
                                                targetSize: cellSize,
                                                contentMode: .aspectFill,
                                                options: nil) {
                                                    result, info in
                                                    albumCell.image2.image = result
                                                    albumCell.image2.clipsToBounds = true
                }
            } 
                
            if images.count > 2 {
                albumCell.image1.layer.borderWidth = 0.5
                albumCell.image1.layer.borderColor = UIColor.white.cgColor
                self.imageManager?.requestImage(for: images[2],
                                                targetSize: cellSize,
                                                contentMode: .aspectFill,
                                                options: nil) {
                                                    result, info in
                                                    albumCell.image1.image = result
                                                    albumCell.image1.clipsToBounds = true
                }
            }
        }
        return albumCell
    }
}


