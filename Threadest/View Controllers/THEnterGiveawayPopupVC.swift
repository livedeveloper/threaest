//
//  THEnterGiveawayPopupVC.swift
//  Threadest
//
//  Created by Jaydeep on 05/03/18.
//  Copyright © 2018 JC. All rights reserved.
//

import UIKit
import Lottie

class THEnterGiveawayPopupVC: THBaseViewController {

    @IBOutlet weak var subView: UIView!
    
    @IBOutlet weak var doneView: UIView!
    
    open var isGiveAwayPopupClose: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        subView.layer.cornerRadius = 10
        subView.layer.masksToBounds = true
        showAnimate()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let animationView = LOTAnimationView(name: "done_button")
        animationView.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        //animationView.center = self.view.center
        animationView.contentMode = .scaleAspectFill
        animationView.animationSpeed = 0.25
        doneView.addSubview(animationView)
        animationView.loopAnimation = false
        animationView.play(fromProgress: 0, toProgress: 1) { (sucess) in
            animationView.pause()
            //animationView.animationProgress = 0
            //animationView.removeFromSuperview()
        }
    }
    
    @IBAction func didTapOnDoneButton(_ sender: Any) {
        removeAnimate()
        self.isGiveAwayPopupClose!()
    }
    
}
