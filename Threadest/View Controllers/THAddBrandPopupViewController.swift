//
//  THAddBrandPopupViewController.swift
//  Threadest
//
//  Created by Jaydeep on 02/01/18.
//  Copyright © 2018 JC. All rights reserved.
//

import UIKit
import SwiftyJSON

class THAddBrandPopupViewController: THBaseViewController, UIImagePickerControllerDelegate,
UINavigationControllerDelegate {
    
    @IBOutlet weak var brandLogoButton: DesignableButton!
    
    @IBOutlet weak var brandNameTextField: DesignableTextField!
    
    @IBOutlet weak var subView: UIView!
    let picker = UIImagePickerController()
    
    var chosenImage : UIImage = UIImage()
    
    open var newBrandcompletionHander : ((_ brandId: String)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showAnimate()
        subView.layer.cornerRadius = 5
        picker.delegate = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func didTapOnCloseButton(_ sender: Any) {
        removeAnimate()
    }
    
    @IBAction func didTapOnSaveBrand(_ sender: Any) {
        if brandNameTextField.text != "" {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THNetworkManager.sharedInstance.createBrand(brandName: brandNameTextField.text!, brandImage: chosenImage, completion: { (json, error) in
                DispatchQueue.main.async {
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                }
                guard let json = json else {
                    return
                }
                let status = json["status"].stringValue
                if status == "true" {
                    if json["data"] != JSON.null {
                        self.newBrandcompletionHander!(json["data"]["id"].stringValue)
                        self.removeAnimate()
                    }
                }
                debugPrint(json)
            }, progressCompletion: { (progress) in
                debugPrint(progress)
            })
        }
    }
    
    @IBAction func didTapOnBrandButton(_ sender: Any) {
        let actionSheetController = UIAlertController(title:nil, message:nil, preferredStyle: .actionSheet)
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            
        }
        actionSheetController.addAction(cancelActionButton)
        
        let TakeActionButton = UIAlertAction(title: "Take from Camera", style: .default) { action -> Void in
            self.picker.allowsEditing = false
            self.picker.sourceType = .camera
            self.present( self.picker, animated: true, completion: nil)
            print("Camera")
        }
        actionSheetController.addAction(TakeActionButton)
        
        let saveActionButton = UIAlertAction(title: "Pick from Photos", style: .default) { action -> Void in
            self.picker.allowsEditing = false
            self.picker.sourceType = .photoLibrary
            self.self.present( self.picker, animated: true, completion: nil)
            print("Photos")
        }
        actionSheetController.addAction(saveActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.brandLogoButton.setImage(chosenImage, for: .normal)
        self.brandLogoButton.layer.cornerRadius = self.brandLogoButton.frame.size.width/2
        self.brandLogoButton.layer.masksToBounds = true
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension THAddBrandPopupViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}

