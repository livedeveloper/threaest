//
//  THARCategoriesView.swift
//  Threadest
//
//  Created by Jaydeep on 09/11/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import CircleProgressView

class THARCategoriesView: UIView {
    @IBOutlet weak var categoriesImageView: UIImageView!
    
    @IBOutlet weak var circularView: CircleProgressView!
    @IBOutlet weak var categoryNameLabel: UILabel!
    class func instanceFromNib(shopCategory: ShopCategory) -> THARCategoriesView {
        let ARCategoryView = UINib(nibName: "THARCategoriesView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! THARCategoriesView
        ARCategoryView.circularView.isHidden = false
        ARCategoryView.categoryNameLabel.text = shopCategory.title
        return ARCategoryView
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
}
