//
//  THARPostView.swift
//  Threadest
//
//  Created by Jaydeep on 20/07/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import CircleProgressView

class THARPostView: UIView {
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var socialPostImage: UIImageView!
    @IBOutlet weak var commentCountLabel: UILabel!
    @IBOutlet weak var upvoteCountLabel: UILabel!
    @IBOutlet weak var rethreadCountLabel: UILabel!
    @IBOutlet weak var circleView: CircleProgressView!
    
    @IBOutlet weak var likeButton: DesignableButton!
    class func instanceFromNib(socialInteraction: SocialInteraction) -> THARPostView {
        let ARProfilePostView = UINib(nibName: "THARPostView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! THARPostView
        ARProfilePostView.circleView.isHidden = false
        ARProfilePostView.profileImage.sd_setImage(with: URL(string: socialInteraction.ownerImageUrl), placeholderImage: nil, options: .retryFailed)
        ARProfilePostView.profileImage.layer.cornerRadius = ARProfilePostView.profileImage.frame.size.width/2
        ARProfilePostView.profileImage.layer.masksToBounds = true
        ARProfilePostView.usernameLabel.text = socialInteraction.ownerUsername
        ARProfilePostView.commentCountLabel.isHidden = socialInteraction.commentsCount > 0 ? false:true
        ARProfilePostView.commentCountLabel.text = "\(socialInteraction.commentsCount)"
        ARProfilePostView.upvoteCountLabel.isHidden = socialInteraction.upvotesCount > 0 ? false:true
        ARProfilePostView.upvoteCountLabel.text = "\(socialInteraction.upvotesCount)"
        ARProfilePostView.rethreadCountLabel.isHidden = socialInteraction.rethreadCount > 0 ? false: true
        ARProfilePostView.rethreadCountLabel.text = "\(socialInteraction.rethreadCount)"
        ARProfilePostView.likeButton.isSelected = socialInteraction.likedByCurrentUser ? true: false
        ARProfilePostView.timeLabel.text = GeneralMethods.sharedInstance.getTimeAgoStringFrom(dateString: socialInteraction.createdAt)
        return ARProfilePostView
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
}

