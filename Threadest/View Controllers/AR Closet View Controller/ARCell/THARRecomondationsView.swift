//
//  THARRecomondationsView.swift
//  Threadest
//
//  Created by Jaydeep on 06/07/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SDWebImage
import IoniconsSwift
import CircleProgressView
import PassKit

class THARRecomondationsView: UIView {
    @IBOutlet weak var shopImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    
    @IBOutlet weak var brandLogoImageView: UIImageView!
    @IBOutlet weak var availableSizeLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var brandNameLabel: UILabel!
    
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var circularView: CircleProgressView!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }

    class func instanceFromNib(closetCategory: ClosetCategory) -> THARRecomondationsView {
        let ARRecomondationsView = UINib(nibName: "THARRecomondationsView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[0] as! THARRecomondationsView
        ARRecomondationsView.likeButton.setImage(Ionicons.iosStarOutline.image(30, color: .hex("fcdf4e", alpha: 1)), for: .normal)
        ARRecomondationsView.likeButton.setImage(Ionicons.star.image(30, color: .hex("fcdf4e", alpha: 1)), for: .selected)
        
        ARRecomondationsView.circularView.isHidden = false
        
        if closetCategory.soldOut {
            ARRecomondationsView.priceLabel.attributedText = NSAttributedString()
            ARRecomondationsView.priceLabel.text = String(format: " $%.2f    ", closetCategory.defaultPrice)
            ARRecomondationsView.availableSizeLabel.isHidden = false
        } else {
            if closetCategory.salePrice == 0.0 {
                ARRecomondationsView.priceLabel.attributedText = NSAttributedString()
                ARRecomondationsView.priceLabel.text = String(format: " $%.2f    ", closetCategory.defaultPrice)
                ARRecomondationsView.availableSizeLabel.isHidden = true
            } else {
                ARRecomondationsView.availableSizeLabel.isHidden = false
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(format: " $%.2f    ", closetCategory.defaultPrice))
                attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
                ARRecomondationsView.priceLabel.text = ""
                ARRecomondationsView.priceLabel.attributedText = attributeString
            }
        }
        
        let price = closetCategory.salePrice == 0.0 ? closetCategory.defaultPrice : closetCategory.salePrice
        ARRecomondationsView.availableSizeLabel.text = closetCategory.soldOut ? " sold out    ":String(format: " $%.2f    ", price)
        ARRecomondationsView.availableSizeLabel.backgroundColor = closetCategory.soldOut ? UIColor.trasperantRed: UIColor.trasperantblue
        ARRecomondationsView.priceLabel.layer.cornerRadius = 15
        ARRecomondationsView.availableSizeLabel.layer.cornerRadius = 15
        if let _ = closetCategory.productVariations.index(where: {$0.quantityAvailable >= 1}) {
        } else {
            ARRecomondationsView.availableSizeLabel.isHidden = false
            ARRecomondationsView.availableSizeLabel.backgroundColor = UIColor.trasperantRed
            ARRecomondationsView.availableSizeLabel.text = " sold out    "
            ARRecomondationsView.priceLabel.attributedText = NSAttributedString()
            ARRecomondationsView.priceLabel.text = String(format: " $%.2f    ", closetCategory.defaultPrice)
        }
        
        ARRecomondationsView.titleLabel.text = closetCategory.title
        ARRecomondationsView.brandNameLabel.text = closetCategory.brandName
        ARRecomondationsView.likeButton.isSelected = closetCategory.likedByCurrentUser
        ARRecomondationsView.commentLabel.isHidden = closetCategory.commentsCount > 0 ? false: true
        ARRecomondationsView.commentLabel.text = "\(closetCategory.commentsCount)"
        ARRecomondationsView.layoutIfNeeded()
        
        ARRecomondationsView.brandLogoImageView.sd_setImage(with: URL(string: closetCategory.brandLogoImageUrl), placeholderImage: nil, options: .retryFailed)
        
        ARRecomondationsView.brandLogoImageView.layer.borderColor = UIColor.lightGray.cgColor
        ARRecomondationsView.brandLogoImageView.layer.borderWidth = 1
        ARRecomondationsView.brandLogoImageView.layer.cornerRadius = ARRecomondationsView.brandLogoImageView.frame.width / 2.0
        ARRecomondationsView.brandLogoImageView.layer.masksToBounds = true
        
        return ARRecomondationsView
    }
}
