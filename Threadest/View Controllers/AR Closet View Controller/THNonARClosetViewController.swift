//
//  THNonARClosetViewController.swift
//  Threadest
//
//  Created by Jaydeep on 22/07/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import IoniconsSwift
import PassKit
import Stripe
import FBSDKLoginKit
import FBSDKCoreKit
import Social
import SDWebImage
import TwitterKit
import Crashlytics
import MapKit
import Lottie
import ARKit
import MapKit
import SwiftyJSON
import StoreKit
import HTPullToRefresh
import Cheers


class THNonARClosetViewController: UIViewController, UIGestureRecognizerDelegate {
    //outlet tool tip view
    @IBOutlet var toolTipClosetView: UIView!
    @IBOutlet weak var toolTipClosetLabel: UILabel!
    var popOverToolTipCloset = Popover()
    var isProductFound = false
    var retailLocation: RetailLocation!
     let cheerView = CheerView()

    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var brandListingCollectionView: UICollectionView!
    @IBOutlet weak var posterCollectionView: UICollectionView!
    
    @IBOutlet weak var posterCollectionTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var posterCollectionBottomConstraint: NSLayoutConstraint!
    
    open var toggleARNONARPopup: (() -> ())?
    
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var addBrandButton: UIButton!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var closetTabButton: UIButton!
    @IBOutlet weak var dropDownArraow: UIButton!
    @IBOutlet weak var THDropDownButton: UIButton!

    @IBOutlet weak var popupHeaderView: UIView!

    @IBOutlet weak var popupTitleLabel: UILabel!

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var ARToggleButton: UIButton!

    @IBOutlet weak var cartCountLabel: UILabel!
    
    @IBOutlet weak var QRCodeButton: UIButton!
    

    //variable for drop down
    var dropdownShowing = false
    var dropdownViewController: THClosetDropdownMenu!

    weak var delegate: SearchBarDelegate?

    var bulletinManager: BulletinManager!

    //variable
    let categoriesListingArray = ["Recommendations", "Pop Ups","Categories", "Favorites", "Purchased", "Your Posts"]
    var selectedCategoriesIndex = 0
    var selectedBrandIndex = -1
    var selectedBrandID = -1
    var selectedPosterType = ""
    var locationId = 0

    var isFromRetailMapView:Bool = false

    var selectedCategory:ClosetCategory?
    var selectedVariation: ProductVariation?
    var selectedSize = [Int : SelectedProduct]()
    var refreshClosetControl: UIRefreshControl!
    var activityViewController : UIActivityViewController!
    
    
    let collectionMargin = CGFloat(16)
    let itemSpacing = CGFloat(10)
    let itemHeight = CGFloat(0)
    
    var itemWidth = CGFloat(0)
    var currentItem = 0
    var currentPage = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        itemWidth =  UIScreen.main.bounds.width - collectionMargin * 2.0
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
        layout.headerReferenceSize = CGSize(width: collectionMargin, height: 0)
        layout.footerReferenceSize = CGSize(width: collectionMargin, height: 0)
        
        layout.minimumLineSpacing = itemSpacing
        layout.scrollDirection = .horizontal
        posterCollectionView.collectionViewLayout = layout
        posterCollectionView.decelerationRate = UIScrollViewDecelerationRateFast
        
        
        addBrandButton.setImage(Ionicons.plusCircled.image(25, color: .black), for: .normal)
        infoButton.setImage(Ionicons.informationCircled.image(25, color: UIColor.white), for: .normal)

        brandListingCollectionView.register(UINib(nibName: BrandListCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: BrandListCollectionViewCell.className)
        posterCollectionView.register(UINib(nibName: THRecomondationProductCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: THRecomondationProductCollectionViewCell.className)
        posterCollectionView.register(UINib(nibName: THARPostCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: THARPostCollectionViewCell.className)
        posterCollectionView.register(UINib(nibName: THNONARCategoryCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: THNONARCategoryCollectionViewCell.className)

        posterCollectionView.addPullToRefresh(actionHandler: {

            self.didTapOnRefreshButton(self)
        }, position: .left)
        posterCollectionView.pullToRefreshView.setTitle("", for: .loading)

        initUI()
        if !isFromRetailMapView {
            let _ = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.loadWelcomePopup), userInfo: nil, repeats: false)
        } else {
            self.loadEndPointForRetailNearByProducts(isRefresh: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                THUserManager.sharedInstance.retailLocation.first { (retail) -> Bool in
                    if retail.id == self.locationId {
                        let eventName = retail.title == "" ? "Popup Shop":retail.title
                        self.popupTitleLabel.text = eventName
                        return true
                    }
                    return false
                }
                let welcomRetailPopup = THWelcomeRetailLocationVC(nibName: THWelcomeRetailLocationVC.className, bundle: nil)
                welcomRetailPopup.locationId = self.locationId
                self.addChildViewController(welcomRetailPopup)
                welcomRetailPopup.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
                
                self.view.addSubview(welcomRetailPopup.view)
                welcomRetailPopup.didMove(toParentViewController: self)
            }
        }

        self.navigationController?.interactivePopGestureRecognizer?.delegate = self

        NotificationCenter.default.removeObserver(self, name: Notification.Name(postNotificationType.notificationOrderSucessfully.rawValue), object: nil)

        NotificationCenter.default.removeObserver(self, name: Notification.Name(postNotificationType.notificationNearBy.rawValue), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(THNonARClosetViewController.ordersucessfull), name: Notification.Name(postNotificationType.notificationOrderSucessfully.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(THNonARClosetViewController.getNearByNotificationCalled), name: Notification.Name(postNotificationType.notificationNearBy.rawValue), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(postNotificationType.notificationFollow.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(THNonARClosetViewController.notificationPostFollow(notification:)), name: Notification.Name(postNotificationType.notificationFollow.rawValue), object: nil)

        NotificationCenter.default.removeObserver(self, name: Notification.Name(postNotificationType.notificationProduct.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(THNonARClosetViewController.notificationProduct(notification:)), name: Notification.Name(postNotificationType.notificationProduct.rawValue), object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        callBrandsToFollowServices()
        if isFromRetailMapView {
            
        } else {
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                //for coach mark
                if usercurrent.gender != "" {
                    if userdefault.object(forKey: userDefaultKey.coachMarkBrandDefault.rawValue) != nil {
                        self.coachMarkClosetOverlay()
                    }
                }
                if usercurrent.tempAccount {
                    self.usernameLabel.text = "Your Closet"
                } else {
                    self.usernameLabel.text = "\(usercurrent.username)'s Closet"
                }
                cartUpdate()
            }
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        let imageCache = SDImageCache.shared()
        imageCache.clearMemory()
    }

    override func didReceiveMemoryWarning() {
        let imageCache = SDImageCache.shared()
        imageCache.clearMemory()
    }

    func loadWelcomePopup() {
        if userdefault.object(forKey: userDefaultKey.bulletinWelcomePopupCloset.rawValue) == nil {
            self.bulletinWelcomeClosetPopUp()
        } else {
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                if usercurrent.gender == "" {
                    self.bullitenItemOfWelcomePopup()
                } else {
                    loadEndPointForRecommendations(isShowLoader: true)
                }
            }
        }
    }

    override func dismissKeyboard() {
        view.endEditing(true)
    }

    func initUI() {
        cartCountLabel.layer.cornerRadius = cartCountLabel.frame.width / 2.0
        cartCountLabel.layer.masksToBounds = true
        profileButton.setImage(Ionicons.iosPerson.image(30, color: .white), for: .normal)

        if #available(iOS 11.0, *) {
            if (ARConfiguration.isSupported) {
                ARToggleButton.isHidden = false
            } else {
                ARToggleButton.isHidden = true
            }
        } else {
            ARToggleButton.isHidden = true
        }
    }


    func ordersucessfull() {
        selectedCategoriesIndex = 4
        loadEndPointForOrder(isRefresh: true)
        if #available(iOS 10.3, *) {
             SKStoreReviewController.requestReview()
        } else {
            // Fallback on earlier versions
        }
    }

    func coachMarkClosetOverlay() {
        if userdefault.object(forKey: userDefaultKey.coachMarkNONARClosetDefault.rawValue) == nil {
            userdefault.set(true, forKey: userDefaultKey.coachMarkNONARClosetDefault.rawValue)
            userdefault.synchronize()
            //Add funnel of coachmark overlay
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "CoachMark Overlay for closer", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "view": "NONAR"])
            }
            DispatchQueue.main.async {
                self.view.endEditing(true)
                let coach = THCoachMarkPageViewController(nibName: THCoachMarkPageViewController.className, bundle: nil)
                coach.isFromCloset = true
                coach.isARView = false
                self.addChildViewController(coach)
                coach.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
                self.view.addSubview(coach.view)
            }
        }
    }
    
    func bulletinWelcomeClosetPopUp() {
        self.bulletinManager = BulletinManager(rootItem: GeneralMethods.sharedInstance.bulletinWelcomeClosetPopup {
            self.bulletinManager.dismissBulletin(animated: true, completion: {
                userdefault.set(true, forKey: userDefaultKey.bulletinWelcomePopupCloset.rawValue)
                userdefault.synchronize()
                DispatchQueue.main.async {
                    self.loadWelcomePopup()
                }
            })
            
        })
        if #available(iOS 10, *) {
            self.bulletinManager.backgroundViewStyle = .blurred(style: .light, isDark: false)
        } else {
            self.bulletinManager.backgroundViewStyle = .dimmed
        }
        self.bulletinManager.prepare()
        self.bulletinManager.presentBulletin(above: self.tabBarController ?? self)
    }

    func bullitenItemOfWelcomePopup() {
        self.bulletinManager = BulletinManager(rootItem: GeneralMethods.sharedInstance.bulletinWelcometoThreadest(maleCompletion: {
            GeneralMethods.sharedInstance.updateMaleFemaleValue(gender: "male")
            self.bulletinManager.push(item: self.bullitenStyleSelectionPopup(gender: "male"))
            //Add funnel of Gender
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Gender selection in welcome popup", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "Gender": "male"])
            }

        }, FemaleCompletion: {
            GeneralMethods.sharedInstance.updateMaleFemaleValue(gender: "female")
            self.bulletinManager.push(item: self.bullitenStyleSelectionPopup(gender: "female"))
            //Add funnel of Gender
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Gender selection in welcome popup", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "Gender": "female"])
            }
        }))
        if #available(iOS 10, *) {
            self.bulletinManager.backgroundViewStyle = .blurred(style: .light, isDark: false)
        } else {
            self.bulletinManager.backgroundViewStyle = .dimmed
        }
        self.bulletinManager.prepare()
        self.bulletinManager.presentBulletin(above: self.tabBarController ?? self)
    }

    func bullitenStyleSelectionPopup(gender: String) -> BulletinItem {
        //Add funnel of style selection
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Style selection Popup", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }
        return SelectionStyleBulletinPage(gender: gender, dismissCompletion: {
            self.bulletinManager.dismissBulletin(animated: true, completion: {
                if userdefault.object(forKey: userDefaultKey.coachMarkBrandDefault.rawValue) == nil {
                    self.didTapOnAddBrand(self)
                    if self.isFromRetailMapView {
                        self.loadEndPointForRetailNearByProducts(isRefresh: false)
                    } else {
                        self.loadEndPointForRecommendations(isShowLoader: false)
                    }
                } else {
                    if self.isFromRetailMapView {
                        self.loadEndPointForRetailNearByProducts(isRefresh: true)
                    } else {
                        self.loadEndPointForRecommendations(isShowLoader: true)
                    }
                }
            })
        })
    }

    func getNearByNotificationCalled() {
        selectedCategoriesIndex = 2
        self.isProductFound = false
    }

    func navigateToProductDetailPage(brandId: Int, productId: Int, geofence: Bool) {
        //Add funnel of closet feed
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Show Product Detail From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THBrandProductManager.sharedInstance.retrieveBrandProfile(with: brandId, productId: productId, geofence: geofence) { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            
            let threeDAvailable = THBrandProductManager.sharedInstance.brandProduct?.productGalleryImages.contains(where: { (imgGallery) -> Bool in
                if imgGallery.image_type == "3D" {
                    return true
                } else {
                    return false
                }
            })
            
            if threeDAvailable! {
                if #available(iOS 11.0, *) {
                    if (ARConfiguration.isSupported) {
                        let ARShopShowController = THARShopShowViewController(nibName: THARShopShowViewController.className, bundle: nil)
                        ARShopShowController.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                        ARShopShowController.isPopup = true
                        let navARShopShowVC = UINavigationController(rootViewController: ARShopShowController)
                        self.present(navARShopShowVC, animated: true, completion: nil)
                        return
                    }
                }
            }
            let shopshowcontroller = THShopshowViewController(nibName: THShopshowViewController.className, bundle: nil)
            shopshowcontroller.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
            shopshowcontroller.isPopup = true
            let navShopShowVC = UINavigationController(rootViewController: shopshowcontroller)
            self.present(navShopShowVC, animated: true, completion: nil)
        }
    }

    func cartUpdate() {
        if let cartCount = UserDefaults.standard.value(forKey: "cartCount") {
            if (cartCount as! Int) == 0 {
                self.cartCountLabel.isHidden = true
            } else {
                self.cartCountLabel.isHidden = false
                self.cartCountLabel.text = "\(cartCount as! Int)"
            }
        } else {
            self.cartCountLabel.isHidden = true
             if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                if userdefault.object(forKey: "cartCount") == nil {
                    userdefault.set(usercurrent.cartCount, forKey: "cartCount")
                    userdefault.synchronize()
                }
            }
        }
    }

    func showClosetToolTip() {
        let options = [
            .type(.up),
            .animationIn(0.3),
            .blackOverlayColor(UIColor.clear),
            .color(UIColor.blueTabbar)
            ] as [PopoverOption]
        self.toolTipClosetView.sizeToFit()

        if userdefault.object(forKey: userDefaultKey.toolTipClosetTabIconDefault.rawValue) == nil {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.toolTipClosetLabel.text = "😎 Tip: Closet gets smarter when you like products and purchase items you love."
                self.popOverToolTipCloset = Popover(options: options, showHandler: nil, dismissHandler: nil)
                self.popOverToolTipCloset.show(self.toolTipClosetView, fromView: self.closetTabButton)
            }
        }
    }

    @IBAction func didTapOnDownArrow(_ sender: Any) {
//        appdelegate.verticle.scrollDown(isAnimated: true)
    }

    @IBAction func didTapOnInfoButton(_ sender: Any) {
        userdefault.removeObject(forKey: userDefaultKey.coachMarkNONARClosetDefault.rawValue)
        userdefault.synchronize()
        coachMarkClosetOverlay()
        //Add funner for Info
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Info Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }
    }

    @IBAction func didTapOnCartButton(_ sender: Any) {
        if let dropdown = dropdownViewController {
            dropdown.removeAnimate()
        }

        let shoppingcartController = THYourCartViewController(nibName: THYourCartViewController.className, bundle: nil)
        self.navigationController?.pushViewController(shoppingcartController, animated: true)
    }

    @IBAction func didTapOnLikescountPostButton(_ sender: UIButton) {
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        let socialInteraction = THProfilePostsManager.sharedInstance.currentUserProfilePosts[sender.tag]
        THSocialInteractionPostsManager.sharedInstance.retrieveLikedPostUserList(for: socialInteraction.id) { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            guard let error = error else {
                let followingController = THUserFollowerViewController(nibName: THUserFollowerViewController.className, bundle: nil)
                followingController.profileType = "Likes"
                let navFollwingController = UINavigationController(rootViewController: followingController)
                self.present(navFollwingController, animated: true, completion: nil)
                return
            }
            debugPrint(error)
        }
    }

    @IBAction func didTapOnLikePostButton(_ sender: UIButton) {
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }
        if !sender.isSelected {
            let cell  = posterCollectionView.cellForItem(at: IndexPath(row: sender.tag, section: 0)) as! THARPostCollectionViewCell
            sender.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                sender.transform = CGAffineTransform(rotationAngle: CGFloat(-179.9).degreesToRadians)
            })
            UIView.animate(withDuration: 0.3, delay: 0.2, options: UIViewAnimationOptions(), animations: {
                sender.transform = CGAffineTransform(rotationAngle: CGFloat(1.1).degreesToRadians)
            }, completion: { (completed) in
                if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    THUserManager.sharedInstance.performUpvote(for: usercurrent.userId, votableId: THProfilePostsManager.sharedInstance.currentUserProfilePosts[sender.tag].id, votableType: "Post", completion: { (vote, error) in
                        guard let _ = error else {
                            THProfilePostsManager.sharedInstance.currentUserProfilePosts[sender.tag].likedByCurrentUser = true
                            THProfilePostsManager.sharedInstance.currentUserProfilePosts[sender.tag].upvotesCount = THProfilePostsManager.sharedInstance.currentUserProfilePosts[sender.tag].upvotesCount + 1
                            THProfilePostsManager.sharedInstance.currentUserProfilePosts[sender.tag].votes.append(vote!)

                            cell.upvoteCountLabel.text = "\(THProfilePostsManager.sharedInstance.currentUserProfilePosts[sender.tag].upvotesCount)"
                            cell.upvoteCountLabel.isHidden = false
                            sender.isSelected  = true
                            Answers.logCustomEvent(withName: "Like post", customAttributes: ["userid":usercurrent.userId,"username": usercurrent.username, "post_Id":"\(THProfilePostsManager.sharedInstance.currentUserProfilePosts[sender.tag].id)"])
                            return
                        }
                    })
                }
                else {

                }
            })
        }
    }

    @IBAction func didTapOnGiveAwayButton(_ sender: UIButton) {
        guard let usercurrent = THUserManager.sharedInstance.currentUserdata() else {
            return
        }
        var categoryCloset: ClosetCategory!
        switch selectedPosterType {
        case "Brand":
            categoryCloset = THBrandsManager.sharedInstance.products[sender.tag]
        case "Purchased":
            categoryCloset = THUserOrdersManager.sharedInstance.userShippedProducts[sender.tag].productData
        case "Recommendations":
            categoryCloset = THProfileClosetManager.sharedInstance.closetRecommendations[sender.tag]
        case "NearBy":
            categoryCloset = THRetailBrandManager.sharedInstance.RetailProducts[sender.tag]
        case "Favorites":
            categoryCloset = THProfileClosetManager.sharedInstance.closetFavorite[sender.tag]
        default:
            return
        }
        self.navigateToProductDetailPage(brandId: categoryCloset.brandId, productId: categoryCloset.Id, geofence: false)
    }

    @IBAction func didTapOnLikeProductButton(_ sender: UIButton) {
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }
        //get current selected product closet
        var categoryCloset: ClosetCategory!
        switch selectedPosterType {
        case "Brand":
            categoryCloset = THBrandsManager.sharedInstance.products[sender.tag]
        case "Purchased":
            categoryCloset = THUserOrdersManager.sharedInstance.userShippedProducts[sender.tag].productData
        case "Recommendations":
            categoryCloset = THProfileClosetManager.sharedInstance.closetRecommendations[sender.tag]
        case "NearBy":
            categoryCloset = THRetailBrandManager.sharedInstance.RetailProducts[sender.tag]
        case "Favorites":
            categoryCloset = THProfileClosetManager.sharedInstance.closetFavorite[sender.tag]
        default:
            return
        }
        //current tapped cell of product
        let posterCell = posterCollectionView.cellForItem(at: IndexPath(row: sender.tag, section: 0)) as! THRecomondationProductCollectionViewCell
        if !sender.isSelected {
            sender.isSelected = true
            let animationView = LOTAnimationView(name: "like")
            animationView.animationProgress = 100
            animationView.center = posterCell.shopImage.center
            animationView.frame = posterCell.shopImage.frame
            animationView.backgroundColor = UIColor.clear
            animationView.contentMode = .scaleAspectFill
            animationView.animationSpeed = 2
            posterCell.shopImage.addSubview(animationView)
            animationView.loopAnimation = false
            
            animationView.play { (sucess) in
                animationView.pause()
                animationView.removeFromSuperview()
                
                posterCell.rethreadButtonTop.backgroundColor = UIColor.lightGray
                UIView.animate(withDuration: 0.6,
                               animations: {
                                posterCell.rethreadButtonTop.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
                },
                               completion: { _ in
                                posterCell.rethreadButtonTop.backgroundColor = UIColor.hexa("178dcd", alpha: 1)
                                UIView.animate(withDuration: 0.6) {
                                    posterCell.rethreadButtonTop.transform = CGAffineTransform.identity
                                }
                })
            }
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                THUserManager.sharedInstance.performUpvote(for: usercurrent.userId, votableId: categoryCloset.Id, votableType: "Product", completion: { (vote, error) in
                    guard let error = error else {
                        categoryCloset.likedByCurrentUser = true
                        categoryCloset.votesCount = categoryCloset.votesCount + 1
                        switch self.selectedPosterType {
                        case "Brand":
                            THBrandsManager.sharedInstance.products[sender.tag] = categoryCloset
                        case "Purchased":
                            THUserOrdersManager.sharedInstance.userShippedProducts[sender.tag].productData = categoryCloset
                        case "Recommendations":
                            THProfileClosetManager.sharedInstance.closetRecommendations[sender.tag] = categoryCloset
                        case "NearBy":
                            THRetailBrandManager.sharedInstance.RetailProducts[sender.tag] = categoryCloset
                        case "Favorites":
                            THProfileClosetManager.sharedInstance.closetFavorite[sender.tag] = categoryCloset
                        default:
                            return
                        }
                        posterCell.likeButton.isSelected  = true
                        //add funnel for product like
                        Answers.logCustomEvent(withName: "Like product", customAttributes: ["userid":usercurrent.userId,"username": usercurrent.username, "product_Id":"\(categoryCloset.Id)"])
                        return
                    }
                    debugPrint(error)
                })
            }
        }
    }

    @IBAction func didTapOnCommentProductButton(_ sender: UIButton) {
        switch selectedPosterType {
        case "Brand":
            let selectCloset = THBrandsManager.sharedInstance.products[sender.tag]
            self.navigateToProductDetailPage(brandId: selectCloset.brandId, productId: selectCloset.Id, geofence: false)
        case "Purchased":
            let selectCloset = THUserOrdersManager.sharedInstance.userShippedProducts[sender.tag].productData
            self.navigateToProductDetailPage(brandId: selectCloset.brandId, productId: selectCloset.Id, geofence: false)
        case "Recommendations":
            let selectCloset = THProfileClosetManager.sharedInstance.closetRecommendations[sender.tag]
            self.navigateToProductDetailPage(brandId: selectCloset.brandId, productId: selectCloset.Id, geofence: false)
        case "Favorites":
            let selectCloset = THProfileClosetManager.sharedInstance.closetFavorite[sender.tag]
            self.navigateToProductDetailPage(brandId: selectCloset.brandId, productId: selectCloset.Id, geofence: false)
        case "NearBy":
            let selectCloset = THRetailBrandManager.sharedInstance.RetailProducts[sender.tag]
            self.navigateToProductDetailPage(brandId: selectCloset.brandId, productId: selectCloset.Id, geofence: true)
        case "Post":
            let socialInteraction = THProfilePostsManager.sharedInstance.currentUserProfilePosts[sender.tag]
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THProfilePostsManager.sharedInstance.retrieveNotificationPost(with: socialInteraction.id, completion: { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                guard error != nil else {
                    let commentController = THCommentViewController(nibName: THCommentViewController.className, bundle: nil)
                    commentController.socialInteraction = socialInteraction
                    let navCommentVC = UINavigationController(rootViewController: commentController)
                    self.present(navCommentVC, animated: true, completion: nil)
                    return
                }
            })
        default:
            break
        }
    }

    @IBAction func didTapOnRethreadButton(_ sender: UIButton) {
        guard let usercurrent = THUserManager.sharedInstance.currentUserdata() else {
            return
        }
        var text = ""
        if selectedPosterType == "Post" {
            let socialInteraction = THProfilePostsManager.sharedInstance.currentUserProfilePosts[sender.tag]
            text = "https://www.threadest.com/posts/\(socialInteraction.id)"
        } else {
            var categoryCloset: ClosetCategory!
            switch selectedPosterType {
            case "Brand":
                categoryCloset = THBrandsManager.sharedInstance.products[sender.tag]
            case "Purchased":
                categoryCloset = THUserOrdersManager.sharedInstance.userShippedProducts[sender.tag].productData
            case "Recommendations":
                categoryCloset = THProfileClosetManager.sharedInstance.closetRecommendations[sender.tag]
            case "Favorites":
                categoryCloset = THProfileClosetManager.sharedInstance.closetFavorite[sender.tag]
            case "NearBy":
                categoryCloset = THRetailBrandManager.sharedInstance.RetailProducts[sender.tag]
            default:
                return
            }
            text = "https://www.threadest.com/products/\(categoryCloset.Id)?b=\(categoryCloset.brandId)&sc=\(usercurrent.sharePostCode)"
        }

        // set up activity view controller
        let textToShare = [ text ]
        activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = sender // so that iPads won't crash
        // present the view controller
        DispatchQueue.main.async {
            self.present(self.activityViewController, animated: true, completion: nil)
        }
    }

    // for Rethread post Threadest
    func rethreadPost(ID: Int, indexRow: Int) {
       // let closetCategory:ClosetCategory = THProfileClosetManager.sharedInstance.closetRecommendations[selectedRow]

        THSocialInteractionPostsManager.sharedInstance.performRethreadPost(for: ID) { (error) in
            guard let error = error else {

                if self.selectedPosterType == "Post" {
                    //Add funnel for Rethread Threadest Post
                    if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                        Answers.logCustomEvent(withName: "Rethread post", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "post_id":"\(ID)", "rethread_type":"Threadest"])
                    }
                    let socialPost = self.posterCollectionView.cellForItem(at: IndexPath(row: indexRow, section: 0)) as! THARPostCollectionViewCell
                        THProfilePostsManager.sharedInstance.currentUserProfilePosts[indexRow].rethreadCount = THProfilePostsManager.sharedInstance.currentUserProfilePosts[indexRow].rethreadCount + 1
                    socialPost.rethreadCountLabel.text = "\(THProfilePostsManager.sharedInstance.currentUserProfilePosts[indexRow].rethreadCount + 1)"
                    socialPost.rethreadCountLabel.isHidden = false
                }
                return
            }
            debugPrint(error)
        }
    }

    // for Rethread post on Facebook
    func rethreadPostOnFacebook(URL: String, ImageURL: String, id: Int) {
        if FBSDKAccessToken.current() != nil {
            if FBSDKAccessToken.current().hasGranted("publish_actions") {
                GeneralMethods.sharedInstance.publishMessage(message: URL, link: ImageURL, completion: { () in
                    //Add funnel for Rethread Threadest Post
                    if id != -1 {
                        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                            Answers.logCustomEvent(withName: "Rethread post", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "post_id":"\(id)", "rethread_type":"Facebook"])
                        }
                    }
                    GeneralMethods.sharedInstance.RethreadPostSucessPopup(message: "Post to Facebook - Successful", viewController: self)
                })
            } else {
                requestPublishPermissions(URL: URL, ImageURL: ImageURL, id: id)
            }
        } else {
            requestPublishPermissions(URL: URL, ImageURL: ImageURL, id: id)
        }
    }

    func requestPublishPermissions(URL: String, ImageURL: String, id: Int) {
        let login: FBSDKLoginManager = FBSDKLoginManager()

        login.logIn(withPublishPermissions: ["publish_actions"], from: self) { (result, error) in
            if (error != nil) {
                print(error!)
            } else if (result?.isCancelled)! {
                print("Canceled")
            } else if (result?.grantedPermissions.contains("publish_actions"))! {
                print("permissions granted")
                GeneralMethods.sharedInstance.publishMessage(message: URL, link: ImageURL, completion: { () in
                    //Add funnel for Rethread Threadest Post
                    if id != -1 {
                        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                            Answers.logCustomEvent(withName: "Rethread post", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "post_id":"\(id)", "rethread_type":"Facebook"])
                        }
                    }
                    GeneralMethods.sharedInstance.RethreadPostSucessPopup(message: "Post to Facebook - Successful", viewController: self)
                })
            }
        }
    }


    func rethreadPostOnTwitter(strURL: String, ImageURL: String, id: Int, type: String) {

        if let twitterAccountId = TWTRTwitter.sharedInstance().sessionStore.session() {
            GeneralMethods.sharedInstance.uploadTwitterMedia(viewController: self, imageURL: ImageURL, status: strURL, token: twitterAccountId.authToken, userId: twitterAccountId.userID, id: id, type: type)
        } else {
            // Not logged in
            TWTRTwitter.sharedInstance().logIn { (session, error) in
                if let _ = session {
                    if let twitterAccountId = TWTRTwitter.sharedInstance().sessionStore.session() {
                        GeneralMethods.sharedInstance.uploadTwitterMedia(viewController: self, imageURL: ImageURL, status: strURL, token: twitterAccountId.authToken, userId: twitterAccountId.userID, id: id, type: type)
                    }
                } else {
                    print("error: \(String(describing: error?.localizedDescription))");
                }
            }
        }
    }


    @IBAction func didTapAddSelectSize(_ sender: UIButton){
        var categoryCloset: ClosetCategory!
        switch selectedPosterType {
        case "Brand":
            categoryCloset = THBrandsManager.sharedInstance.products[sender.tag]
        case "Purchased":
            categoryCloset = THUserOrdersManager.sharedInstance.userShippedProducts[sender.tag].productData
        case "Recommendations":
            categoryCloset = THProfileClosetManager.sharedInstance.closetRecommendations[sender.tag]
        case "NearBy":
            categoryCloset = THRetailBrandManager.sharedInstance.RetailProducts[sender.tag]
        case "Favorites":
            categoryCloset = THProfileClosetManager.sharedInstance.closetFavorite[sender.tag]
        default:
            return
        }
        if categoryCloset.soldOut {
            return
        }
        if let _ = categoryCloset.productVariations.index(where: {$0.quantityAvailable >= 1}) {
            let popAddNewCard = THProductSizePopUp(nibName: THProductSizePopUp.className, bundle: nil)
            popAddNewCard.delegate = self
            popAddNewCard.categoryDetail = categoryCloset
            self.addChildViewController(popAddNewCard)
            popAddNewCard.view.frame = self.view.frame
            self.view.addSubview(popAddNewCard.view)
            popAddNewCard.didMove(toParentViewController: self)
        }
    }

    @IBAction func didTapAddToCart(_ sender: UIButton){
        switch selectedPosterType {
        case "Brand":
            self.selectedCategory = THBrandsManager.sharedInstance.products[sender.tag]
        case "Purchased":
            self.selectedCategory = THUserOrdersManager.sharedInstance.userShippedProducts[sender.tag].productData
        case "Recommendations":
            self.selectedCategory = THProfileClosetManager.sharedInstance.closetRecommendations[sender.tag]
        case "NearBy":
            self.selectedCategory = THRetailBrandManager.sharedInstance.RetailProducts[sender.tag]
        case "Favorites":
            self.selectedCategory = THProfileClosetManager.sharedInstance.closetFavorite[sender.tag]
        default:
            return
        }
        let product = self.selectedSize[(self.selectedCategory?.Id)!]
        let productVar = self.selectedCategory?.productVariations[product!.productIndex]
        GeneralMethods.sharedInstance.loadLoading(view: self.view)

        THCartManager.sharedInstance.addToCart(with: (productVar?.productId)!, productVariationId: (productVar?.id)!, postId:-1) { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)

            if let error = error {
                self.showAlert(withMessage: error.localizedDescription)
            }else {
                self.cartUpdate()
                /*self.showAlert(title: "Success", message: "Successfully add \(self.selectedCategory?.title ?? "") to cart", okClick: {

                    // Do Something
                })*/
                self.DoneAnimation()
            }
        }
    }

    func DoneAnimation() {
        let animationDoneView = LOTAnimationView(name: "done_button")
        animationDoneView.animationProgress = 100
        animationDoneView.backgroundColor = UIColor.clear
        animationDoneView.frame = CGRect(x: 0, y: 0, width: 400, height: 400)
        animationDoneView.center = view.center
        animationDoneView.contentMode = .scaleAspectFill
        animationDoneView.animationSpeed = 0.8
        view.addSubview(animationDoneView)
        animationDoneView.loopAnimation = false
        animationDoneView.play { (sucess) in
            animationDoneView.pause()
            animationDoneView.removeFromSuperview()
        }
    }


    @IBAction func didTapAddApplePay(_ sender: UIButton){
        let paymentNetworks = [PKPaymentNetwork.discover, .masterCard, .amex, .visa]
        if PKPassLibrary.isPassLibraryAvailable() {
            if !PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetworks) {
                self.present(GeneralMethods.sharedInstance.AlertviewGeneral(title: "Add a Credit Card to Wallet", body: "Would you like to add a credit card to your wallet now?", cancelbutton: "No", okbutton: "Yes", completion: { () in
                    let lib = PKPassLibrary()
                    lib.openPaymentSetup()
                }), animated: true, completion: nil)
            }
        }
        switch selectedPosterType {
        case "Brand":
            self.selectedCategory = THBrandsManager.sharedInstance.products[sender.tag]
        case "Purchased":
            self.selectedCategory = THUserOrdersManager.sharedInstance.userShippedProducts[sender.tag].productData
        case "Recommendations":
            self.selectedCategory = THProfileClosetManager.sharedInstance.closetRecommendations[sender.tag]
        case "NearBy":
            self.selectedCategory = THRetailBrandManager.sharedInstance.RetailProducts[sender.tag]
        case "Favorites":
            self.selectedCategory = THProfileClosetManager.sharedInstance.closetFavorite[sender.tag]
        default:
            return
        }
        let product = self.selectedSize[self.selectedCategory!.Id]
        self.selectedVariation = self.selectedCategory?.productVariations[product!.productIndex]
        let paymentRequest = Stripe.paymentRequest(withMerchantIdentifier: STPPaymentConfiguration.shared().appleMerchantIdentifier!)
        paymentRequest.supportedNetworks = paymentNetworks
        paymentRequest.requiredShippingAddressFields = [.all]

        let price = self.selectedCategory!.salePrice == 0.0 ? self.selectedCategory!.defaultPrice:self.selectedCategory!.salePrice
        let payTarget = PKPaymentSummaryItem(label: "THREADEST INC.", amount: NSDecimalNumber(value: price))

        paymentRequest.paymentSummaryItems = [payTarget]

        if  Stripe.canSubmitPaymentRequest(paymentRequest) {
            let paymentAuthorizationVC = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest)
            paymentAuthorizationVC.delegate = self
            self.present(paymentAuthorizationVC, animated: true, completion: nil)
            //Add funnel for Apple pay button
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Apple Pay Button", customAttributes: ["userid":usercurrent.userId,
                                                                                        "username": usercurrent.username, "Price":price])
            }
        }
    }


    @IBAction func didTapOnRefreshButton(_ sender: Any) {
        //Add funnel for Refresh button
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Refresh button", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "type":selectedPosterType])
        }
        switch selectedPosterType {
        case "Categories":
            loadEndPointForCategories(isShowLoader: false, isRefresh: true)
            break
        case "Recommendations":
            loadEndPointForRecommendations(isShowLoader: false, isRefresh: true)
            break
        case "Purchased":
            loadEndPointForOrder(isRefresh: true)
            break
        case "Brand":
            loadEndPointForBrand(brandId: selectedBrandID, isRefresh: true)
            break
        case "Post":
            loadEndPointForYourPosts(isRefresh: true)
            break
        case "Favorites":
            loadEndPointClosetFavorite()
            break
        case "NearBy":
            loadEndPointForRetailNearByProducts(isRefresh: true)
            break
        default:
            break
        }
    }

    @IBAction func didTapOnCameraButton(_ sender: Any) {
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }
        //Add funnel of closet feed
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Camera From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }
        let swiftyCameraVC = THSwiftyCameraViewController(nibName: THSwiftyCameraViewController.className, bundle: nil)
        swiftyCameraVC.view.frame.size.width = screenwidth
        swiftyCameraVC.view.frame.size.height = screenheight
        swiftyCameraVC.completionHander = {
            object, isfrom in
            if let thsocial = self.tabBarController?.viewControllers?.first as? THSocialViewController {
                thsocial.productObject = object
                thsocial.isFromCamera = true
            }
        }
        self.present(swiftyCameraVC, animated: true, completion: nil)
    }

    @IBAction func didTapOnProfileButton(_ sender: Any) {
        //Add funnel of closet feed
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Profile From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let profileVC = storyBoard.instantiateViewController(withIdentifier: PZProfileTableViewController.className)
        let navprofilePostVC = UINavigationController(rootViewController: profileVC)
        self.present(navprofilePostVC, animated: true, completion: nil)
    }

    @IBAction func didTapOnThreadestBlack(_ sender: Any) {
        //Add funnel of closet feed
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "MyBrand From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }
        let BrandVC = THBlackBrandViewController(nibName: THBlackBrandViewController.className, bundle: nil)
        BrandVC.isMyBrand = "my"
        BrandVC.blackBrandCompletionHander = {
            self.didTapOnCameraButton(self)
        }
        self.present(BrandVC, animated: true, completion: nil)
    }



    @IBAction func didTapOnAddBrand(_ sender: Any) {
        //Add funnel of closet feed
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "DiscoverBrand From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }
        let BrandVC = THBlackBrandViewController(nibName: THBlackBrandViewController.className, bundle: nil)
        BrandVC.isMyBrand = "discover"
        BrandVC.blackBrandCompletionHander = {
            self.didTapOnCameraButton(self)
        }
        self.present(BrandVC, animated: true, completion: nil)
    }

    @IBAction func didTapOnViewBrand(_ sender: Any) {
        //Add funnel of closet feed
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "MyBrand From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }
        let BrandVC = THBlackBrandViewController(nibName: THBlackBrandViewController.className, bundle: nil)
        BrandVC.isMyBrand = "my"
        BrandVC.blackBrandCompletionHander = {
            self.didTapOnCameraButton(self)
        }
        self.present(BrandVC, animated: true, completion: nil)
    }

    @IBAction func didTapOnToolTipNext(_ sender: Any) {
        if userdefault.object(forKey: userDefaultKey.toolTipClosetTabIconDefault.rawValue) == nil {
            userdefault.set("true", forKey: userDefaultKey.toolTipClosetTabIconDefault.rawValue)
            userdefault.synchronize()
            self.popOverToolTipCloset.dismiss()
        }
    }

    @IBAction func didTapOnToolTipClose(_ sender: Any) {
        if userdefault.object(forKey: userDefaultKey.toolTipClosetTabIconDefault.rawValue) == nil {
            userdefault.set("true", forKey: userDefaultKey.toolTipClosetTabIconDefault.rawValue)
            userdefault.synchronize()
            self.popOverToolTipCloset.dismiss()
        }
    }

    @IBAction func didTapOnDropDownButton(_ sender: UIButton) {

        let popController = THPopOverMenuViewController(nibName: THPopOverMenuViewController.className, bundle: nil)

        // set the presentation style
        popController.modalPresentationStyle = UIModalPresentationStyle.popover

        popController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
        popController.popoverPresentationController?.delegate = self
        popController.popoverPresentationController?.sourceView = sender
        popController.popoverPresentationController?.sourceRect = sender.bounds
        popController.preferredContentSize = CGSize(width: THDropDownButton.frame.size.width, height: 308)
        popController.popOverCompletionHander = {
            itemSelected in
            self.dropDownArraow.isSelected = false
            switch itemSelected {
            case "Profile":
                self.didTapOnProfileButton(self)
                break
            case "Your Brands":
                self.didTapOnViewBrand(self)
                break
            case "Add Brands":
                self.didTapOnAddBrand(self)
                break
            case "Camera":
                self.didTapOnCameraButton(self)
                break
            case "Social":
                self.tabBarController?.selectedIndex = 0
                break
            case "Shopping":
                self.tabBarController?.selectedIndex = 2
                break
            default:
                break
            }
        }
        // present the popover
        self.present(popController, animated: true, completion: nil)
    }

    @IBAction func didTapOnBrandProfileButton(_ sender: UIButton) {
        var category:ClosetCategory!
        switch selectedPosterType {
        case "Brand":
            category = THBrandsManager.sharedInstance.products[sender.tag]
        case "Purchased":
            category = THUserOrdersManager.sharedInstance.userShippedProducts[sender.tag].productData
        case "Recommendations":
            category = THProfileClosetManager.sharedInstance.closetRecommendations[sender.tag]
        case "NearBy":
            category = THRetailBrandManager.sharedInstance.RetailProducts[sender.tag]
        case "Favorites":
            category = THProfileClosetManager.sharedInstance.closetFavorite[sender.tag]
        default:
            return
        }
        let brandProfileVC:THBrandProfileViewController = THBrandProfileViewController()
        brandProfileVC.isFrom = "brandlogo"
        brandProfileVC.brandId = category.brandId
        self.navigationController?.pushViewController(brandProfileVC, animated: true)
    }

    @IBAction func didTapOnARToggleButton(_ sender: UIButton) {
        if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) == .denied {
            bulletinManager = BulletinManager(rootItem: GeneralMethods.sharedInstance.bullitenItemOfCameraDeniedPermission(openSettingCompletion: {
                self.bulletinManager.dismissBulletin(animated: true, completion: {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!)
                    } else {
                        UIApplication.shared.openURL(URL(string: "prefs://")!)
                    }
                })
            }, NotNowCompletion: {
                self.bulletinManager.dismissBulletin(animated: true, completion: {
                })
            }))
            if #available(iOS 10, *) {
                self.bulletinManager.backgroundViewStyle = .blurred(style: .light, isDark: false)
            } else {
                self.bulletinManager.backgroundViewStyle = .dimmed
            }
            bulletinManager.prepare()
            bulletinManager.presentBulletin(above: self.tabBarController ?? self)
            return
        }

        userdefault.set(true, forKey: userDefaultKey.ARONOFF.rawValue)
        userdefault.synchronize()
        var arProfileClosetController: UIViewController!

        if #available(iOS 11.0, *) {
            if (ARConfiguration.isSupported) {
                if userdefault.object(forKey: userDefaultKey.ARONOFF.rawValue) == nil {
                    arProfileClosetController = THNonARClosetViewController(nibName: THNonARClosetViewController.className, bundle: nil)
                } else {
                    if let ARON = userdefault.object(forKey: userDefaultKey.ARONOFF.rawValue) as? Bool {
                        if ARON {
                            arProfileClosetController = THARClosetViewController(nibName: THARClosetViewController.className, bundle: nil)
                        } else {
                            arProfileClosetController = THNonARClosetViewController(nibName: THNonARClosetViewController.className, bundle: nil)
                        }
                    } else {
                        arProfileClosetController = THARClosetViewController(nibName: THARClosetViewController.className, bundle: nil)
                    }
                }
            } else {
                arProfileClosetController = THNonARClosetViewController(nibName: THNonARClosetViewController.className, bundle: nil)
            }
        } else {
            arProfileClosetController = THNonARClosetViewController(nibName: THNonARClosetViewController.className, bundle: nil)
        }
        let navClosetVC = UINavigationController(rootViewController: arProfileClosetController)
        navClosetVC.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        navClosetVC.tabBarItem.image = #imageLiteral(resourceName: "ic_tab_closet")
        navClosetVC.tabBarItem.selectedImage = #imageLiteral(resourceName: "ic_tab_closet")
        self.tabBarController?.viewControllers![3] = navClosetVC
        
        //Add funner for toggle AR
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Toggle AR", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "status": "on"])
        }
        NotificationCenter.default.removeObserver(self, name: Notification.Name(postNotificationType.notificationOrderSucessfully.rawValue), object: nil)

        NotificationCenter.default.removeObserver(self, name: Notification.Name(postNotificationType.notificationNearBy.rawValue), object: nil)
    }
    
    
    @IBAction func didTapOnARToogleFromPopupbutton(_ sender: Any) {
        if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) == .denied {
            bulletinManager = BulletinManager(rootItem: GeneralMethods.sharedInstance.bullitenItemOfCameraDeniedPermission(openSettingCompletion: {
                self.bulletinManager.dismissBulletin(animated: true, completion: {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!)
                    } else {
                        UIApplication.shared.openURL(URL(string: "prefs://")!)
                    }
                })
            }, NotNowCompletion: {
                self.bulletinManager.dismissBulletin(animated: true, completion: {
                })
            }))
            if #available(iOS 10, *) {
                self.bulletinManager.backgroundViewStyle = .blurred(style: .light, isDark: false)
            } else {
                self.bulletinManager.backgroundViewStyle = .dimmed
            }
            bulletinManager.prepare()
            bulletinManager.presentBulletin(above: self.tabBarController ?? self)
            return
        }
        
        if #available(iOS 11.0, *) {
            if (ARConfiguration.isSupported) {
                self.dismiss(animated: false, completion: {
                    self.toggleARNONARPopup!()
                })
            }
        }
    }
    
    @IBAction func didTapOnDropdownMenu(_ sender: Any) {
        if !dropdownShowing {
            if let _ = dropdownViewController {
                return
            }
            
            var yPos : CGFloat = 0
            if UIDevice().userInterfaceIdiom == .phone {
                switch UIScreen.main.nativeBounds.height {
                case 2436:
                    yPos = 88
                default:
                    yPos = 64
                }
            }
            
            dropdownViewController = THClosetDropdownMenu(nibName: THClosetDropdownMenu.className, bundle: nil)
            self.addChildViewController(dropdownViewController)
            dropdownViewController.view.frame = CGRect(x: 0, y: yPos, width: screenwidth, height: screenheight-yPos)
            self.view.addSubview(dropdownViewController.view)
            dropdownViewController.isARView = false
            dropdownViewController.showAnimate()
        } else {
            if let dropdown = dropdownViewController {
                dropdown.removeAnimate()
            }
        }
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "DropDown Menu", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "From": "NON AR Closet"])
        }
    }
    
    @IBAction func didTapOnQRCodeButton(_ sender: Any) {
        let qr = QRViewController()
        let navQR = UINavigationController(rootViewController: qr)
        navQR.setNavigationBarHidden(true, animated: false)
        self.present(navQR, animated: true, completion: nil) 
    }
    

    func popupShops() {
        if let dropdown = dropdownViewController {
            dropdown.removeFromParentViewController()
            dropdown.view.removeFromSuperview()
            dropdownViewController = nil
            dropdownShowing = false
            self.tabBarController?.selectedIndex = 1
        }
    }

    @IBAction func didtapOnPopUpCloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }


    //MARK: Notification observer
    func notificationPostFollow(notification: NSNotification) {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
        }
        let followNotificationJson = JSON(notification.object!)
        THProfilePostsManager.sharedInstance.retrieveCurrentProfilePosts(with: followNotificationJson["notification_creator_id"].intValue) { (error) in
            guard error != nil else {
                UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber - 1
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let userPostProfileVC = storyBoard.instantiateViewController(withIdentifier: PZProfileTableViewController.className) as! PZProfileTableViewController
                userPostProfileVC.profile = THProfilePostsManager.sharedInstance.currentUserProfile
                userPostProfileVC.isSearch = true
                userPostProfileVC.userId = followNotificationJson["notification_creator_id"].intValue
                self.navigationController?.pushViewController(userPostProfileVC, animated: true)
                return
            }
        }
    }


    func notificationProduct(notification: NSNotification) {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
        }
        let productNotificationJson = JSON(notification.object!)
        self.navigateToProductDetailPage(brandId: productNotificationJson["brand_id"].intValue, productId: productNotificationJson["product_id"].intValue, geofence: false)
    }

    //MARK: ALL ENDPOINT METHODS
    func callBrandsToFollowServices() {
        THProfilePostsManager.sharedInstance.profileBrandFollowing = [Brand]()
        THProfilePostsManager.sharedInstance.pageBrand = 1
        THProfilePostsManager.sharedInstance.didLoadDataBrand = false
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            THProfilePostsManager.sharedInstance.retrieveProfileBrandFollowing(with: usercurrent.userId) { (error) in
                guard let error = error else {
                    
                    self.brandListingCollectionView.reloadData()
                    return
                }
                debugPrint(error)
            }
        }
    }

    //MARK: LOAD ENDPOINT FOR AR VIEWS
    func loadEndPointForBrand(brandId: Int, isRefresh: Bool = false) {
        selectedPosterType = "Brand"
        if isRefresh {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THBrandsManager.sharedInstance.products = [ClosetCategory]()
            self.currentPage = 0
            self.posterCollectionView.reloadData()
            THBrandsManager.sharedInstance.page = 1
            THBrandsManager.sharedInstance.retrieveBrand(for: brandId, isFromCloset: true) { (cartBrand, error) in
                self.posterCollectionView.pullToRefreshView(at: .left).stopAnimating()
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                if(error==nil) {
                    self.posterCollectionView.reloadData()
                }
            }
        } else {
            if THBrandsManager.sharedInstance.products.count > 0 {
                if THBrandsManager.sharedInstance.products[0].brandId == brandId {
                    self.posterCollectionView.reloadData()
                    self.posterCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: false)
                } else {
                    self.loadEndPointForBrand(brandId: brandId, isRefresh: true)
                }
            } else {
                self.loadEndPointForBrand(brandId: brandId, isRefresh: true)
            }
        }
    }

    func loadEndPointForRetailNearByProducts(isRefresh: Bool = false) {
        if self.isFromRetailMapView {
            self.addBrandButton.isHidden = true
            self.brandListingCollectionView.isHidden = true
            self.popupHeaderView.isHidden = false
            self.QRCodeButton.isHidden = false
            self.posterCollectionTopConstraint.constant = -40
            self.posterCollectionBottomConstraint.constant = 54
        }
        selectedPosterType = "NearBy"
        if isRefresh {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THRetailBrandManager.sharedInstance.RetailProducts = [ClosetCategory]()
            self.posterCollectionView.reloadData()
            self.currentPage = 0
            THRetailBrandManager.sharedInstance.page = 1
            THRetailBrandManager.sharedInstance.retrievingRetailNearByProduct(for: locationId) { (error) in
                self.posterCollectionView.pullToRefreshView(at: .left).stopAnimating()
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                if self.isFromRetailMapView {
                    
                   
                }
                if(error==nil) {
                    self.posterCollectionView.reloadData()
                }
            }
        } else {
            if THRetailBrandManager.sharedInstance.RetailProducts.count > 0 {
                self.posterCollectionView.reloadData()
                self.posterCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: false)
            } else {
                self.loadEndPointForRetailNearByProducts(isRefresh: true)
            }
        }
    }

    func loadEndPointClosetFavorite(isRefresh: Bool = false) {
        selectedPosterType = "Favorites"
        if isRefresh {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                THProfileClosetManager.sharedInstance.closetFavorite = [ClosetCategory]()
                self.posterCollectionView.reloadData()
                self.currentPage = 0
                THProfileClosetManager.sharedInstance.pageFavorite = 1
                THProfileClosetManager.sharedInstance.didLoadDataFavorite = false
                THProfileClosetManager.sharedInstance.retrieveProfileClosetFavorite(with: usercurrent.userId, completion: { (error) in
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    if(error==nil) {
                        self.posterCollectionView.reloadData()
                    }
                })
            }
        } else {
            if THProfileClosetManager.sharedInstance.closetFavorite.count > 0 {
                self.posterCollectionView.reloadData()
                self.posterCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: false)
            } else {
                self.loadEndPointClosetFavorite(isRefresh: true)
            }
        }
    }

    func loadEndPointForCategories(isShowLoader: Bool, isRefresh:Bool = false) {
        selectedPosterType = "Categories"
        if isRefresh {
            if isShowLoader {
                GeneralMethods.sharedInstance.loadLoading(view: self.view)
            }
            self.posterCollectionView.reloadData()
            self.currentPage = 0
            THSocialFeedManager.sharedInstance.shopCategories = [ShopCategory]()
            THSocialFeedManager.sharedInstance.page = 1
            THSocialFeedManager.sharedInstance.didLoadData = false
            THSocialFeedManager.sharedInstance.retrieveNextPosts(completion: { (error) in
                if isShowLoader {
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                }
                if error != nil {
                    return
                }
                self.posterCollectionView.reloadData()
            })
        } else {
            if THSocialFeedManager.sharedInstance.shopCategories.count > 0 {
                self.posterCollectionView.reloadData()
                self.posterCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: false)
            } else {
                self.loadEndPointForCategories(isShowLoader: isShowLoader, isRefresh: true)
            }
        }

    }

    func loadEndPointForRecommendations(isShowLoader: Bool, isRefresh: Bool = false) {
        selectedPosterType = "Recommendations"
        selectedBrandIndex = 1
        if isRefresh {
            if isShowLoader {
                GeneralMethods.sharedInstance.loadLoading(view: self.view)
            }
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                self.posterCollectionView.reloadData()
                self.currentPage = 0
                THProfileClosetManager.sharedInstance.page = 1
                THProfileClosetManager.sharedInstance.didLoadData = false
                THProfileClosetManager.sharedInstance.retrieveProfileCloset(with: usercurrent.userId) { (error) in
                    if isShowLoader {
                        GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    }
                    self.posterCollectionView.pullToRefreshView(at: .left).stopAnimating()
                    guard let error = error else {
                        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                            if usercurrent.tempAccount {
                                self.usernameLabel.text = "Your Closet"
                            } else {
                                self.usernameLabel.text = "\(usercurrent.username)'s Closet"
                            }
                        }
                        self.posterCollectionView.reloadData()
                        return
                    }
                    debugPrint(error)
                }
            }
        } else {
            if THProfileClosetManager.sharedInstance.closetRecommendations.count > 0 {
                self.posterCollectionView.reloadData()
                self.posterCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: false)
            } else {
                self.loadEndPointForRecommendations(isShowLoader: isShowLoader, isRefresh: true)
            }
        }

    }

    func loadEndPointForOrder(isRefresh: Bool = false) {
        selectedPosterType = "Purchased"
        if isRefresh {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THUserOrdersManager.sharedInstance.userShippedProducts = [PurchasesOrders]()
            self.posterCollectionView.reloadData()
            self.currentPage = 0
            THUserOrdersManager.sharedInstance.page = 1
            THUserOrdersManager.sharedInstance.didLoadData = false
            THUserOrdersManager.sharedInstance.retrieveShippedOrder(with: (THUserManager.sharedInstance.currentUserdata()?.userId)!) { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                guard let error = error else {
                    self.posterCollectionView.reloadData()
                    return
                }
                debugPrint(error)
            }
        } else {
            if THUserOrdersManager.sharedInstance.userShippedProducts.count > 0 {
                self.posterCollectionView.reloadData()
                self.posterCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: false)
            } else {
                self.loadEndPointForOrder(isRefresh: true)
            }
        }
    }

    func loadEndPointForYourPosts(isRefresh: Bool = false) {
        selectedPosterType = "Post"
        if isRefresh {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THProfilePostsManager.sharedInstance.currentUserProfilePosts = [SocialInteraction]()
            self.posterCollectionView.reloadData()
            self.currentPage = 0
            THProfilePostsManager.sharedInstance.currentUserPage = 1
            THProfilePostsManager.sharedInstance.currentUserdidLoadData = false
            THProfilePostsManager.sharedInstance.retrieveCurrentProfilePosts(with: (THUserManager.sharedInstance.currentUserdata()?.userId)!) { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                guard error != nil else {
                    self.posterCollectionView.reloadData()
                    return
                }
            }
        } else {
            if THProfilePostsManager.sharedInstance.currentUserProfilePosts.count > 0 {
                self.posterCollectionView.reloadData()
                self.posterCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: false)
            } else {
                self.loadEndPointForYourPosts(isRefresh: true)
            }
        }

    }

    func loadMore(indexPath: IndexPath) {
        switch self.selectedPosterType {
        case "Categories":
            if indexPath.row == THSocialFeedManager.sharedInstance.shopCategories.count - 3 {
                if THSocialFeedManager.sharedInstance.didLoadData {
                    THSocialFeedManager.sharedInstance.didLoadData = false
                    THSocialFeedManager.sharedInstance.retrieveNextPosts(completion: { (error) in

                        guard error != nil else {
                            self.posterCollectionView.reloadData()
                            return
                        } })
                }
            }
            break
        case "Brand":
            if indexPath.row == THBrandsManager.sharedInstance.products.count - 3 {
                if THBrandsManager.sharedInstance.didLoadData {
                    THBrandsManager.sharedInstance.didLoadData = false
                    THBrandsManager.sharedInstance.retrieveBrand(for: selectedBrandID, isFromCloset: true) { (cartBrand, error) in
                        if(error==nil) {
                            self.posterCollectionView.reloadData()
                        }
                    }
                }
            }
            break
        case "NearBy":
            if indexPath.row == THRetailBrandManager.sharedInstance.RetailProducts.count - 3 {
                if THRetailBrandManager.sharedInstance.didLoadData {
                    THRetailBrandManager.sharedInstance.didLoadData = false
                    THRetailBrandManager.sharedInstance.retrievingRetailNearByProduct(for: locationId, completion: { (error) in
                        self.posterCollectionView.reloadData()
                    })
                }
            }
        case "Purchased":
            if indexPath.row == THUserOrdersManager.sharedInstance.userShippedProducts.count - 3 {
                    if THBrandsManager.sharedInstance.didLoadData {
                        THUserOrdersManager.sharedInstance.didLoadData = false
                        THUserOrdersManager.sharedInstance.retrieveShippedOrder(with: (THUserManager.sharedInstance.currentUserdata()?.userId)!) { (error) in
                            guard let error = error else {
                                self.posterCollectionView.reloadData()
                                return
                            }
                            debugPrint(error)
                        }
                }
            }
        case "Recommendations":
            if indexPath.row == THProfileClosetManager.sharedInstance.closetRecommendations.count - 3 {
                if THProfileClosetManager.sharedInstance.didLoadData {
                    THProfileClosetManager.sharedInstance.didLoadData = false
                    THProfileClosetManager.sharedInstance.retrieveProfileCloset(with: (THUserManager.sharedInstance.currentUserdata()?.userId)!) { (error) in
                        guard let error = error else {
                            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                                if usercurrent.tempAccount {
                                    self.usernameLabel.text = "Your Closet"
                                } else {
                                    self.usernameLabel.text = "\(usercurrent.username)'s Closet"
                                }
                            }
                            self.posterCollectionView.reloadData()
                            return
                        }
                        debugPrint(error)
                    }
                }
            }
        case "Post":
            if indexPath.row == THProfilePostsManager.sharedInstance.currentUserProfilePosts.count - 3 {
                if THProfilePostsManager.sharedInstance.currentUserdidLoadData {
                   THProfilePostsManager.sharedInstance.currentUserdidLoadData = false
                    THProfilePostsManager.sharedInstance.retrieveCurrentProfilePosts(with: (THUserManager.sharedInstance.currentUserdata()?.userId)!) { (error) in
                        guard error != nil else {
                            self.posterCollectionView.reloadData()
                            return
                        }
                    }
                }
            }
        case "Favorites":
            if indexPath.row == THProfileClosetManager.sharedInstance.closetFavorite.count - 3 {
                if THProfileClosetManager.sharedInstance.didLoadDataFavorite {
                    THProfileClosetManager.sharedInstance.didLoadDataFavorite = false
                    THProfileClosetManager.sharedInstance.retrieveProfileClosetFavorite(with: (THUserManager.sharedInstance.currentUserdata()?.userId)!, completion: { (error) in
                        guard error != nil else {
                            self.posterCollectionView.reloadData()
                            return
                        }
                    })
                }
            }
        default:
            break
        }
    }
}

extension THNonARClosetViewController: UIScrollViewDelegate {

}

extension THNonARClosetViewController: UICollectionViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if scrollView == self.posterCollectionView {
            let pageWidth = Float(itemWidth + itemSpacing)
            let targetXContentOffset = Float(targetContentOffset.pointee.x)
            let contentWidth = Float(posterCollectionView.contentSize.width  )
            var newPage = Float(currentPage)
            
            if velocity.x == 0 {
                newPage = floor( (targetXContentOffset - Float(pageWidth) / 2) / Float(pageWidth)) + 1.0
            } else {
                newPage = Float(velocity.x > 0 ? currentPage + 1 : currentPage - 1)
                if newPage < 0 {
                    newPage = 0
                }
                if (newPage > contentWidth / pageWidth) {
                    newPage = ceil(contentWidth / pageWidth) - 1.0
                }
            }
            
            currentPage = Int(newPage)
            let point = CGPoint (x: CGFloat(newPage * pageWidth), y: targetContentOffset.pointee.y)
            targetContentOffset.pointee = point
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView.tag {
        case 0:
            selectedCategoriesIndex = -1
            selectedBrandIndex = indexPath.item
            brandListingCollectionView.reloadData()
            switch indexPath.item {
                case 0:
                    self.loadEndPointClosetFavorite(isRefresh: true)
                    return
                case 1:
                    self.loadEndPointForRecommendations(isShowLoader: false)
                    return
                default:
                    break
            }
            
            selectedBrandID = THProfilePostsManager.sharedInstance.profileBrandFollowing[indexPath.item-2].id
            loadEndPointForBrand(brandId: THProfilePostsManager.sharedInstance.profileBrandFollowing[indexPath.item-2].id)
            //Add funnel of brand on closet feed
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Brand Closet Feed", customAttributes: ["userid":usercurrent.userId,
                                                                                         "username": usercurrent.username, "brand_id":THProfilePostsManager.sharedInstance.profileBrandFollowing[indexPath.row].id,
                                                                                         "brand_name":THProfilePostsManager.sharedInstance.profileBrandFollowing[indexPath.row].name])
            }
            break
        case 1:
            switch selectedPosterType {
            case "Categories":
                let selectShopCategory = THSocialFeedManager.sharedInstance.shopCategories[indexPath.row]
                let shopFeedController = THShopThreadestBlackViewController(nibName: THShopThreadestBlackViewController.className, bundle: nil)
                shopFeedController.shopTitle = selectShopCategory.title
                shopFeedController.cartCategoryString = selectShopCategory.category
                shopFeedController.categoryID = selectShopCategory.id
                shopFeedController.navigationFrom = "shop"
                shopFeedController.isPopup = true
                let shopFeedNavController = UINavigationController(rootViewController: shopFeedController)
                self.present(shopFeedNavController, animated: true, completion: nil)
            case "Brand":
                let selectCloset = THBrandsManager.sharedInstance.products[indexPath.row]
                self.navigateToProductDetailPage(brandId: selectCloset.brandId, productId: selectCloset.Id, geofence: false)
            case "Purchased":
                let selectCloset = THUserOrdersManager.sharedInstance.userShippedProducts[indexPath.row].productData
                self.navigateToProductDetailPage(brandId: selectCloset.brandId, productId: selectCloset.Id, geofence: false)
            case "Favorites":
                let selectCloset = THProfileClosetManager.sharedInstance.closetFavorite[indexPath.row]
                self.navigateToProductDetailPage(brandId: selectCloset.brandId, productId: selectCloset.Id, geofence: false)
            case "NearBy":
                let selectCloset = THRetailBrandManager.sharedInstance.RetailProducts[indexPath.row]
                self.navigateToProductDetailPage(brandId: selectCloset.brandId, productId: selectCloset.Id, geofence: true)
            case "Recommendations":
                let selectCloset = THProfileClosetManager.sharedInstance.closetRecommendations[indexPath.row]
                self.navigateToProductDetailPage(brandId: selectCloset.brandId, productId: selectCloset.Id, geofence: false)
            case "Post":
                let socialInteraction = THProfilePostsManager.sharedInstance.currentUserProfilePosts[indexPath.row]
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THProfilePostsManager.sharedInstance.retrieveNotificationPost(with: socialInteraction.id, completion: { (error) in
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    guard error != nil else {
                        let commentController = THCommentViewController(nibName: THCommentViewController.className, bundle: nil)
                        commentController.socialInteraction = socialInteraction
                        commentController.isPopup = true
                        let navCommentVC = UINavigationController(rootViewController: commentController)
                        self.present(navCommentVC, animated: true, completion: nil)
                        return
                    }
                })
            default:
                break
            }
            break
        case 2:
            selectedBrandIndex = -1
            selectedCategoriesIndex = indexPath.row
            brandListingCollectionView.reloadData()
            let selectedCategory = categoriesListingArray[indexPath.row]
            switch selectedCategory {
            case "Purchased":
                //Add funnel of closet feed
                if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    Answers.logCustomEvent(withName: "Tap on Purchased Tab From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
                }
                loadEndPointForOrder()
                break
            case "Categories":
                //Add funnel of Category tab
                if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    Answers.logCustomEvent(withName: "Tap on Categories Tab From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
                }
                loadEndPointForCategories(isShowLoader: true)
                break
            case "Recommendations":
                //Add funnel of closet feed
                if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    Answers.logCustomEvent(withName: "Tap on Recommendations Tab From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
                }
                loadEndPointForRecommendations(isShowLoader: true)
                break
            case "Your Posts":
                //Add funnel of closet feed
                if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    Answers.logCustomEvent(withName: "Tap on Your Posts Tab From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
                }
                loadEndPointForYourPosts()
                break
            case "Favorites":
                //Add funnel of closet feed
                if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    Answers.logCustomEvent(withName: "Tap on Favorites Tab From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
                }
                loadEndPointClosetFavorite()
                break
            case "Pop Ups":
                break
            default:
                break
            }
            break
        default:
            break
        }

    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        switch collectionView.tag {
        case 0:
            if indexPath.row == THProfilePostsManager.sharedInstance.profileBrandFollowing.count - 3 {
                if THProfilePostsManager.sharedInstance.didLoadDataBrand {
                    THProfilePostsManager.sharedInstance.didLoadDataBrand = false
                    if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                        THProfilePostsManager.sharedInstance.retrieveProfileBrandFollowing(with: usercurrent.userId) { (error) in
                            guard let error = error else {
                                self.brandListingCollectionView.reloadData()
                                return
                            }
                            debugPrint(error)
                        }
                    }
                }
            }
        case 1:
           self.loadMore(indexPath: indexPath)
        default:
            break
        }
    }

    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        switch collectionView.tag {
        case 1:
            if selectedPosterType == "Post" {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: THARPostCollectionViewCell.className, for: indexPath) as! THARPostCollectionViewCell
                cell.commentButton.removeTarget(nil, action: nil, for: .touchUpInside)
                cell.likeButton.removeTarget(nil, action: nil, for: .touchUpInside)
                cell.likeCountButton.removeTarget(nil, action: nil, for: .touchUpInside)
                cell.rethreadTopButton.removeTarget(nil, action:nil, for: .touchUpInside)
            } else {
                let posterCell = collectionView.dequeueReusableCell(withReuseIdentifier: THRecomondationProductCollectionViewCell.className, for: indexPath) as! THRecomondationProductCollectionViewCell
                posterCell.likeButton.removeTarget(nil, action: nil, for: .touchUpInside)
                posterCell.commentButton.removeTarget(nil, action: nil, for: .touchUpInside)
                posterCell.rethreadButtonTop.removeTarget(nil, action: nil, for: .touchUpInside)
                posterCell.brandImageButton.removeTarget(nil, action: nil, for: .touchUpInside)
            }
        default:
            break
        }
    }
}

extension THNonARClosetViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView.tag {
        case 0:
            return THProfilePostsManager.sharedInstance.profileBrandFollowing.count + 2
        case 1:
            switch selectedPosterType {
            case "Brand":
                return  THBrandsManager.sharedInstance.products.count
            case "Purchased":
                return THUserOrdersManager.sharedInstance.userShippedProducts.count
            case "Recommendations":
               return THProfileClosetManager.sharedInstance.closetRecommendations.count
            case "Post":
                return THProfilePostsManager.sharedInstance.currentUserProfilePosts.count
            case "Favorites":
                return THProfileClosetManager.sharedInstance.closetFavorite.count
            case "NearBy":
                return THRetailBrandManager.sharedInstance.RetailProducts.count
            case "Categories":
                return THSocialFeedManager.sharedInstance.shopCategories.count
            default:
                return 0
            }
        case 2:
            return categoriesListingArray.count
        default:
            return 2
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView.tag {
        case 0:
            let brandCell = collectionView.dequeueReusableCell(withReuseIdentifier: BrandListCollectionViewCell.className, for: indexPath) as! BrandListCollectionViewCell
            if selectedBrandIndex == indexPath.item {
                brandCell.brandImageView.layer.borderWidth = 2
                brandCell.brandImageView.layer.borderColor = UIColor.richBlueButton.cgColor
            } else {
                brandCell.brandImageView.layer.borderWidth = 1
                brandCell.brandImageView.layer.borderColor = UIColor.lightGray.cgColor
            }
            if indexPath.item < 2 {
                
                brandCell.brandImageView.image = indexPath.item == 0 ? Ionicons.androidFavorite.image(35, color: .custColor(r: 233, g: 0, b: 55, a: 1)) : UIImage(named: "rc_icon")
                brandCell.brandImageView.contentMode = .center
            } else {
                brandCell.brandImageView.sd_setImage(with: URL(string: THProfilePostsManager.sharedInstance.profileBrandFollowing[indexPath.item-2].logoURL), placeholderImage: nil, options: .retryFailed)
                 brandCell.brandImageView.contentMode = .scaleAspectFit
            }
            return brandCell
        case 1:
            let posterCell = collectionView.dequeueReusableCell(withReuseIdentifier: THRecomondationProductCollectionViewCell.className, for: indexPath) as! THRecomondationProductCollectionViewCell
            var closetCategory: ClosetCategory?
            switch selectedPosterType {
            case "Brand":
                closetCategory = THBrandsManager.sharedInstance.products[indexPath.item]
            case "Purchased":
                closetCategory = THUserOrdersManager.sharedInstance.userShippedProducts[indexPath.item].productData
            case "NearBy":
                closetCategory = THRetailBrandManager.sharedInstance.RetailProducts[indexPath.item]
            case "Recommendations":
                closetCategory = THProfileClosetManager.sharedInstance.closetRecommendations[indexPath.item]
            case "Favorites":
                closetCategory = THProfileClosetManager.sharedInstance.closetFavorite[indexPath.item]
            case "Post":
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: THARPostCollectionViewCell.className, for: indexPath) as! THARPostCollectionViewCell
                cell.commentButton.tag = indexPath.item
                cell.commentButton.addTarget(self, action: #selector(THNonARClosetViewController.didTapOnCommentProductButton(_:)), for: UIControlEvents.touchUpInside)
                cell.likeButton.tag = indexPath.item
                cell.likeButton.addTarget(self, action: #selector(THNonARClosetViewController.didTapOnLikePostButton(_:)), for: .touchUpInside)
                cell.likeCountButton.tag = indexPath.item
                cell.likeCountButton.addTarget(self, action: #selector(THNonARClosetViewController.didTapOnLikescountPostButton(_:)), for: .touchUpInside)
                cell.rethreadTopButton.tag = indexPath.item
                cell.rethreadTopButton.addTarget(self, action: #selector(THNonARClosetViewController.didTapOnRethreadButton(_:)), for: .touchUpInside)
                cell.configure(withSocialInteraction: THProfilePostsManager.sharedInstance.currentUserProfilePosts[indexPath.item])
                return cell
            case "Categories":
                let categoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: THNONARCategoryCollectionViewCell.className, for: indexPath) as! THNONARCategoryCollectionViewCell
                categoryCell.configure(withCategory: THSocialFeedManager.sharedInstance.shopCategories[indexPath.item])
                return categoryCell
            default:
                break
            }
            posterCell.giveawayButton.isHidden = closetCategory?.product_type == "Giveaway" ? false:true
            if closetCategory?.product_type == "Giveaway" {
                posterCell.giveawayButton.setTitle(closetCategory?.giveaway_button_text, for: .normal)
            }
            posterCell.configure(withClosetCategory: closetCategory!)
            posterCell.likeButton.tag = indexPath.item
            posterCell.likeButton.addTarget(self, action: #selector(THNonARClosetViewController.didTapOnLikeProductButton(_:)), for: .touchUpInside)
            posterCell.commentButton.tag = indexPath.item
            posterCell.commentButton.addTarget(self, action: #selector(THNonARClosetViewController.didTapOnCommentProductButton(_:)), for: .touchUpInside)
            posterCell.giveawayButton.tag = indexPath.item
            posterCell.giveawayButton.addTarget(self, action: #selector(THNonARClosetViewController.didTapOnGiveAwayButton(_:)), for: .touchUpInside)
            posterCell.rethreadButtonTop.tag = indexPath.item
            posterCell.rethreadButtonTop.addTarget(self, action: #selector(THNonARClosetViewController.didTapOnRethreadButton(_:)), for: .touchUpInside)

            posterCell.brandImageButton.tag = indexPath.item
            posterCell.brandImageButton.addTarget(self, action: #selector(THNonARClosetViewController.didTapOnBrandProfileButton(_:)), for: .touchUpInside)

            posterCell.configure(withClosetCategory: closetCategory!)

            posterCell.selectSizeButton.tag = indexPath.item
            posterCell.selectSizeButton?.addTarget(self, action: #selector(THNonARClosetViewController.didTapAddSelectSize(_:)), for: UIControlEvents.touchUpInside)
            posterCell.addToCartButton.tag = indexPath.item
            posterCell.addToCartButton?.addTarget(self, action: #selector(THNonARClosetViewController.didTapAddToCart(_:)), for: UIControlEvents.touchUpInside)
            posterCell.applepaymentbutton.tag = indexPath.item
            posterCell.applepaymentbutton.addTarget(self, action: #selector(THNonARClosetViewController.didTapAddApplePay(_:)), for: .touchUpInside)

            if let product = self.selectedSize[(closetCategory?.Id)!] {
                posterCell.selectSizeButton.setTitle(product.selectedSize, for: .normal)
                posterCell.addToCartButton.isHidden = false
                posterCell.applePayButton.isHidden = false
                posterCell.payButtonTrailingToCell.isActive = true
            } else {
                posterCell.selectSizeButton.setTitle("Select Size", for: .normal)
                posterCell.addToCartButton.isHidden = true
                posterCell.applePayButton.isHidden = true
                posterCell.payButtonTrailingToCell.isActive = false
            }
            return posterCell
        case 2:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: THCategoriedListingCollectionViewCell.className, for: indexPath) as! THCategoriedListingCollectionViewCell
            cell.configure(categoryName: categoriesListingArray[indexPath.row])
            if selectedCategoriesIndex == indexPath.row {
                cell.categoriedLabel.backgroundColor = UIColor.richBlueButton
            } else {
                switch categoriesListingArray[indexPath.row] {
                case "Pop Ups":
                    cell.categoriedLabel.backgroundColor = UIColor.hex("81c88f", alpha: 1)
                    break
                case "Favorites":
                    cell.categoriedLabel.backgroundColor = UIColor.hex("fcdf4e", alpha: 1)
                    break
                default:
                    cell.categoriedLabel.backgroundColor = UIColor.closetBlack
                    break
                }
            }
            return cell
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: THCategoriedListingCollectionViewCell.className, for: indexPath) as! THCategoriedListingCollectionViewCell
            cell.categoriedLabel.text = categoriesListingArray[indexPath.row]
            return cell
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
        switch collectionView.tag {
        case 0:
            if screenheight <= 568 {
                return CGSize(width: 40, height: 40)
            } else {
                return CGSize(width: 50, height: 50)
            }
        case 1:
            if deviceType == .pad {
                return CGSize(width: 350, height: 380)
            } else {
                return CGSize(width: collectionView.frame.size.width - 32.0, height: collectionView.frame.size.height - 10.0)
            }
        case 2:
            var stringWidth:CGSize!
            let categoryName = categoriesListingArray[indexPath.row]
            if screenheight <= 568 {
                stringWidth = categoryName.size(attributes: [NSFontAttributeName: UIFont.AppFontRegular(11)])
            } else {
                stringWidth = categoryName.size(attributes: [NSFontAttributeName: UIFont.AppFontRegular(14)])
            }
            return CGSize(width: Int(stringWidth.width + 15.0), height: 35)
        default:
            return CGSize(width: 0, height: 0)
        }
    }
}

extension THNonARClosetViewController: SelectedSize {
    func selectedSize(id: Int, product: SelectedProduct) {
        self.selectedSize[id] = product
        self.posterCollectionView.reloadData()
    }
}

extension THNonARClosetViewController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .overFullScreen
    }

    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {

    }
}

//@available(iOS 11.0, *)
extension THNonARClosetViewController: LocationMangerRegionDelegate {

    func retailEndpoitCalled() {

    }

    func locationManagerUpdate(coordinate: CLLocationCoordinate2D) {

    }


    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {

    }

    func locationManager(_ manager: CLLocationManager, didVisit visit: CLVisit) {
        self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "", body: "Did visit", completion: {
        }), animated: true, completion: nil)
    }

    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        let state = UIApplication.shared.applicationState
        if state == .background {
            GeneralMethods.sharedInstance.dispatchlocalNotification(with: "Threadest", body: "Products found nearby! Go to the nearby tab in your closet to check it out.", userInfo: nil, at: Date())
        } else if state == .active {
           
        }
    }

    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
       
    }

    func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
        switch state {
        case .inside:
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            self.isProductFound = true
            
            break
        case .outside:
          
            break
        case .unknown:
            break
        }
    }
}

extension THNonARClosetViewController: UIPopoverControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        dropDownArraow.isSelected = true
        return .none
    }

    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        dropDownArraow.isSelected = false
        return true
    }
}

extension THNonARClosetViewController: PKPaymentAuthorizationViewControllerDelegate {
    @available(iOS 11.0, *)
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
        // Apple Pay Stripe
        STPAPIClient.shared().createToken(with: payment) { (token, error) in

            guard (error == nil) else {
                completion(PKPaymentAuthorizationResult.init(status: PKPaymentAuthorizationStatus.failure, errors: [error!]))
                return
            }

            if let card = token!.allResponseFields["card"] as? NSDictionary {

                let cardToken = card["id"] as! String
                let tokenID = token!.tokenId
                var firstName = ""
                var lastName = ""
                var address1 = ""
                var address2 = ""
                var state = ""
                var city = ""
                var zipCode = ""
                var phoneNo = ""
                var email = ""

                let streets = payment.shippingContact?.postalAddress!.street.components(separatedBy: "\n")

                if let first = payment.shippingContact?.name?.givenName {
                    firstName = first
                }

                if let last = payment.shippingContact?.name?.familyName {
                    lastName = last
                }

                if let add1 = streets?[0] {
                    address1 = add1
                }

                if ((streets?.count)! > 1) {
                    let add2 = streets?[1]
                    address2 = add2!
                }

                if let addState = payment.shippingContact?.postalAddress!.state {
                    state = addState
                }

                if let addCity = payment.shippingContact?.postalAddress!.city  {
                    city = addCity
                }

                if let addZip = payment.shippingContact?.postalAddress!.postalCode  {
                    zipCode = addZip
                }

                if let phone = payment.shippingContact?.phoneNumber  {
                    phoneNo = phone.stringValue
                }

                if let emailAddress = payment.shippingContact?.emailAddress {
                    email = emailAddress
                }
                let productVar = self.selectedVariation!

                THApplePayManager.sharedInstance.instantBuyNative(with: productVar.productId, productVarId: productVar.id,tokenId: tokenID, cardToken: cardToken, firstName: firstName, lastName: lastName, streetAddress1: address1, streetAddress2: address2, state: state, city: city, zipCode: zipCode, phone: phoneNo, email: email, postId: -1, completion: { (error,message ) in

                    guard (error == nil) else {
                        completion(PKPaymentAuthorizationResult.init(status: PKPaymentAuthorizationStatus.failure, errors: nil))
                        return
                    }

                    if message != "" {
                        controller.dismiss(animated: false, completion: {
                            self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "", body: message!, completion: { () in}), animated: false, completion: nil)
                        })
                    } else {
                        completion(PKPaymentAuthorizationResult.init(status: PKPaymentAuthorizationStatus.success, errors: nil))
                        controller.dismiss(animated: false) {
                            self.view.window!.rootViewController?.dismiss(animated: true, completion: {
                                NotificationCenter.default.post(name: Notification.Name(postNotificationType.notificationOrderSucessfully.rawValue), object: nil, userInfo: nil)
                            })
                        }
                    }
                })
            }
        }
    }

    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {

        // Apple Pay Stripe
        STPAPIClient.shared().createToken(with: payment) { (token, error) in

            guard (error == nil) else {
                completion(.failure)
                return
            }

            if let card = token!.allResponseFields["card"] as? NSDictionary {

                let cardToken = card["id"] as! String
                let tokenID = token!.tokenId
                var firstName = ""
                var lastName = ""
                var address1 = ""
                var address2 = ""
                var state = ""
                var city = ""
                var zipCode = ""
                var phoneNo = ""
                var email = ""

                let streets = payment.shippingContact?.postalAddress!.street.components(separatedBy: "\n")

                if let first = payment.shippingContact?.name?.givenName {
                    firstName = first
                }

                if let last = payment.shippingContact?.name?.familyName {
                    lastName = last
                }

                if let add1 = streets?[0] {
                    address1 = add1
                }

                if ((streets?.count)! > 1) {
                    let add2 = streets?[1]
                    address2 = add2!
                }

                if let addState = payment.shippingContact?.postalAddress!.state {
                    state = addState
                }

                if let addCity = payment.shippingContact?.postalAddress!.city  {
                    city = addCity
                }

                if let addZip = payment.shippingContact?.postalAddress!.postalCode  {
                    zipCode = addZip
                }

                if let phone = payment.shippingContact?.phoneNumber  {
                    phoneNo = phone.stringValue
                }

                if let emailAddress = payment.shippingContact?.emailAddress {
                    email = emailAddress
                }
                let productVar = self.selectedVariation!

                THApplePayManager.sharedInstance.instantBuyNative(with: productVar.productId, productVarId: productVar.id,tokenId: tokenID, cardToken: cardToken, firstName: firstName, lastName: lastName, streetAddress1: address1, streetAddress2: address2, state: state, city: city, zipCode: zipCode, phone: phoneNo, email: email, postId: -1, completion: { (error,message ) in

                    guard (error == nil) else {
                        completion(.failure)
                        return
                    }

                    if message != "" {
                        controller.dismiss(animated: false, completion: {
                            self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "", body: message!, completion: { () in}), animated: false, completion: nil)
                        })
                    } else {
                        completion(.success)
                        controller.dismiss(animated: false) {
                            self.view.window!.rootViewController?.dismiss(animated: true, completion: {
                                NotificationCenter.default.post(name: Notification.Name(postNotificationType.notificationOrderSucessfully.rawValue), object: nil, userInfo: nil)
                            })
                        }
                    }
                })
            }
        }
    }

    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        controller.dismiss(animated: false) {

        }
    }



    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didSelectShippingContact contact: PKContact, completion: @escaping (PKPaymentAuthorizationStatus, [PKShippingMethod], [PKPaymentSummaryItem]) -> Void) {

        guard let zipcode = contact.postalAddress?.postalCode, let city = contact.postalAddress?.city, let state = contact.postalAddress?.state else{
            completion(.invalidShippingPostalAddress, [], [])
            return
        }

        let productVar = self.selectedVariation!
        THApplePayManager.sharedInstance.instantBuyTaxCalculation(with: productVar.productId, zipCode: zipcode, city: city, state: state) { (error, tax) in

            guard (error == nil) else {
                completion(.invalidShippingPostalAddress, [], [])
                return
            }

            let price = self.selectedCategory!.salePrice == 0.0 ? self.selectedCategory!.defaultPrice:self.selectedCategory!.salePrice
            var summaryItems = [PKPaymentSummaryItem]()
            let subtotal = PKPaymentSummaryItem(label: "SUBTOTAL", amount: NSDecimalNumber(value: price))
            let salesTax = PKPaymentSummaryItem(label: "SALES TAX & FEES", amount: NSDecimalNumber(value: tax!))
            let shipping = PKPaymentSummaryItem(label: "SHIPPING", amount: 0)

            let target = Double(price.adding(Float(tax!)))
            let payTarget = PKPaymentSummaryItem(label: "THREADEST INC.", amount: NSDecimalNumber(value: target))

            summaryItems = [subtotal, salesTax, shipping, payTarget]

            completion(.success, [], summaryItems)

        }
    }
}

class markerAnnotation: MKPointAnnotation {
    var id: String?
    var brandName: String?
}

