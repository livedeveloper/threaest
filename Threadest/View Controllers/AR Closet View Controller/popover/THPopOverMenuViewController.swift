//
//  THPopOverMenuViewController.swift
//  Threadest
//
//  Created by Jaydeep on 20/09/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import ARKit

class THPopOverMenuViewController: UIViewController {
    
    open var popOverCompletionHander : ((_ itemSelected: String)->())?
    
    @IBOutlet weak var objTableView: UITableView!
    var itemArray = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        itemArray = ["Profile","Search","Your Brands","Add Brands","Camera", "Social", "Shopping"]
       
         objTableView.register(UINib(nibName: THDropDownTableViewCell.className, bundle: nil), forCellReuseIdentifier: THDropDownTableViewCell.className)
        // Do any additional setup after loading the view.
    }
}

extension THPopOverMenuViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true) {
            self.popOverCompletionHander!(self.itemArray[indexPath.row])
        }
    }
}

extension THPopOverMenuViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dropdownCell = tableView.dequeueReusableCell(withIdentifier: THDropDownTableViewCell.className, for: indexPath) as! THDropDownTableViewCell
        dropdownCell.itemLabel.text = itemArray[indexPath.row]
        return dropdownCell
    }
}

