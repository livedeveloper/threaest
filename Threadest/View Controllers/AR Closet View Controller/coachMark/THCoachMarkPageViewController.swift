//
//  THCoachMarkPageViewController.swift
//  Threadest
//
//  Created by Jaydeep on 06/11/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THCoachMarkPageViewController: UIViewController {
    var closetCoachArray = ["coach_closet02","coach_closet03","coach_closet04"]
    var currentIndex = -1
    var currentImage: UIImage!
    var isARView: Bool = true
    @IBOutlet weak var coachImageView: UIImageView!
    @IBOutlet weak var centerNextButton: DesignableButton!
    
    @IBOutlet weak var centerQuitButton: DesignableButton!
    
    @IBOutlet weak var bottomQuitButton: DesignableButton!
    
    @IBOutlet weak var bottomNextButton: DesignableButton!
    
    var isFromCloset: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        if isFromCloset {
            nextImageView()
            centerNextButton.isHidden = false
            centerQuitButton.isHidden = false
            
            bottomNextButton.isHidden = true
            bottomQuitButton.isHidden = true
        } else {
            centerNextButton.isHidden = true
            centerQuitButton.isHidden = true
            
            bottomNextButton.isHidden = false
            bottomQuitButton.isHidden = false
        }
    }
    
    func initUI() {
        if isARView {
            closetCoachArray = ["coach_closet02","coach_closet04"]
        } else {
            closetCoachArray = ["coach_closet02"]
        }
    }
    
    func showAnimate() {
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.8, animations: {
            self.view.alpha = 1.0
        });
    }
    
    func removeView() {
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }

    func nextImageView() {
        coachImageView.alpha = 0
        if currentIndex < closetCoachArray.count-1 {
            currentIndex = currentIndex + 1
            currentImage = UIImage(named: closetCoachArray[currentIndex])
        } else {
            removeView()
            return
        }
        coachImageView.image = currentImage
        UIView.animate(withDuration: 0.8, animations: {
            self.coachImageView.alpha = 1
        }) { (finish) in
            
        }
    }
    
    @IBAction func didTapOnCenterNextButton(_ sender: Any) {
        nextImageView()
    }
    
    @IBAction func didTapOnBottomNextButton(_ sender: Any) {
        removeView()
    }
    
    @IBAction func didTapOnQuitButton(_ sender: Any) {
        removeView()
    }
    
}
