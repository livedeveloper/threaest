//
//  THProductFountViewController.swift
//  Threadest
//
//  Created by Jaydeep on 07/09/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import Lottie

class THProductFountViewController: UIViewController {
    @IBOutlet weak var okButton: DesignableButton!
    
    open var productFoundPopupCompletionHander : (()->())?
    
    var timer:Timer!
    
    @IBOutlet weak var messageLabel: UILabel!
    var messageText = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        showAnimate()
        messageLabel.text = messageText
        timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(THProductFountViewController.removeFadeAway), userInfo: nil, repeats: false)
        let animationDoneView = LOTAnimationView(name: "done_button")
        animationDoneView.animationProgress = 100
        animationDoneView.backgroundColor = UIColor.clear
        animationDoneView.frame = CGRect(x: 0, y: 0, width: okButton.frame.size.width, height: okButton.frame.size.height-5)
        //animationDoneView.center = okButton.center
        animationDoneView.contentMode = .scaleAspectFill
        animationDoneView.animationSpeed = 0.5
        okButton.addSubview(animationDoneView)
        animationDoneView.loopAnimation = false
        animationDoneView.play { (sucess) in
            animationDoneView.pause()
            //animationDoneView.removeFromSuperview()
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeFadeAway() {
        if timer != nil {
            timer.invalidate()
            timer = nil
        }
        self.productFoundPopupCompletionHander!()
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    func removeAnimate() {
        if timer != nil {
            timer.invalidate()
            timer = nil
        }
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }

    @IBAction func didTapOkButton(_ sender: Any) {
        removeAnimate()
        self.productFoundPopupCompletionHander!()
    }
    
    @IBAction func didCloseButton(_ sender: Any) {
        removeAnimate()
        //self.productFoundPopupCompletionHander!()
    }
}
