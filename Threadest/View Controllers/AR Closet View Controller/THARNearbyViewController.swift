//
//  THARNearbyViewController.swift
//  Threadest
//
//  Created by mobilestar on 2/28/18.
//  Copyright © 2018 JC. All rights reserved.
//

import UIKit
import MapKit
import Crashlytics
import ARKit

class THARNearbyViewController: UIViewController {

    //Map view
    @IBOutlet weak var mapView: MKMapView!

    var userAnnotation: MKPointAnnotation?
    var locationEstimateAnnotation: MKPointAnnotation?
    var brandLocationAnnotationArray = [MKPointAnnotation]()
    var updateUserLocationTimer: Timer?

    let regionRadius: CLLocationDistance = 300

    var centerMapOnUserLocation: Bool = true

    let locationManager = LocationManager()

    //the bulletin manager.
    var bulletinManager: BulletinManager!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = true

        self.setupViews()
        self.loadMap()

        NotificationCenter.default.addObserver(self, selector: #selector(THARNearbyViewController.getNearByNotificationCalled), name: Notification.Name(postNotificationType.notificationNearBy.rawValue), object: nil)
    }

    func setupViews() {
        locationManager.regionDelegate = self
        self.mapView.delegate = self
        self.mapView.showsUserLocation = true

        if #available(iOS 11.0, *) {
            setupUserTrackingButtonAndScaleView()
        } else {
            // Fallback on earlier versions
        }

        if updateUserLocationTimer != nil {
            updateUserLocationTimer?.invalidate()
            updateUserLocationTimer = nil
        }

        updateUserLocationTimer = Timer.scheduledTimer(timeInterval: 6, target: self, selector: #selector(THARNearbyViewController.updateLocationWhenMapViewAppear), userInfo: nil, repeats: true)
    }

    @available(iOS 11.0, *)
    func setupUserTrackingButtonAndScaleView() {
        mapView.showsUserLocation = true
        let button = MKUserTrackingButton(mapView: mapView)
        button.layer.backgroundColor = UIColor(white: 1, alpha: 0.8).cgColor
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 5
        button.translatesAutoresizingMaskIntoConstraints = false
        mapView.addSubview(button)
        NSLayoutConstraint.activate([button.bottomAnchor.constraint(equalTo: mapView.bottomAnchor, constant: -10),
                                     button.trailingAnchor.constraint(equalTo: mapView.trailingAnchor, constant: -10)])

    }

    func updateLocationWhenMapViewAppear() {
        guard let currentCoordinates = LocationManager().currentLocation?.coordinate else {
            return
        }
        let locationMapPoint = MKMapPointForCoordinate(currentCoordinates)

        for brandan in self.brandLocationAnnotationArray {
            let distance = MKMetersBetweenMapPoints(locationMapPoint, MKMapPointForCoordinate(brandan.coordinate))
            if distance <= 50 {
                if brandan.subtitle == "Reach this location to access." {
                    brandan.subtitle = "You've discovered a Popup Shop! Tap to enter."
                    guard let markerAnno = brandan as? markerAnnotation else {
                        return
                    }
                    if #available(iOS 11.0, *) {
                        let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: markerAnno.id!) as? MKMarkerAnnotationView
                        annotationView?.markerTintColor = UIColor.hex("81c88f", alpha: 1)
                    } else {
                        // Fallback on earlier versions
                    }
                }
            } else {
                if brandan.subtitle == "You've discovered a Popup Shop! Tap to enter." {
                    brandan.subtitle = "Reach this location to access."
                    guard let markerAnno = brandan as? markerAnnotation else {
                        return
                    }
                    if #available(iOS 11.0, *) {
                        let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: markerAnno.id!) as? MKMarkerAnnotationView
                        annotationView?.markerTintColor = UIColor.hex("178dcd", alpha: 1)
                    } else {
                        // Fallback on earlier versions
                    }

                }
            }
        }
    }

    func getNearByNotificationCalled() {
        locationManager.retailStoreLocation(view: self.view)
        self.updateUserBrandLocation()
    }

    func loadMap() {
        if userdefault.object(forKey: userDefaultKey.bulletinPopupShopNearYou.rawValue) == nil {
            self.bulletinItemOfWelcomePopupShop {
                userdefault.set(true, forKey: userDefaultKey.bulletinPopupShopNearYou.rawValue)
                userdefault.synchronize()
                self.loadMap()
            }
        } else {
            if CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedAlways &&
                CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedWhenInUse && CLLocationManager.authorizationStatus() != CLAuthorizationStatus.denied {
                self.bullitenItemOfLocationAllowPermission(completion: {
                })

                self.updateUserBrandLocation()
                return
            }

            if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.denied ||
                CLLocationManager.authorizationStatus() == CLAuthorizationStatus.restricted {
                self.bullitenItemOfLocationDeniedPermission()
            }
        }




        let allAnnotations = self.mapView.annotations
        self.mapView.removeAnnotations(allAnnotations)

        brandLocationAnnotationArray = [MKPointAnnotation]()

        self.updateUserBrandLocation()
    }

    func updateUserBrandLocation() {
        //selectedPosterType = "Nearby"
        //Add funnel of Popup shop

        if let lat = LocationManager().currentLocation?.coordinate.latitude, let long = LocationManager().currentLocation?.coordinate.longitude {
            GeneralMethods.sharedInstance.loadLoading(view: view)
            THUserManager.sharedInstance.performUpdateUserLocaltion(with: lat, logitude: long, completion: { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                if THUserManager.sharedInstance.retailLocation.count > 0 {

                    self.centerMapOnLocation(location: LocationManager().currentLocation!)


                    for rlocation  in THUserManager.sharedInstance.retailLocation
                    {
                        guard let retailLatitude = CLLocationDegrees(exactly: rlocation.latitude), let retailLongitude = CLLocationDegrees(exactly: rlocation.longitude) else {
                            return
                        }

                        self.addAnnotationMapView(latitude: retailLatitude, logitude: retailLongitude, name: rlocation.brandName, id: "\(rlocation.id)")
                    }
                }
                else
                {
                    //show alert No Popups Found
                    let ac = UIAlertController(title: "Message", message: "No Popup Shops found within a 5 mile radius.", preferredStyle: .alert)
                    ac.addAction(UIAlertAction(title: "OK", style: .default))
                    self.present(ac, animated: true)
                }
            })
        }
    }

    func bulletinItemOfWelcomePopupShop(completion: @escaping () -> Void) {
        self.bulletinManager = BulletinManager(rootItem: GeneralMethods.sharedInstance.bullitenItemOfWelcomeRetailPopupShop {
            self.bulletinManager.dismissBulletin(animated: true, completion: {
                completion()
            })
        })
        if #available(iOS 10, *) {
            self.bulletinManager.backgroundViewStyle = .blurred(style: .light, isDark: false)
        } else {
            self.bulletinManager.backgroundViewStyle = .dimmed
        }
        self.bulletinManager.prepare()
        self.bulletinManager.presentBulletin(above: self.tabBarController ?? self)
    }

    func bullitenItemOfLocationAllowPermission(completion: @escaping () -> Void) {
        self.bulletinManager = BulletinManager(rootItem: GeneralMethods.sharedInstance.bullitenItemOfLocationAllowPermission(yesCompletion: {
            //Add funnel of location selection
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Location Allow Popup", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "status": "yes"])
            }
            self.bulletinManager.dismissBulletin(animated: true, completion: {
                self.locationManager.requestAuthorization(com: {
                    completion()
                    self.loadMap()
                })
            })
        }, noCompletion: {
            //Add funnel of location selection
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Location Allow Popup", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "status": "no"])
            }
        }))
        if #available(iOS 10, *) {
            self.bulletinManager.backgroundViewStyle = .blurred(style: .light, isDark: false)
        } else {
            self.bulletinManager.backgroundViewStyle = .dimmed
        }
        self.bulletinManager.prepare()
        self.bulletinManager.presentBulletin(above: self.tabBarController ?? self)
    }

    func bullitenItemOfLocationDeniedPermission() {
        self.bulletinManager = BulletinManager(rootItem: GeneralMethods.sharedInstance.bullitenItemOfLocationDeniedPermission(openSettingsCompletion: {
            //Add funnel of location permission in setting
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Location permission from settings", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
            }
            self.bulletinManager.dismissBulletin(animated: true, completion: {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!)
                } else {
                    UIApplication.shared.openURL(URL(string: "prefs://")!)
                }
            })
        }, notNowCompletion: {

        }))
        if #available(iOS 10, *) {
            self.bulletinManager.backgroundViewStyle = .blurred(style: .light, isDark: false)
        } else {
            self.bulletinManager.backgroundViewStyle = .dimmed
        }
        self.bulletinManager.prepare()
        self.bulletinManager.presentBulletin(above: self.tabBarController ?? self)
    }


    func addAnnotationMapView(latitude: CLLocationDegrees, logitude: CLLocationDegrees, name: String, id: String)  {
        let destinationCoordinates = CLLocationCoordinate2D(latitude: latitude , longitude: logitude)
        let annotation = markerAnnotation()
        annotation.id = id
        annotation.coordinate = destinationCoordinates
        annotation.title = name

        let locationMapPoint = MKMapPointForCoordinate((LocationManager().currentLocation?.coordinate)!)
        let pinMapPoint = MKMapPointForCoordinate(destinationCoordinates)
        let distance = MKMetersBetweenMapPoints(locationMapPoint, pinMapPoint)
        if distance <= 50 {
            annotation.subtitle = "You've discovered a Popup Shop! Tap to enter."
        } else {
            annotation.subtitle = "Reach this location to access."
        }
        mapView.addAnnotation(annotation)
        brandLocationAnnotationArray.append(annotation)
        let circle = MKCircle(center: destinationCoordinates, radius: 50)
        circle.title = name

        mapView.add(circle)
    }



    func centerMapOnLocation(location: CLLocation) {

        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }

    func drawPolylineOnMap(sorceCoordinates: CLLocationCoordinate2D, destinationCoordinates: CLLocationCoordinate2D, title: String) {

        let sourcePlacemark = MKPlacemark(coordinate: sorceCoordinates, addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: destinationCoordinates, addressDictionary: nil)

        // 4.
        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)

        // 7.
        let directionRequest = MKDirectionsRequest()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinationMapItem
        directionRequest.transportType = .automobile

        // Calculate the direction
        let directions = MKDirections(request: directionRequest)

        // 8.
        directions.calculate {
            (response, error) -> Void in

            guard let response = response else {
                if let error = error {
                    print("Error: \(error)")
                }

                return
            }

            let route = response.routes[0]
            self.mapView.add((route.polyline), level: MKOverlayLevel.aboveRoads)

            let rect = route.polyline.boundingMapRect
            self.mapView.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
        }
    }

    @objc func updateUserLocation() {

        if self.userAnnotation == nil {
            self.userAnnotation = MKPointAnnotation()
            self.mapView.addAnnotation(self.userAnnotation!)
        }

        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.allowUserInteraction, animations: {
            self.userAnnotation?.coordinate = (self.locationManager.currentLocation?.coordinate)!

        }, completion: nil)
    }

    @IBAction func didTapOnQRCode(_ sender: Any) {

        let qr = QRViewController()
        let navQR = UINavigationController(rootViewController: qr)
        navQR.setNavigationBarHidden(true, animated: false)
        self.present(navQR, animated: true, completion: nil)
    }

}

extension THARNearbyViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        guard let marker = view.annotation as? markerAnnotation else {
            return
        }
        if (view.annotation?.subtitle)! != "You've discovered a Popup Shop! Tap to enter." {
            return
        }
        //Add funnel of Popup shop
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Open Popup Shop Products", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }

        self.NONARViewController(markerID: Int(marker.id!)!)
        /*if #available(iOS 11.0, *) {
            if (ARConfiguration.isSupported) {
                ARViewController(markerID: Int(marker.id!)!)
            } else {
                self.NONARViewController(markerID: Int(marker.id!)!)
            }
        } else {
            self.NONARViewController(markerID: Int(marker.id!)!)
        }*/
    }

    func ARViewController(markerID: Int) {
        if #available(iOS 11.0, *) {
            let ARVC = THARClosetViewController(nibName: THARClosetViewController.className, bundle: nil)
            ARVC.selectedLocationId = markerID
            ARVC.selectedPosterType = "NearBy"
            ARVC.isFromRetailMapView = true
            ARVC.toggleARNONARPopup = {
                self.NONARViewController(markerID: markerID)
            }
            self.present(ARVC, animated: true, completion: nil)
        }
    }

    func NONARViewController(markerID: Int) {
        let NonArVC = THNonARClosetViewController(nibName: THNonARClosetViewController.className, bundle: nil)
        NonArVC.locationId = markerID
        NonArVC.isFromRetailMapView = true
        NonArVC.toggleARNONARPopup = {
            self.ARViewController(markerID: markerID)
        }
        self.present(NonArVC, animated: true, completion: nil)
    }


    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let overlay = overlay as? MKCircle {
            let circleRenderer = MKCircleRenderer(circle: overlay)
            circleRenderer.strokeColor = UIColor.green
            circleRenderer.lineWidth = 1.0
            circleRenderer.fillColor = UIColor.clear
            return circleRenderer
        }
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.hex("#178dcd", alpha: 1)
        renderer.lineWidth = 4.0
        return renderer
    }

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        view.setSelected(true, animated: true)
    }

    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        view.setSelected(false, animated: true)
    }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil

        }
        guard let markerAnno = annotation as? markerAnnotation else {
            return nil
        }

        if #available(iOS 11.0, *) {
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: markerAnno.id!) as? MKMarkerAnnotationView
            if annotationView == nil {
                annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: markerAnno.id)
                annotationView?.titleVisibility = .visible
                annotationView?.subtitleVisibility = .adaptive
                annotationView?.markerTintColor = markerAnno.subtitle == "You've discovered a Popup Shop! Tap to enter." ? UIColor.hex("81c88f", alpha: 1):UIColor.hex("178dcd", alpha: 1)
                annotationView?.animatesWhenAdded = true
                annotationView?.glyphImage = nil
                annotationView?.glyphText = markerAnno.title
                annotationView?.glyphTintColor = UIColor.white
                annotationView?.canShowCallout = true
                let brandButton = UIButton(type: .detailDisclosure)//UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
                brandButton.layer.borderWidth = 1
                brandButton.layer.borderColor = UIColor.gray.cgColor
                brandButton.layer.cornerRadius = brandButton.frame.size.width/2
                brandButton.layer.masksToBounds = true
                annotationView?.rightCalloutAccessoryView = brandButton
                if let i = THUserManager.sharedInstance.retailLocation.index(where: { $0.id == Int(markerAnno.id!)}){
                    let retailLocation = THUserManager.sharedInstance.retailLocation[i]
                    brandButton.sd_setImage(with: URL(string: retailLocation.brandLogoURL), for: .normal, placeholderImage: nil, options: .retryFailed, completed: nil)
                    brandButton.sd_setImage(with: URL(string: retailLocation.brandLogoURL), for: .normal, placeholderImage: nil, options: .retryFailed, completed: { (image, error, cache, url) in
                        brandButton.setImage(image?.withRenderingMode(.alwaysOriginal), for: .normal)
                    })
                }

            } else {
                annotationView?.annotation = annotation
                annotationView?.markerTintColor = markerAnno.subtitle == "You've discovered a Popup Shop! Tap to enter." ? UIColor.hex("81c88f", alpha: 1):UIColor.hex("178dcd", alpha: 1)
            }
            return annotationView
        } else {
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: markerAnno.id!) as? MKPinAnnotationView
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: markerAnno.id!)
            if annotationView == nil {
                annotationView?.canShowCallout = markerAnno.subtitle == "You've discovered a Popup Shop! Tap to enter." ? true: false
                annotationView?.pinTintColor = markerAnno.subtitle == "You've discovered a Popup Shop! Tap to enter." ? UIColor.hex("81c88f", alpha: 1):UIColor.hex("178dcd", alpha: 1)
                let brandButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
                brandButton.layer.borderWidth = 1
                brandButton.layer.borderColor = UIColor.gray.cgColor
                brandButton.layer.cornerRadius = brandButton.frame.size.width/2
                brandButton.layer.masksToBounds = true
                annotationView?.rightCalloutAccessoryView = brandButton
                if let i = THUserManager.sharedInstance.retailLocation.index(where: { $0.id == Int(markerAnno.id!)}){
                    let retailLocation = THUserManager.sharedInstance.retailLocation[i]
                    brandButton.sd_setImage(with: URL(string: retailLocation.brandLogoURL), for: .normal, placeholderImage: nil, options: .retryFailed, completed: nil)
                    brandButton.sd_setImage(with: URL(string: retailLocation.brandLogoURL), for: .normal, placeholderImage: nil, options: .retryFailed, completed: { (image, error, cache, url) in
                        //annotationView?.image = image
                    })
                }

            } else {
                annotationView?.annotation = annotation
            }
            return annotationView
        }
    }

    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }

}

//@available(iOS 11.0, *)
extension THARNearbyViewController: LocationMangerRegionDelegate {
    func retailEndpoitCalled() {

    }

    func locationManagerUpdate(coordinate: CLLocationCoordinate2D) {

    }

    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {

    }


    func locationManager(_ manager: CLLocationManager, didVisit visit: CLVisit) {
        self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "", body: "Did visit", completion: {
        }), animated: true, completion: nil)
    }

    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        let state = UIApplication.shared.applicationState
        if state == .background {
            GeneralMethods.sharedInstance.dispatchlocalNotification(with: "Threadest", body: "Products found nearby! Go to the nearby tab in your closet to check it out.", userInfo: nil, at: Date())
        } else if state == .active {

        }
    }

    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {

    }

    func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
        switch state {
        case .inside:

            break
        case .outside:

            break
        case .unknown:
            break
        }
    }
}

//extension THARNearbyViewController: MKMapViewDelegate {
//    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
//        let renderer = MKPolylineRenderer(overlay: overlay)
//        renderer.strokeColor = UIColor.hex("#178dcd", alpha: 1)
//        renderer.lineWidth = 4.0
//        return renderer
//    }
//
//    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
//        guard let sourceCoordinates = self.userAnnotation?.coordinate else {
//            return
//        }
//
//        guard let destinationCoordinates = view.annotation?.coordinate else {
//            return
//        }
//        guard let title = view.annotation?.title else {
//            return
//        }
//        if title != "My Location" && title != nil {
//            self.mapView.removeOverlays(self.mapView.overlays)
//            drawPolylineOnMap(sorceCoordinates: sourceCoordinates, destinationCoordinates: destinationCoordinates, title: title!)
//        }
//    }
//}

