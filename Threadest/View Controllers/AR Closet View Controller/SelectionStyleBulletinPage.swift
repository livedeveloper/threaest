//
//  SelectionStyleBulletinPage.swift
//  Threadest
//
//  Created by Jaydeep on 31/10/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation

class SelectionStyleBulletinPage: BulletinItem {

    /**
     * The object managing the item. Required by the `BulletinItem` protocol.
     *
     * You can use it to switch to the previous/next item or dismiss the bulletin.
     *
     * Always check if it is nil, as the manager will unset this value when the current item changes.
     */

    weak var manager: BulletinManager?

    /**
     * Whether the item can be dismissed. If you set this value to `true`, the user will be able
     * to dimiss the bulletin by tapping outside the card.
     *
     * You should set it to true for optional items or if it is the last in a configuration sequence.
     */

    var isDismissable: Bool = false

    /**
     * The block of code to execute when the bulletin item is dismissed. This is called when the bulletin
     * is moved out of view.
     *
     * You can leave it `nil` if `isDismissable` is set to false.
     *
     * - parameter item: The item that is being dismissed. When calling `dismissalHandler`, the manager
     * passes a reference to `self` so you don't have to manage weak references yourself.
     */

    public var dismissalHandler: ((_ item: BulletinItem) -> Void)? = nil

    /**
     * The item to display after this one. You can modify it at runtime based on user selection for
     * instance.
     *
     * Here, we will change the time based on the pet chosen by the user.
     */

    var nextItem: BulletinItem?

    /**
     * An object that creates standard inteface components.
     *
     * You should use it to create views whenever possible. It also allows to customize the tint color
     * of the buttons.
     */

    let interfaceFactory = BulletinInterfaceFactory()

    let menStyleArray = ["Casual", "Streetwear", "Luxury", "Business"]
    let mendStyleImageArray = ["man_casual","man_street","man_luxury","man_business"]
    let womenStyleArray = ["Casual", "Luxury", "Streetwear", "Business"]
    let womenStyleImageArray = ["woman_casual","woman_luxury","woman_street","woman_business"]
    var selectedStyle = [String]()

    var dismissCompletionHander : (()->())?

    var strGender = ""

    // MARK: - Interface Elements
    private var styleButtonContainer = Array<ContainerView<UIButton>>()
    private var saveButtonContainer: ContainerView<HighlightButton>!

    // MARK: - BulletinItem

    /**
     * Called by the manager when the item is about to be removed from the bulletin.
     *
     * Use this function as an opportunity to do any clean up or remove tap gesture recognizers /
     * button targets from your views to avoid retain cycles.
     */
    init(gender: String, dismissCompletion: @escaping () -> Void) {
        dismissCompletionHander = dismissCompletion
        strGender = gender
    }

    func tearDown() {
        saveButtonContainer?.contentView.removeTarget(self, action: nil, for: .touchUpInside)
    }

    /**
     * Called by the manager to build the view hierachy of the bulletin.
     *
     * We need to return the view in the order we want them displayed. You should use a
     * `BulletinInterfaceFactory` to generate standard views, such as title labels and buttons.
     */

    func makeArrangedSubviews() -> [UIView] {
        var arrangedSubviews = [UIView]()

        // Title Label

        let title = "Choose your favorite styles"
        let titleLabel = interfaceFactory.makeTitleLabel(reading: title)
        arrangedSubviews.append(titleLabel)

        // Description Label

        let isDescriptionCompact = false // The text is short, so we don't need to display it with a smaller font
        let descriptionLabel = interfaceFactory.makeDescriptionLabel(isCompact: isDescriptionCompact)
        descriptionLabel.text = "What type of clothes do you like in your closet?"
        arrangedSubviews.append(descriptionLabel)

        // Pets Stack

        // We add choice cells to a group stack because they need less spacing
        let petsStack = interfaceFactory.makeGroupStackBig(spacing: 16)
        arrangedSubviews.append(petsStack)

        var styleArray = ["", "", "", ""]
        var styleImageArray = ["", "", "", ""]
        if strGender == "male" {
            styleArray = menStyleArray
            styleImageArray = mendStyleImageArray
        } else {
            styleArray = womenStyleArray
            styleImageArray = womenStyleImageArray
        }

        // Cat Button
        for i in 0..<styleArray.count-2 {
            let catButtonContainer = createChoiceCell(imageName: styleImageArray[i], title: styleArray[i], isSelected: false, index: i)
            petsStack.addArrangedSubview(catButtonContainer)
            self.styleButtonContainer.append(catButtonContainer)
        }

        let petsStack1 = interfaceFactory.makeGroupStackBig(spacing: 16)
        arrangedSubviews.append(petsStack1)

        // Cat Button
        for i in styleArray.count-2..<styleArray.count {
            let catButtonContainer = createChoiceCell(imageName: styleImageArray[i], title: styleArray[i], isSelected: false, index: i)
            petsStack1.addArrangedSubview(catButtonContainer)
            self.styleButtonContainer.append(catButtonContainer)
        }
        // Save Button

        let saveButtonContainer = interfaceFactory.makeActionButton(title: "Save")
        saveButtonContainer.contentView.addTarget(self, action: #selector(saveButtonTapped), for: .touchUpInside)
        arrangedSubviews.append(saveButtonContainer)

        return arrangedSubviews

    }

    // MARK: - Custom Views

    /**
     * Creates a custom choice cell.
     */

    func createChoiceCell(imageName: String, title: String, isSelected: Bool, index: Int) -> ContainerView<UIButton> {

        let button = UIButton()
        button.setTitle(title, for: .normal)
        button.setImage(UIImage(named: imageName), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        //button.titleEdgeInsets = UIEdgeInsets(top: 0, left: -100, bottom: 0, right: 0)
        //button.setBackgroundImage(UIImage(named: imageName), for: .normal)
        //button.contentHorizontalAlignment = .center
        //button.contentMode = .scaleAspectFit
        button.accessibilityLabel = title

        if isSelected {
            button.accessibilityTraits |= UIAccessibilityTraitSelected
        } else {
            button.accessibilityTraits &= ~UIAccessibilityTraitSelected
        }
        button.tag = index
        button.addTarget(self, action: #selector(SelectionStyleBulletinPage.didTapOnStyleButton(_:)), for: .touchUpInside)
        let buttonContainer : ContainerView<UIButton> = ContainerView(button) 
        buttonContainer.layer.cornerRadius = 12
        buttonContainer.layer.borderWidth = 2
        //buttonContainer.layer.masksToBounds = true
        buttonContainer.heightAnchor.constraint(equalToConstant: 100).isActive = true
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 100, width: (screenwidth-56)/2, height: 21))
        titleLabel.text = title
        titleLabel.textAlignment = .center
        titleLabel.font.withSize(12)
        titleLabel.textColor = UIColor.lightGray
       // titleLabel.center = buttonContainer.center
        buttonContainer.addSubview(titleLabel)

        let buttonColor = isSelected ? interfaceFactory.tintColor : .lightGray
        buttonContainer.layer.borderColor = buttonColor.cgColor
       // buttonContainer.contentView.setTitleColor(buttonColor, for: .normal)
        buttonContainer.layer.borderColor = buttonColor.cgColor


        return buttonContainer

    }

    // MARK: - Touch Events

    @IBAction func didTapOnStyleButton(_ sender: UIButton) {
        debugPrint(sender.tag)
        var styleArray = ["", "", "", ""]
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.gender == "male" {
                styleArray = menStyleArray
            } else {
                styleArray = womenStyleArray
            }
        }

        if selectedStyle.contains(styleArray[sender.tag]) {
            if let i = selectedStyle.index(of: styleArray[sender.tag]) {
                selectedStyle.remove(at: i)
                let catButtonColor = UIColor.lightGray
                styleButtonContainer[sender.tag].layer.borderColor = catButtonColor.cgColor
                styleButtonContainer[sender.tag].contentView.setTitleColor(catButtonColor, for: .normal)
                styleButtonContainer[sender.tag].contentView.accessibilityTraits |= UIAccessibilityTraitSelected

                if let label = styleButtonContainer[sender.tag].subviews.last as? UILabel {
                    label.textColor = UIColor.lightGray
                }
            }
        } else {
            selectedStyle.append(styleArray[sender.tag])
            let catButtonColor = interfaceFactory.tintColor
            styleButtonContainer[sender.tag].layer.borderColor = catButtonColor.cgColor
            styleButtonContainer[sender.tag].contentView.setTitleColor(catButtonColor, for: .normal)
            styleButtonContainer[sender.tag].contentView.accessibilityTraits |= UIAccessibilityTraitSelected
            if let label = styleButtonContainer[sender.tag].subviews.last as? UILabel {
                label.textColor = UIColor.hexa("178dcd", alpha: 1.0)
            }
        }
    }


    /// Called when the save button is tapped.
    @objc func saveButtonTapped() {
        if selectedStyle.count != 0 {
            let tagList = selectedStyle.joined(separator: ", ")
            if let currentUser = THUserManager.sharedInstance.currentUserdata() {
                dismissCompletionHander!()
                THUserManager.sharedInstance.performUpdateStylePreferenceTaglist(with: currentUser.userId, tagList: tagList, completion: { (error) in

                })
            } else {

            }
        } else {

        }

    }

}


