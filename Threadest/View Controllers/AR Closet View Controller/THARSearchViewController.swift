//
//  THARSearchViewController.swift
//  Threadest
//
//  Created by mobilestar on 2/28/18.
//  Copyright © 2018 JC. All rights reserved.
//

import UIKit
import Crashlytics

class THARSearchViewController: UIViewController, UITextFieldDelegate {

    weak var delegate: SearchBarDelegate?

    //Outlet
    @IBOutlet weak var btnUsers: DesignableButton!
    @IBOutlet weak var btnProduct: DesignableButton!
    @IBOutlet weak var btnBrand: DesignableButton!
    @IBOutlet weak var searchHeaderTextField: DesignableTextField!
    @IBOutlet weak var filterTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var objTableView: UITableView!
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var viewBottomConstraint: NSLayoutConstraint!
    //Instance Variables
    var pageUser:UInt = 0
    var pageBrand:UInt = 0

    var maxNoOfUserPages:UInt = 0
    var maxNoOfBrandPages:UInt = 0

    var btnsArray:[DesignableButton] = []
    var searchedResuls:NSArray = []
    var searchedUserResuls:NSMutableArray = []
    var postsSocialInteraction = [SocialInteraction]()
    var searchedBrandResuls:NSMutableArray = []
    var searchQuery:THSearchQueryManager?
    var postTagCompletion : PostTagCompletion?
    var selectedCollectionView : UICollectionView?

    enum ScrollDirectionType {
        case ScrollDirectionNone
        case ScrollDirectionRight
        case ScrollDirectionLeft
    }

    var scrollDirection : ScrollDirectionType = .ScrollDirectionNone
    //for search
    var selectedSearchType:String?
    var pageAll:UInt = 0
    var maxNoOfPagesAll:UInt = 0
    var maxNoOfPages:UInt = 0
    var page:UInt = 0

    var fromButton : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(THSearchViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(THSearchViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        self.searchQuery = THSearchQueryManager()

        appendBtnsInArray()
        self.navigationController?.isNavigationBarHidden = true
        self.setupViews()
        self.searchTableView.isHidden = true
        self.searchTableView.tableFooterView = UIView()

        self.searchResults(search: "", searchType: THSearchQueryManager.kStringConstantUser)
        self.searchResults(search: "", searchType: THSearchQueryManager.kStringConstantBrand)
        self.refresh()
    }

    func refresh() {
        THSearchSocialInteractionManager.sharedInstance.page = 1
        THSearchSocialInteractionManager.sharedInstance.didLoadData = true
        THSearchSocialInteractionManager.sharedInstance.pullToRefresh = true
        THSearchSocialInteractionManager.sharedInstance.retrieveSocialInteractionPosts { (error) in
            guard let error = error else {
                //self.objTableView.reloadData()
                
                self.postsSocialInteraction = THSearchSocialInteractionManager.sharedInstance.socialInteractionPosts
                self.objTableView.reloadData()
                
                return
            }
            debugPrint(error)
        }
        
    }
    func loadMoreSocialfeed() {
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        
        if THSearchSocialInteractionManager.sharedInstance.didLoadData {
            THSearchSocialInteractionManager.sharedInstance.didLoadData = false
            THSearchSocialInteractionManager.sharedInstance.retrieveSocialInteractionPosts { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                guard let error = error else {
                    //self.objTableView.reloadData()
                    
                    self.postsSocialInteraction = THSearchSocialInteractionManager.sharedInstance.socialInteractionPosts
                    self.selectedCollectionView?.reloadData()
                    
                    return
                }
                debugPrint(error)
            }
        }
        
    }
    func setupViews() {
        
        let searchIcon = UIImage(named: "icons-search")
        let searchImageView = UIImageView(image: searchIcon)
        searchImageView.contentMode = .center
        searchImageView.frame = CGRect(x: 0, y: 0, width: 50, height: self.searchHeaderTextField.frame.size.height - 10)
        searchHeaderTextField.leftView = searchImageView
        searchHeaderTextField.leftViewMode = .always
        searchHeaderTextField.layer.cornerRadius = searchHeaderTextField.frame.size.height/2
        
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Search(Main) Button tap", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }
        filterTrailingConstraint.constant = -20
        searchHeaderTextField.delegate = self

        objTableView.register(UINib(nibName: THListOfSuggestionUsersCell.className, bundle: nil), forCellReuseIdentifier: THListOfSuggestionUsersCell.className)

        searchTableView.register(UINib(nibName: HTARSearchTableViewCell.className, bundle: nil), forCellReuseIdentifier: HTARSearchTableViewCell.className)
        searchTableView.rowHeight = 80;

        self.searchTableView.keyboardDismissMode = .onDrag
        self.searchQuery = THSearchQueryManager()
        self.selectedSearchType = "All"
        loadMore(searchType: -1)

    }

    @IBAction func didTapOnFilter(_ sender: Any) {

    }

    @IBAction func didTapType(_ sender: Any?) {
        fromButton = true
        let tappedBtn = sender as! DesignableButton
        switch tappedBtn.tag {
        case 0:
            self.selectedSearchType = THSearchQueryManager.kStringConstantUser
            break
        case 1:
            self.selectedSearchType = THSearchQueryManager.kStringConstantBrand
            break
        case 2:
            self.selectedSearchType = THSearchQueryManager.kStringConstantProduct
            break
        default:
            self.selectedSearchType = THSearchQueryManager.kStringConstantAll
            break
        }
        if self.searchHeaderTextField.isFirstResponder {
            self.selectType()
        }else {
            self.searchHeaderTextField.becomeFirstResponder()
        }
    }

    public func selectType () {
        let btnBackgroudColor = UIColor(red: 0/255, green: 138/255, blue: 208/255, alpha: 1.0)
        
        for btn in btnsArray {
            
            btn.setTitleColor(UIColor.black, for: .normal)
            btn.alpha = 0.4;
            btn.backgroundColor = UIColor.white
            btn.titleLabel?.font = UIFont(name: "SFUIText-Regular", size: 13)
            
        }
        
        switch self.selectedSearchType {
        case THSearchQueryManager.kStringConstantUser?:
            
            self.btnUsers.alpha = 1.0;
            self.btnUsers.setTitleColor(UIColor.white, for: .normal)
            self.btnUsers.backgroundColor = btnBackgroudColor
            self.btnUsers.titleLabel?.font = UIFont(name: "SFUIText-Semibold", size: 13)
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Search User Tab", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "From": "NON AR Closet"])
            }
            break
        case THSearchQueryManager.kStringConstantBrand?:
            self.btnBrand.alpha = 1.0;
            self.btnBrand.setTitleColor(UIColor.white, for: .normal)
            self.btnBrand.backgroundColor = btnBackgroudColor
            self.btnBrand.titleLabel?.font = UIFont(name: "SFUIText-Semibold", size: 13)
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Search Brand Tab", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "From": "NON AR Closet"])
            }
            break
        case THSearchQueryManager.kStringConstantProduct?:
            self.btnProduct.alpha = 1.0;
            self.btnProduct.setTitleColor(UIColor.white, for: .normal)
            self.btnProduct.backgroundColor = btnBackgroudColor
            self.btnProduct.titleLabel?.font = UIFont(name: "SFUIText-Semibold", size: 13)
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Search Product Tab", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "From": "NON AR Closet"])
            }
            break
        default:
           
            break
        }
        
        
        if(self.selectedSearchType == THSearchQueryManager.kStringConstantAll) {
            self.searchResultsAll(search: self.searchHeaderTextField.text!)
        }
        else {
            self.searchResults(search: self.searchHeaderTextField.text!, searchType: self.selectedSearchType!)
        }
        
    }
    //MARK: - custom function
    private func appendBtnsInArray() {
        btnsArray = [btnUsers, btnBrand, btnProduct];
    }

    private func searchResultsAll(search:String){
        self.pageAll = 0
        self.searchQuery?.indexQueryUser?.query.page = self.pageAll
        self.searchQuery?.indexQueryProduct?.query.page = self.pageAll
        self.searchQuery?.indexQueryBrand?.query.page = self.pageAll

        self.searchQuery?.queryResultsAll(search: search, userPage: true, productPage: true, brandPage: true) { (searchList, nbPageUser, nbPageProduct, nbPageBrand) in
            self.searchedResuls = []
            self.searchedResuls = searchList as! NSArray

            self.searchTableView.reloadData()
            let pagesArray = [nbPageUser, nbPageProduct, nbPageBrand]
            self.maxNoOfPagesAll = pagesArray.max()!
        }
    }

    private func searchResults(search:String, searchType:String){

        if !searchTableView.isHidden {
            self.page = 0
            self.searchQuery?.query?.page = self.page
            self.searchQuery?.query?.filters = ""
            self.searchQuery?.queryResults(search: search, searchType: searchType) { (searchList, nbPage) in
                self.searchedResuls = []
                self.searchedResuls = searchList as! NSArray

                self.maxNoOfPages = nbPage
                self.searchTableView.reloadData()
            }
            return
        }
        switch searchType {
        case THSearchQueryManager.kStringConstantUser:
            pageUser = 0
            self.searchQuery?.query?.page = self.pageUser
            break
        case THSearchQueryManager.kStringConstantBrand:
            pageBrand = 0
            self.searchQuery?.query?.page = self.pageBrand
            break
        default: break

        }

        self.searchQuery?.query?.filters = ""
        self.searchQuery?.queryResults(search: "", searchType: searchType) { (searchList, nbPage) in

            switch searchType {
            case THSearchQueryManager.kStringConstantUser:
                self.searchedUserResuls = []
                self.maxNoOfUserPages = nbPage
                break
            case THSearchQueryManager.kStringConstantBrand:
                self.searchedBrandResuls = []
                self.maxNoOfBrandPages = nbPage
                break
           
            default: break

            }

            let searchedResuls1 = searchList as! NSArray

            for (index, element) in searchedResuls1.enumerated() {
                print("Item \(index): \(element)")
                if element is SearchUser {
                    self.searchedUserResuls.add(element)
                }else if element is SearchBrand {
                    self.searchedBrandResuls.add(element)
                }
            }

            self.objTableView.reloadData()
        }
    }
    public func loadMore(searchType:Int) {

        if searchType == -1 {
            self.searchTableView.addInfiniteScrolling {
                if(self.selectedSearchType == THSearchQueryManager.kStringConstantAll) {
                    self.pageAll+=1
                    if (self.pageAll  >= self.maxNoOfPagesAll) {
                        self.searchTableView.infiniteScrollingView.stopAnimating();
                        return // All pages already loaded
                    } else {
                        self.fetchMoreResultsAll()
                    }
                } else {
                    self.page+=1
                    if (self.page  >= self.maxNoOfPages) {
                        self.searchTableView.infiniteScrollingView.stopAnimating();
                        return // All pages already loaded
                    } else {
                        self.fetchMoreResults(searchType: searchType)
                    }
                }
            }
            return
        }
        if(searchType == 0) {
            self.pageUser += 1

            if (self.pageUser  >= self.maxNoOfUserPages) {

                return // All pages already loaded
            }
        }else if(searchType == 1) {
            self.pageBrand += 1

            if (self.pageBrand  >= self.maxNoOfBrandPages) {

                return // All pages already loaded
            }
        }
        self.fetchMoreResults(searchType: searchType)

    }

    private func fetchMoreResultsAll() {
        self.searchQuery?.indexQueryUser?.query.page = self.pageAll;
        self.searchQuery?.indexQueryProduct?.query.page = self.pageAll;
        self.searchQuery?.indexQueryBrand?.query.page = self.pageAll;

        self.searchQuery?.queryResultsAll(search: self.searchHeaderTextField.text!, userPage: true, productPage: true, brandPage: true) { (searchList, nbPageUser, nbPageProduct, nbPageBrand) in
            let moreResults:NSArray = searchList as! NSArray
            let finalArray:NSMutableArray = []
            for (index, element) in self.searchedResuls.enumerated() {
                print("Item \(index): \(element)")
                finalArray.add(element)
            }
            for (index, element) in moreResults.enumerated() {
                print("Item \(index): \(element)")
                finalArray.add(element)
            }
            self.searchedResuls = finalArray as NSArray
            let pagesArray = [nbPageUser, nbPageProduct, nbPageBrand]
            self.maxNoOfPagesAll = pagesArray.max()!
            self.searchTableView.reloadData()
            self.searchTableView.infiniteScrollingView.stopAnimating();
        }
    }

    private func fetchMoreResults(searchType:Int) {

        if searchType == -1 {
            self.searchQuery?.query?.page = self.page
            self.searchQuery?.query?.filters = ""
            self.searchQuery?.queryResults(search: self.searchHeaderTextField.text!, searchType: self.selectedSearchType!) { (searchList, nbPage) in
                let moreResults:NSArray = searchList as! NSArray
                let finalArray:NSMutableArray = []
                for (index, element) in self.searchedResuls.enumerated() {
                    print("Item \(index): \(element)")
                    finalArray.add(element)
                }
                for (index, element) in moreResults.enumerated() {
                    print("Item \(index): \(element)")
                    finalArray.add(element)
                }
                self.searchedResuls = finalArray as NSArray
                self.maxNoOfPages = nbPage
                self.searchTableView.reloadData()
                self.searchTableView.infiniteScrollingView.stopAnimating();
            }
            return
        }
        var searchTypeString : String!
        switch searchType {
        case 0:
            self.searchQuery?.query?.page = self.pageUser
            searchTypeString = THSearchQueryManager.kStringConstantUser
            break
        case 1:
            self.searchQuery?.query?.page = self.pageBrand
            searchTypeString = THSearchQueryManager.kStringConstantBrand
            break
        default: break

        }
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        self.searchQuery?.query?.filters = ""
        self.searchQuery?.queryResults(search: "", searchType: searchTypeString) { (searchList, nbPage) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            switch searchType {
            case 0:
                self.maxNoOfUserPages = nbPage
                break
            case 1:
                self.maxNoOfBrandPages = nbPage
                break
            default: break

            }

            let searchedResuls1 = searchList as! NSArray

            for (index, element) in searchedResuls1.enumerated() {
                print("Item \(index): \(element)")
                if element is SearchUser {
                    self.searchedUserResuls.add(element)
                }else if element is SearchBrand {
                    self.searchedBrandResuls.add(element)
                }
            }

            self.selectedCollectionView?.reloadData()

        }
    }
    // MARK: - UITextFieldDelegate Method

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {

        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let search = textFieldText.replacingCharacters(in: range, with: string)
//        self.loadMore(searchType: -1)
        if(self.selectedSearchType == THSearchQueryManager.kStringConstantAll) {
            self.searchResultsAll(search: search)
        }
        else {
            self.searchResults(search: search, searchType: self.selectedSearchType!)
        }
        return true;
    }

    // MARK: - Custom Methods
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {

            self.viewBottomConstraint.constant = keyboardSize.height

        }
        if !fromButton {
            self.selectedSearchType = THSearchQueryManager.kStringConstantAll
        }
        
        self.searchTableView.isHidden = false
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
            self.selectType()
            self.fromButton = false
        }
      
    }

    func keyboardWillHide(notification: NSNotification) {

        self.viewBottomConstraint.constant = 0
        self.fromButton = false
    }
    
    func didTapFollowButton(_ sender: UIButton) {
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }
        let user = self.searchedUserResuls[sender.tag] as! SearchUser
        let id = Int(user.objectID!)!
        if let suggestionCell = objTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? THListOfSuggestionUsersCell {
            
            suggestionCell.collectionView.performBatchUpdates({
                suggestionCell.collectionView.deleteItems(at: [IndexPath(row: sender.tag, section: 0)])
                self.searchedUserResuls.removeObject(at: sender.tag)
            }, completion: { (true) in
                suggestionCell.collectionView.reloadData()
            })
            if self.searchedUserResuls.count == 0 {
                self.searchResults(search: "", searchType: THSearchQueryManager.kStringConstantUser)
            }
        }
        THSocialInteractionPostsManager.sharedInstance.performFollowUser(for: id) { (error) in
        }
    }
    
    func didTapSkipButton(_ sender: UIButton) {
        if let suggestionCell = objTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? THListOfSuggestionUsersCell {
            
            suggestionCell.collectionView.performBatchUpdates({
                suggestionCell.collectionView.deleteItems(at: [IndexPath(row: sender.tag, section: 0)])
                self.searchedUserResuls.removeObject(at: sender.tag)
                
            }, completion: { (true) in
                suggestionCell.collectionView.reloadData()
            })
            if self.searchedUserResuls.count == 0 {
                self.searchResults(search: "", searchType: THSearchQueryManager.kStringConstantUser)
            }
        }
    }
    
}

extension THARSearchViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 4 {
            return 80
        }
        switch indexPath.section {
        case 0:
            return 150
        case 1:
            return 150
        case 2:
            if deviceType == .pad {
                return 350
            } else {
                return 270 * screenscale
            }
        default:
            return 150
        }

        // return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 3 {
            return
        }
        let tappedObj = self.searchedResuls[indexPath.row]

        if tappedObj is SearchProduct {

            THBrandProductManager.sharedInstance.retrieveBrandProfile(with:(tappedObj as! SearchProduct).brand_id!, productId:Int((tappedObj as! SearchProduct).objectID!)!, geofence: false) { (error) in
                let shopshowcontroller = THShopshowViewController(nibName: THShopshowViewController.className, bundle: nil)
                shopshowcontroller.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                self.navigationController?.pushViewController(shopshowcontroller, animated: true)
            }
        }
        else if tappedObj is SearchUser {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let userPostProfileVC = storyBoard.instantiateViewController(withIdentifier: PZProfileTableViewController.className) as! PZProfileTableViewController
            userPostProfileVC.findUser = (tappedObj as! SearchUser)
            userPostProfileVC.isSearch = true
            userPostProfileVC.userId = Int((tappedObj as! SearchUser).objectID!)
             let profileNavVC = UINavigationController(rootViewController: userPostProfileVC)
            self.present(profileNavVC, animated: true, completion: nil)
        }else if tappedObj is SearchBrand {
            let brandProfileVC:THBrandProfileViewController = THBrandProfileViewController()
            brandProfileVC.brand = (tappedObj as! SearchBrand)
            self.navigationController?.pushViewController(brandProfileVC, animated: true)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
extension THARSearchViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {

        if tableView.tag == 4 {
            return 1
        }
        var countofSection = 3 as Int
        if postsSocialInteraction.count == 0 {
            countofSection -= 1
        }

        if self.searchedBrandResuls.count == 0 {
            countofSection -= 1
        }

        if self.searchedUserResuls.count == 0 {
            countofSection -= 1
        }
        return countofSection
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 4 {
            return (self.searchedResuls as AnyObject).count
        }
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: HTARSearchTableViewCell.className, for: indexPath) as! HTARSearchTableViewCell
            let obj = self.searchedResuls[indexPath.row]
            cell.setObject(obj: obj)
            return cell
        }
        let suggestionCell = tableView.dequeueReusableCell(withIdentifier: THListOfSuggestionUsersCell.className, for: indexPath) as! THListOfSuggestionUsersCell
        suggestionCell.collectionView.delegate = self
        suggestionCell.collectionView.dataSource = self
        suggestionCell.collectionView.reloadData()
        suggestionCell.selectionStyle = .none
        suggestionCell.collectionView.tag = indexPath.section
        suggestionCell.suggestionLabelTopConstraint.constant = -22
        suggestionCell.seeAllButton.isHidden = true
        return suggestionCell
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if tableView.tag == 4 {
            return ""
        }
        switch section {
        case 0:
            return "Featured Users"
        case 1:
            return "Featured Brands"
        case 2:
            return "Featured Events/Pop Ups"
        default:
            return ""
        }
    }
}

extension THARSearchViewController: UICollectionViewDelegate {
    // somewhere in the class implementation
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.tag > 2) {
            return
        }
        
        if(scrollView.panGestureRecognizer.translation(in: scrollView.superview).x > 0)
        {
            scrollDirection = .ScrollDirectionLeft
        }
        else if (scrollView.panGestureRecognizer.translation(in: scrollView.superview).x < 0)
        {
            print("down")
            scrollDirection = .ScrollDirectionRight
        }else {
            scrollDirection = .ScrollDirectionNone
        }
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if (scrollView.tag > 2 ) {
            return
        }
        if scrollDirection == .ScrollDirectionRight {
            if scrollView.tag == 2  {
                self.loadMoreSocialfeed()
                selectedCollectionView = scrollView as? UICollectionView
                self.selectedCollectionView?.reloadData()
            }else {
                selectedCollectionView = scrollView as? UICollectionView
                self.loadMore(searchType: scrollView.tag)
            }
            
        }

    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        switch collectionView.tag {
        case 0:

            let user = self.searchedUserResuls[indexPath.item] as! SearchUser
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THProfilePostsManager.sharedInstance.profilePosts = [SocialInteraction]()
            THProfilePostsManager.sharedInstance.page = 1
            THProfilePostsManager.sharedInstance.didLoadData = false
            THProfilePostsManager.sharedInstance.retrieveProfilePosts(with: Int(user.objectID!)!) { (error) in
                guard error != nil else {
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber - 1
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let userPostProfileVC = storyBoard.instantiateViewController(withIdentifier: PZProfileTableViewController.className) as! PZProfileTableViewController
                    userPostProfileVC.profile = THProfilePostsManager.sharedInstance.profile
                    userPostProfileVC.isSearch = true
                    userPostProfileVC.userId = Int(user.objectID!)!
                    let profileNavVC = UINavigationController(rootViewController: userPostProfileVC)
                    self.present(profileNavVC, animated: true, completion: nil)
                    return
                }
            }
        case 1:

            let obj = self.searchedBrandResuls[indexPath.item] as! SearchBrand

            let brandProfileVC:THBrandProfileViewController = THBrandProfileViewController()
            brandProfileVC.brand = obj
            self.navigationController?.pushViewController(brandProfileVC, animated: true)

            return

        default:
            return
        }



    }
}

extension THARSearchViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView.tag {
        case 0:
            return self.searchedUserResuls.count
        case 1:
            return self.searchedBrandResuls.count
        case 2:
            return self.postsSocialInteraction.count
        default:
            return 0
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView.tag {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: THSuggestionUserCell.cellIdentifier(), for: indexPath) as! THSuggestionUserCell
            let obj = self.searchedUserResuls[indexPath.item] as! SearchUser
            
            if (obj.thumbnail_image_url != nil) {
                cell.imageView.sd_setImage(with: URL(string: obj.thumbnail_image_url), placeholderImage: nil, options: .retryFailed, completed: nil)

            }

            cell.nameLabel.text = obj.username
            cell.followButton.tag = indexPath.item
            cell.followButton.backgroundColor = UIColor(red: 0/255, green: 138/255, blue: 208/255, alpha: 1.0)
            cell.closeButton.tag = indexPath.item
            cell.closeButton.addTarget(self, action: #selector(THARSearchViewController.didTapSkipButton(_:)), for: .touchUpInside)
            cell.followButton.addTarget(self, action: #selector(THARSearchViewController.didTapFollowButton(_:)), for: UIControlEvents.touchUpInside)

            return cell
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: THARSearchBrandCollectionViewCell.cellIdentifier(), for: indexPath) as! THARSearchBrandCollectionViewCell

            let obj = self.searchedBrandResuls[indexPath.item] as! SearchBrand

            if (obj.brand_logo_image_url != nil) {
                cell.imgBrand.sd_setImage(with: URL(string: obj.brand_logo_image_url!), placeholderImage: nil, options: .retryFailed, completed: nil)

            }

            cell.lblBrandName.text = obj.name
            cell.imgBrand.contentMode = .scaleAspectFit
            cell.imgBrand.layer.cornerRadius = 45
            cell.imgBrand.layer.masksToBounds = true
            cell.imgBrand.layer.borderWidth = 2
            cell.imgBrand.layer.borderColor = UIColor.lightGray.cgColor

            return cell

        case 2:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: THARSearchSocialfeedCollectionViewCell.cellIdentifier(), for: indexPath) as! THARSearchSocialfeedCollectionViewCell
            cell.imgSocialfeed.contentMode = .scaleAspectFill
            let socialInteraction = postsSocialInteraction[indexPath.item]
            
            cell.circularProgressView.isHidden = false
            cell.imgSocialfeed.sd_setImage(with: URL(string: socialInteraction.largeImageUrl), placeholderImage: nil, options: .retryFailed, progress: { (block1, block2, imageurl) in
                let progress = Double(block1)/Double(block2)
                DispatchQueue.main.async {
                    cell.circularProgressView.setProgress(progress, animated: true)
                }
            }) { (image, error, imageCacheType, imageUrl) in
                if image != nil {
                    cell.circularProgressView.isHidden = true
                }
            }
          
            return cell

        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: THSuggestionUserCell.cellIdentifier(), for: indexPath) as! THSuggestionUserCell
            return cell
        }


    }
}

extension THARSearchViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView.tag {
        case 0:
            return CGSize(width: 130, height: 150)
        case 1:
            let obj = self.searchedBrandResuls[indexPath.item] as! SearchBrand
            var calculatedWidth = obj.name?.width(withConstrainedHeight: 30, font: UIFont (name: "SFUIText-Semibold", size: 14)!)
            if (Double(calculatedWidth!) < 90.0) {
                calculatedWidth = 90;
            }
            return CGSize(width: calculatedWidth!, height: 110)
        case 2:
            if deviceType == .pad {
                return CGSize(width: 350, height: 350)
            } else {
                return CGSize(width: 270 * screenscale, height: 270 * screenscale)
            }
        default:
            return CGSize(width: 130, height: 150)
        }

    }
}

