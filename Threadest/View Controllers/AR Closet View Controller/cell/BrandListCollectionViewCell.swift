//
//  BrandListCollectionViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 05/07/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class BrandListCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var brandImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if screenheight <= 568 {
            brandImageView.layer.cornerRadius = 20
        } else {
            brandImageView.layer.cornerRadius = 25
        }
        
        brandImageView.layer.masksToBounds = true
        brandImageView.layer.borderWidth = 1
        brandImageView.layer.borderColor = UIColor.lightGray.cgColor
        // Initialization code
    }

}
