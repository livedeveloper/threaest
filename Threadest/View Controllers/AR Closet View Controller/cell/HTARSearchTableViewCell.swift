//
//  HTARSearchTableViewCell.swift
//  Threadest
//
//  Created by Maximiliano on 3/6/18.
//  Copyright © 2018 JC. All rights reserved.
//

import UIKit

class HTARSearchTableViewCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    public func setTitleMode(titleMode:Bool) {
        if titleMode {
            self.lblName.isHidden = false
            self.lblDescription.isHidden = true
            self.lblTitle.isHidden = true
        }else {
            self.lblName.isHidden = true
            self.lblDescription.isHidden = false
            self.lblTitle.isHidden = false
        }
    }
    public func setObject(obj:Any) {
        if obj is SearchUser {
            self.setUserObj(user: obj as! SearchUser)
        } else if obj is SearchProduct {
            self.setProductObj(product: obj as! SearchProduct)
        } else if obj is SearchBrand {
            self.setBrandObj(brand: obj as! SearchBrand)
        }
    }
    
    private func setUserObj(user:SearchUser) {
        self.setTitleMode(titleMode: true)
        self.lblName.text = user.username
        if(user.thumbnail_image_url != nil) {
            self.imgProfile.sd_setImage(with: NSURL(string:user.thumbnail_image_url) as URL?)
        }
        
        imgProfile.layer.cornerRadius = imgProfile.frame.width/2
        imgProfile.layer.masksToBounds = true
        imgProfile.layer.borderWidth = 2
        imgProfile.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    private func setProductObj(product:SearchProduct) {
        self.setTitleMode(titleMode: false)
        self.lblTitle.text = product.title
        if product.current_price != nil {
            self.lblDescription.text = String(format: "$%.2f", product.current_price!)
        } else {
            self.lblDescription.text = ""
        }
        
        if(product.default_thumbnail_image_url != nil) {
            imgProfile.sd_setImage(with: NSURL(string:product.default_thumbnail_image_url) as URL?)
        }
        imgProfile.layer.cornerRadius = 0
        imgProfile.layer.masksToBounds = true
        imgProfile.layer.borderWidth = 0
        imgProfile.layer.borderColor = UIColor.clear.cgColor
    }
    
    private func setBrandObj(brand:SearchBrand) {
        self.setTitleMode(titleMode: false)
        self.lblTitle.text = brand.name
        self.lblDescription.text = String(format: "%d Products", brand.products_count!)
        if let brandLogo = brand.brand_logo_image_url {
            self.imgProfile.sd_setImage(with: URL(string: brandLogo), placeholderImage: nil, options: .retryFailed, completed: nil)
        }
        imgProfile.layer.cornerRadius = imgProfile.frame.width/2
        imgProfile.layer.masksToBounds = true
        imgProfile.layer.borderWidth = 2
        imgProfile.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
