//
//  THRecomondationProductCollectionViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 05/07/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import CircleProgressView
import PassKit
import IoniconsSwift

class THRecomondationProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var shopImage: UIImageView!
    @IBOutlet weak var availableSizeLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var commentLabel: UILabel!
    
    @IBOutlet weak var circularView: CircleProgressView!
    
    @IBOutlet weak var brandNameLabel: UILabel!
    @IBOutlet weak var brandLogoImageView: UIImageView!
    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet weak var applePayButton: UIButton!
    @IBOutlet weak var selectSizeButton: UIButton!
    
    @IBOutlet weak var brandImageButton: UIButton!
    
    @IBOutlet weak var rethreadButton: UIButton!
    @IBOutlet weak var rethreadButtonTop: UIButton!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet var payButtonTrailingToCell: NSLayoutConstraint!
    
    @IBOutlet weak var giveawayButton: DesignableButton!
    
    
    var isFromRecommendation = false
    
    var applepaymentbutton = PKPaymentButton()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 8
        self.layer.masksToBounds = true
        
        brandLogoImageView.layer.borderColor = UIColor.lightGray.cgColor
        brandLogoImageView.layer.borderWidth = 1
        brandLogoImageView.layer.cornerRadius = brandLogoImageView.frame.width / 2.0
        brandLogoImageView.layer.masksToBounds = true
        
        likeButton.setImage(Ionicons.androidFavorite.image(30, color: .lightGray), for: .normal)
        likeButton.setImage(Ionicons.androidFavorite.image(30, color: .custColor(r: 233, g: 0, b: 55, a: 1)), for: .selected)
        
        priceLabel.layer.cornerRadius = 15
        priceLabel.layer.masksToBounds = true
        
        availableSizeLabel.layer.cornerRadius = 15
        availableSizeLabel.layer.masksToBounds = true
        let sizeOfCard: CGSize!
        if deviceType == .pad {
            sizeOfCard = CGSize(width: 350, height: 380)
        } else {
            sizeOfCard = CGSize(width: 270 * screenscale, height: 385 * screenscale)
        }
        let paymentNetworks = [PKPaymentNetwork.discover, .masterCard, .amex, .visa]
        if PKPassLibrary.isPassLibraryAvailable() {
            if !PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetworks) {
                applepaymentbutton = PKPaymentButton(paymentButtonType: .setUp, paymentButtonStyle: .black)
                applepaymentbutton.frame.size.width = (sizeOfCard.width-30)/3
                applepaymentbutton.frame.size.height = 32
                applePayButton.addSubview(applepaymentbutton)
            } else {
                applepaymentbutton = PKPaymentButton(paymentButtonType: .buy, paymentButtonStyle: .black)
                applepaymentbutton.frame.size.width = (sizeOfCard.width-30)/3
                applepaymentbutton.frame.size.height = 32
                applePayButton.addSubview(applepaymentbutton)
            }
        }
    }
    
    func configure(withClosetCategory closetCategory: ClosetCategory) {
        if closetCategory.soldOut {
            priceLabel.attributedText = NSAttributedString()
            priceLabel.text = String(format: " $%.2f    ", closetCategory.defaultPrice)
            availableSizeLabel.isHidden = false
        } else {
            if closetCategory.salePrice == 0.0 {
                priceLabel.attributedText = NSAttributedString()
                priceLabel.text = String(format: " $%.2f    ", closetCategory.defaultPrice)
                availableSizeLabel.isHidden = true
            } else {
                availableSizeLabel.isHidden = false
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(format: " $%.2f    ", closetCategory.defaultPrice))
                attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
                priceLabel.text = ""
                priceLabel.attributedText = attributeString
            }
        }
        
        descriptionLabel.text = closetCategory.title
        likeLabel.isHidden = closetCategory.votesCount > 0 ? false:true
        likeLabel.text = "\(closetCategory.votesCount)"
        commentLabel.isHidden = closetCategory.commentsCount > 0 ? false: true
        commentLabel.text = "\(closetCategory.commentsCount)"
        likeButton.isSelected = closetCategory.likedByCurrentUser ? true: false
        
        brandNameLabel.text = closetCategory.brandName
        brandLogoImageView.sd_setImage(with: URL(string: closetCategory.brandLogoImageUrl), placeholderImage: nil, options: .retryFailed) { (image, error, cache, url) in
            
        }
        
       /* availableSizeLabel.isHidden = true
        if let currentUser = THUserManager.sharedInstance.currentUserdata(){
            if let _ = closetCategory.productVariations.index(where: { $0.size == currentUser.shirtSize }) {
                availableSizeLabel.isHidden = false
            } }
        if closetCategory.soldOut {
            availableSizeLabel.isHidden = false
        }*/
        
        /*availableSizeLabel.text = closetCategory.soldOut ? " sold out    ":" avail. in your size!  "
        availableSizeLabel.backgroundColor = closetCategory.soldOut ? UIColor.trasperantRed: UIColor.trasperantBlack
        if let _ = closetCategory.productVariations.index(where: {$0.quantityAvailable >= 1}) {
        } else {
            availableSizeLabel.isHidden = false
            availableSizeLabel.backgroundColor = UIColor.trasperantRed
            availableSizeLabel.text = " sold out    "
        }*/
        let price = closetCategory.salePrice == 0.0 ? closetCategory.defaultPrice : closetCategory.salePrice
        availableSizeLabel.text = closetCategory.soldOut ? " sold out    ":String(format: " $%.2f    ", price)
        availableSizeLabel.backgroundColor = closetCategory.soldOut ? UIColor.trasperantRed: UIColor.trasperantblue
        
        if let _ = closetCategory.productVariations.index(where: {$0.quantityAvailable >= 1}) {
        } else {
            availableSizeLabel.isHidden = false
            availableSizeLabel.backgroundColor = UIColor.trasperantRed
            availableSizeLabel.text = " sold out    "
            priceLabel.attributedText = NSAttributedString()
            priceLabel.text = String(format: " $%.2f    ", closetCategory.defaultPrice)
        }
        
        circularView.isHidden = false
        
        shopImage.sd_setImage(with: URL(string: closetCategory.defaultImageUrl), placeholderImage: nil, options: .retryFailed, progress: { (block1, block2, imageurl) in
            let progress = Double(block1)/Double(block2)
            DispatchQueue.main.async {
                self.circularView.setProgress(progress, animated: true)
            }
        }) { (image, error, imageCacheType, imageUrl) in
            if image != nil {
                self.circularView.isHidden = true
            }else {
                print("image not found")
            }
        }
    }
}
