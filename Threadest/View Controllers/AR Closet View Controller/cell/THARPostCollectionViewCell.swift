//
//  THARPostCollectionViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 22/07/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import CircleProgressView

class THARPostCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var socialPostImage: UIImageView!
    @IBOutlet weak var commentCountLabel: UILabel!
    @IBOutlet weak var upvoteCountLabel: UILabel!
    @IBOutlet weak var rethreadCountLabel: UILabel!
    
    @IBOutlet weak var likeButton: DesignableButton!
    @IBOutlet weak var likeCountButton: UIButton!
    
    @IBOutlet weak var commentButton: DesignableButton!
    
    @IBOutlet weak var rethreadButton: DesignableButton!
    
    @IBOutlet weak var rethreadTopButton: UIButton!
    
    @IBOutlet weak var circularView: CircleProgressView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        profileImage.layer.cornerRadius = profileImage.frame.size.width/2
        profileImage.layer.masksToBounds = true
        
        self.layer.cornerRadius = 8
        self.layer.masksToBounds = true
    }

    func configure(withSocialInteraction socialInteraction: SocialInteraction) {
        circularView.isHidden = false
        socialPostImage.sd_setImage(with: URL(string: socialInteraction.mediumImageUrl), placeholderImage: nil, options: .retryFailed, progress: { (block1, block2, imageurl) in
            let progress = Double(block1)/Double(block2)
            DispatchQueue.main.async {
                self.circularView.setProgress(progress, animated: true)
            }
        }) { (image, error, imageCacheType, imageUrl) in
            if image != nil {
                self.circularView.isHidden = true
            }else {
                print("image not found")
            }
        }
        profileImage.sd_setImage(with: URL(string: socialInteraction.ownerImageUrl), placeholderImage: nil, options: .retryFailed)
        usernameLabel.text = socialInteraction.ownerUsername
        commentCountLabel.isHidden = socialInteraction.commentsCount > 0 ? false:true
        commentCountLabel.text = "\(socialInteraction.commentsCount)"
        upvoteCountLabel.isHidden = socialInteraction.upvotesCount > 0 ? false:true
        upvoteCountLabel.text = "\(socialInteraction.upvotesCount)"
        rethreadCountLabel.isHidden = socialInteraction.rethreadCount > 0 ? false: true
        rethreadCountLabel.text = "\(socialInteraction.rethreadCount)"
        likeButton.isSelected = socialInteraction.likedByCurrentUser ? true: false
        timeLabel.text = GeneralMethods.sharedInstance.getTimeAgoStringFrom(dateString: socialInteraction.createdAt)
    }
    
    func configure(withSearchedProduct searchProduct: SearchProduct) {
        circularView.isHidden = false
        socialPostImage.sd_setImage(with: URL(string: searchProduct.default_medium_image_url), placeholderImage: nil, options: .retryFailed, progress: { (block1, block2, imageurl) in
            let progress = Double(block1)/Double(block2)
            DispatchQueue.main.async {
                self.circularView.setProgress(progress, animated: true)
            }
        }) { (image, error, imageCacheType, imageUrl) in
            if image != nil {
                self.circularView.isHidden = true
            }else {
                print("image not found")
            }
        }
//        profileImage.sd_setImage(with: URL(string: socialInteraction.ownerImageUrl), placeholderImage: nil, options: .retryFailed)
//        usernameLabel.text = socialInteraction.ownerUsername
//        commentCountLabel.isHidden = socialInteraction.commentsCount > 0 ? false:true
//        commentCountLabel.text = "\(socialInteraction.commentsCount)"
//        upvoteCountLabel.isHidden = socialInteraction.upvotesCount > 0 ? false:true
//        upvoteCountLabel.text = "\(socialInteraction.upvotesCount)"
//        rethreadCountLabel.isHidden = socialInteraction.rethreadCount > 0 ? false: true
//        rethreadCountLabel.text = "\(socialInteraction.rethreadCount)"
//        likeButton.isSelected = socialInteraction.likedByCurrentUser ? true: false
//        timeLabel.text = GeneralMethods.sharedInstance.getTimeAgoStringFrom(dateString: socialInteraction.createdAt)
    }
}
