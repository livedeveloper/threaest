//
//  THCategoriedListingCollectionViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 05/07/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THCategoriedListingCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var categoriedLabel: UILabel!
    @IBOutlet weak var cagegoryWidthConstant: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        categoriedLabel.layer.cornerRadius = 17.5
        categoriedLabel.layer.masksToBounds = true
    }
    
    func configure(categoryName: String) {
        categoriedLabel.text = categoryName
        var stringWidth:CGSize!
        if screenheight <= 568 {
            categoriedLabel.font = UIFont.AppFontRegular(11)
            stringWidth = categoryName.size(attributes: [NSFontAttributeName: UIFont.AppFontRegular(11)])
        } else {
            categoriedLabel.font = UIFont.AppFontRegular(14)
            stringWidth = categoryName.size(attributes: [NSFontAttributeName: UIFont.AppFontRegular(14)])
        }
        cagegoryWidthConstant.constant = stringWidth.width + 15
    }
}
