//
//  THARSearchSocialfeedCollectionViewCell.swift
//  Threadest
//
//  Created by Maximiliano on 3/8/18.
//  Copyright © 2018 JC. All rights reserved.
//

import UIKit
import CircleProgressView

class THARSearchSocialfeedCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgSocialfeed: UIImageView!
    @IBOutlet weak var circularProgressView: CircleProgressView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
