//
//  THARSearchBrandCollectionViewCell.swift
//  Threadest
//
//  Created by Maximiliano on 3/8/18.
//  Copyright © 2018 JC. All rights reserved.
//

import UIKit

class THARSearchBrandCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgBrand: UIImageView!
    
    @IBOutlet weak var lblBrandName: UILabel!
    
    func configure(withBrand brand: Brand) {
        // TODO: Add placeholder
        imgBrand.sd_setImage(with: URL(string: brand.logoURL), placeholderImage: nil, options: .retryFailed)
        lblBrandName.text = brand.name
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
