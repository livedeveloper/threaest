//
//  THNONARCategoryCollectionViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 10/11/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import CircleProgressView

class THNONARCategoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var circleView: CircleProgressView!
    
    @IBOutlet weak var categoryLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        categoryImageView.layer.cornerRadius = 8
        categoryImageView.layer.masksToBounds = true
        // Initialization code
        let sizeOfCard: CGSize!
        if deviceType == .pad {
            sizeOfCard = CGSize(width: 350, height: 380)
        } else {
            sizeOfCard = CGSize(width: 270 * screenscale, height: 385 * screenscale)
        }
    }
    
    func configure(withCategory category: ShopCategory) {
        // TODO: Add placeholder
        circleView.isHidden = false
        categoryImageView.sd_setImage(with: URL(string: category.imageURL), placeholderImage: nil, options: .retryFailed, progress: { (block1, block2, imageurl) in
            let progress = Double(block1)/Double(block2)
            DispatchQueue.main.async {
                self.circleView.setProgress(progress, animated: true)
            }
        }) { (image, error, imageCacheType, imageUrl) in
            if image != nil {
                self.circleView.isHidden = true
                //for tag view
            }else {
                //image not found
            }
        }
        categoryLabel.text = category.title
    }

}
