//
//  THVideoPreviewViewController.swift
//  Threadest
//
//  Created by Jaydeep on 24/09/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import AVKit
import IoniconsSwift
import Photos
import Lottie


class THVideoPreviewViewController: UIViewController {

    @IBOutlet weak var videoView: UIView!
    var videoURL:URL!
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var closeButton: UIButton!
    
    @IBOutlet weak var objActivityIndicator: UIActivityIndicatorView!
    
    
    var isShowBulletingVideo = false
    var player: AVPlayer!
    
    open var completion : (()->())?
    override func viewDidLoad() {
        
        super.viewDidLoad()
        saveButton.setImage(Ionicons.iosDownload.image(30, color: .white), for: .normal)
        DispatchQueue.main.async {
            self.objActivityIndicator.startAnimating()
            self.player = AVPlayer(url: self.videoURL)
           // self.player.addObserver(self, forKeyPath: "status", options: NSKeyValueObservingOptions(rawValue: 0), context: nil)
            
            self.player.currentItem?.addObserver(self, forKeyPath: "playbackBufferEmpty", options: .new, context: nil)
            self.player.currentItem?.addObserver(self, forKeyPath: "playbackLikelyToKeepUp", options: .new, context: nil)
            self.player.currentItem?.addObserver(self, forKeyPath: "playbackBufferFull", options: .new, context: nil)
            
            
            let playerLayer = AVPlayerLayer(player: self.player)
            playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
            playerLayer.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
            self.videoView.layer.addSublayer(playerLayer)
            self.player.seek(to: kCMTimeZero)
            self.player.actionAtItemEnd = .none
            self.player.play()
            NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.player.currentItem, queue: nil, using: { (_) in
                DispatchQueue.main.async {
                    let t1 = CMTimeMake(0, 1);
                    self.player.seek(to: t1)
                    self.player.play()
                } })
        }
       
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
       /* if keyPath == "status" {
            if self.player.status == .readyToPlay {
                objActivityIndicator.stopAnimating()
            }
            if self.player.status == .failed {
                objActivityIndicator.stopAnimating()
            }
            if self.player.status == .unknown {
                objActivityIndicator.startAnimating()
            }
        }*/
        
        if object is AVPlayerItem {
            switch keyPath {
            case "playbackBufferEmpty"?:
                objActivityIndicator.startAnimating()
                
            case "playbackLikelyToKeepUp"?:
                objActivityIndicator.stopAnimating()
                
            case "playbackBufferFull"?:
                objActivityIndicator.stopAnimating()
            default:
                break
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
       // self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        closeButton.isHidden = false
        
    }

    @IBAction func didTapOnCloseButton(_ sender: Any) {
        self.dismiss(animated: true) {
            if self.isShowBulletingVideo {
                self.completion!()
            }
        }
    }
    
    @IBAction func didTapOnDownloadButton(_ sender: Any) {
        if PHPhotoLibrary.authorizationStatus() == .authorized {
            self.save(video: videoURL)
        } else {
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == .authorized {
                    self.save(video: self.videoURL)
                }
            })
        }
    }
    
    private func save(video url: URL) {
        PHPhotoLibrary.shared().performChanges({
            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url)
        }) { (done, error) in
            DispatchQueue.main.async {
                let animationDoneView = LOTAnimationView(name: "done_button")
                animationDoneView.animationProgress = 100
                animationDoneView.backgroundColor = UIColor.clear
                animationDoneView.frame = CGRect(x: 0, y: 0, width: 400, height: 400)
                animationDoneView.center = self.view.center
                animationDoneView.contentMode = .scaleAspectFill
                animationDoneView.animationSpeed = 0.8
                self.view.addSubview(animationDoneView)
                animationDoneView.loopAnimation = false
                animationDoneView.play { (sucess) in
                    animationDoneView.pause()
                    animationDoneView.removeFromSuperview()
                }
            }
            print(done, error as Any)
        }
    }
}
