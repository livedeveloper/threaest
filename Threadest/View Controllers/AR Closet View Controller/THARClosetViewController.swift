//
//  THARClosetViewController.swift
//  Threadest
//
//  Created by Jaydeep on 05/07/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import Foundation
import ARKit
import SceneKit
import MapKit
import SDWebImage
import IoniconsSwift
import PassKit
import Crashlytics
import AudioToolbox
import Photos
import Lottie
import FBSDKCoreKit
import SwiftyJSON
import StoreKit
import ZIPFoundation
import Alamofire


@available(iOS 11.0, *)
class THARClosetViewController: UIViewController, UIGestureRecognizerDelegate {
    //outlet tool tip view
    @IBOutlet var toolTipClosetView: UIView!
    @IBOutlet weak var toolTipClosetLabel: UILabel!
    var popOverToolTipCloset = Popover()
    var isShowSetupPopup = true
    var showElementTimer:Timer!
    var isProductFound = false
    var isSingleRow = false
    var isFromRetailMapView:Bool = false

    open var toggleARNONARPopup: (() -> ())?

    @IBOutlet weak var popupLabel: UILabel!
    @IBOutlet weak var popupHeaderView: UIView!
    @IBOutlet weak var usernameBackground: UILabel!
    @IBOutlet weak var cartCountLabel: UILabel!
    @IBOutlet weak var startLottieAnimationView: UIView!

    @IBOutlet weak var qrButton: UIButton!

    @IBOutlet weak var startButton: DesignableButton!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var topBrandConstraint: NSLayoutConstraint!
    @IBOutlet weak var hiddenClosetButtonFortoolTip: UIButton!
    @IBOutlet weak var dropDownArraow: UIButton!
    @IBOutlet weak var sceneView: UIView!
    @IBOutlet weak var categoriesCollectionView: UICollectionView!
    @IBOutlet weak var brandListingCollectionView: UICollectionView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var infoButton: UIButton!

    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var searchBarView: UIView!
    @IBOutlet weak var searchHeaderTextField: DesignableTextField!

    @IBOutlet weak var paginationButton: UIButton!

    weak var delegate: SearchBarDelegate?

    @IBOutlet weak var ARToggleButton: UIButton!
    @IBOutlet weak var THDropDownButton: UIButton!
    @IBOutlet weak var closetTabButton: UIButton!
    @IBOutlet weak var arrowLeftCategoryButton: UIButton!
    @IBOutlet weak var arrowRightCategoryButton: UIButton!

    @IBOutlet weak var gridSingleButton: UIButton!
    @IBOutlet weak var RecordingLabel: UILabel!

    @IBOutlet weak var visibleSearchTextField: DesignableTextField!

    @IBOutlet weak var userProfileButton: UIButton!
    @IBOutlet weak var cartView: UIView!

    //variable for drop down
    var dropdownShowing = false
    var dropdownViewController: THClosetDropdownMenu!

    var recordingTimer: Timer!

    //AR kit Location variable
    let sceneLocationView = SceneLocationView()
    var locationNodes = [LocationNode]()
    var locationBrandNodes = [LocationNode]()

    let locationManager = LocationManager()
    var isCameraPermissionPopup = true
    var isLoadCategoriesEndpoint = true

    //AR variable
    var selectedPosterType = ""

    //variable
    var categoriesListingArray = ["Recommendations", "Pop Ups","Categories", "Favorites", "Purchased", "Your Posts"]
    var selectedCategoriesIndex = 0
    var selectedBrandIndex = -1
    var selectedBrandID = -1
    var categoryType = "main"
    var cartCategoryString = ""
    var categoryID = -1
    var selectedLocationId = 0

    //ARKit pagination index
    var currentIndex = 0
    var nextIndex = 0

    var screenCenter: CGPoint?

    //AR Recording
    private var writer: SCNVideoWriter? = nil

    var virtualObject: LocationNode? // TODO: Remove this and create an array with virtualobject

    // MARK: - Debug Visualizations
    let DEFAULT_DISTANCE_CAMERA_TO_OBJECTS = Float(10)

    var recentVirtualObjectDistances = [CGFloat]()

   /* var showDebugVisuals: Bool = true {
        didSet {
            messagePanel.isHidden = !showDebugVisuals
            //planes.values.forEach { $0.showDebugVisualization(showDebugVisuals) }

            if showDebugVisuals {
                sceneLocationView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
            } else {
                sceneLocationView.debugOptions = []
            }
        }
    }*/

    func setupDebug() {
        messagePanel.layer.cornerRadius = 18.0
        messagePanel.clipsToBounds = true
    }

    func setupScene() {
        sceneLocationView.setUp(viewController: self, session: sceneLocationView.session)
        DispatchQueue.main.async {
            self.screenCenter = self.sceneLocationView.bounds.mid
        }
    }

    // MARK: - UI Elements and Actions

    @IBOutlet weak var messagePanel: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    var textManager: TextManager!

    func setupUIControls() {
        textManager = TextManager(viewController: self)
        messageLabel.text = ""
    }


    // MARK: - Planes
    var currentPlaneAnchor:ARPlaneAnchor?
    var isFocusSqurePosition: Bool = false

    func addPlane(node: SCNNode, anchor: ARPlaneAnchor) {
        if isSingleRow {
            if sceneLocationView.sceneNode?.childNodes.first == nil {
                currentPlaneAnchor = anchor
                isFocusSqurePosition = false
                /*for locationnode in self.locationNodes {
                    self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                }
                z = 2
                isPagination = true
                self.currentIndex = 0
                self.nextIndex = 0
                switch selectedPosterType {
                    case "Post":
                        self.SetupUserPosterInRow(isFirstLoad: true)
                        break
                    case "Categories":
                        if categoryType == "main" {
                            self.SetupCategoriesPosterInRow(isFirstLoad: true)
                        } else {
                            self.SetupProductPosterInRow(isFirstLoad: true)
                        }
                        break
                    default:
                        self.SetupProductPosterInRow(isFirstLoad: true)
                        break
                }*/
            }
        }
    }

    func restartPlaneDetection() {
        // configure session
        if let worldSessionConfig = sceneLocationView.configuration {
            worldSessionConfig.planeDetection = .horizontal
            sceneLocationView.session.run(worldSessionConfig, options: [.resetTracking, .removeExistingAnchors])
        }
    }

    // MARK: - Focus Square
    var focusSquare: FocusSquare?
    var dragOnInfinitePlanesEnabled = true
    var currentGesture: Gesture?

    func setupFocusSquare() {
        focusSquare?.isHidden = true
        focusSquare?.removeFromParentNode()
        focusSquare = FocusSquare()
        sceneLocationView.focusSquare?.addChildNode(focusSquare!)

        //textManager.scheduleMessage("FIND A SURFACE WITH YOUR CAMERA. 📲", inSeconds: 20.0, messageType: .focusSquare)
    }

    func updateFocusSquare() {
        if !isSingleRow {
            return
        }
        guard let screenCenter = screenCenter else { return }
        if sceneLocationView.sceneNode?.childNodes.first != nil {
            if sceneLocationView.debugOptions != [] {
                UIView.animate(withDuration: 0.6, animations: {
                    self.startButton.alpha = 0
                    self.startLottieAnimationView.alpha = 0
                }, completion: { (complete) in
                    self.startButton.isHidden = true
                    self.startLottieAnimationView.isHidden = true
                })
                sceneLocationView.debugOptions = []
            }
            focusSquare?.hide()
        } else {
            if sceneLocationView.debugOptions == [] {
                UIView.animate(withDuration: 0.6, animations: {
                    self.startButton.alpha = 1
                    self.startLottieAnimationView.alpha = 1
                }, completion: { (complete) in
                    self.startButton.isHidden = false
                    self.startLottieAnimationView.isHidden = false
                })
                sceneLocationView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
            }
            focusSquare?.unhide()
            let (worldPos, planeAnchor, _) = worldPositionFromScreenPosition(screenCenter, objectPos: focusSquare?.position)
            if let worldPos = worldPos {
                focusSquare?.update(for: worldPos, planeAnchor: planeAnchor, camera: sceneLocationView.session.currentFrame?.camera)
                textManager.cancelScheduledMessage(forType: .focusSquare)
            }
        }
    }

    //PAGINATION VARIABLE
    var z: Float = 2
    var isPagination: Bool = true

    //Map view
    let mapView = MKMapView()
    let closeMapButton = UIButton(frame: CGRect(x: 10, y: 20, width: 44, height: 44))
    var userAnnotation: MKPointAnnotation?
    var locationEstimateAnnotation: MKPointAnnotation?
    var brandLocationAnnotationArray = [markerAnnotation]()

    ///Whether to show a map view
    var showMapView: Bool = true

    var centerMapOnUserLocation: Bool = true

    //the bulletin manager.
    var bulletinManager: BulletinManager!

    //lottie animation
    let animationView = LOTAnimationView(name: "star")

    var updateUserLocationBasedOnBrandTimer: Timer?


    override func viewDidLoad() {
        super.viewDidLoad()

        gridSingleButton.isSelected = true
        self.startButton.isHidden = true
        self.startLottieAnimationView.isHidden = true
        let gridBlue = Ionicons.grid.image(44, color: UIColor.hex("#178dcd", alpha: 1))
        let listBlue = Ionicons.navicon.image(44, color: UIColor.hex("#178dcd", alpha: 1))
        self.gridSingleButton.setBackgroundImage(gridBlue, for: .normal)
         self.gridSingleButton.setBackgroundImage(listBlue, for: .selected)
        RecordingLabel.alpha = 0
        addButton.setImage(Ionicons.plusCircled.image(25, color: .black), for: .normal)
        infoButton.setImage(Ionicons.informationCircled.image(25, color: UIColor.white), for: .normal)
        userProfileButton.setImage(Ionicons.iosPerson.image(30, color: .white), for: .normal)
        locationManager.regionDelegate = self
        showTimerElement()

        self.setDoneOnKeyboard()
        sceneLocationView.orientToTrueNorth = false
        sceneLocationView.locationEstimateMethod = .mostRelevantEstimate
        sceneLocationView.locationDelegate = self

        sceneView.addSubview(sceneLocationView)

        initUI()
        categoriesCollectionView.register(UINib(nibName: THCategoriedListingCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: THCategoriedListingCollectionViewCell.className)
        brandListingCollectionView.register(UINib(nibName: BrandListCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: BrandListCollectionViewCell.className)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(THARClosetViewController.didTapScreen(_:)))
        self.sceneView.addGestureRecognizer(tapGesture)

        NotificationCenter.default.removeObserver(self, name: Notification.Name(postNotificationType.notificationOrderSucessfully.rawValue), object: nil)

        NotificationCenter.default.removeObserver(self, name: Notification.Name(postNotificationType.notificationNearBy.rawValue), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(THARClosetViewController.ordersucessfull), name: Notification.Name(postNotificationType.notificationOrderSucessfully.rawValue), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(THARClosetViewController.getNearByNotificationCalled), name: Notification.Name(postNotificationType.notificationNearBy.rawValue), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(THARClosetViewController.appGoesBackgroundTimeStopRecording), name: .UIApplicationDidEnterBackground, object: nil)

        NotificationCenter.default.removeObserver(self, name: Notification.Name(postNotificationType.notificationFollow.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(THNonARClosetViewController.notificationPostFollow(notification:)), name: Notification.Name(postNotificationType.notificationFollow.rawValue), object: nil)

        NotificationCenter.default.removeObserver(self, name: Notification.Name(postNotificationType.notificationProduct.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(THARClosetViewController.notificationProduct(notification:)), name: Notification.Name(postNotificationType.notificationProduct.rawValue), object: nil)

        setupUIControls()
        setupScene()
        setupDebug()
        sceneLocationView.debugOptions = []
        let _ = Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { (timesr) in
            self.setupFocusSquare()
        }
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self

        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(THARClosetViewController.handleSWIPEGesture(gesture:)))
        swipeRight.direction = .right
        self.sceneLocationView.addGestureRecognizer(swipeRight)

        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(THARClosetViewController.handleSWIPEGesture(gesture:)))
        swipeLeft.direction = .left
        self.sceneLocationView.addGestureRecognizer(swipeLeft)
        setupUserTrackingButtonAndScaleView()
        if isFromRetailMapView {
             cameraAllowPermissionWithoutPopup()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                THUserManager.sharedInstance.retailLocation.first { (retail) -> Bool in
                    if retail.id == self.selectedLocationId {
                        let eventName = retail.title == "" ? "Popup Shop":retail.title
                        self.popupLabel.text = eventName
                        return true
                    }
                    return false
                }
                let welcomRetailPopup = THWelcomeRetailLocationVC(nibName: THWelcomeRetailLocationVC.className, bundle: nil)
                welcomRetailPopup.locationId = self.selectedLocationId
                self.addChildViewController(welcomRetailPopup)
                welcomRetailPopup.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
                self.view.addSubview(welcomRetailPopup.view)
                welcomRetailPopup.didMove(toParentViewController: self)
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        UIApplication.shared.statusBarStyle = .lightContent
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        let usercurrent = THUserManager.sharedInstance.currentUserdata()

        if isFromRetailMapView {
            DispatchQueue.main.async {
                self.sceneLocationView.run()
            }
        } else {
            if usercurrent?.gender == "" {
                bullitenItemOfWelcomePopup()
            } else if userdefault.object(forKey: userDefaultKey.coachMarkBrandDefault.rawValue) == nil {
                    self.didTapOnAddBrand(self)
            } else if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) == .authorized {
                showStartButtonAnimation()
                coachMarkClosetOverlay()
                sceneLocationView.run()
                if isLoadCategoriesEndpoint {
                    isLoadCategoriesEndpoint = false
                    loadEndPointForRecommendations(isShowLoader: true)
                }
            } else if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) == .denied {
                self.bulletinManager = BulletinManager(rootItem: self.bullitenItemOfCameraDeniedPermission())
                self.bulletinManager.backgroundViewStyle = .blurred(style: .light, isDark: false)
                self.bulletinManager.prepare()
                self.bulletinManager.presentBulletin(above: self.tabBarController ?? self)
            } else {
                self.cameraAllowPermissionWithoutPopup()
            }
        }


        callBrandsToFollowServices()
        brandListingCollectionView.reloadData()
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                self.usernameLabel.text = "Your Closet"
            } else {
                self.usernameLabel.text = "\(usercurrent.username)'s Closet"
            }

            if let cartCount = UserDefaults.standard.value(forKey: "cartCount") {
                if (cartCount as! Int) == 0 {
                    self.cartCountLabel.isHidden = true
                } else {
                    self.cartCountLabel.isHidden = false
                    self.cartCountLabel.text = "\(cartCount as! Int)"
                }
            } else {
                self.cartCountLabel.isHidden = true
                if userdefault.object(forKey: "cartCount") == nil {
                    userdefault.set(usercurrent.cartCount, forKey: "cartCount")
                    userdefault.synchronize()
                }
            }
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        UIApplication.shared.isIdleTimerDisabled = true
        if isShowSetupPopup {
            isShowSetupPopup = false
           // handlePopup()
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       appGoesBackgroundTimeStopRecording()
        //if !isFromRetailMapView {
        DispatchQueue.main.async {
            self.sceneLocationView.pause()
        }
        //}
        let imageCache = SDImageCache.shared()
        imageCache.clearMemory()
    }

    override func didReceiveMemoryWarning() {
        let imageCache = SDImageCache.shared()
        imageCache.clearMemory()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        sceneLocationView.frame = CGRect(
            x: sceneView.frame.origin.x,
            y: sceneView.frame.origin.y,
            width: self.sceneView.frame.size.width,
            height: self.sceneView.frame.size.height)

        mapView.frame = CGRect(
            x: 0,
            y: 0,
            width: self.view.frame.size.width,
            height: self.view.frame.size.height)
    }

    func setDoneOnKeyboard() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(THARClosetViewController.dismissKeyboard))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        self.searchHeaderTextField.inputAccessoryView = keyboardToolbar
    }

    override func dismissKeyboard() {
        view.endEditing(true)
    }

    func initUI() {
        usernameBackground.layer.cornerRadius = 15
        usernameBackground.layer.masksToBounds = true
        cartCountLabel.layer.cornerRadius = cartCountLabel.frame.width / 2.0
        cartCountLabel.layer.masksToBounds = true
        closeMapButton.setImage(UIImage(named: "close"), for: .normal)
        closeMapButton.backgroundColor = UIColor.hex("#178dcd", alpha: 1)
        closeMapButton.layer.cornerRadius = 22
        closeMapButton.layer.masksToBounds = true
        closeMapButton.addTarget(self, action: #selector(THARClosetViewController.hideMapView(_:)), for: .touchUpInside)
        mapView.addSubview(closeMapButton)
        if screenheight <= 568 {
            refreshButton.titleLabel?.font = UIFont.AppFontRegular(11)
        } else {
            refreshButton.titleLabel?.font = UIFont.AppFontRegular(12)
        }
        paginationButton.isHidden = isSingleRow ? true: false
    }

    func handleSWIPEGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if isSingleRow {
            return
        }
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            let btn = UIButton()
            btn.tag = 1
            self.didTapOnPaginationButton(btn)
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.left {
            let btn = UIButton()
            btn.tag = 0
            self.didTapOnPaginationButton(btn)
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.up {
            print("Swipe Up")
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.down {
            print("Swipe Down")
        }
    }

    func showStartButtonAnimation() {
        DispatchQueue.main.async {
            self.animationView.animationProgress = 100
            self.animationView.backgroundColor = UIColor.clear
            self.animationView.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
            self.animationView.contentMode = .scaleAspectFill
            self.animationView.animationSpeed = 0.5
            self.startLottieAnimationView.addSubview(self.animationView)
            self.animationView.loopAnimation = false
            self.animationView.play { (sucess) in
                self.animationView.pause()
            }
        }
    }

    func bullitenItemOfWelcomePopup() {
        self.bulletinManager = BulletinManager(rootItem: GeneralMethods.sharedInstance.bulletinWelcometoThreadest(maleCompletion: {
            GeneralMethods.sharedInstance.updateMaleFemaleValue(gender: "male")
            self.bulletinManager.push(item: self.bullitenStyleSelectionPopup(gender: "male"))
            //Add funnel of Gender
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Gender selection in welcome popup", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "Gender": "male"])
            }
        }, FemaleCompletion: {
            GeneralMethods.sharedInstance.updateMaleFemaleValue(gender: "female")
            self.bulletinManager.push(item: self.bullitenStyleSelectionPopup(gender: "female"))
            //Add funnel of Gender
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Gender selection in welcome popup", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "Gender": "female"])
            }
        }))

        self.bulletinManager.backgroundViewStyle = .blurred(style: .light, isDark: false)

        self.bulletinManager.prepare()
        self.bulletinManager.presentBulletin(above: self.tabBarController ?? self)
    }

    func bullitenStyleSelectionPopup(gender: String) -> BulletinItem {
        //Add funnel of style selection
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Style selection Popup", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }
        return SelectionStyleBulletinPage(gender: gender, dismissCompletion: {
            if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) == .denied {
                self.bulletinManager.push(item: self.bullitenItemOfCameraDeniedPermission())
            } else {
                self.bulletinManager.dismissBulletin(animated: true, completion: {
                    if userdefault.object(forKey: userDefaultKey.coachMarkBrandDefault.rawValue) == nil {
                        self.didTapOnAddBrand(self)
                    } else {
                        if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) == .notDetermined {
                            self.cameraAllowPermissionWithoutPopup()
                        } else if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) == .authorized {
                            self.showStartButtonAnimation()
                            self.coachMarkClosetOverlay()
                            DispatchQueue.main.async {
                                self.sceneLocationView.run()
                            }
                            if self.isFromRetailMapView {
                                self.loadEndPointForRetailNearByProducts(isRefresh: true)
                            } else {
                                if self.isLoadCategoriesEndpoint {
                                    self.isLoadCategoriesEndpoint = false
                                    self.loadEndPointForRecommendations(isShowLoader: true)
                                }
                            }
                        }
                    }
                })
            }
        })
    }

    func bullitenItemOfLocationAllowPermission(completion: @escaping () -> Void) {
        self.bulletinManager = BulletinManager(rootItem: GeneralMethods.sharedInstance.bullitenItemOfLocationAllowPermission(yesCompletion: {
            //Add funnel of location selection
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Location Allow Popup", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "status": "yes"])
            }
            self.bulletinManager.dismissBulletin(animated: true, completion: {
                self.locationManager.requestAuthorization(com: {
                    completion()
                })
            })
        }, noCompletion: {
            //Add funnel of location selection
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Location Allow Popup", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "status": "no"])
            }
            self.bulletinManager.dismissBulletin(animated: true, completion: {
                self.loadEndPointForRecommendations(isShowLoader: true)
                self.categoriesCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
                self.collectionView(self.categoriesCollectionView, didSelectItemAt: IndexPath(item: 0, section: 0))
            })
        }))

        self.bulletinManager.backgroundViewStyle = .blurred(style: .light, isDark: false)
        self.bulletinManager.prepare()
        self.bulletinManager.presentBulletin(above: self.tabBarController ?? self)
    }

    func bullitenItemOfLocationDeniedPermission() {
        self.bulletinManager = BulletinManager(rootItem: GeneralMethods.sharedInstance.bullitenItemOfLocationDeniedPermission(openSettingsCompletion: {
            self.bulletinManager.dismissBulletin(animated: true, completion: {
                //Add funnel of location permission in setting
                if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    Answers.logCustomEvent(withName: "Location permission from settings", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
                }
                UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!)
            })
        }, notNowCompletion: {
            self.bulletinManager.dismissBulletin(animated: true, completion: {
                self.loadEndPointForRecommendations(isShowLoader: true)
                self.categoriesCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
                self.collectionView(self.categoriesCollectionView, didSelectItemAt: IndexPath(item: 0, section: 0))
            })
        }))
        self.bulletinManager.backgroundViewStyle = .blurred(style: .light, isDark: false)
        self.bulletinManager.prepare()
        self.bulletinManager.presentBulletin(above: self.tabBarController ?? self)
    }

    func cameraAllowPermissionWithoutPopup() {
        AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (permission) in
            if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) == .authorized {
                //Add funnel of camera permission
                if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    Answers.logCustomEvent(withName: "Camera permission", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "status": "authorized"])
                }
                DispatchQueue.main.async {
                    self.sceneLocationView.run()
                }
                if self.isFromRetailMapView {
                    self.loadEndPointForRetailNearByProducts(isRefresh: true)
                } else {
                    self.showStartButtonAnimation()
                    self.coachMarkClosetOverlay()
                    if self.isLoadCategoriesEndpoint {
                        self.isLoadCategoriesEndpoint = false
                        self.loadEndPointForRecommendations(isShowLoader: true)
                    }
                }
            } else {
                //Add funnel of camera permission
                if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    Answers.logCustomEvent(withName: "Camera permission", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "status": "denied"])
                }
                DispatchQueue.main.async {
                    self.bulletinManager = BulletinManager(rootItem: self.bullitenItemOfCameraDeniedPermission())
                    self.bulletinManager.backgroundViewStyle = .blurred(style: .light, isDark: false)
                    self.bulletinManager.prepare()
                    self.bulletinManager.presentBulletin(above: self.tabBarController ?? self)
                }
            }
        })
    }

    func coachMarkClosetOverlay() {
        if userdefault.object(forKey: userDefaultKey.coachMarkARClosetDefault.rawValue) == nil {
            userdefault.set(true, forKey: userDefaultKey.coachMarkARClosetDefault.rawValue)
            userdefault.synchronize()
            //Add funnel of coachmark overlay
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "CoachMark Overlay for closer", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "view": "AR"])
            }
            DispatchQueue.main.async {
                self.view.endEditing(true)
                let coach = THCoachMarkPageViewController(nibName: THCoachMarkPageViewController.className, bundle: nil)
                coach.isFromCloset = true
                coach.isARView = true
                self.addChildViewController(coach)
                coach.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
                self.view.addSubview(coach.view)
            }
        }
    }

    func bullitenItemOfCameraDeniedPermission() -> BulletinItem {
        return GeneralMethods.sharedInstance.bullitenItemOfCameraDeniedPermission(openSettingCompletion: {
            //Add funnel of camera permission from setting
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Camera permission from settings", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
            }
            self.bulletinManager.dismissBulletin(animated: true, completion: {
                UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!)
            })
        }, NotNowCompletion: {
            self.bulletinManager.dismissBulletin(animated: true, completion: {
                userdefault.set(false, forKey: userDefaultKey.ARONOFF.rawValue)
                userdefault.synchronize()
                appdelegate.swipeNavigationView()
            })
        })
    }

    func setupUserTrackingButtonAndScaleView() {
        mapView.showsUserLocation = true
        let button = MKUserTrackingButton(mapView: mapView)
        button.layer.backgroundColor = UIColor(white: 1, alpha: 0.8).cgColor
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 5
        button.translatesAutoresizingMaskIntoConstraints = false
        mapView.addSubview(button)
        NSLayoutConstraint.activate([button.bottomAnchor.constraint(equalTo: mapView.bottomAnchor, constant: -10),
                                     button.trailingAnchor.constraint(equalTo: mapView.trailingAnchor, constant: -10)])

    }

    func showMapViewMethod() {
        if !showMapView {
            return
        }
        //Add funnel of closet feed
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Show MapView", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }
        showMapView = false
        mapView.delegate = self
        mapView.showsCompass = true
        mapView.showsUserLocation = true
        view.addSubview(mapView)
        mapView.alpha = 0
        mapView.mapType = .standard
        UIView.animate(withDuration: 0.5) {
            self.mapView.alpha = 1
        }

        if updateUserLocationBasedOnBrandTimer != nil {
            updateUserLocationBasedOnBrandTimer?.invalidate()
            updateUserLocationBasedOnBrandTimer = nil
        }

        updateUserLocationBasedOnBrandTimer = Timer.scheduledTimer(timeInterval: 6, target: self, selector: #selector(THARClosetViewController.updateLocationWhenMapViewAppear), userInfo: nil, repeats: true)
    }


    @IBAction func hideMapView(_ sender: Any) {
        showMapView = true
        UIView.animate(withDuration: 0.5, animations: {
            self.mapView.alpha = 0
        }) { (complete) in
            self.mapView.removeFromSuperview()
            self.mapView.delegate = nil
        }

        if updateUserLocationBasedOnBrandTimer != nil {
            updateUserLocationBasedOnBrandTimer?.invalidate()
            updateUserLocationBasedOnBrandTimer = nil
        }
    }

    func drawPolylineOnMap(sorceCoordinates: CLLocationCoordinate2D, destinationCoordinates: CLLocationCoordinate2D, title: String) {

        let sourcePlacemark = MKPlacemark(coordinate: sorceCoordinates, addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: destinationCoordinates, addressDictionary: nil)

        // 4.
        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)

        // 7.
        let directionRequest = MKDirectionsRequest()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinationMapItem
        directionRequest.transportType = .automobile

        // Calculate the direction
        let directions = MKDirections(request: directionRequest)

        // 8.
        directions.calculate {
            (response, error) -> Void in

            guard let response = response else {
                if let error = error {
                    print("Error: \(error)")
                }

                return
            }

            let route = response.routes[0]
            self.mapView.add((route.polyline), level: MKOverlayLevel.aboveRoads)

            let rect = route.polyline.boundingMapRect
            self.mapView.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
        }
    }


    func showClosetToolTip() {
        let options = [
            .type(.up),
            .animationIn(0.3),
            .blackOverlayColor(UIColor.clear),
            .color(UIColor.blueTabbar)
            ] as [PopoverOption]
        self.toolTipClosetView.sizeToFit()

        if userdefault.object(forKey: userDefaultKey.toolTipClosetTabIconDefault.rawValue) == nil {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.toolTipClosetLabel.text = "😎 Tip: Closet gets smarter when you favorite and purchase pieces."
                self.popOverToolTipCloset = Popover(options: options, showHandler: nil, dismissHandler: nil)
                self.popOverToolTipCloset.show(self.toolTipClosetView, fromView: self.closetTabButton)
            }
        }
    }

    func ordersucessfull() {
        for locationnode in self.locationNodes {
            self.sceneLocationView.removeLocationNode(locationNode: locationnode)
        }
        selectedCategoriesIndex = 4
        categoriesCollectionView.reloadData()
        self.categoriesCollectionView.scrollToItem(at: IndexPath(item: selectedCategoriesIndex, section: 0), at: .centeredHorizontally, animated: true)
        self.collectionView(categoriesCollectionView, didSelectItemAt: IndexPath(item: selectedCategoriesIndex, section: 0))
        loadEndPointForOrder(isRefresh: true)
        SKStoreReviewController.requestReview()
    }

    func getNearByNotificationCalled() {
        //self.tabBarController?.selectedIndex = 1
//        appdelegate.verticle.scrollUP(isAnimated: true)
            for viewController in self.childViewControllers {
                viewController.dismiss(animated: true, completion: nil)
            }
        for locationnode in self.locationNodes {
            self.sceneLocationView.removeLocationNode(locationNode: locationnode)
        }
        selectedCategoriesIndex = 2
        categoriesCollectionView.reloadData()
        self.categoriesCollectionView.scrollToItem(at: IndexPath(item: selectedCategoriesIndex, section: 0), at: .centeredHorizontally, animated: true)
        self.collectionView(categoriesCollectionView, didSelectItemAt: IndexPath(item: selectedCategoriesIndex, section: 0))
        self.isProductFound = false
        self.brandLocationAnnotationArray = [markerAnnotation]()
        //self.locationManager.retailStoreLocation(view: self.view)
        self.updateUserBrandLocation()
    }

    fileprivate func showAllView() {
        /*if self.topBrandConstraint.constant == 50 {
            return
        }
        UIView.animate(withDuration: 1, animations: {
            self.topBar.alpha = 1
            self.topBrandConstraint.constant = 50
        })*/
    }

    fileprivate func showTimerElement() {
        /*showElementTimer = Timer.scheduledTimer(withTimeInterval: 10, repeats: false, block: { (timer) in
            UIView.animate(withDuration: 1, animations: {
                self.topBar.alpha = 0
            }, completion: { (sucess) in
                self.topBrandConstraint.constant = 6
            })
        })*/
    }

    func showHideSearchButton(isShow: Bool) {
        if isShow {
            visibleSearchTextField.isHidden = false
            infoButton.isHidden = false
            userProfileButton.isHidden = false
            cartView.isHidden = false
        } else {
            visibleSearchTextField.isHidden = true
            infoButton.isHidden = true
            userProfileButton.isHidden = true
            cartView.isHidden = true
        }
    }

    fileprivate func searchBarHideShow() {
        if searchBarView.isHidden {
            let searchPopup = THSearchResultAllViewController(nibName: THSearchResultAllViewController.className, bundle: nil)
            delegate = searchPopup
            searchPopup.objSearchTextField = searchHeaderTextField
            searchPopup.isFromScreen = "Closet"
            searchHeaderTextField.delegate = searchPopup
            self.addChildViewController(searchPopup)
            searchPopup.view.frame = CGRect(x: 0, y: topBar.frame.origin.y + topBar.frame.size.height, width: screenwidth, height: screenheight-(topBar.frame.origin.y + topBar.frame.size.height))
            self.view.addSubview(searchPopup.view)
            searchBarView.alpha = 0
            searchBarView.isHidden = false
            UIView.animate(withDuration: 0.25, animations: {
                self.searchBarView.alpha = 1
            }, completion: { (completed) in

            })
            showHideSearchButton(isShow: false)

        } else {
            self.searchHeaderTextField.text = ""
            self.delegate?.didTapOnCloseButton(view: self.searchBarView)
            self.view.endEditing(true)
            /*UIView.animate(withDuration: 0.25, animations: {
                self.searchBarView.alpha = 0
            }, completion: { (completed) in
                self.searchBarView.isHidden = true
            })*/
            showHideSearchButton(isShow: false)
        }
    }

    func navigateToProductDetailPage(brandId: Int, productId: Int, geoFence:Bool) {
        //Add funnel of closet feed
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Show Product Detail From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THBrandProductManager.sharedInstance.retrieveBrandProfile(with: brandId, productId: productId, geofence: geoFence) { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            
            guard let threeDAvailable = THBrandProductManager.sharedInstance.brandProduct?.productGalleryImages.contains(where: { (imgGallery) -> Bool in
                if imgGallery.image_type == "3D" {
                    return true
                } else {
                    return false
                }
            }) else {
                return
            }
            
            if threeDAvailable {
                if #available(iOS 11.0, *) {
                    if (ARConfiguration.isSupported) {
                        let ARShopShowController = THARShopShowViewController(nibName: THARShopShowViewController.className, bundle: nil)
                        ARShopShowController.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                        ARShopShowController.isPopup = true
                        let navARShopShowVC = UINavigationController(rootViewController: ARShopShowController)
                        self.present(navARShopShowVC, animated: true, completion: nil)
                        return
                    }
                }
            }
            let shopshowcontroller = THShopshowViewController(nibName: THShopshowViewController.className, bundle: nil)
            shopshowcontroller.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
            shopshowcontroller.isPopup = true
            let navShopShowVC = UINavigationController(rootViewController: shopshowcontroller)
            self.present(navShopShowVC, animated: true, completion: nil)
        }
    }
    @IBAction func didTapOnCaptureButton(_ sender: Any) {
        self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "Captured", body: "Saved to gallery.", completion: { () in
        }), animated: true, completion: nil)
    }

    func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }

    @IBAction func didTapOnDownArrow(_ sender: Any) {
//        appdelegate.verticle.scrollDown(isAnimated: true)
    }


    @IBAction func didTapOnCartButton(_ sender: Any) {
        if let dropdown = dropdownViewController {
            dropdown.removeAnimate()
        }
        let shoppingcartController = THYourCartViewController(nibName: THYourCartViewController.className, bundle: nil)
        self.navigationController?.pushViewController(shoppingcartController, animated: true)
    }

    @IBAction func didTapScreen(_ sender: UITapGestureRecognizer) {
        self.showAllView()
        if showElementTimer != nil {
            showElementTimer.invalidate()
            showElementTimer = nil
        }
        self.showTimerElement()
        let p = sender.location(in: self.sceneLocationView)
        let hitResults = self.sceneLocationView.hitTest(p, options: [:])
        // check that we clicked on at least one object
        if hitResults.count > 0 {
            let result = hitResults[0]
            if let name = result.node.name {
                if name == "brandLocation" {
                    showMapViewMethod()
                    return
                }
                switch selectedPosterType {
                    case "Categories":
                        if categoryType == "main" {
                            if let categoryIndex = THSocialFeedManager.sharedInstance.shopCategories.index(where: {$0.id == Int(name)}) {
                                self.sceneLocationView.removeTapMeNode()
                                let shopCategory = THSocialFeedManager.sharedInstance.shopCategories[categoryIndex]
                                for locationnode in self.locationBrandNodes {
                                    self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                                }
                                for locationnode in self.locationNodes {
                                    self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                                }
                                if self.categoryID == shopCategory.id && self.cartCategoryString == shopCategory.category {
                                     self.loadEndPointForSubCategoriesProduct(isShowLoader: false)
                                } else {
                                    self.cartCategoryString = shopCategory.category
                                    self.categoryID = shopCategory.id
                                    self.loadEndPointForSubCategoriesProduct(isShowLoader: true, isRefresh: true)
                                }
                            }
                        } else {
                            if let categoryIndex = THFeaturedFeedCategoryManager.sharedInstance.shopCategory.index(where: {$0.Id == Int(name)}) {
                                self.sceneLocationView.removeTapMeNode()
                                let closetCategory = THFeaturedFeedCategoryManager.sharedInstance.shopCategory[categoryIndex]
                                self.navigateToProductDetailPage(brandId: closetCategory.brandId, productId: closetCategory.Id, geoFence: false)
                            }
                        }
                        break
                    case "Recommendations":
                        if let recommondationIndex = THProfileClosetManager.sharedInstance.closetRecommendations.index(where: {$0.Id == Int(name)}) {
                            self.sceneLocationView.removeTapMeNode()
                            let closetCategory = THProfileClosetManager.sharedInstance.closetRecommendations[recommondationIndex]
                            self.navigateToProductDetailPage(brandId: closetCategory.brandId, productId: closetCategory.Id, geoFence: false)
                        }
                        break
                    case "Purchased":
                        if let purchasedIndex = THUserOrdersManager.sharedInstance.userShippedProducts.index(where: {$0.productData.Id == Int(name)}) {
                            self.sceneLocationView.removeTapMeNode()
                            let closetCategory = THUserOrdersManager.sharedInstance.userShippedProducts[purchasedIndex].productData
                            self.navigateToProductDetailPage(brandId: closetCategory.brandId, productId: closetCategory.Id, geoFence: false)
                        }
                        break
                    case "Brand":
                        if let brandIndex = THBrandsManager.sharedInstance.products.index(where: {$0.Id == Int(name)}) {
                            self.sceneLocationView.removeTapMeNode()
                            let closetCategory = THBrandsManager.sharedInstance.products[brandIndex]
                            self.navigateToProductDetailPage(brandId: closetCategory.brandId, productId: closetCategory.Id, geoFence: false)
                        }
                        break
                    case "Favorites":
                        if let favoritesIndex = THProfileClosetManager.sharedInstance.closetFavorite.index(where: {$0.Id == Int(name)}) {
                            self.sceneLocationView.removeTapMeNode()
                            let closetCategory = THProfileClosetManager.sharedInstance.closetFavorite[favoritesIndex]
                            self.navigateToProductDetailPage(brandId: closetCategory.brandId, productId: closetCategory.Id, geoFence: false)
                        }
                        break
                    case "NearBy":
                        if let nearByIndex = THRetailBrandManager.sharedInstance.RetailProducts.index(where: {$0.Id == Int(name)}) {
                            self.sceneLocationView.removeTapMeNode()
                            let closetCategory = THRetailBrandManager.sharedInstance.RetailProducts[nearByIndex]
                            self.navigateToProductDetailPage(brandId: closetCategory.brandId, productId: closetCategory.Id, geoFence: true)
                        }
                        break
                    case "Post":
                        if let postIndex = THProfilePostsManager.sharedInstance.currentUserProfilePosts.index(where: {$0.id == Int(name)}) {
                            self.sceneLocationView.removeTapMeNode()
                            let socialInteraction = THProfilePostsManager.sharedInstance.currentUserProfilePosts[postIndex]
                            GeneralMethods.sharedInstance.loadLoading(view: self.view)
                            THProfilePostsManager.sharedInstance.retrieveNotificationPost(with: socialInteraction.id, completion: { (error) in
                                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                                guard error != nil else {
                                    let commentController = THCommentViewController(nibName: THCommentViewController.className, bundle: nil)
                                    commentController.socialInteraction = socialInteraction
                                    commentController.isPopup = true
                                    let navCommentVC = UINavigationController(rootViewController: commentController)
                                    self.present(navCommentVC, animated: true, completion: nil)
                                    return
                                }
                            })
                        }
                        break
                    default:
                        break
                }
            }
        }
    }


    @IBAction func didTapOnStartButton(_ sender: Any) {
        if isSingleRow {
            //Add funnel of start AR
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "start(AR) Button", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
            }
            if sceneLocationView.sceneNode?.childNodes.first == nil {
                for locationnode in self.locationNodes {
                    self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                }
               /* if self.currentPlaneAnchor == nil {
                    return
                }*/
                z = 2
                isPagination = true
                self.currentIndex = 0
                self.nextIndex = 0
                self.currentPlaneAnchor = nil
                self.isFocusSqurePosition = true

                switch selectedPosterType {
                case "Post":
                    self.SetupUserPosterInRow(isFirstLoad: true)
                    break
                case "Categories":
                    if categoryType == "main" {
                        self.SetupCategoriesPosterInRow(isFirstLoad: true)
                    } else {
                        self.SetupProductPosterInRow(isFirstLoad: true)
                    }
                    break
                default:
                    self.SetupProductPosterInRow(isFirstLoad: true)
                    break
                }
            }
        }
    }

    @IBAction func didTapOnSearchBackButon(_ sender: Any) {
        searchBarHideShow()
        if searchBarView.alpha == 0 {
            self.showHideSearchButton(isShow: true)
        }
        self.showAllView()
        showElementTimer = nil
        self.showTimerElement()
    }

    @IBAction func didTapOnSearchButton(_ sender: Any) {
        //Add funnel of search button
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Search(Main) Button tap", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }
        searchBarHideShow()
        if showElementTimer != nil {
            showElementTimer.invalidate()
            showElementTimer = nil
        }

        self.topBar.alpha = 1
        self.topBrandConstraint.constant = 50
    }

    @IBAction func didTapOnFilterButton(_ sender: Any) {
        delegate?.didTapOnFilterButton()
    }



    @IBAction func didTapOnCameraButton(_ sender: Any) {
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }
        //Add funnel of closet feed
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Camera From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }
        let swiftyCameraVC = THSwiftyCameraViewController(nibName: THSwiftyCameraViewController.className, bundle: nil)
        swiftyCameraVC.view.frame.size.width = screenwidth
        swiftyCameraVC.view.frame.size.height = screenheight
        swiftyCameraVC.completionHander = {
            object, isfrom in
            if let thsocial = self.tabBarController?.viewControllers?.first as? THSocialViewController {
                thsocial.productObject = object
                thsocial.isFromCamera = true
//                appdelegate.verticle.scrollDown(isAnimated: false)
            }
        }
        self.present(swiftyCameraVC, animated: true, completion: nil)
        /*let cameraVC = THCameraViewController(nibName: THCameraViewController.className, bundle: nil)
        cameraVC.completionHander = {
             object, isfrom in
            //let navsocial = self.tabBarController?.viewControllers?[0] as! UINavigationController
            let thsocial = appdelegate.verticle.socialVC as! THSocialViewController//navsocial.viewControllers[0] as! THSocialViewController
            thsocial.productObject = object
            thsocial.isFromCamera = true
            appdelegate.verticle.scrollDown()
            //self.tabBarController?.selectedIndex = 0
        }
        self.present(cameraVC, animated: true, completion: nil)*/
    }

    @IBAction func didTapOnProfileButton(_ sender: Any) {
        //Add funnel of closet feed
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Profile From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let profilePostVC = storyBoard.instantiateViewController(withIdentifier: PZProfileTableViewController.className) as! PZProfileTableViewController
        let navProfileVC = UINavigationController(rootViewController: profilePostVC)
        self.present(navProfileVC, animated: true, completion: nil)
    }

    @IBAction func didTapOnThreadestBlack(_ sender: Any) {
        //Add funnel of closet feed
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "MyBrand From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }
        let BrandVC = THBlackBrandViewController(nibName: THBlackBrandViewController.className, bundle: nil)
        BrandVC.isMyBrand = "my"
        BrandVC.blackBrandCompletionHander = {
            self.didTapOnCameraButton(self)
        }
        self.present(BrandVC, animated: true, completion: nil)
    }


    @IBAction func didTapOnAddBrand(_ sender: Any) {
        //Add funnel of closet feed
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "DiscoverBrand From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }
        let BrandVC = THBlackBrandViewController(nibName: THBlackBrandViewController.className, bundle: nil)
        BrandVC.isMyBrand = "discover"
        BrandVC.blackBrandCompletionHander = {
            self.didTapOnCameraButton(self)
        }
        self.present(BrandVC, animated: true, completion: nil)
    }

    @IBAction func didTapOnViewBrand(_ sender: Any) {
        //Add funnel of closet feed
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "MyBrand From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }
        let BrandVC = THBlackBrandViewController(nibName: THBlackBrandViewController.className, bundle: nil)
        BrandVC.isMyBrand = "my"
        BrandVC.blackBrandCompletionHander = {
            self.didTapOnCameraButton(self)
        }
        self.present(BrandVC, animated: true, completion: nil)
    }


    @IBAction func didTapOnRefreshButton(_ sender: Any) {
        for nd in (sceneLocationView.sceneNode?.childNodes)! {
            nd.geometry?.firstMaterial?.diffuse.contents = nil
            for child in nd.childNodes {
                child.geometry?.firstMaterial?.diffuse.contents = nil
                child.removeFromParentNode()
            }
            nd.removeFromParentNode()
        }

        //Add funnel for Refresh button
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Refresh button", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "type":selectedPosterType])
        }

        for locationnode in self.locationNodes {
            self.sceneLocationView.removeLocationNode(locationNode: locationnode)
        }
        if isSingleRow {
             sceneLocationView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
            currentPlaneAnchor = nil
            isFocusSqurePosition = false
            self.restartPlaneDetection()
            setupFocusSquare()
            self.startButton.alpha = 0
            self.startLottieAnimationView.alpha = 0
            self.startButton.isHidden = false
            self.startLottieAnimationView.isHidden = false
            UIView.animate(withDuration: 0.4, animations: {
                self.startButton.alpha = 1
                self.startLottieAnimationView.alpha = 1
            }, completion: { (complete) in

            })
        } else {
            sceneLocationView.debugOptions = []
            sceneLocationView.reset()
            focusSquare?.hide()
            UIView.animate(withDuration: 0.4, animations: {
                self.startButton.alpha = 0
                self.startLottieAnimationView.alpha = 0
            }, completion: { (complete) in
                self.startButton.isHidden = true
                self.startLottieAnimationView.isHidden = true
            })
        }

        switch selectedPosterType {
        case "Categories":
            if categoryType == "main" {
                loadEndPointForCategories(isShowLoader: true, isRefresh: true)
            } else {
                loadEndPointForSubCategoriesProduct(isShowLoader: true, isRefresh: true)
            }
            break
        case "Recommendations":
            loadEndPointForRecommendations(isShowLoader: true, isRefresh: true)
            break
        case "Purchased":
            loadEndPointForOrder(isRefresh: true)
            break
        case "Brand":
            loadEndPointForBrand(brandId: selectedBrandID, isRefresh: true)
            break
        case "Post":
            loadEndPointForYourPosts(isRefresh: true)
            break
        case "Favorites":
            loadEndPointForClosetFavorites(isRefresh: true)
            break
        case "NearBy":
            if self.selectedLocationId == 0 {
                let allAnnotations = self.mapView.annotations
                self.mapView.removeAnnotations(allAnnotations)

                self.isProductFound = false
                brandLocationAnnotationArray = [markerAnnotation]()
                locationManager.retailStoreLocation(view: self.view)
                self.updateUserBrandLocation()
            } else {
                loadEndPointForRetailNearByProducts(isRefresh: true)
            }
            break
        case "Nearby":
            if self.selectedLocationId == 0 {
                let allAnnotations = self.mapView.annotations
                self.mapView.removeAnnotations(allAnnotations)

                self.isProductFound = false
                brandLocationAnnotationArray = [markerAnnotation]()
                locationManager.retailStoreLocation(view: self.view)
                self.updateUserBrandLocation()
            } else {
                loadEndPointForRetailNearByProducts(isRefresh: true)
            }
            break
        default:
            break
        }
    }

    @IBAction func didTapOnClosetButton(_ sender: Any) {
        if userdefault.object(forKey: userDefaultKey.toolTipClosetTabIconDefault.rawValue) == nil {
            userdefault.set("true", forKey: userDefaultKey.toolTipClosetTabIconDefault.rawValue)
            userdefault.synchronize()
            self.popOverToolTipCloset.dismiss()
        }
    }

    @IBAction func didTapOnClosetNextButton(_ sender: Any) {
        if userdefault.object(forKey: userDefaultKey.toolTipClosetTabIconDefault.rawValue) == nil {
            userdefault.set("true", forKey: userDefaultKey.toolTipClosetTabIconDefault.rawValue)
            userdefault.synchronize()
            self.popOverToolTipCloset.dismiss()
        }
    }

    @IBAction func didTapOnPaginationButton(_ sender: UIButton) {
        let isPrevious = sender.tag == 1 ? true:false
        //Add funnel of closet feed
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Pagination Button From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }
        switch selectedPosterType {
        case "Post":
            self.SynchorizePosterPost(isNext: true, isPrevious:isPrevious)
            break
        case "Categories":
            if categoryType == "main" {
                self.SynchorizePosterCategories(isNext: true, isPrevious:isPrevious)
            } else {
                self.SynchorizePoster(isNext: true, isPrevious:isPrevious)
            }
            break
        default:
            self.SynchorizePoster(isNext: true, isPrevious:isPrevious)
            break
        }
    }

    @IBAction func didTapOnDropDownButton(_ sender: UIButton) {
        //Add funnel of closet feed
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "DropDown Menu From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }

        let popController = THPopOverMenuViewController(nibName: THPopOverMenuViewController.className, bundle: nil)

        // set the presentation style
        popController.modalPresentationStyle = UIModalPresentationStyle.popover

        popController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
        popController.popoverPresentationController?.delegate = self
        popController.popoverPresentationController?.sourceView = sender
        popController.popoverPresentationController?.sourceRect = sender.bounds
        popController.preferredContentSize = CGSize(width: THDropDownButton.frame.size.width, height: 308)
        popController.popOverCompletionHander = {
            itemSelected in
            self.dropDownArraow.isSelected = false
            switch itemSelected {
                case "Profile":
                    self.didTapOnProfileButton(self)
                    break
                case "Search":
                    self.didTapOnSearchButton(self)
                    break
                case "Your Brands":
                    self.didTapOnViewBrand(self)
                    break
                case "Add Brands":
                    self.didTapOnAddBrand(self)
                    break
                case "Camera":
                    self.didTapOnCameraButton(self)
                    break
                case "Social":
                    self.tabBarController?.selectedIndex = 0
                    break
                case "Shopping":
                    self.tabBarController?.selectedIndex = 2
                    break
                default:
                    break
            }
        }
        // present the popover
        self.present(popController, animated: true, completion: nil)
    }


    @IBAction func didTapOnNextRightArrowButton(_ sender: Any) {
        let collectionBounds = self.categoriesCollectionView.bounds
        let contentOffset = CGFloat(floor(self.categoriesCollectionView.contentOffset.x + collectionBounds.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffset)
       /* let visibleCells = categoriesCollectionView.visibleCells
        if let firstCell = visibleCells.last {
            if let indexPath = categoriesCollectionView.indexPath(for: firstCell as! THCategoriedListingCollectionViewCell) {
                if indexPath.item + 1 == categoriesListingArray.count {
                    return
                }
                self.categoriesCollectionView.scrollToItem(at: IndexPath(item: indexPath.item + 1, section: 0), at: .right, animated: true)
                //self.collectionView(categoriesCollectionView, didSelectItemAt: IndexPath(item: indexPath.item + 1, section: 0))
            }
        }*/
    }


    @IBAction func didTapOnNextLeftArrowButton(_ sender: Any) {
        let collectionBounds = self.categoriesCollectionView.bounds
        let contentOffset = CGFloat(floor(self.categoriesCollectionView.contentOffset.x - collectionBounds.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffset)
        /*let visibleCells = categoriesCollectionView.visibleCells
        if let firstCell = visibleCells.first {
            if let indexPath = categoriesCollectionView.indexPath(for: firstCell as! THCategoriedListingCollectionViewCell) {
                if indexPath.item - 1 == -1 {
                    return
                }
                self.categoriesCollectionView.scrollToItem(at: IndexPath(item: indexPath.item - 1, section: 0), at: .left, animated: true)
                //self.collectionView(categoriesCollectionView, didSelectItemAt: IndexPath(item: indexPath.item - 1, section: 0))
            }
        }*/
    }

    func moveCollectionToFrame(contentOffset : CGFloat) {

        let frame: CGRect = CGRect(x : contentOffset ,y : self.categoriesCollectionView.contentOffset.y ,width : self.categoriesCollectionView.frame.width,height : self.categoriesCollectionView.frame.height)
        self.categoriesCollectionView.scrollRectToVisible(frame, animated: true)
    }

    @IBAction func didTouchStartRecordVideoButton(_ sender: Any) {
        //recordButton.setImage(UIImage(named: "recordStop"), for: .normal)
        if recordingTimer != nil {
            recordingTimer.invalidate()
            recordingTimer = nil
        }
        recordingTimer = nil
        recordingTimer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: { (timer) in
            self.RecordingLabel.alpha = 0
            UIView.animate(withDuration: 0.4, animations: {
                self.RecordingLabel.alpha = 1
            })
        })

        do {
            writer = try SCNVideoWriter(scene: sceneLocationView.scene)
            writer?.updateFrameHandler = { (image, time) in
            }
            writer?.startWriting()
        } catch let e {
            print(e)
        }
    }


    @IBAction func didTouchStopVideoRecordButton(_ sender: UIButton) {
        recordButton.isSelected = !recordButton.isSelected
        if recordButton.isSelected {
            //Add funnel of closet feed
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Start Record From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
            }

            self.didTouchStartRecordVideoButton(self)
        } else {
            if recordingTimer != nil {
                recordingTimer.invalidate()
                recordingTimer = nil
            }
            self.RecordingLabel.alpha = 0
            //Add funnel of closet feed
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Stop Record From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
            }
            writer?.finishWriting(completionHandler: { [weak self] (url) in
                let videoPreview = THVideoPreviewViewController(nibName: THVideoPreviewViewController.className, bundle: nil)
                videoPreview.videoURL = url
                print("done", url.path)
                self?.present(videoPreview, animated: true, completion: nil)
            })
        }
    }

    @IBAction func didTapOnGridSinglePosterButton(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if gridSingleButton.isSelected {
            paginationButton.isHidden = false
            isSingleRow = false
            sceneLocationView.debugOptions = []
            //Add funnel of closet feed
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Tap on Grid Poster From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
            }
        } else {
            paginationButton.isHidden = true
            isSingleRow = true
            //Add funnel of closet feed
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Tap on Single Poster From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
            }
        }
        self.didTapOnRefreshButton(self)
    }

    @IBAction func didTapOnDropdownMenu(_ sender: Any) {
        if !dropdownShowing {
            if let _ = dropdownViewController {
                return
            }
            var yPos : CGFloat = 0
            if UIDevice().userInterfaceIdiom == .phone {
                switch UIScreen.main.nativeBounds.height {
                case 2436:
                    yPos = 88
                default:
                    yPos = 64
                }
            }
            dropdownViewController = THClosetDropdownMenu(nibName: THClosetDropdownMenu.className, bundle: nil)
            self.addChildViewController(dropdownViewController)
            dropdownViewController.view.frame = CGRect(x: 0, y: yPos, width: screenwidth, height: screenheight-yPos)
            self.view.addSubview(dropdownViewController.view)
            dropdownViewController.isARView = true
            dropdownViewController.showAnimate()
        } else {
            if let dropdown = dropdownViewController {
                dropdown.removeAnimate()
            }
        }

        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "DropDown Menu", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "From": "AR Closet"])
        }
    }

    @IBAction func didTapOnARToggleButton(_ sender: UIButton) {
        //Add funner for toggle AR
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Toggle AR", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "status": "off"])
        }
        userdefault.set(false, forKey: userDefaultKey.ARONOFF.rawValue)
        userdefault.synchronize()
        var arProfileClosetController: UIViewController!

        if (ARConfiguration.isSupported) {
            if userdefault.object(forKey: userDefaultKey.ARONOFF.rawValue) == nil {
                arProfileClosetController = THNonARClosetViewController(nibName: THNonARClosetViewController.className, bundle: nil)
            } else {
                if let ARON = userdefault.object(forKey: userDefaultKey.ARONOFF.rawValue) as? Bool {
                    if ARON {
                        arProfileClosetController = THARClosetViewController(nibName: THARClosetViewController.className, bundle: nil)
                    } else {
                        arProfileClosetController = THNonARClosetViewController(nibName: THNonARClosetViewController.className, bundle: nil)
                    }
                } else {
                    arProfileClosetController = THARClosetViewController(nibName: THARClosetViewController.className, bundle: nil)
                }
            }
        } else {
            arProfileClosetController = THNonARClosetViewController(nibName: THNonARClosetViewController.className, bundle: nil)
        }

        let navClosetVC = UINavigationController(rootViewController: arProfileClosetController)
        navClosetVC.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        navClosetVC.tabBarItem.image = #imageLiteral(resourceName: "ic_tab_closet")
        navClosetVC.tabBarItem.selectedImage = #imageLiteral(resourceName: "ic_tab_closet")
        self.tabBarController?.viewControllers![3] = navClosetVC
        //appdelegate.swipeNavigationView()
    }

    @IBAction func didTapOnARToggleFromPopUp(_ sender: Any) {
        self.dismiss(animated: false) {
            self.toggleARNONARPopup!()
        }
    }


    @IBAction func didTapOnInfoButton(_ sender: Any) {
        userdefault.removeObject(forKey: userDefaultKey.coachMarkARClosetDefault.rawValue)
        userdefault.synchronize()
        coachMarkClosetOverlay()
        //Add funner for Info
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Info Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }
    }


    @IBAction func didTapOnQRbutton(_ sender: Any) {
        let qr = QRViewController()
        let navQR = UINavigationController(rootViewController: qr)
        navQR.setNavigationBarHidden(true, animated: false)
        self.present(navQR, animated: true, completion: nil)
    }


    func popupShops() {
        if let dropdown = dropdownViewController {
            dropdown.removeFromParentViewController()
            dropdown.view.removeFromSuperview()
            dropdownViewController = nil
            dropdownShowing = false
            self.tabBarController?.selectedIndex = 1
        }
    }



    @IBAction func didTaponPopupCloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }


    func appGoesBackgroundTimeStopRecording() {
        if recordButton.isSelected {
            recordButton.isSelected = false
            if recordingTimer != nil {
                recordingTimer.invalidate()
                recordingTimer = nil
            }
            self.RecordingLabel.alpha = 0
            writer?.finishWriting(completionHandler: { (url) in

            })
        }
    }


    private func checkAuthorizationAndPresentActivityController(toShare data: Any, using presenter: UIViewController) {
        switch PHPhotoLibrary.authorizationStatus() {
        case .authorized:
            let activityViewController = UIActivityViewController(activityItems: [data], applicationActivities: nil)
            //activityViewController.excludedActivityTypes = [UIActivityType.addToReadingList, UIActivityType.openInIBooks, UIActivityType.print]
            presenter.present(activityViewController, animated: true, completion: nil)
        case .restricted, .denied:
            let libraryRestrictedAlert = UIAlertController(title: "Photos access denied",
                                                           message: "Please enable Photos access for this application in Settings > Privacy to allow saving screenshots.",
                                                           preferredStyle: UIAlertControllerStyle.alert)
            libraryRestrictedAlert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            presenter.present(libraryRestrictedAlert, animated: true, completion: nil)
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({ (authorizationStatus) in
                if authorizationStatus == .authorized {
                    let activityViewController = UIActivityViewController(activityItems: [data], applicationActivities: nil)
                    //activityViewController.excludedActivityTypes = [UIActivityType.addToReadingList, UIActivityType.openInIBooks, UIActivityType.print]
                    presenter.present(activityViewController, animated: true, completion: nil)
                }
            })
        }
    }

    //MARK: Notification observer
    func notificationPostFollow(notification: NSNotification) {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
        }
        let followNotificationJson = JSON(notification.object!)
//        appdelegate.verticle.scrollUP(isAnimated: false)
        //GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THProfilePostsManager.sharedInstance.retrieveCurrentProfilePosts(with: followNotificationJson["notification_creator_id"].intValue) { (error) in
            guard error != nil else {
                //GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber - 1
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let userPostProfileVC = storyBoard.instantiateViewController(withIdentifier: PZProfileTableViewController.className) as! PZProfileTableViewController
                userPostProfileVC.profile = THProfilePostsManager.sharedInstance.currentUserProfile
                userPostProfileVC.isSearch = true
                userPostProfileVC.userId = followNotificationJson["notification_creator_id"].intValue
                self.navigationController?.pushViewController(userPostProfileVC, animated: true)
                return
            }
        }
    }

    func notificationProduct(notification: NSNotification) {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
        }
        let productNotificationJson = JSON(notification.object!)
        self.navigateToProductDetailPage(brandId: productNotificationJson["brand_id"].intValue, productId: productNotificationJson["product_id"].intValue, geoFence: false)
    
    }

    //MARK: LOAD ENDPOINT FOR AR VIEWS
    func callBrandsToFollowServices() {
        THProfilePostsManager.sharedInstance.profileBrandFollowing = [Brand]()
        THProfilePostsManager.sharedInstance.pageBrand = 1
        THProfilePostsManager.sharedInstance.didLoadDataBrand = false
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            THProfilePostsManager.sharedInstance.retrieveProfileBrandFollowing(with: usercurrent.userId) { (error) in
                guard let error = error else {
                    self.brandListingCollectionView.reloadData()
                    return
                }
                debugPrint(error)
            }
        }
    }


    func loadEndPointForBrand(brandId: Int, isRefresh: Bool = false) {
        selectedPosterType = "Brand"
        if isRefresh {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THBrandsManager.sharedInstance.products = [ClosetCategory]()
            THBrandsManager.sharedInstance.page = 1
            THBrandsManager.sharedInstance.retrieveBrand(for: brandId, isFromCloset: true) { (cartBrand, error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                if(error==nil) {
                    self.currentIndex = 0
                    self.nextIndex = 0
                    if self.isSingleRow {
                        if self.sceneLocationView.sceneNode?.childNodes.first == nil {
                            for locationnode in self.locationNodes {
                                self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                            }
                            self.z = 2
                            self.isPagination = true
                            if self.currentPlaneAnchor == nil && self.isFocusSqurePosition == false {
                                return
                            }
                            self.SetupProductPosterInRow(isFirstLoad: false)
                        }
                    } else {
                        self.SynchorizePoster(isNext: false, isPrevious: false)
                    }
                }
            }
        } else {
            if THBrandsManager.sharedInstance.products.count > 0 {
                self.currentIndex = 0
                self.nextIndex = 0
                if self.isSingleRow {
                    if self.sceneLocationView.sceneNode?.childNodes.first == nil {
                        for locationnode in self.locationNodes {
                            self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                        }
                        self.z = 2
                        self.isPagination = true
                        if self.currentPlaneAnchor == nil && self.isFocusSqurePosition == false {
                            return
                        }
                        self.SetupProductPosterInRow(isFirstLoad: false)
                    }
                } else {
                    self.SynchorizePoster(isNext: false, isPrevious: false)
                }
            } else {
                self.loadEndPointForBrand(brandId: brandId, isRefresh: true)
            }
        }
    }

    func loadEndPointForSubCategoriesProduct(isShowLoader: Bool, isRefresh: Bool = false) {
        if cartCategoryString == "" || categoryID == -1 {
            return
        }
        selectedPosterType = "Categories"
        categoryType = "sub"
        selectedCategoriesIndex = 0
        DispatchQueue.main.async {
            self.categoriesCollectionView.reloadData()
        }
        if isRefresh {
            if isShowLoader {
                GeneralMethods.sharedInstance.loadLoading(view: self.view)
            }
            THFeaturedFeedCategoryManager.sharedInstance.shopCategory =  [ClosetCategory]()
            THFeaturedFeedCategoryManager.sharedInstance.page = 1
            THFeaturedFeedCategoryManager.sharedInstance.didLoadData = false
            THFeaturedFeedCategoryManager.sharedInstance.retrieveFeaturedFeedCategory(with: cartCategoryString, id: categoryID, completion: { (error) in

                if isShowLoader {
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                }
                if error != nil {
                    return
                }
                self.categoriesListingArray[0] = "< Categories"
                self.categoriesCollectionView.reloadItems(at: [IndexPath(row: 0, section: 0)])
                self.currentIndex = 0
                self.nextIndex = 0
                if self.isSingleRow {
                    if self.sceneLocationView.sceneNode?.childNodes.first == nil {
                        for locationnode in self.locationNodes {
                            self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                        }
                        self.z = 2
                        self.isPagination = true
                        if self.currentPlaneAnchor == nil && self.isFocusSqurePosition == false{
                            return
                        }
                        self.SetupProductPosterInRow(isFirstLoad: true)
                    }
                } else {
                    self.SynchorizePoster(isNext: true, isPrevious: false)
                }
            })
        } else {
            if THFeaturedFeedCategoryManager.sharedInstance.shopCategory.count > 0 {
                self.categoriesListingArray[0] = "< Categories"
                self.categoriesCollectionView.reloadItems(at: [IndexPath(row: 0, section: 0)])
                self.currentIndex = 0
                self.nextIndex = 0
                if self.isSingleRow {
                    if self.sceneLocationView.sceneNode?.childNodes.first == nil {
                        for locationnode in self.locationNodes {
                            self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                        }
                        self.z = 2
                        self.isPagination = true
                        if self.currentPlaneAnchor == nil && self.isFocusSqurePosition == false{
                            return
                        }
                        self.SetupProductPosterInRow(isFirstLoad: true)
                    }
                } else {
                    self.SynchorizePoster(isNext: true, isPrevious: false)
                }
            } else {
                self.loadEndPointForSubCategoriesProduct(isShowLoader: true, isRefresh: true)
            }
        }
    }

    func loadEndPointForCategories(isShowLoader: Bool, isRefresh: Bool = false) {
        selectedPosterType = "Categories"
        categoryType = "main"
        cartCategoryString = ""
        categoryID = -1
        selectedCategoriesIndex = 0
        DispatchQueue.main.async {
            self.categoriesCollectionView.reloadData()
        }
        if isRefresh {
            if isShowLoader {
                GeneralMethods.sharedInstance.loadLoading(view: self.view)
            }
            THSocialFeedManager.sharedInstance.shopCategories = [ShopCategory]()
            THSocialFeedManager.sharedInstance.page = 1
            THSocialFeedManager.sharedInstance.didLoadData = false
            THSocialFeedManager.sharedInstance.retrieveNextPosts(completion: { (error) in
                if isShowLoader {
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                }
                if error != nil {
                    return
                }
                self.currentIndex = 0
                self.nextIndex = 0
                if self.isSingleRow {
                    if self.sceneLocationView.sceneNode?.childNodes.first == nil {
                        for locationnode in self.locationNodes {
                            self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                        }
                        self.z = 2
                        self.isPagination = true
                        if self.currentPlaneAnchor == nil && self.isFocusSqurePosition == false{
                            return
                        }
                        self.SetupCategoriesPosterInRow(isFirstLoad: false)
                    }
                } else {
                    self.SynchorizePosterCategories(isNext: false, isPrevious:false)
                }
            })
        } else {
            if THSocialFeedManager.sharedInstance.shopCategories.count > 0 {
                self.currentIndex = 0
                self.nextIndex = 0
                if self.isSingleRow {
                    if self.sceneLocationView.sceneNode?.childNodes.first == nil {
                        for locationnode in self.locationNodes {
                            self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                        }
                        self.z = 2
                        self.isPagination = true
                        if self.currentPlaneAnchor == nil && self.isFocusSqurePosition == false{
                            return
                        }
                        self.SetupCategoriesPosterInRow(isFirstLoad: false)
                    }
                } else {
                    self.SynchorizePosterCategories(isNext: false, isPrevious:false)
                }
            } else {
                self.loadEndPointForCategories(isShowLoader: true, isRefresh: true)
            }
        }
    }

    func loadEndPointForRecommendations(isShowLoader: Bool, isRefresh: Bool = false) {
        selectedPosterType = "Recommendations"
        selectedCategoriesIndex = 1
        selectedBrandIndex = 1
        DispatchQueue.main.async {
            self.categoriesCollectionView.reloadData()
        }
        if isRefresh {
            if isShowLoader {
                GeneralMethods.sharedInstance.loadLoading(view: self.view)
            }
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                THProfileClosetManager.sharedInstance.closetRecommendations = [ClosetCategory]()
                THProfileClosetManager.sharedInstance.page = 1
                THProfileClosetManager.sharedInstance.didLoadData = false
                THProfileClosetManager.sharedInstance.retrieveProfileCloset(with: usercurrent.userId) { (error) in
                    if isShowLoader {
                        GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    }

                    self.currentIndex = 0
                    self.nextIndex = 0
                    if self.isSingleRow {
                        if self.sceneLocationView.sceneNode?.childNodes.first == nil {
                            for locationnode in self.locationNodes {
                                self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                            }
                            self.z = 2
                            self.isPagination = true
                            if self.currentPlaneAnchor == nil && self.isFocusSqurePosition == false{
                                return
                            }
                            self.SetupProductPosterInRow(isFirstLoad: false)
                        }
                    } else {
                        self.SynchorizePoster(isNext: false, isPrevious: false)
                    }
                }
            }
        } else {
            if THProfileClosetManager.sharedInstance.closetRecommendations.count > 0 {
                self.currentIndex = 0
                self.nextIndex = 0
                if self.isSingleRow {
                    if self.sceneLocationView.sceneNode?.childNodes.first == nil {
                        for locationnode in self.locationNodes {
                            self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                        }
                        self.z = 2
                        self.isPagination = true
                        if self.currentPlaneAnchor == nil && self.isFocusSqurePosition == false{
                            return
                        }
                        self.SetupProductPosterInRow(isFirstLoad: false)
                    }
                } else {
                    self.SynchorizePoster(isNext: false, isPrevious: false)
                }
            } else {
                self.loadEndPointForRecommendations(isShowLoader: isShowLoader, isRefresh: true)
            }
        }
    }

    func loadEndPointForOrder(isRefresh: Bool = false) {
        selectedPosterType = "Purchased"
        selectedCategoriesIndex = 4
        categoriesCollectionView.reloadData()
        if isRefresh {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THUserOrdersManager.sharedInstance.userShippedProducts = [PurchasesOrders]()
            THUserOrdersManager.sharedInstance.page = 1
            THUserOrdersManager.sharedInstance.retrieveShippedOrder(with: (THUserManager.sharedInstance.currentUserdata()?.userId)!) { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                guard let error = error else {
                    self.currentIndex = 0
                    self.nextIndex = 0
                    if self.isSingleRow {
                        if self.sceneLocationView.sceneNode?.childNodes.first == nil {
                            for locationnode in self.locationNodes {
                                self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                            }
                            self.z = 2
                            self.isPagination = true
                            if self.currentPlaneAnchor == nil && self.isFocusSqurePosition == false{
                                return
                            }
                            self.SetupProductPosterInRow(isFirstLoad: false)
                        }
                    } else {
                        self.SynchorizePoster(isNext: false, isPrevious: false)
                    }
                    return
                }
                debugPrint(error)
            }
        } else {
            if THUserOrdersManager.sharedInstance.userShippedProducts.count > 0 {
                self.currentIndex = 0
                self.nextIndex = 0
                if self.isSingleRow {
                    if self.sceneLocationView.sceneNode?.childNodes.first == nil {
                        for locationnode in self.locationNodes {
                            self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                        }
                        self.z = 2
                        self.isPagination = true
                        if self.currentPlaneAnchor == nil && self.isFocusSqurePosition == false{
                            return
                        }
                        self.SetupProductPosterInRow(isFirstLoad: false)
                    }
                } else {
                    self.SynchorizePoster(isNext: false, isPrevious: false)
                }
            } else {
                self.loadEndPointForOrder(isRefresh: true)
            }
        }
    }

    func loadEndPointForYourPosts(isRefresh: Bool = false) {
        selectedPosterType = "Post"
        selectedCategoriesIndex = 5
        categoriesCollectionView.reloadData()
        if isRefresh {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THProfilePostsManager.sharedInstance.currentUserProfilePosts = [SocialInteraction]()
            THProfilePostsManager.sharedInstance.currentUserPage = 1
            THProfilePostsManager.sharedInstance.currentUserdidLoadData = false
            THProfilePostsManager.sharedInstance.retrieveCurrentProfilePosts(with: (THUserManager.sharedInstance.currentUserdata()?.userId)!) { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                guard error != nil else {
                    self.currentIndex = 0
                    self.nextIndex = 0
                    if self.isSingleRow {
                        if self.sceneLocationView.sceneNode?.childNodes.first == nil {
                            for locationnode in self.locationNodes {
                                self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                            }
                            self.z = 2
                            self.isPagination = true
                            if self.currentPlaneAnchor == nil && self.isFocusSqurePosition == false {
                                return
                            }
                            self.SetupUserPosterInRow(isFirstLoad: false)
                        }
                    } else {
                        self.SynchorizePosterPost(isNext: false, isPrevious: false)
                    }
                    return
                }
            }
        } else {
            if THProfilePostsManager.sharedInstance.currentUserProfilePosts.count > 0 {
                self.currentIndex = 0
                self.nextIndex = 0
                if self.isSingleRow {
                    if self.sceneLocationView.sceneNode?.childNodes.first == nil {
                        for locationnode in self.locationNodes {
                            self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                        }
                        self.z = 2
                        self.isPagination = true
                        if self.currentPlaneAnchor == nil && self.isFocusSqurePosition == false {
                            return
                        }
                        self.SetupUserPosterInRow(isFirstLoad: false)
                    }
                } else {
                    self.SynchorizePosterPost(isNext: false, isPrevious: false)
                }
            } else {
                self.loadEndPointForYourPosts(isRefresh: true)
            }
        }
    }


    func loadEndPointForClosetFavorites(isRefresh: Bool = false) {
        selectedPosterType = "Favorites"
        selectedCategoriesIndex = 3
        categoriesCollectionView.reloadData()
        if isRefresh {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                THProfileClosetManager.sharedInstance.closetFavorite = [ClosetCategory]()
                THProfileClosetManager.sharedInstance.pageFavorite = 1
                THProfileClosetManager.sharedInstance.didLoadDataFavorite = false
                THProfileClosetManager.sharedInstance.retrieveProfileClosetFavorite(with: usercurrent.userId, completion: { (error) in
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    self.currentIndex = 0
                    self.nextIndex = 0
                    if self.isSingleRow {
                        if self.sceneLocationView.sceneNode?.childNodes.first == nil {
                            for locationnode in self.locationNodes {
                                self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                            }
                            self.z = 2
                            self.isPagination = true
                            if self.currentPlaneAnchor == nil && self.isFocusSqurePosition == false {
                                return
                            }
                            self.SetupProductPosterInRow(isFirstLoad: false)
                        }
                    } else {
                        self.SynchorizePoster(isNext: false, isPrevious: false)
                    }
                })
            }
        } else {
            if THProfileClosetManager.sharedInstance.closetFavorite.count > 0 {
                self.currentIndex = 0
                self.nextIndex = 0
                if self.isSingleRow {
                    if self.sceneLocationView.sceneNode?.childNodes.first == nil {
                        for locationnode in self.locationNodes {
                            self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                        }
                        self.z = 2
                        self.isPagination = true
                        if self.currentPlaneAnchor == nil && self.isFocusSqurePosition == false {
                            return
                        }
                        self.SetupProductPosterInRow(isFirstLoad: false)
                    }
                } else {
                    self.SynchorizePoster(isNext: false, isPrevious: false)
                }
            } else {
                self.loadEndPointForClosetFavorites(isRefresh: true)
            }
        }
    }

   /* func loadEndPointForNearByProducts() {
        selectedPosterType = "NearBy"
        selectedCategoriesIndex = 2
        categoriesCollectionView.reloadData()
        GeneralMethods.sharedInstance.loadLoading(view: self.view)

        THRetailBrandManager.sharedInstance.RetailProducts = [ClosetCategory]()
        THRetailBrandManager.sharedInstance.page = 1
        THRetailBrandManager.sharedInstance.didLoadData = true
        THRetailBrandManager.sharedInstance.retrievingRetailNearByProduct(for: selectedLocationId, completion: { (error) in

            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            guard let error = error else {
                self.currentIndex = 0
                self.nextIndex = 0
                if self.isSingleRow {
                    if self.sceneLocationView.sceneNode?.childNodes.first == nil {
                        for locationnode in self.locationNodes {
                            self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                        }
                        self.z = 2
                        self.isPagination = true
                        if self.currentPlaneAnchor == nil && self.isFocusSqurePosition == false {
                            return
                        }
                        self.SetupProductPosterInRow(isFirstLoad: false)
                    }
                } else {
                    self.SynchorizePoster(isNext: false, isPrevious: false)
                }
                return
            }
            self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "", body: "No products found at this location", completion: {
            }), animated: true, completion: nil)

        })
    }*/

    func loadEndPointForRetailNearByProducts(isRefresh: Bool = false) {

        selectedPosterType = "NearBy"
        if isRefresh {
            if self.isFromRetailMapView {
                DispatchQueue.main.async {
                    self.popupHeaderView.isHidden = false
                    self.addButton.isHidden = true
                    self.brandListingCollectionView.isHidden = true
                    self.gridSingleButton.isHidden = true
                    self.qrButton.isHidden = false
                }

            }
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THRetailBrandManager.sharedInstance.RetailProducts = [ClosetCategory]()
            THRetailBrandManager.sharedInstance.page = 1
            THRetailBrandManager.sharedInstance.retrievingRetailNearByProduct(for: selectedLocationId) { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                if self.isFromRetailMapView {


                }
                //self.isFromRetailMapView = false
                if(error==nil) {
                    self.currentIndex = 0
                    self.nextIndex = 0
                    if self.isSingleRow {
                        if self.sceneLocationView.sceneNode?.childNodes.first == nil {
                            for locationnode in self.locationNodes {
                                self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                            }
                            self.z = 2
                            self.isPagination = true
                            if self.currentPlaneAnchor == nil && self.isFocusSqurePosition == false {
                                return
                            }
                            self.SetupProductPosterInRow(isFirstLoad: false)
                        }
                    } else {
                        self.SynchorizePoster(isNext: false, isPrevious: false)
                    }
                }
            }
        } else {
            if THRetailBrandManager.sharedInstance.RetailProducts.count > 0 {
                self.currentIndex = 0
                self.nextIndex = 0
                if self.isSingleRow {
                    if self.sceneLocationView.sceneNode?.childNodes.first == nil {
                        for locationnode in self.locationNodes {
                            self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                        }
                        self.z = 2
                        self.isPagination = true
                        if self.currentPlaneAnchor == nil && self.isFocusSqurePosition == false {
                            return
                        }
                        self.SetupProductPosterInRow(isFirstLoad: false)
                    }
                } else {
                    self.SynchorizePoster(isNext: false, isPrevious: false)
                }
            } else {
                self.loadEndPointForRetailNearByProducts(isRefresh: true)
            }
        }
    }

    func updateUserBrandLocation() {
        //selectedPosterType = "Nearby"
        if let lat = LocationManager().currentLocation?.coordinate.latitude, let long = LocationManager().currentLocation?.coordinate.longitude {
            GeneralMethods.sharedInstance.loadLoading(view: view)
            THUserManager.sharedInstance.performUpdateUserLocaltion(with: lat, logitude: long, completion: { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                if THUserManager.sharedInstance.retailLocation.count > 0 {

                    self.centerMapOnLocation(location: LocationManager().currentLocation!)


                    for rlocation  in THUserManager.sharedInstance.retailLocation
                    {
                        guard let retailLatitude = CLLocationDegrees(exactly: rlocation.latitude), let retailLongitude = CLLocationDegrees(exactly: rlocation.longitude) else {
                            return
                        }

                        self.addAnnotationMapView(latitude: retailLatitude, logitude: retailLongitude, name: rlocation.brandName, id: "\(rlocation.id)")
                    }

                    self.showMapViewMethod()
                }
                else
                {
                    //show alert No Popups Found
                    let ac = UIAlertController(title: "Message", message: "No Popup Shops found within a 5 mile radius.", preferredStyle: .alert)
                    ac.addAction(UIAlertAction(title: "OK", style: .default))
                    self.present(ac, animated: true)
                }
            })
        }
    }

    let regionRadius: CLLocationDistance = 1000
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }

    func updateLocationWhenMapViewAppear() {
        guard let currentCoordinates = LocationManager().currentLocation?.coordinate else {
            return
        }
        let locationMapPoint = MKMapPointForCoordinate(currentCoordinates)

        for brandan in self.brandLocationAnnotationArray {
            let distance = MKMetersBetweenMapPoints(locationMapPoint, MKMapPointForCoordinate(brandan.coordinate))
            if distance <= 50 {
                if brandan.subtitle == "Reach this location to access." {
                    brandan.subtitle = "You've discovered a Popup Shop! Tap to enter."
                    let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: brandan.id!) as? MKMarkerAnnotationView
                    annotationView?.markerTintColor = UIColor.hex("81c88f", alpha: 1)

                }
            } else {
                if brandan.subtitle == "You've discovered a Popup Shop! Tap to enter." {
                    brandan.subtitle = "Reach this location to access."
                    let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: brandan.id!) as? MKMarkerAnnotationView
                    annotationView?.markerTintColor = UIColor.hex("178dcd", alpha: 1)
                }
            }
        }
    }

    func addAnnotationMapView(latitude: CLLocationDegrees, logitude: CLLocationDegrees, name: String, id: String)  {
        let destinationCoordinates = CLLocationCoordinate2D(latitude: latitude , longitude: logitude)
        let annotation = markerAnnotation()
        annotation.id = id
        annotation.coordinate = destinationCoordinates
        annotation.title = name

        let locationMapPoint = MKMapPointForCoordinate((LocationManager().currentLocation?.coordinate)!)
        let pinMapPoint = MKMapPointForCoordinate(destinationCoordinates)
        let distance = MKMetersBetweenMapPoints(locationMapPoint, pinMapPoint)
        if distance <= 50 {
            annotation.subtitle = "You've discovered a Popup Shop! Tap to enter."
        } else {
            annotation.subtitle = "Reach this location to access."
        }
        mapView.addAnnotation(annotation)
        brandLocationAnnotationArray.append(annotation)
        let circle = MKCircle(center: destinationCoordinates, radius: 50)
        circle.title = name
        mapView.add(circle)
    }


    func SetupProductPosterInRow(isFirstLoad: Bool) {
        if isPagination == false {
            return
        }
        isPagination = false
        var categories = [ClosetCategory]()
        switch selectedPosterType {
        case "Categories":
            categories = THFeaturedFeedCategoryManager.sharedInstance.shopCategory
        case "Recommendations":
            categories = THProfileClosetManager.sharedInstance.closetRecommendations
        case "Brand":
            categories = THBrandsManager.sharedInstance.products
        case "Purchased":
            for category in THUserOrdersManager.sharedInstance.userShippedProducts {
                categories.append(category.productData)
            }
        case "NearBy":
            categories = THRetailBrandManager.sharedInstance.RetailProducts
        case "Favorites":
            categories = THProfileClosetManager.sharedInstance.closetFavorite
        default:
            break
        }
        if categories.count == 0 {
            return
        }
        DispatchQueue.main.async {
            self.paginationButton.isEnabled = false
        }

        self.currentIndex = self.nextIndex
        if currentIndex >= categories.count {
            return
        }
        loadPaginationOfPoster(totalSlot: categories.count/(9*(currentIndex+1)))
        if (nextIndex + 9) < categories.count {
            self.nextIndex = self.nextIndex + 9
        } else {
            self.nextIndex = categories.count
        }
        var isGetProduct = false
       // DispatchQueue.main.async {
            for currentPostion in self.currentIndex..<self.nextIndex {
                isGetProduct = true
                let closetCategory = categories[currentPostion]
                if self.currentPlaneAnchor != nil {
                    var childPostion = -1
                    if self.sceneLocationView.sceneNode?.childNodes.first == nil {
                        childPostion = 0
                    }
                    let annotationNode = LocationAnnotationNode(location: nil, viewRecomodation: THARRecomondationsView.instanceFromNib(closetCategory: closetCategory), name: "\(closetCategory.Id)", imageurl: closetCategory.defaultImageUrl, childPostion: childPostion)
                    annotationNode.isFirstLoad = isFirstLoad
                    annotationNode.isSingle = true
                    annotationNode.isShowTapText = childPostion == 0 ? true:false
                    annotationNode.x = self.currentPlaneAnchor!.center.x
                    annotationNode.y = self.currentPlaneAnchor!.center.y
                    annotationNode.z = self.currentPlaneAnchor!.center.z - self.z
                    self.z = self.z + 2.0
                    annotationNode.name = "\(closetCategory.Id)"
                    self.sceneLocationView.addNode(locationNode: annotationNode)
                    self.locationNodes.append(annotationNode)
                } else if self.focusSquare != nil {
                    var childPostion = -1
                    if self.sceneLocationView.sceneNode?.childNodes.first == nil {
                        childPostion = 0
                    }
                        let annotationNode = LocationAnnotationNode(location: nil, viewRecomodation: THARRecomondationsView.instanceFromNib(closetCategory: closetCategory), name: "\(closetCategory.Id)", imageurl: closetCategory.defaultImageUrl,  childPostion: childPostion)
                        
                        annotationNode.isFirstLoad = isFirstLoad
                        annotationNode.isSingle = true
                        annotationNode.isShowTapText = childPostion == 0 ? true:false
                        annotationNode.x = self.focusSquare!.position.x
                        annotationNode.y = self.focusSquare!.position.y
                        annotationNode.z = self.focusSquare!.position.z - self.z
                        self.z = self.z + 2.0
                        
                        annotationNode.name = "\(closetCategory.Id)"
                        self.sceneLocationView.addNode(locationNode: annotationNode)
                        self.locationNodes.append(annotationNode)
                }
            }
            if isGetProduct {
                self.isPagination = true
            }
      //  }

        if isFirstLoad {
            textManager.showMessage("WALK FORWARD TO SEE MORE🚶‍♀️🚶", autoHide: true)
        }
    }

    func SetupUserPosterInRow(isFirstLoad: Bool) {
        if isPagination == false {
            return
        }
        isPagination = false
        if THProfilePostsManager.sharedInstance.currentUserProfilePosts.count == 0 {
            return
        }
        paginationButton.isEnabled = false
        self.currentIndex = self.nextIndex
        if currentIndex >= THProfilePostsManager.sharedInstance.currentUserProfilePosts.count {
            return
        }
        loadPaginationOfPoster(totalSlot: THProfilePostsManager.sharedInstance.currentUserProfilePosts.count/(9*(currentIndex+1)))
        if (nextIndex + 9) < THProfilePostsManager.sharedInstance.currentUserProfilePosts.count {
            self.nextIndex = self.nextIndex + 9
        } else {
            self.nextIndex = THProfilePostsManager.sharedInstance.currentUserProfilePosts.count
        }
        var isGetUserPoster = false
       // DispatchQueue.main.async {
            for currentPostion in self.currentIndex..<self.nextIndex {
                isGetUserPoster = true
                let postInteractions = THProfilePostsManager.sharedInstance.currentUserProfilePosts[currentPostion]
                if self.currentPlaneAnchor != nil {
                    var childPostion = -1
                    if self.sceneLocationView.sceneNode?.childNodes.first == nil {
                        childPostion = 0
                    }
                    let annotationNode = LocationAnnotationNode(location: nil, viewPost: THARPostView.instanceFromNib(socialInteraction: postInteractions), name: "\(postInteractions.id)", imageurl: postInteractions.mediumImageUrl, childPostion: childPostion)
                    annotationNode.isFirstLoad = isFirstLoad
                    annotationNode.isSingle = true
                    annotationNode.isShowTapText = childPostion == 0 ? true:false
                    annotationNode.x = self.currentPlaneAnchor!.center.x
                    annotationNode.y = self.currentPlaneAnchor!.center.y
                    annotationNode.z = self.currentPlaneAnchor!.center.z - self.z
                    self.z = self.z + 2.0
                    annotationNode.name = "\(postInteractions.id)"
                    self.sceneLocationView.addNode(locationNode: annotationNode)
                    self.locationNodes.append(annotationNode)
                } else if self.focusSquare != nil {
                    var childPostion = -1
                    if self.sceneLocationView.sceneNode?.childNodes.first == nil {
                        childPostion = 0
                    }
                    let annotationNode = LocationAnnotationNode(location: nil, viewPost: THARPostView.instanceFromNib(socialInteraction: postInteractions), name: "\(postInteractions.id)", imageurl: postInteractions.mediumImageUrl, childPostion: childPostion)
                    annotationNode.isFirstLoad = isFirstLoad
                    annotationNode.isSingle = true
                    annotationNode.isShowTapText = childPostion == 0 ? true:false
                    annotationNode.x = self.focusSquare!.position.x
                    annotationNode.y = self.focusSquare!.position.y
                    annotationNode.z = self.focusSquare!.position.z - self.z
                    self.z = self.z + 2.0
                    annotationNode.name = "\(postInteractions.id)"
                    self.sceneLocationView.addNode(locationNode: annotationNode)
                    self.locationNodes.append(annotationNode)
                }
            }

            if isGetUserPoster {
                self.isPagination = true
            }
      //  }

        if isFirstLoad {
            textManager.showMessage("WALK FORWARD TO SEE MORE🚶‍♀️🚶", autoHide: true)
        }
    }

    func SetupCategoriesPosterInRow(isFirstLoad: Bool) {
        if isPagination == false {
            return
        }
        isPagination = false
        if THSocialFeedManager.sharedInstance.shopCategories.count == 0 {
            return
        }
        paginationButton.isEnabled = false
        self.currentIndex = self.nextIndex
        if currentIndex >= THSocialFeedManager.sharedInstance.shopCategories.count {
            return
        }
        loadPaginationOfPoster(totalSlot: THSocialFeedManager.sharedInstance.shopCategories.count/(9*(currentIndex+1)))
        if (nextIndex + 9) < THSocialFeedManager.sharedInstance.shopCategories.count {
            self.nextIndex = self.nextIndex + 9
        } else {
            self.nextIndex = THSocialFeedManager.sharedInstance.shopCategories.count
        }
        var isGetUserPoster = false
       // DispatchQueue.main.async {
            for currentPostion in self.currentIndex..<self.nextIndex {
                isGetUserPoster = true
                let shopCategory = THSocialFeedManager.sharedInstance.shopCategories[currentPostion]
                if self.currentPlaneAnchor != nil {
                    var childPostion = -1
                    if self.sceneLocationView.sceneNode?.childNodes.first == nil {
                        childPostion = 0
                    }
                    let annotationNode = LocationAnnotationNode(location: nil, viewCategories: THARCategoriesView.instanceFromNib(shopCategory: shopCategory), name: "\(shopCategory.id)", imageurl: "\(shopCategory.imageURL)", childPostion: childPostion)
                    annotationNode.isFirstLoad = isFirstLoad
                    annotationNode.isSingle = true
                    annotationNode.isShowTapText = childPostion == 0 ? true:false
                    annotationNode.x = self.currentPlaneAnchor!.center.x
                    annotationNode.y = self.currentPlaneAnchor!.center.y
                    annotationNode.z = self.currentPlaneAnchor!.center.z - self.z
                    self.z = self.z + 2.0
                    self.sceneLocationView.addNode(locationNode: annotationNode)
                    self.locationNodes.append(annotationNode)
                } else if self.focusSquare != nil {
                    var childPostion = -1
                    if self.sceneLocationView.sceneNode?.childNodes.first == nil {
                        childPostion = 0
                    }
                    let annotationNode = LocationAnnotationNode(location: nil, viewCategories: THARCategoriesView.instanceFromNib(shopCategory: shopCategory), name: "\(shopCategory.id)", imageurl: "\(shopCategory.imageURL)", childPostion: childPostion)
                    annotationNode.isFirstLoad = isFirstLoad
                    annotationNode.isSingle = true
                    annotationNode.isShowTapText = childPostion == 0 ? true:false
                    annotationNode.x = self.focusSquare!.position.x
                    annotationNode.y = self.focusSquare!.position.y
                    annotationNode.z = self.focusSquare!.position.z - self.z
                    self.z = self.z + 2.0
                    self.sceneLocationView.addNode(locationNode: annotationNode)
                    self.locationNodes.append(annotationNode)
                }
            }
            if isGetUserPoster {
                self.isPagination = true
            }
        //}
        if isFirstLoad {
            self.textManager.showMessage("WALK FORWARD TO SEE MORE🚶‍♀️🚶", autoHide: true)
        }
    }


    func SynchorizePoster(isNext: Bool, isPrevious: Bool) {
        for locationnode in self.locationNodes {
            self.sceneLocationView.removeLocationNode(locationNode: locationnode)
            locationnode.geometry?.firstMaterial?.diffuse.contents = nil
        }
        self.locationNodes = [LocationNode]()
        var i = 0
        var r = -2.0

        var categories = [ClosetCategory]()
        switch selectedPosterType {
        case "Categories":
            categories = THFeaturedFeedCategoryManager.sharedInstance.shopCategory
        case "Recommendations":
            categories = THProfileClosetManager.sharedInstance.closetRecommendations
        case "Brand":
            categories = THBrandsManager.sharedInstance.products
        case "Purchased":
            for category in THUserOrdersManager.sharedInstance.userShippedProducts {
                categories.append(category.productData)
            }
        case "NearBy":
            categories = THRetailBrandManager.sharedInstance.RetailProducts
        case "Favorites":
            categories = THProfileClosetManager.sharedInstance.closetFavorite
        default:
            break
        }
        DispatchQueue.main.async {
            self.paginationButton.isEnabled = false
        }
        if isPrevious {
            if nextIndex <= 0 {
                return
            }
            self.nextIndex = self.currentIndex
            if nextIndex >= 9 {
                self.currentIndex = self.nextIndex-9
            } else {
                self.currentIndex = 0
            }
        } else {
            self.currentIndex = self.nextIndex
            loadPaginationOfPoster(totalSlot: categories.count/(9*(currentIndex+1)))
            if (nextIndex + 9) < categories.count {
                self.nextIndex = self.nextIndex + 9
            } else {
                self.nextIndex = categories.count
            }
        }

      //  DispatchQueue.main.async {
            var isVibrate = true
            for currentPostion in self.currentIndex..<self.nextIndex {
                let closetCategory = categories[currentPostion]
                let annotationNode = LocationAnnotationNode(location: nil, viewRecomodationGrid: THARRecomondationsView.instanceFromNib(closetCategory: closetCategory), name: "\(closetCategory.Id)", imageurl: closetCategory.defaultImageUrl)
                if i < 3 {
                    r = r + 1.1
                    annotationNode.x = Float(r)
                    annotationNode.y = -2.0 + 2
                    annotationNode.z = -3.0
                    if i == 1 {
                        annotationNode.isShowTapText = true
                    }
                } else if i < 6 {
                    annotationNode.x = Float(r)
                    annotationNode.y = -3.2 + 2
                    annotationNode.z = -3.0
                    r = r - 1.1
                } else {
                    r = r + 1.1
                    annotationNode.x = Float(r)
                    annotationNode.y = -4.5 + 2
                    annotationNode.z = -3.0
                    

                    if i == 9 {
                        return
                    }
                }
                i = i + 1
                annotationNode.name = "\(closetCategory.Id)"
                self.sceneLocationView.addNode(locationNode: annotationNode)
                self.locationNodes.append(annotationNode)
                if !isNext {
                    if isVibrate {
                        isVibrate = false
                        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                    }
                }
            }
      //  }

    }

    func SynchorizePosterPost(isNext: Bool, isPrevious: Bool) {
        for locationnode in self.locationNodes {
            self.sceneLocationView.removeLocationNode(locationNode: locationnode)
        }
        self.locationNodes = [LocationNode]()
        var i = 0
        var r = -2.0

        paginationButton.isEnabled = false
        if isPrevious {
            if nextIndex <= 0 {
                return
            }
            self.nextIndex = self.currentIndex
            if nextIndex >= 9 {
                self.currentIndex = self.nextIndex-9
            } else {
                self.currentIndex = 0
            }
        } else {
            self.currentIndex = self.nextIndex
            loadPaginationOfPoster(totalSlot: THProfilePostsManager.sharedInstance.currentUserProfilePosts.count/(9*(currentIndex+1)))
            if (nextIndex + 9) < THProfilePostsManager.sharedInstance.currentUserProfilePosts.count {
                self.nextIndex = self.nextIndex + 9
            } else {
                self.nextIndex = THProfilePostsManager.sharedInstance.currentUserProfilePosts.count
            }
        }


       // DispatchQueue.main.async {
            var isVibrate = true
            for currentPostion in self.currentIndex..<self.nextIndex {
                let postInteractions = THProfilePostsManager.sharedInstance.currentUserProfilePosts[currentPostion]
                let annotationNode = LocationAnnotationNode(location: nil, viewPostGrid: THARPostView.instanceFromNib(socialInteraction: postInteractions), name: "\(postInteractions.id)", imageurl: postInteractions.mediumImageUrl)
                if i < 3 {
                    r = r + 1.1
                    annotationNode.x = Float(r)
                    annotationNode.y = -4.5 + 2
                    annotationNode.z = -3.0
                } else if i < 6 {
                    annotationNode.x = Float(r)
                    annotationNode.y = -3.2 + 2
                    annotationNode.z = -3.0
                    r = r - 1.1
                } else {
                    r = r + 1.1
                    annotationNode.x = Float(r)
                    annotationNode.y = -2.0 + 2
                    annotationNode.z = -3.0
                    if i == 7 {
                        annotationNode.isShowTapText = true
                    }

                    if i == 9 {
                        return
                    }
                }
                i = i + 1
                annotationNode.name = "\(postInteractions.id)"
                self.sceneLocationView.addNode(locationNode: annotationNode)
                self.locationNodes.append(annotationNode)
                if !isNext {
                    if isVibrate {
                       isVibrate = false
                AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                    }
                }
            }
       // }
    }

    func SynchorizePosterCategories(isNext: Bool, isPrevious: Bool) {
        for locationnode in self.locationNodes {
            self.sceneLocationView.removeLocationNode(locationNode: locationnode)
        }
        self.locationNodes = [LocationNode]()
        var i = 0
        var r = -2.0

        paginationButton.isEnabled = false
        if isPrevious {
            if nextIndex <= 0 {
                return
            }
            self.nextIndex = self.currentIndex
            if nextIndex >= 9 {
                self.currentIndex = self.nextIndex-9
            } else {
                self.currentIndex = 0
            }

        } else {
            self.currentIndex = self.nextIndex
            loadPaginationOfPoster(totalSlot: THSocialFeedManager.sharedInstance.shopCategories.count/(9*(currentIndex+1)))
            if (nextIndex + 9) < THSocialFeedManager.sharedInstance.shopCategories.count {
                self.nextIndex = self.nextIndex + 9
            } else {
                self.nextIndex = THSocialFeedManager.sharedInstance.shopCategories.count
            }
        }


       // DispatchQueue.main.async {
            var isVibrate = true
            for currentPostion in self.currentIndex..<self.nextIndex {
                let shopCategory = THSocialFeedManager.sharedInstance.shopCategories[currentPostion]
                let annotationNode = LocationAnnotationNode(location: nil, viewCategories: THARCategoriesView.instanceFromNib(shopCategory: shopCategory), name: "\(shopCategory.id)", imageurl: "\(shopCategory.imageURL)", childPostion: -1, isSingleView: false)
                if i < 3 {
                    r = r + 1.1
                    annotationNode.x = Float(r)
                    annotationNode.y = -4.5 + 2
                    annotationNode.z = -3.0
                } else if i < 6 {
                    annotationNode.x = Float(r)
                    annotationNode.y = -3.2 + 2
                    annotationNode.z = -3.0
                    r = r - 1.1
                } else {
                    r = r + 1.1
                    annotationNode.x = Float(r)
                    annotationNode.y = -2.0 + 2
                    annotationNode.z = -3.0
                    if i == 7 {
                        annotationNode.isShowTapText = true
                    }

                    if i == 9 {
                        return
                    }
                }
                i = i + 1
                //annotationNode.name = "\(postInteractions.id)"
                self.sceneLocationView.addNode(locationNode: annotationNode)
                self.locationNodes.append(annotationNode)
                if !isNext {
                    if isVibrate {
                        isVibrate = false
                        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                    }
                }
            }
       // }
    }


    func loadPaginationOfPoster(totalSlot: Int) {
        if totalSlot <= 1 {
            switch selectedPosterType {
            case "Categories":
                handlingPaginationButton()
                if categoryType == "main" {
                    THSocialFeedManager.sharedInstance.retrieveNextPosts(completion: { (error) in
                        if error != nil {
                            return
                        }
                        self.handlingPaginationButton()
                    })
                } else {
                    THFeaturedFeedCategoryManager.sharedInstance.retrieveFeaturedFeedCategory(with: cartCategoryString, id: categoryID, completion: { (error) in
                        guard let error = error else {

                            return
                        }
                        self.handlingPaginationButton()
                        debugPrint(error)
                    })
                }

            case "Recommendations":
                handlingPaginationButton()
                if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    THProfileClosetManager.sharedInstance.retrieveProfileCloset(with: usercurrent.userId) { (error) in
                        if error != nil {
                            return
                        }
                        self.handlingPaginationButton()
                    }
                }
                break
            case "Brand":
                handlingPaginationButton()
                THBrandsManager.sharedInstance.retrieveBrand(for: selectedBrandID, isFromCloset: true) { (cartBrand, error) in
                    if(error==nil) {
                        self.handlingPaginationButton()
                    }
                }
                break
            case "Purchased":
                self.paginationButton.isEnabled = true
                THUserOrdersManager.sharedInstance.retrieveShippedOrder(with: (THUserManager.sharedInstance.currentUserdata()?.userId)!) { (error) in
                    if error == nil{

                    }
                }
                break
            case "Favorites":
                handlingPaginationButton()
                THProfileClosetManager.sharedInstance.retrieveProfileClosetFavorite(with: (THUserManager.sharedInstance.currentUserdata()?.userId)!, completion: { (error) in
                    if error == nil {
                        self.handlingPaginationButton()
                    }
                })
                break

            case "NearBy":
                handlingPaginationButton()
            THRetailBrandManager.sharedInstance.retrievingRetailNearByProduct(for: selectedLocationId, completion: { (error) in
                    if error == nil {
                        self.handlingPaginationButton()
                    }
                })

            case "Post":
                handlingPaginationButton()
                THProfilePostsManager.sharedInstance.retrieveCurrentProfilePosts(with: (THUserManager.sharedInstance.currentUserdata()?.userId)!) { (error) in
                    if error == nil {
                        self.handlingPaginationButton()
                    }
                }
                break
            default:
                break
            }
        } else {
            self.paginationButton.isEnabled = true
        }
    }

    func handlingPaginationButton() {
        switch selectedPosterType {
        case "Categories":
            if categoryType == "main" {
                if self.currentIndex+9 < THSocialFeedManager.sharedInstance.shopCategories.count {
                    self.paginationButton.isEnabled = true
                }
            } else {
                if self.currentIndex+9 < THFeaturedFeedCategoryManager.sharedInstance.shopCategory.count {
                    self.paginationButton.isEnabled = true
                }
            }

            break
        case "Recommendations":
            if self.currentIndex+9 < THProfileClosetManager.sharedInstance.closetRecommendations.count {
                self.paginationButton.isEnabled = true
            }
            break
        case "Brand":
            if self.currentIndex+9 < THBrandsManager.sharedInstance.products.count {
                self.paginationButton.isEnabled = true
            }
            break
        case "Favorites":
            if self.currentIndex+9 < THProfileClosetManager.sharedInstance.closetFavorite.count {
                self.paginationButton.isEnabled = true
            }
            break

        case "Post":
            if self.currentIndex+9 < THProfilePostsManager.sharedInstance.currentUserProfilePosts.count {
                self.paginationButton.isEnabled = true
            }
            break
        case "NearBy":
            if self.currentIndex+9 < THRetailBrandManager.sharedInstance.RetailProducts.count {
                self.paginationButton.isEnabled = true
            }
        default:
            break
        }

    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let object = virtualObject else {
            //let p = touches.location(in: self.sceneLocationView)
           /* let hitResults = touches.first?.location(in: self.sceneLocationView)
            // check that we clicked on at least one object
            if hitResults.count > 0 {
                let result = hitResults[0]
                if let name = result.node.name {
                    self.virtualObject = self.sceneLocationView.sceneNode?.childNode(withName: (name)!, recursively: true) as! LocationNode
                    self.virtualObject?.viewController = self
                }
            }*/
            return
        }

        if currentGesture == nil {
            currentGesture = Gesture.startGestureFromTouches(touches, self.sceneLocationView, object)
        } else {
            currentGesture = currentGesture!.updateGestureFromTouches(touches, .touchBegan)
        }

        displayVirtualObjectTransform()
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        currentGesture = currentGesture?.updateGestureFromTouches(touches, .touchMoved)
        displayVirtualObjectTransform()
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        currentGesture = currentGesture?.updateGestureFromTouches(touches, .touchEnded)
    }

    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
//        guard let object = virtualObject else {
//            return
//        }
        currentGesture = currentGesture?.updateGestureFromTouches(touches, .touchCancelled)
    }

    //ARKit files
    func displayVirtualObjectTransform() {
        guard let object = virtualObject, let cameraTransform = sceneLocationView.session.currentFrame?.camera.transform else {
            return
        }

        // Output the current translation, rotation & scale of the virtual object as text.
        let cameraPos = SCNVector3.positionFromTransform(cameraTransform)
        let vectorToCamera = cameraPos - object.position

        let distanceToUser = vectorToCamera.length()

        var angleDegrees = Int(((object.eulerAngles.y) * 180) / Float.pi) % 360
        if angleDegrees < 0 {
            angleDegrees += 360
        }

        let distance = String(format: "%.2f", distanceToUser)
        let scale = String(format: "%.2f", object.scale.x)
    }

    func moveVirtualObjectToPosition(_ pos: SCNVector3?, _ instantly: Bool, _ filterPosition: Bool) {

         guard let newPosition = pos else {

        // Reset the content selection in the menu only if the content has not yet been initially placed.
        if virtualObject == nil {
            //resetVirtualObject()

        }
           return
        }

        if instantly {
            setNewVirtualObjectPosition(newPosition)
        } else {
            updateVirtualObjectPosition(newPosition, filterPosition)
        }
    }

    func setNewVirtualObjectPosition(_ pos: SCNVector3) {

        guard let object = virtualObject, let cameraTransform = sceneLocationView.session.currentFrame?.camera.transform else {
            return
        }

        recentVirtualObjectDistances.removeAll()

        let cameraWorldPos = SCNVector3.positionFromTransform(cameraTransform)
        var cameraToPosition = pos - cameraWorldPos
        cameraToPosition.setMaximumLength(DEFAULT_DISTANCE_CAMERA_TO_OBJECTS)

        object.position = cameraWorldPos + cameraToPosition

        if object.parent == nil {
            sceneLocationView.scene.rootNode.addChildNode(object)
        }
    }

    func resetVirtualObject() {
        /*virtualObject?.unloadModel()
         virtualObject?.removeFromParentNode()
         virtualObject = nil

         addObjectButton.setImage(#imageLiteral(resourceName: "add"), for: [])
         addObjectButton.setImage(#imageLiteral(resourceName: "addPressed"), for: [.highlighted])*/
    }

    func updateVirtualObjectPosition(_ pos: SCNVector3, _ filterPosition: Bool) {
        guard let object = virtualObject else {
            return
        }

        guard let cameraTransform = sceneLocationView.session.currentFrame?.camera.transform else {
            return
        }

        let cameraWorldPos = SCNVector3.positionFromTransform(cameraTransform)
        var cameraToPosition = pos - cameraWorldPos
        cameraToPosition.setMaximumLength(DEFAULT_DISTANCE_CAMERA_TO_OBJECTS)

        // Compute the average distance of the object from the camera over the last ten
        // updates. If filterPosition is true, compute a new position for the object
        // with this average. Notice that the distance is applied to the vector from
        // the camera to the content, so it only affects the percieved distance of the
        // object - the averaging does _not_ make the content "lag".
        let hitTestResultDistance = CGFloat(cameraToPosition.length())

        recentVirtualObjectDistances.append(hitTestResultDistance)
        recentVirtualObjectDistances.keepLast(10)

        if filterPosition {
            let averageDistance = recentVirtualObjectDistances.average!

            cameraToPosition.setLength(Float(averageDistance))
            let averagedDistancePos = cameraWorldPos + cameraToPosition

            object.position = averagedDistancePos
        } else {
            object.position = cameraWorldPos + cameraToPosition
        }
    }

    func worldPositionFromScreenPosition(_ position: CGPoint,
                                         objectPos: SCNVector3?,
                                         infinitePlane: Bool = false) -> (position: SCNVector3?,
        planeAnchor: ARPlaneAnchor?,
        hitAPlane: Bool) {

            // -------------------------------------------------------------------------------
            // 1. Always do a hit test against exisiting plane anchors first.
            //    (If any such anchors exist & only within their extents.)

            let planeHitTestResults = sceneLocationView.hitTest(position, types: .existingPlaneUsingExtent)
            if let result = planeHitTestResults.first {

                let planeHitTestPosition = SCNVector3.positionFromTransform(result.worldTransform)
                let planeAnchor = result.anchor

                // Return immediately - this is the best possible outcome.
                return (planeHitTestPosition, planeAnchor as? ARPlaneAnchor, true)
            }

            // -------------------------------------------------------------------------------
            // 2. Collect more information about the environment by hit testing against
            //    the feature point cloud, but do not return the result yet.

            var featureHitTestPosition: SCNVector3?
            var highQualityFeatureHitTestResult = false

            let highQualityfeatureHitTestResults =
                sceneLocationView.hitTestWithFeatures(position, coneOpeningAngleInDegrees: 18, minDistance: 0.2, maxDistance: 2.0)

            if !highQualityfeatureHitTestResults.isEmpty {
                let result = highQualityfeatureHitTestResults[0]
                featureHitTestPosition = result.position
                highQualityFeatureHitTestResult = true
            }

            // -------------------------------------------------------------------------------
            // 3. If desired or necessary (no good feature hit test result): Hit test
            //    against an infinite, horizontal plane (ignoring the real world).

            if (infinitePlane && dragOnInfinitePlanesEnabled) || !highQualityFeatureHitTestResult {

                let pointOnPlane = objectPos ?? SCNVector3Zero

                let pointOnInfinitePlane = sceneLocationView.hitTestWithInfiniteHorizontalPlane(position, pointOnPlane)
                if pointOnInfinitePlane != nil {
                    return (pointOnInfinitePlane, nil, true)
                }
            }

            // ---------------------------var trackingFallbackTimer: Timer?----------------------------------------------------
            // 4. If available, return the result of the hit test against high quality
            //    features if the hit tests against infinite planes were skipped or no
            //    infinite plane was hit.

            if highQualityFeatureHitTestResult {
                return (featureHitTestPosition, nil, false)
            }

            // -------------------------------------------------------------------------------
            // 5. As a last resort, perform a second, unfiltered hit test against features.
            //    If there are no features in the scene, the result returned here will be nil.

            let unfilteredFeatureHitTestResults = sceneLocationView.hitTestWithFeatures(position)
            if !unfilteredFeatureHitTestResults.isEmpty {
                let result = unfilteredFeatureHitTestResults[0]
                return (result.position, nil, false)
            }

            return (nil, nil, false)
    }

    func checkIfObjectShouldMoveOntoPlane(anchor: ARPlaneAnchor) {
        /* guard let object = sceneLocationView.sceneNode, let planeAnchorNode = sceneLocationView.node(for: anchor) else {
         return
         }

         // Get the object's position in the plane's coordinate system.
         let objectPos = planeAnchorNode.convertPosition(object.position, from: object.parent)

         if objectPos.y == 0 {
         return; // The object is already on the plane
         }

         // Add 10% tolerance to the corners of the plane.
         let tolerance: Float = 0.1

         let minX: Float = anchor.center.x - anchor.extent.x / 2 - anchor.extent.x * tolerance
         let maxX: Float = anchor.center.x + anchor.extent.x / 2 + anchor.extent.x * tolerance
         let minZ: Float = anchor.center.z - anchor.extent.z / 2 - anchor.extent.z * tolerance
         let maxZ: Float = anchor.center.z + anchor.extent.z / 2 + anchor.extent.z * tolerance

         if objectPos.x < minX || objectPos.x > maxX || objectPos.z < minZ || objectPos.z > maxZ {
         return
         }

         // Drop the object onto the plane if it is near it.
         let verticalAllowance: Float = 0.03
         if objectPos.y > -verticalAllowance && objectPos.y < verticalAllowance {
         textManager.showDebugMessage("OBJECT MOVED\nSurface detected nearby")

         SCNTransaction.begin()
         SCNTransaction.animationDuration = 0.5
         SCNTransaction.animationTimingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
         object.position.y = anchor.transform.columns.3.y
         SCNTransaction.commit()
         }*/
    }

}

@available(iOS 11.0, *)
extension THARClosetViewController: UICollectionViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == categoriesCollectionView {
            let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
            let intPageNumber = Int(pageNumber)
            if intPageNumber != selectedCategoriesIndex {
                //self.collectionView(categoriesCollectionView, didSelectItemAt: IndexPath(item: intPageNumber, section: 0))
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView.tag == 0 {
            if indexPath.row == THProfilePostsManager.sharedInstance.profileBrandFollowing.count - 1 {
                if THProfilePostsManager.sharedInstance.didLoadDataBrand {
                    THProfilePostsManager.sharedInstance.didLoadDataBrand = false
                    if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                        THProfilePostsManager.sharedInstance.retrieveProfileBrandFollowing(with: usercurrent.userId) { (error) in
                            guard let error = error else {
                                self.brandListingCollectionView.reloadData()
                                return
                            }
                            debugPrint(error)
                        }
                    }
                }
            }
        }
    }


    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView.tag {
        case 0:
            for locationnode in self.locationNodes {
                self.sceneLocationView.removeLocationNode(locationNode: locationnode)
            }
            for locationnode in self.locationBrandNodes {
                self.sceneLocationView.removeLocationBrandNode(locationNode: locationnode)
            }
            //sceneLocationView.reset()
            selectedBrandIndex = indexPath.row
            selectedCategoriesIndex = -1
            //categoriesCollectionView.reloadData()
            brandListingCollectionView.reloadData()
            switch indexPath.item {
            case 0:
                self.loadEndPointForClosetFavorites()
                return
            case 1:
                self.loadEndPointForRecommendations(isShowLoader: false)
                return
            default:
                break
            }
            
            if selectedBrandID == THProfilePostsManager.sharedInstance.profileBrandFollowing[indexPath.row-2].id {
                loadEndPointForBrand(brandId: selectedBrandID)
            } else {
                selectedBrandID = THProfilePostsManager.sharedInstance.profileBrandFollowing[indexPath.row-2].id
                loadEndPointForBrand(brandId: selectedBrandID, isRefresh: true)
            }

            //Add funnel of brand on closet feed
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Brand Closet Feed", customAttributes: ["userid":usercurrent.userId,
                                                                                         "username": usercurrent.username, "brand_id":THProfilePostsManager.sharedInstance.profileBrandFollowing[indexPath.row].id,
                                                                                         "brand_name":THProfilePostsManager.sharedInstance.profileBrandFollowing[indexPath.row].name])
            }
            break
        case 1:
            if indexPath.row == selectedCategoriesIndex {
                if categoriesListingArray[indexPath.row] == "< Categories" {
                    self.categoriesListingArray[0] = "Categories"
                    self.categoriesCollectionView.reloadItems(at: [IndexPath(row: 0, section: 0)])
                    categoryType = "Main"
                    cartCategoryString = ""
                    categoryID = -1
                    for locationnode in self.locationBrandNodes {
                        self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                    }
                    for locationnode in self.locationNodes {
                        self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                    }

                    self.loadEndPointForCategories(isShowLoader: true)
                }

                if selectedCategoriesIndex == 2 {
                    self.showMapViewMethod()
                }
                return
            }
            selectedCategoriesIndex = indexPath.row
            selectedBrandIndex = -1
            brandListingCollectionView.reloadData()
            categoriesCollectionView.reloadData()
            let selectedCategory = categoriesListingArray[indexPath.row]
            switch selectedCategory {
                case "Categories":
                    //Add funnel of Category tab
                    if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                        Answers.logCustomEvent(withName: "Tap on Categories Tab From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
                    }
                    for locationnode in self.locationBrandNodes {
                        self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                    }
                    for locationnode in self.locationNodes {
                        self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                    }
                    loadEndPointForCategories(isShowLoader: true)
                    break
                case "< Categories":
                    //Add funnel of Category tab
                    if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                        Answers.logCustomEvent(withName: "Tap on Categories Tab From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
                    }
                    for locationnode in self.locationBrandNodes {
                        self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                    }
                    for locationnode in self.locationNodes {
                        self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                    }
                    loadEndPointForSubCategoriesProduct(isShowLoader: true)
                    break
                case "Purchased":
                    for locationnode in self.locationBrandNodes {
                        self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                    }
                    for locationnode in self.locationNodes {
                        self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                    }
                    //Add funnel of closet feed
                    if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                        Answers.logCustomEvent(withName: "Tap on Purchased Tab From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
                    }
                    loadEndPointForOrder()
                    break
                case "Recommendations":
                    //Add funnel of closet feed
                    if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                        Answers.logCustomEvent(withName: "Tap on Recommendations Tab From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
                    }
                    for locationnode in self.locationBrandNodes {
                        self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                    }
                    for locationnode in self.locationNodes {
                        self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                    }
                    loadEndPointForRecommendations(isShowLoader: true)
                    break
                case "Your Posts":
                    //Add funnel of closet feed
                    if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                        Answers.logCustomEvent(withName: "Tap on Your Posts Tab From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
                    }
                    for locationnode in self.locationBrandNodes {
                        self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                    }
                    for locationnode in self.locationNodes {
                        self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                    }
                    loadEndPointForYourPosts()
                    break
                case "Favorites":
                    //Add funnel of closet feed
                    if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                        Answers.logCustomEvent(withName: "Tap on Favorites Tab From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
                    }
                    for locationnode in self.locationBrandNodes {
                        self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                    }
                    for locationnode in self.locationNodes {
                        self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                    }
                    loadEndPointForClosetFavorites()
                    break
                case "Pop Ups":
                    if CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedAlways &&
                        CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedWhenInUse && CLLocationManager.authorizationStatus() != CLAuthorizationStatus.denied {
                        self.bullitenItemOfLocationAllowPermission(completion: {
                            //Add funnel of closet feed
                            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                                Answers.logCustomEvent(withName: "Tap on Nearby Tab From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
                            }
                            for locationnode in self.locationBrandNodes {
                                self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                            }
                            let allAnnotations = self.mapView.annotations
                            self.mapView.removeAnnotations(allAnnotations)

                            self.isProductFound = false
                            self.brandLocationAnnotationArray = [markerAnnotation]()
                            self.locationManager.retailStoreLocation(view: self.view)

                            self.updateUserBrandLocation()
                        })
                        return
                    }

                    if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.denied ||
                        CLLocationManager.authorizationStatus() == CLAuthorizationStatus.restricted {
                        self.bullitenItemOfLocationDeniedPermission()
                    }
                    //Add funnel of closet feed
                    if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                        Answers.logCustomEvent(withName: "Tap on Nearby Tab From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
                    }
                    for locationnode in self.locationBrandNodes {
                        self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                    }
                    let allAnnotations = self.mapView.annotations
                    self.mapView.removeAnnotations(allAnnotations)

                    self.isProductFound = false
                    brandLocationAnnotationArray = [markerAnnotation]()
                    locationManager.retailStoreLocation(view: self.view)
                    self.updateUserBrandLocation()
                    break
                default:
                    break
            }
            break
        default:
            break
        }
    }
}

@available(iOS 11.0, *)
extension THARClosetViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView.tag {
        case 0:
            return THProfilePostsManager.sharedInstance.profileBrandFollowing.count + 2
        case 1:
            return categoriesListingArray.count
        default:
            return 0
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView.tag {
        case 0:
            let brandCell = collectionView.dequeueReusableCell(withReuseIdentifier: BrandListCollectionViewCell.className, for: indexPath) as! BrandListCollectionViewCell
            if indexPath.row == selectedBrandIndex {
                brandCell.brandImageView.layer.borderWidth = 2
                brandCell.brandImageView.layer.borderColor = UIColor.richBlueButton.cgColor
            } else {
                brandCell.brandImageView.layer.borderWidth = 1
                brandCell.brandImageView.layer.borderColor = UIColor.lightGray.cgColor
            }
            
            if indexPath.item < 2 {
                
                brandCell.brandImageView.image = indexPath.item == 0 ? Ionicons.androidFavorite.image(35, color: .custColor(r: 233, g: 0, b: 55, a: 1)) : UIImage(named: "rc_icon")
                brandCell.brandImageView.contentMode = .center
            } else {
                brandCell.brandImageView.sd_setImage(with: URL(string: THProfilePostsManager.sharedInstance.profileBrandFollowing[indexPath.item-2].logoURL), placeholderImage: nil, options: .retryFailed)
                brandCell.brandImageView.contentMode = .scaleAspectFit
            }
            return brandCell
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: THCategoriedListingCollectionViewCell.className, for: indexPath) as! THCategoriedListingCollectionViewCell
            cell.configure(categoryName: categoriesListingArray[indexPath.row])
            if selectedCategoriesIndex == indexPath.row {
                cell.categoriedLabel.backgroundColor = UIColor.hex("178dcd", alpha: 1)
            } else {
                switch categoriesListingArray[indexPath.row] {
                case "Pop Ups":
                    cell.categoriedLabel.backgroundColor = UIColor.hex("81c88f", alpha: 1)
                    break
                case "Favorites":
                    cell.categoriedLabel.backgroundColor = UIColor.hex("fcdf4e", alpha: 1)
                    break
                default:
                    cell.categoriedLabel.backgroundColor = UIColor.closetBlack
                    break
                }
            }
            return cell
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: THCategoriedListingCollectionViewCell.className, for: indexPath) as! THCategoriedListingCollectionViewCell
            cell.configure(categoryName: categoriesListingArray[indexPath.row])
            return cell
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
        switch collectionView.tag {
        case 0:
            if screenheight <= 568 {
                return CGSize(width: 40, height: 40)
            } else {
                return CGSize(width: 50, height: 50)
            }
        case 1:
            var stringWidth:CGSize!
            let categoryName = categoriesListingArray[indexPath.row]
            if screenheight <= 568 {
                stringWidth = categoryName.size(attributes: [NSFontAttributeName: UIFont.AppFontRegular(11)])
            } else {
                stringWidth = categoryName.size(attributes: [NSFontAttributeName: UIFont.AppFontRegular(14)])
            }
            return CGSize(width: Int(stringWidth.width + 15.0), height: 35)
        default:
             return CGSize(width: 0, height: 0)
        }
    }
}


@available(iOS 11.0, *)
extension THARClosetViewController: ARSwipeDelegate {
    func swipeDown() {
        self.sceneLocationView.pause()
    }

    func swipeUP() {
        self.sceneLocationView.run()
    }
}


@available(iOS 11.0, *)
extension THARClosetViewController: SceneLocationViewDelegate {
    func renderer(_ renderer: SCNSceneRenderer, willUpdate node: SCNNode, for anchor: ARAnchor) {
    }

    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        if !isSingleRow {
            return
        }
        if let planeAnchor = anchor as? ARPlaneAnchor {
            self.addPlane(node: node, anchor: planeAnchor)
        }
    }

    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
    }

    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {

    }

    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        if !isSingleRow {
            return
        }
       if sceneLocationView.sceneNode?.childNodes.last != nil {
            guard let lastNodez = sceneLocationView.sceneNode?.childNodes.last?.position.z else {
                return
            }

            guard let cameraZ = sceneLocationView.pointOfView?.position.z else {
                return
            }
            if lastNodez > cameraZ-1 {
                switch selectedPosterType {
                    case "Post":
                        self.SetupUserPosterInRow(isFirstLoad: false)
                        break
                    case "Categories":
                        if categoryType == "main" {
                            self.SetupCategoriesPosterInRow(isFirstLoad: false)
                        } else {
                            self.SetupProductPosterInRow(isFirstLoad: false)
                        }
                        break
                    default:
                        self.SetupProductPosterInRow(isFirstLoad: false)
                        break
                }
            }
        }

        DispatchQueue.main.async {
            self.updateFocusSquare()
        }
    }

    func sessionInterruptionEnded(_ session: ARSession) {
        if !isSingleRow {
            return
        }

        textManager.showMessage("Resetting your Smart Closet...")
    }

    func sessionWasInterrupted(_ session: ARSession) {
    }

    func session(_ session: ARSession, didFailWithError error: Error) {
    }

    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        if !isSingleRow {
            return
        }
        switch camera.trackingState {
            case .normal:
                 if sceneLocationView.sceneNode?.childNodes.first != nil {
                    textManager.showTrackingQualityInfo(for: camera.trackingState, autoHide: true)
                 } else {
                    DispatchQueue.main.async {
                        self.messagePanel.isHidden = true
                    }
                 }
                break
            default:
                textManager.showTrackingQualityInfo(for: camera.trackingState, autoHide: true)
                break
        }
    }

    func sceneLocationViewDidAddSceneLocationEstimate(sceneLocationView: SceneLocationView, position: SCNVector3, location: CLLocation) {
    }

    func sceneLocationViewDidRemoveSceneLocationEstimate(sceneLocationView: SceneLocationView, position: SCNVector3, location: CLLocation) {
    }

    func sceneLocationViewDidConfirmLocationOfNode(sceneLocationView: SceneLocationView, node: LocationNode) {
    }

    func sceneLocationViewDidSetupSceneNode(sceneLocationView: SceneLocationView, sceneNode: SCNNode) {
    }

    func sceneLocationViewDidUpdateLocationAndScaleOfLocationNode(sceneLocationView: SceneLocationView, locationNode: LocationNode) {
    }
}

@available(iOS 11.0, *)
extension THARClosetViewController: LocationMangerRegionDelegate {
    func locationManagerUpdate(coordinate: CLLocationCoordinate2D) {

    }

    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
    }

    func retailEndpoitCalled() {
      /*  let _ = Timer.scheduledTimer(withTimeInterval: 4, repeats: false) { (sucess) in
            if !self.isProductFound {
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "", body: "No location-based products found near you! More coming soon!", completion: {
                }), animated: true, completion: nil)
            }
        }*/
    }

    func locationManager(_ manager: CLLocationManager, didVisit visit: CLVisit) {
    }

    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {

        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        let state = UIApplication.shared.applicationState
        if state == .background {
            var userInfo = [String:String]()
            userInfo["location"] = "location"
            GeneralMethods.sharedInstance.dispatchlocalNotification(with: "Threadest", body: "YOU ARE NEAR AN AR POPUP SHOP😎! LOOK AROUND THROUGH YOUR CAMERA 📲", userInfo: userInfo, at: Date())
        } else if state == .active {
            /*for locationnode in self.locationNodes {
                self.sceneLocationView.removeLocationNode(locationNode: locationnode)
            }
            if let i = THUserManager.sharedInstance.retailLocation.index(where: { $0.id == Int(region.identifier)}){
                for brandan in self.brandLocationAnnotationArray {
                    if brandan.id == "\(THUserManager.sharedInstance.retailLocation[i].id)" {
                        brandan.subtitle = "You've discovered a Popup Shop! Tap to enter."
                        self.mapView.showAnnotations([brandan], animated: true)
                    }
                }
            }*/
           /* let productFoundPopup = THProductFountViewController(nibName: THProductFountViewController.className, bundle: nil)
            productFoundPopup.messageText = "YOU ARE NEAR AN AR POPUP SHOP😎! LOOK AROUND THROUGH YOUR CAMERA 📲"
            productFoundPopup.productFoundPopupCompletionHander = {
                for locationnode in self.locationNodes {
                    self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                }
                 if let i = THUserManager.sharedInstance.retailLocation.index(where: { $0.id == Int(region.identifier)}){
                    self.selectedLocationId = THUserManager.sharedInstance.retailLocation[i].id
                    self.loadEndPointForNearByProducts()
                }
            }
            self.addChildViewController(productFoundPopup)
            productFoundPopup.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
            self.view.addSubview(productFoundPopup.view)
            productFoundPopup.didMove(toParentViewController: self)*/
        }
    }

    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
       /* if let i = THUserManager.sharedInstance.retailLocation.index(where: { $0.id == Int(region.identifier)}){
            for brandan in self.brandLocationAnnotationArray {
                if brandan.id == "\(THUserManager.sharedInstance.retailLocation[i].id)" {
                    brandan.subtitle = "Reach this location to access."
                    self.mapView.showAnnotations([brandan], animated: true)
                }
            }
        }*/
    }

    func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
        switch state {
            case .inside:
                //Add funnel of closet feed
              /*if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    Answers.logCustomEvent(withName: "Inside Region From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
                }
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                self.isProductFound = true
                let productFoundPopup = THProductFountViewController(nibName: THProductFountViewController.className, bundle: nil)
                productFoundPopup.messageText = "YOU ARE NEAR AN AR POPUP SHOP😎! LOOK AROUND THROUGH YOUR CAMERA 📲"
                productFoundPopup.productFoundPopupCompletionHander = {
                    for locationnode in self.locationNodes {
                    self.sceneLocationView.removeLocationNode(locationNode: locationnode)
                    }
                    if let i = THUserManager.sharedInstance.retailLocation.index(where: { $0.id == Int(region.identifier)}){
                        //self.sceneLocationView.reset()
                        self.selectedLocationId = THUserManager.sharedInstance.retailLocation[i].id
                        self.loadEndPointForNearByProducts()
                    }
                }
                self.addChildViewController(productFoundPopup)
                productFoundPopup.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
                self.view.addSubview(productFoundPopup.view)
                productFoundPopup.didMove(toParentViewController: self)
                AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))*/
                break
            case .outside:
               /* if let i = THUserManager.sharedInstance.retailLocation.index(where: { $0.id == Int(region.identifier)}){

                    guard let retailLatitude = CLLocationDegrees(exactly: THUserManager.sharedInstance.retailLocation[i].latitude), let retailLongitude = CLLocationDegrees(exactly: THUserManager.sharedInstance.retailLocation[i].longitude) else {
                        return
                    }
                    self.addAnnotationMapView(latitude: retailLatitude, logitude: retailLongitude, name: THUserManager.sharedInstance.retailLocation[i].brandName)
                    let pinLocation = CLLocation(coordinate: CLLocationCoordinate2D(latitude: retailLatitude, longitude: retailLongitude), altitude: (LocationManager().currentLocation?.altitude)!)

                    let retailStoreLocation = CLLocation(latitude: retailLatitude, longitude: retailLongitude)

                    guard let  distance = LocationManager().currentLocation?.distance(from: retailStoreLocation) else {
                        return
                    }
                    let distanceInMile = distance/1609.344
                    var strMile = ""
                    if distanceInMile < 2 {
                        strMile = String(format: "%.2f mile", distance/1609.344)
                    } else {
                        strMile = String(format: "%.2f miles", distance/1609.344)
                    }
                    let pinLocationNode = LocationAnnotationNode(location: pinLocation, nameURL: THUserManager.sharedInstance.retailLocation[i].brandLogoURL, distance: strMile)
                   pinLocationNode.name = "brandLocation"
                   pinLocationNode.locationConfirmed = true
                    sceneLocationView.addLocationNodeWithConfirmedLocation(locationNode: pinLocationNode)
                    self.locationBrandNodes.append(pinLocationNode)
                }*/
                break
            case .unknown:
                break
            }
    }
}

@available(iOS 11.0, *)
extension THARClosetViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        return false
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {

    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.didTapOnSearchButton(self)
        self.delegate?.showSearchBar(textField: textField)
        return false
    }
}

extension UIView {
    func recursiveSubviews() -> [UIView] {
        var recursiveSubviews = self.subviews
        for subview in subviews {
            recursiveSubviews.append(contentsOf: subview.recursiveSubviews())
        }
        return recursiveSubviews
    }
}

@available(iOS 11.0, *)
extension THARClosetViewController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        dropDownArraow.isSelected = true
        if showElementTimer != nil {
            showElementTimer.invalidate()
            showElementTimer = nil
        }
        self.topBar.alpha = 1
        self.topBrandConstraint.constant = 50
        return .none
    }

    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        dropDownArraow.isSelected = false
        self.showTimerElement()
    }
}

@available(iOS 11.0, *)
extension THARClosetViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        guard let marker = view.annotation as? markerAnnotation else {
            return
        }
        if (view.annotation?.subtitle)! != "You've discovered a Popup Shop! Tap to enter." {
            return
        }
        for locationnode in self.locationBrandNodes {
            self.sceneLocationView.removeLocationNode(locationNode: locationnode)
        }
        for locationnode in self.locationNodes {
            self.sceneLocationView.removeLocationNode(locationNode: locationnode)
        }
        //Add funnel of closet feed
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Enter Region From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }
        self.hideMapView(self)
        if self.selectedLocationId == Int(marker.id!)! {
            self.loadEndPointForRetailNearByProducts()
        } else {
            self.selectedLocationId = Int(marker.id!)!
            self.loadEndPointForRetailNearByProducts(isRefresh: true)
        }
    }



    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let overlay = overlay as? MKCircle {
            let circleRenderer = MKCircleRenderer(circle: overlay)
            circleRenderer.strokeColor = UIColor.red
            circleRenderer.lineWidth = 1.0
            circleRenderer.fillColor = UIColor.clear
            return circleRenderer
        }
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.hex("#178dcd", alpha: 1)
        renderer.lineWidth = 4.0
        return renderer
    }

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        view.setSelected(true, animated: true)
    }

    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        view.setSelected(false, animated: true)
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }

        guard let markerAnno = annotation as? markerAnnotation else {
            return nil
        }

        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: markerAnno.id!) as? MKMarkerAnnotationView
        if annotationView == nil {
            annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: markerAnno.id)
            annotationView?.titleVisibility = .visible
            annotationView?.subtitleVisibility = .adaptive
            annotationView?.markerTintColor = markerAnno.subtitle == "You've discovered a Popup Shop! Tap to enter." ? UIColor.hex("81c88f", alpha: 1):UIColor.hex("178dcd", alpha: 1)
            annotationView?.animatesWhenAdded = true
            annotationView?.glyphImage = nil
            annotationView?.glyphText = markerAnno.title
            annotationView?.glyphTintColor = UIColor.white
            annotationView?.canShowCallout = true

            let brandButton = UIButton(type: .detailDisclosure)//UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
            //brandButton.buttonType = .detailDisclosure
            brandButton.layer.borderWidth = 1
            brandButton.layer.borderColor = UIColor.gray.cgColor
            brandButton.layer.cornerRadius = brandButton.frame.size.width/2
            brandButton.layer.masksToBounds = true
            annotationView?.rightCalloutAccessoryView = brandButton
            if let i = THUserManager.sharedInstance.retailLocation.index(where: { $0.id == Int(markerAnno.id!)}){
                let retailLocation = THUserManager.sharedInstance.retailLocation[i]
                brandButton.sd_setImage(with: URL(string: retailLocation.brandLogoURL), for: .normal, placeholderImage: nil, options: .retryFailed, completed: nil)
                brandButton.sd_setImage(with: URL(string: retailLocation.brandLogoURL), for: .normal, placeholderImage: nil, options: .retryFailed, completed: { (image, error, cache, url) in
                    brandButton.setImage(image?.withRenderingMode(.alwaysOriginal), for: .normal)
                })
            }

        } else {
            annotationView?.annotation = annotation
            annotationView?.markerTintColor = markerAnno.subtitle == "You've discovered a Popup Shop! Tap to enter." ? UIColor.hex("81c88f", alpha: 1):UIColor.hex("178dcd", alpha: 1)
        }
        return annotationView
    }

    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }

}

