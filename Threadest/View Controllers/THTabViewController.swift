//
//  THTabViewController.swift
//  Threadest
//
//  Created by Jaydeep on 17/04/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import ARKit
import Crashlytics

class THTabViewController: UITabBarController {//SwipeableTabBarController {
    // MARK: Private vars
    
    var selectedTabIndex = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewControllers()
        configureTabBar()
    }
    
    // MARK: Private helpers
    fileprivate func configureTabBar() {
        
        self.delegate = self
        UITabBar.appearance().clipsToBounds = true
        self.tabBar.tintColor = UIColor.blueTabbar
        if #available(iOS 10.0, *) {
            self.tabBar.unselectedItemTintColor = UIColor.closetBlack
        } else {
            // Fallback on earlier versions
        }
    }
    
    fileprivate func configureViewControllers() {
        //AR Profile closet
        var arProfileClosetController: UIViewController!
        
        if #available(iOS 11.0, *) {
            if (ARConfiguration.isSupported) {
                if userdefault.object(forKey: userDefaultKey.ARONOFF.rawValue) == nil {
                    arProfileClosetController = THNonARClosetViewController(nibName: THNonARClosetViewController.className, bundle: nil)
                } else {
                    if let ARON = userdefault.object(forKey: userDefaultKey.ARONOFF.rawValue) as? Bool {
                        if ARON {
                            arProfileClosetController = THARClosetViewController(nibName: THARClosetViewController.className, bundle: nil)
                        } else {
                            arProfileClosetController = THNonARClosetViewController(nibName: THNonARClosetViewController.className, bundle: nil)
                        }
                    } else {
                        arProfileClosetController = THARClosetViewController(nibName: THARClosetViewController.className, bundle: nil)
                    }
                }
            } else {
                arProfileClosetController = THNonARClosetViewController(nibName: THNonARClosetViewController.className, bundle: nil)
            }
        } else {
            arProfileClosetController = THNonARClosetViewController(nibName: THNonARClosetViewController.className, bundle: nil)
        }
        //arProfileClosetController = THNonARClosetViewController(nibName: THNonARClosetViewController.className, bundle: nil)
        
        let arProfileClosetNavigationController = createNavigationController(withRootController: arProfileClosetController, title: "Closet", image: #imageLiteral(resourceName: "ic_tab_closet"), selectedImage: #imageLiteral(resourceName: "ic_tab_closet"))

        let swiftyCameraVC = THSwiftyCameraViewController(nibName: THSwiftyCameraViewController.className, bundle: nil)
        let cameraNavigationController = createNavigationController(withRootController: swiftyCameraVC,
                                                                    title: "New",
                                                                    image:#imageLiteral(resourceName: "Group"), selectedImage: #imageLiteral(resourceName: "Group"))
        cameraNavigationController.isNavigationBarHidden = true
        
        // Search
        let searchController = THARSearchViewController(nibName: THARSearchViewController.className, bundle: nil)
        let searchNavigationController = createNavigationController(withRootController: searchController,
                                                                    title: "Search",
                                                                    image:#imageLiteral(resourceName: "ic_tab_search"), selectedImage: #imageLiteral(resourceName: "ic_tab_search"))
        
        // Social
        let socialController = THSocialViewController(nibName: THSocialViewController.className, bundle: nil)
        let socialNavigationController = createNavigationController(withRootController: socialController,
                                                                    title: "Home",
                                                                    image: #imageLiteral(resourceName: "ic_tab_home"), selectedImage: #imageLiteral(resourceName: "ic_tab_home"))
        
        // Nearby
        let nearbyController = THARNearbyViewController(nibName: THARNearbyViewController.className, bundle: nil)
        let nearbyNavigationController = createNavigationController(withRootController: nearbyController,
                                                                    title: "Nearby",
                                                                    image: #imageLiteral(resourceName: "ic_tab_nearby"), selectedImage: #imageLiteral(resourceName: "ic_tab_nearby"))
        //
        
        viewControllers = [socialNavigationController,nearbyNavigationController, cameraNavigationController, arProfileClosetNavigationController,searchNavigationController]
    }
    
    fileprivate func createNavigationController(withRootController rootController: UIViewController, title: String, image: UIImage, selectedImage: UIImage) -> UINavigationController {
        let navigationController = UINavigationController(rootViewController: rootController)
        navigationController.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        navigationController.tabBarItem.image = image
        navigationController.tabBarItem.selectedImage = selectedImage
        
        return navigationController
    }
}

extension THTabViewController: UITabBarControllerDelegate {
    /*func tabBarController(_ tabBarController: UITabBarController, animationControllerForTransitionFrom fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        guard let fromVCIndex = tabBarController.viewControllers?.index(of: fromVC),
            let toVCIndex   = tabBarController.viewControllers?.index(of: toVC) else {
                return nil
        }
        
        swipeAnimatedTransitioning.fromLeft = fromVCIndex > toVCIndex
        return swipeAnimatedTransitioning
    }*/
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        viewController.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        
        let navController = viewController as! UINavigationController
        if (navController.topViewController is THARSearchViewController) {
            let searchController = navController.topViewController as! THARSearchViewController
            if (searchController.searchTableView != nil) {
                searchController.searchTableView.isHidden = true
                searchController.selectedSearchType = THSearchQueryManager.kStringConstantAll
            }
        }
        
        if (navController.topViewController is THSocialViewController) {
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Social Feed", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
            }
        }
        
        if (navController.topViewController is THARNearbyViewController) {
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Popup Shop Tab", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
            }
        }
        
        if (navController.topViewController is THSwiftyCameraViewController) {
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Camera Tab", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
            }
        }
        
        if #available(iOS 11.0, *) {
            if (navController.topViewController is THARClosetViewController) {
                //Add funnel of closet feed
                if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    Answers.logCustomEvent(withName: "AR closet feed", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
                }
            }
        }
        
        if (navController.topViewController is THNonARClosetViewController) {
            //Add funnel of closet feed
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "NON AR closet feed", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
            }
        }
        
        return true
    }
    
    func tabBarController(_ tabBarController: UITabBarController,
                                   didSelect viewController: UIViewController){
        let navController = viewController as! UINavigationController
        if (navController.topViewController is THSocialViewController && selectedTabIndex == 0) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MyNotification"), object:nil,
                                            userInfo:nil)
        }
        selectedTabIndex = tabBarController.selectedIndex
        
    }
    
}
