//
//  THFacebookPostingPopUp.swift
//  Threadest
//
//  Created by Jaydeep on 16/05/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THFacebookPostingPopUp: THBaseViewController {
    @IBOutlet weak var facebookPopup: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    var message:String!

    override func viewDidLoad() {
        super.viewDidLoad()
        facebookPopup.layer.cornerRadius = 10
        facebookPopup.layer.masksToBounds = true
        messageLabel.text = message;
        showAnimate()
    }
    
    @IBAction func btnActionClose(_ sender: Any) {
        removeAnimate()
    }
    
    @IBAction func btnActionOk(_ sender: Any) {
        removeAnimate()
    }
}
