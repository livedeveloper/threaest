//
//  THShopTableViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 17/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import CircleProgressView
import PassKit
import IoniconsSwift

class THShopTableViewCell: UITableViewCell {
    @IBOutlet weak var shopImage: UIImageView!
    @IBOutlet weak var availableSizeLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var commentLabel: UILabel!
    
    @IBOutlet weak var circularView: CircleProgressView!
    
    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet weak var applePayButton: UIButton!
    
    
    @IBOutlet weak var contactButton: UIButton!
    @IBOutlet weak var rethreadButton: UIButton!
    @IBOutlet weak var rethreadButtonTop: UIButton!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    var isFromRecommendation = false
    @IBOutlet weak var heightConstraintView: NSLayoutConstraint!
    var applepaymentbutton = PKPaymentButton()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contactButton.setImage(Ionicons.helpCircled.image(30, color: .gray), for: .normal)
        let paymentNetworks = [PKPaymentNetwork.discover, .masterCard, .amex, .visa]
        if PKPassLibrary.isPassLibraryAvailable() {
            if !PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetworks) {
             applepaymentbutton = PKPaymentButton(paymentButtonType: .setUp, paymentButtonStyle: .black)
                applepaymentbutton.frame.size.width = screenwidth/2-19
                applepaymentbutton.frame.size.height = addToCartButton.frame.size.height
                applePayButton.addSubview(applepaymentbutton)
            } else {
                applepaymentbutton = PKPaymentButton(paymentButtonType: .buy, paymentButtonStyle: .black)
                applepaymentbutton.frame.size.width = screenwidth/2-19
                applepaymentbutton.frame.size.height = addToCartButton.frame.size.height
                applePayButton.addSubview(applepaymentbutton)
            }
        }
        likeButton.setImage(Ionicons.iosStarOutline.image(30, color: .hex("fcdf4e", alpha: 1)), for: .normal)
        likeButton.setImage(Ionicons.star.image(30, color: .hex("fcdf4e", alpha: 1)), for: .selected)
    }
    
    
    func configure(withClosetCategory closetCategory: ClosetCategory) {
        if closetCategory.soldOut {
            priceLabel.attributedText = NSAttributedString()
            priceLabel.text = String(format: " $%.2f    ", closetCategory.defaultPrice)
            availableSizeLabel.isHidden = false
        } else {
            if closetCategory.salePrice == 0.0 {
                priceLabel.attributedText = NSAttributedString()
                priceLabel.text = String(format: " $%.2f    ", closetCategory.defaultPrice)
                availableSizeLabel.isHidden = true
            } else {
                availableSizeLabel.isHidden = false
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(format: " $%.2f    ", closetCategory.defaultPrice))
                attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
                priceLabel.text = ""
                priceLabel.attributedText = attributeString
            }
        }
        //priceLabel.text = String(format: " $%.2f    ", closetCategory.defaultPrice)
        descriptionLabel.text = closetCategory.title
        //likeLabel.isHidden = closetCategory.votesCount > 0 ? false:true
        //likeLabel.text = "\(closetCategory.votesCount)"
        commentLabel.isHidden = closetCategory.commentsCount > 0 ? false: true
        commentLabel.text = "\(closetCategory.commentsCount)"
        likeButton.isSelected = closetCategory.likedByCurrentUser ? true: false
        
       /* availableSizeLabel.isHidden = true
        if let currentUser = THUserManager.sharedInstance.currentUserdata(){
        if let _ = closetCategory.productVariations.index(where: { $0.size == currentUser.shirtSize }) {
                availableSizeLabel.isHidden = false
            } }
        if closetCategory.soldOut {
            availableSizeLabel.isHidden = false
        }*/
        let price = closetCategory.salePrice == 0.0 ? closetCategory.defaultPrice : closetCategory.salePrice
        availableSizeLabel.text = closetCategory.soldOut ? " sold out    ":String(format: " $%.2f    ", price)
        availableSizeLabel.backgroundColor = closetCategory.soldOut ? UIColor.trasperantRed: UIColor.trasperantblue
        if let _ = closetCategory.productVariations.index(where: {$0.quantityAvailable >= 1}) {
        } else {
            availableSizeLabel.isHidden = false
            availableSizeLabel.backgroundColor = UIColor.trasperantRed
            availableSizeLabel.text = " sold out    "
            priceLabel.attributedText = NSAttributedString()
            priceLabel.text = String(format: " $%.2f    ", closetCategory.defaultPrice)
        }
        
        circularView.isHidden = false
        
        var displayImageUrlString = String()
        if(isFromRecommendation) {
            displayImageUrlString = closetCategory.defaultImageUrl
        } else {
            displayImageUrlString = closetCategory.defaultMediumImageUrl
        }
        let imgHeight = CGFloat(closetCategory.defaultMediumImageHeight)
        let imgWidth = CGFloat(closetCategory.defaultMediumImageWidth)
        heightConstraintView.constant = imgWidth != 0 ? (imgHeight/imgWidth) * screenwidth:screenwidth
        shopImage.sd_setImage(with: URL(string: displayImageUrlString), placeholderImage: nil, options: .retryFailed, progress: { (block1, block2, imageurl) in
            let progress = Double(block1)/Double(block2)
            DispatchQueue.main.async {
                self.circularView.setProgress(progress, animated: true)
            }
        }) { (image, error, imageCacheType, imageUrl) in
            if image != nil {
                self.circularView.isHidden = true
            }else {
                print("image not found")
            }
        }
    }
    
    @IBAction func likeButtonAction(_ sender: UIButton) {
        
    }
    
}
