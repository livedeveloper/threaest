//
//  THProductVariationsTableViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 15/07/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THProductVariationsTableViewCell: UITableViewCell {
    @IBOutlet weak var sizeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        sizeLabel.layer.borderWidth = 1
        
        sizeLabel.layer.borderColor = UIColor(red: 75/255.0, green: 75/255.0, blue: 75/255.0, alpha: 1).cgColor
        sizeLabel.layer.cornerRadius = 17
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
