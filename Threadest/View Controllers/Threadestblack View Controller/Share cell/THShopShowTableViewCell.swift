//
//  THShopShowTableViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 08/04/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import PassKit
import IoniconsSwift
import FRHyperLabel

class THShopShowTableViewCell: UITableViewCell {
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var cartButton: UIButton!
    @IBOutlet weak var cartCountLabel: UILabel!
    
    @IBOutlet weak var soldOutLabel: UILabel!
    @IBOutlet weak var objCollectionView: UICollectionView!
    
    @IBOutlet weak var arrowDownButton: UIButton!
    @IBOutlet weak var commentsButton: DesignableButton!
    
    @IBOutlet weak var objScrollView: UIScrollView!
    @IBOutlet weak var scrollViewImage: UIImageView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var brandLogoImage: UIImageView!
    
   /* @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var commentCountLabel: UILabel!*/
    @IBOutlet weak var recordingLabel: UILabel!
    @IBOutlet weak var recordButton: UIButton!
    
    
    
    @IBOutlet weak var bottomArrowBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var descriptionTopConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var contactButton: UIButton!
    
    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var salePriceLabel: UILabel!
    
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var addToCardButton: UIButton!
    @IBOutlet weak var applePayButton: UIButton!
   // @IBOutlet weak var rethreadButton: UIButton!
    
    @IBOutlet weak var scoreButton: UIButton!
    @IBOutlet weak var btnNotifyMe: UIButton!

    @IBOutlet weak var impressionsLbl: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionDropDownButton: UIButton!
    
    @IBOutlet weak var giveAwayButton: DesignableButton!
    
    @IBOutlet weak var chooseYourSizeLabel: UILabel!
    
    @IBOutlet weak var freeShippingLabel: UILabel!
    @IBOutlet weak var truckImage: UIImageView!
    @IBOutlet weak var fullFilledByLabel: FRHyperLabel!
    @IBOutlet weak var constraintImageHeight: NSLayoutConstraint!
    @IBOutlet weak var descriptionLabelHeight: NSLayoutConstraint!
    var applePaymentButton = PKPaymentButton()
    
    var globelClosetCategory:ClosetCategory!
    override func awakeFromNib() {
        super.awakeFromNib()
        contactButton.setImage(Ionicons.helpCircled.image(30, color: .gray), for: .normal)
        
        let paymentNetworks = [PKPaymentNetwork.discover, .masterCard, .amex, .visa]
        if PKPassLibrary.isPassLibraryAvailable() {
            if !PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetworks) {
                applePaymentButton = PKPaymentButton(paymentButtonType: .setUp, paymentButtonStyle: .black)
                applePaymentButton.frame.size.width = screenwidth/2-19
                applePaymentButton.frame.size.height = 40//addToCartButton.frame.size.height
                applePayButton.addSubview(applePaymentButton)
            } else {
                applePaymentButton = PKPaymentButton(paymentButtonType: .buy, paymentButtonStyle: .black)
                applePaymentButton.frame.size.width = screenwidth/2-19
                applePaymentButton.frame.size.height = 40//addToCartButton.frame.size.height
                applePayButton.addSubview(applePaymentButton)
            }
        }
        likeButton.setImage(Ionicons.androidFavorite.image(30, color: .lightGray), for: .normal)
        likeButton.setImage(Ionicons.androidFavorite.image(30, color: .custColor(r: 233, g: 0, b: 55, a: 1)), for: .selected)
        
        cartCountLabel.layer.cornerRadius = cartCountLabel.frame.width / 2.0
        cartCountLabel.layer.masksToBounds = true
        brandLogoImage.layer.borderColor = UIColor.lightGray.cgColor
        brandLogoImage.layer.borderWidth = 1
        brandLogoImage.layer.cornerRadius = brandLogoImage.frame.width / 2.0
        brandLogoImage.layer.masksToBounds = true
        objScrollView.delegate = self
        objCollectionView.register(UINib(nibName: THSizeCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: THSizeCollectionViewCell.className)

        
        viewBottom.layer.shadowColor = UIColor.lightGray.cgColor
        viewBottom.layer.shadowOffset = CGSize(width: 2, height: -2)
        viewBottom.layer.shadowOpacity = 0.5
        viewBottom.layer.shadowRadius = 3
        
        applePayButton.isEnabled = false
        addToCardButton.isEnabled = false
        
        soldOutLabel.layer.cornerRadius = 20
        soldOutLabel.layer.borderColor = UIColor.lightGray.cgColor
        soldOutLabel.layer.borderWidth = 2
        soldOutLabel.layer.masksToBounds = true
        
        btnNotifyMe.layer.cornerRadius = 20
        btnNotifyMe.layer.borderColor = UIColor(red:0.51, green:0.78, blue:0.56, alpha:1.0).cgColor
        btnNotifyMe.layer.borderWidth = 2
        btnNotifyMe.layer.masksToBounds = true
        btnNotifyMe.isHidden = true
    }
    
    func configure(categoryDetail: ClosetCategory, isThreeD: Bool = false) {
        if categoryDetail.salePrice != 0 {
            self.scoreButton.backgroundColor = UIColor.lightGray
            UIView.animate(withDuration: 0.6,
                           animations: {
                            self.scoreButton.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
            },
                           completion: { _ in
                            self.scoreButton.backgroundColor = UIColor.hexa("178dcd", alpha: 1)
                            UIView.animate(withDuration: 0.6) {
                                self.scoreButton.transform = CGAffineTransform.identity
                            }
            })
        }
        if categoryDetail.soldOut {
            priceLabel.attributedText = NSAttributedString()
            priceLabel.text = String(format: " $%.2f    ", categoryDetail.defaultPrice)
            salePriceLabel.isHidden = true
            self.scoreButton.setAttributedTitle(nil, for: .normal)
            self.scoreButton.setTitle(" share ", for: .normal)
        } else {
            if categoryDetail.salePrice == 0.0 {
                priceLabel.attributedText = NSAttributedString()
                priceLabel.text = String(format: " $%.2f    ",
                                         categoryDetail.defaultPrice)
                salePriceLabel.isHidden = true
                self.scoreButton.setAttributedTitle(nil, for: .normal)
                self.scoreButton.setTitle(" share ", for: .normal)
                //self.scoreButton.setTitle("   earn for recommending   ", for: .normal)
            } else {
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(format: " $%.2f    ", categoryDetail.defaultPrice))
                attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
                priceLabel.text = ""
                priceLabel.attributedText = attributeString
                salePriceLabel.isHidden = false
                salePriceLabel.text = String(format: " $%.2f    ", categoryDetail.salePrice)
                let formattedString = NSMutableAttributedString()
                formattedString
                    .normal("   share to earn ")
                    .bold(String(format: "$%.2f", categoryDetail.currentUserDiscountAmount))
                    .normal(" off   ")
                self.scoreButton.setTitle("", for: .normal)
                self.scoreButton.setTitle(" share ", for: .normal)
                //self.scoreButton.setAttributedTitle(formattedString, for: .normal)
            }
        }
        
        if categoryDetail.productGalleryImages.count > 0 {
            let imgHeight = CGFloat(categoryDetail.productGalleryImages[0].mediumImageHeight)
            let imgWidth = CGFloat(categoryDetail.productGalleryImages[0].mediumImageWidth)
            constraintImageHeight.constant = (imgHeight/imgWidth) * screenwidth
        }
        globelClosetCategory = categoryDetail
        if let cartCount = UserDefaults.standard.value(forKey: "cartCount") {
            if (cartCount as! Int) == 0 {
                self.cartCountLabel.isHidden = true
            }
            else {
                self.cartCountLabel.isHidden = false
                self.cartCountLabel.text = "\(cartCount as! Int)"
            }
        } else {
            self.cartCountLabel.isHidden = true
        }
        if !isThreeD {
            let productGalleryImages = categoryDetail.productGalleryImages
            
            for i in 0..<productGalleryImages.count {
                let imgHeight = CGFloat(productGalleryImages[i].mediumImageHeight)
                let imgWidth = CGFloat(productGalleryImages[i].mediumImageWidth)
                let imgview = UIImageView(frame: CGRect(x: CGFloat(i)*screenwidth, y: 0, width: screenwidth, height: (imgHeight/imgWidth) * screenwidth))
                imgview.sd_setImage(with: URL(string: productGalleryImages[i].mediumImageUrl), placeholderImage: nil, options: .retryFailed)
                
                objScrollView.addSubview(imgview)
            }
            DispatchQueue.main.async {
                self.objScrollView.contentSize = CGSize(width: CGFloat(productGalleryImages.count) * screenwidth, height: self.objScrollView.frame.size.height)
            }
            //setup page control
            pageControl.numberOfPages = productGalleryImages.count
            pageControl.currentPage = 0
        }
        
        
        
        //fullFilledByLabel.setLinkForSubstring("\(categoryDetail.brandName)", withAttribute: [NSAttributedStringKey.foregroundColor.rawValue: UIColor.green]) { (label, str) in
        
        productTitleLabel.text = categoryDetail.title
        /*commentCountLabel.isHidden = categoryDetail.commentsCount > 0 ? false:true
        commentCountLabel.text = "\(categoryDetail.commentsCount)"*/
        //likeCountLabel.isHidden = categoryDetail.votesCount > 0 ? false:true
        //likeCountLabel.text = "\(categoryDetail.votesCount)"
        likeButton.isSelected = categoryDetail.likedByCurrentUser ? true:false
       // impressionsLbl.text = categoryDetail.impressionsCount > 50 ? "\(categoryDetail.impressionsCount) views" : ""
        impressionsLbl.text = ""
        brandLogoImage.sd_setImage(with: URL(string: categoryDetail.brandLogoImageUrl), placeholderImage: nil, options: .retryFailed)
        
       // btnNotifyMe.isHidden = !categoryDetail.soldOut
        
    }
    
    
    @IBAction func didTapOnDescriptionDropDown(_ sender: UIButton) {
       // descriptionLabel.text = descriptionLabel.text
       /* sender.isSelected = !sender.isSelected
        self.descriptionLabel.numberOfLines = sender.isSelected ? 0 : 1
        if sender.isSelected {
            descriptionLabelHeight.constant = 100
        } else {
            descriptionLabelHeight.constant = 21
        }*/
    }
    
    
    
}

extension THShopShowTableViewCell: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
        /*if let closet = globelClosetCategory {
            let imgHeight = CGFloat(closet.productGalleryImages[Int(pageNumber)].mediumImageHeight)
            let imgWidth = CGFloat(closet.productGalleryImages[Int(pageNumber)].mediumImageWidth)
            constraintImageHeight.constant = (imgHeight/imgWidth) * screenwidth
        }*/
    }
}

extension NSMutableAttributedString {
    @discardableResult func bold(_ text:String) -> NSMutableAttributedString {
        let attrs:[String:AnyObject] = [NSFontAttributeName: UIFont.AppFontBold(12), NSForegroundColorAttributeName: UIColor.white]
        let boldString = NSMutableAttributedString(string: text, attributes:attrs)
        self.append(boldString)
        return self
    }
    
    @discardableResult func normal(_ text:String)->NSMutableAttributedString {
        let normal =  NSAttributedString(string: text, attributes: [NSFontAttributeName: UIFont.AppFontSemiBold(12), NSForegroundColorAttributeName: UIColor.white])
        self.append(normal)
        return self
    }
}


