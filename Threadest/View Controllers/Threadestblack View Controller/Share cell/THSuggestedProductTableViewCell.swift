//
//  THSuggestedProductTableViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 03/11/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THSuggestedProductTableViewCell: UITableViewCell {

    @IBOutlet weak var objSuggestedCollectionView: UICollectionView!
    
    @IBOutlet weak var heightSuggestedConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        objSuggestedCollectionView.register(UINib(nibName: THSuggestionProductCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: THSuggestionProductCollectionViewCell.className)
        objSuggestedCollectionView.setCollectionViewLayout(CHTCollectionViewWaterfallLayout(), animated: false)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
