//
//  THSignUpGiveAwayTableViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 06/03/18.
//  Copyright © 2018 JC. All rights reserved.
//

import UIKit

class THSignUpGiveAwayTableViewCell: UITableViewCell {
    @IBOutlet weak var facebookButton: DesignableButton!
    @IBOutlet weak var emailButton: DesignableTextField!
    
    @IBOutlet weak var passwordButton: DesignableTextField!
    
    @IBOutlet weak var usernameField: DesignableTextField!
    
    @IBOutlet weak var phoneField: DesignableTextField!
    
    @IBOutlet weak var signupButton: DesignableButton!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
