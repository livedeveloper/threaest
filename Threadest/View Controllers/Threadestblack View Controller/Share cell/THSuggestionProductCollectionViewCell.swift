//
//  THSuggestionProductCollectionViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 02/11/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import SDWebImage

class THSuggestionProductCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var viewOut: UIView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var brandNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       /* viewOut.layer.cornerRadius = 5
        viewOut.layer.borderWidth = 0.5
        viewOut.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        viewOut.layer.masksToBounds = true*/
    }
    
    func configure(closetCategory: ClosetCategory) {
        productNameLabel.text = closetCategory.title
        brandNameLabel.text = closetCategory.brandName
        productImageView.sd_setImage(with: URL(string: closetCategory.defaultImageUrl), placeholderImage: nil, options: .retryFailed, completed: nil)
       /* let price = closetCategory.salePrice == 0.0 ? closetCategory.defaultPrice : closetCategory.salePrice
        priceLabel.text = closetCategory.soldOut ? "":String(format: " $%.2f    ", price)*/
        
        if closetCategory.soldOut {
            priceLabel.attributedText = NSAttributedString()
            priceLabel.text = String(format: " $%.2f    ", closetCategory.defaultPrice)
        } else {
            if closetCategory.salePrice == 0.0 {
                priceLabel.attributedText = NSAttributedString()
                priceLabel.text = String(format: " $%.2f    ", closetCategory.defaultPrice)
            } else {
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(format: " $%.2f ", closetCategory.defaultPrice))
                attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
                let salePriceString: NSMutableAttributedString = NSMutableAttributedString(string: String(format: "  $%.2f    ", closetCategory.salePrice))
                
                salePriceString.addAttribute(NSForegroundColorAttributeName, value: UIColor.hexa("178dcd", alpha: 1), range: NSMakeRange(0, salePriceString.length))
                salePriceString.addAttribute(NSFontAttributeName, value: UIFont.AppFontBold(12), range: NSMakeRange(0, salePriceString.length))
                
                attributeString.append(salePriceString)
                priceLabel.text = ""
                priceLabel.attributedText = attributeString
            }
        }
        
       /* let price = closetCategory.salePrice == 0.0 ? closetCategory.defaultPrice : closetCategory.salePrice
        availableLabel.text = closetCategory.soldOut ? "":String(format: " $%.2f    ", price)*/
    }

}
