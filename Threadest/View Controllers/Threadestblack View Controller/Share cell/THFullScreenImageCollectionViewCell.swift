//
//  THFullScreenImageCollectionViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 21/05/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THFullScreenImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var fullScreenImage: UIImageView!
    
    func configure(productImage: ProductGalleryImage) {
        fullScreenImage.sd_setImage(with: URL(string: productImage.mediumImageUrl), placeholderImage: nil, options: .retryFailed)
       
    }
}
