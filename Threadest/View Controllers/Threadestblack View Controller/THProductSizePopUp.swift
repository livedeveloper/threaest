//
//  THProductSizePopUp.swift
//  Threadest
//
//  Created by Nasrullah Khan on 11/04/2017.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THProductSizePopUp: THBaseViewController {

    @IBOutlet weak var sizeTableView: UITableView!
    var categoryDetail:ClosetCategory!
    var productVariations = [ProductVariation]()
    var delegate: SelectedSize?
   
    @IBOutlet weak var viewPopupResize: UIView!
    override func viewDidLoad() {
        sizeTableView.register(UINib(nibName: THProductVariationsTableViewCell.className, bundle: nil), forCellReuseIdentifier: THProductVariationsTableViewCell.className)
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        self.showAnimate()
        for productvariation in categoryDetail.productVariations {
            if productvariation.quantityAvailable >= 1 {
                productVariations.append(productvariation)
            }
        }
        
        viewPopupResize.layer.cornerRadius = 8
        viewPopupResize.layer.masksToBounds = true
    }
    
    @IBAction func didTapDismiss(_ sender: UIButton) {
        self.removeAnimate()
    }

}

extension THProductSizePopUp: UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productVariations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: THProductVariationsTableViewCell.className, for: indexPath) as! THProductVariationsTableViewCell
        if productVariations[indexPath.row].quantityAvailable >= 1 {
            
        }
        cell.sizeLabel.text = productVariations[indexPath.row].size
        cell.selectionStyle = .none
        return cell
    }
}

extension THProductSizePopUp: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedProduct = SelectedProduct(selectedSize: productVariations[indexPath.row].size, productIndex: indexPath.row)
        self.delegate?.selectedSize(id: self.categoryDetail.Id, product: selectedProduct)
        self.removeAnimate()
    }
}
