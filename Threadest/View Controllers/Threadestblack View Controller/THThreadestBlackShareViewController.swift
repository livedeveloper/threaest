//
//  THThreadestBlackShareViewController.swift
//  Threadest
//
//  Created by jigar on 24/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import Social
import MessageUI


class THThreadestBlackShareViewController: UIViewController {
    
    
    @IBOutlet weak var lblAheadPeople: UILabel!
    

    @IBOutlet weak var lblBehindPeople: UILabel!
    
    var subscriberInfo : SubScriber?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        lblAheadPeople.text = "\((subscriberInfo?.earlyAccessPosition)! - 1) people ahead of you"

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapTweetBtn(_ sender: Any) {
        if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter) {
            let serviceShare:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            
            shareWithSLComposer(serviceShare: serviceShare, service: "Twitter")
        }
        else{
            showAlert(withMessage: "Twitter")
        }
        
        
        }

    
    @IBAction func didTapFacebookBtn(_ sender: Any) {
        if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook) {
            let serviceShare:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            
            shareWithSLComposer(serviceShare: serviceShare, service: "Facebook")
        }
        else{
            showAlert(withMessage: "Facebook")
        }
    }
    
    @IBAction func didTapEmailBtn(_ sender: Any) {
        shareViaEmail()
        
    }
    
    @IBAction func didTapLinkedInBtn(_ sender: Any) {

    }
    
    @IBAction func didTapOnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func shareWithSLComposer(serviceShare:SLComposeViewController,service:String)
    {
        serviceShare.title="share"
        serviceShare.setInitialText(subscriberInfo?.shareUrl)
        self.present(serviceShare, animated: true, completion: nil)
    }
}

extension THThreadestBlackShareViewController:MFMailComposeViewControllerDelegate{
    
    func shareViaEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mailVC = MFMailComposeViewController()
            mailVC.mailComposeDelegate = self
            mailVC.setSubject("Share")
            mailVC.setMessageBody((subscriberInfo?.shareUrl)!, isHTML: false)
            self.present(mailVC, animated: true, completion: nil)
        } else {
            self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "", body: "Uh oh! Your Apple email isn’t configured yet. Please configure in order to continue.", completion: {
            }), animated: true, completion: nil)
        }
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?)
    {
        dismiss(animated: true, completion: nil)
    }

    
}
