//
//  THShopshowViewController.swift
//  Threadest
//
//  Created by Jaydeep on 18/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import PassKit
import Stripe
import FBSDKLoginKit
import FBSDKCoreKit
import Social
import TwitterKit
import SDWebImage
import MessageUI
import Crashlytics
import IQKeyboardManagerSwift
import Lottie
import Cheers

class THShopshowViewController: UIViewController , UIGestureRecognizerDelegate{
    var shareCode = ""
    var emailText = ""
    var passwordText = ""
    var usernameText = ""
    var phoneText = ""


    //for swipe down gesture
    public var minimumVelocityToHide = 1500 as CGFloat
    public var minimumScreenRatioToHide = 0.5 as CGFloat
    public var animationDuration = 0.2 as TimeInterval

    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var commentTextField: UITextField!

    //Constraint
    @IBOutlet weak var bottomTableViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomViewCommentConstraint: NSLayoutConstraint!

    @IBOutlet weak var commentHeightConstraint: NSLayoutConstraint!
    //variable
    var selectedndex = -1
    var postId = -1
    var descriptionDropdown = false

    var categoryDetail:ClosetCategory!
    var productVariations = [ProductVariation]()
    var suggestProduct = [ClosetCategory]()
    var isPopup = false
    var page = 1

    var isTrackingPanLocation = false
    var panGestureRecognizer : UIPanGestureRecognizer!
    var isGiveAwayTappedForTempAccount = false

    @IBOutlet weak var objTableView: UITableView!
    let animationView = LOTAnimationView(name: "exploding_stars")

    //cheers view
    let cheerView = CheerView()

    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        if categoryDetail.popup_exclusive != "" {
            cheerView.config.particle = .confetti(allowedShapes: Particle.ConfettiShape.all)
            view.addSubview(cheerView)
        }

        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(THShopshowViewController.panRecognized(recognizer:)))
        panGestureRecognizer.delegate = self
        objTableView.addGestureRecognizer(panGestureRecognizer)

        //Add funnel for visiting product detail page
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Product detail page", customAttributes: ["userid":usercurrent.userId,
                                                                                       "username": usercurrent.username, "title":categoryDetail.title, "brand name": categoryDetail.brandName, "product_id": categoryDetail.Id])
        }
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        //self.objTableView.contentInset = UIEdgeInsets(top: -20, left: 0, bottom: 0, right: 0)
        objTableView.register(UINib(nibName: THShopShowTableViewCell.className, bundle: nil), forCellReuseIdentifier: THShopShowTableViewCell.className)

        objTableView.register(UINib(nibName: THCommentTableViewCell.className, bundle: nil), forCellReuseIdentifier: THCommentTableViewCell.className)

        objTableView.register(UINib(nibName: THSignUpGiveAwayTableViewCell.className, bundle: nil), forCellReuseIdentifier: THSignUpGiveAwayTableViewCell.className)

        objTableView.register(UINib(nibName: THSuggestedProductTableViewCell.className, bundle: nil), forCellReuseIdentifier: THSuggestedProductTableViewCell.className)

            for productvariation in categoryDetail.productVariations {
                if productvariation.quantityAvailable >= 1 {
                    productVariations.append(productvariation)
                    selectedndex = 0
                }
            }


        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)

        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true

        THBrandsManager.sharedInstance.suggestionProduct = [ClosetCategory]()
        THBrandsManager.sharedInstance.retrieveSuggestionOfProduct(brandId: categoryDetail.brandId, productId: categoryDetail.Id, page: page) { (error) in
            if THBrandsManager.sharedInstance.suggestionProduct.count > 0 {
                self.page += 1
                self.suggestProduct.append(contentsOf: THBrandsManager.sharedInstance.suggestionProduct)
                THBrandsManager.sharedInstance.didLoadData = true
            }
            if let _ = self.objTableView.cellForRow(at: IndexPath(row: 0, section: 2)) as? THSuggestedProductTableViewCell {
                self.objTableView.reloadRows(at: [IndexPath(row: 0, section: 2)], with: .none)
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
//        appdelegate.verticle.scrollEnabled(isEnable: false)
        //self.tabBarController?.tabBar.isHidden = true
        IQKeyboardManager.sharedManager().enable = false
    }

    override func viewWillDisappear(_ animated: Bool) {
//        appdelegate.verticle.scrollEnabled(isEnable: true)
        IQKeyboardManager.sharedManager().enable = true
        let imageCache = SDImageCache.shared()
        imageCache.clearMemory()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        cheerView.frame = view.bounds
    }

    override func didReceiveMemoryWarning() {
        let imageCache = SDImageCache.shared()
        imageCache.clearMemory()
    }

    override func viewDidAppear(_ animated: Bool) {
        objTableView.reloadData()
        if categoryDetail.popup_exclusive != "" {
            cheerView.start()

            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                self.cheerView.stop()
            }
        }
    }

    func slideViewVerticallyTo(_ y: CGFloat) {
        self.view.frame.origin = CGPoint(x: 0, y: y)
    }

    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                break
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
                break
            default:
                break
            }
        }
    }

    public func panRecognized(recognizer:UIPanGestureRecognizer) {
        if recognizer.state == .began && objTableView.contentOffset.y == 0 {
            recognizer.setTranslation(CGPoint.zero, in : objTableView)
            isTrackingPanLocation = true
        }
        else if recognizer.state != .ended && recognizer.state != .cancelled &&
            recognizer.state != .failed && isTrackingPanLocation {
            let panOffset = recognizer.translation(in: objTableView)

            let eligiblePanOffset = panOffset.y > 200
            if eligiblePanOffset
            {
                recognizer.isEnabled = false
                recognizer.isEnabled = true
                self.dismiss(animated: true, completion: nil)
            }

            if panOffset.y < 0
            {
                isTrackingPanLocation = false
            }
        }
        else
        {
            isTrackingPanLocation = false
        }
    }

    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,
                                  shouldRecognizeSimultaneouslyWith
        otherGestureRecognizer : UIGestureRecognizer)->Bool
    {
        return true
    }



    private func initUI(){
        if isPopup {

        } else {

        }
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(THShopshowViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        objTableView.addGestureRecognizer(tap)

        let notiCenter = NotificationCenter.default
        notiCenter.addObserver(self, selector: #selector(THCommentViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        notiCenter.addObserver(self, selector: #selector(THCommentViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    func loadDoneAnimatedButton(fileName:String) {
        let animationDoneView = LOTAnimationView(name: fileName)
        animationDoneView.animationProgress = 100
        animationDoneView.backgroundColor = UIColor.clear
        animationDoneView.frame = CGRect(x: 0, y: 0, width: 400, height: 400)
        animationDoneView.center = view.center
        animationDoneView.contentMode = .scaleAspectFill
        animationDoneView.animationSpeed = 0.8
        view.addSubview(animationDoneView)
        animationDoneView.loopAnimation = false
        animationDoneView.play { (sucess) in
            animationDoneView.pause()
            animationDoneView.removeFromSuperview()
        }
    }



    override func dismissKeyboard() {
        view.endEditing(true)
    }



    //MARK: All Action methods
    @IBAction func didTapBackbutton(_ sender: Any) {
        if isPopup {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }

    @IBAction func didTapOnLikeButton(_ sender: UIButton) {
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }
        let shopCell = objTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! THShopShowTableViewCell
        if !shopCell.likeButton.isSelected {
            sender.isSelected = true
            let animationLikeView = LOTAnimationView(name: "like")
            animationLikeView.animationProgress = 100
            animationLikeView.center = shopCell.objScrollView.center
            animationLikeView.frame = shopCell.objScrollView.frame
            animationLikeView.backgroundColor = UIColor.clear
            animationLikeView.contentMode = .scaleAspectFill
            animationLikeView.animationSpeed = 2
            shopCell.objScrollView.addSubview(animationLikeView)
            animationLikeView.loopAnimation = false
            
            animationLikeView.play { (sucess) in
                animationLikeView.pause()
                animationLikeView.removeFromSuperview()
                
                shopCell.scoreButton.backgroundColor = UIColor.lightGray
                UIView.animate(withDuration: 0.6,
                               animations: {
                                shopCell.scoreButton.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
                },
                               completion: { _ in
                                shopCell.scoreButton.backgroundColor = UIColor.hexa("178dcd", alpha: 1)
                                UIView.animate(withDuration: 0.6) {
                                    shopCell.scoreButton.transform = CGAffineTransform.identity
                                }
                })
                
            }
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                THUserManager.sharedInstance.performUpvote(for: usercurrent.userId, votableId: self.categoryDetail.Id, votableType: "Product", completion: { (vote, error) in
                    guard let error = error else {
                        if let i = THProfileClosetManager.sharedInstance.closetCategory.index(where: {$0.Id == self.categoryDetail.Id}) {
                            THProfileClosetManager.sharedInstance.closetCategory[i].votesCount = self.categoryDetail.votesCount + 1
                            THProfileClosetManager.sharedInstance.closetCategory[i].likedByCurrentUser = true
                        }
                        if let featureIndex = THFeaturedFeedCategoryManager.sharedInstance.shopCategory.index(where: {$0.Id == self.categoryDetail.Id}) {
                            THFeaturedFeedCategoryManager.sharedInstance.shopCategory[featureIndex].votesCount = self.categoryDetail.votesCount + 1
                            THFeaturedFeedCategoryManager.sharedInstance.shopCategory[featureIndex].likedByCurrentUser = true
                        }
                        if let recommondationIndex = THProfileClosetManager.sharedInstance.closetRecommendations.index(where: {$0.Id == self.categoryDetail.Id}) {
                            THProfileClosetManager.sharedInstance.closetRecommendations[recommondationIndex].votesCount = self.categoryDetail.votesCount + 1
                            THProfileClosetManager.sharedInstance.closetRecommendations[recommondationIndex].likedByCurrentUser = true
                        }
                        shopCell.likeButton.isSelected  = true
                        //add funnel for product like
                        Answers.logCustomEvent(withName: "Like product", customAttributes: ["userid":usercurrent.userId,"username": usercurrent.username, "product_Id":"\(self.categoryDetail.Id)"])
                        return
                    }
                    debugPrint(error)
                })
            }
            
            
           /* UIView.animate(withDuration: 0.6,
                           animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
            },
                           completion: { _ in
                            sender.isSelected = true
                            UIView.animate(withDuration: 0.6) {
                                sender.transform = CGAffineTransform.identity
                                
                            }
                            
            })*/
            
            
            
            
            /*sender.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                sender.transform = CGAffineTransform(rotationAngle: CGFloat(-179.9).degreesToRadians)
            })
            UIView.animate(withDuration: 0.3, delay: 0.2, options: UIViewAnimationOptions(), animations: {
                sender.transform = CGAffineTransform(rotationAngle: CGFloat(1.1).degreesToRadians)
            }, completion: { (completed) in
                
                else {

                }
            })*/
        }
    }

    @IBAction func didTapAddtoCart(_ sender: UIButton) {
        let productVar = self.categoryDetail!.productVariations[self.selectedndex]
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THCartManager.sharedInstance.addToCart(with: productVar.productId, productVariationId: productVar.id, postId: postId, shareCode: shareCode) { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            if let error = error {
                self.showAlert(withMessage: error.localizedDescription)
            }else {
                self.objTableView.reloadData()
                //Add to cart Funnel
                let productPrice = self.categoryDetail.salePrice == 0.0 ? self.categoryDetail.defaultPrice: self.categoryDetail.salePrice
                Answers.logAddToCart(withPrice: NSDecimalNumber(value: productPrice), currency: "USD", itemName: self.categoryDetail.title, itemType: self.categoryDetail.brandName, itemId: "\(self.categoryDetail.Id)", customAttributes: ["description":self.categoryDetail.description])
               /* self.showAlert(title: "Success", message: "Successfully add \(self.categoryDetail!.title) to cart", okClick: {
                  //  self.navigationController!.popViewController(animated: true)
                })*/
                self.loadDoneAnimatedButton(fileName: "done_button")
            }
        }
    }

    @IBAction func didTapShoppingcartbutton(_ sender: Any){
        let shoppingcartController = THYourCartViewController(nibName: THYourCartViewController.className, bundle: nil)
        self.navigationController?.pushViewController(shoppingcartController, animated: true)
    }


    @IBAction func didTapApplePay(_ sender: UIButton) {
       /* if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }*/
        let paymentNetworks = [PKPaymentNetwork.discover, .masterCard, .amex, .visa]
        if PKPassLibrary.isPassLibraryAvailable() {
            if !PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetworks) {
                self.present(GeneralMethods.sharedInstance.AlertviewGeneral(title: "Add a Credit Card to Wallet", body: "Would you like to add a credit card to your wallet now?", cancelbutton: "No", okbutton: "Yes", completion: { () in
                    let lib = PKPassLibrary()
                    lib.openPaymentSetup()
                }), animated: true, completion: nil)
            }
        }
        let paymentRequest = Stripe.paymentRequest(withMerchantIdentifier: STPPaymentConfiguration.shared().appleMerchantIdentifier!)

        paymentRequest.supportedNetworks = paymentNetworks
        paymentRequest.requiredShippingAddressFields = [.all ]

        let price = self.categoryDetail.salePrice == 0.0 ? self.categoryDetail.defaultPrice:self.categoryDetail.salePrice
        let payTarget = PKPaymentSummaryItem(label: "THREADEST INC.", amount: NSDecimalNumber(value: price))

        paymentRequest.paymentSummaryItems = [payTarget]

        if  Stripe.canSubmitPaymentRequest(paymentRequest) {
            let paymentAuthorizationVC = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest)
            paymentAuthorizationVC.delegate = self
            self.present(paymentAuthorizationVC, animated: true, completion: nil)
            //Add funnel for Apple pay button
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Apple Pay Button", customAttributes: ["userid":usercurrent.userId,
                                                                                        "username": usercurrent.username, "Price":price])
            }
        } else {
            // there is a problem with your Apple Pay configuration.
        }
    }

    @IBAction func didTapOnCommentButton(_ sender: Any) {
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }
        if commentTextField.text == "" {
            return
        }

        var tempuserid = -1
        if let currentUser = THUserManager.sharedInstance.currentUserdata() {
            tempuserid = currentUser.userId
            Answers.logCustomEvent(withName: "Comment product", customAttributes: ["userid":currentUser.userId, "username": currentUser.username, "product_id" : categoryDetail.Id, "comment" : commentTextField.text!])
        }
        let postProductComment = PostProductComment(comment: commentTextField.text!, brandId: categoryDetail.brandId, productId: categoryDetail.Id, userId: tempuserid, commentTitle: "Product")
        commentTextField.text = ""
        THCommentManager.sharedInstance.performPostProductComment(with: postProductComment) { (comment, error) in
            guard let error = error else {
                if let i = THProfileClosetManager.sharedInstance.closetCategory.index(where: {$0.Id == self.categoryDetail.Id}) {
                    THProfileClosetManager.sharedInstance.closetCategory[i].comments.append(comment!)
                    THProfileClosetManager.sharedInstance.closetCategory[i].commentsCount = self.categoryDetail.commentsCount + 1
                }
                if let featureIndex = THFeaturedFeedCategoryManager.sharedInstance.shopCategory.index(where: {$0.Id == self.categoryDetail.Id}) {
                    THFeaturedFeedCategoryManager.sharedInstance.shopCategory[featureIndex].comments.append(comment!)
                    THFeaturedFeedCategoryManager.sharedInstance.shopCategory[featureIndex].commentsCount = self.categoryDetail.commentsCount + 1
                }
                if let recommondationIndex = THProfileClosetManager.sharedInstance.closetRecommendations.index(where: {$0.Id == self.categoryDetail.Id}) {
                    THProfileClosetManager.sharedInstance.closetRecommendations[recommondationIndex].comments.append(comment!)
                    THProfileClosetManager.sharedInstance.closetRecommendations[recommondationIndex].commentsCount = self.categoryDetail.commentsCount + 1
                }
                //let shopCell = self.objTableView.dequeueReusableCell(withIdentifier: THShopShowTableViewCell.className, for: IndexPath(row: 0, section: 0)) as! THShopShowTableViewCell
                //shopCell.commentCountLabel.isHidden = false
                self.categoryDetail.commentsCount = self.categoryDetail.commentsCount + 1
                self.categoryDetail.comments.append(comment!)
                self.objTableView.beginUpdates()
                self.objTableView.reloadRows(at: [IndexPath(row: 0, section: 0), IndexPath(row: 0, section: 2)], with: .none)
                self.objTableView.insertRows(at: [IndexPath(row: self.categoryDetail.comments.count - 1, section: 1)], with: .fade)
                self.objTableView.endUpdates()
                self.objTableView.scrollToRow(at: IndexPath(row: self.categoryDetail.comments.count - 1, section: 1), at: UITableViewScrollPosition.bottom, animated: true)
                self.view.endEditing(true)
                UIView.animate(withDuration: 0.6, animations: {
                    self.commentView.alpha = 0
                }, completion: { (complete) in
                    self.commentView.isHidden = true
                })
                return
            }
            debugPrint(error)
        }
    }

    @IBAction func  didTapNotifyMeButton(_ sender: UIButton) {
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THUserManager.sharedInstance.performNotifyWhenStockAvailable(with: self.categoryDetail.brandId, productId: self.categoryDetail.Id) { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            if error == nil {
                self.loadDoneAnimatedButton(fileName: "done_button")
            }
        }
    }

    @IBAction func didTapOnScoreButton(_ sender: UIButton) {
        guard let usercurrent = THUserManager.sharedInstance.currentUserdata() else {
            return
        }

        if usercurrent.tempAccount {
            appdelegate.setupAccountPopup(viewController: self)
            return
        }
        let text = "https://www.threadest.com/products/\(categoryDetail.Id)?b=\(categoryDetail.brandId)&sc=\(usercurrent.sharePostCode)"
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash

        // exclude some activity types from the list (optional)
        //activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook, UIActivityType.copyToPasteboard, UIActivityType.mail, UIActivityType.message, UIActivityType.postToTwitter]

        // present the view controller
        DispatchQueue.main.async {
            self.present(activityViewController, animated: true, completion: nil)
        }
         /*animationView.frame = CGRect(x: 0, y: 0, width: 400, height: 400)
         animationView.center = self.view.center
         animationView.contentMode = .scaleAspectFill
         animationView.animationSpeed = 0.5
         view.addSubview(animationView)
        animationView.loopAnimation = false
        animationView.play(fromProgress: 0, toProgress: 0.60) { (sucess) in
            self.animationView.pause()
            self.animationView.animationProgress = 0
            self.animationView.removeFromSuperview()
            let statuspopup = THStatusPopup(nibName: THStatusPopup.className, bundle: nil)
            statuspopup.closetCategory = self.categoryDetail
            self.addChildViewController(statuspopup)
            statuspopup.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
            self.view.addSubview(statuspopup.view)
            statuspopup.didMove(toParentViewController: self)
        }*/
    }

  /*  @IBAction func didTapOnRethreadButton(_ sender: UIButton) {
        guard let usercurrent = THUserManager.sharedInstance.currentUserdata() else {
            return
        }

        if usercurrent.tempAccount {
            appdelegate.setupAccountPopup(viewController: self)
            return
        }

        let cell = objTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as!THShopShowTableViewCell
        if !cell.rethreadButton.isSelected {
            cell.rethreadButton.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                cell.rethreadButton.transform = CGAffineTransform(rotationAngle: CGFloat(-179.9).degreesToRadians)
            })
            UIView.animate(withDuration: 0.3, delay: 0.2, options: UIViewAnimationOptions(), animations: {
                cell.rethreadButton.transform = CGAffineTransform(rotationAngle: CGFloat(1.1).degreesToRadians)
            }, completion: { (completed) in
                let rethreadpopup = THRethreadPopupViewController(nibName: THRethreadPopupViewController.className, bundle: nil)
                rethreadpopup.productLinkString = "https://www.threadest.com/products/\(self.categoryDetail.Id)?b=\(self.categoryDetail.brandId)&sc=\(usercurrent.sharePostCode)"
                self.addChildViewController(rethreadpopup)
                rethreadpopup.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
                self.view.addSubview(rethreadpopup.view)
                rethreadpopup.didMove(toParentViewController: self)
                rethreadpopup.completionHander = {selectedRethreadArray in
                    for selectedRethread in selectedRethreadArray! {
                        switch selectedRethread as! String {
                        case "Threadest":
                            self.rethreadPost()
                            break
                        case "Facebook":
                            self.rethreadPostOnFacebook()
                            break
                        case "Twitter":
                            self.rethreadPostOnTwitter(closetCategory:self.categoryDetail)
                            break
                        default:
                            break
                        }
                    }
                }
            })
        } else {

        }
    }*/

    @IBAction func didTapOnDescriptionDropDown(_ sender: UIButton) {
        //sender.isSelected = !sender.isSelected
        descriptionDropdown = !descriptionDropdown
        self.objTableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
    }

    @IBAction func didTapOnEmailButton(_ sender: UIButton) {
        if MFMailComposeViewController.canSendMail() {
            let mailComposerVC = MFMailComposeViewController()
            mailComposerVC.mailComposeDelegate = self
            mailComposerVC.setToRecipients(["hello@threadest.com"])
            mailComposerVC.setSubject("Threadest Inquiry")
            mailComposerVC.setMessageBody("Hi, I'm viewing \(categoryDetail.title) and have a question...", isHTML: false)
            self.present(mailComposerVC, animated: true, completion: nil)
        } else {
            self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "", body: "Uh oh! Your Apple email isn’t configured yet. Please configure in order to continue.", completion: {
            }), animated: true, completion: nil)
        }

    }

    @IBAction func didTapOnCommentButtonFromCell(_sender: UIButton) {
        self.commentView.alpha = 0
        self.commentView.isHidden = true
        UIView.animate(withDuration: 0.6) {
            self.commentView.alpha = 1
            self.commentView.isHidden = false
            self.commentHeightConstraint.constant = 50
        }
    }

    @IBAction func didTapOnDownArrow(_sender: UIButton) {
        let indexPath = IndexPath(row: 0, section: 2)
        self.objTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
    }

    func clearTextField() {
        usernameText = ""
        emailText = ""
        passwordText = ""
        phoneText = ""
    }

     @IBAction func didTapOnSignupEmailGiveAwayButton(_sender: UIButton) {
        guard validateFields() else {
            return
        }
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            var user: User = usercurrent
            user.email = emailText
            user.password = passwordText
            user.username = usernameText
            user.phoneNumber = phoneText
            user.tempAccount = false
            THUserManager.sharedInstance.performUpdateUserAccount(for: usercurrent.userId, user: user, completion: { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                self.clearTextField()
                self.isGiveAwayTappedForTempAccount = false
                self.objTableView.reloadData()
            })
        }
    }

     @IBAction func didTapOnFacebookSignupGiveAwayButton(_sender: UIButton) {
        THUserManager.sharedInstance.performFacebookAuth(forViewController: self) { (userExists, authToken, email, userId, gender, error) in
            if let error = error {
                if userId != "" && gender != "" && authToken != "" {
                    let alertController = UIAlertController(title: "Email", message: "Please enter your email:", preferredStyle: .alert)

                    let confirmAction = UIAlertAction(title: "Confirm", style: .default) { (_) in
                        if let field = alertController.textFields![0] as? UITextField {
                            // store your data
                            guard let emailAddress = field.text, emailAddress.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
                                return
                            }

                            self.checkUserAlreadyExisted(token: authToken, userid: userId, emailAddress: emailAddress, gender: gender)
                        } else {
                            // user did not fill field
                        }
                    }

                    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }

                    alertController.addTextField { (textField) in
                        textField.placeholder = "Email"
                    }

                    alertController.addAction(confirmAction)
                    alertController.addAction(cancelAction)

                    self.present(alertController, animated: true, completion: nil)

                } else {
                    let alertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)

                    alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                }

            } else {
                if let userExists = userExists {
                    if userExists {
                        self.clearTextField()
                        self.isGiveAwayTappedForTempAccount = false
                        self.objTableView.reloadData()
                    } else {
                        self.showEmailSignUpController(usingFacebookAuth: true, authToken: authToken, emailAddress: email, userId: userId, gender: gender)
                    }
                } else {
                    assertionFailure("We should never get here")
                }
            }
        }
    }

    func showEmailSignUpController(usingFacebookAuth shouldUseFacebookAuth: Bool, authToken: String = "", emailAddress: String = "", userId: String = "", gender: String = "") {
        let emailSignUpController = THEmailSignUpViewController(nibName: THEmailSignUpViewController.className, bundle: nil)

        emailSignUpController.shouldUseFacebookAuth = shouldUseFacebookAuth
        emailSignUpController.facebookAuthToken = authToken
        emailSignUpController.facebookEmailAddress = emailAddress
        emailSignUpController.facebookUserID = userId
        emailSignUpController.facebookGender = gender
        emailSignUpController.isFromTempAccount = true
        navigationController?.pushViewController(emailSignUpController, animated: true)
    }

    func checkUserAlreadyExisted(token: String, userid: String, emailAddress: String, gender: String) {
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THUserManager.sharedInstance.performCheckIfAuthUserExists(with: emailAddress, authToken: token, userID: userid, completion: { (userExists, err) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            // Request fails
            if let err = err {
                return
            }

            // Request succeeds
            if let userExists = userExists {
                if userExists {
                    self.clearTextField()
                    self.isGiveAwayTappedForTempAccount = false
                    self.objTableView.reloadData()
                } else {
                    // Redirect to register
                    self.showEmailSignUpController(usingFacebookAuth: true, authToken: token, emailAddress: emailAddress, userId: userid, gender: gender)
                }
            } else {

            }
        })
    }


    // for Rethread post Threadest
    func rethreadPost() {
        THSocialInteractionPostsManager.sharedInstance.performRethreadPost(for: categoryDetail.Id) { (error) in
            guard let error = error else {
                /*let socialCell = self.objTableView.cellForRow(at: IndexPath(row: selectedRow, section: 1)) as! THShareDetailTableViewCell

                 socialCell.rethreadCountLabel.text = "\(socialInteraction.rethreadCount + 1)"
                 THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[selectedRow].rethreadCount = socialInteraction.rethreadCount + 1*/
                return
            }
            debugPrint(error)
        }
    }

    @IBAction func didTapOnGiveAwayButton(_ sender: UIButton) {

        guard let usercurrent = THUserManager.sharedInstance.currentUserdata() else {
            return
        }

        Answers.logCustomEvent(withName: "Giveaway Button", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "From": "Product Detail Page"])

        if usercurrent.tempAccount {
            if isGiveAwayTappedForTempAccount == true {
                return
            }
            isGiveAwayTappedForTempAccount = true
            objTableView.beginUpdates()
            objTableView.insertRows(at: [IndexPath(row: 1, section: 0)], with: .bottom)
            objTableView.endUpdates()
            objTableView.scrollToRow(at: IndexPath(row: 1, section: 0), at: .bottom, animated: true)
            return
        }

        let enterGiveAwayPopup = THEnterGiveawayPopupVC(nibName: THEnterGiveawayPopupVC.className, bundle: nil)
        enterGiveAwayPopup.isGiveAwayPopupClose = {
            if self.categoryDetail.winner_ids.contains(usercurrent.userId) {
                //call GiveAway api
                THBrandProductManager.sharedInstance.performGiveAwayContestants(with: self.categoryDetail.brandId, productId: self.categoryDetail.Id, completion: { (error) in
                    
                })
                
                self.cheerView.config.particle = .confetti(allowedShapes: Particle.ConfettiShape.all)
                self.view.addSubview(self.cheerView)
                self.cheerView.start()

                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    let claimPopup = THClaimPriceViewController(nibName: THClaimPriceViewController.className, bundle: nil)
                    claimPopup.closetCategory = self.categoryDetail
                    claimPopup.isClaimPopupClose = {
                        self.cheerView.stop()
                        self.cheerView.removeFromSuperview()
                    }
                    self.addChildViewController(claimPopup)
                    claimPopup.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
                    self.view.addSubview(claimPopup.view)
                    claimPopup.didMove(toParentViewController: self)
                }
            }
        }
        self.addChildViewController(enterGiveAwayPopup)
        enterGiveAwayPopup.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
        self.view.addSubview(enterGiveAwayPopup.view)
        enterGiveAwayPopup.didMove(toParentViewController: self)
    }


    // for Rethread post on Facebook
    func rethreadPostOnFacebook() {
        guard let usercurrent = THUserManager.sharedInstance.currentUserdata() else {
            return
        }
        if FBSDKAccessToken.current() != nil {
            if FBSDKAccessToken.current().hasGranted("publish_actions") {
                GeneralMethods.sharedInstance.publishMessage(message: "https://www.threadest.com/products/\(self.categoryDetail.Id)?b=\(self.categoryDetail.brandId)&sc=\(usercurrent.sharePostCode)", link: self.categoryDetail.defaultImageUrl, completion: { () in
                    GeneralMethods.sharedInstance.RethreadPostSucessPopup(message: "Post to Facebook - Successful", viewController: self)
                })
            } else {
                requestPublishPermissions(closetCategory:self.categoryDetail)
            }
        } else {
            requestPublishPermissions(closetCategory:self.categoryDetail)
        }
    }

    func requestPublishPermissions(closetCategory: ClosetCategory) {
        guard let usercurrent = THUserManager.sharedInstance.currentUserdata() else {
            return
        }
        let login: FBSDKLoginManager = FBSDKLoginManager()
        login.logIn(withPublishPermissions: ["publish_actions"], from: self) { (result, error) in
            if (error != nil) {
                print(error!)
            } else if (result?.isCancelled)! {
                print("Canceled")
            } else if (result?.grantedPermissions.contains("publish_actions"))! {
                print("permissions granted")
                GeneralMethods.sharedInstance.publishMessage(message: "https://www.threadest.com/products/\(closetCategory.Id)?b=\(closetCategory.brandId)&sc=\(usercurrent.sharePostCode)", link: closetCategory.defaultImageUrl, completion: { () in
                    GeneralMethods.sharedInstance.RethreadPostSucessPopup(message: "Post to Facebook - Successful", viewController: self)
                })
            }
        }
    }


    func rethreadPostOnTwitter(closetCategory: ClosetCategory) {
        guard let usercurrent = THUserManager.sharedInstance.currentUserdata() else {
            return
        }
        if let twitterAccountId = TWTRTwitter.sharedInstance().sessionStore.session() {
            GeneralMethods.sharedInstance.uploadTwitterMedia(viewController: self, imageURL: closetCategory.defaultImageUrl, status: "https://www.threadest.com/products/\(closetCategory.Id)?b=\(closetCategory.brandId)&sc=\(usercurrent.sharePostCode)", token: twitterAccountId.authToken, userId: twitterAccountId.userID, id: closetCategory.Id, type: "Product")
        } else {
            // Not logged in
            TWTRTwitter.sharedInstance().logIn { (session, error) in
                if let _ = session {
                    if let twitterAccountId = TWTRTwitter.sharedInstance().sessionStore.session() {
                        GeneralMethods.sharedInstance.uploadTwitterMedia(viewController: self, imageURL: closetCategory.defaultImageUrl, status: "https://www.threadest.com/products/\(closetCategory.Id)?b=\(closetCategory.brandId)&sc=\(usercurrent.sharePostCode)", token: twitterAccountId.authToken, userId: twitterAccountId.userID, id: closetCategory.Id, type: "Product")
                    }
                } else {
                    print("error: \(String(describing: error?.localizedDescription))");
                }
            }
        }
    }


    //MARK:- Show/HideKeyboard
    func keyboardWillShow(_ notification: Notification) {
        DispatchQueue.main.async {
            let info = notification.userInfo! as Dictionary
            let floatAnimationDuration:TimeInterval = info[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval

            UIView.animate(withDuration: floatAnimationDuration, animations: {
                if let keyboardRectValue = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                    self.bottomViewCommentConstraint.constant = keyboardRectValue.height
                   // self.bottomTableViewConstraint.constant = keyboardRectValue.height + self.commentView.frame.size.height
                    self.view.layoutIfNeeded()
                    self.objTableView.beginUpdates()
                    if THCommentManager.sharedInstance.comments.count > 0 {
                        self.objTableView.scrollToRow(at: IndexPath(row: THCommentManager.sharedInstance.comments.count - 1, section: 1), at: .bottom, animated: true)
                    }
                    self.objTableView.endUpdates()
                }
            })
        }
    }

    func keyboardWillHide(_ notification: Notification) {
        DispatchQueue.main.async {
            let info = notification.userInfo! as Dictionary
            let floatAnimationDuration:TimeInterval = info[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval

            UIView.animate(withDuration: floatAnimationDuration, animations: {
                self.bottomViewCommentConstraint.constant = 0
                self.bottomTableViewConstraint.constant = 0
                self.view.layoutIfNeeded()
                self.objTableView.beginUpdates()
                self.objTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                self.objTableView.setContentOffset(CGPoint.zero, animated: true)
                self.objTableView.endUpdates()
            })
        }
    }

    func imageTappedOnBrandLogoImage(tapGestureRecognizer: UITapGestureRecognizer) {
        let brandProfileVC:THBrandProfileViewController = THBrandProfileViewController()
        brandProfileVC.isFrom = "brandlogo"
        brandProfileVC.brandId = categoryDetail.brandId
        self.navigationController?.pushViewController(brandProfileVC, animated: true)
    }

    func imageTappedOnScrollImage(tapGestureRecognizer: UITapGestureRecognizer) {
        let fullScreenVC = THFullScreenImageViewController(nibName: THFullScreenImageViewController.className, bundle: nil)
        fullScreenVC.categoryDetail = categoryDetail
        self.navigationController?.pushViewController(fullScreenVC, animated: true)
    }

    fileprivate func validateFields() -> Bool {
        guard emailText.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
            showAlert(withMessage: "Please insert an email!")

            return false
        }

        guard emailText.isValidEmail() else {
            showAlert(withMessage: "Please insert a valid email!")

            return false
        }

        guard passwordText.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
            showAlert(withMessage: "Please insert a password!")

            return false
        }

        guard usernameText.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
            showAlert(withMessage: "Please insert a username!")

            return false
        }

        guard phoneText.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
            showAlert(withMessage: "Please insert a phone!")

            return false
        }

        return true
    }
}


extension THShopshowViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView.tag == 2 {
            if indexPath.item == self.suggestProduct.count - 1 {
                if THBrandsManager.sharedInstance.didLoadData {
                    THBrandsManager.sharedInstance.didLoadData = false
                    THBrandsManager.sharedInstance.retrieveSuggestionOfProduct(brandId: categoryDetail.brandId, productId: categoryDetail.Id, page: self.page, completion: { (error) in
                        if THBrandsManager.sharedInstance.suggestionProduct.count > 0 {
                            self.page += 1
                            self.suggestProduct.append(contentsOf: THBrandsManager.sharedInstance.suggestionProduct)
                            THBrandsManager.sharedInstance.didLoadData = true
                            if let cell = self.objTableView.cellForRow(at: IndexPath(row: 0, section: 2)) as? THSuggestedProductTableViewCell {
                                cell.objSuggestedCollectionView.reloadData()
                            }
                        }
                    })
                }
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 1 {
            let shopCell = objTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! THShopShowTableViewCell
            self.selectedndex = indexPath.row
            shopCell.addToCardButton.isEnabled = true
            shopCell.applePayButton.isEnabled = true
            shopCell.objCollectionView.reloadData()
        } else {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THBrandProductManager.sharedInstance.retrieveBrandProfile(with: self.suggestProduct[indexPath.row].brandId, productId: self.suggestProduct[indexPath.row].Id, geofence: false) { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                let shopshowcontroller = THShopshowViewController(nibName: THShopshowViewController.className, bundle: nil)
                shopshowcontroller.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                self.navigationController?.pushViewController(shopshowcontroller, animated: true)
            }
        }

    }
}

extension THShopshowViewController: UICollectionViewDataSource, CHTCollectionViewDelegateWaterfallLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView.tag == 1 ? productVariations.count: self.suggestProduct.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: THSizeCollectionViewCell.className, for: indexPath) as! THSizeCollectionViewCell
            cell.sizeLabel.text = productVariations[indexPath.row].size
            if selectedndex == indexPath.row {
                cell.rightImage.isHidden = false
                cell.sizeLabel.alpha = 1
                cell.sizeLabel.layer.cornerRadius = 20
                cell.sizeLabel.layer.borderWidth = 2
                cell.sizeLabel.font = UIFont(name: "SFUIText-Semibold", size: 16)
                cell.sizeLabel.textColor = UIColor(red: 0/255.0, green: 138/255.0, blue: 207/255.0, alpha: 1)
                cell.sizeLabel.layer.borderColor = UIColor(red: 0/255.0, green: 138/255.0, blue: 207/255.0, alpha: 1).cgColor
                cell.sizeLabel.layer.masksToBounds  = true
            } else {
                cell.rightImage.isHidden = true
                cell.sizeLabel.alpha = 0.5
                cell.sizeLabel.layer.cornerRadius = 20
                cell.sizeLabel.layer.borderWidth = 2
                cell.sizeLabel.textColor = UIColor(red: 75/255.0, green: 75/255.0, blue: 75/255.0, alpha: 1)
                cell.sizeLabel.font = UIFont(name: "SFUIText-Regular", size: 16)
                cell.sizeLabel.layer.borderColor = UIColor(red: 74/255.0, green: 74/255.0, blue: 74/255.0, alpha: 1).cgColor
                cell.sizeLabel.layer.masksToBounds  = true
            }

            return cell
        } else {
            let suggestionCell = collectionView.dequeueReusableCell(withReuseIdentifier: THSuggestionProductCollectionViewCell.className, for: indexPath) as! THSuggestionProductCollectionViewCell
            suggestionCell.configure(closetCategory: self.suggestProduct[indexPath.row])
            return suggestionCell
        }

    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 1 {
            let stringProductVariable = productVariations[indexPath.row].size as NSString
            let sringWidth = stringProductVariable.size(attributes: [NSFontAttributeName: UIFont.AppFontSemiBold(16)])
            return CGSize(width: sringWidth.width + 55, height: 40)
        } else {
            let nbCol = 2
            let flowLayout = collectionViewLayout as! CHTCollectionViewWaterfallLayout
            let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(nbCol - 1))
            let size = (collectionView.bounds.width - totalSpace) / CGFloat(nbCol)
            let imgHeight = CGFloat(self.suggestProduct[indexPath.row].defaultMediumImageHeight)
            let imgWidth = CGFloat(self.suggestProduct[indexPath.row].defaultMediumImageWidth)
            let newHeight = (imgHeight/imgWidth)*size
            return CGSize(width: size, height: newHeight+63)
        }
    }

    func colletionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, heightForHeaderInSection section: NSInteger) -> CGFloat {
        return 0
    }


}

extension THShopshowViewController: UITableViewDelegate {

}

extension THShopshowViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if categoryDetail.product_type == "Giveaway" {
            return 1
        }
        return 3
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if isGiveAwayTappedForTempAccount {
                return 2
            }
            return 1
        case 1:
            return categoryDetail.comments.count
        case 2:
            return 1
        default:
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            if indexPath.row == 0 {
                let shopshowCell = tableView.dequeueReusableCell(withIdentifier: THShopShowTableViewCell.className, for: indexPath) as! THShopShowTableViewCell
                shopshowCell.selectionStyle = UITableViewCellSelectionStyle.none
                shopshowCell.configure(categoryDetail: categoryDetail)
                shopshowCell.giveAwayButton.isHidden = categoryDetail.product_type == "Giveaway" ? false:true
                if categoryDetail.product_type == "Giveaway" {
                    shopshowCell.giveAwayButton.setTitle(categoryDetail.giveaway_button_text, for: .normal)
                    shopshowCell.bottomArrowBottomConstraint.constant = -150
                    shopshowCell.descriptionTopConstraint.constant = -21
                    //shopshowCell.descriptionLabel.isHidden = true
                    //shopshowCell.descriptionDropDownButton.isHidden = true
                    shopshowCell.objCollectionView.isHidden = true
                    shopshowCell.addToCardButton.isHidden = true
                    shopshowCell.applePaymentButton.isHidden = true
                    shopshowCell.applePayButton.isHidden = true
                    shopshowCell.arrowDownButton.isHidden = true
                    shopshowCell.soldOutLabel.isHidden = true
                    shopshowCell.btnNotifyMe.isHidden = true
                    shopshowCell.fullFilledByLabel.isHidden = true
                    shopshowCell.priceLabel.isHidden = true
                    shopshowCell.truckImage.isHidden = true
                    shopshowCell.chooseYourSizeLabel.isHidden = true
                    shopshowCell.freeShippingLabel.isHidden = true
                    shopshowCell.viewBottom.isHidden = true
                }
                shopshowCell.fullFilledByLabel.text = "Fulfilled by: \(categoryDetail.brandName)"
                shopshowCell.fullFilledByLabel.setLinksForSubstrings(["\(categoryDetail.brandName)"]) { (label, str) in
                    let brandProfileVC:THBrandProfileViewController = THBrandProfileViewController()
                    brandProfileVC.isFrom = "brandlogo"
                    brandProfileVC.brandId = self.categoryDetail.brandId
                    self.navigationController?.pushViewController(brandProfileVC, animated: true)
                }
                shopshowCell.descriptionDropDownButton.isSelected = !descriptionDropdown
                shopshowCell.descriptionLabel.numberOfLines = descriptionDropdown ? 0 : 1
                shopshowCell.descriptionLabel.text = categoryDetail.description
                if isPopup {
                    shopshowCell.backButton.setImage(UIImage(named: "arrow_blue_down"), for: .normal)
                } else {
                    shopshowCell.backButton.setImage(UIImage(named: "cart_blue_back"), for: .normal)
                }
                shopshowCell.backButton.addTarget(self, action: #selector(THShopshowViewController.didTapBackbutton(_:)), for: UIControlEvents.touchUpInside)
                shopshowCell.objCollectionView.delegate = self
                shopshowCell.objCollectionView.dataSource = self

                shopshowCell.scoreButton.addTarget(self, action: #selector(THShopshowViewController.didTapOnScoreButton(_:)), for: .touchUpInside)
                shopshowCell.addToCardButton.addTarget(self, action: #selector(THShopshowViewController.didTapAddtoCart(_:)), for: UIControlEvents.touchUpInside)
                shopshowCell.giveAwayButton.addTarget(self, action: #selector(THShopshowViewController.didTapOnGiveAwayButton(_:)), for: .touchUpInside)
                shopshowCell.applePaymentButton.addTarget(self, action: #selector(THShopshowViewController.didTapApplePay(_:)), for: UIControlEvents.touchUpInside)
                shopshowCell.likeButton.addTarget(self, action: #selector(THShopshowViewController.didTapOnLikeButton(_:)), for: UIControlEvents.touchUpInside)
                shopshowCell.cartButton.addTarget(self, action: #selector(THShopshowViewController.didTapShoppingcartbutton(_:)), for: .touchUpInside)
                //shopshowCell.rethreadButton.tag = indexPath.row
                //shopshowCell.rethreadButton.addTarget(self, action: #selector(THShopshowViewController.didTapOnRethreadButton(_:)), for: .touchUpInside)
                let tapGestureRecognizerFullFill = UITapGestureRecognizer(target: self, action: #selector(imageTappedOnBrandLogoImage(tapGestureRecognizer:)))
                shopshowCell.fullFilledByLabel.isUserInteractionEnabled = true
                shopshowCell.fullFilledByLabel.addGestureRecognizer(tapGestureRecognizerFullFill)

                shopshowCell.contactButton.addTarget(self, action: #selector(THShopshowViewController.didTapOnEmailButton(_:)), for: .touchUpInside)
                shopshowCell.descriptionDropDownButton.addTarget(self, action: #selector(THShopshowViewController.didTapOnDescriptionDropDown(_:)), for: .touchUpInside)
                shopshowCell.commentsButton.addTarget(self, action: #selector(THShopshowViewController.didTapOnCommentButtonFromCell(_sender:)), for: .touchUpInside)
                shopshowCell.arrowDownButton.addTarget(self, action: #selector(THShopshowViewController.didTapOnDownArrow(_sender:)), for: .touchUpInside)

                let tapGestureRecognizerBrandLogo = UITapGestureRecognizer(target: self, action: #selector(imageTappedOnBrandLogoImage(tapGestureRecognizer:)))
                shopshowCell.brandLogoImage.isUserInteractionEnabled = true
                shopshowCell.brandLogoImage.addGestureRecognizer(tapGestureRecognizerBrandLogo)

                let tapGestureOnScrollImage = UITapGestureRecognizer(target: self, action: #selector(THShopshowViewController.imageTappedOnScrollImage(tapGestureRecognizer:)))
                shopshowCell.objScrollView.addGestureRecognizer(tapGestureOnScrollImage)
                shopshowCell.soldOutLabel.isHidden = self.productVariations.count == 0 ? false: true
                if self.productVariations.count == 0 {
                    shopshowCell.scoreButton.setAttributedTitle(nil, for: .normal)
                    shopshowCell.scoreButton.setTitle(" share ", for: .normal)
                }
                shopshowCell.btnNotifyMe.isHidden = self.productVariations.count == 0 ? false: true
                shopshowCell.btnNotifyMe.addTarget(self, action: #selector(THShopshowViewController.didTapNotifyMeButton(_:)), for: .touchUpInside)

                if selectedndex != -1 {
                    shopshowCell.addToCardButton.isEnabled = true
                    shopshowCell.applePayButton.isEnabled = true
                }

                return shopshowCell
            } else {
                let signupGiveAwayCell = tableView.dequeueReusableCell(withIdentifier: THSignUpGiveAwayTableViewCell.className, for: indexPath) as! THSignUpGiveAwayTableViewCell
                signupGiveAwayCell.selectionStyle = .none
                signupGiveAwayCell.emailButton.text = emailText
                signupGiveAwayCell.passwordButton.text = passwordText
                signupGiveAwayCell.usernameField.text = usernameText
                signupGiveAwayCell.phoneField.text = phoneText
                signupGiveAwayCell.facebookButton.addTarget(self, action: #selector(THShopshowViewController.didTapOnFacebookSignupGiveAwayButton(_sender:)), for: .touchUpInside)
                signupGiveAwayCell.signupButton.addTarget(self, action: #selector(THShopshowViewController.didTapOnSignupEmailGiveAwayButton(_sender:)), for: .touchUpInside)
                signupGiveAwayCell.emailButton.delegate = self
                signupGiveAwayCell.passwordButton.delegate = self
                signupGiveAwayCell.usernameField.delegate = self
                signupGiveAwayCell.phoneField.delegate = self
                return signupGiveAwayCell
            }

        case 1:
            let commentCell = tableView.dequeueReusableCell(withIdentifier: THCommentTableViewCell.className, for: indexPath) as! THCommentTableViewCell
            commentCell.selectionStyle = UITableViewCellSelectionStyle.none
            commentCell.configure(with: categoryDetail.comments[indexPath.row])
            commentCell.commentImageProfile.isHidden = indexPath.row == 0 ? false : true
            return commentCell
        case 2:
            let suggestedCell = tableView.dequeueReusableCell(withIdentifier: THSuggestedProductTableViewCell.className, for: indexPath) as! THSuggestedProductTableViewCell
            suggestedCell.objSuggestedCollectionView.delegate = self
            suggestedCell.objSuggestedCollectionView.dataSource = self
            if self.suggestProduct.count == 0 {
                suggestedCell.heightSuggestedConstraint.constant = 0
            } else if self.suggestProduct.count < 7 {
                suggestedCell.heightSuggestedConstraint.constant = (screenheight/2) + 100
            } else {
                suggestedCell.heightSuggestedConstraint.constant = screenheight
            }
            suggestedCell.layoutIfNeeded()
            suggestedCell.objSuggestedCollectionView.reloadData()
            return suggestedCell
        default:
            let shopshowCell = tableView.dequeueReusableCell(withIdentifier: THShopShowTableViewCell.className, for: indexPath)
            return shopshowCell
        }
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        switch indexPath.section {
        case 0:
            return false
        case 1:
            if let currentUser = THUserManager.sharedInstance.currentUserdata() {
                if currentUser.userId == categoryDetail.comments[indexPath.row].userId {
                    return true
                }
            }
            return false
        default:
            return false
        }
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            let comment = categoryDetail.comments[indexPath.row]
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THCommentManager.sharedInstance.performDeleteProductComment(with: categoryDetail.brandId, productId: categoryDetail.Id, commentId: comment.id, completion: { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                guard error != nil else {
                    self.categoryDetail.comments.remove(at: indexPath.row)
                    self.objTableView.deleteRows(at: [indexPath], with: .left)
                    self.objTableView.reloadData()
                    return
                }
            })
        }
    }


    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 568
        case 1:
            return 40
        case 2:
            return 0
        default:
            return 40
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return UITableViewAutomaticDimension
            if self.categoryDetail.product_type == "Giveaway" {
                if self.categoryDetail.productGalleryImages.count > 0 {
                    let imgHeight = CGFloat(self.categoryDetail.productGalleryImages[0].mediumImageHeight)
                    let imgWidth = CGFloat(self.categoryDetail.productGalleryImages[0].mediumImageWidth)
                    return (imgHeight/imgWidth) * screenwidth + 104
                }
            }
            return UITableViewAutomaticDimension
        default:
            return UITableViewAutomaticDimension
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: screenwidth, height: 30))
        let headerLabel = UILabel(frame: CGRect(x: 10, y: 0, width: screenwidth-10, height: 25))
        headerLabel.font = UIFont.AppFontBold(14)
        headerLabel.text = "Related Products"
        headerLabel.textColor = UIColor.black
        headerView.addSubview(headerLabel)
        headerView.backgroundColor = UIColor.white
        return headerView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 2 {
            return 30
        } else {
            return 0
        }
    }
}

extension THShopshowViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        switch textField.tag {
        case 1:
            emailText = textFieldText.replacingCharacters(in: range, with: string)
        case 2:
            passwordText = textFieldText.replacingCharacters(in: range, with: string)
        case 3:
            usernameText = textFieldText.replacingCharacters(in: range, with: string)
        case 4:
            phoneText = textFieldText.replacingCharacters(in: range, with: string)
        default:
            break
        }

        return true
    }
}

extension THShopshowViewController: PKPaymentAuthorizationViewControllerDelegate {
    @available(iOS 11.0, *)
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
        // Apple Pay Stripe
        STPAPIClient.shared().createToken(with: payment) { (token, error) in

            guard (error == nil) else {
                completion(PKPaymentAuthorizationResult.init(status: PKPaymentAuthorizationStatus.failure, errors: [error!]))
                return
            }

            if let card = token!.allResponseFields["card"] as? NSDictionary {

                let cardToken = card["id"] as! String
                let tokenID = token!.tokenId
                var firstName = ""
                var lastName = ""
                var address1 = ""
                var address2 = ""
                var state = ""
                var city = ""
                var zipCode = ""
                var phoneNo = ""
                var email = ""

                let streets = payment.shippingContact?.postalAddress!.street.components(separatedBy: "\n")

                if let first = payment.shippingContact?.name?.givenName {
                    firstName = first
                }

                if let last = payment.shippingContact?.name?.familyName {
                    lastName = last
                }

                if let add1 = streets?[0] {
                    address1 = add1
                }

                if ((streets?.count)! > 1) {
                    let add2 = streets?[1]
                    address2 = add2!
                }

                if let addState = payment.shippingContact?.postalAddress?.state {
                    state = addState
                }

                if let addCity = payment.shippingContact?.postalAddress?.city  {
                    city = addCity
                }

                if let addZip = payment.shippingContact?.postalAddress?.postalCode  {
                    zipCode = addZip
                }

                if let phone = payment.shippingContact?.phoneNumber  {
                    phoneNo = phone.stringValue
                }

                if let emailAddress = payment.shippingContact?.emailAddress {
                    email = emailAddress
                }
                let productVar = self.productVariations[self.selectedndex]

                THApplePayManager.sharedInstance.instantBuyNative(with: productVar.productId, productVarId: productVar.id,tokenId: tokenID, cardToken: cardToken, firstName: firstName, lastName: lastName, streetAddress1: address1, streetAddress2: address2, state: state, city: city, zipCode: zipCode, phone: phoneNo, email: email, postId: self.postId,shareCode: self.shareCode,completion: { (error,message ) in

                    guard (error == nil) else {
                         completion(PKPaymentAuthorizationResult.init(status: PKPaymentAuthorizationStatus.failure, errors: nil))
                        return
                    }


                    if message != "" {
                        controller.dismiss(animated: false, completion: {
                            self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "", body: message!, completion: { () in}), animated: false, completion: nil)
                        })
                    } else {
                        completion(PKPaymentAuthorizationResult.init(status: PKPaymentAuthorizationStatus.success, errors: nil))
                        controller.dismiss(animated: false) {
                            //Add funnel for complete purchase
                            let price = self.categoryDetail.salePrice == 0.0 ? self.categoryDetail.defaultPrice:self.categoryDetail.salePrice
                            Answers.logPurchase(withPrice: NSDecimalNumber(value: price), currency: "USD", success: true, itemName: self.categoryDetail.title, itemType: self.categoryDetail.brandName, itemId: "\(self.categoryDetail.Id)", customAttributes: [:])

                            NotificationCenter.default.post(name: Notification.Name(postNotificationType.notificationOrderSucessfully.rawValue), object: nil, userInfo: nil)
                            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                                appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
                                (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
                            }
                            return
                        }
                    }
                })
            }
        }
    }


    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {

        // Apple Pay Stripe
        STPAPIClient.shared().createToken(with: payment) { (token, error) in

            guard (error == nil) else {
                completion(.failure)
                return
            }

            if let card = token!.allResponseFields["card"] as? NSDictionary {

                let cardToken = card["id"] as! String
                let tokenID = token!.tokenId
                var firstName = ""
                var lastName = ""
                var address1 = ""
                var address2 = ""
                var state = ""
                var city = ""
                var zipCode = ""
                var phoneNo = ""
                var email = ""

                let streets = payment.shippingContact?.postalAddress!.street.components(separatedBy: "\n")

                if let first = payment.shippingContact?.name?.givenName {
                     firstName = first
                }

                if let last = payment.shippingContact?.name?.familyName {
                    lastName = last
                }

                if let add1 = streets?[0] {
                    address1 = add1
                }

                if ((streets?.count)! > 1) {
                    let add2 = streets?[1]
                    address2 = add2!
                }

                if let addState = payment.shippingContact?.postalAddress?.state {
                    state = addState
                }

                if let addCity = payment.shippingContact?.postalAddress?.city  {
                    city = addCity
                }

                if let addZip = payment.shippingContact?.postalAddress?.postalCode  {
                    zipCode = addZip
                }

                if let phone = payment.shippingContact?.phoneNumber  {
                    phoneNo = phone.stringValue
                }

                if let emailAddress = payment.shippingContact?.emailAddress {
                    email = emailAddress
                }
                let productVar = self.productVariations[self.selectedndex]

                THApplePayManager.sharedInstance.instantBuyNative(with: productVar.productId, productVarId: productVar.id,tokenId: tokenID, cardToken: cardToken, firstName: firstName, lastName: lastName, streetAddress1: address1, streetAddress2: address2, state: state, city: city, zipCode: zipCode, phone: phoneNo, email: email, postId: self.postId,completion: { (error,message ) in

                    guard (error == nil) else {
                        completion(.failure)
                        return
                    }


                    if message != "" {
                        controller.dismiss(animated: false, completion: {
                            self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "", body: message!, completion: { () in}), animated: false, completion: nil)
                        })
                    } else {
                    completion(.success)
                    controller.dismiss(animated: false) {
                        //Add funnel for complete purchase
                        let price = self.categoryDetail.salePrice == 0.0 ? self.categoryDetail.defaultPrice:self.categoryDetail.salePrice
                        Answers.logPurchase(withPrice: NSDecimalNumber(value: price), currency: "USD", success: true, itemName: self.categoryDetail.title, itemType: self.categoryDetail.brandName, itemId: "\(self.categoryDetail.Id)", customAttributes: [:])

                        NotificationCenter.default.post(name: Notification.Name(postNotificationType.notificationOrderSucessfully.rawValue), object: nil, userInfo: nil)
                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
                            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
                        }
                        return

                        /*if self.tabBarController == nil {
                            self.dismiss(animated: true, completion: {
                                self.tabBarController?.selectedIndex = 1
                                NotificationCenter.default.post(name: Notification.Name(postNotificationType.notificationOrderSucessfully.rawValue), object: nil, userInfo: nil)
                            })
                        } else {
                            self.tabBarController?.selectedIndex = 1
                           NotificationCenter.default.post(name: Notification.Name(postNotificationType.notificationOrderSucessfully.rawValue), object: nil, userInfo: nil)
                        }*/

                    }
                }
                })
            }
        }
    }

    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        controller.dismiss(animated: false) {

        }
    }



    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didSelectShippingContact contact: PKContact, completion: @escaping (PKPaymentAuthorizationStatus, [PKShippingMethod], [PKPaymentSummaryItem]) -> Void) {

        guard let zipcode = contact.postalAddress?.postalCode, let city = contact.postalAddress?.city, let state = contact.postalAddress?.state else{
            completion(.invalidShippingPostalAddress, [], [])
            return
        }

        let productVar = productVariations[self.selectedndex]

        //let productVar = self.categoryDetail!.productVariations[self.selectedndex]

        THApplePayManager.sharedInstance.instantBuyTaxCalculation(with: productVar.productId, zipCode: zipcode, city: city, state: state) { (error, tax) in

            guard (error == nil) else {
                completion(.invalidShippingPostalAddress, [], [])
                return
            }

            let price = self.categoryDetail.salePrice == 0.0 ? self.categoryDetail.defaultPrice:self.categoryDetail.salePrice
            var summaryItems = [PKPaymentSummaryItem]()
            let subtotal = PKPaymentSummaryItem(label: "SUBTOTAL", amount: NSDecimalNumber(value: price))
            let salesTax = PKPaymentSummaryItem(label: "SALES TAX & FEES", amount: NSDecimalNumber(value: tax!))
            let shipping = PKPaymentSummaryItem(label: "SHIPPING", amount: 0)

            let target = Double(price.adding(Float(tax!)))
            let payTarget = PKPaymentSummaryItem(label: "THREADEST INC.", amount: NSDecimalNumber(value: target))
            summaryItems = [subtotal, salesTax, shipping, payTarget]
            completion(.success, [], summaryItems)
        }
    }
}

extension THShopshowViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}


extension THShopshowViewController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .overFullScreen
    }

    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        // updateSettings()
    }
}
