//
//  THShopThreadestBlackViewController.swift
//  Threadest
//
//  Created by Jaydeep on 17/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import PassKit
import Stripe
import FBSDKLoginKit
import FBSDKCoreKit
import Social
import SDWebImage
import MessageUI
import TwitterKit
import Crashlytics
import Lottie
import ARKit

protocol SelectedSize {
    func selectedSize(id: Int, product: SelectedProduct)
}

struct SelectedProduct {
    var selectedSize: String
    var productIndex: Int
}

class THShopThreadestBlackViewController: UIViewController, UIGestureRecognizerDelegate {
    @IBOutlet weak var cartCountLabel: UILabel!
    @IBOutlet weak var objTableView: UITableView!

    @IBOutlet weak var searchBarView: UIView!
    @IBOutlet weak var searchTextField: DesignableTextField!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!

    // variable
    var cartCategoryString = ""
    var categoryID = -1
    // for which page navigation
    var navigationFrom = ""
    var isPopup = false
    var shopTitle = ""

    var shopCategories = [ClosetCategory]()
    var selectedCategory:ClosetCategory?
    var selectedVariation: ProductVariation?
    var selectedSize = [Int : SelectedProduct]()
    var refreshControl: UIRefreshControl!
    weak var delegate: SearchBarDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        searchTextField.delegate = self
         objTableView.register(UINib(nibName: THShopTableViewCell.className, bundle: nil), forCellReuseIdentifier: THShopTableViewCell.className)
        objTableView.keyboardDismissMode = .onDrag
        //pull to refresh
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(THShopThreadestBlackViewController.refresh(sender:)), for: UIControlEvents.valueChanged)
        objTableView.addSubview(refreshControl)
        initUI()


        // for shopping page category endpoint list
        if navigationFrom == "shop" {
        THFeaturedFeedCategoryManager.sharedInstance.shopCategory =  [ClosetCategory]()
        THFeaturedFeedCategoryManager.sharedInstance.page = 1
        THFeaturedFeedCategoryManager.sharedInstance.didLoadData = false
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THFeaturedFeedCategoryManager.sharedInstance.retrieveFeaturedFeedCategory(with: cartCategoryString, id: categoryID, completion: { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            guard let error = error else {
                //Add funnel for shop category
                if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    Answers.logCustomEvent(withName: "Shop category", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username, "category_id":"\(self.categoryID)", "category_name":self.cartCategoryString])
                }
                self.shopCategories = THFeaturedFeedCategoryManager.sharedInstance.shopCategory
                self.objTableView.reloadData()
                self.objTableView.setNeedsLayout()
                self.objTableView.layoutIfNeeded()
                self.objTableView.updateConstraints()
                self.objTableView.setNeedsUpdateConstraints()
                self.objTableView.reloadData()
                return
            }
            debugPrint(error)
            })
        } else if navigationFrom == "closet" {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                THProfileClosetManager.sharedInstance.retrieveClosetCategory(with: usercurrent.userId, category: cartCategoryString) { (error) in
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    guard let error = error else {
                        self.shopCategories = THProfileClosetManager.sharedInstance.closetCategory
                        self.objTableView.reloadData()
                        self.objTableView.setNeedsLayout()
                        self.objTableView.layoutIfNeeded()
                        self.objTableView.updateConstraints()
                        self.objTableView.setNeedsUpdateConstraints()
                        self.objTableView.reloadData()
                        return
                    }
                    debugPrint(error)
                    }
                }
        }

        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)

        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)

        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        searchBarHideShow()
    }

    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                break
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
                break
            default:
                break
            }
        }
    }



    override func viewWillAppear(_ animated: Bool) {
        if let cartCount = UserDefaults.standard.value(forKey: "cartCount"){
            if (cartCount as! Int) == 0 {
                self.cartCountLabel.isHidden = true
            }
            else {
                self.cartCountLabel.isHidden = false
                self.cartCountLabel.text = "\(cartCount as! Int)"
            }
        } else {
            self.cartCountLabel.isHidden = true
        }

       // self.tabBarController?.tabBar.isHidden = true
        if navigationFrom == "closet" {
            self.shopCategories = THProfileClosetManager.sharedInstance.closetCategory
            self.objTableView.reloadData()
        } else if navigationFrom == "shop" {
            self.shopCategories = THFeaturedFeedCategoryManager.sharedInstance.shopCategory
            self.objTableView.reloadData()
        }
    }


    override func viewWillDisappear(_ animated: Bool) {
        let imageCache = SDImageCache.shared()
        imageCache.clearMemory()
    }

    override func didReceiveMemoryWarning() {
        let imageCache = SDImageCache.shared()
        imageCache.clearMemory()
    }


    fileprivate func initUI(){
        if isPopup {
            backButton.setImage(UIImage(named: "arrow_blue_down"), for: .normal)
        } else {
            backButton.setImage(UIImage(named: "cart_blue_back"), for: .normal)
        }
        titleLabel.text = shopTitle
        cartCountLabel.layer.cornerRadius = cartCountLabel.frame.width / 2.0
        cartCountLabel.layer.masksToBounds = true
    }

    //pull to refresh methods
    func refresh(sender:AnyObject) {
        if navigationFrom == "shop" {
            THFeaturedFeedCategoryManager.sharedInstance.page = 1
            THFeaturedFeedCategoryManager.sharedInstance.didLoadData = true
            THFeaturedFeedCategoryManager.sharedInstance.pullToRefresh = true

            THFeaturedFeedCategoryManager.sharedInstance.retrieveFeaturedFeedCategory(with: cartCategoryString, id: categoryID, completion: { (error) in
                guard let error = error else {
                    self.refreshControl.endRefreshing()
                    self.shopCategories = THFeaturedFeedCategoryManager.sharedInstance.shopCategory
                    self.objTableView.reloadData()
                    self.objTableView.setNeedsLayout()
                    self.objTableView.layoutIfNeeded()
                    self.objTableView.updateConstraints()
                    self.objTableView.setNeedsUpdateConstraints()
                    self.objTableView.reloadData()
                    return
                }
                debugPrint(error)
            })
        }

        else if navigationFrom == "closet" {
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                THProfileClosetManager.sharedInstance.retrieveClosetCategory(with: usercurrent.userId, category: cartCategoryString) { (error) in
                    guard let error = error else {
                        self.refreshControl.endRefreshing()
                        self.shopCategories = THProfileClosetManager.sharedInstance.closetCategory
                        self.objTableView.reloadData()
                        self.objTableView.setNeedsLayout()
                        self.objTableView.layoutIfNeeded()
                        self.objTableView.updateConstraints()
                        self.objTableView.setNeedsUpdateConstraints()
                        self.objTableView.reloadData()
                        return
                    }
                    debugPrint(error)
                }
            }
        }
    }


    @IBAction func didTapshoppingCartbutton(_ sender: Any){
        /*if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }*/
        let shoppingcartController = THYourCartViewController(nibName: THYourCartViewController.className, bundle: nil)
        self.navigationController?.pushViewController(shoppingcartController, animated: true)
    }


    @IBAction func didTapBackButton(_ sender: Any){
        if isPopup {
            self.navigationController?.dismiss(animated: true, completion: nil)
        } else {
             let _ = self.navigationController?.popViewController(animated: true)
        }
    }

    @IBAction func didTapOnLikeButton(_ sender: UIButton) {
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }
        let shopCell = objTableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! THShopTableViewCell
        if !sender.isSelected {
            sender.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                sender.transform = CGAffineTransform(rotationAngle: CGFloat(-179.9).degreesToRadians)
            })
            UIView.animate(withDuration: 0.3, delay: 0.2, options: UIViewAnimationOptions(), animations: {
                sender.transform = CGAffineTransform(rotationAngle: CGFloat(1.1).degreesToRadians)
            }, completion: { (completed) in
                var shopCategory:ClosetCategory = self.shopCategories[sender.tag]
                if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                    THUserManager.sharedInstance.performUpvote(for: usercurrent.userId, votableId: shopCategory.Id, votableType: "Product", completion: { (vote, error) in
                        guard let error = error else {
                            shopCategory.likedByCurrentUser = true
                            shopCategory.votesCount = shopCategory.votesCount + 1
                            self.shopCategories[sender.tag] = shopCategory
                            shopCell.likeButton.isSelected  = true

                            let animationView = LOTAnimationView(name: "star_blast")
                            animationView.animationProgress = 100
                            animationView.center = shopCell.shopImage.center
                            animationView.frame = shopCell.shopImage.frame
                            animationView.backgroundColor = UIColor.clear
                            animationView.contentMode = .scaleAspectFill
                            animationView.animationSpeed = 1
                            shopCell.shopImage.addSubview(animationView)
                            animationView.loopAnimation = false
                            animationView.play { (sucess) in
                                animationView.pause()
                                animationView.removeFromSuperview()
                                //add funnel for product like
                                Answers.logCustomEvent(withName: "Like product", customAttributes: ["userid":usercurrent.userId,"username": usercurrent.username, "product_Id":"\(shopCategory.Id)"])
                            }
                            return
                        }
                        debugPrint(error)
                    })
                }
                else {

                }
            })
        } else {

        }
    }

    @IBAction func didTapAddSelectSize(_ sender: UIButton){
        let categoryDetail = shopCategories[sender.tag]
        if categoryDetail.soldOut {
            return
        }
        if let _ = categoryDetail.productVariations.index(where: {$0.quantityAvailable >= 1}) {
            let popAddNewCard = THProductSizePopUp(nibName: THProductSizePopUp.className, bundle: nil)
            popAddNewCard.delegate = self
            popAddNewCard.categoryDetail = categoryDetail
            self.addChildViewController(popAddNewCard)
            popAddNewCard.view.frame = self.view.frame
            self.view.addSubview(popAddNewCard.view)
            popAddNewCard.didMove(toParentViewController: self)
        }
    }

    fileprivate func searchBarHideShow() {
        let searchPopup = THSearchResultAllViewController(nibName: THSearchResultAllViewController.className, bundle: nil)
        searchPopup.isFromScreen = "Category"
        delegate = searchPopup
        searchTextField.delegate = self
        searchPopup.category = cartCategoryString
        self.addChildViewController(searchPopup)
        searchPopup.view.frame = CGRect(x: 0, y: 108, width: screenwidth, height: screenheight-108)
        searchPopup.view.isHidden = true
        //self.view.addSubview(searchPopup.view)
        UIView.animate(withDuration: 0.25, animations: {
            //self.searchBarView.alpha = 1
        }, completion: { (completed) in
            self.view.addSubview(searchPopup.view)
        })
       /* searchBarView.alpha = 0
        searchBarView.isHidden = false
        UIView.animate(withDuration: 0.25, animations: {
            self.searchBarView.alpha = 1
        }, completion: { (completed) in
            self.view.addSubview(searchPopup.view)
        })*/
    }


    @IBAction func didTapOnFilterButton(_ sender: Any) {
        delegate?.didTapOnFilterButton()
    }


    @IBAction func didTapAddToCart(_ sender: UIButton){
        /*if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }*/
        let category = shopCategories[sender.tag]
        let product = self.selectedSize[category.Id]
        let productVar = shopCategories[sender.tag].productVariations[product!.productIndex]
        GeneralMethods.sharedInstance.loadLoading(view: self.view)

        THCartManager.sharedInstance.addToCart(with: productVar.productId, productVariationId: productVar.id, postId:-1) { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            if let error = error {
                self.showAlert(withMessage: error.localizedDescription)
            }else {
                if let cartCount = UserDefaults.standard.value(forKey: "cartCount")
                {
                    if (cartCount as! Int) == 0 {
                        self.cartCountLabel.isHidden = true
                    }
                    else {
                        self.cartCountLabel.isHidden = false
                        self.cartCountLabel.text = "\(cartCount as! Int)"
                    }
                } else {
                    self.cartCountLabel.isHidden = true
                }
                //Add to cart Funnel
                let productPrice = category.salePrice == 0.0 ? category.defaultPrice: category.salePrice
                Answers.logAddToCart(withPrice: NSDecimalNumber(value: productPrice), currency: "USD", itemName: category.title, itemType: category.brandName, itemId: "\(category.Id)", customAttributes: ["description":category.description])

                self.showAlert(title: "Success", message: "Successfully add \(category.title) to cart", okClick: {

                   // Do Something
                })
            }
        }
    }


    @IBAction func didTapAddApplePay(_ sender: UIButton){
       /* if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }*/
        let paymentNetworks = [PKPaymentNetwork.discover, .masterCard, .amex, .visa]
        if PKPassLibrary.isPassLibraryAvailable() {
            if !PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetworks) {
                self.present(GeneralMethods.sharedInstance.AlertviewGeneral(title: "Add a Credit Card to Wallet", body: "Would you like to add a credit card to your wallet now?", cancelbutton: "No", okbutton: "Yes", completion: { () in
                    let lib = PKPassLibrary()
                    lib.openPaymentSetup()
                }), animated: true, completion: nil)
            }
        }
        self.selectedCategory = self.shopCategories[sender.tag]
        let product = self.selectedSize[self.selectedCategory!.Id]
        self.selectedVariation = shopCategories[sender.tag].productVariations[product!.productIndex]
        let paymentRequest = Stripe.paymentRequest(withMerchantIdentifier: STPPaymentConfiguration.shared().appleMerchantIdentifier!)

        paymentRequest.supportedNetworks = paymentNetworks
        paymentRequest.requiredShippingAddressFields = [.all ]

        let price = self.selectedCategory!.salePrice == 0.0 ? self.selectedCategory!.defaultPrice:self.selectedCategory!.salePrice
        let payTarget = PKPaymentSummaryItem(label: "THREADEST INC.", amount: NSDecimalNumber(value: price))

        paymentRequest.paymentSummaryItems = [payTarget]

        if  Stripe.canSubmitPaymentRequest(paymentRequest) {
            let paymentAuthorizationVC = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest)
            paymentAuthorizationVC.delegate = self
            self.present(paymentAuthorizationVC, animated: true, completion: nil)
            //Add funnel for Apple pay button
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Apple Pay Button", customAttributes: ["userid":usercurrent.userId,
                                                                                        "username": usercurrent.username, "Price":price])
            }
        } else {
            // there is a problem with your Apple Pay configuration.
        }

    }

    @IBAction func didTapOnRethreadButton(_ sender: UIButton) {
        guard let usercurrent = THUserManager.sharedInstance.currentUserdata() else {
            return
        }

        if usercurrent.tempAccount {
            appdelegate.setupAccountPopup(viewController: self)
            return
        }

        let cell = objTableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as!THShopTableViewCell
        let closetCategory:ClosetCategory = shopCategories[sender.tag]
        if !cell.rethreadButton.isSelected {
            cell.rethreadButton.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                cell.rethreadButton.transform = CGAffineTransform(rotationAngle: CGFloat(-179.9).degreesToRadians)
            })
            UIView.animate(withDuration: 0.3, delay: 0.2, options: UIViewAnimationOptions(), animations: {
                cell.rethreadButton.transform = CGAffineTransform(rotationAngle: CGFloat(1.1).degreesToRadians)
            }, completion: { (completed) in
                let rethreadpopup = THRethreadPopupViewController(nibName: THRethreadPopupViewController.className, bundle: nil)
                rethreadpopup.productLinkString = "https://www.threadest.com/products/\(closetCategory.Id)?b=\(closetCategory.brandId)&sc=\(usercurrent.sharePostCode)"
                self.addChildViewController(rethreadpopup)
                rethreadpopup.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
                self.view.addSubview(rethreadpopup.view)
                rethreadpopup.didMove(toParentViewController: self)
                rethreadpopup.completionHander = {selectedRethreadArray in
                    for selectedRethread in selectedRethreadArray! {
                        switch selectedRethread as! String {
                        case "Threadest":
                            self.rethreadPost(selectedRow: sender.tag)
                            break
                        case "Facebook":
                            self.rethreadPostOnFacebook(selectedRow: sender.tag)
                            break
                        case "Twitter":
                            self.rethreadPostOnTwitter(closetCategory:self.shopCategories[sender.tag])
                            break
                        default:
                            break
                        }
                    }
                }
            })
        } else {

        }
    }

     @IBAction func didTapOnContactButton(_ sender: UIButton) {
        if MFMailComposeViewController.canSendMail() {
            let closetCategory:ClosetCategory = shopCategories[sender.tag]
            let mailComposerVC = MFMailComposeViewController()
            mailComposerVC.mailComposeDelegate = self
            mailComposerVC.setToRecipients(["hello@threadest.com"])
            mailComposerVC.setSubject("Threadest Inquiry")
            mailComposerVC.setMessageBody("Hi, I'm viewing \(closetCategory.title) and have a question...", isHTML: false)
            self.present(mailComposerVC, animated: true, completion: nil)
        } else {
            self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "", body: "Uh oh! Your Apple email isn’t configured yet. Please configure in order to continue.", completion: {
            }), animated: true, completion: nil)
        }
    }




    // for Rethread post Threadest
    func rethreadPost(selectedRow: Int) {
         let closetCategory:ClosetCategory = shopCategories[selectedRow]

        THSocialInteractionPostsManager.sharedInstance.performRethreadPost(for: closetCategory.Id) { (error) in
            guard let error = error else {
                /*let socialCell = self.objTableView.cellForRow(at: IndexPath(row: selectedRow, section: 1)) as! THShareDetailTableViewCell

                socialCell.rethreadCountLabel.text = "\(socialInteraction.rethreadCount + 1)"
                THSocialInteractionPostsManager.sharedInstance.socialInteractionPosts[selectedRow].rethreadCount = socialInteraction.rethreadCount + 1*/
                return
            }
            debugPrint(error)
        }
    }

    // for Rethread post on Facebook
    func rethreadPostOnFacebook(selectedRow: Int) {
        guard let usercurrent = THUserManager.sharedInstance.currentUserdata() else {
            return
        }
        let closetCategory:ClosetCategory = shopCategories[selectedRow]
        if FBSDKAccessToken.current() != nil {
            if FBSDKAccessToken.current().hasGranted("publish_actions") {
                GeneralMethods.sharedInstance.publishMessage(message: "https://www.threadest.com/products/\(closetCategory.Id)?b=\(closetCategory.brandId)&sc=\(usercurrent.sharePostCode)", link: closetCategory.defaultImageUrl, completion: { () in
                        GeneralMethods.sharedInstance.RethreadPostSucessPopup(message: "Post to Facebook - Successful", viewController: self)
                })
            } else {
                requestPublishPermissions(closetCategory:closetCategory)
            }
        } else {
            requestPublishPermissions(closetCategory:closetCategory)
        }
    }

    func requestPublishPermissions(closetCategory: ClosetCategory) {
        guard let usercurrent = THUserManager.sharedInstance.currentUserdata() else {
            return
        }
        let login: FBSDKLoginManager = FBSDKLoginManager()
        login.logIn(withPublishPermissions: ["publish_actions"], from: self) { (result, error) in
            if (error != nil) {
                print(error!)
            } else if (result?.isCancelled)! {
                print("Canceled")
            } else if (result?.grantedPermissions.contains("publish_actions"))! {
                print("permissions granted")
                GeneralMethods.sharedInstance.publishMessage(message: "https://www.threadest.com/products/\(closetCategory.Id)?b=\(closetCategory.brandId)&sc=\(usercurrent.sharePostCode)", link: closetCategory.defaultImageUrl, completion: { () in
                    GeneralMethods.sharedInstance.RethreadPostSucessPopup(message: "Post to Facebook - Successful", viewController: self)
                })
            }
        }
    }


    func rethreadPostOnTwitter(closetCategory: ClosetCategory) {
        guard let usercurrent = THUserManager.sharedInstance.currentUserdata() else {
            return
        }
        if let twitterAccountId = TWTRTwitter.sharedInstance().sessionStore.session() {
            GeneralMethods.sharedInstance.uploadTwitterMedia(viewController: self, imageURL: closetCategory.defaultImageUrl, status: "https://www.threadest.com/products/\(closetCategory.Id)?b=\(closetCategory.brandId)&sc=\(usercurrent.sharePostCode)", token: twitterAccountId.authToken, userId: twitterAccountId.userID, id: closetCategory.Id, type: "Product")
        } else {
            // Not logged in
            TWTRTwitter.sharedInstance().logIn { (session, error) in
                if let _ = session {
                    if let twitterAccountId = TWTRTwitter.sharedInstance().sessionStore.session() {
                        GeneralMethods.sharedInstance.uploadTwitterMedia(viewController: self, imageURL: closetCategory.defaultImageUrl, status: "https://www.threadest.com/products/\(closetCategory.Id)?b=\(closetCategory.brandId)&sc=\(usercurrent.sharePostCode)", token: twitterAccountId.authToken, userId: twitterAccountId.userID, id: closetCategory.Id, type: "Product")
                    }
                } else {
                    print("error: \(String(describing: error?.localizedDescription))");
                }
            }
        }
    }

    func pinchZoomProductImage(sender: UIPinchGestureRecognizer) {
        let tapLocation = sender.location(in: self.objTableView)
        if let indexPath : IndexPath = self.objTableView.indexPathForRow(at: tapLocation) {
            let shopCell = self.objTableView.cellForRow(at: indexPath) as! THShopTableViewCell
            TMImageZoom.shared().gestureStateChanged(sender, withZoom: shopCell.shopImage)
        } else {
            TMImageZoom.shared().resetImageZoom()
        }

    }
}

extension THShopThreadestBlackViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THBrandProductManager.sharedInstance.retrieveBrandProfile(with: shopCategories[indexPath.row].brandId, productId: shopCategories[indexPath.row].Id, geofence: false) { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            let threeDAvailable = THBrandProductManager.sharedInstance.brandProduct?.productGalleryImages.contains(where: { (imgGallery) -> Bool in
                if imgGallery.image_type == "3D" {
                    return true
                } else {
                    return false
                }
            })
            
            if threeDAvailable! {
                if #available(iOS 11.0, *) {
                    if (ARConfiguration.isSupported) {
                        let ARShopShowController = THARShopShowViewController(nibName: THARShopShowViewController.className, bundle: nil)
                        ARShopShowController.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                        self.navigationController?.pushViewController(ARShopShowController, animated: true)
                        return
                    }
                }
            }
            let shopshowcontroller = THShopshowViewController(nibName: THShopshowViewController.className, bundle: nil)
            shopshowcontroller.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
            self.navigationController?.pushViewController(shopshowcontroller, animated: true)
        }
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if navigationFrom == "shop" {
            if indexPath.row == self.shopCategories.count - 1 {
                if THFeaturedFeedCategoryManager.sharedInstance.didLoadData {
                    THFeaturedFeedCategoryManager.sharedInstance.didLoadData = false
                        THFeaturedFeedCategoryManager.sharedInstance.retrieveFeaturedFeedCategory(with: cartCategoryString, id: categoryID, completion: { (error) in
                            guard let error = error else {
                                self.shopCategories = THFeaturedFeedCategoryManager.sharedInstance.shopCategory
                                self.objTableView.reloadData()
                                self.objTableView.setNeedsLayout()
                                self.objTableView.layoutIfNeeded()
                                self.objTableView.updateConstraints()
                                self.objTableView.setNeedsUpdateConstraints()
                                self.objTableView.reloadData()
                                return
                            }
                            debugPrint(error)
                        })
                }
            }
        }
    }
}

extension THShopThreadestBlackViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return shopCategories.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let ShopCell = tableView.dequeueReusableCell(withIdentifier: THShopTableViewCell.className, for: indexPath) as! THShopTableViewCell
        ShopCell.selectionStyle = UITableViewCellSelectionStyle.none
        ShopCell.availableSizeLabel.layer.cornerRadius = 15
        ShopCell.priceLabel.layer.cornerRadius = 15
        ShopCell.availableSizeLabel.layer.masksToBounds = true
        ShopCell.priceLabel.layer.masksToBounds = true
        ShopCell.likeButton.tag = indexPath.row
        ShopCell.configure(withClosetCategory: shopCategories[indexPath.row])
        ShopCell.likeButton.addTarget(self, action: #selector(THShopThreadestBlackViewController.didTapOnLikeButton(_:)), for: UIControlEvents.touchUpInside)
        ShopCell.addToCartButton?.tag = indexPath.row
        ShopCell.applepaymentbutton.tag = indexPath.row
        ShopCell.rethreadButtonTop.tag = indexPath.row
        ShopCell.rethreadButtonTop.addTarget(self, action: #selector(THShopThreadestBlackViewController.didTapOnRethreadButton(_:)), for: .touchUpInside)
        ShopCell.contactButton.tag = indexPath.row
        ShopCell.contactButton.addTarget(self, action: #selector(THShopThreadestBlackViewController.didTapOnContactButton(_:)), for: .touchUpInside)

        if let _ = self.selectedSize[shopCategories[indexPath.row].Id] {
            ShopCell.addToCartButton.backgroundColor = .clear
            ShopCell.addToCartButton?.setTitle("Add to cart", for: .normal)
            ShopCell.addToCartButton.setImage(UIImage(named: "cart_icon"), for: .normal)

            ShopCell.applepaymentbutton.isEnabled = true
            ShopCell.addToCartButton?.addTarget(self, action: #selector(THShopThreadestBlackViewController.didTapAddToCart(_:)), for: UIControlEvents.touchUpInside)
      }else {
            ShopCell.addToCartButton?.setTitle("Select Size", for: .normal)
            ShopCell.addToCartButton.setImage(nil, for: .normal)
            ShopCell.addToCartButton.backgroundColor = UIColor(red:0.09, green:0.55, blue:0.80, alpha:1.0)
            ShopCell.applepaymentbutton.isEnabled = false

             ShopCell.addToCartButton?.addTarget(self, action: #selector(THShopThreadestBlackViewController.didTapAddSelectSize(_:)), for: UIControlEvents.touchUpInside)
        }

        ShopCell.applepaymentbutton.addTarget(self, action: #selector(THShopThreadestBlackViewController.didTapAddApplePay(_:)), for: UIControlEvents.touchUpInside)
        let pinchZoomProductImage = UIPinchGestureRecognizer.init(target: self, action: #selector(THShopThreadestBlackViewController.pinchZoomProductImage(sender:)))
        ShopCell.shopImage.addGestureRecognizer(pinchZoomProductImage)
        return ShopCell
    }

    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let ShopCell = tableView.dequeueReusableCell(withIdentifier: THShopTableViewCell.className, for: indexPath) as! THShopTableViewCell
        ShopCell.likeButton.removeTarget(nil, action: nil, for: .touchUpInside)
        ShopCell.rethreadButtonTop.removeTarget(nil, action: nil, for: .touchUpInside)
        ShopCell.addToCartButton.removeTarget(nil, action: nil, for: .touchUpInside)
        ShopCell.applepaymentbutton.removeTarget(nil, action: nil, for: .touchUpInside)
        ShopCell.contactButton.removeTarget(nil, action: nil, for: .touchUpInside)
        if let pinchGesture = ShopCell.shopImage.gestureRecognizers?[0] as? UIPinchGestureRecognizer{
            ShopCell.shopImage.removeGestureRecognizer(pinchGesture)
        }
    }


    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 1//50
    }

   /* func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewheader = UIView(frame: CGRect(x: 0, y: 0, width: screenwidth, height: 50))
        viewheader.backgroundColor = UIColor.white
        let followpeoplelabel:UILabel = UILabel(frame: CGRect(x: 55, y: 0, width: screenwidth - 80, height: viewheader.frame.height))
        followpeoplelabel.font = UIFont(name: "SFUIText-Regular", size: 14)
        followpeoplelabel.textColor = UIColor.custColor(r: 74, g: 74, b: 74, a: 1)
        followpeoplelabel.text = "Free shipping & returns on all orders"
        viewheader.addSubview(followpeoplelabel)

        let viewallbutton:UIButton = UIButton(frame: CGRect(x: 10, y: 0, width: 40, height: viewheader.frame.size.height))
        viewallbutton.setImage(UIImage(named: "shop_delivery-truck"), for: UIControlState.normal)
        viewheader.addSubview(viewallbutton)
        return viewheader
    }*/

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 320
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

extension THShopThreadestBlackViewController: SelectedSize {
    func selectedSize(id: Int, product: SelectedProduct) {
        self.selectedSize[id] = product
        self.objTableView.reloadData()
    }
}

extension THShopThreadestBlackViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.delegate?.textFieldDidBeginEdit(textField)
        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.delegate?.textField(textField, shouldChangeCharactersIn: range, replacementString: string)
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension THShopThreadestBlackViewController: PKPaymentAuthorizationViewControllerDelegate {
    @available(iOS 11.0, *)
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
        // Apple Pay Stripe
        STPAPIClient.shared().createToken(with: payment) { (token, error) in

            guard (error == nil) else {
                completion(PKPaymentAuthorizationResult.init(status: PKPaymentAuthorizationStatus.failure, errors: [error!]))
                return
            }

            if let card = token!.allResponseFields["card"] as? NSDictionary {

                let cardToken = card["id"] as! String
                let tokenID = token!.tokenId
                var firstName = ""
                var lastName = ""
                var address1 = ""
                var address2 = ""
                var state = ""
                var city = ""
                var zipCode = ""
                var phoneNo = ""
                var email = ""

                let streets = payment.shippingContact?.postalAddress!.street.components(separatedBy: "\n")

                if let first = payment.shippingContact?.name?.givenName {
                    firstName = first
                }

                if let last = payment.shippingContact?.name?.familyName {
                    lastName = last
                }

                if let add1 = streets?[0] {
                    address1 = add1
                }

                if ((streets?.count)! > 1) {
                    let add2 = streets?[1]
                    address2 = add2!
                }

                if let addState = payment.shippingContact?.postalAddress!.state {
                    state = addState
                }

                if let addCity = payment.shippingContact?.postalAddress!.city  {
                    city = addCity
                }

                if let addZip = payment.shippingContact?.postalAddress!.postalCode  {
                    zipCode = addZip
                }

                if let phone = payment.shippingContact?.phoneNumber  {
                    phoneNo = phone.stringValue
                }

                if let emailAddress = payment.shippingContact?.emailAddress {
                    email = emailAddress
                }
                let productVar = self.selectedVariation!

                THApplePayManager.sharedInstance.instantBuyNative(with: productVar.productId, productVarId: productVar.id,tokenId: tokenID, cardToken: cardToken, firstName: firstName, lastName: lastName, streetAddress1: address1, streetAddress2: address2, state: state, city: city, zipCode: zipCode, phone: phoneNo, email: email, postId: -1, completion: { (error,message ) in

                    guard (error == nil) else {
                        completion(PKPaymentAuthorizationResult.init(status: PKPaymentAuthorizationStatus.failure, errors: nil))
                        return
                    }

                    if message != "" {
                        controller.dismiss(animated: false, completion: {
                            self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "", body: message!, completion: { () in}), animated: false, completion: nil)
                        })
                    } else {
                        completion(PKPaymentAuthorizationResult.init(status: PKPaymentAuthorizationStatus.success, errors: nil))
                        controller.dismiss(animated: false) {
                            //Add funnel for complete purchase
                            let price = self.selectedCategory!.salePrice == 0.0 ? self.selectedCategory!.defaultPrice:self.selectedCategory!.salePrice
                            Answers.logPurchase(withPrice: NSDecimalNumber(value: price), currency: "USD", success: true, itemName: self.selectedCategory!.title, itemType: self.selectedCategory!.brandName, itemId: "\(self.selectedCategory!.Id)", customAttributes: [:])

                            NotificationCenter.default.post(name: Notification.Name(postNotificationType.notificationOrderSucessfully.rawValue), object: nil, userInfo: nil)
                            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                                appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
                                (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
                            }
                        }
                    }
                })
            }
        }
    }
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {

        // Apple Pay Stripe
        STPAPIClient.shared().createToken(with: payment) { (token, error) in

            guard (error == nil) else {
                completion(.failure)
                return
            }

            if let card = token!.allResponseFields["card"] as? NSDictionary {

                let cardToken = card["id"] as! String
                let tokenID = token!.tokenId
                var firstName = ""
                var lastName = ""
                var address1 = ""
                var address2 = ""
                var state = ""
                var city = ""
                var zipCode = ""
                var phoneNo = ""
                var email = ""

                let streets = payment.shippingContact?.postalAddress!.street.components(separatedBy: "\n")

                if let first = payment.shippingContact?.name?.givenName {
                     firstName = first
                }

                if let last = payment.shippingContact?.name?.familyName {
                    lastName = last
                }

                if let add1 = streets?[0] {
                    address1 = add1
                }

                if ((streets?.count)! > 1) {
                    let add2 = streets?[1]
                    address2 = add2!
                }

                if let addState = payment.shippingContact?.postalAddress!.state {
                    state = addState
                }

                if let addCity = payment.shippingContact?.postalAddress!.city  {
                    city = addCity
                }

                if let addZip = payment.shippingContact?.postalAddress!.postalCode  {
                    zipCode = addZip
                }

                if let phone = payment.shippingContact?.phoneNumber  {
                    phoneNo = phone.stringValue
                }

                if let emailAddress = payment.shippingContact?.emailAddress {
                    email = emailAddress
                }
                let productVar = self.selectedVariation!

                THApplePayManager.sharedInstance.instantBuyNative(with: productVar.productId, productVarId: productVar.id,tokenId: tokenID, cardToken: cardToken, firstName: firstName, lastName: lastName, streetAddress1: address1, streetAddress2: address2, state: state, city: city, zipCode: zipCode, phone: phoneNo, email: email, postId: -1, completion: { (error,message ) in

                    guard (error == nil) else {
                        completion(.failure)
                        return
                    }

                    if message != "" {
                        controller.dismiss(animated: false, completion: {
                            self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "", body: message!, completion: { () in}), animated: false, completion: nil)
                        })
                    } else {
                    completion(.success)
                    controller.dismiss(animated: false) {
                        //Add funnel for complete purchase
                        let price = self.selectedCategory!.salePrice == 0.0 ? self.selectedCategory!.defaultPrice:self.selectedCategory!.salePrice
                        Answers.logPurchase(withPrice: NSDecimalNumber(value: price), currency: "USD", success: true, itemName: self.selectedCategory!.title, itemType: self.selectedCategory!.brandName, itemId: "\(self.selectedCategory!.Id)", customAttributes: [:])

                        NotificationCenter.default.post(name: Notification.Name(postNotificationType.notificationOrderSucessfully.rawValue), object: nil, userInfo: nil)
                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
                            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
                            }
                        }
                    }
                })
            }
        }
    }

    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        controller.dismiss(animated: false) {

        }
    }



    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didSelectShippingContact contact: PKContact, completion: @escaping (PKPaymentAuthorizationStatus, [PKShippingMethod], [PKPaymentSummaryItem]) -> Void) {

        guard let zipcode = contact.postalAddress?.postalCode, let city = contact.postalAddress?.city, let state = contact.postalAddress?.state else{
            completion(.invalidShippingPostalAddress, [], [])
            return
        }

        let productVar = self.selectedVariation!

        THApplePayManager.sharedInstance.instantBuyTaxCalculation(with: productVar.productId, zipCode: zipcode, city: city, state: state) { (error, tax) in

            guard (error == nil) else {
                completion(.invalidShippingPostalAddress, [], [])
                return
            }
            let price = self.selectedCategory!.salePrice == 0.0 ? self.selectedCategory!.defaultPrice:self.selectedCategory!.salePrice
            var summaryItems = [PKPaymentSummaryItem]()
            let subtotal = PKPaymentSummaryItem(label: "SUBTOTAL", amount: NSDecimalNumber(value: price))
            let salesTax = PKPaymentSummaryItem(label: "SALES TAX & FEES", amount: NSDecimalNumber(value: tax!))
            let shipping = PKPaymentSummaryItem(label: "SHIPPING", amount: 0)

            let target = Double(price.adding(Float(tax!)))
            let payTarget = PKPaymentSummaryItem(label: "THREADEST INC.", amount: NSDecimalNumber(value: target))

            summaryItems = [subtotal, salesTax, shipping, payTarget]

            completion(.success, [], summaryItems)

        }
    }
}

extension THShopThreadestBlackViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}

