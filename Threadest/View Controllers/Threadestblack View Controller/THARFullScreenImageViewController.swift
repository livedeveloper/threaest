//
//  THFullScreenImageViewController.swift
//  Threadest
//
//  Created by Jaydeep on 21/05/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import IoniconsSwift
import Photos
import ARKit
import Crashlytics

@available(iOS 11.0, *)
class THARFullScreenImageViewController: UIViewController {
    @IBOutlet weak var objCollectionView: UICollectionView!
    
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    var categoryDetail:ClosetCategory!
    
    
    //AR Kit Variables
    let sceneLocationView = SceneLocationView()
    private var writer: SCNVideoWriter? = nil
    var screenCenter: CGPoint?
    
    var recordingTimer: Timer!
    @IBOutlet weak var RecordingLabel: UILabel!
    @IBOutlet weak var recordButton: UIButton!
    var isLoadARObject = true
    var selectedImagePageIndex = 0
    
    func setupScene() {
        sceneLocationView.setUp(viewController: self, session: sceneLocationView.session)
        DispatchQueue.main.async {
            self.screenCenter = self.sceneLocationView.bounds.mid
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
         sceneLocationView.debugOptions = []
        setupScene()
        downloadButton.setImage(Ionicons.iosDownloadOutline.image(30, color: .blueTabbar), for: .normal)
        objCollectionView.register(UINib(nibName: THFullScreenImageCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: THFullScreenImageCollectionViewCell.className)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
        NotificationCenter.default.addObserver(self, selector: #selector(THARFullScreenImageViewController.appGoesBackgroundTimeStopRecording), name: .UIApplicationDidEnterBackground, object: nil)
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //self.tabBarController?.tabBar.isHidden = true
        pageControl.numberOfPages = categoryDetail.productGalleryImages.count
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        appGoesBackgroundTimeStopRecording()
        DispatchQueue.main.async {
            self.sceneLocationView.pause()
        }
    }
    
    func appGoesBackgroundTimeStopRecording() {
        if recordButton.isSelected {
            recordButton.isSelected = false
            if recordingTimer != nil {
                recordingTimer.invalidate()
                recordingTimer = nil
            }
            self.RecordingLabel.alpha = 0
            writer?.finishWriting(completionHandler: { (url) in
                
            })
        }
    }

    
    func loadThreeDModel(threeDUrl: String) {
        if !isLoadARObject {
            return
        }
        isLoadARObject = false
        let modelId = (threeDUrl as NSString).lastPathComponent
        GeneralMethods.sharedInstance.loadNodeWithID((modelId as NSString).deletingPathExtension, modelUrl: threeDUrl, completion: { (node) in
            let annotationNode = LocationAnnotationNode(location: nil, node: node!)
            
            annotationNode.isFirstLoad = false
            annotationNode.isSingle = true
            annotationNode.isShowTapText = false
            annotationNode.scale = SCNVector3(x: 0.3, y: 0.3, z: 0.3)
            annotationNode.x = 0//self.focusSquare!.position.x
            annotationNode.y = -1//self.focusSquare!.position.y
            annotationNode.z = -2//self.focusSquare!.position.z - self.z
            
            annotationNode.name = "\(self.categoryDetail.Id)"
            //self.sceneLocationView.removeLocationNode(locationNode: annotationNode)
            self.sceneLocationView.addNode(locationNode: annotationNode)
        })
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                
                break
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
                break
            default:
                break
            }
        }
    }
    
    @IBAction func didTouchStartRecordVideoButton(_ sender: Any) {
        //recordButton.setImage(UIImage(named: "recordStop"), for: .normal)
        if recordingTimer != nil {
            recordingTimer.invalidate()
            recordingTimer = nil
        }
        recordingTimer = nil
        recordingTimer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: { (timer) in
            self.RecordingLabel.alpha = 0
            UIView.animate(withDuration: 0.4, animations: {
                self.RecordingLabel.alpha = 1
            })
        })
        
        do {
            writer = try SCNVideoWriter(scene: sceneLocationView.scene)
            writer?.updateFrameHandler = { (image, time) in
            }
            writer?.startWriting()
        } catch let e {
            print(e)
        }
    }
    
    
    @IBAction func didTouchStopVideoRecordButton(_ sender: UIButton) {
        recordButton.isSelected = !recordButton.isSelected
        if recordButton.isSelected {
            //Add funnel of closet feed
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Start Record From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
            }
            
            self.didTouchStartRecordVideoButton(self)
        } else {
            if recordingTimer != nil {
                recordingTimer.invalidate()
                recordingTimer = nil
            }
            self.RecordingLabel.alpha = 0
            //Add funnel of closet feed
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Stop Record From Closet", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
            }
            writer?.finishWriting(completionHandler: { [weak self] (url) in
                let videoPreview = THVideoPreviewViewController(nibName: THVideoPreviewViewController.className, bundle: nil)
                videoPreview.videoURL = url
                print("done", url.path)
                self?.present(videoPreview, animated: true, completion: nil)
            })
        }
    }

    
    @IBAction func didTapOnBackButton(_ sender: Any) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func didTapOnDownloadButton(_ sender: Any) {
        if let socialCell = self.objCollectionView.cellForItem(at: IndexPath(item: pageControl.currentPage, section: 0)) as? THFullScreenImageCollectionViewCell {
            let image = socialCell.fullScreenImage.image
            if PHPhotoLibrary.authorizationStatus() == .authorized {
                UIImageWriteToSavedPhotosAlbum(image!, nil, nil, nil)
                self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "Captured", body: "Saved to gallery.", completion: { () in
                }), animated: true, completion: nil)
            } else {
                PHPhotoLibrary.requestAuthorization({ (status) in
                    if status == .authorized {
                        UIImageWriteToSavedPhotosAlbum(image!, nil, nil, nil)
                        self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "Captured", body: "Saved to gallery.", completion: { () in
                        }), animated: true, completion: nil)
                    }
                })
            }
        }
    }
    
    
    func pinchZoomSocialPostImage(sender: UIPinchGestureRecognizer) {
        let tapLocation = sender.location(in: objCollectionView)
        if let indexPath : IndexPath = objCollectionView.indexPathForItem(at: tapLocation) {
            if let socialCell = self.objCollectionView.cellForItem(at: indexPath) as? THFullScreenImageCollectionViewCell {
                TMImageZoom.shared().gestureStateChanged(sender, withZoom: socialCell.fullScreenImage)
            } else {
                TMImageZoom.shared().resetImageZoom()
            }
        } else {
            TMImageZoom.shared().resetImageZoom()
        }
    }
}

@available(iOS 11.0, *)
extension THARFullScreenImageViewController: UICollectionViewDelegate,UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x / objCollectionView.frame.size.width)
        let scrollOffsetPages = Int(scrollView.contentOffset.x / screenwidth)
        if categoryDetail.productGalleryImages[scrollOffsetPages].image_type == "3D" {
            if selectedImagePageIndex != scrollOffsetPages {
                selectedImagePageIndex = scrollOffsetPages
                isLoadARObject = true
                DispatchQueue.main.async {
                    self.sceneLocationView.run()
                }
                self.loadThreeDModel(threeDUrl: categoryDetail.productGalleryImages[scrollOffsetPages].three_dimensional_object_url)
                self.recordButton.isHidden = false
            } else {
                //sceneLocationView.pause()
                selectedImagePageIndex = scrollOffsetPages
                
            }
        } else {
            DispatchQueue.main.async {
                self.sceneLocationView.pause()
            }
            appGoesBackgroundTimeStopRecording()
            selectedImagePageIndex = scrollOffsetPages
            self.recordButton.isHidden = true
        }
    }
}

@available(iOS 11.0, *)
extension THARFullScreenImageViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryDetail.productGalleryImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let fullScreenCell = collectionView.dequeueReusableCell(withReuseIdentifier: THFullScreenImageCollectionViewCell.className, for: indexPath) as! THFullScreenImageCollectionViewCell
        if categoryDetail.productGalleryImages[indexPath.item].image_type == "3D" {
            sceneLocationView.removeFromSuperview()
            fullScreenCell.fullScreenImage.addSubview(sceneLocationView)
            if indexPath.item == 0  {
                recordButton.isHidden = false
                DispatchQueue.main.async {
                    self.sceneLocationView.run()
                }
                self.loadThreeDModel(threeDUrl: categoryDetail.productGalleryImages[indexPath.item].three_dimensional_object_url)
            }
            sceneLocationView.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
        } else {
            let pinchZoomSocilPostImage = UIPinchGestureRecognizer.init(target: self, action: #selector(THFullScreenImageViewController.pinchZoomSocialPostImage(sender:)))
            fullScreenCell.fullScreenImage.addGestureRecognizer(pinchZoomSocilPostImage)
            fullScreenCell.configure(productImage: categoryDetail.productGalleryImages[indexPath.row])
        }
        
        return fullScreenCell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize{
         if categoryDetail.productGalleryImages[indexPath.item].image_type == "3D" {
            return CGSize(width: screenwidth, height: screenheight)
        }
        let imgHeight = CGFloat(categoryDetail.productGalleryImages[indexPath.row].mediumImageHeight)
        let imgWidth = CGFloat(categoryDetail.productGalleryImages[indexPath.row].mediumImageWidth)
        return CGSize(width: screenwidth, height: (imgHeight/imgWidth) * screenwidth)
    }
}

