//
//  THThreadestBlackViewController.swift
//  Threadest
//
//  Created by Jaydeep on 18/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit


class THThreadestBlackViewController: UIViewController {
    //outlet
    @IBOutlet weak var emailAddressTextField: UITextField!
    @IBOutlet weak var requestEarlyAccessButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
    }
    
    private func initUI(){
        emailAddressTextField.layer.cornerRadius = 25
        requestEarlyAccessButton.layer.cornerRadius = 25
        
        emailAddressTextField.attributedPlaceholder = NSAttributedString(string: "Enter email address",
                                                               attributes: [NSForegroundColorAttributeName: UIColor.custColor(r: 155, g: 155, b: 155, a: 1)])
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
        
    @IBAction func didTapOnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
    
    @IBAction func didTapEarlyAccessAction(_ sender: Any) {
        
        guard validateFields() else {
            return
        }
        
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
         THEarlyAccess.sharedInstance.subscribeToEarlyAccess(toEmail: emailAddressTextField.text!) { (error) in
            
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            if let error = error {
                self.showAlert(withMessage: error.localizedDescription)
                return
            } else
            {
               let ahead = THEarlyAccess.sharedInstance.subscriber?.earlyAccessPosition
                print(ahead as Any)
                
                let thBlackShareController = THThreadestBlackShareViewController(nibName: THThreadestBlackShareViewController.className, bundle: nil)
                thBlackShareController.subscriberInfo = THEarlyAccess.sharedInstance.subscriber
                self.navigationController?.pushViewController(thBlackShareController, animated: true)
            }
            
        }
    }
    
    internal override func showAlert(withMessage message: String) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        present(alertController, animated: true, completion: nil)
    }
    
    
    fileprivate func validateFields() -> Bool {
        guard let emailTextFieldText = emailAddressTextField.text, emailTextFieldText.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
            showAlert(withMessage: "Please insert an email!")
            
            return false
        }
        
        guard emailTextFieldText.isValidEmail() else {
            showAlert(withMessage: "Please insert a valid email!")
            
            return false
        }
        return true
    }
    
    
        
}
