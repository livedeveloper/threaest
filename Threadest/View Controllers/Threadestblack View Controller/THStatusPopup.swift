//
//  THStatusPopup.swift
//  Threadest
//
//  Created by Jaydeep on 05/09/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import Lottie

class THStatusPopup: THBaseViewController {
    @IBOutlet weak var giftAnimationView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    var closetCategory: ClosetCategory!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.showAnimate()

        guard let usercurrent = THUserManager.sharedInstance.currentUserdata() else {
            return
        }
        if closetCategory.salePrice == 0.0 {
            messageLabel.text = "Earn 10% of our sale each time someone buys this piece with your link! \n\n Get friends to check out \(closetCategory.title) to increase your status with \(closetCategory.brandName)!"
        } else {
            messageLabel.text = "Get someone to view \(closetCategory.title) and earn \(String(format: "$%.2f", closetCategory.currentUserDiscountAmount)) off as an influencer of \(closetCategory.brandName)! \n\n Earn 10% of our sale each time someone buys this piece with your link."
        }

        let animationView = LOTAnimationView(name: "star")
        animationView.animationProgress = 100
        animationView.backgroundColor = UIColor.clear
        animationView.frame = CGRect(x: 0, y: 0, width: 45, height: 45)
        animationView.contentMode = .scaleAspectFill
        animationView.animationSpeed = 0.5
        giftAnimationView.addSubview(animationView)
        animationView.loopAnimation = false

        animationView.play { (sucess) in
            animationView.pause()
            //animationView.loopAnimation = true
            //animationView.removeFromSuperview()
        }
    }

    @IBAction func didTapOnCloseButton(_ sender: Any) {
        removeAnimate()
    }

    @IBAction func didTapOnCopyLink(_ sender: Any) {
        guard let usercurrent = THUserManager.sharedInstance.currentUserdata() else {
            return
        }
        let pasteBoard = UIPasteboard.general
        pasteBoard.string = "https://www.threadest.com/products/\(self.closetCategory.Id)?b=\(self.closetCategory.brandId)&sc=\(usercurrent.sharePostCode)"

        let animationDoneView = LOTAnimationView(name: "done_button")
        animationDoneView.animationProgress = 100
        animationDoneView.backgroundColor = UIColor.clear
        animationDoneView.frame = CGRect(x: 0, y: 0, width: 400, height: 400)
        animationDoneView.center = view.center
        animationDoneView.contentMode = .scaleAspectFill
        animationDoneView.animationSpeed = 0.8
        view.addSubview(animationDoneView)
        animationDoneView.loopAnimation = false
        animationDoneView.play { (sucess) in
            animationDoneView.pause()
            animationDoneView.removeFromSuperview()
        }
    }

    @IBAction func didTapOnShareNowButton(_ sender: Any) {
        removeAnimate()
        guard let usercurrent = THUserManager.sharedInstance.currentUserdata() else {
            return
        }
        let activityViewController = UIActivityViewController(activityItems: ["https://www.threadest.com/products/\(self.closetCategory.Id)?b=\(self.closetCategory.brandId)&sc=\(usercurrent.sharePostCode)"], applicationActivities: nil)
        DispatchQueue.main.async {
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
}
