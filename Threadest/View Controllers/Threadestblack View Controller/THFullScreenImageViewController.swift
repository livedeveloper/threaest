//
//  THFullScreenImageViewController.swift
//  Threadest
//
//  Created by Jaydeep on 21/05/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import IoniconsSwift
import Photos

class THFullScreenImageViewController: UIViewController {
    @IBOutlet weak var objCollectionView: UICollectionView!

    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    var categoryDetail:ClosetCategory!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        downloadButton.setImage(Ionicons.iosDownloadOutline.image(30, color: .blueTabbar), for: .normal)
        objCollectionView.register(UINib(nibName: THFullScreenImageCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: THFullScreenImageCollectionViewCell.className)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //self.tabBarController?.tabBar.isHidden = true
        pageControl.numberOfPages = categoryDetail.productGalleryImages.count
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                
                break
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
                break
            default:
                break
            }
        }
    }

    @IBAction func didTapOnBackButton(_ sender: Any) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func didTapOnDownloadButton(_ sender: Any) {
        if let socialCell = self.objCollectionView.cellForItem(at: IndexPath(item: pageControl.currentPage, section: 0)) as? THFullScreenImageCollectionViewCell {
            let image = socialCell.fullScreenImage.image
            if PHPhotoLibrary.authorizationStatus() == .authorized {
                UIImageWriteToSavedPhotosAlbum(image!, nil, nil, nil)
                self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "Captured", body: "Saved to gallery.", completion: { () in
                }), animated: true, completion: nil)
            } else {
                PHPhotoLibrary.requestAuthorization({ (status) in
                    if status == .authorized {
                        UIImageWriteToSavedPhotosAlbum(image!, nil, nil, nil)
                        self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "Captured", body: "Saved to gallery.", completion: { () in
                        }), animated: true, completion: nil)
                    }
                })
            }
        }
    }
    
    
    func pinchZoomSocialPostImage(sender: UIPinchGestureRecognizer) {
        let tapLocation = sender.location(in: objCollectionView)
        if let indexPath : IndexPath = objCollectionView.indexPathForItem(at: tapLocation) {
            if let socialCell = self.objCollectionView.cellForItem(at: indexPath) as? THFullScreenImageCollectionViewCell {
                TMImageZoom.shared().gestureStateChanged(sender, withZoom: socialCell.fullScreenImage)
            } else {
                TMImageZoom.shared().resetImageZoom()
            }
        } else {
            TMImageZoom.shared().resetImageZoom()
        }
    }
}

extension THFullScreenImageViewController: UICollectionViewDelegate,UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x / objCollectionView.frame.size.width)
    }
}


extension THFullScreenImageViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryDetail.productGalleryImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let fullScreenCell = collectionView.dequeueReusableCell(withReuseIdentifier: THFullScreenImageCollectionViewCell.className, for: indexPath) as! THFullScreenImageCollectionViewCell
        let pinchZoomSocilPostImage = UIPinchGestureRecognizer.init(target: self, action: #selector(THFullScreenImageViewController.pinchZoomSocialPostImage(sender:)))
        fullScreenCell.fullScreenImage.addGestureRecognizer(pinchZoomSocilPostImage)
        fullScreenCell.configure(productImage: categoryDetail.productGalleryImages[indexPath.row])
        return fullScreenCell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize{
        let imgHeight = CGFloat(categoryDetail.productGalleryImages[indexPath.row].mediumImageHeight)
        let imgWidth = CGFloat(categoryDetail.productGalleryImages[indexPath.row].mediumImageWidth)
        return CGSize(width: screenwidth, height: (imgHeight/imgWidth) * screenwidth)
    }
}
