//
//  THClaimPriceViewController.swift
//  Threadest
//
//  Created by Jaydeep on 09/03/18.
//  Copyright © 2018 JC. All rights reserved.
//

import UIKit
import SDWebImage
class THClaimPriceViewController: THBaseViewController {

    var closetCategory:ClosetCategory!
    @IBOutlet weak var subView: UIView!
    
    @IBOutlet weak var logoImageView: UIImageView!
    
    @IBOutlet weak var messageLabel: UILabel!
    
    open var isClaimPopupClose: (() -> ())?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showAnimate()
        subView.layer.cornerRadius = 10
        subView.layer.masksToBounds = true
        messageLabel.text = "Please see a representative of \(closetCategory.brandName) to claim your prize."
        logoImageView.sd_setImage(with: URL(string: closetCategory.brandLogoImageUrl), placeholderImage: nil, options: .retryFailed, completed: nil)
        logoImageView.layer.cornerRadius = logoImageView.frame.size.width/2
        logoImageView.layer.masksToBounds = true
        // Do any additional setup after loading the view.
    }

    @IBAction func didTapOnOkayButton(_ sender: Any) {
        removeAnimate()
        isClaimPopupClose!()
    }
    
}
