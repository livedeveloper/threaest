//
//  THSuggestionDisplayManager.swift
//  Threadest
//
//  Created by Vladimir Goncharov on 08/06/2017.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

protocol THSuggestionDisplayManagerDelegate: class {
    func suggestionManager(manager: THSuggestionDisplayManager, didTapCloseByIndex: Int, suggestion: Suggestion)
    func suggestionManager(manager: THSuggestionDisplayManager, didTapFollowByIndex: Int, suggestion: Suggestion)
}

//MARK: -
class THSuggestionDisplayManager: NSObject {
    
    var items: [Suggestion]? {
        didSet {
            self.reloadData()
        }
    }
    weak var collectionView: UICollectionView? {
        didSet {
            self.prepare(collectionView: self.collectionView)
        }
    }
    weak var delegate: THSuggestionDisplayManagerDelegate?
    
    private func prepare(collectionView: UICollectionView?) {
        collectionView?.register(THSuggestionUserCell.nib(), forCellWithReuseIdentifier: THSuggestionUserCell.cellIdentifier())
        collectionView?.delegate = self
        collectionView?.dataSource = self
    }
    
    //MARK: - Methods
    func reloadData() {
        self.collectionView?.reloadData()
    }
    
    func reset() {
        self.collectionView = nil
        self.items = nil
        self.delegate = nil
    }
}

//MARK: -
extension THSuggestionDisplayManager: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let suggestion = self.items![indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: THSuggestionUserCell.cellIdentifier(), for: indexPath) as! THSuggestionUserCell
        cell.config(suggestion: suggestion)
        cell.delegate = self
        return cell
    }
}

//MARK: -
extension THSuggestionDisplayManager: UICollectionViewDelegate {
    
}

//MARK: -
extension THSuggestionDisplayManager: THSuggestionUserCellDelegate {
    
    func suggestionUserCellDidTapClose(cell: THSuggestionUserCell) {
        if let index = self.collectionView?.indexPath(for: cell)?.row {
            self.delegate?.suggestionManager(manager: self, didTapCloseByIndex: index, suggestion: self.items![index])
        }
    }
    
    func suggestionUserCellDidTapFollow(cell: THSuggestionUserCell) {
        if let index = self.collectionView?.indexPath(for: cell)?.row {
            self.delegate?.suggestionManager(manager: self, didTapFollowByIndex: index, suggestion: self.items![index])
        }
    }
}

