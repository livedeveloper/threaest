//
//  THFindFriendsConnectViewController.swift
//  Threadest
//
//  Created by Jaydeep on 10/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import Crashlytics
import Lottie
import ARKit

class THFindFriendsConnectViewController: THBaseViewController {
    @IBOutlet weak var inviteFriendButton: DesignableButton!
    @IBOutlet weak var promoCodeTextField: DesignableTextField!
    
    @IBOutlet weak var withyouCodeLabel: UILabel!
    
    var isNav = false
    override func viewDidLoad() {
        super.viewDidLoad()
         view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        //Add funnel for Invite Friend page
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Invite Friend icon", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
        }
        setDoneOnKeyboard()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
         if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
           
           self.withyouCodeLabel.text = "with your code: \(usercurrent.invitePromoCode) & get a free shirt!"
            THUserManager.sharedInstance.retrieveInviteFriendNeededToGetPrice(userId: usercurrent.userId, completion: { (error) in
                var titleMsg = ""
                if THUserManager.sharedInstance.inviteFriendNeededToGetPrice == 0 {
                    self.withyouCodeLabel.text = "Congratulations!"
                    titleMsg = "Get your free t-shirt"
                } else {
                    titleMsg = "Invite \(THUserManager.sharedInstance.inviteFriendNeededToGetPrice) friends"
                }
                self.inviteFriendButton.setTitle(titleMsg, for: .normal)
            })
        }
    }
    
    @IBAction func didTapOnGoButton(_ sender: Any) {
        self.view.endEditing(true)
        if promoCodeTextField.text == "" {
            
        } else {
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                GeneralMethods.sharedInstance.loadLoading(view: self.view)
                THUserManager.sharedInstance.performReedemFromPromoCode(userId: usercurrent.userId, promoCode: promoCodeTextField.text!, completion: { (error) in
                        GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                     var titleMsg = ""
                        if THUserManager.sharedInstance.inviteFriendNeededToGetPrice == 0 {
                            self.promoCodeTextField.text = ""
                            self.withyouCodeLabel.text = "Congratulations!"
                            titleMsg = "Get your free t-shirt"
                        } else {
                            titleMsg = "Invite \(THUserManager.sharedInstance.inviteFriendNeededToGetPrice) friends"
                        }
                    self.inviteFriendButton.setTitle(titleMsg, for: .normal)
                    })
            }
        }
    }
    
    
    func setDoneOnKeyboard() {
        let keyboardToolbar = generateToolbar()
        promoCodeTextField.inputAccessoryView = keyboardToolbar
    }
    
    
    @IBAction func didTapBackButton(_ sender: Any) {
        if isNav {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    
    @IBAction func didTapInviteFriendButton(_ sender: Any) {
        if self.inviteFriendButton.titleLabel?.text == "Get your free t-shirt" {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THBrandProductManager.sharedInstance.retrieveBrandProfile(with: 9, productId: 73567, geofence: false) { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                let threeDAvailable = THBrandProductManager.sharedInstance.brandProduct?.productGalleryImages.contains(where: { (imgGallery) -> Bool in
                    if imgGallery.image_type == "3D" {
                        return true
                    } else {
                        return false
                    }
                })
                
                if threeDAvailable! {
                    if #available(iOS 11.0, *) {
                        if (ARConfiguration.isSupported) {
                            let ARShopShowController = THARShopShowViewController(nibName: THARShopShowViewController.className, bundle: nil)
                            ARShopShowController.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                            self.navigationController?.pushViewController(ARShopShowController, animated: true)
                            return
                        }
                    }
                }
                let shopshowcontroller = THShopshowViewController(nibName: THShopshowViewController.className, bundle: nil)
                shopshowcontroller.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                self.navigationController?.pushViewController(shopshowcontroller, animated: true)
            }
        } else {
            var promoCode = ""
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                promoCode = "Here is your promocode: \(usercurrent.invitePromoCode)"
            }
            let content = "👋, this new fashion app, Threadest for iPhone is giving away free t-shirts when you invite 3 friends. First, sign up and use my promo code: https://appsto.re/us/-umpeb.i \n\(promoCode). Then invite 2 more friends to get a shirt too!"
            let activityViewController = UIActivityViewController(activityItems: [content], applicationActivities: nil)
            DispatchQueue.main.async {
                self.present(activityViewController, animated: true, completion: nil)
            }
        }
    }
}
