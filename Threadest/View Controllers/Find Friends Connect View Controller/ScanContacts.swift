//
//  ScanContacts.swift
//  Threadest
//
//  Created by jigar on 17/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import Foundation
import Contacts

public struct ContactInfo {
    let contactID: String
    let name: String
    let phone: String
    let email: String
    let birthDate: String
    let facebookId:String
}


public class ScanContacts: NSObject {
    
    
    
    var arrImportedFrnd = NSMutableArray()
    var  arrTosend = NSMutableArray()
    var arrContacts : Array<ContactInfo>! = Array()
    
    
    
    
    
    func findContactsOnBackgroundThread(_ completion: @escaping (_ isSuccess:Bool,_ resultArray:Array<ContactInfo>) -> Void) {
        
      DispatchQueue.global(qos: .userInitiated).async(execute: { () -> Void in
        
            // User's Contact Property
            let keysToFetch = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName),CNContactPhoneNumbersKey,CNContactSocialProfilesKey,CNContactEmailAddressesKey,CNContactBirthdayKey,CNContactIdentifierKey] as [Any] //CNContactIdentifierKey
            let fetchRequest = CNContactFetchRequest( keysToFetch: keysToFetch as! [CNKeyDescriptor])
            var contacts = [CNContact]()
            CNContact.localizedString(forKey: CNLabelPhoneNumberiPhone)
            
            if #available(iOS 10.0, *) {
                fetchRequest.mutableObjects = false
            } else {
                // Fallback on earlier versions
            }
            fetchRequest.unifyResults = true
            fetchRequest.sortOrder = .userDefault
            
            let contactStoreID = CNContactStore().defaultContainerIdentifier()
            print("\(contactStoreID)")
            
            
            do {
                
                try CNContactStore().enumerateContacts(with: fetchRequest) { (contact, stop) -> Void in
                    //do something with contact
                    if contact.phoneNumbers.count > 0 {
                        contacts.append(contact)
                    }
                    
                }
            } catch let e as NSError {
                print(e.localizedDescription)
            }
            
            DispatchQueue.main.async(execute: { () -> Void in
                
                
                for person: CNContact in contacts as [CNContact]
                {
                    var ContactName:String!
                    
                    let name: String? = CNContactFormatter.string(from: person, style: .fullName)
                    print(name)
                    
                    if let contactNameVal = CNContactFormatter.string(from: person, style: .fullName)
                    {
                        ContactName = contactNameVal
                    }
                    else
                    {
                        ContactName = ""
                    }
                    
                    var phoneNumber: NSString?
                    var mobiles = [String]()
                    
                    for ContctNumVar: CNLabeledValue in person.phoneNumbers
                    {
                        let MobNumVar  = (ContctNumVar.value ).value(forKey: "digits") as? String
                        print(MobNumVar!)
                        mobiles.append(MobNumVar!)
                        
                    }
                    
                    if(mobiles.count > 0)
                    {
                        for index in 0 ..< mobiles.count
                        {
                            phoneNumber = mobiles[index] as NSString?
                            var strFacebookId : String!
                            strFacebookId = ""
                            
                            if(person.socialProfiles.count > 0)
                            {
                                
                                for i in 0..<person.socialProfiles.count {
                                    if (person.socialProfiles[i].value ).service == CNSocialProfileServiceFacebook {
                                        strFacebookId = (person.socialProfiles[i].value).userIdentifier
                                        
                                    } else {
                                        strFacebookId = ""
                                    }
                                }
                            }
                            else
                            {
                                strFacebookId = ""
                            }
                            
                            let emailsArray = person.emailAddresses
                            let emailId : NSString?
                            if(emailsArray.count > 0)
                            {
                                emailId = person.emailAddresses[0].value
                                
                            }
                            else
                            {
                                emailId = ""
                            }
                            var Bdate: String?
                            
                            if let bDayPro = person.birthday?.date
                            {
                                
                                let dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                                dateFormatter.dateFormat = "MM/dd/yyyy"
                                
                                dateFormatter.timeZone = TimeZone(identifier: "UTC")
                                
                                Bdate = dateFormatter.string(from: bDayPro)
                            }
                            else
                            {
                                Bdate = ""
                            }
                            let recordId = "\(person.identifier)"
                            
                        
                            
                            let singleContact = ContactInfo(contactID: recordId, name: ContactName, phone: phoneNumber! as String, email: emailId! as String, birthDate: Bdate!, facebookId: strFacebookId!)
                            
                            self.arrContacts.append(singleContact)
                            
                            
                        }
                        
                    }
                    else
                    {
                        phoneNumber = ""
                        var strFacebookId : String!
                        strFacebookId = ""
                        
                        if(person.socialProfiles.count > 0)
                        {
                            
                            for i in 0..<person.socialProfiles.count {
                                if (person.socialProfiles[i].value ).service == CNSocialProfileServiceFacebook {
                                    strFacebookId = (person.socialProfiles[i].value).userIdentifier
                                    
                                } else {
                                    strFacebookId = ""
                                }
                            }
                        }
                        else
                        {
                            strFacebookId = ""
                        }
                        
                        let emailsArray = person.emailAddresses
                        let emailId : NSString?
                        if(emailsArray.count > 0)
                        {
                            emailId = person.emailAddresses[0].value
                            
                        }
                        else
                        {
                            emailId = ""
                        }
                        var Bdate: String?
                        
                        if let bDayPro = person.birthday?.date
                        {
                            
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                            dateFormatter.dateFormat = "MM/dd/yyyy"
                            
                            dateFormatter.timeZone = TimeZone(identifier: "UTC")
                            
                            Bdate = dateFormatter.string(from: bDayPro)
                        }
                        else
                        {
                            Bdate = ""
                        }
                        let recordId = "\(person.identifier)"
                        
                        
                        let singleContact = ContactInfo(contactID: recordId, name: ContactName, phone: phoneNumber! as String, email: emailId! as String, birthDate: Bdate!, facebookId: strFacebookId!)
                        
                        self.arrContacts.append(singleContact)

                        
                        
                    }
                    
                    
                }
              
                    
                    completion(true, self.arrContacts)
                    
                
            })
        })
    }

    
    
}
