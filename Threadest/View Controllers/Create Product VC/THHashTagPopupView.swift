//
//  THHashTagPopupView.swift
//  Threadest
//
//  Created by Jaydeep on 15/06/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit


class THHashTagPopupView: THBaseViewController {

    @IBOutlet weak var viewPopup: UIView!
    @IBOutlet weak var hashTagView: UITextView!
    
    open var hashTagcompletionHander : ((_ tag:PostTag)->())?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewPopup.layer.cornerRadius = 5
        viewPopup.layer.masksToBounds = true
        showAnimate()
        
        let tapOutside: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(THHashTagPopupView.dismissKeyboards))
        tapOutside.cancelsTouchesInView = false
        hashTagView.becomeFirstResponder()
        view.addGestureRecognizer(tapOutside)
        // Do any additional setup after loading the view.
    }
    
    func dismissKeyboards() {
        view.endEditing(true)
    }

    @IBAction func didTapOnOutSide(_ sender: Any) {
        removeAnimate()
    }
    
    
    @IBAction func didTapOnCloseButton(_ sender: Any) {
        removeAnimate()
    }

    @IBAction func didTapOnDoneButton(_ sender: Any) {
        if hashTagView.text != "" || hashTagView.text != "#hashtag" {
            hashTagcompletionHander!(PostTag(postId: hashTagView.text, tagType: "Hash Tag"))
        }
        removeAnimate()
    }
}

extension THHashTagPopupView: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "#hashtag" {
            hashTagView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            hashTagView.text = "#hashtag"
        }
    }
    
}


