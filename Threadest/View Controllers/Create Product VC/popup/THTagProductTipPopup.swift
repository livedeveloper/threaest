//
//  THTagProductTipPopup.swift
//  Threadest
//
//  Created by Jaydeep on 09/09/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import Lottie

class THTagProductTipPopup: THBaseViewController {
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var starAnimationView: UIView!
    
    open var tagProductPopupCompletionHander : (()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showAnimate()
        subView.layer.cornerRadius = 2
        // Do any additional setup after loading the view.
        let animationView = LOTAnimationView(name: "star")
        animationView.animationProgress = 100
        animationView.backgroundColor = UIColor.clear
        animationView.frame = CGRect(x: 0, y: 0, width: 45, height: 45)
        animationView.contentMode = .scaleAspectFill
        animationView.animationSpeed = 0.5
        starAnimationView.addSubview(animationView)
        animationView.loopAnimation = false
        
        animationView.play { (sucess) in
            animationView.pause()
            //animationView.loopAnimation = true
            //animationView.removeFromSuperview()
        }
    }

    @IBAction func didTapCloseButton(_ sender: Any) {
        removeAnimate()
    }
    
    @IBAction func didTapOkButton(_ sender: Any) {
        tagProductPopupCompletionHander!()
        removeAnimate()
    }
    
}
