//
//  THProductTagAlertPopup.swift
//  Threadest
//
//  Created by Jaydeep on 18/08/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THProductTagAlertPopup: THBaseViewController {

    @IBOutlet weak var subView: UIView!

    @IBOutlet weak var messageLabel: UILabel!



    override func viewDidLoad() {
        super.viewDidLoad()
        subView.layer.cornerRadius = 2
        showAnimate()

        if let userCurrent = THUserManager.sharedInstance.currentUserdata() {
            messageLabel.text = "@\(userCurrent.username), in order to post on Threadest, you’ll need to tag a product or brand in your post. Start by clicking the highlighted shirt icon to search over thousands products."
        }

    }

    @IBAction func didTapOkayButton(_ sender: Any) {
        removeAnimate()
    }

    @IBAction func didTapCloseButton(_ sender: Any) {
        removeAnimate()
    }

}
