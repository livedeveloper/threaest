//
//  THTagView.swift
//  Threadest
//
//  Created by sagar r on 4/4/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THTagView: UIView {


    static func instance(type: String) -> THTagView {
        
        let tagOuterView = THTagView(frame: CGRect(x: 0.0, y: 0.0, width: 30.0, height: 30.0))
        if type == "blue" {
            tagOuterView.backgroundColor = UIColor.white
        } else {
            tagOuterView.backgroundColor = UIColor.hex("178dcd", alpha: 1.0)
        }
        tagOuterView.layer.cornerRadius = 15.0
        tagOuterView.layer.masksToBounds = true
        tagOuterView.isUserInteractionEnabled = true
        
        let tagInerView = UIView(frame: CGRect(x: 5, y: 5, width: 20, height: 20))
        if type == "blue" {
            tagInerView.backgroundColor = UIColor.hex("178dcd", alpha: 1.0)
        } else {
            tagInerView.backgroundColor = UIColor.black
        }
        tagInerView.layer.cornerRadius = 10.0
        tagInerView.layer.masksToBounds = true
        tagInerView.isUserInteractionEnabled = false
        tagOuterView.addSubview(tagInerView)
        
        return tagOuterView
    }
}
