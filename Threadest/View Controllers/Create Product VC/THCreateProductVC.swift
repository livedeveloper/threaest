//
//  THCreateProductVC.swift
//  Threadest
//
//  Created by sagarr on 3/23/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import AVFoundation
import SwiftyJSON
import AKImageCropperView
import IoniconsSwift
import TOCropViewController

typealias PostTagCompletion = (_ tag:PostTag) -> Void

public struct PostTag {
    let postId: String
    let tagType: String
 }
public struct CreateProductResponse {

    let hasSucceeded: Bool
    let message: String
    let socialPost : SocialInteraction?

}

extension CreateProductResponse: JSONConvertible {
    static func fromJSON(json: JSON) -> CreateProductResponse {
        let success = json["status"].boolValue
        var message = ""
        var socialPost : SocialInteraction?
        // The message can be either an array or a single string
        if let serverMessage = json["message"].string {
            message = serverMessage
        } else if let serverMessageArray = json["message"].array {
            message = serverMessageArray.flatMap { return $0.stringValue }.joined(separator: "\n")
        }
        let data = json["data"]
        if data != JSON.null {
           socialPost = SocialInteraction.fromJSON(json: data)
        }
        return CreateProductResponse(hasSucceeded: success, message: message, socialPost : socialPost)
    }
}


class THCreateProductVC: THBaseViewController {
    //outlet for tool tip
    @IBOutlet var toolTipView: UIView!
    @IBOutlet weak var toolTipLabel: UILabel!
    @IBOutlet weak var tagButton: UIButton!
    @IBOutlet weak var cropButton: UIButton!
    
    @IBOutlet weak var commentButton: UIButton!
    var popover = Popover()
    var isShowTagProductPopup = true

    fileprivate let screenView:UIView = UIView(frame: CGRect(origin: CGPoint.zero, size: TTUtils.screenSize))
    fileprivate let slider:TTSlider = TTSlider(frame: CGRect(origin: CGPoint.zero, size: TTUtils.screenSize))
    fileprivate let textField = TTTextField(y: TTUtils.screenSize.height/2, width: TTUtils.screenSize.width, heightOfScreen: TTUtils.screenSize.height)
    fileprivate var data:[TTFilter] = []

    open var image : UIImage?

    open var completionHander : ((_ createProduxt: CreateProduct, _ isfromCamera: String)->())?

    @IBOutlet weak var viewBottomContainer: UIView!
    @IBOutlet weak var buttonProduct: UIButton!
    @IBOutlet weak var TagProductBrandLabel: UILabel!

    @IBOutlet weak var facebookSwitch: UISwitch!
    @IBOutlet weak var twitterSwitch: UISwitch!

    @IBOutlet weak var removeTagButton: UIButton!
    @IBOutlet weak var removeTagLabel: UILabel!

    @IBOutlet weak var commentTextView: UITextView!

    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var commentDropDownButton: UIButton!

    @IBOutlet weak var bottomCommentViewConstraint: NSLayoutConstraint!

    fileprivate var isAddingTag = false
    fileprivate var lastNormalizedPoint = CGPoint.zero

    fileprivate var arrayTags = [CGPoint]()
    fileprivate var arrayPosts = [PostTag]()
    override func viewDidLoad() {
        super.viewDidLoad()
        cropButton.setImage(Ionicons.iosCropStrong.image(30, color: .white), for: .normal)
        setDoneOnKeyboard()
        screenView.backgroundColor = UIColor.black
        view.insertSubview(screenView, at: 0)

        setupSlider()
        setupTextField()
        self.commentDropDownButton.isSelected = true
       /* let notiCenter = NotificationCenter.default
        notiCenter.addObserver(self, selector: #selector(THCreateProductVC.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        notiCenter.addObserver(self, selector: #selector(THCreateProductVC.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)*/
        /* self.showPopOverToolTip()*/
    }

    override var prefersStatusBarHidden : Bool {
        return true
    }
    

    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
        if isAddingTag {
            isAddingTag = false
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(textField)
    }

    override func viewDidAppear(_ animated: Bool) {
        UIView.setAnimationsEnabled(true)
        if isShowTagProductPopup {
            isShowTagProductPopup = false
            let productTagPopup = THTagProductTipPopup(nibName: THTagProductTipPopup.className, bundle: nil)
            productTagPopup.tagProductPopupCompletionHander = {
                self.onProductTagTapped(self.buttonProduct)
            }
            self.addChildViewController(productTagPopup)
            productTagPopup.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
            self.view.addSubview(productTagPopup.view)
        }
    }

    func setDoneOnKeyboard() {
        let keyboardToolbar = generateToolbar()
        self.commentTextView.inputAccessoryView = keyboardToolbar
    }

    override func dismissKeyboard() {
        view.endEditing(true)
    }

    func doneButtonAction() {
        self.commentTextView.resignFirstResponder()
    }

    func showPopOverToolTip() {
        let options = [
            .type(.up),
            .animationIn(0.3),
            .blackOverlayColor(UIColor.clear),
            .color(UIColor.blueTabbar)
            ] as [PopoverOption]
        toolTipView.sizeToFit()
        if userdefault.object(forKey: userDefaultKey.toolTipCameraFilterDefault.rawValue) == nil {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                self.toolTipLabel.text = "😎😍👍 \nSwipe for filters, add a caption, and hashtag brands you're wearing."
                self.popover = Popover(options: options, showHandler: nil, dismissHandler: nil)
                self.popover.show(self.toolTipView, fromView: self.commentView)
            }
        } else if userdefault.object(forKey: userDefaultKey.toolTipCameraTagDefault.rawValue) == nil {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                self.toolTipLabel.text = "Tap the shirt to tag products on Threadest in your photos. Increase your score for driving views! 😎"
                self.popover = Popover(options: options, showHandler: nil, dismissHandler: nil)
                self.popover.show(self.toolTipView, fromView: self.buttonProduct)
            }
        }
    }

    // MARK: Setup
    fileprivate func setupSlider() {

        if let resizedImage = image!.resizeWithWidth(TTUtils.screenSize.width){
            self.createData(resizedImage)
        }

        self.slider.dataSource = self
        self.slider.isUserInteractionEnabled = true
        self.slider.isMultipleTouchEnabled = true
        self.slider.isExclusiveTouch = false

        self.screenView.addSubview(slider)
        self.slider.reloadData()
    }

    fileprivate func setupTextField() {
        self.screenView.addSubview(textField)

        let tapGesture  = UITapGestureRecognizer()
        tapGesture.addTarget(self, action: #selector(THCreateProductVC.handleTap(_:)))
        tapGesture.delegate = self
        self.slider.addGestureRecognizer(tapGesture)

        NotificationCenter.default.addObserver(self.textField, selector: #selector(TTTextField.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self.textField, selector: #selector(TTTextField.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self.textField, selector: #selector(TTTextField.keyboardTypeChanged(_:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
    }

    // MARK: Functions
    fileprivate func createData(_ image: UIImage) {
        self.data = TTFilter.generateFilters(TTFilter(frame: self.slider.frame, withImage: image), filters: TTFilter.filterNameList)
    }

    //MARK:- Show/HideKeyboard
    func keyboardWillShow(_ notification: Notification) {
        DispatchQueue.main.async {
            let info = notification.userInfo! as Dictionary
            let floatAnimationDuration:TimeInterval = info[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval

            UIView.animate(withDuration: floatAnimationDuration, animations: {
                if let keyboardRectValue = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                   // self.bottomCommentViewConstraint.constant = keyboardRectValue.height
                    self.view.layoutIfNeeded()
                }
            })
        }
    }

    func keyboardWillHide(_ notification: Notification) {
        DispatchQueue.main.async {
            let info = notification.userInfo! as Dictionary
            let floatAnimationDuration:TimeInterval = info[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval
            UIView.animate(withDuration: floatAnimationDuration, animations: {
                self.bottomCommentViewConstraint.constant = 95
                self.view.layoutIfNeeded()
            })
        }
    }



    // MARK: Actions
    @IBAction func didTaponTag(_ sender: UIButton) {
       /* if tagButton.isSelected == false {
            didTapOnRemoveTag(self)
        }*/
        sender.isSelected = !sender.isSelected
        self.buttonProduct.isSelected = false
    }

    @IBAction func onCloseTapped(_ sender: UIButton) {
        self.dismiss(animated: true) {
        }
    }

    @IBAction func onProductTagTapped(_ sender: UIButton) {
        /*if buttonProduct.isSelected == false {
            didTapOnRemoveTag(self)
        }*/
        buttonProduct.isSelected = !buttonProduct.isSelected
        TagProductBrandLabel.textColor = buttonProduct.isSelected ? UIColor.blueTabbar: UIColor.white
        self.tagButton.isSelected = false
    }


    @IBAction func onCreateTapped(_ sender: UIButton) {
        let currentFilter = self.data[slider.currentIndex]
        guard let currentImage = currentFilter.image else {
            return
        }
            var isProductTag = false
            for point in arrayTags {
                if arrayPosts[(arrayTags.index(of: point)!)].tagType == "Product Tag" || arrayPosts[(arrayTags.index(of: point)!)].tagType == "Brand Tag"{
                    isProductTag = true
                }
            }
            if !isProductTag {
                let productTagPopup = THProductTagAlertPopup(nibName: THProductTagAlertPopup.className, bundle: nil)
                self.addChildViewController(productTagPopup)
                productTagPopup.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
                self.view.addSubview(productTagPopup.view)
                return
            }
        var array_tags = [[String:String]]()

        for point in arrayTags {
            var dict_t = [String:String]()
            if arrayPosts[(arrayTags.index(of: point)!)].tagType == "Product Tag" {
                dict_t = ["x": "\(point.x)", "y": "\(point.y-self.frameForPhoto().origin.y)","product_id":arrayPosts[(arrayTags.index(of: point)!)].postId,"tag_type": "Product Tag"]

            } else if arrayPosts[(arrayTags.index(of: point)!)].tagType == "Brand Tag" {
                dict_t = ["x": "\(point.x)", "y": "\(point.y-self.frameForPhoto().origin.y)","brand_id":arrayPosts[(arrayTags.index(of: point)!)].postId,"tag_type": "Brand Tag"]
            } else {
                dict_t = ["x": "\(point.x)", "y": "\(point.y-self.frameForPhoto().origin.y)","hash_tag":arrayPosts[(arrayTags.index(of: point)!)].postId,"tag_type": "Hash Tag"]
            }
          // let dict_t = ["x": "\(point.x)", "y": "\(point.y-self.frameForPhoto().origin.y)","product_id":arrayPosts[(arrayTags.index(of: point)!)].postId,"tag_type": "Product Tag"]
            array_tags.append(dict_t)
        }

        let trimmed = array_tags.description.replacingOccurrences(of: "[\"", with: "{\"").replacingOccurrences(of: "\"]", with: "\"}")

        print(trimmed)
        var userid = -1

        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            userid = usercurrent.userId
        }
        var comment = ""
        if (commentTextView.text != "Add a caption..." && commentTextView.text != "") {
            comment = commentTextView.text
        }

        let object = CreateProduct(content: "", photo_tags: trimmed, product_image: currentImage, comment:comment, usreid: userid, facebook: facebookSwitch.isOn, twitter: twitterSwitch.isOn, tagType: "Product Tag")
        self.dismiss(animated: true) {
            self.completionHander!(object, "true")
        }
    }

    private func didRecievedSingleTap(_ tap:UITapGestureRecognizer){
    }

    @IBAction func didTapOnRemoveTag(_ sender: Any) {
        let myViews = self.screenView.subviews.filter{$0 is THTagView}
        for tagview in myViews {
            tagview.removeFromSuperview()
        }
        arrayTags = [CGPoint]()
        arrayPosts = [PostTag]()
        hideRemoveTabButton()
    }

    @IBAction func didTaponCommentDone(_ sender: Any) {
        self.hideCommentBox()
        if (commentTextView.text != "Write a comment" && commentTextView.text != "") {
            self.textField.text = commentTextView.text
            self.textField.show()
            self.commentTextView.resignFirstResponder()
        }
    }

    @IBAction func didTapOnCommentButton(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if self.commentView.isHidden == true {
            showCommentbox()
        } else {
            hideCommentBox()
        }
    }

    @IBAction func didTapOnCommentDropDownButton(_ sender: Any) {
        if commentDropDownButton.isSelected{
            hideCommentBox()
        } else {
            showCommentbox()
        }
    }

    @IBAction func didTapOnLyrics(_ sender: Any) {
        let searchLyricsViewController = THSearchLyricsViewController(nibName: THSearchLyricsViewController.className, bundle: nil)
        let navigationController = UINavigationController.init(rootViewController: searchLyricsViewController);
        navigationController.setNavigationBarHidden(true, animated: false)
        present(navigationController, animated: true, completion: nil)
    }

    func showRemoveTagButton() {
        self.removeTagLabel.isHidden = false
        self.removeTagButton.isHidden = false
        removeTagLabel.alpha = 0
        removeTagButton.alpha = 0
        UIView.animate(withDuration: 0.5, animations: {
            self.removeTagButton.alpha = 1
            self.removeTagLabel.alpha = 1
        }) { (complete) in

        }
    }

    func hideRemoveTabButton() {
        removeTagLabel.alpha = 1
        removeTagButton.alpha = 1
        UIView.animate(withDuration: 0.5, animations: {
            self.removeTagButton.alpha = 0
            self.removeTagLabel.alpha = 0
        }) { (complete) in
            self.removeTagLabel.isHidden = true
            self.removeTagButton.isHidden = true
        }
    }

    func showCommentbox() {
        self.commentView.isHidden = false
        self.commentView.alpha = 0
        UIView.animate(withDuration: 0.5, animations: {
            self.commentView.alpha = 1
        }) { (completed) in
             self.commentDropDownButton.isSelected = true
        }
    }

    func hideCommentBox() {
        self.commentView.alpha = 1
        UIView.animate(withDuration: 0.5, animations: {
            self.commentView.alpha = 0
        }) { (completed) in
            self.commentView.isHidden = true
            self.commentDropDownButton.isSelected = false
            self.commentTextView.resignFirstResponder()
        }
    }

    //tool tip action methods
    @IBAction func didTapOnToolTipCloseButton(_ sender: Any) {
        self.popover.dismiss()
        if userdefault.object(forKey: userDefaultKey.toolTipCameraFilterDefault.rawValue) == nil {
            userdefault.set("true", forKey: userDefaultKey.toolTipCameraFilterDefault.rawValue)
            userdefault.synchronize()
        } else if userdefault.object(forKey: userDefaultKey.toolTipCameraTagDefault.rawValue) == nil {
            userdefault.set("true", forKey: userDefaultKey.toolTipCameraTagDefault.rawValue)
            userdefault.synchronize()
        }
    }

    @IBAction func didTapOnToolTipNextButton(_ sender: Any) {
        self.popover.dismiss()
        if userdefault.object(forKey: userDefaultKey.toolTipCameraFilterDefault.rawValue) == nil {
            userdefault.set("true", forKey: userDefaultKey.toolTipCameraFilterDefault.rawValue)
            userdefault.synchronize()
            showPopOverToolTip()
        } else if userdefault.object(forKey: userDefaultKey.toolTipCameraTagDefault.rawValue) == nil {
            userdefault.set("true", forKey: userDefaultKey.toolTipCameraTagDefault.rawValue)
            userdefault.synchronize()
             buttonProduct.isSelected = true
        }
    }
    
    @IBAction func didTapOnCropButton(_ sender: Any) {
        let cropViewController = TOCropViewController(croppingStyle: TOCropViewCroppingStyle.default, image: self.image!)
        cropViewController.delegate = self
        present(cropViewController, animated: true, completion: nil)
        /*let imageCroppingVC = THImageCroppingViewController(nibName: THImageCroppingViewController.className, bundle: nil)
        imageCroppingVC.image = image
        imageCroppingVC.CroppingCompletionHander = {
            cropimage in
            self.image = cropimage
            self.didTapOnRemoveTag(self)
            self.createData(self.image!)
            self.setupSlider()
        }
        self.present(imageCroppingVC, animated: true, completion: nil)
    }*/
    
    }
}
    
extension THCreateProductVC: TOCropViewControllerDelegate {
    func cropViewController(_ cropViewController: TOCropViewController, didCropToImage image: UIImage, rect cropRect: CGRect, angle: Int) {
        cropViewController.dismiss(animated: false) {
            let images = UIImage(cgImage: (image.cgImage)!, scale: 2.0, orientation:  .up)

            self.image = images
            self.didTapOnRemoveTag(self)
            self.createData(self.image!)
            self.setupSlider()
        }
    }
}

// MARK: - Extension SNSlider DataSource
extension THCreateProductVC: TTSliderDataSource {
    func numberOfSlides(_ slider: TTSlider) -> Int {
        return data.count
    }

    func slider(_ slider: TTSlider, slideAtIndex index: Int) -> TTFilter {

        return data[index]
    }

    func startAtIndex(_ slider: TTSlider) -> Int {
        return 0
    }
}

// MARK: - Extension Gesture Recognizer Delegate and touch Handler for TextField

extension THCreateProductVC: UIGestureRecognizerDelegate {
    func handleTap(_ tap:UITapGestureRecognizer) {

        if buttonProduct.isSelected{
            let currentFilter = self.data[slider.currentIndex]
            guard currentFilter.image != nil else {
                return
            }
            let touchPoint = tap.location(in: currentFilter);
            let normalizedTapLocation = self.normalizedPositionForPoint(touchPoint, frame: self.frameForPhoto())
            self.tagPhotoAtNormalizedPoint(normalizedPoint: normalizedTapLocation)
            print("Touch point ->>>> %@", touchPoint)
            print("Touch normalised point ->>>> %@", normalizedTapLocation)
        } else if tagButton.isSelected {
            let currentFilter = self.data[slider.currentIndex]
            guard currentFilter.image != nil else {
                return
            }
            let touchPoint = tap.location(in: currentFilter);
            let normalizedTapLocation = self.normalizedPositionForPoint(touchPoint, frame: self.frameForPhoto())
            self.hashTagPhotoAtNormalizedPoint(normalizedPoint: normalizedTapLocation)
            print("Touch point ->>>> %@", touchPoint)
            print("Touch normalised point ->>>> %@", normalizedTapLocation)
        } else{
            self.commentTextView.resignFirstResponder()
            /*if self.commentView.isHidden == true {
                    showCommentbox()
            }*/
            //self.textField.handleTap()
        }
    }

    func normalizedPositionForPoint(_ point: CGPoint, frame: CGRect) -> CGPoint{

        let currentFilter = self.data[slider.startingIndex]
        let pointNew = CGPoint(x: point.x - (frame.origin.x - currentFilter.frame.origin.x), y: point.y - (frame.origin.y - currentFilter.frame.origin.y))
        let normalizedPoint = CGPoint(x: pointNew.x / frame.size.width, y: pointNew.y / frame.size.height)
        return normalizedPoint;
    }

    func frameForPhoto() -> CGRect{

        let currentFilter = self.data[slider.startingIndex]
        guard let currentImage = currentFilter.image else {
            return CGRect.zero
        }
        print("currentImage.size --> %@",currentImage.size)
        var  photoDisplayedFrame : CGRect?;
        if currentFilter.contentMode == .scaleAspectFit{
            photoDisplayedFrame = AVMakeRect(aspectRatio: currentImage.size, insideRect: currentFilter.frame)
        }else if currentFilter.contentMode == .center{

            var photoOrigin = CGPoint.zero
            photoOrigin.x = (currentFilter.frame.size.width - currentImage.size.width) * 1.0
            photoOrigin.y = (currentFilter.frame.size.height - currentImage.size.height) * 1.0
            photoDisplayedFrame = CGRect(x: photoOrigin.x, y: photoOrigin.y, width: currentImage.size.width, height: currentImage.size.height)
        }else{
            photoDisplayedFrame = CGRect.zero
        }


        return photoDisplayedFrame!;
    }

    func hashTagPhotoAtNormalizedPoint(normalizedPoint:CGPoint) {
        let hashTagpopup = THHashTagPopupView(nibName: THHashTagPopupView.className, bundle: nil)
        self.addChildViewController(hashTagpopup)
        hashTagpopup.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
        hashTagpopup.hashTagcompletionHander = { hashTag in

            let tagView = THTagView.instance(type: "black")
            self.startNewTag(tagView: tagView, normalizedPoint: self.lastNormalizedPoint)
            self.arrayPosts.append(hashTag)
        }
        self.view.addSubview(hashTagpopup.view)
        isAddingTag = true
        lastNormalizedPoint = normalizedPoint
    }

    func tagPhotoAtNormalizedPoint(normalizedPoint:CGPoint){

        let searchViewController = THSearchViewController(nibName: THSearchViewController.className, bundle: nil)
        searchViewController.isTagging = true
        let tagCompletionHandler: PostTagCompletion = {postTag in

            let tagView = THTagView.instance(type: "blue")
            self.startNewTag(tagView: tagView, normalizedPoint: self.lastNormalizedPoint)
            self.arrayPosts.append(postTag)
            self.dismiss(animated: true, completion: {
            })
        }
        searchViewController.postTagCompletion = tagCompletionHandler
        let navigationController = UINavigationController.init(rootViewController: searchViewController);
        navigationController.setNavigationBarHidden(true, animated: false)
        present(navigationController, animated: true, completion: nil)
        isAddingTag = true
        lastNormalizedPoint = normalizedPoint

    }

    func startNewTag(tagView:THTagView, normalizedPoint:CGPoint){
        let photoFrame = self.frameForPhoto();
        let tagLocation = CGPoint(x: photoFrame.origin.x + (photoFrame.size.width * normalizedPoint.x), y: photoFrame.origin.y + (photoFrame.size.height * normalizedPoint.y))
        tagView.accessibilityIdentifier = "tagview"
        tagView.frame.origin  = tagLocation
        self.screenView.addSubview(tagView)
        arrayTags.append(tagLocation)
        self.showRemoveTagButton()
    }

    func startNewHashTag(tagView:THTagView, normalizedPoint:CGPoint) {
        let photoFrame = self.frameForPhoto();
        let tagLocation = CGPoint(x: photoFrame.origin.x + (photoFrame.size.width * normalizedPoint.x), y: photoFrame.origin.y + (photoFrame.size.height * normalizedPoint.y))
        tagView.accessibilityIdentifier = "hashtagview"
        tagView.frame.origin  = tagLocation
        self.screenView.addSubview(tagView)
        arrayTags.append(tagLocation)
        self.showRemoveTagButton()
    }
}

extension THCreateProductVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Add a caption..." {
            self.commentTextView.text = ""
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            self.commentTextView.text = "Add a caption..."
        }
    }
}
