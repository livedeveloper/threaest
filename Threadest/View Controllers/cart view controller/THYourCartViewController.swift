//
//  THYourCartViewController.swift
//  Threadest
//
//  Created by Jaydeep on 12/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import CoreLocation
import PassKit
import Stripe
import IoniconsSwift
import Crashlytics
import ARKit

class THYourCartViewController: UIViewController
{
    //Outlet
    @IBOutlet weak var objTableview: UITableView!
    
    @IBOutlet weak var payNowButton: UIButton!
    @IBOutlet weak var AppleCheckoutButton: UIButton!
    var paymentMethods = [(PaymentMethod, Bool)]()
    var selectedIndexPaymentMethods = 0
    var sectionName = ["","Shipping Address","Payment Methods","Order Summary"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Add funnel for cart button
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            Answers.logCustomEvent(withName: "Cart Button", customAttributes: ["userid":usercurrent.userId,
                                                                                     "username": usercurrent.username])
        }
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        objTableview.register(UINib(nibName: THCartTableViewCell.className, bundle: nil), forCellReuseIdentifier: THCartTableViewCell.className)
        objTableview.register(UINib(nibName: THShippingAddressTableViewCell.className, bundle: nil), forCellReuseIdentifier: THShippingAddressTableViewCell.className)
        objTableview.register(UINib(nibName: THOrderSummaryTableViewCell.className, bundle: nil), forCellReuseIdentifier: THOrderSummaryTableViewCell.className)
        objTableview.register(UINib(nibName: THPaymentCardTableViewCell.className, bundle: nil), forCellReuseIdentifier: THPaymentCardTableViewCell.className)
        
        let paymentNetworks = [PKPaymentNetwork.discover, .masterCard, .amex, .visa]
        if PKPassLibrary.isPassLibraryAvailable() {
            if !PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetworks) {
                let paymentbutton = PKPaymentButton(paymentButtonType: .setUp, paymentButtonStyle: .black)
                paymentbutton.frame.size.width = screenwidth-30
                paymentbutton.frame.size.height = 50
                paymentbutton.addTarget(self, action: #selector(THYourCartViewController.didTapToApplePayButton(_:)), for: .touchUpInside)
                AppleCheckoutButton.addSubview(paymentbutton)
            } else {
                let paymentbutton = PKPaymentButton(paymentButtonType: .buy, paymentButtonStyle: .black)
                paymentbutton.frame.size.width = screenwidth-30
                paymentbutton.frame.size.height = 50
                paymentbutton.addTarget(self, action: #selector(THYourCartViewController.didTapToApplePayButton(_:)), for: .touchUpInside)
                AppleCheckoutButton.addSubview(paymentbutton)
            }
        }
        payNowButton.layer.cornerRadius = 5
        payNowButton.isHidden = true
        AppleCheckoutButton.isHidden = false
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        var profileLatitude = THProfileClosetManager.sharedInstance.profile?.latitude
        var profileLongitude = THProfileClosetManager.sharedInstance.profile?.longitude
        
        if profileLatitude == nil || profileLongitude == nil {
            profileLatitude = 41.8837452210701
            profileLongitude = -87.6484460046754
            /*if CLLocationManager.locationServicesEnabled() {
                switch(CLLocationManager.authorizationStatus()) {
                case .notDetermined, .restricted, .denied:
                    print("No access")
                    self.present(GeneralMethods.sharedInstance.AlertviewGeneral(title: "Location", body: "Turn On Location Services to Allow Your Location", cancelbutton: "Cancel", okbutton: "Settings", completion: { () in
                        UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                    }), animated: true, completion: nil)
                    
                case .authorizedAlways, .authorizedWhenInUse:
                    print("Access")
                }
            } else {
                print("Location services are not enabled")
            }*/
            //return
        }
        loadData()
        loadShippingAddress()
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THCartManager.sharedInstance.retrieveCartProducts(with: profileLatitude!, long: profileLongitude!) { (error ) in
            
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            if error == nil {
                self.objTableview.reloadData()
            }
        }
    }
    
    func loadData() {
        // Payment Methods
        let applePayMethod = PaymentMethod(userid: -1, id: -1, token: "", createdAt: "", updatedAt: "", defaultBool: false, thirdPartyAccountId: -1, thirdPartyAccessToken: "", brand: "Apple Pay", last4: "", paymentType: "", paymentObjectId: "")
        self.paymentMethods.insert((applePayMethod, false), at: 0)
        
        THTransactionManager.sharedInstance.retrieUserPaymentMethods { (error ) in
            guard (error == nil) else {
                return
            }
            THTransactionManager.sharedInstance.paymentMethods.map({ (paymentmethod) in
                    self.paymentMethods.append((paymentmethod, false))
            })
            self.objTableview.reloadData()
        }
        
    }
    
    func loadShippingAddress() {
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        //Shipping Address Label
        THShippingManager.sharedInstance.retrieveShippingAddress { (error ) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            guard (error == nil) else {
                return
            }
            self.objTableview.reloadData()
        }
    }

    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                break
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
                break
            default:
                break
            }
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        //self.tabBarController?.tabBar.isHidden = true
//        appdelegate.verticle.scrollEnabled(isEnable: false)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
//        appdelegate.verticle.scrollEnabled(isEnable: true)
    }
    
    
    //navigate view to closet purchased when order done sucessfull
    func navigateToClosetPurchasedWhenOrderDone() {
        //Add funnel for complete purchase
        Answers.logPurchase(withPrice: NSDecimalNumber(value: THCartManager.sharedInstance.cart!.totalPrice), currency: "USD", success: true, itemName: "", itemType: "", itemId: "", customAttributes: [:])
        /*if self.tabBarController == nil {
            self.dismiss(animated: true, completion: {
                self.tabBarController?.selectedIndex = 1
                NotificationCenter.default.post(name: Notification.Name(postNotificationType.notificationOrderSucessfully.rawValue), object: nil, userInfo: nil)
            })
        } else {
            self.tabBarController?.selectedIndex = 1
            NotificationCenter.default.post(name: Notification.Name(postNotificationType.notificationOrderSucessfully.rawValue), object: nil, userInfo: nil)
        }*/
         NotificationCenter.default.post(name: Notification.Name(postNotificationType.notificationOrderSucessfully.rawValue), object: nil, userInfo: nil)
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
        }
    }
    
    func deleteProduct(sender: UIButton) {
        if let cart = THCartManager.sharedInstance.cart {
            let cartItemId = cart.cartItems[sender.tag].id
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THCartManager.sharedInstance.deleteCartProduct(with: cartItemId) { (error ) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                
                guard (error == nil) else {
                    self.showAlert(withMessage: error!.localizedDescription)
                    return
                }
                
                self.objTableview.reloadData()
            }
        }
    }
    
    @IBAction func btnActionBack(_ sender: Any) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapToApplePayButton(_ sender: Any) {
        let paymentNetworks = [PKPaymentNetwork.discover, .masterCard, .amex, .visa]
        if PKPassLibrary.isPassLibraryAvailable() {
            if !PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetworks) {
                self.present(GeneralMethods.sharedInstance.AlertviewGeneral(title: "Add a Credit Card to Wallet", body: "Would you like to add a credit card to your wallet now?", cancelbutton: "No", okbutton: "Yes", completion: { () in
                    let lib = PKPassLibrary()
                    lib.openPaymentSetup()
                }), animated: true, completion: nil)
                return
            }
        }
        if THCartManager.sharedInstance.cart == nil || THCartManager.sharedInstance.cart?.cartItems.count == 0 {
            return
        }
        let paymentRequest = Stripe.paymentRequest(withMerchantIdentifier: STPPaymentConfiguration.shared().appleMerchantIdentifier!)
        
        paymentRequest.supportedNetworks = paymentNetworks
        paymentRequest.requiredShippingAddressFields = [.all]
        
        paymentRequest.paymentSummaryItems = [
            PKPaymentSummaryItem(label: "Subtotal", amount: NSDecimalNumber(value: THCartManager.sharedInstance.cart!.subtotal)),
            PKPaymentSummaryItem(label: "Estimated Tax", amount: NSDecimalNumber(value: THCartManager.sharedInstance.cart!.taxes)),
            PKPaymentSummaryItem(label: "Shipping & Returning Fee", amount: NSDecimalNumber(value: THCartManager.sharedInstance.cart!.shippingCost)),
            PKPaymentSummaryItem(label: "THREADEST INC.", amount: NSDecimalNumber(value: THCartManager.sharedInstance.cart!.totalPrice))
        ]
        
        if  Stripe.canSubmitPaymentRequest(paymentRequest) {
            let paymentAuthorizationVC = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest)
            paymentAuthorizationVC.delegate = self
            self.present(paymentAuthorizationVC, animated: true, completion: nil)
            //Add funnel for Apple pay button
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Apple Pay Button", customAttributes: ["userid":usercurrent.userId,
                                                                                        "username": usercurrent.username, "Price":THCartManager.sharedInstance.cart!.totalPrice])
            }
        } else {
            // there is a problem with your Apple Pay configuration.
        }
    }
    
    
    @IBAction func didTapToPayNow(_ sender: Any) {
        if THCartManager.sharedInstance.cart == nil || THCartManager.sharedInstance.cart?.cartItems.count == 0 {
            return
        }
        let paymentshippingVC = THPaymentShipViewController(nibName: THPaymentShipViewController.className, bundle: nil)
        self.navigationController?.pushViewController(paymentshippingVC, animated: true)
    }
    
    @IBAction func didTapToAddPaymentMethodsAndShipping(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            let popAddShippingAddress = THAddShippingAddressPopupVC(nibName: THAddShippingAddressPopupVC.className, bundle: nil)
            popAddShippingAddress.delegate = self
            self.addChildViewController(popAddShippingAddress)
            popAddShippingAddress.view.frame = self.view.frame
            self.view.addSubview(popAddShippingAddress.view)
            popAddShippingAddress.didMove(toParentViewController: self)
                break
        case 2:
            let addCardViewController = STPAddCardViewController()
            addCardViewController.delegate = self
            // STPAddCardViewController must be shown inside a UINavigationController.
            let navigationController = UINavigationController(rootViewController: addCardViewController)
            self.present(navigationController, animated: true, completion: nil)
            break
        default:
            break
        }
    }
}

extension THYourCartViewController: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            if let cart = THCartManager.sharedInstance.cart {
                GeneralMethods.sharedInstance.loadLoading(view: self.view)
                THBrandProductManager.sharedInstance.retrieveBrandProfile(with: cart.cartItems[indexPath.row].product.brand.id, productId: cart.cartItems[indexPath.row].productid, geofence: false) { (error) in
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    let threeDAvailable = THBrandProductManager.sharedInstance.brandProduct?.productGalleryImages.contains(where: { (imgGallery) -> Bool in
                        if imgGallery.image_type == "3D" {
                            return true
                        } else {
                            return false
                        }
                    })
                    
                    if threeDAvailable! {
                        if #available(iOS 11.0, *) {
                            if (ARConfiguration.isSupported) {
                                let ARShopShowController = THARShopShowViewController(nibName: THARShopShowViewController.className, bundle: nil)
                                ARShopShowController.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                                self.navigationController?.pushViewController(ARShopShowController, animated: true)
                                return
                            }
                        }
                    }
                    let shopshowcontroller = THShopshowViewController(nibName: THShopshowViewController.className, bundle: nil)
                    shopshowcontroller.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                    self.navigationController?.pushViewController(shopshowcontroller, animated: true)
                }
            }
        case 2:
            if indexPath.row == 0 {
                payNowButton.isHidden = true
                AppleCheckoutButton.isHidden = false
            } else {
                payNowButton.isHidden = false
                AppleCheckoutButton.isHidden = true
            }
            selectedIndexPaymentMethods = indexPath.row
            self.objTableview.reloadData()
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            break
        case 2:
            break
        default:
            break
        }
    }
}

extension THYourCartViewController: UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionName.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if let cart = THCartManager.sharedInstance.cart {
                return cart.cartItems.count
            }
        case 1:
            return 1
        case 2:
            return self.paymentMethods.count
        case 3:
            return 1
        default:
            return 0
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: THCartTableViewCell.className, for: indexPath) as! THCartTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.btnDelete.layer.cornerRadius = cell.btnDelete.frame.size.width/2
            cell.btnDelete.tag = indexPath.row
            cell.btnDelete.addTarget(self, action: #selector(self.deleteProduct(sender:)), for: .touchUpInside)
            
            let cartItem = THCartManager.sharedInstance.cart!.cartItems[indexPath.row]
            
            cell.productImage.sd_setImage(with: URL(string: (cartItem.product.defaultImageURL)))
            cell.productName.text = cartItem.product.title
            cell.productPrice.text = String(format: "$%.2f", cartItem.price)
            cell.size.text = "Size : \(cartItem.size)"
            cell.estDelivery.text = "Est. Delivery: \(cartItem.estimatedDelivery)"
            cell.shippedBy.text = "Shipped by \(cartItem.product.brandName)"
            return cell
        case 1:
            let shippingCell = tableView.dequeueReusableCell(withIdentifier: THShippingAddressTableViewCell.className, for: indexPath) as! THShippingAddressTableViewCell
            if let address = THShippingManager.sharedInstance.shippingAddress {
                shippingCell.shippingAddressLabel.text = "\(address.address1) \(address.address2) \(address.city) \(address.state) \(address.zipcode)"
            }
            return shippingCell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: THPaymentCardTableViewCell.className, for: indexPath) as! THPaymentCardTableViewCell
            
            let paymentMethodTuple = self.paymentMethods[indexPath.row]
            let paymentMethod = paymentMethodTuple.0
            
            switch selectedIndexPaymentMethods == indexPath.row {
            case true:
                self.paymentMethods[indexPath.row].1 = true
                cell.btnradio.isSelected = true
            case false:
                self.paymentMethods[indexPath.row].1 = false
                cell.btnradio.isSelected = false
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            if indexPath.row == 0 {
                let paymentNetworks = [PKPaymentNetwork.discover, .masterCard, .amex, .visa]
                if PKPassLibrary.isPassLibraryAvailable() {
                    if !PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetworks) {
                        let paymentbutton = PKPaymentButton(paymentButtonType: .setUp, paymentButtonStyle: .black)
                        paymentbutton.frame.size.width = cell.imgcardtype.frame.size.width
                        paymentbutton.frame.size.height = cell.imgcardtype.frame.size.height
                        cell.imgcardtype.addSubview(paymentbutton)
                    } else {
                        let paymentbutton = PKPaymentButton(paymentButtonType: .buy, paymentButtonStyle: .black)
                        paymentbutton.frame.size.width = cell.imgcardtype.frame.size.width
                        paymentbutton.frame.size.height = cell.imgcardtype.frame.size.height
                        cell.imgcardtype.addSubview(paymentbutton)
                    }
                }
                //cell.imgcardtype.image =
                cell.lblcardtype.text = paymentMethod.brand
            } else {
                cell.imgcardtype.image = UIImage(named: "\(paymentMethod.brand)")
                cell.lblcardtype.text = "Ending in \(paymentMethod.last4)"
            }
            return cell
        case 3:
            let orderCell = tableView.dequeueReusableCell(withIdentifier: THOrderSummaryTableViewCell.className, for: indexPath) as! THOrderSummaryTableViewCell
            if let cart = THCartManager.sharedInstance.cart {
                orderCell.subTotalLabel.text = String(format: "$%.2f", cart.subtotal)
                orderCell.estimatedTaxLabel.text = String(format: "$%.2f", cart.taxes)
                orderCell.shippingReturnFeesLabel.text = String(format: "$%.2f", cart.shippingCost)
                orderCell.totalLabel.text =  String(format: "$%.2f", cart.totalPrice)
            }
            return orderCell
        default:
            let shippingCell = tableView.dequeueReusableCell(withIdentifier: THShippingAddressTableViewCell.className, for: indexPath) as! THShippingAddressTableViewCell
            return shippingCell
        }

    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 100
        case 1:
            return 42
        case 3:
            return 181
        default:
            return 10
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: screenwidth, height: 35))
        headerView.backgroundColor = UIColor.hexa("#178dcd", alpha: 1)
        let headerLabel = UILabel(frame: CGRect(x: 15, y: 0, width: screenwidth-15, height: 35))
        headerLabel.font = UIFont.AppFontBold(15)
        headerLabel.textColor = UIColor.white
        headerLabel.text = sectionName[section]
        headerView.addSubview(headerLabel)
        
        if section == 1 || section == 2 {
            let addButton = UIButton(frame: CGRect(x: screenwidth-50, y: 0, width: 40, height: 35))
            addButton.setImage(Ionicons.iosPlusOutline.image(30, color: .white), for: .normal)
            addButton.tag = section
            addButton.addTarget(self, action: #selector(THYourCartViewController.didTapToAddPaymentMethodsAndShipping(_:)), for: .touchUpInside)
            headerView.addSubview(addButton)
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 1
        }
        return 35.0
    }
    
}


extension THYourCartViewController: PaymentMethodAdded {
    func paymentMethodAdded() {
        self.loadData()
    }
}

extension THYourCartViewController: ShippingAddressUpdated {
    func shippingAddressUpdated() {
        self.loadShippingAddress()
    }
}


extension THYourCartViewController: PKPaymentAuthorizationViewControllerDelegate {
    @available(iOS 11.0, *)
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
        // Apple Pay Stripe
        STPAPIClient.shared().createToken(with: payment) { (token, error) in
            
            guard (error == nil) else {
                completion(PKPaymentAuthorizationResult.init(status: PKPaymentAuthorizationStatus.failure, errors: [error!]))
                return
            }
            
            if let card = token!.allResponseFields["card"] as? NSDictionary {
                
                let cardToken = card["id"] as! String
                let tokenID = token!.tokenId
                var firstName = ""
                var lastName = ""
                var address1 = ""
                var address2 = ""
                var state = ""
                var city = ""
                var zipCode = ""
                var phoneNo = ""
                var email = ""
                
                let streets = payment.shippingContact?.postalAddress!.street.components(separatedBy: "\n")
                
                if let first = payment.shippingContact?.name?.givenName {
                    firstName = first
                }
                
                if let last = payment.shippingContact?.name?.familyName {
                    lastName = last
                }
                
                if let add1 = streets?[0] {
                    address1 = add1
                }
                
                if ((streets?.count)! > 1) {
                    let add2 = streets?[1]
                    address2 = add2!
                }
                
                if let addState = payment.shippingContact?.postalAddress!.state {
                    state = addState
                }
                
                if let addCity = payment.shippingContact?.postalAddress!.city  {
                    city = addCity
                }
                
                if let addZip = payment.shippingContact?.postalAddress!.postalCode  {
                    zipCode = addZip
                }
                
                if let phone = payment.shippingContact?.phoneNumber  {
                    phoneNo = phone.stringValue
                }
                
                if let emailAddress = payment.shippingContact?.emailAddress {
                    email = emailAddress
                }
                
                THApplePayManager.sharedInstance.instantBuyFromCartNative(with: tokenID, cardToken: cardToken, firstName: firstName, lastName: lastName, streetAddress1: address1, streetAddress2: address2, state: state, city: city, zipCode: zipCode, phone: phoneNo, email: email, completion: { (error, message) in
                    
                    guard (error == nil) else {
                        completion(PKPaymentAuthorizationResult.init(status: PKPaymentAuthorizationStatus.failure, errors: nil))
                        return
                    }
                    
                    if message != "" {
                        controller.dismiss(animated: false, completion: {
                            self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "", body: message!, completion: { () in}), animated: false, completion: nil)
                        })
                    } else {
                        completion(PKPaymentAuthorizationResult.init(status: PKPaymentAuthorizationStatus.success, errors: nil))
                        controller.dismiss(animated: false, completion: {
                            self.navigateToClosetPurchasedWhenOrderDone()
                        })
                    }
                })
            }
        }
    }
    
    
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {
        
        // Apple Pay Stripe
        STPAPIClient.shared().createToken(with: payment) { (token, error) in
            
            guard (error == nil) else {
                completion(.failure)
                return
            }
            
            if let card = token!.allResponseFields["card"] as? NSDictionary {
                
                let cardToken = card["id"] as! String
                let tokenID = token!.tokenId
                var firstName = ""
                var lastName = ""
                var address1 = ""
                var address2 = ""
                var state = ""
                var city = ""
                var zipCode = ""
                var phoneNo = ""
                var email = ""
                
                let streets = payment.shippingContact?.postalAddress!.street.components(separatedBy: "\n")
                
                if let first = payment.shippingContact?.name?.givenName {
                    firstName = first
                }
                
                if let last = payment.shippingContact?.name?.familyName {
                    lastName = last
                }
                
                if let add1 = streets?[0] {
                    address1 = add1
                }
                
                if ((streets?.count)! > 1) {
                    let add2 = streets?[1]
                    address2 = add2!
                }
                
                if let addState = payment.shippingContact?.postalAddress!.state {
                    state = addState
                }
                
                if let addCity = payment.shippingContact?.postalAddress!.city  {
                    city = addCity
                }
                
                if let addZip = payment.shippingContact?.postalAddress!.postalCode  {
                    zipCode = addZip
                }
                
                if let phone = payment.shippingContact?.phoneNumber  {
                    phoneNo = phone.stringValue
                }
                
                if let emailAddress = payment.shippingContact?.emailAddress {
                    email = emailAddress
                }
                
                THApplePayManager.sharedInstance.instantBuyFromCartNative(with: tokenID, cardToken: cardToken, firstName: firstName, lastName: lastName, streetAddress1: address1, streetAddress2: address2, state: state, city: city, zipCode: zipCode, phone: phoneNo, email: email, completion: { (error, message) in
                    
                    guard (error == nil) else {
                        completion(.failure)
                        return
                    }
                    
                    if message != "" {
                        controller.dismiss(animated: false, completion: {
                            self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "", body: message!, completion: { () in}), animated: false, completion: nil)
                        })
                    } else {
                        completion(.success)
                        controller.dismiss(animated: false, completion: {
                            self.navigateToClosetPurchasedWhenOrderDone()
                        })
                    }
                })
            }
        }
    }
    
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        controller.dismiss(animated: false) {
            // navigate to closet purchases section to show pending order
        }
    }
    
    
    
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didSelectShippingContact contact: PKContact, completion: @escaping (PKPaymentAuthorizationStatus, [PKShippingMethod], [PKPaymentSummaryItem]) -> Void) {
        
        guard let zipcode = contact.postalAddress?.postalCode, let city = contact.postalAddress?.city, let state = contact.postalAddress?.state else{
            completion(.invalidShippingPostalAddress, [], [])
            return
        }
        
        //let productVar = self.selectedVariation!
        
        
        THApplePayManager.sharedInstance.instantBuyFromCartTaxCalculation(with: zipcode, city: city, state: state) { (error, tax) in
            
            guard (error == nil) else {
                completion(.invalidShippingPostalAddress, [], [])
                return
            }
            
            var summaryItems = [PKPaymentSummaryItem]()
            let subtotal = PKPaymentSummaryItem(label: "Subtotal", amount: NSDecimalNumber(value: THCartManager.sharedInstance.cart!.subtotal))
            let salesTax = PKPaymentSummaryItem(label: "Estimated Tax", amount: NSDecimalNumber(value: tax!))
            let shipping = PKPaymentSummaryItem(label: "Shipping & Returning Fee", amount: NSDecimalNumber(value: THCartManager.sharedInstance.cart!.shippingCost))
            let payTarget = PKPaymentSummaryItem(label: "THREADEST INC.", amount: NSDecimalNumber(value: THCartManager.sharedInstance.cart!.totalPrice))
            
            summaryItems = [subtotal, salesTax, shipping, payTarget]
            
            completion(.success, [], summaryItems)
            
        }
    }
}

extension THYourCartViewController: STPAddCardViewControllerDelegate {
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock) {
       
        if let card = token.allResponseFields["card"] as? NSDictionary {
            let paymentType = card["object"] as! String
            let brand = card["brand"] as! String
            let last4 = card["last4"] as! String
            let paymentObjectId = card["id"] as! String
            
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THTransactionManager.sharedInstance.saveUserPaymentMethod(with: token.tokenId, paymentType: paymentType, brand: brand, last4: Int(last4)!, paymentObjectId: paymentObjectId, completion: { (error) in
                self.dismiss(animated: true, completion: nil)
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                guard (error == nil) else {
                    self.showAlert(withMessage: error!.localizedDescription)
                    return
                }
                
                self.showAlert(title: "Success", message: "Payment Method added successfully", okClick: {
                })
                self.loadData()
            })
        }
    }
    
}
