//
//  THOrderCompleteViewController.swift
//  Threadest
//
//  Created by Jaydeep on 15/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THOrderCompleteViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapClosebutton(_ sender: Any) {
        _ = self.navigationController?.popToRootViewController(animated: true)
    }

    @IBAction func didTapBackToHome(_ sender: Any) {
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
   

}
