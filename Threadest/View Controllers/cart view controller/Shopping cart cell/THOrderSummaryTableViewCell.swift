//
//  THOrderSummaryTableViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 12/07/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import IoniconsSwift

class THOrderSummaryTableViewCell: UITableViewCell {
    @IBOutlet weak var subTotalLabel: UILabel!
    @IBOutlet weak var estimatedTaxLabel: UILabel!
    @IBOutlet weak var shippingReturnFeesLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var viewCartTotal: UIView!
    @IBOutlet weak var promoCodeButton: UIButton!
    @IBOutlet weak var promoCodeTextField: UITextField!
    
    @IBOutlet weak var closePromoCodeButton: UIButton!
    
    @IBOutlet weak var congratulationsLabel: UILabel!
    @IBOutlet weak var promoCodeRefundLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        closePromoCodeButton.setImage(Ionicons.closeCircled.image(20, color: UIColor.black), for: .normal)
        viewCartTotal.layer.shadowColor = UIColor.lightGray.cgColor
        viewCartTotal.layer.shadowOffset = CGSize.zero
        viewCartTotal.layer.shadowOpacity = 0.5
        viewCartTotal.layer.shadowRadius = 2
        setDoneOnKeyboard()
    }
    
    func setDoneOnKeyboard() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(THOrderSummaryTableViewCell.dismissKeyboard))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        promoCodeTextField.inputAccessoryView = keyboardToolbar
    }
    
    func dismissKeyboard() {
        promoCodeTextField.resignFirstResponder()
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
