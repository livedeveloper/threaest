//
//  THPaymentCardTableViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 14/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THPaymentCardTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewout: UIView!
    @IBOutlet weak var btnradio: UIButton!
    @IBOutlet weak var imgcardtype: UIImageView!
    @IBOutlet weak var lblcardtype: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = UITableViewCellSelectionStyle.none
        viewout.layer.cornerRadius = 5
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configCell( paymentMethod : PaymentMethod) {
        
        imgcardtype.image = UIImage(named: "\(paymentMethod.brand)")
        lblcardtype.text = "\(paymentMethod.brand) - \(paymentMethod.last4)"
    }
    
    func cellSelected(selected : Bool) {
        
        if selected == true {
            btnradio.isSelected = true
            viewout.layer.borderWidth = 2
            viewout.layer.borderColor = UIColor(red: 126/255, green: 211/255, blue: 33/255, alpha: 1).cgColor
        } else {
            btnradio.isSelected = false
            viewout.layer.borderWidth = 2
            viewout.layer.borderColor = UIColor.white.cgColor
        }
        
    }
    
}


