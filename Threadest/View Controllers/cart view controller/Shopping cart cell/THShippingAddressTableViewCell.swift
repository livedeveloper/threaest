//
//  THShippingAddressTableViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 11/07/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THShippingAddressTableViewCell: UITableViewCell {

    @IBOutlet weak var shippingAddressLabel: UILabel!
    
    @IBOutlet weak var addButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
