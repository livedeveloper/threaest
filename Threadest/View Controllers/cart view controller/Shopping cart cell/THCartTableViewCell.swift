//
//  THCartTableViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 13/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THCartTableViewCell: UITableViewCell {

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var size: UILabel!
    @IBOutlet weak var estDelivery: UILabel!
    @IBOutlet weak var shippedBy: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
}
