//
//  THPaymentShipViewController.swift
//  Threadest
//
//  Created by Jaydeep on 14/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import PassKit
import Stripe


protocol PaymentMethodAdded {
    func paymentMethodAdded()
}

protocol ShippingAddressUpdated {
    func shippingAddressUpdated()
}

class THPaymentShipViewController: UIViewController {

    @IBOutlet weak var btneditshippingaddress: UIButton!
    @IBOutlet weak var viewtotal: UIView!
    @IBOutlet weak var totalCashLabel: UILabel!

    @IBOutlet weak var applePayButton: UIButton!

    @IBOutlet weak var objtableview: UITableView!
    @IBOutlet weak var shippingAddressLbl: UILabel!

    var paymentRequest: PKPaymentRequest!
    var paymentSucceeded: Bool = false

    var paymentMethods = [(PaymentMethod, Bool)]()
    var selectedIndexPaymentMethods = -1

    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        if let total = THCartManager.sharedInstance.cart?.totalPrice {
            self.totalCashLabel.text = String(format: "$ %.2f", total)//"$\(total)"
        }


        let paymentNetworks = [PKPaymentNetwork.discover, .masterCard, .amex, .visa]
        if PKPassLibrary.isPassLibraryAvailable() {
            if !PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetworks) {
                applePayButton.setImage(UIImage(named: "setup_pay"), for: .normal)
            } else {
                self.applePayButton.isEnabled = Stripe.deviceSupportsApplePay()
                applePayButton.setImage(UIImage(named: "shop_buywithapplepay"), for: .normal)
            }
        }

        objtableview.register(UINib(nibName: THPaymentCardTableViewCell.className, bundle: nil), forCellReuseIdentifier: THPaymentCardTableViewCell.className)
        self.loadData()
        self.loadShippingAddress()

        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)

        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)

        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }

    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                break
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
                break
            default:
                break
            }
        }
    }




    func loadData() {

        // Payment Methods
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THTransactionManager.sharedInstance.retrieUserPaymentMethods { (error ) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            guard (error == nil) else {
                return
            }
            self.paymentMethods = THTransactionManager.sharedInstance.paymentMethods.map({ (paymentmethod) -> (PaymentMethod, Bool) in
                return (paymentmethod, false)
            })
            self.objtableview.reloadData()
        }

    }

    func loadShippingAddress() {
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        //Shipping Address Label
        THShippingManager.sharedInstance.retrieveShippingAddress { (error ) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            guard (error == nil) else {
                return
            }
            let address = THShippingManager.sharedInstance.shippingAddress
            self.shippingAddressLbl.text = "\(address!.address1) \(address!.address2) \(address!.zipcode) \(address!.state) \(address!.city)"
        }
    }

    private func initUI()
    {
        btneditshippingaddress.layer.cornerRadius = btneditshippingaddress.frame.width/2

        viewtotal.layer.shadowColor = UIColor.lightGray.cgColor
        viewtotal.layer.shadowOffset = CGSize.zero
        viewtotal.layer.shadowOpacity = 0.5
        viewtotal.layer.shadowRadius = 2
    }

    //navigate view to closet purchased when order done sucessfull
    func navigateToClosetPurchasedWhenOrderDone() {
        self.view.window!.rootViewController?.dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: Notification.Name(postNotificationType.notificationOrderSucessfully.rawValue), object: nil, userInfo: nil)
        })
    }

    @IBAction func didTapApplePay(_ sender: Any) {

        // Apple Pay Stripe

        let paymentNetworks = [PKPaymentNetwork.discover, .masterCard, .amex, .visa]
        if PKPassLibrary.isPassLibraryAvailable() {
            if !PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetworks) {
                self.present(GeneralMethods.sharedInstance.AlertviewGeneral(title: "Add a Credit Card to Wallet", body: "Would you like to add a credit card to your wallet now?", cancelbutton: "No", okbutton: "Yes", completion: { () in
                    let lib = PKPassLibrary()
                    lib.openPaymentSetup()
                }), animated: true, completion: nil)
                return
            }
        }

        let paymentRequest = Stripe.paymentRequest(withMerchantIdentifier: STPPaymentConfiguration.shared().appleMerchantIdentifier!)

        paymentRequest.supportedNetworks = paymentNetworks
        paymentRequest.requiredShippingAddressFields = [.all]

        paymentRequest.paymentSummaryItems = [
            PKPaymentSummaryItem(label: "Subtotal", amount: NSDecimalNumber(value: THCartManager.sharedInstance.cart!.subtotal)),
            PKPaymentSummaryItem(label: "Estimated Tax", amount: NSDecimalNumber(value: THCartManager.sharedInstance.cart!.taxes)),
            PKPaymentSummaryItem(label: "Shipping & Returning Fee", amount: NSDecimalNumber(value: THCartManager.sharedInstance.cart!.shippingCost)),
            PKPaymentSummaryItem(label: "THREADEST INC.", amount: NSDecimalNumber(value: THCartManager.sharedInstance.cart!.totalPrice))
        ]

        if  Stripe.canSubmitPaymentRequest(paymentRequest) {
            let paymentAuthorizationVC = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest)
            paymentAuthorizationVC.delegate = self
            self.present(paymentAuthorizationVC, animated: true, completion: nil)
        } else {
            // there is a problem with your Apple Pay configuration.
        }
    }

    @IBAction func didTapBackButton(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func didTapAddNewCard(_ sender: Any) {
        let addCardViewController = STPAddCardViewController()
        addCardViewController.delegate = self
        // STPAddCardViewController must be shown inside a UINavigationController.
        let navigationController = UINavigationController(rootViewController: addCardViewController)
        self.present(navigationController, animated: true, completion: nil)
       /* let popAddNewCard = THAddNewCardPopupVC(nibName: THAddNewCardPopupVC.className, bundle: nil)
        popAddNewCard.delegate = self
        self.addChildViewController(popAddNewCard)
        popAddNewCard.view.frame = self.view.frame
        self.view.addSubview(popAddNewCard.view)
        popAddNewCard.didMove(toParentViewController: self)*/
    }

    @IBAction func didTapEditShippingAdd(_ sender: Any) {
        let popAddShippingAddress = THAddShippingAddressPopupVC(nibName: THAddShippingAddressPopupVC.className, bundle: nil)
        popAddShippingAddress.delegate = self
        self.addChildViewController(popAddShippingAddress)
        popAddShippingAddress.view.frame = self.view.frame
        self.view.addSubview(popAddShippingAddress.view)
        popAddShippingAddress.didMove(toParentViewController: self)
    }

    @IBAction func didTapPayNow(_ sender: Any) {
        if self.paymentMethods.count == 0 || selectedIndexPaymentMethods == -1 {
            return
        }
        let selectedPaymentMethods = self.paymentMethods[selectedIndexPaymentMethods].0

        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if let address = THShippingManager.sharedInstance.shippingAddress {
                let fullNameArr = address.name.components(separatedBy: " ")
                let firstName: String = fullNameArr[0]
                let lastName: String = fullNameArr.count > 1 ? fullNameArr[1] : ""
                let payNowOrder = PayNowOrder(orderToken: selectedPaymentMethods.paymentObjectId, orderStreetAddress1: address.address1, orderStreetAddress2: address.address2, orderZipCode: address.zipcode, orderCity: address.city, orderState: address.state, orderFirstName: firstName, orderLastName: lastName, orderPhoneNumber: usercurrent.phoneNumber)


                THPayNowOrderManager.sharedInstance.payNowCreateNativeiosOrder(with: payNowOrder, completion: { (error, message) in
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    guard let error = error else {
                        if message != "" {
                            self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "", body: message!, completion: { () in}), animated: false, completion: nil)
                        } else {
                        self.navigateToClosetPurchasedWhenOrderDone()
                        }
                        return
                    }
                    debugPrint(error)
                })
            }
        }
    }
}

extension THPaymentShipViewController:UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.paymentMethods[indexPath.row].1 = true
        selectedIndexPaymentMethods = indexPath.row
        let cell = objtableview.cellForRow(at: indexPath) as! THPaymentCardTableViewCell
        cell.btnradio.isSelected = true
        cell.viewout.layer.borderWidth = 2
        cell.viewout.layer.borderColor = UIColor(red: 126/255, green: 211/255, blue: 33/255, alpha: 1).cgColor

    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath)
    {
        self.paymentMethods[indexPath.row].1 = false

        let cell = objtableview.cellForRow(at: indexPath) as? THPaymentCardTableViewCell
        cell?.btnradio.isSelected = false
        cell?.viewout.layer.borderWidth = 2
        cell?.viewout.layer.borderColor = UIColor.white.cgColor
    }
}

extension THPaymentShipViewController : UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.paymentMethods.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: THPaymentCardTableViewCell.className, for: indexPath) as! THPaymentCardTableViewCell
        let paymentMethodTuple = self.paymentMethods[indexPath.row]
        let paymentMethod = paymentMethodTuple.0
        cell.cellSelected(selected: paymentMethodTuple.1)
        cell.configCell(paymentMethod: paymentMethod)
        return cell
    }
}

extension THPaymentShipViewController: PaymentMethodAdded {
    func paymentMethodAdded() {
        self.loadData()
    }
}

extension THPaymentShipViewController: ShippingAddressUpdated {
    func shippingAddressUpdated() {
        self.loadShippingAddress()
    }
}


extension THPaymentShipViewController: PKPaymentAuthorizationViewControllerDelegate {

    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {

        // Apple Pay Stripe
        STPAPIClient.shared().createToken(with: payment) { (token, error) in

            guard (error == nil) else {
                completion(.failure)
                return
            }

            if let card = token!.allResponseFields["card"] as? NSDictionary {

                let cardToken = card["id"] as! String
                let tokenID = token!.tokenId
                var firstName = ""
                var lastName = ""
                var address1 = ""
                var address2 = ""
                var state = ""
                var city = ""
                var zipCode = ""
                var phoneNo = ""
                var email = ""

                let streets = payment.shippingContact?.postalAddress!.street.components(separatedBy: "\n")

                if let first = payment.shippingContact?.name?.givenName {
                     firstName = first
                }

                if let last = payment.shippingContact?.name?.familyName {
                    lastName = last
                }

                if let add1 = streets?[0] {
                    address1 = add1
                }

                if ((streets?.count)! > 1) {
                    let add2 = streets?[1]
                    address2 = add2!
                }

                if let addState = payment.shippingContact?.postalAddress!.state {
                    state = addState
                }

                if let addCity = payment.shippingContact?.postalAddress!.city  {
                    city = addCity
                }

                if let addZip = payment.shippingContact?.postalAddress!.postalCode  {
                    zipCode = addZip
                }

                if let phone = payment.shippingContact?.phoneNumber  {
                    phoneNo = phone.stringValue
                }

                if let emailAddress = payment.shippingContact?.emailAddress {
                    email = emailAddress
                }

                THApplePayManager.sharedInstance.instantBuyFromCartNative(with: tokenID, cardToken: cardToken, firstName: firstName, lastName: lastName, streetAddress1: address1, streetAddress2: address2, state: state, city: city, zipCode: zipCode, phone: phoneNo, email: email, completion: { (error, message) in

                    guard (error == nil) else {
                        completion(.failure)
                        return
                    }

                    if message != "" {
                    controller.dismiss(animated: false, completion: {
                    self.present(GeneralMethods.sharedInstance.SimpleAlertview(title: "", body: message!, completion: { () in}), animated: false, completion: nil)
                        })
                    } else {
                        completion(.success)
                        controller.dismiss(animated: false, completion: {
                            self.navigateToClosetPurchasedWhenOrderDone()
                        })
                    }
                })
            }
        }
    }

    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        controller.dismiss(animated: false) {
            // navigate to closet purchases section to show pending order
        }
    }



    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didSelectShippingContact contact: PKContact, completion: @escaping (PKPaymentAuthorizationStatus, [PKShippingMethod], [PKPaymentSummaryItem]) -> Void) {

        guard let zipcode = contact.postalAddress?.postalCode, let city = contact.postalAddress?.city, let state = contact.postalAddress?.state else{
            completion(.invalidShippingPostalAddress, [], [])
            return
        }

        //let productVar = self.selectedVariation!


        THApplePayManager.sharedInstance.instantBuyFromCartTaxCalculation(with: zipcode, city: city, state: state) { (error, tax) in

            guard (error == nil) else {
                completion(.invalidShippingPostalAddress, [], [])
                return
            }

            var summaryItems = [PKPaymentSummaryItem]()
            let subtotal = PKPaymentSummaryItem(label: "Subtotal", amount: NSDecimalNumber(value: THCartManager.sharedInstance.cart!.subtotal))
            let salesTax = PKPaymentSummaryItem(label: "Estimated Tax", amount: NSDecimalNumber(value: tax!))
            let shipping = PKPaymentSummaryItem(label: "Shipping & Returning Fee", amount: NSDecimalNumber(value: THCartManager.sharedInstance.cart!.shippingCost))
            let payTarget = PKPaymentSummaryItem(label: "THREADEST INC.", amount: NSDecimalNumber(value: THCartManager.sharedInstance.cart!.totalPrice))

            summaryItems = [subtotal, salesTax, shipping, payTarget]

            completion(.success, [], summaryItems)

        }
    }
}

extension THPaymentShipViewController: STPAddCardViewControllerDelegate {
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock) {
       
        if let card = token.allResponseFields["card"] as? NSDictionary {
            let paymentType = card["object"] as! String
            let brand = card["brand"] as! String
            let last4 = card["last4"] as! String
            let paymentObjectId = card["id"] as! String
            
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THTransactionManager.sharedInstance.saveUserPaymentMethod(with: token.tokenId, paymentType: paymentType, brand: brand, last4: Int(last4)!, paymentObjectId: paymentObjectId, completion: { (error) in
                self.dismiss(animated: true, completion: nil)
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                guard (error == nil) else {
                    self.showAlert(withMessage: error!.localizedDescription)
                    return
                }
                
                self.showAlert(title: "Success", message: "Payment Method added successfully", okClick: {
                })
                self.loadData()
            })
        }
    }
    
}

