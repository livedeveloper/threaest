//
//  THAddNewCardPopupVC.swift
//  Threadest
//
//  Created by Jaydeep on 15/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import Stripe

class THAddNewCardPopupVC: THBaseViewController {

    @IBOutlet weak var cardNumberTextField: UITextField!
    @IBOutlet weak var expireDateTextField: UITextField!
    @IBOutlet weak var cvcTextField: UITextField!
    
    var delegate: PaymentMethodAdded?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.cardNumberTextField.delegate = self
        self.expireDateTextField.delegate = self
        self.cvcTextField.delegate = self
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        self.showAnimate()
        
    }
    
    func doneButtonAction() {
        self.view.endEditing(true)
    }


    @IBAction func didTaponClose(_ sender: Any) {
        self.removeAnimate()
    }
    
    @IBAction func didTapAddNewCard(_ sender: Any) {
        if cardNumberTextField.text == "" {
            return
        } else if cvcTextField.text == "" {
            return
        } else if expireDateTextField.text == "" {
            return
        }
        // Initiate the card
        let stripCard = STPCard()
        
        // Split the expiration date to extract Month & Year
        if self.expireDateTextField.text!.isEmpty == false {
            let expirationDate = self.expireDateTextField.text?.components(separatedBy: "/")
            if (expirationDate?.count)! < 2 {
                return
            }
            if (expirationDate?[1]) == "" {
                return
            }
            let expMonth = UInt((expirationDate?[0])!)
            let expYear = UInt((expirationDate?[1])!)
            
            // Send the card info to Strip to get the token
            stripCard.number = self.cardNumberTextField.text
            stripCard.cvc = self.cvcTextField.text
            stripCard.expMonth = expMonth!
            stripCard.expYear = expYear!
        }else {
            return
        }
        
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        STPAPIClient.shared().createToken(withCard: stripCard, completion: { (token, error) -> Void in
            
            if error != nil {
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                self.showAlert(withMessage: error!.localizedDescription)
                return
            }
        
            if let card = token!.allResponseFields["card"] as? NSDictionary {
                let paymentType = card["object"] as! String
                let brand = card["brand"] as! String
                let last4 = card["last4"] as! String
                let paymentObjectId = card["id"] as! String
                
                THTransactionManager.sharedInstance.saveUserPaymentMethod(with: token!.tokenId, paymentType: paymentType, brand: brand, last4: Int(last4)!, paymentObjectId: paymentObjectId, completion: { (error) in
                    
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    guard (error == nil) else {
                        self.showAlert(withMessage: error!.localizedDescription)
                        return
                    }
                    
                    self.showAlert(title: "Success", message: "Payment Method added successfully", okClick: {
                        self.delegate!.paymentMethodAdded()
                        self.removeAnimate()
                    })
                })
            }
        })
    }
   
}

extension THAddNewCardPopupVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case cvcTextField:
            if range.length > 0 {
                return true
            }
            if range.location > 3 {
                return false
            }
           /* if (textField.text?.characters.count)! > 3 {
                return false
            } else {
                return true
            }*/
        case expireDateTextField:
            if range.length > 0 {
                return true
            }
            if range.location >= 5 {
                return false
            }
            var originalText = textField.text
            if range.location == 2 {
                originalText?.append("/")
                textField.text = originalText
            }
            return true
            break
        default:
            break
        }
        return true
    }
}




