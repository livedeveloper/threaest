//
//  THAddShippingAddressPopupVC.swift
//  Threadest
//
//  Created by Jaydeep on 15/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THAddShippingAddressPopupVC: THBaseViewController {

    @IBOutlet weak var streetAddress1: UITextField!
    @IBOutlet weak var streetAddress2: UITextField!
    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var state: UITextField!
    @IBOutlet weak var zipcode: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    
    var delegate: ShippingAddressUpdated?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.showAnimate()
        
        let tapOutside: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(THAddShippingAddressPopupVC.dismissKeyboards))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        setDoneOnKeyboard()
        view.addGestureRecognizer(tapOutside)
    }
    
    func setDoneOnKeyboard() {
        let keyboardToolbar = generateToolbar()
        streetAddress1.inputAccessoryView = keyboardToolbar
        streetAddress2.inputAccessoryView = keyboardToolbar
        city.inputAccessoryView = keyboardToolbar
        state.inputAccessoryView = keyboardToolbar
        zipcode.inputAccessoryView = keyboardToolbar
        nameTextField.inputAccessoryView = keyboardToolbar
    }
    
    func dismissKeyboards() {
        view.endEditing(true)
    }

    //MARK: Validations of all textfield
    fileprivate func validateFields() -> Bool {
        guard let nameTextfield = nameTextField.text, nameTextfield.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
            showAlert(withMessage: "Please insert a Name!")
            return false
        }
        
        guard let streetAddressField = streetAddress1.text, streetAddressField.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
            showAlert(withMessage: "Please insert a street address!")
            return false
        }
        
        guard let cityTextFieldText = city.text, cityTextFieldText.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
            showAlert(withMessage: "Please insert a city!")
            return false
        }
        
        guard let stateTextField = state.text, stateTextField.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
            showAlert(withMessage: "Please insert a state!")
            
            return false
        }
        
        guard let zipTextFieldText = zipcode.text, zipTextFieldText.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
            showAlert(withMessage: "Please insert a zipcode!")
            
            return false
        }
        
        return true
    }


    
    @IBAction func didTapCloseButton(_ sender: Any) {
        self.removeAnimate()
    }

    @IBAction func didTapNewShippingAddress(_ sender: Any) {
        guard validateFields() else {
            return
        }

        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        guard THUserManager.sharedInstance.currentUserdata() != nil else {
            return
        }
        THShippingManager.sharedInstance.saveUserShippingAddress(with: self.nameTextField.text!, address1: self.streetAddress1.text!, address2: self.streetAddress2.text!, city: self.city.text!, state: self.state.text!, zipCode: self.zipcode.text!) { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            guard (error == nil) else {
                self.showAlert(withMessage: error!.localizedDescription)
                return
            }
            
            self.showAlert(title: "Success", message: "Shipping Address successfully updated", okClick: {
                self.delegate?.shippingAddressUpdated()
                self.removeAnimate()
            })
        }
    }
}

extension THAddShippingAddressPopupVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        return false
    }
}







