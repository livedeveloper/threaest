//
//  THAlreadyAccountPopupVC.swift
//  Threadest
//
//  Created by Jaydeep on 26/09/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import Crashlytics

class THAlreadyAccountPopupVC: THBaseViewController {
    @IBOutlet weak var subView: UIView!
    open var alreadyAccountCompletionHander : ((_ type: String)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        subView.layer.cornerRadius = 10
        subView.layer.masksToBounds = true
        showAnimate()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func didTapClosetButton(_ sender: Any) {
        removeAnimate()
    }
    
    @IBAction func didTapYesButton(_ sender: Any) {
        removeAnimate()
        self.alreadyAccountCompletionHander!("yes")
    }
    
    @IBAction func didTapNoButton(_ sender: Any) {
        removeAnimate()
        self.alreadyAccountCompletionHander!("no")
    }
}
