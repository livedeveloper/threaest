//
//  THOnboardingViewController.swift
//  Threadest
//
//  Created by Mihai on 27/04/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import Crashlytics

class THOnboardingViewController: UIViewController {
    var isRedirectToLogin: Bool = false
    @IBOutlet weak var onboardingScrollView: UIScrollView!
    @IBOutlet weak var startButton: UIButton!
 
    fileprivate var onboardingViewControllers = [UIViewController]()
    fileprivate var didConfigureControllers = false

    @IBOutlet weak var pageControl: UIPageControl!
    //the bulletin manager.
    var bulletinManager: BulletinManager!
    weak var delegate: OnboardingPageDelegate?
    
    convenience init(onboardingViewControllers: [UIViewController]) {
        self.init(nibName: THOnboardingViewController.className, bundle: nil)
        self.onboardingViewControllers = onboardingViewControllers
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            self.additionalSafeAreaInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        } else {
            // Fallback on earlier versions
        }
        configureOnboarding()
        self.view.alpha = 0
        UIView.animate(withDuration: 1.5) {
            self.view.alpha = 1
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.setNeedsLayout()
        if !didConfigureControllers {
            
            didConfigureControllers = true
            configureOnboarding()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
        if isRedirectToLogin {
            isRedirectToLogin = false
            let loginViewController = THLoginViewController(nibName: THLoginViewController.className, bundle: nil)
            navigationController?.pushViewController(loginViewController, animated: false)
        }
    }

    fileprivate func configureOnboarding() {
        onboardingScrollView.delegate = self
        var currentX: CGFloat = 0.0
        pageControl.currentPage = 0
        pageControl.numberOfPages = onboardingViewControllers.count
        for viewController in onboardingViewControllers {
            addChildViewController(viewController)
            viewController.view.frame = CGRect(x: currentX, y: 0.0, width: onboardingScrollView.frame.width, height: onboardingScrollView.frame.height)
            currentX += onboardingScrollView.frame.width
            viewController.willMove(toParentViewController: self)
            onboardingScrollView.addSubview(viewController.view)
            viewController.didMove(toParentViewController: self)
            
        }
        onboardingScrollView.contentSize = CGSize(width: currentX, height: onboardingScrollView.frame.height-20)
    }
    
    @IBAction func didTapOnStartButton(_ sender: Any) {
        
        bulletinManager = BulletinManager(rootItem: GeneralMethods.sharedInstance.bulletinAlradyAccount(yesCompletion: {
            self.bulletinManager.dismissBulletin(animated: true, completion: {
                let loginViewController = THLoginViewController(nibName: THLoginViewController.className, bundle: nil)
                self.navigationController?.pushViewController(loginViewController, animated: true)
            })
        }, noCompletion: {
            self.bulletinManager.dismissBulletin(animated: true, completion: {
                GeneralMethods.sharedInstance.loadLoading(view: self.view)
                let completion: UserManagerGenericCompletion = { (error) in
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    
                    if let error = error {
                        self.showAlert(withMessage: error.localizedDescription)
                        return
                    } else {
                        //Add funnel for start button
                        Answers.logCustomEvent(withName: "Start", customAttributes: [:])
                        appdelegate.swipeNavigationView()
                    }
                }
                let registration = Registration(username: "", password: "", passwordConfirmation: "", mobilePhoneNumber: "", email: "")
                THUserManager.sharedInstance.registerUser(with: registration, tempAccount: true, completion: completion)
            })
        }))
        if #available(iOS 10, *) {
            self.bulletinManager.backgroundViewStyle = .blurred(style: .light, isDark: false)
        } else {
            self.bulletinManager.backgroundViewStyle = .dimmed
        }
        self.bulletinManager.prepare()
        self.bulletinManager.presentBulletin(above: self)
    }
    
    
}

extension THOnboardingViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x / screenwidth)
        delegate?.onboardingPageIndex(page: pageControl.currentPage)
        
        if Int(scrollView.contentOffset.x / screenwidth) > 0 {
            self.startButton.isHidden = false
        }else{
            self.startButton.isHidden = true
        }
    }
}

extension THOnboardingViewController: OnboardingNavigationDelegate {
    func taponBlueDots() {
        onboardingScrollView.scrollRectToVisible(CGRect(x: screenwidth*2, y: onboardingScrollView.frame.origin.y, width: onboardingScrollView.frame.size.width, height: onboardingScrollView.frame.size.width), animated: true)
    }

    func onboardingPageViewControllerShouldProceed(pageViewController: THOnboardingPageViewController) {
        bulletinManager = BulletinManager(rootItem: GeneralMethods.sharedInstance.bulletinAlradyAccount(yesCompletion: {
            self.bulletinManager.dismissBulletin(animated: true, completion: {
                let loginViewController = THLoginViewController(nibName: THLoginViewController.className, bundle: nil)
                loginViewController.isPopup = true
                let navLoginVC = UINavigationController(rootViewController: loginViewController)
                self.present(navLoginVC, animated: true, completion: nil)
            })
        }, noCompletion: {
            self.bulletinManager.dismissBulletin(animated: true, completion: {
                GeneralMethods.sharedInstance.loadLoading(view: self.view)
                let completion: UserManagerGenericCompletion = { (error) in
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    
                    if let error = error {
                        self.showAlert(withMessage: error.localizedDescription)
                        return
                    } else {
                        //Add funnel for start button
                        Answers.logCustomEvent(withName: "Start", customAttributes: [:])
                        appdelegate.swipeNavigationView()
                    }
                }
                let registration = Registration(username: "", password: "", passwordConfirmation: "", mobilePhoneNumber: "", email: "")
                THUserManager.sharedInstance.registerUser(with: registration, tempAccount: true, completion: completion)
            })
        }))
        if #available(iOS 10, *) {
            self.bulletinManager.backgroundViewStyle = .blurred(style: .light, isDark: false)
        } else {
            self.bulletinManager.backgroundViewStyle = .dimmed
        }
        self.bulletinManager.prepare()
        self.bulletinManager.presentBulletin(above: self)
        /*let AccountPopup = THAlreadyAccountPopupVC(nibName: THAlreadyAccountPopupVC.className, bundle: nil)
        AccountPopup.alreadyAccountCompletionHander = {
            type in
            switch type {
            case "yes":
                let loginViewController = THLoginViewController(nibName: THLoginViewController.className, bundle: nil)
               loginViewController.isPopup = true
                 let navLoginVC = UINavigationController(rootViewController: loginViewController)
                self.present(navLoginVC, animated: true, completion: nil)
                //self.navigationController?.pushViewController(loginViewController, animated: true)
                break
            case "no":
                GeneralMethods.sharedInstance.loadLoading(view: self.view)
                let completion: UserManagerGenericCompletion = { (error) in
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    
                    if let error = error {
                        self.showAlert(withMessage: error.localizedDescription)
                        return
                    } else {
                        //Add funnel for start button
                        Answers.logCustomEvent(withName: "Start", customAttributes: [:])
                        appdelegate.swipeNavigationView()
                    }
                }
                let registration = Registration(username: "", password: "", passwordConfirmation: "", mobilePhoneNumber: "", email: "")
                THUserManager.sharedInstance.registerUser(with: registration, tempAccount: true, completion: completion)
                break
            default:
                break
            }
        }
        self.addChildViewController(AccountPopup)
        AccountPopup.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
        self.view.addSubview(AccountPopup.view)*/
    }
}

protocol OnboardingPageDelegate: class {
    func onboardingPageIndex(page: Int)
}

