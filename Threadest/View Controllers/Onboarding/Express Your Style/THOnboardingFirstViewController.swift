//
//  THOnboardingFirstViewController.swift
//  Threadest
//
//  Created by admin on 3/7/18.
//  Copyright © 2018 JC. All rights reserved.
//

import UIKit
import Crashlytics

class THOnboardingFirstViewController: UIViewController {

    @IBOutlet weak var yourStyleImageView: UIImageView!
    
    fileprivate var yourStyleImage = UIImage()
    var bulletinManager: BulletinManager!
    
    func configure(with image: UIImage, showsPanels: Bool = false, isVideo: Bool, strVideoUrl: String = "") {
        yourStyleImage = image
       
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
        yourStyleImageView.image = yourStyleImage
           
    }   

    @IBAction func didTapOnStartButton(_ sender: Any) {
        bulletinManager = BulletinManager(rootItem: GeneralMethods.sharedInstance.bulletinAlradyAccount(yesCompletion: {
            self.bulletinManager.dismissBulletin(animated: true, completion: {
                let loginViewController = THLoginViewController(nibName: THLoginViewController.className, bundle: nil)
                self.navigationController?.pushViewController(loginViewController, animated: true)
            })
        }, noCompletion: {
            self.bulletinManager.dismissBulletin(animated: true, completion: {
                GeneralMethods.sharedInstance.loadLoading(view: self.view)
                let completion: UserManagerGenericCompletion = { (error) in
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    
                    if let error = error {
                        self.showAlert(withMessage: error.localizedDescription)
                        return
                    } else {
                        //Add funnel for start button
                        Answers.logCustomEvent(withName: "Start", customAttributes: [:])
                        appdelegate.swipeNavigationView()
                    }
                }
                let registration = Registration(username: "", password: "", passwordConfirmation: "", mobilePhoneNumber: "", email: "")
                THUserManager.sharedInstance.registerUser(with: registration, tempAccount: true, completion: completion)
            })
        }))
        if #available(iOS 10, *) {
            self.bulletinManager.backgroundViewStyle = .blurred(style: .light, isDark: false)
        } else {
            self.bulletinManager.backgroundViewStyle = .dimmed
        }
        self.bulletinManager.prepare()
        self.bulletinManager.presentBulletin(above: self)
    }
    
    @IBAction func didTapOnConnectFacebook(_ sender: Any) {
        THUserManager.sharedInstance.performFacebookAuth(forViewController: self) { (userExists, authToken, emailAddress, userId, gender, error) in
            if let error = error {
                if userId != "" && gender != "" && authToken != "" {
                    let alertController = UIAlertController(title: "Email", message: "Please enter your email:", preferredStyle: .alert)
                    let confirmAction = UIAlertAction(title: "Confirm", style: .default) { (_) in
                        if let field = alertController.textFields![0] as? UITextField {
                            // store your data
                            guard let emailAddress = field.text, emailAddress.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
                                return
                            }
                            
                            self.checkUserAlreadyExisted(token: authToken, userid: userId, emailAddress: emailAddress, gender: gender)
                        } else {
                            // user did not fill field
                        }
                    }
                    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
                    alertController.addTextField { (textField) in
                        textField.placeholder = "Email"
                    }
                    alertController.addAction(confirmAction)
                    alertController.addAction(cancelAction)
                    self.present(alertController, animated: true, completion: nil)
                } else {
                    let alertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                    
                    alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                }
            } else {
                if let userExists = userExists {
                    if userExists {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.swipeNavigationView()
                    } else {
                        self.showEmailSignUpController(usingFacebookAuth: true, authToken: authToken, emailAddress: emailAddress, userId: userId, gender: gender)
                    }
                } else {
                    assertionFailure("We should never get here")
                }
            }
        }
    }
    
    @IBAction func didTapSignUpEmail(_ sender: Any) {
        showEmailSignUpController(usingFacebookAuth: false)
        //present(emailSignUpController, animated: true, completion: nil)
    }
    
    func showEmailSignUpController(usingFacebookAuth shouldUseFacebookAuth: Bool, authToken: String = "", emailAddress: String = "", userId: String = "", gender: String = "") {
        let emailSignUpController = THEmailSignUpViewController(nibName: THEmailSignUpViewController.className, bundle: nil)
        
        emailSignUpController.shouldUseFacebookAuth = shouldUseFacebookAuth
        emailSignUpController.facebookAuthToken = authToken
        emailSignUpController.facebookEmailAddress = emailAddress
        emailSignUpController.facebookUserID = userId
        emailSignUpController.facebookGender = gender
        
        navigationController?.pushViewController(emailSignUpController, animated: true)
    }
    
    func checkUserAlreadyExisted(token: String, userid: String, emailAddress: String, gender: String) {
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THUserManager.sharedInstance.performCheckIfAuthUserExists(with: emailAddress, authToken: token, userID: userid, completion: { (userExists, err) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            // Request fails
            if let err = err {
                return
            }
            
            // Request succeeds
            if let userExists = userExists {
                if userExists {
                    // Regular flow -> user exists, go to closet - token saved in performCheck endpoint
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.swipeNavigationView()
                } else {
                    // Redirect to register
                    self.showEmailSignUpController(usingFacebookAuth: true, authToken: token, emailAddress: emailAddress, userId: userid, gender: gender)
                }
            } else {
                
            }
        })
    }
    
}


