//
//  THOnboardingPageViewController.swift
//  Threadest
//
//  Created by Mihai on 27/04/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import AVKit
import Crashlytics

class THOnboardingPageViewController: UIViewController {
    @IBOutlet weak var yourStyleImageView: UIImageView!
    @IBOutlet weak var bigCircleView: UIView!
    @IBOutlet weak var smallCircleView: UIView!
    @IBOutlet weak var onboardingPanel: UIView!
    
    @IBOutlet weak var circleTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var circleLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var infoTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var infoTitleLabel: UILabel!
    @IBOutlet weak var infoSubtitleLabel: UILabel!
    @IBOutlet weak var startButton: UIButton!
    
    @IBOutlet weak var tagButton: UIButton!
    
    @IBOutlet weak var playerView: UIView!
    var isVideoPlayer = false
    var player: AVPlayer!
    var videoUrl = ""
    var bulletinManager: BulletinManager!
    
    weak var delegate: OnboardingNavigationDelegate?
    
    fileprivate var yourStyleImage = UIImage()
    fileprivate var showsOnboardingPanels = true
    fileprivate var didLayoutSubviews = false
    
    func configure(with image: UIImage, showsPanels: Bool = false, isVideo: Bool, strVideoUrl: String = "") {
            videoUrl = strVideoUrl
            isVideoPlayer = isVideo
            yourStyleImage = image
            showsOnboardingPanels = showsPanels
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            self.additionalSafeAreaInsets = UIEdgeInsets(top: -20, left: 0, bottom: 0, right: 0)
        } else {
            // Fallback on earlier versions
        }
        bigCircleView.layer.borderWidth = 1.0
        bigCircleView.layer.borderColor = UIColor.spiroDiscoBall.cgColor
        if isVideoPlayer {
            playerView.isHidden = false
            DispatchQueue.main.async {
                self.player = AVPlayer(url: URL(string: self.videoUrl)!)
                let playerLayer = AVPlayerLayer(player: self.player)
                playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
                playerLayer.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
                self.playerView.layer.addSublayer(playerLayer)
               playerLayer.zPosition = -1
                self.player.seek(to: kCMTimeZero)
                self.player.actionAtItemEnd = .none
                self.player.play()
                NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.player.currentItem, queue: nil, using: { (_) in
                    DispatchQueue.main.async {
                        let t1 = CMTimeMake(0, 1);
                        self.player.seek(to: t1)
                        self.player.play()
                    } })
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isVideoPlayer {
            playerView.isHidden = true
            yourStyleImageView.image = yourStyleImage
            onboardingPanel.isHidden = !showsOnboardingPanels
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.startButton.layer.zPosition = 10
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.setNeedsLayout()
        if !didLayoutSubviews {
            didLayoutSubviews = true
            view.layoutIfNeeded()
        }
    }
    
    @IBAction func didTapGotIt(sender: UIButton) {
        delegate?.onboardingPageViewControllerShouldProceed(pageViewController: self)
    }    
    
    @IBAction func didTapOnTagButton(_ sender: Any) {
        delegate?.taponBlueDots()
    }
}

extension AVPlayer {
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
}

protocol OnboardingNavigationDelegate: class {
    func onboardingPageViewControllerShouldProceed(pageViewController: THOnboardingPageViewController)
    func taponBlueDots()
}
