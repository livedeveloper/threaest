//
//  THFindFriendSocialTableViewCell.swift
//  Threadest
//
//  Created by Jaydeep on 28/12/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THFindFriendSocialTableViewCell: UITableViewCell {

    @IBOutlet weak var facebookButton: DesignableButton!
    @IBOutlet weak var contactButton: DesignableButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
