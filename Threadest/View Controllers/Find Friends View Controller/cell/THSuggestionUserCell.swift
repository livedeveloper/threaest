//
//  THSuggestionUserCell.swift
//  Threadest
//
//  Created by Vladimir Goncharov on 08/06/2017.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

protocol THSuggestionUserCellDelegate: class {
    func suggestionUserCellDidTapClose(cell: THSuggestionUserCell)
    func suggestionUserCellDidTapFollow(cell: THSuggestionUserCell)
}

class THSuggestionUserCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var verifyCheckImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var suggestedLabel: UILabel!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    weak var delegate: THSuggestionUserCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.imageView.layer.cornerRadius = self.imageView.frame.size.width/2
        self.imageView.layer.borderColor = UIColor.white.cgColor
        self.imageView.layer.borderWidth = 4
        self.imageView.layer.masksToBounds = true
        self.followButton.layer.cornerRadius = 6.0
        
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        self.layer.masksToBounds = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
      /*  self.delegate = nil
        self.imageView.sd_cancelCurrentImageLoad()
        self.nameLabel.text = nil
        self.suggestedLabel.text = nil*/
    }
    
    func config(suggestion: Suggestion) {
        self.imageView.sd_setImage(with: URL(string: suggestion.user.imageURL))
        self.nameLabel.text = suggestion.user.username
        self.suggestedLabel.text = suggestion.type.title
    }
    
    @IBAction func didTapCloseButton(sender: UIButton) {
        self.delegate?.suggestionUserCellDidTapClose(cell: self)
    }

    @IBAction func didTapFollowButton(sender: UIButton) {
        self.delegate?.suggestionUserCellDidTapFollow(cell: self)
    }
}

fileprivate extension SuggestionType {
    var title: String? {
        switch self {
        case .facebook: return "Facebook friend"
        case .contacts: return "From your contants"
        case .popular: return "Popular user"
        default: return nil
        }
    }
}
