//
//  THListOfSuggestionUsersCell.swift
//  Threadest
//
//  Created by Vladimir Goncharov on 08/06/2017.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

protocol THListOfSuggestionUsersCellDelegate: class {
    func listOfSuggestionCellDidTapSeeAll(cell: THListOfSuggestionUsersCell)
}

class THListOfSuggestionUsersCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var suggestionLabel: UILabel!
    @IBOutlet weak var seeAllButton: UIButton!
    @IBOutlet weak var suggestionLabelTopConstraint: NSLayoutConstraint!
    
    weak var delegate: THListOfSuggestionUsersCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(THSuggestionUserCell.nib(), forCellWithReuseIdentifier: THSuggestionUserCell.cellIdentifier())
        collectionView.register(THARSearchBrandCollectionViewCell.nib(), forCellWithReuseIdentifier: THARSearchBrandCollectionViewCell.cellIdentifier())
        collectionView.register(THARSearchSocialfeedCollectionViewCell.nib(), forCellWithReuseIdentifier: THARSearchSocialfeedCollectionViewCell.cellIdentifier())
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.delegate = nil
        self.collectionView.setContentOffset(CGPoint.zero, animated: false)
        self.collectionView.delegate = nil
        self.collectionView.dataSource = nil
        self.collectionView.reloadData()
    }
    
    @IBAction func didTapSeeAllButton(sender: UIButton) {
        self.delegate?.listOfSuggestionCellDidTapSeeAll(cell: self)
    }
}
