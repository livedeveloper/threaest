//
//  THFindFriendTableCell.swift
//  Threadest
//
//  Created by jigar on 18/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit

class THFindFriendTableCell: UITableViewCell {
    

    @IBOutlet weak var lblFullName: UILabel!
    
    @IBOutlet weak var lblPhoneNumber: UILabel!
    
    @IBOutlet weak var imgUserContact: UIImageView!
    
    @IBOutlet weak var btnInviteNewFriend: UIButton!
    
    @IBOutlet weak var btnThreadestUser: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
