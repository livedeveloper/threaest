//
//  THFindFriendsViewController.swift
//  Threadest
//
//  Created by Jaydeep on 10/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import MessageUI

class THFindFriendsViewController: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var btnThreadestUsers: UIButton!
    @IBOutlet weak var tblContacts: UITableView!
    var arrContacts : Array<ContactInfo>! = Array()
    @IBOutlet weak var viewInviteAll: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        tblContacts.register(UINib(nibName: THFindFriendTableCell.className, bundle: nil), forCellReuseIdentifier: THFindFriendTableCell.className)
        scanUserContacts()
        // Do any additional setup after loading the view.
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)

        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)

        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }

    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                break
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
                break
            default:
                break
            }
        }
    }


    fileprivate func initUI() {
        btnThreadestUsers.layer.cornerRadius = btnThreadestUsers.frame.width / 2.0
        btnThreadestUsers.layer.masksToBounds = true
        viewInviteAll.layer.shadowColor = UIColor.lightGray.cgColor
        viewInviteAll.layer.shadowOffset = CGSize.zero
        viewInviteAll.layer.shadowOpacity = 0.5
        viewInviteAll.layer.shadowRadius = 2
    }




    @IBAction func didTapBackButton(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func didTapInviteAllAction(_ sender: Any) {
    }

    func scanUserContacts() {
        ScanContacts.shared.findContactsOnBackgroundThread { (true, resultContacts) in
            self.arrContacts = resultContacts
            self.tblContacts.reloadData()

        }
    }

    func inviteFriendToThreadest(_ sender: UIButton) {
        let pointInTable: CGPoint =  sender.convert(sender.bounds.origin, to: tblContacts)
        let cellIndexPath = tblContacts.indexPathForRow(at: pointInTable)
        print((cellIndexPath! as NSIndexPath).row)

        sendSMSText(phoneNumber: arrContacts[(cellIndexPath! as NSIndexPath).row].phone)

    }

    func sendSMSText(phoneNumber: String) {
        var promoCode = ""
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            promoCode = "Here is your promocode: \(usercurrent.invitePromoCode)"
        }
        let content = "👋, this new fashion app, Threadest for iPhone is giving away free t-shirts when you invite 3 friends. First, sign up and use my promo code: https://appsto.re/us/-umpeb.i \n\(promoCode). Then invite 2 more friends to get a shirt too!"
        let activityViewController = UIActivityViewController(activityItems: [content], applicationActivities: nil)
        DispatchQueue.main.async {
            self.present(activityViewController, animated: true, completion: nil)
        }
        

        /*if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            var promoCode = ""
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
               promoCode = "Here is your promocode: \(usercurrent.invitePromoCode)"
            }
            controller.body = "👋, this new fashion app, Threadest for iPhone is giving away free t-shirts when you invite 3 friends. First, sign up and use my promo code: https://appsto.re/us/-umpeb.i \n\(promoCode). Then invite 2 more friends to get a shirt too!"
            controller.recipients = [phoneNumber]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }*/
    }
}


    extension THFindFriendsViewController:UITableViewDelegate
    {
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
        {


        }

        func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath)
        {

        }
    }

    extension THFindFriendsViewController:UITableViewDataSource
    {
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }

        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return arrContacts.count
        }

        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: THFindFriendTableCell.className, for: indexPath) as! THFindFriendTableCell

            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.lblFullName.text = arrContacts[indexPath.row].name
            cell.lblPhoneNumber.text = arrContacts[indexPath.row].phone

            if(indexPath.row == 0)
            {
                cell.btnInviteNewFriend.isHidden = true
            }
            else
            {
                cell.btnInviteNewFriend.isHidden = false
            }

            cell.btnInviteNewFriend.addTarget(self, action: #selector(THFindFriendsViewController.inviteFriendToThreadest(_:)), for: UIControlEvents.touchUpInside)
            return cell
        }
    }

extension THFindFriendsViewController : MFMessageComposeViewControllerDelegate{

    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        //... handle sms screen actions
        self.dismiss(animated: true, completion: nil)
    }
}

