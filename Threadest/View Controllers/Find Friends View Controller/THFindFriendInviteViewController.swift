//
//  THFindFriendInviteViewController.swift
//  Threadest
//
//  Created by Jaydeep on 22/12/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import Contacts

class THFindFriendInviteViewController: UIViewController {

    @IBOutlet weak var badgeLabel: UILabel!
    @IBOutlet weak var objTableView: UITableView!

    var isFromContactBulletin = false
    var completionHander : (()->())?

    override func viewDidLoad() {
        super.viewDidLoad()
        badgeLabel.layer.cornerRadius = 5
        badgeLabel.layer.masksToBounds = true
        objTableView.register(UINib(nibName: THFindFriendSocialTableViewCell.className, bundle: nil), forCellReuseIdentifier: THFindFriendSocialTableViewCell.className)
        objTableView.register(UINib(nibName: THListOfSuggestionUsersCell.className, bundle: nil), forCellReuseIdentifier: THListOfSuggestionUsersCell.className)
        objTableView.register(UINib(nibName: THFriendMatchStyleTableViewCell.className, bundle: nil), forCellReuseIdentifier: THFriendMatchStyleTableViewCell.className)
        let store = CNContactStore()
        store.requestAccess(for: .contacts) { granted, error in}
        // Do any additional setup after loading the view.

        loadMore()
        THUserManager.sharedInstance.performUsertoFollow(completion: { (error) in
            guard let error = error else {
                self.objTableView.reloadData()
                return
            }
            debugPrint(error)
        })
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    func loadMore() {
        self.objTableView.addInfiniteScrolling(actionHandler: {
            THUserManager.sharedInstance.performUsertoFollow(completion: { (error) in
                guard let error = error else {
                    self.objTableView.infiniteScrollingView.stopAnimating();
                    self.objTableView.reloadData()
                    return
                }
                debugPrint(error)
            })
        })
    }


    @IBAction func didTapOnInviteButton(_ sender: Any) {
        let findFriendConnectController = THFindFriendsConnectViewController(nibName: THFindFriendsConnectViewController.className, bundle: nil)
        findFriendConnectController.isNav = true
        self.navigationController?.pushViewController(findFriendConnectController, animated: true)
    }


    @IBAction func didTapOnCloseButton(_ sender: Any) {
        self.dismiss(animated: true) {
            if self.isFromContactBulletin {
                self.completionHander!()
            }
        }
    }

    @IBAction func didTapOnContactButton(_ sender: UIButton) {
        let contactVC = THFriendFacebookContactViewController(nibName: THFriendFacebookContactViewController.className, bundle: nil)
        contactVC.isContact = true
        self.navigationController?.pushViewController(contactVC, animated: true)
    }

    @IBAction func didTapOnFacebookButton(_ sender: UIButton) {
        let contactVC = THFriendFacebookContactViewController(nibName: THFriendFacebookContactViewController.className, bundle: nil)
        contactVC.isContact = false
        self.navigationController?.pushViewController(contactVC, animated: true)
    }

    func didTapFollowButton(_ sender: UIButton) {
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }
        let id = THSocialInteractionPostsManager.sharedInstance.followPeopleList[sender.tag].id
        if let suggestionCell = objTableView.cellForRow(at: IndexPath(row: 0, section: 1)) as? THListOfSuggestionUsersCell {

            suggestionCell.collectionView.performBatchUpdates({
                suggestionCell.collectionView.deleteItems(at: [IndexPath(row: sender.tag, section: 0)])
                THSocialInteractionPostsManager.sharedInstance.followPeopleList.remove(at: sender.tag)
            }, completion: { (true) in
                suggestionCell.collectionView.reloadData()
            })
            if THSocialInteractionPostsManager.sharedInstance.followPeopleList.count == 0 {
                self.objTableView.reloadSections(IndexSet(integer: 1), with: .none)
            }
        }
        THSocialInteractionPostsManager.sharedInstance.performFollowUser(for: id) { (error) in
        }
    }

    func didTapSkipButton(_ sender: UIButton) {
        if let suggestionCell = objTableView.cellForRow(at: IndexPath(row: 0, section: 1)) as? THListOfSuggestionUsersCell {

            suggestionCell.collectionView.performBatchUpdates({
                suggestionCell.collectionView.deleteItems(at: [IndexPath(row: sender.tag, section: 0)])
                THSocialInteractionPostsManager.sharedInstance.followPeopleList.remove(at: sender.tag)
            }, completion: { (true) in
                suggestionCell.collectionView.reloadData()
            })
            if THSocialInteractionPostsManager.sharedInstance.followPeopleList.count == 0 {
                self.objTableView.reloadSections(IndexSet(integer: 1), with: .none)
            }
        }
    }


    @IBAction func didTapFollowUnFollowButton(_ sender: UIButton) {
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions(), animations: {

                sender.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)

            }, completion: {_ in

                UIView.animate(withDuration: 0.2, animations: {
                    sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                })
                if sender.isSelected {
                    THSocialInteractionPostsManager.sharedInstance.performUnfollowUser(for: THUserManager.sharedInstance.usersToFollow[sender.tag].id, completion: { (error) in
                        sender.isSelected = false
                        THUserManager.sharedInstance.usersToFollow[sender.tag].isFollow = false
                    })

                }
                else {
                    THUserManager.sharedInstance.performFollowUser(with: THUserManager.sharedInstance.usersToFollow[sender.tag].id, completion: { (error) in
                        guard let error = error else {
                            sender.isSelected = true
                            THUserManager.sharedInstance.usersToFollow[sender.tag].isFollow = true
                            return
                        }
                        debugPrint(error)
                    })
                }
            })
        }
    }

    @IBAction func followUnfollowTouchDragExist(_ sender: DesignableButton) {
        UIView.animate(withDuration: 0.1, delay: 0.0, options: UIViewAnimationOptions(), animations: {
            sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }, completion: { _ in
        })
    }


    @IBAction func followUnfollowTouchDown(_ sender: UIButton) {
        UIView.animate(withDuration: 0.0, animations: {
            sender.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        })
    }

    @IBAction func didTapOnBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension THFindFriendInviteViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2 {
            let user = THUserManager.sharedInstance.usersToFollow[indexPath.row]
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            THProfilePostsManager.sharedInstance.profilePosts = [SocialInteraction]()
            THProfilePostsManager.sharedInstance.page = 1
            THProfilePostsManager.sharedInstance.didLoadData = false
            THProfilePostsManager.sharedInstance.retrieveProfilePosts(with: user.id) { (error) in
                guard error != nil else {
                    GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let userPostProfileVC = storyBoard.instantiateViewController(withIdentifier: PZProfileTableViewController.className) as! PZProfileTableViewController
                    userPostProfileVC.profile = THProfilePostsManager.sharedInstance.profile
                    userPostProfileVC.isSearch = true
                    userPostProfileVC.userId = user.id
                    self.navigationController?.pushViewController(userPostProfileVC, animated: true)
                    return
                }
            }
        }
    }
}


extension THFindFriendInviteViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return THSocialInteractionPostsManager.sharedInstance.followPeopleList.count > 0 ? 1:0
        }
        return section == 2 ? THUserManager.sharedInstance.usersToFollow.count:1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let socialCell = tableView.dequeueReusableCell(withIdentifier: THFindFriendSocialTableViewCell.className, for: indexPath) as! THFindFriendSocialTableViewCell
            socialCell.selectionStyle = .none
            socialCell.contactButton.addTarget(self, action: #selector(THFindFriendInviteViewController.didTapOnContactButton(_:)), for: .touchUpInside)
            socialCell.facebookButton.addTarget(self, action: #selector(THFindFriendInviteViewController.didTapOnFacebookButton(_:)), for: .touchUpInside)
            return socialCell
        case 1:
            let suggestionCell = tableView.dequeueReusableCell(withIdentifier: THListOfSuggestionUsersCell.className, for: indexPath) as! THListOfSuggestionUsersCell
            suggestionCell.collectionView.delegate = self
            suggestionCell.collectionView.dataSource = self
            suggestionCell.collectionView.reloadData()
            suggestionCell.selectionStyle = .none
            return suggestionCell
        case 2:
            let matchStyleCell = tableView.dequeueReusableCell(withIdentifier: THFriendMatchStyleTableViewCell.className, for: indexPath) as! THFriendMatchStyleTableViewCell
            let user = THUserManager.sharedInstance.usersToFollow[indexPath.row]
            matchStyleCell.profileImageView.sd_setImage(with: URL(string: user.thumbnailImageUrl), placeholderImage: nil, options: .retryFailed, completed: nil)
            matchStyleCell.followButton.tag = indexPath.row
            matchStyleCell.followButton.addTarget(self, action: #selector(THFindFriendInviteViewController.didTapFollowUnFollowButton(_:)), for: .touchUpInside)
            matchStyleCell.followButton.addTarget(self, action: #selector(THFindFriendInviteViewController.followUnfollowTouchDown(_:)), for: .touchDown)
            matchStyleCell.followButton.addTarget(self, action: #selector(THFindFriendInviteViewController.followUnfollowTouchDragExist(_:)), for: .touchDragExit)
            matchStyleCell.usernameLabel.text = user.username
            matchStyleCell.followButton.isSelected = user.isFollow
            matchStyleCell.selectionStyle = .none
            return matchStyleCell
        default:
            return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 2 ? "PEOPLE WITH STYLE":""
    }
}

extension THFindFriendInviteViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let user = THSocialInteractionPostsManager.sharedInstance.followPeopleList[indexPath.item]
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THProfilePostsManager.sharedInstance.profilePosts = [SocialInteraction]()
        THProfilePostsManager.sharedInstance.page = 1
        THProfilePostsManager.sharedInstance.didLoadData = false
        THProfilePostsManager.sharedInstance.retrieveProfilePosts(with: user.id) { (error) in
            guard error != nil else {
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber - 1
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let userPostProfileVC = storyBoard.instantiateViewController(withIdentifier: PZProfileTableViewController.className) as! PZProfileTableViewController
                userPostProfileVC.profile = THProfilePostsManager.sharedInstance.profile
                userPostProfileVC.isSearch = true
                userPostProfileVC.userId = user.id
                self.navigationController?.pushViewController(userPostProfileVC, animated: true)
                return
            }
        }

    }
}

extension THFindFriendInviteViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return THSocialInteractionPostsManager.sharedInstance.followPeopleList.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: THSuggestionUserCell.cellIdentifier(), for: indexPath) as! THSuggestionUserCell
        let userFollow = THSocialInteractionPostsManager.sharedInstance.followPeopleList[indexPath.item]

        cell.imageView.sd_setImage(with: URL(string: userFollow.thumbnailImageUrl), placeholderImage: nil, options: .retryFailed, completed: nil)

        cell.nameLabel.text = userFollow.username
        cell.followButton.tag = indexPath.item
        cell.closeButton.tag = indexPath.item
        cell.closeButton.addTarget(self, action: #selector(THFindFriendInviteViewController.didTapSkipButton(_:)), for: .touchUpInside)
        cell.followButton.addTarget(self, action: #selector(THFindFriendInviteViewController.didTapFollowButton(_:)), for: UIControlEvents.touchUpInside)
        return cell
    }
}

extension THFindFriendInviteViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 130, height: 150)
    }
}




