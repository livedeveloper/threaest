//
//  THFriendFacebookContactViewController.swift
//  Threadest
//
//  Created by Jaydeep on 29/12/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import Contacts
import FacebookCore
import FacebookLogin
import FBSDKCoreKit


class THFriendFacebookContactViewController: UIViewController {

    @IBOutlet weak var headerLabel: UILabel!
    var usersToFollow: [FollowPeople] = [FollowPeople]()
    
    var isContact: Bool = true
    @IBOutlet weak var objTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        objTableView.register(UINib(nibName: THFriendMatchStyleTableViewCell.className, bundle: nil), forCellReuseIdentifier: THFriendMatchStyleTableViewCell.className)
        // Do any additional setup after loading the view.
        if isContact {
            headerLabel.text = "My Contacts"
            scanContact()
        } else {
            headerLabel.text = "Facebook Friends"
            if let accessToken = AccessToken.current {
                self.usersFromFacebook(FBAuthToken: accessToken.authenticationToken)
            } else {
                LoginManager().logIn([.publicProfile, .email,.userFriends], viewController: self) { (loginResult) in
                    print(loginResult)
                    switch loginResult {
                    case .success(_, _, let accessToken):
                        self.usersFromFacebook(FBAuthToken: accessToken.authenticationToken)
                        break
                    case .failed(let error):
                        break
                    default:
                        break
                    }
                }
            }
        }
    }
    
    func scanContact() {
        var  arrPhones = [String]()
        if ScanContacts.shared.arrContacts.count == 0 {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
            self.usersToFollow = THUserManager.sharedInstance.usersToFollowFromPhone
            self.objTableView.reloadData()
            ScanContacts.shared.findContactsOnBackgroundThread { (true, resultContacts) in
                
                for object: ContactInfo in resultContacts as [ContactInfo]
                {
                    arrPhones.append(object.phone)
                }
                let trimmed = arrPhones.description.replacingOccurrences(of: "[", with:"").replacingOccurrences(of: "]", with:"").replacingOccurrences(of: " ", with: "")
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                self.usersFromContact(PhoneNumber: trimmed, isLoad: true)
            }
            /*THUserManager.sharedInstance.performUsertoFollow(completion: { (error) in
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
                guard let error = error else {
                    self.usersToFollow = THUserManager.sharedInstance.usersToFollow
                    self.objTableView.reloadData()
                    ScanContacts.shared.findContactsOnBackgroundThread { (true, resultContacts) in
                        
                        for object: ContactInfo in resultContacts as [ContactInfo]
                        {
                            arrPhones.append(object.phone)
                        }
                        let trimmed = arrPhones.description.replacingOccurrences(of: "[", with:"").replacingOccurrences(of: "]", with:"").replacingOccurrences(of: " ", with: "")
                        self.usersFromContact(PhoneNumber: trimmed, isLoad: false)
                    }
                    return
                }
                debugPrint(error)
            })*/
        } else {
            for object: ContactInfo in ScanContacts.shared.arrContacts as [ContactInfo]
            {
                arrPhones.append(object.phone)
            }
            let trimmed = arrPhones.description.replacingOccurrences(of: "[", with:"").replacingOccurrences(of: "]", with:"").replacingOccurrences(of: " ", with: "")
            self.usersFromContact(PhoneNumber: trimmed, isLoad: true)
        }
    }
    
    func usersFromContact(PhoneNumber: String, isLoad: Bool) {
        if isLoad {
            GeneralMethods.sharedInstance.loadLoading(view: self.view)
        }
        THUserManager.sharedInstance.usersToFollowFromPhone = [FollowPeople]()
        THUserManager.sharedInstance.performUsertoFollowFromPhoneBook(for: PhoneNumber, completion: { (error) in
            if isLoad {
                GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            }
            if let error = error {
                self.showAlert(withMessage: error.localizedDescription)
            } else {
                self.usersToFollow = THUserManager.sharedInstance.usersToFollowFromPhone
                self.objTableView.reloadData()
                if self.usersToFollow.count == 0 {
                    self.showAlert(title: "No contacts found", message: "Threadest is better with friends.", buttonText: "Invite", okClick: {
                        let findFriendConnectController = THFindFriendsConnectViewController(nibName: THFindFriendsConnectViewController.className, bundle: nil)
                        let navFindFriendConnectController = UINavigationController(rootViewController: findFriendConnectController)
                        self.present(navFindFriendConnectController, animated: true, completion: nil)
                    })
                }
            }
        })
    }
    
    func usersFromFacebook(FBAuthToken: String) {
        GeneralMethods.sharedInstance.loadLoading(view: self.view)
        THUserManager.sharedInstance.performUsertoFollowFromFB(for: FBAuthToken, completion: { (error) in
            GeneralMethods.sharedInstance.dismissLoading(view: self.view)
            if let error = error {
                self.showAlert(withMessage: error.localizedDescription)
            } else {
                self.usersToFollow = THUserManager.sharedInstance.usersToFollowFromFB
                self.objTableView.reloadData()
                if self.usersToFollow.count == 0 {
                    self.showAlert(title: "No friend found", message: "Threadest is better with friends.", buttonText: "Invite", okClick: {
                        let findFriendConnectController = THFindFriendsConnectViewController(nibName: THFindFriendsConnectViewController.className, bundle: nil)
                        let navFindFriendConnectController = UINavigationController(rootViewController: findFriendConnectController)
                        self.present(navFindFriendConnectController, animated: true, completion: nil)
                    })
                }
            }
        })
    }
    
    @IBAction func didTapFollowUnFollowButton(_ sender: UIButton) {
        if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
            if usercurrent.tempAccount {
                appdelegate.setupAccountPopup(viewController: self)
                return
            }
        }
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions(), animations: {
                
                sender.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
                
            }, completion: {_ in
                
                UIView.animate(withDuration: 0.2, animations: {
                    sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                })
                if sender.isSelected {
                    THSocialInteractionPostsManager.sharedInstance.performUnfollowUser(for: self.usersToFollow[sender.tag].id, completion: { (error) in
                        sender.isSelected = false
                        self.usersToFollow[sender.tag].isFollow = false
                    })
                    
                }
                else {
                    THUserManager.sharedInstance.performFollowUser(with: self.usersToFollow[sender.tag].id, completion: { (error) in
                        guard let error = error else {
                            sender.isSelected = true
                            self.usersToFollow[sender.tag].isFollow = true
                            return
                        }
                        debugPrint(error)
                    })
                }
            })
        }
    }
    
    @IBAction func followUnfollowTouchDragExist(_ sender: DesignableButton) {
        UIView.animate(withDuration: 0.1, delay: 0.0, options: UIViewAnimationOptions(), animations: {
            sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }, completion: { _ in
        })
    }
    
    
    @IBAction func followUnfollowTouchDown(_ sender: UIButton) {
        UIView.animate(withDuration: 0.0, animations: {
            sender.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        })
    }
    
    @IBAction func didTapOnBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension THFriendFacebookContactViewController: UITableViewDelegate {
    
}

extension THFriendFacebookContactViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.usersToFollow.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let matchStyleCell = tableView.dequeueReusableCell(withIdentifier: THFriendMatchStyleTableViewCell.className, for: indexPath) as! THFriendMatchStyleTableViewCell
        matchStyleCell.selectionStyle = .none
        let user = self.usersToFollow[indexPath.row]
        matchStyleCell.profileImageView.sd_setImage(with: URL(string: user.thumbnailImageUrl), placeholderImage: nil, options: .retryFailed, completed: nil)
        matchStyleCell.usernameLabel.text = user.username
        matchStyleCell.followButton.tag = indexPath.row
        matchStyleCell.followButton.addTarget(self, action: #selector(THFriendFacebookContactViewController.didTapFollowUnFollowButton(_:)), for: .touchUpInside)
        matchStyleCell.followButton.addTarget(self, action: #selector(THFriendFacebookContactViewController.followUnfollowTouchDown(_:)), for: .touchDown)
        matchStyleCell.followButton.addTarget(self, action: #selector(THFriendFacebookContactViewController.followUnfollowTouchDragExist(_:)), for: .touchDragExit)
        matchStyleCell.followButton.isSelected = self.usersToFollow[indexPath.row].isFollow
        matchStyleCell.saparatorLabel.isHidden = false
        return matchStyleCell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "FRIENDS ON THREADEST"
    }
}
