
//
//  AppDelegate.swift
//  Threadest
//
//  Created by Mihai Andrei Rustiuc on 05/03/17.
//  Copyright © 2017 JC. All rights reserved.
//

import UIKit
import FacebookCore
import FBSDKCoreKit
import Stripe
import KeychainSwift
import SwiftyJSON
import Fabric
import Crashlytics
import IQKeyboardManagerSwift
import TwitterKit
import ARKit
import UserNotifications
import SDWebImage

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var isLocationUpdated: Bool?
    var latLongTimer: Timer?

    var isPressedHome = false
//    fileprivate let threadestFirstRunDefaultsKey = "threadestFirstRun"

    //for swipe variable
//    var verticle:VerticalScrollViewController!
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        window = UIWindow(frame: UIScreen.main.bounds)
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        if let currentUser = THUserManager.sharedInstance.currentUserdata() {
            THUserManager.sharedInstance.perfomCurrentInfluencerScore(with: currentUser.userId)
        }
//        let userDefaults = UserDefaults.standard
//
//        //if !userDefaults.bool(forKey: threadestFirstRunDefaultsKey)
//        if userDefaults.object(forKey: threadestFirstRunDefaultsKey) == nil {
//            userDefaults.set(true, forKey: threadestFirstRunDefaultsKey)
//            userDefaults.synchronize()
//            let keychainSwift = KeychainSwift()
//            keychainSwift.clear()
//        }

        ThreadestStorage.clearDataInKeychainIfNeeded()

        SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)

        Heap.setAppId("278445279");
        #if DEBUG
           Heap.enableVisualizer();
           //Fabric.sharedSDK().debug = true
        #endif
        TWTRTwitter.sharedInstance().start(withConsumerKey: "zCnurnx6ECE12aVm2aTq3FzLQ", consumerSecret: "7LeFYKRYBtlb8d2cM4yZwIlR1PRHfhznQLqvwZ1i5DvemQTljC")

        Fabric.with([Crashlytics.self, STPAPIClient.self])
        STPPaymentConfiguration.shared().publishableKey = "pk_live_C3QZz5EnDQFDTFIwZ998nJ5x"
        STPPaymentConfiguration.shared().appleMerchantIdentifier = "merchant.com.threadest.threadest"



        if let _ = THUserManager.sharedInstance.currentUserdata() {
            if THUserManager.sharedInstance.authToken() == "" {
                startScreenView(isNavigateToLogin: false)
            } else {
                if THUserManager.sharedInstance.hasRegisteredForPushNotifications() {
                    // We need to call this on every app launch - in case the device token changes over time, we need to update it for the current user
                    THUserManager.sharedInstance.requestPushNotificationPermissions(completion: {granted in
                        debugPrint(granted)
                    })
                }
                swipeNavigationView()
            }
        }
        else {
            startScreenView(isNavigateToLogin: false)
        }
        isLocationUpdated = false
        if let launchOptions = launchOptions {
            if let userInfo = launchOptions[UIApplicationLaunchOptionsKey.remoteNotification] as? [AnyHashable : Any] {
                let userInfoDictionary = JSON(userInfo)
                let notificationType = userInfoDictionary["notification_type"].stringValue

                switch notificationType {
                case "Follow":
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                        NotificationCenter.default.post(name: Notification.Name(postNotificationType.notificationFollow.rawValue), object: userInfoDictionary.dictionaryValue, userInfo: userInfo)
                    }
                    break
                case "Comment":
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                        NotificationCenter.default.post(name: Notification.Name(postNotificationType.notificationPost.rawValue), object: userInfoDictionary.dictionaryValue, userInfo: userInfo)
                    }
                    break
                case "Vote":
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                        NotificationCenter.default.post(name: Notification.Name(postNotificationType.notificationPost.rawValue), object: userInfoDictionary.dictionaryValue, userInfo: userInfo)
                    }
                    break
                case "Score Increase":
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                        NotificationCenter.default.post(name: Notification.Name(postNotificationType.notificationInfluenceScore.rawValue), object: userInfoDictionary.dictionaryValue, userInfo: userInfo)
                    }
                    break
                case "Product":
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                        NotificationCenter.default.post(name: Notification.Name(postNotificationType.notificationProduct.rawValue), object: userInfoDictionary.dictionaryValue, userInfo: userInfo)
                    }
                    break
                default:
                    break
                }

                // Handle push notifications here
            } else {
                //let tabController = window!.rootViewController as? UITabBarController
                //appdelegate.verticle.scrollUP(isAnimated: true)
                //tabController?.selectedIndex = 0
              /*  DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                    NotificationCenter.default.post(name: Notification.Name(postNotificationType.notificationNearBy.rawValue), object: nil , userInfo: nil)
                }*/
            }
        }

        SDImageCache.shared().config.shouldDecompressImages = false
        SDWebImageDownloader.shared().shouldDecompressImages = false
        return true
    }

    func swipeNavigationView() {
//        var arProfileClosetController: UIViewController!
//        if #available(iOS 11.0, *) {
//            if (ARConfiguration.isSupported) {
//                if userdefault.object(forKey: userDefaultKey.ARONOFF.rawValue) == nil {
//                    arProfileClosetController = THNonARClosetViewController(nibName: THNonARClosetViewController.className, bundle: nil)
//                } else {
//                    if let ARON = userdefault.object(forKey: userDefaultKey.ARONOFF.rawValue) as? Bool {
//                        if ARON {
//                            arProfileClosetController = THARClosetViewController(nibName: THARClosetViewController.className, bundle: nil)
//                        } else {
//                            arProfileClosetController = THNonARClosetViewController(nibName: THNonARClosetViewController.className, bundle: nil)
//                        }
//                    } else {
//                        arProfileClosetController = THARClosetViewController(nibName: THARClosetViewController.className, bundle: nil)
//                    }
//                }
//            } else {
//                arProfileClosetController = THNonARClosetViewController(nibName: THNonARClosetViewController.className, bundle: nil)
//            }
//        } else {
//            arProfileClosetController = THNonARClosetViewController(nibName: THNonARClosetViewController.className, bundle: nil)
//        }
//        let navClosetVC = UINavigationController(rootViewController: arProfileClosetController)
//
//        let socialVC = THSocialViewController(nibName: THSocialViewController.className, bundle: nil)
//        let navSocialVC = UINavigationController(rootViewController: socialVC)
//
//        let swipeViewController = VerticalScrollViewController.verticalScrollVcWith(middleVc: navClosetVC, topVc: nil, bottomVc: navSocialVC)
//
//        verticle = swipeViewController.delegate
//        let navTabbar = UINavigationController(rootViewController: swipeViewController)

        let tabBarVC = THTabViewController()
        let navTabbar = UINavigationController(rootViewController: tabBarVC)
        navTabbar.setNavigationBarHidden(true, animated: false)
        window!.rootViewController = navTabbar
        window!.makeKeyAndVisible()
    }

    func startScreenView(isNavigateToLogin: Bool) {
        let startViewController2 = THStartViewController(nibName: THStartViewController.className, bundle: nil)
        let siteTourOneController = THOnboardingFirstViewController(nibName: THOnboardingFirstViewController.className, bundle: nil)
        if #available(iOS 11.0, *) {
            if (ARConfiguration.isSupported) {
                siteTourOneController.configure(with: #imageLiteral(resourceName: "site_tour1_ar"), showsPanels: true, isVideo: false)
            } else {
                siteTourOneController.configure(with: #imageLiteral(resourceName: "site_tour1_nonar"), showsPanels: true,  isVideo: false)
            }
        } else {
            siteTourOneController.configure(with: #imageLiteral(resourceName: "site_tour1_nonar"), showsPanels: true, isVideo: false)
        }


        let siteTourTwoController = THOnboardingPageViewController(nibName: THOnboardingPageViewController.className, bundle: nil)
        siteTourTwoController.configure(with: #imageLiteral(resourceName: "site_tour2"), showsPanels: true, isVideo: false)

        let siteTourThreeController = THOnboardingPageViewController(nibName: THOnboardingPageViewController.className, bundle: nil)
        siteTourThreeController.configure(with: #imageLiteral(resourceName: "site_tour3"),showsPanels: true, isVideo: false)

        let siteTourFourController = THOnboardingPageViewController(nibName: THOnboardingPageViewController.className, bundle: nil)
        siteTourFourController.configure(with:#imageLiteral(resourceName: "site_tour4"), showsPanels: true, isVideo: false)

        let siteTourFiveController = THOnboardingPageViewController(nibName: THOnboardingPageViewController.className, bundle: nil)
        siteTourFiveController.configure(with: #imageLiteral(resourceName: "site_tour5"), showsPanels: true, isVideo: false)

        let onboardingController = THOnboardingViewController(onboardingViewControllers: [siteTourOneController,siteTourTwoController,siteTourThreeController,siteTourFourController,siteTourFiveController])
        onboardingController.delegate = startViewController2
        onboardingController.isRedirectToLogin = isNavigateToLogin
        siteTourTwoController.delegate = onboardingController
        siteTourThreeController.delegate = onboardingController
        siteTourFourController.delegate = onboardingController
        siteTourFiveController.delegate = onboardingController

        let navigationController = UINavigationController(rootViewController: onboardingController)

        window = UIWindow(frame: UIScreen.main.bounds)

        navigationController.setNavigationBarHidden(true, animated: false)
        window!.rootViewController = navigationController

        window!.makeKeyAndVisible()
    }

    func setupAccountPopup(viewController: UIViewController) {
        let saveAccountPopup = THSaveAccountPopupVC(nibName: THSaveAccountPopupVC.className, bundle: nil)
        viewController.addChildViewController(saveAccountPopup)
        saveAccountPopup.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
        saveAccountPopup.saveAccountPopupCompletionHander = {
            let saveAccountPopup2 = THSaveAccountPopup2VC(nibName: THSaveAccountPopup2VC.className, bundle: nil)
            viewController.addChildViewController(saveAccountPopup2)
            saveAccountPopup2.view.frame = CGRect(x: 0, y: 0, width: screenwidth, height: screenheight)
            viewController.view.addSubview(saveAccountPopup2.view)
        }
        viewController.view.addSubview(saveAccountPopup.view)
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        if userdefault.object(forKey: userDefaultKey.acceptPushNotification.rawValue) == nil {
            userdefault.set(true, forKey: userDefaultKey.acceptPushNotification.rawValue)
            userdefault.synchronize()
            //Add funnel for accept notification
            if let usercurrent = THUserManager.sharedInstance.currentUserdata() {
                Answers.logCustomEvent(withName: "Accept Notifications", customAttributes: ["userid":usercurrent.userId, "username": usercurrent.username])
            }
        }
        let token = deviceToken.map { String(format: "%.2hhx", $0) }.joined()

        THUserManager.sharedInstance.syncPNToken(token: token, completion: nil)
    }

    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            NotificationCenter.default.post(name: Notification.Name(postNotificationType.notificationNearBy.rawValue), object: nil , userInfo: nil)
        }
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        let userInfoDictionary = JSON(userInfo)
        let notificationType = userInfoDictionary["notification_type"].stringValue
        switch notificationType {
        case "Follow":
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                NotificationCenter.default.post(name: Notification.Name(postNotificationType.notificationFollow.rawValue), object: userInfoDictionary.dictionaryValue, userInfo: userInfo)
            }
            break
        case "Comment":
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                NotificationCenter.default.post(name: Notification.Name(postNotificationType.notificationPost.rawValue), object: userInfoDictionary.dictionaryValue, userInfo: userInfo)
            }
            break
        case "Vote":
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                NotificationCenter.default.post(name: Notification.Name(postNotificationType.notificationPost.rawValue), object: userInfoDictionary.dictionaryValue, userInfo: userInfo)
            }
            break
        case "Score Increase":
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                NotificationCenter.default.post(name: Notification.Name(postNotificationType.notificationInfluenceScore.rawValue), object: userInfoDictionary.dictionaryValue, userInfo: userInfo)
            }
            break
        case "Product":
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                NotificationCenter.default.post(name: Notification.Name(postNotificationType.notificationProduct.rawValue), object: userInfoDictionary.dictionaryValue, userInfo: userInfo)
            }
            break
        default:

            break
        }
    }

    /*func setTimerForUpdateLocation() {
        latLongTimer?.invalidate()
        latLongTimer = nil
        latLongTimer = Timer.scheduledTimer(timeInterval:180, target: self, selector: #selector(AppDelegate.callEndPointOfUpdateLocation), userInfo: nil, repeats: true)
    }*/

    func callEndPointOfUpdateLocation() {
        if let _ = THUserManager.sharedInstance.currentUserdata() {
            if let lat = LocationManager().currentLocation?.coordinate.latitude, let long = LocationManager().currentLocation?.coordinate.longitude {
                THUserManager.sharedInstance.performUpdateUserLocaltion(with: lat, logitude: long, completion: { (error) in

                })
            }
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        if let _ = THUserManager.sharedInstance.currentUserdata() {
            if THUserManager.sharedInstance.authToken() != "" {
                LocationManager().BackGroundLocation()
            }
            callEndPointOfUpdateLocation()
        }
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        if let currentUser = THUserManager.sharedInstance.currentUserdata() {
            THUserManager.sharedInstance.perfomCurrentInfluencerScore(with: currentUser.userId)
            if THUserManager.sharedInstance.authToken() != "" {
                LocationManager().forgroundLocation()
            }
            callEndPointOfUpdateLocation()
        }
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        /*if Branch.getInstance().application(app, open: url, options: options) {
            return true
        }*/
        if TWTRTwitter.sharedInstance().application(app, open:url, options: options) {
            return true
        }

        return SDKApplicationDelegate.shared.application(app, open: url, options: options)
    }

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        
         return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }

    // Respond to Universal Links
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        let pathURL = userActivity.webpageURL
        if let _ = THUserManager.sharedInstance.currentUserdata() {
            if (pathURL?.pathComponents.contains("products"))! {
                guard let productId = Int((pathURL?.pathComponents.last)!)  else {
                    return true
                }

                var dict = [String:String]()
                let components = URLComponents(url: pathURL!, resolvingAgainstBaseURL: false)!
                if let queryItems = components.queryItems {
                    for item in queryItems {
                        dict[item.name] = item.value!
                    }
                }
                guard let brandId = Int(dict["b"]!) else {
                    return true
                }

                guard let sc = Int(dict["sc"]!) else {
                    return true
                }
                GeneralMethods.sharedInstance.loadLoading(view: (self.window?.rootViewController?.view)!)
                THBrandProductManager.sharedInstance.retrieveBrandProfile(with: brandId, productId: productId, geofence: false, shareCode:"\(sc)") { (error) in
                    GeneralMethods.sharedInstance.dismissLoading(view: (self.window?.rootViewController?.view)!)
                    let threeDAvailable = THBrandProductManager.sharedInstance.brandProduct?.productGalleryImages.contains(where: { (imgGallery) -> Bool in
                        if imgGallery.image_type == "3D" {
                            return true
                        } else {
                            return false
                        }
                    })
                    
                    if threeDAvailable! {
                        if #available(iOS 11.0, *) {
                            if (ARConfiguration.isSupported) {
                                let ARShopShowController = THARShopShowViewController(nibName: THARShopShowViewController.className, bundle: nil)
                                ARShopShowController.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                                ARShopShowController.shareCode = "\(sc)"
                                let navShopShowVC = self.window?.rootViewController as! UINavigationController
                                navShopShowVC.navigationController?.pushViewController(ARShopShowController, animated: true)
                                return
                            }
                        }
                    }
                    let shopshowcontroller = THShopshowViewController(nibName: THShopshowViewController.className, bundle: nil)
                    shopshowcontroller.categoryDetail = THBrandProductManager.sharedInstance.brandProduct
                    shopshowcontroller.shareCode = "\(sc)"
                    let navShopShowVC = self.window?.rootViewController as! UINavigationController
                    navShopShowVC.navigationController?.pushViewController(shopshowcontroller, animated: true)
                    
                }
            } else if (pathURL?.pathComponents.contains("posts"))! {
                guard let postId = Int((pathURL?.pathComponents.last)!)  else {
                    return true
                }
                GeneralMethods.sharedInstance.loadLoading(view: (self.window?.rootViewController?.view)!)
                THProfilePostsManager.sharedInstance.retrieveNotificationPost(with: postId, completion: { (error) in
                    GeneralMethods.sharedInstance.dismissLoading(view: (self.window?.rootViewController?.view)!)
                    if THProfilePostsManager.sharedInstance.notificationPost == nil {
                        return
                    }
                    guard error != nil else {
                        let commentController = THCommentViewController(nibName: THCommentViewController.className, bundle: nil)
                        commentController.socialInteraction = THProfilePostsManager.sharedInstance.notificationPost
                        let commentVC = self.window?.rootViewController as! UINavigationController
                        commentVC.pushViewController(commentController, animated: true)
                        return
                    }
                })
            }
        }
        return true
    }
}

@available(iOS 9.0, *)
extension AppDelegate: UNUserNotificationCenterDelegate {

    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
    }

    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, forRemoteNotification userInfo: [AnyHashable : Any], withResponseInfo responseInfo: [AnyHashable : Any], completionHandler: @escaping () -> Void) {
    }

    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, forRemoteNotification userInfo: [AnyHashable : Any], completionHandler: @escaping () -> Void) {
    }
}


